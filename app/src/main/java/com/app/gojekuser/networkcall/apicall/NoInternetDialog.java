package com.app.gojekuser.networkcall.apicall;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.app.taxiapp.R;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.InternetCallback;

public class NoInternetDialog extends Dialog {

    private InternetCallback internetCallback;
    TextView try_again,no_internet_connection_text;

    public NoInternetDialog(@NonNull Context context) {
        this(context, R.style.FullScreenDialog);
    }


    private NoInternetDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.no_internet_dialog);
        this.setCancelable(true);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.setGravity(Gravity.BOTTOM);
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        try_again = findViewById(R.id.try_again);
        no_internet_connection_text = findViewById(R.id.no_internet_connection_text);
        try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkConnected(getContext())) {
                    dismiss();
                    internetCallback.onConnected();

                } else {
                    shakeAnimation();
                }
            }
        });
    }

    private void shakeAnimation() {
        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake_animation);
        no_internet_connection_text.startAnimation(shake);
    }

    public void setOnConnected(InternetCallback internetCallback) {
        this.internetCallback = internetCallback;
    }
}
