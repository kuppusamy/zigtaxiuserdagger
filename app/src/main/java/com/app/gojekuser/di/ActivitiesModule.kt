package com.app.gojekuser.di


import com.app.foodappuser.view.activities.EmptyFoodLayout
import com.app.gojekuser.MainActivity
import com.app.gojekuser.di.ActivityInterfaceModules
import com.app.taxiapp.view.activity.EmptyTaxiActviity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.app.gojekuser.di.ActivityScope

@Module
interface ActivitiesModule {

//    @ContributesAndroidInjector(modules = [LoginActivityModule::class])
//    @ActivityScope
//    fun provideLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    @ActivityScope
    fun provideMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [ActivityInterfaceModules::class])
    @ActivityScope
    fun provideEmptyTaxiActviity(): EmptyTaxiActviity

    @ContributesAndroidInjector(modules = [ActivityInterfaceModules::class])
    @ActivityScope
    fun provideEmptyFoodLayout(): EmptyFoodLayout


}

