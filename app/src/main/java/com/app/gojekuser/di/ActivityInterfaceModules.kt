package com.app.gojekuser.di

import com.app.gojekuser.navigators.FoodNavigator
import com.app.gojekuser.navigators.TaxiNavigator
import com.app.foodappuser.NavigationInterFaceFood
import com.app.taxiapp.NavigationInterFaceTaxi
import dagger.Binds
import dagger.Module

@Module
interface ActivityInterfaceModules {

    @Binds
    fun bindNavigationInterfaceFood(foodNavigator: FoodNavigator): NavigationInterFaceFood

    @Binds
    fun bindNavigationInterfaceTaxi(taxiNavigator: TaxiNavigator) : NavigationInterFaceTaxi
}