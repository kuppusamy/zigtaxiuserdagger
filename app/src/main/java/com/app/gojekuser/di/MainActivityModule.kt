package com.app.gojekuser.di

import com.app.gojekuser.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface MainActivityModule {

//    @ContributesAndroidInjector(modules = [FragmentAModule::class])
//    @FragmentScope
//    fun provideFragmentA(): FragmentA

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    @ActivityScope
    fun providerMaiActivity(): MainActivity


}