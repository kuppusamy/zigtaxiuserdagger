package com.app.gojekuser.di

import com.app.gojekuser.ApplicationClass
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,ActivitiesModule::class])
interface ApplicationComponent : AndroidInjector<ApplicationClass> {

    override fun inject(applicationClass: ApplicationClass)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(applicationClass: ApplicationClass): ApplicationComponent.Builder

        fun build(): ApplicationComponent
    }
}

