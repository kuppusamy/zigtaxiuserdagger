package com.app.gojekuser.navigators

import android.app.Activity
import com.app.foodappuser.NavigationInterFaceFood
import com.app.foodappuser.view.activities.EmptyFoodLayout
import com.app.gojekuser.MainActivity
import com.app.gojekuser.di.ActivityScope
import javax.inject.Inject

@ActivityScope
class FoodNavigator @Inject constructor() : NavigationInterFaceFood {
    override fun goToMain(activity: Activity) {
        activity.startActivity(MainActivity.getIntent(activity))
    }

    override fun goToActvityA(activity: Activity) {
        activity.startActivity(EmptyFoodLayout.getIntent(activity))
    }
}