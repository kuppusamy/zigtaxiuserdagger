package com.app.gojekuser.navigators

import android.app.Activity
import com.app.gojekuser.MainActivity
import com.app.gojekuser.di.ActivityScope
import com.app.taxiapp.NavigationInterFaceTaxi
import com.app.taxiapp.view.activity.EmptyTaxiActviity
import javax.inject.Inject

@ActivityScope
class TaxiNavigator @Inject constructor() : NavigationInterFaceTaxi {
    override fun goToMain(activity: Activity) {
        activity.startActivity(MainActivity.getIntent(activity))
    }

    override fun goToActvityC(activity: Activity) {
        activity.startActivity(EmptyTaxiActviity.getIntent(activity))
    }

}