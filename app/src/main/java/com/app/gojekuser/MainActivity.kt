package com.app.gojekuser

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.foodappuser.view.activities.EmptyFoodLayout
import com.app.taxiapp.view.activity.EmptyTaxiActviity
import com.app.taxiapp.view.activity.MainActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DaggerAppCompatActivity() {


    companion object {
        fun getIntent(context: Context) = Intent(context, com.app.gojekuser.MainActivity::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        taxi.setOnClickListener { startActivity(Intent(this, EmptyTaxiActviity::class.java)) }
        food.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    com.app.foodappuser.view.activities.EmptyFoodLayout::class.java
                )
            )
        }

    }
}
