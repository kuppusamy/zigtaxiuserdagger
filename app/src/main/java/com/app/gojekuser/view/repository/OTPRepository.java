package com.app.gojekuser.view.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.app.gojekuser.networkcall.apicall.ApiCall;
import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.model.apiresponsemodel.ResendOTPResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

public class OTPRepository {
private SharedHelper sharedHelper;

    public OTPRepository(Context context) {
        sharedHelper=new SharedHelper(context);
    }

    public LiveData<ResendOTPResponse> resendOTP(InputForAPI inputForAPI) {
        final MutableLiveData<ResendOTPResponse> mutableLiveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ResendOTPResponse checkResponse = gson.fromJson(response.toString(), ResendOTPResponse.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                ResendOTPResponse checkResponse = new ResendOTPResponse();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }
    public LiveData<FlagCheckResponseModel> verifyOTP(InputForAPI inputForAPI) {
        final MutableLiveData<FlagCheckResponseModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                FlagCheckResponseModel checkResponse = gson.fromJson(response.toString(), FlagCheckResponseModel.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                FlagCheckResponseModel checkResponse = new FlagCheckResponseModel();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }
    public LiveData<AuthenticationResponse> loginWithOTP(InputForAPI inputForAPI) {
        final MutableLiveData<AuthenticationResponse> mutableLiveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                AuthenticationResponse checkResponse = gson.fromJson(response.toString(), AuthenticationResponse.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                AuthenticationResponse checkResponse = new AuthenticationResponse();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }

    public int getOTPtimer() {

        return Integer.parseInt(sharedHelper.getAppConfiguration().getData().getAuthConfig().getOtp_timer())*1000;
    }
}
