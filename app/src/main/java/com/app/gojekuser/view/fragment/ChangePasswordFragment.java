package com.app.gojekuser.view.fragment;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;


import com.app.gojekuser.R;
import com.app.gojekuser.databinding.FragmentChangePasswordBinding;
import com.app.gojekuser.helper.UrlHelper;
import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.gojekuser.view.activity.PhoneNumberActivity;
import com.app.gojekuser.view.viewmodel.ChangePasswordViewModel;
import com.app.gojekuser.view.viewmodel.CommonViewModel;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;

/**
 * A simple {@link Fragment} subclass.
 */

public class ChangePasswordFragment extends Fragment {
    private FragmentChangePasswordBinding binding;
    private CircularProgressButton progressButton;
    private ChangePasswordViewModel viewModel;
    private CommonViewModel commonViewModel;
    private boolean isPasswordVisibleOne = false;
    private boolean isPasswordVisibleTwo = false;


    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    public void enableProceedButton() {
        progressButton.setEnabled(true);
    }

    public void disableProceedButton() {
        progressButton.setEnabled(false);
    }


    private void togglePasswordvisibilityOne(EditText passwordEdittext, ImageView passwordHolder) {
        if (!isPasswordVisibleOne) {
            passwordHolder.setImageResource(R.drawable.hide_password);
            passwordEdittext.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            isPasswordVisibleOne = true;
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        } else {
            isPasswordVisibleOne = false;
            passwordHolder.setImageResource(R.drawable.show_password);
            passwordEdittext.setTransformationMethod(new PasswordTransformationMethod());
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        }
    }

    private void togglePasswordvisibilityTwo(EditText passwordEdittext, ImageView passwordHolder) {
        if (!isPasswordVisibleTwo) {
            passwordHolder.setImageResource(R.drawable.hide_password);
            passwordEdittext.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            isPasswordVisibleTwo = true;
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        } else {
            isPasswordVisibleTwo = false;
            passwordHolder.setImageResource(R.drawable.show_password);
            passwordEdittext.setTransformationMethod(new PasswordTransformationMethod());
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        }
    }


    private void preload() {
        binding.loadingLayout.setVisibility(View.VISIBLE);
        progressButton.startAnimation();
        disableProceedButton();

    }

    private void postload() {
        binding.loadingLayout.setVisibility(View.GONE);
    }

    private void revertCircularAnimation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressButton.revertAnimation(new Function0<Unit>() {
                    @Override
                    public Unit invoke() {
                        return null;
                    }
                });
            }
        }, 1000);
    }


    private void stopAnimation(final AuthenticationResponse authenticationResponse) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressButton.doneLoadingAnimation(Utils.getPrimaryColor(getActivity()), BitmapFactory.decodeResource(getResources(), R.drawable.done));
                postLoad();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        revertCircularAnimation();
                        if (authenticationResponse != null) {
                            if (authenticationResponse.getError()) {
                                postload();
                                enableProceedButton();
                                Utils.displayError(getActivity().getWindow().findViewById(android.R.id.content), authenticationResponse.getMsg());
                            } else {
                                postload();
                                handleChangeOtpSuccess();
                            }
                        }
                    }
                }, 500);


            }
        }, 1000);
    }


    private void handleChangeOtpSuccess() {
        movePhoneNumberActivity();
    }

    public void movePhoneNumberActivity() {
        Intent intent = new Intent(getActivity(), PhoneNumberActivity.class);
        getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
        intent.putExtra(Constants.IntentKeys.LOGIN_TYPE,Constants.LoginType.NORMAL);
        startActivity(intent);
        getActivity().finish();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                enableProceedButton();
            }
        }, 500);

    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false);
        progressButton = binding.nextButton;
        viewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        initListeners();
        return binding.getRoot();
    }


    public void postLoad() {
        binding.loadingLayout.setVisibility(View.GONE);

    }

    private void initListeners() {

        progressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.dismissKeyboard(getActivity());
                String getPasswordValidation = Utils.isValidPassword(binding.newPassword.getText().toString());
                if (getPasswordValidation.equalsIgnoreCase("true")) {
                    if (binding.newPassword.getText().toString().equalsIgnoreCase(binding.confirmPassword.getText().toString())) {
                        preload();
                        try {
                            changePassword();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        binding.confirmPassword.setError(getResources().getString(R.string.error_password_mismatch));
                    }

                } else {
                    binding.newPassword.setError(getPasswordValidation);
                }
            }
        });

        binding.newPasswordHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                togglePasswordvisibilityOne(binding.newPassword, binding.newPasswordHolder);
            }
        });


        binding.confirmPasswordHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                togglePasswordvisibilityTwo(binding.confirmPassword, binding.confirmPasswordHolder);
            }
        });

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        binding.blockView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }


    private void changePassword() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.MOBILE, commonViewModel.getUserMobile());
        jsonObject.put(Constants.ApiKeys.PASSWORD, binding.newPassword.getText().toString());
        jsonObject.put(Constants.ApiKeys.COUNTRY_CODE, commonViewModel.getUserCountryCode());
        jsonObject.put(Constants.ApiKeys.OTP, commonViewModel.getOTP());
        jsonObject.put(Constants.ApiKeys.UUID, Utils.getDeviceId(getActivity()));
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setUrl(UrlHelper.UPDATE_PASSWORD);
        proceedChangePassowrd(inputForAPI);

    }

    private void proceedChangePassowrd(InputForAPI inputForAPI) {
        viewModel.changePassword(inputForAPI).observe(this, new Observer<AuthenticationResponse>() {
            @Override
            public void onChanged(@Nullable AuthenticationResponse authenticationResponse) {
                stopAnimation(authenticationResponse);
            }
        });
    }


}
