package com.app.gojekuser.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;


import com.app.gojekuser.R;
import com.app.gojekuser.databinding.ActivitySignInBinding;
import com.app.gojekuser.view.adapter.SignInPageSlidingAdapter;
import com.app.gojekuser.view.viewmodel.SignInPageViewModel;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.customviews.CustomViewPager;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AppConfiguration;
import com.app.taxiapp.view.activity.BaseActivity;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class SignInActivity extends BaseActivity {
    ActivitySignInBinding signInBinding;
    public static CustomViewPager slidingViewPager;
    CircleIndicator circleIndicator;
    List<AppConfiguration.Slider> welcomePageModels;
    SignInPageViewModel signInPageViewModel;
    private SignInPageSlidingAdapter adapter;
    SharedHelper sharedHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedHelper = new SharedHelper(SignInActivity.this);
        signInBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in);
        signInPageViewModel = ViewModelProviders.of(this).get(SignInPageViewModel.class);
        welcomePageModels = signInPageViewModel.getHomepageContent();
        slidingViewPager = signInBinding.viewPager;
        setAdapter();
        signInBinding.skipText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToLastFragment();
            }
        });

    }

    private void moveToLastFragment() {
        slidingViewPager.setCurrentItem(welcomePageModels.size() - 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setAdapter() {


        adapter = new SignInPageSlidingAdapter(getSupportFragmentManager(), welcomePageModels,getIntent().getBooleanExtra(Constants.IntentKeys.SKIP,false));
        slidingViewPager.setAdapter(adapter);

        slidingViewPager.setOffscreenPageLimit(welcomePageModels.size());
        circleIndicator = signInBinding.indicator;
        circleIndicator.setViewPager(slidingViewPager);
        slidingViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i == (welcomePageModels.size() - 1)) {
                    signInBinding.skipParent.setVisibility(View.GONE);
                    circleIndicator.setVisibility(View.INVISIBLE);
                } else {
                    signInBinding.skipParent.setVisibility(View.VISIBLE);
                    circleIndicator.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


    }

    public static void displayError(View view, String toastmsg) {
        Snackbar snackbar = Snackbar.make(view, toastmsg, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }


}
