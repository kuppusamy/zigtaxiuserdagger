package com.app.gojekuser.view.fragment;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.app.gojekuser.MainActivity;
import com.app.gojekuser.R;
import com.app.gojekuser.databinding.FragmentPasswordBinding;
import com.app.gojekuser.helper.UrlHelper;
import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.gojekuser.view.viewmodel.CommonViewModel;
import com.app.gojekuser.view.viewmodel.PasswordViewModel;
import com.app.taxiapp.helpers.AnimationHelper;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.Callback;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;


import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;

/**
 * A simple {@link Fragment} subclass.
 */

public class PasswordFragment extends Fragment {
    private FragmentPasswordBinding binding;
    private CircularProgressButton nextButton;
    private PasswordViewModel model;
    private CommonViewModel commonViewModel;
    private Callback callback;
    private AnimationHelper.LoaderZigZagMain progressbar;
    private String TAG = PasswordFragment.class.getSimpleName();

    public PasswordFragment() {
        // Required empty public constructor
    }

    public void replaceListener(Callback callback) {
        this.callback = callback;
    }


    private void enableProceedButton() {
        nextButton.setEnabled(true);
    }

    private void disableProceedButton() {
        nextButton.setEnabled(false);
    }

    private void enableForgotButton() {
        nextButton.setEnabled(true);
    }

    private void disableForgotButton() {
        nextButton.setEnabled(false);
    }

    private void preload(String resetPassword) {

        if (resetPassword.equalsIgnoreCase(Constants.OtpType.LOGIN)) {
            startCircularAnimation();
        } else {
            startLineProgress();
        }
        binding.loadingLayout.setVisibility(View.VISIBLE);

    }

    private void startLineProgress() {
        progressbar.start();
    }

    private void stopLineProgress() {
        progressbar.stop();
    }

    private void postLoad() {
        stopLineProgress();
        binding.loadingLayout.setVisibility(View.GONE);
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_password, container, false);
        nextButton = binding.nextButton;
        model = ViewModelProviders.of(this).get(PasswordViewModel.class);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        progressbar = new AnimationHelper.LoaderZigZagMain(binding.progressBarLay.progressBar, binding.progressBarLay.loadingParent);
        initListeners();
        return binding.getRoot();
    }

    private void initListeners() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getPasswordValidation = Utils.isValidPassword(binding.password.getText().toString());
                if (getPasswordValidation.equalsIgnoreCase("true")) {
                    Utils.dismissKeyboard(getActivity());
                    preload(Constants.OtpType.LOGIN);
                    disableProceedButton();
                    try {
                        buildLoginInputs();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    binding.password.setError(getPasswordValidation);
                }
            }
        });


        binding.passwordHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.togglePasswordvisibility(binding.password, binding.passwordHolder);
            }
        });


        binding.forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preload(Constants.OtpType.RESET_PASSWORD);
                Utils.dismissKeyboard(getActivity());
                disableForgotButton();
                try {
                    buildForgotPasswordInput();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });


        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


    }

    private void buildForgotPasswordInput() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.MOBILE, commonViewModel.getUserMobile());
        jsonObject.put(Constants.ApiKeys.COUNTRY_CODE, commonViewModel.getUserCountryCode());
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setUrl(UrlHelper.FORGOT_PASSWORD);
        hitAPI(inputForAPI);
    }


    private void startCircularAnimation() {
        nextButton.startAnimation();
    }

    private void doneCircularAnimation() {
        nextButton.doneLoadingAnimation(ContextCompat.getColor(getActivity(), android.R.color.black),
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_done_white_48dp));
    }


    private void revertCircularAnimation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nextButton.revertAnimation(new Function0<Unit>() {
                    @Override
                    public Unit invoke() {
                        return null;
                    }
                });
            }
        }, 2000);

        nextButton.setEnabled(true);
    }


    public void stopAnimation(final AuthenticationResponse authenticationResponse) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doneCircularAnimation();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        postLoad();
                        if (authenticationResponse != null) {
                            if (authenticationResponse.getError()) {
                                Utils.displayError(getActivity().getWindow().findViewById(android.R.id.content), authenticationResponse.getMsg());
                                revertCircularAnimation();
                                enableProceedButton();
                            } else {
                                handleLoginSuccess(authenticationResponse);
                            }
                        }
                    }
                }, 200);
            }
        }, 200);


    }

    private void handleLoginSuccess(AuthenticationResponse authenticationResponse) {

        commonViewModel.setUserData(authenticationResponse);
        AnimationHelper.presentActivity(getActivity(), nextButton
                , MainActivity.class);
        revertCircularAnimation();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                enableProceedButton();
            }
        }, 500);

    }


    private void hitAPI(InputForAPI inputForAPI) {
        commonViewModel.flagCheckResponse(inputForAPI).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(@Nullable FlagCheckResponseModel flagCheckResponseModel) {
                postLoad();
                if (flagCheckResponseModel != null) {
                    enableForgotButton();
                    if (!flagCheckResponseModel.isError()) {
                        callback.onSuccess("");
                    } else {
                        Utils.displayError(getActivity().findViewById(android.R.id.content), flagCheckResponseModel.getMsg());
                    }
                }
            }
        });
    }


    private void buildLoginInputs() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.MOBILE, commonViewModel.getUserMobile());
        jsonObject.put(Constants.ApiKeys.PASSWORD, binding.password.getText().toString());
        jsonObject.put(Constants.ApiKeys.COUNTRY_CODE, commonViewModel.getUserCountryCode());
        jsonObject.put(Constants.ApiKeys.UUID, Utils.getDeviceId(getActivity()));
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.VERIFY_PASSWORD);
        inputForAPI.setJsonObject(jsonObject);
        proceedLogin(inputForAPI);
    }

    private void proceedLogin(InputForAPI inputForAPI) {
        model.loginWithPassword(inputForAPI).observe(this, new Observer<AuthenticationResponse>() {
            @Override
            public void onChanged(@Nullable AuthenticationResponse authenticationResponse) {
                stopAnimation(authenticationResponse);

            }
        });
    }

}
