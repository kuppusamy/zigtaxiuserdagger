package com.app.gojekuser.view.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.gojekuser.view.repository.OTPRepository;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.model.apiresponsemodel.ResendOTPResponse;

public class OTPViewModel extends AndroidViewModel {
    OTPRepository otpRepository;

    public OTPViewModel(@NonNull Application application) {
        super(application);
        otpRepository=new OTPRepository(application.getApplicationContext());

    }

    public int getOTPtimer()
    {
        return  otpRepository.getOTPtimer();

    }


    public LiveData<ResendOTPResponse> resendOTP(InputForAPI inputForAPI) {
        return otpRepository.resendOTP(inputForAPI);
    }


    public LiveData<FlagCheckResponseModel> verifyOTP(InputForAPI inputForAPI) {
        return otpRepository.verifyOTP(inputForAPI);
    }
    public LiveData<AuthenticationResponse> loginWithOTP(InputForAPI inputForAPI) {
        return otpRepository.loginWithOTP(inputForAPI);
    }



}
