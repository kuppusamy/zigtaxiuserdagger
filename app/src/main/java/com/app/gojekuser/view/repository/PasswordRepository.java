package com.app.gojekuser.view.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.app.gojekuser.networkcall.apicall.ApiCall;
import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

public class PasswordRepository {


    public LiveData<AuthenticationResponse> loginWithPassword(InputForAPI inputForAPI) {
        final MutableLiveData<AuthenticationResponse> mutableLiveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                AuthenticationResponse checkResponse = gson.fromJson(response.toString(), AuthenticationResponse.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                AuthenticationResponse checkResponse = new AuthenticationResponse();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }

}
