package com.app.gojekuser.view.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.gojekuser.view.repository.PasswordRepository;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;


public class PasswordViewModel extends AndroidViewModel {
    private PasswordRepository passwordRepository;

    public PasswordViewModel(@NonNull Application application) {
        super(application);
        passwordRepository = new PasswordRepository();
    }


    public LiveData<AuthenticationResponse> loginWithPassword(InputForAPI inputForAPI) {
        return passwordRepository.loginWithPassword(inputForAPI);
    }



}
