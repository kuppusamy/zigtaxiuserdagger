package com.app.gojekuser.view.fragment;

import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;

import androidx.databinding.DataBindingUtil;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.app.gojekuser.MainActivity;
import com.app.gojekuser.R;
import com.app.gojekuser.databinding.FragmentRegisterBinding;
import com.app.gojekuser.helper.UrlHelper;
import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.gojekuser.view.viewmodel.CommonViewModel;
import com.app.gojekuser.view.viewmodel.RegisterViewModel;
import com.app.taxiapp.helpers.AnimationHelper;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.edittextfilter.NoInitialSpaceFilter;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.model.apiresponsemodel.SocialLoginResponseModel;
import com.hbb20.CountryCodePicker;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;


public class RegisterFragment extends Fragment {
    private FragmentRegisterBinding binding;
    CountryCodePicker countryCodePicker;
    private RegisterViewModel registerViewModel;
    private CommonViewModel commonViewModel;
    private CircularProgressButton progressButton;
    public String loginType;
    SocialLoginResponseModel socialLoginResponseModel;
    private SharedHelper sharedHelper;

    public RegisterFragment() {
        // Required empty public constructor
    }


    public void enableProceedButton() {
        progressButton.setEnabled(true);
    }

    public void disableProceedButton() {
        progressButton.setEnabled(false);
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        progressButton = binding.nextButton;
        countryCodePicker = binding.countryCodePicker;
        sharedHelper = new SharedHelper(getActivity());
        Locale locale = new Locale(sharedHelper.getSelectedLanguage());
        try {

            if (locale.getLanguage().equalsIgnoreCase("en")) {
                countryCodePicker.changeDefaultLanguage(CountryCodePicker.Language.valueOf("ENGLISH"));
            } else {
                countryCodePicker.changeDefaultLanguage(CountryCodePicker.Language.valueOf(locale.getDisplayLanguage(Locale.ENGLISH).toUpperCase()));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            countryCodePicker.changeDefaultLanguage(CountryCodePicker.Language.valueOf("ENGLISH"));

        }

        initListeners();
        initFilters();
        setData();
        return binding.getRoot();
    }


    private void setData() {
        binding.phoneNumber.setText(commonViewModel.getUserMobile());
        binding.countryCodePicker.setCountryForNameCode(commonViewModel.getCountryNameCode());
        if (registerViewModel.getConfigurationType().equalsIgnoreCase(Constants.AuthType.PASSWORD)) {
            binding.passwordParent.setVisibility(View.VISIBLE);
        } else {
            binding.passwordParent.setVisibility(View.GONE);
        }
        loginType = getArguments().getString(Constants.IntentKeys.LOGIN_TYPE);
        if (!loginType.equalsIgnoreCase(Constants.LoginType.NORMAL)) {
            socialLoginResponseModel = getArguments().getParcelable(Constants.ArgumentKeys.DATA);
            binding.firstName.setText(socialLoginResponseModel.getFirst_name());
            binding.lastName.setText(socialLoginResponseModel.getLast_name());
            binding.email.setText(socialLoginResponseModel.getEmail());
        }
    }

    private void initFilters() {
        binding.firstName.setFilters(new InputFilter[]{new NoInitialSpaceFilter()});
        binding.lastName.setFilters(new InputFilter[]{new NoInitialSpaceFilter()});
    }

    private void initListeners() {

        binding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.dismissKeyboard(getActivity());
                if (validInputs().equalsIgnoreCase("true")) {
                    disableProceedButton();
                    preload();
                    postInputs();
                }
            }
        });

        binding.passwordHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.togglePasswordvisibility(binding.password, binding.passwordHolder);
            }
        });

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
    }


    private void postInputs() {

        registerViewModel.registerUser(buildRegisterInputs()).observe(this, new Observer<AuthenticationResponse>() {
            @Override
            public void onChanged(@Nullable AuthenticationResponse authenticationResponse) {
                stopAnimation(authenticationResponse);
            }
        });

    }

    public void preload() {
        binding.loadingLayout.setVisibility(View.VISIBLE);
        progressButton.startAnimation();
    }

    public void postload() {
        binding.loadingLayout.setVisibility(View.GONE);
    }

    private void revertCircularAnimation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressButton.revertAnimation(new Function0<Unit>() {
                    @Override
                    public Unit invoke() {
                        return null;
                    }
                });
            }
        }, 1000);
    }


    public void stopAnimation(final AuthenticationResponse authenticationResponse) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressButton.doneLoadingAnimation(Utils.getPrimaryColor(getActivity()), BitmapFactory.decodeResource(getResources(), R.drawable.done));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        revertCircularAnimation();
                        if (authenticationResponse != null) {
                            if (authenticationResponse.getError()) {
                                postload();
                                enableProceedButton();
                                Utils.displayError(getActivity().getWindow().findViewById(android.R.id.content), authenticationResponse.getMsg());
                            } else {
                                postload();
                                handleRegisterSuccess(authenticationResponse);
                            }
                        }
                    }
                }, 500);


            }
        }, 1000);
    }


    private void moveMainActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void handleRegisterSuccess(AuthenticationResponse authenticationResponse) {
        commonViewModel.setUserData(authenticationResponse);
        AnimationHelper.presentActivity(getActivity(), progressButton, MainActivity.class);
        disableProceedButton();
    }

    private String validInputs() {
        String val = "true";
        String isPasswordValid = Utils.isValidPassword(binding.password.getText().toString());
        if (!Utils.isValidMobile(binding.phoneNumber.getText().toString())) {
            binding.phoneNumber.setError(getResources().getString(R.string.error_enter_valid_mobile));
            val = "false";
            return val;
        } else if (Utils.isEmpty(binding.firstName.getText().toString())) {
            binding.firstName.setError(getResources().getString(R.string.error_enter_first_name));
            val = "false";
            return val;
        } else if (Utils.isEmpty(binding.lastName.getText().toString())) {
            binding.lastName.setError(getResources().getString(R.string.error_enter_last_name));
            val = "false";
            return val;
        } else if (!Utils.isValidEmail(binding.email.getText().toString())) {
            binding.email.setError(getResources().getString(R.string.error_enter_valid_email));
            val = "false";
            return val;
        } else if (!isPasswordValid.equalsIgnoreCase("true")) {
            if (registerViewModel.getConfigurationType().equalsIgnoreCase(Constants.AuthType.PASSWORD)) {
                binding.password.setError(isPasswordValid);
                val = "false";
                return val;
            } else {
                return val;
            }
        } else {
            return val;
        }

    }

    private InputForAPI buildRegisterInputs() {
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ApiKeys.COUNTRY_CODE, commonViewModel.getUserCountryCode());
            jsonObject.put(Constants.ApiKeys.MOBILE, commonViewModel.getUserMobile());
            jsonObject.put(Constants.ApiKeys.EMAIL, binding.email.getText().toString());
            jsonObject.put(Constants.ApiKeys.FIRST_NAME, binding.firstName.getText().toString());
            jsonObject.put(Constants.ApiKeys.LAST_NAME, binding.lastName.getText().toString());
            jsonObject.put(Constants.ApiKeys.COUNTRY_ID, binding.countryCodePicker.getSelectedCountryNameCode());
            jsonObject.put(Constants.ApiKeys.LOGIN_TYPE, loginType);
            jsonObject.put(Constants.ApiKeys.LANGUAGE_NAME, sharedHelper.getSelectedLanguage());
            if (!loginType.equalsIgnoreCase(Constants.LoginType.NORMAL)) {
                jsonObject.put(Constants.ApiKeys.SOCIAL_TOKEN, socialLoginResponseModel.getId());
            }
            jsonObject.put(Constants.ApiKeys.PASSWORD, binding.password.getText().toString());
            jsonObject.put(Constants.ApiKeys.UUID, Utils.getDeviceId(getActivity()));
            inputForAPI.setJsonObject(jsonObject);
            inputForAPI.setUrl(UrlHelper.SIGN_UP);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return inputForAPI;
    }

}
