package com.app.gojekuser.view.fragment;


import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import com.app.gojekuser.MainActivity;
import com.app.gojekuser.R;
import com.app.gojekuser.databinding.FragmentOtpBinding;
import com.app.gojekuser.helper.UrlHelper;
import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.gojekuser.view.viewmodel.CommonViewModel;
import com.app.gojekuser.view.viewmodel.OTPViewModel;
import com.app.taxiapp.helpers.AnimationHelper;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.customviews.otplayout.OnOtpCompletionListener;
import com.app.taxiapp.helpers.interfaces.Callback;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.model.apiresponsemodel.ResendOTPResponse;

import org.json.JSONException;
import org.json.JSONObject;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;

/**
 * A simple {@link Fragment} subclass.
 */
public class OTPFragment extends Fragment implements OnOtpCompletionListener {

    private FragmentOtpBinding binding;

    private CommonViewModel commonViewModel;
    private Callback callback;
    private OTPViewModel otpViewModel;
    private CountDownTimer countDownTimer;
    private String otpType;
    private SharedHelper sharedHelper;
    private String resendText;
    private AnimationHelper.LoaderZigZagMain progressbar;
    private String TAG = OTPFragment.class.getSimpleName();

    public OTPFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_otp, container, false);
        resendText = getResources().getString(R.string.resend_otp);
        sharedHelper = new SharedHelper(getActivity());
        otpViewModel = ViewModelProviders.of(this).get(OTPViewModel.class);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        if (getArguments() != null) {
            otpType = getArguments().getString(Constants.ArgumentKeys.TYPE);
        }
        progressbar = new AnimationHelper.LoaderZigZagMain(binding.progressBar.progressBar, binding.progressBar.loadingParent);
        initListeners();
        startTimer();
        return binding.getRoot();
    }

    public void enableProceedButton() {
        binding.nextButton.setEnabled(true);
    }

    public void disableProceedButton() {
        binding.nextButton.setEnabled(false);
    }


    public void enableResendButton() {
        binding.nextButton.setEnabled(true);
    }

    public void disableResendButton() {
        binding.nextButton.setEnabled(false);
    }

    public void replaceListener(Callback callback) {
        this.callback = callback;
    }


    private void initListeners() {
        binding.resendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.resendOTP.getText().toString().equalsIgnoreCase(getString(R.string.resend_otp))) {
                    Utils.dismissKeyboard(getActivity());
                    preload();
                    disableResendButton();
                    try {
                        resendOTP();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        binding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getOtp().length() > 3) {
                    disableProceedButton();
                    Utils.dismissKeyboard(getActivity());
                    preload();

                    if (otpType.equals(Constants.OtpType.LOGIN)) {
                        try {
                            loginWithOTP();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            verifyOTP();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Utils.dismissKeyboard(getActivity());
                    Utils.displayError(getActivity().findViewById(android.R.id.content), getString(R.string.please_enter_valid_otp));
                }

            }
        });

        binding.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

    }

    private void loginWithOTP() throws JSONException {

        otpViewModel.loginWithOTP(getInputForVerifyOTP()).observe(this, new Observer<AuthenticationResponse>() {
            @Override
            public void onChanged(@Nullable AuthenticationResponse flagCheckResponseModel) {
                postLoad();

                if (flagCheckResponseModel != null) {
                    if (flagCheckResponseModel.getError()) {
                        revertCircularAnimation();
                        Utils.displayError(getActivity().getWindow().findViewById(android.R.id.content), flagCheckResponseModel.getMsg());
                        enableProceedButton();
                    } else {
                        countDownTimer.cancel();
                        stopAnimation(flagCheckResponseModel);

                    }
                }
            }
        });

    }

    private void verifyOTP() throws JSONException {
        otpViewModel.verifyOTP(getInputForVerifyOTP()).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(@Nullable FlagCheckResponseModel flagCheckResponseModel) {
                postLoad();
                if (flagCheckResponseModel != null) {
                    if (flagCheckResponseModel.isError()) {
                        Utils.displayError(getActivity().getWindow().findViewById(android.R.id.content), flagCheckResponseModel.getMsg());
                        enableProceedButton();
                    } else {
                        countDownTimer.cancel();
                        sharedHelper.setOTP(getOtp());
                        clearOTP();
                        if (otpType.equalsIgnoreCase(Constants.OtpType.REGISTER)) {
                            callback.onSuccess(Constants.OtpType.REGISTER);
                        } else if (otpType.equalsIgnoreCase(Constants.OtpType.RESET_PASSWORD)) {
                            callback.onSuccess(Constants.OtpType.RESET_PASSWORD);
                        } else {
                            callback.onSuccess(Constants.OtpType.CHANGE_MOBILE_NUMBER);
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                enableProceedButton();
                            }
                        }, 500);

                    }
                }
            }
        });
    }

    private void clearOTP() {
        binding.otpView.setText("");
    }


    public void preload() {
        binding.loadingLayout.setVisibility(View.VISIBLE);

        if (otpType.equalsIgnoreCase(Constants.OtpType.LOGIN)) {
            startCircularAnimation();
        } else {
            startLineProgress();
        }
    }

    public void startLineProgress() {
        progressbar.start();
    }

    private void startCircularAnimation() {
        binding.nextButton.startAnimation();
    }


    private void revertCircularAnimation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.nextButton.revertAnimation(new Function0<Unit>() {
                    @Override
                    public Unit invoke() {
                        return null;
                    }
                });
            }
        }, 2000);
    }


    private void doneCircularAnimation() {
        binding.nextButton.doneLoadingAnimation(ContextCompat.getColor(getActivity(), android.R.color.black),
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_done_white_48dp));

    }


    public void stopAnimation(final AuthenticationResponse flagCheckResponseModel) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doneCircularAnimation();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        postLoad();
                        if (flagCheckResponseModel != null) {
                            if (flagCheckResponseModel.getError()) {
                                Utils.displayError(getActivity().getWindow().findViewById(android.R.id.content), flagCheckResponseModel.getMsg());
                                revertCircularAnimation();
                                enableProceedButton();
                                clearOTP();
                            } else {
                                handleLoginSuccess(flagCheckResponseModel);
                            }
                        }


                    }
                }, 200);
            }
        }, 200);


    }


    private void handleLoginSuccess(AuthenticationResponse authenticationResponse) {
        commonViewModel.setUserData(authenticationResponse);
        AnimationHelper.presentActivity(getActivity(), binding.nextButton, MainActivity.class);
        revertCircularAnimation();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                enableProceedButton();
            }
        }, 500);

    }


    private void stopLineProgress() {
        progressbar.stop();
    }

    private void postLoad() {
        binding.loadingLayout.setVisibility(View.GONE);

        if (!otpType.equalsIgnoreCase(Constants.OtpType.LOGIN)) {
            stopLineProgress();
        }


    }


    private InputForAPI getInputForVerifyOTP() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.MOBILE, commonViewModel.getUserMobile());
        jsonObject.put(Constants.ApiKeys.COUNTRY_CODE, commonViewModel.getUserCountryCode());
        jsonObject.put(Constants.ApiKeys.OTP, getOtp());
        jsonObject.put(Constants.ApiKeys.TYPE, otpType);
        jsonObject.put(Constants.ApiKeys.UUID, Utils.getDeviceId(getActivity()));
        jsonObject.put(Constants.ApiKeys.LANGUAGE_NAME, sharedHelper.getSelectedLanguage());
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.VERIFY_OTP);
        inputForAPI.setJsonObject(jsonObject);
        return inputForAPI;
    }

    private String getOtp() {
        return binding.otpView.getText().toString();
    }

    private void resendOTP() throws JSONException {
        otpViewModel.resendOTP(getInputforResendOTP()).observe(this, new Observer<ResendOTPResponse>() {
            @Override
            public void onChanged(@Nullable ResendOTPResponse resendOTPResponse) {
                postLoad();
                enableResendButton();
                revertCircularAnimation();
                if (resendOTPResponse != null) {
                    if (resendOTPResponse.isError()) {
                        Utils.displayError(getActivity().getWindow().findViewById(android.R.id.content), resendOTPResponse.getMsg());
                    } else {
                        Utils.displayError(getActivity().getWindow().findViewById(android.R.id.content), resendOTPResponse.getMsg());
                        startTimer();
                    }
                }
            }
        });
    }


    private InputForAPI getInputforResendOTP() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.MOBILE, commonViewModel.getUserMobile());
        jsonObject.put(Constants.ApiKeys.COUNTRY_CODE, commonViewModel.getUserCountryCode());
        jsonObject.put(Constants.ApiKeys.TYPE, otpType);
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.RESEND_OTP);
        inputForAPI.setJsonObject(jsonObject);
        return inputForAPI;
    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(otpViewModel.getOTPtimer(), 1000) {

            public void onTick(long millisUntilFinished) {
                binding.resendOTP.setText(Utils.convertSecondsToMinutes(millisUntilFinished / 1000));
            }

            public void onFinish() {
                binding.resendOTP.setText(resendText);
            }

        };
        countDownTimer.start();
        enableResendButton();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            countDownTimer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOtpCompleted(String otp) {

    }


}
