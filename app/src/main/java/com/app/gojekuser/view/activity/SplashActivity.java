package com.app.gojekuser.view.activity;

import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import com.app.gojekuser.MainActivity;
import com.app.gojekuser.R;
import com.app.gojekuser.helper.UrlHelper;
import com.app.gojekuser.networkcall.apicall.NoInternetDialog;
import com.app.gojekuser.view.viewmodel.SplashViewModel;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AppConfiguration;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.view.activity.BaseActivity;
import com.facebook.FacebookSdk;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;

import java.util.Locale;

public class SplashActivity extends BaseActivity {
    SplashViewModel viewModel;
    SharedHelper sharedHelper;
    private NoInternetDialog noInternetDialog;
    private String TAG = SplashActivity.class.getSimpleName();

    AppUpdateInfo obtainedInfo;
    AppUpdateManager appUpdateManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
        FacebookSdk.sdkInitialize(getApplicationContext());
        sharedHelper = new SharedHelper(SplashActivity.this);
        setPhoneLanguage();
        viewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
//        checkForUpdateAvailability();
        getAppConfiguration();
    }


    private void checkForUpdateAvailability() {

        appUpdateManager = AppUpdateManagerFactory.create(SplashActivity.this);

// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                    obtainedInfo = appUpdateInfo;
                    try {
                        updateApp(appUpdateInfo, appUpdateManager);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                } else {
                    getAppConfiguration();
                }
            }
        });
    }


    private void updateApp(AppUpdateInfo appUpdateInfo, AppUpdateManager appUpdateManager) throws IntentSender.SendIntentException {
        appUpdateManager.startUpdateFlowForResult(
                // Pass the intent that is returned by 'getAppUpdateInfo()'.
                appUpdateInfo,
                // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                AppUpdateType.IMMEDIATE,
                // The current activity making the update request.
                this,
                // Include a request code to later monitor this update request.
                Constants.IntentPermissionCode.UPDATE_CHECK);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.IntentPermissionCode.UPDATE_CHECK) {
            if (resultCode != RESULT_OK) {
                try {
                    updateApp(obtainedInfo, appUpdateManager);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            } else {
                getAppConfiguration();
            }
        }
    }



    private void setPhoneLanguage() {

        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        if (sharedHelper.getSelectedLanguage().length() > 0) {
            conf.setLocale(new Locale(sharedHelper.getSelectedLanguage().toLowerCase()));
        } else {
            conf.setLocale(new Locale("en"));
        }
        res.updateConfiguration(conf, dm);
    }


    private void getAppConfiguration() {

        viewModel.getAppConfiguration(getInputs()).observe(this, new Observer<AppConfiguration>() {
            @Override
            public void onChanged(@Nullable AppConfiguration appConfiguration) {
                if (appConfiguration != null && !appConfiguration.isError()) {
                    sharedHelper.setAppConfiguration(appConfiguration);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onSplashCompleted();
                        }
                    }, 200);
                } else {
                    if (appConfiguration != null) {
                        onFailureResponse(appConfiguration.getMsg());
                    }
                }
            }
        });
    }


    private void onFailureResponse(String msg) {

        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }

    }

    private void displayError(String error_message) {
        Utils.displayError(findViewById(android.R.id.content), error_message);
    }

    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(SplashActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                getAppConfiguration();
            }
        });
    }

    public void onSplashCompleted() {
        if (viewModel.getIsLoggedIn()) {
            moveMainActivity();
        } else {
            moveSignInActivity();
        }

    }

    private void moveMainActivity() {
        Utils.log(TAG, "token:" + sharedHelper.getToken());
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private InputForAPI getInputs() {
        InputForAPI inputForAPI = new InputForAPI(SplashActivity.this);
        inputForAPI.setUrl(UrlHelper.GET_APP_CONFIGURATION);
        return inputForAPI;

    }

    private void moveSignInActivity() {
        Intent intent = new Intent(SplashActivity.this, SignInActivity.class);
        if (sharedHelper.getTutorialDone().equalsIgnoreCase("true")) {
            intent.putExtra(Constants.IntentKeys.SKIP, true);
        } else {
            intent.putExtra(Constants.IntentKeys.SKIP, false);
        }

        startActivity(intent);
    }

}
