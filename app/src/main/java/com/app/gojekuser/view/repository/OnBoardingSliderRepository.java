package com.app.gojekuser.view.repository;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONObject;

public class OnBoardingSliderRepository {

    private String TAG = OnBoardingSliderRepository.class.getSimpleName();

    public LiveData<JSONObject> getDataForFacebook(LoginResult loginResult) {

        final MutableLiveData<JSONObject> liveData = new MutableLiveData<>();
        Utils.log(TAG,"token:"+loginResult.getAccessToken());
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        liveData.setValue(object);

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender,age_range,locale");
        request.setParameters(parameters);
        request.executeAsync();
        return liveData;
    }


    public LiveData<AuthenticationResponse> checkSocialTokenExistence(InputForAPI inputForAPI) {

        final MutableLiveData<AuthenticationResponse> mutableLiveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                AuthenticationResponse checkResponse = gson.fromJson(response.toString(), AuthenticationResponse.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                AuthenticationResponse checkResponse = new AuthenticationResponse();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }
}
