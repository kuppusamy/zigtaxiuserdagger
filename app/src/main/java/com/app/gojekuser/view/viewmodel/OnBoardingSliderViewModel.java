package com.app.gojekuser.view.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.OnBoardingSliderRepository;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

public class OnBoardingSliderViewModel extends AndroidViewModel {
    OnBoardingSliderRepository repository;
    public OnBoardingSliderViewModel(@NonNull Application application) {
        super(application);
        repository=new OnBoardingSliderRepository();
    }

    public LiveData<JSONObject> getDataForFacebook(LoginResult loginResult) {
        return  repository.getDataForFacebook(loginResult);
    }

    public LiveData<AuthenticationResponse> checkSocialTokenExistence(InputForAPI inputForAPI) {
        return repository.checkSocialTokenExistence(inputForAPI);
    }
}
