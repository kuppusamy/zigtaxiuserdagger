package com.app.gojekuser.view.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.gojekuser.view.repository.OTPRepository;
import com.app.gojekuser.view.repository.RegisterRepository;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;


public class RegisterViewModel extends AndroidViewModel {
    private OTPRepository otpRepository;
    private RegisterRepository registerRepository;

    public RegisterViewModel(@NonNull Application application) {
        super(application);
        otpRepository = new OTPRepository(application.getApplicationContext());
        registerRepository = new RegisterRepository(application.getApplicationContext());
    }


    public String getConfigurationType() {
        return registerRepository.getConfigurationType();
    }


    public LiveData<AuthenticationResponse> registerUser(InputForAPI inputForAPI) {
        return registerRepository.registerUser(inputForAPI);
    }



}
