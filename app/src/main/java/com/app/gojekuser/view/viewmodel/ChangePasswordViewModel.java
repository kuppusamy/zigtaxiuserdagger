package com.app.gojekuser.view.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.gojekuser.view.repository.ChangePasswordRepository;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;

public class ChangePasswordViewModel extends AndroidViewModel {
    ChangePasswordRepository changePasswordRepository;

    public ChangePasswordViewModel(@NonNull Application application) {
        super(application);
        changePasswordRepository = new ChangePasswordRepository();
    }

    public LiveData<AuthenticationResponse> changePassword(InputForAPI inputs) {
        return changePasswordRepository.changePassword(inputs);
    }


}
