package com.app.gojekuser.view.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.gojekuser.R;
import com.app.gojekuser.databinding.ActivityPhoneNumberBinding;
import com.app.gojekuser.networkcall.apicall.InputForAPI;
import com.app.gojekuser.view.fragment.ChangePasswordFragment;
import com.app.gojekuser.view.fragment.OTPFragment;
import com.app.gojekuser.view.fragment.PasswordFragment;
import com.app.gojekuser.view.fragment.RegisterFragment;
import com.app.gojekuser.view.viewmodel.CommonViewModel;
import com.app.gojekuser.view.viewmodel.PhoneNumberViewModel;
import com.app.taxiapp.helpers.AnimationHelper;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.Callback;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.MobileNumberCheckResponse;
import com.app.taxiapp.model.apiresponsemodel.SocialLoginResponseModel;

import com.app.taxiapp.view.activity.BaseActivity;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;


public class PhoneNumberActivity extends BaseActivity {
    ActivityPhoneNumberBinding binding;
    CountryCodePicker countryCodePicker;
    TextView progressButton;
    CommonViewModel commonViewModel;
    PhoneNumberViewModel viewModel;
    SharedHelper sharedHelper;
    AnimationHelper.LoaderZigZagMain progressbar;
    String loginType = Constants.LoginType.NORMAL;
    SocialLoginResponseModel socialLoginResponseModel;


    public void enableProceedButton() {
        progressButton.setEnabled(true);
    }

    public void disableProceedButton() {
        progressButton.setEnabled(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.showStatusBar(PhoneNumberActivity.this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_phone_number);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        viewModel = ViewModelProviders.of(this).get(PhoneNumberViewModel.class);
        sharedHelper = new SharedHelper(PhoneNumberActivity.this);
        try {
            loginType = getIntent().getStringExtra(Constants.IntentKeys.LOGIN_TYPE);
            socialLoginResponseModel = getIntent().getParcelableExtra(Constants.IntentKeys.DATA);

        } catch (Exception e) {
            e.printStackTrace();
        }
        progressbar = new AnimationHelper.LoaderZigZagMain(binding.progressBarLay.progressBar, binding.progressBarLay.loadingParent);
        progressButton = binding.nextButton;
        countryCodePicker = binding.countryCodePicker;
        Locale locale = new Locale(sharedHelper.getSelectedLanguage());

        try {

            if (locale.getLanguage().equalsIgnoreCase("en")) {
                countryCodePicker.changeDefaultLanguage(CountryCodePicker.Language.valueOf("ENGLISH"));
            } else {
                countryCodePicker.changeDefaultLanguage(CountryCodePicker.Language.valueOf(locale.getDisplayLanguage(Locale.ENGLISH).toUpperCase()));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            countryCodePicker.changeDefaultLanguage(CountryCodePicker.Language.valueOf("ENGLISH"));

        }



        initListeners();

    }

    public void preload() {
        binding.loadingLayout.setVisibility(View.VISIBLE);
        progressbar.start();

    }

    public void postLoad() {
        binding.loadingLayout.setVisibility(View.GONE);
        progressbar.stop();


    }


    private void initListeners() {
        progressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isValidMobile(binding.phoneNumber.getText().toString())) {
                    disableProceedButton();
                    preload();
                    try {
                        buildPhoneNumberCheckInputs();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    binding.phoneNumber.setError(getString(R.string.valid_phone_number));
                }
            }
        });

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        progressButton.setVisibility(View.VISIBLE);
        boolean isCloseFlag = false;
        FragmentManager fragmentManager = getSupportFragmentManager();
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
            String stack_name = fragmentManager.getBackStackEntryAt(i).getName();
            if (stack_name.equalsIgnoreCase(RegisterFragment.class.getSimpleName()) || stack_name.equalsIgnoreCase(ChangePasswordFragment.class.getSimpleName())) {
                isCloseFlag = true;
            }
        }

        if (isCloseFlag) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            super.onBackPressed();
        }
    }


    private void buildPhoneNumberCheckInputs() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.MOBILE, binding.phoneNumber.getText().toString());
        jsonObject.put(Constants.ApiKeys.COUNTRY_CODE, countryCodePicker.getSelectedCountryCodeWithPlus());
        InputForAPI inputForAPI = new InputForAPI(PhoneNumberActivity.this);


        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setUrl(UrlHelper.CHECK_MOBILE_EXISTENCE);
        viewModel.checkMobileNumber(inputForAPI).observe(this, new Observer<MobileNumberCheckResponse>() {
            @Override
            public void onChanged(@Nullable final MobileNumberCheckResponse mobileNumberCheckResponse) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        postLoad();
                        if (mobileNumberCheckResponse != null) {
                            handlecheckResponse(mobileNumberCheckResponse);
                        }
                    }
                }, 1000);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void handlecheckResponse(MobileNumberCheckResponse mobileNumberCheckResponse) {
        sharedHelper.setMobile(binding.phoneNumber.getText().toString());
        sharedHelper.setCountryCode(binding.countryCodePicker.getSelectedCountryCodeWithPlus());
        sharedHelper.setCountryNameCode(binding.countryCodePicker.getSelectedCountryNameCode());
        try {
            Utils.dismissKeyboard(PhoneNumberActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (loginType.equalsIgnoreCase(Constants.LoginType.NORMAL)) {
            if (mobileNumberCheckResponse.isError()) {
                placeOTP(Constants.OtpType.REGISTER);
            } else {
                if (viewModel.getConfigurationType().equalsIgnoreCase(Constants.AuthType.PASSWORD)) {
                    placePassword();
                } else {
                    placeOTP(Constants.OtpType.LOGIN);
                }
            }
        } else {
            if (mobileNumberCheckResponse.isError()) {
                placeOTP(Constants.OtpType.REGISTER);
            } else {
                enableProceedButton();
                Utils.displayError(findViewById(android.R.id.content), mobileNumberCheckResponse.getMsg());
            }
        }
    }

    private void placeOTP(String register) {
        OTPFragment otpFragment = new OTPFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.ArgumentKeys.TYPE, register);
        bundle.putString(Constants.ArgumentKeys.LOGIN_TYPE, loginType);
        bundle.putParcelable(Constants.ArgumentKeys.DATA, socialLoginResponseModel);
        otpFragment.setArguments(bundle);
        otpFragment.replaceListener(new Callback() {
            @Override
            public void onSuccess(String type) {
                if (type.equalsIgnoreCase(Constants.OtpType.REGISTER)) {
                    placeRegister();
                } else {
                    placechangePassword();
                }
            }
        });
        Utils.navigateToFragment(R.id.fragmentContainer, otpFragment, getSupportFragmentManager());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                enableProceedButton();
            }
        }, 500);


    }

    private void placechangePassword() {
        ChangePasswordFragment passwordFragment = new ChangePasswordFragment();
        Utils.navigateToFragment(R.id.fragmentContainer, passwordFragment, getSupportFragmentManager());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                enableProceedButton();
            }
        }, 500);

    }


    private void placeRegister() {
        RegisterFragment registerFragment = new RegisterFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.ArgumentKeys.LOGIN_TYPE, loginType);
        bundle.putParcelable(Constants.ArgumentKeys.DATA, socialLoginResponseModel);

        registerFragment.setArguments(bundle);
        Utils.navigateToFragment(R.id.fragmentContainer, registerFragment, getSupportFragmentManager());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                enableProceedButton();
            }
        }, 500);


    }

    private void placePassword() {
        PasswordFragment passwordFragment = new PasswordFragment();
        passwordFragment.replaceListener(new Callback() {
            @Override
            public void onSuccess(String type) {
                placeOTP(Constants.OtpType.RESET_PASSWORD);
            }
        });
        Utils.navigateToFragment(R.id.fragmentContainer, passwordFragment, getSupportFragmentManager());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                enableProceedButton();
            }
        }, 500);


    }

}
