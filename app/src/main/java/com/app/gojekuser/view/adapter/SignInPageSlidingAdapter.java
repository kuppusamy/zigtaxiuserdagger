package com.app.gojekuser.view.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.gojekuser.view.fragment.SignInPageSlidingFragment;
import com.app.taxiapp.model.apiresponsemodel.AppConfiguration;

import java.util.List;

public class SignInPageSlidingAdapter extends FragmentStatePagerAdapter {
    private List<AppConfiguration.Slider> welcomePageModels;
    private boolean booleanExtra;


    public SignInPageSlidingAdapter(FragmentManager fm, List<AppConfiguration.Slider> welcomePageModels, boolean booleanExtra) {
        super(fm);
        this.welcomePageModels = welcomePageModels;
        this.booleanExtra = booleanExtra;

    }



    @Override
    public Fragment getItem(int position) {
        AppConfiguration.Slider pageModel = welcomePageModels.get(position);
        Fragment fragment = SignInPageSlidingFragment.newInstance(pageModel, position, welcomePageModels.size(),booleanExtra);
        return fragment;
    }

    @Override
    public int getCount() {
        return welcomePageModels.size();
    }
}

