package com.app.gojekuser

import android.content.Context
import android.text.TextUtils
import androidx.multidex.MultiDex
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.app.gojekuser.di.DaggerApplicationComponent
import com.app.taxiapp.helpers.rxbus.RxBus
import dagger.android.support.DaggerApplication

class ApplicationClass : DaggerApplication() {

    private val applicationInjector = DaggerApplicationComponent.builder()
        .application(this)
        .build()

    override fun applicationInjector() = applicationInjector


    private var mRequestQueue: RequestQueue? = null
    var context1: Context? = null
    val TAG = ApplicationClass::class.java.simpleName
    var rxBus1: RxBus? = null


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {
        private var mInstance: ApplicationClass? = null
        @Synchronized
        fun getInstance(): ApplicationClass? {
            return mInstance
        }
    }

    fun getRequestQueue(): RequestQueue? {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(applicationContext)
        }
        return mRequestQueue
    }


    fun <T> addToRequestQueue(
        req: Request<T>,
        tag: String?
    ) { // set the default tag if tag is empty
        req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        getRequestQueue()!!.add(req)
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        req.tag = TAG
        getRequestQueue()!!.add(req)
    }

    fun cancelPendingRequests(tag: Any?) {
        if (mRequestQueue != null) {
            mRequestQueue!!.cancelAll(tag)
        }
    }

    fun getContext(): Context? {
        return context1
    }

    override fun onCreate() {
        super.onCreate()

        context1 = applicationContext
        mInstance = this
        rxBus1 = RxBus()
    }


    fun getRxBus(): RxBus? {
        return rxBus1
    }

}