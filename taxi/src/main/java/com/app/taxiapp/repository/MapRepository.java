package com.app.taxiapp.repository;


import com.app.taxiapp.AppController;
import com.app.taxiapp.R;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.AutoCompleteAddressResponseModel;
import com.app.taxiapp.model.apiresponsemodel.GetLatLongFromIdResponseModel;
import com.app.taxiapp.model.apiresponsemodel.GoogleLocationResponseModel.GooglePlaceResponseModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class MapRepository {


    public LiveData<GooglePlaceResponseModel> getAddresDetails(InputForAPI inputForAPI) {
        final MutableLiveData<GooglePlaceResponseModel> liveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {


            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GooglePlaceResponseModel flagResponseModel = gson.fromJson(response.toString(), GooglePlaceResponseModel.class);
                liveData.setValue(flagResponseModel);
            }

            @Override
            public void setResponseError(String error) {

                GooglePlaceResponseModel flagResponseModel = new GooglePlaceResponseModel();
                flagResponseModel.setStatus("false");
                liveData.setValue(flagResponseModel);

            }
        });
        return liveData;
    }

    public LiveData<AutoCompleteAddressResponseModel> autoCompleteAddress(InputForAPI inputForAPI) {
        final MutableLiveData<AutoCompleteAddressResponseModel> liveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {

            @Override
            public void setDataResponse(JSONObject response) {
                if (response.optString(Constants.ApiKeys.STATUS).equalsIgnoreCase("OK")) {
                    Gson gson = new Gson();
                    AutoCompleteAddressResponseModel flagResponseModel = gson.fromJson(response.toString(), AutoCompleteAddressResponseModel.class);
                    flagResponseModel.setError(false);
                    liveData.setValue(flagResponseModel);
                } else {
                    AutoCompleteAddressResponseModel flagResponseModel = new AutoCompleteAddressResponseModel();
                    flagResponseModel.setError(true);
                    flagResponseModel.setStatus(AppController.getContext().getResources().getString(R.string.enter_valid_address));
                    liveData.setValue(flagResponseModel);

                }
            }

            @Override
            public void setResponseError(String error) {

                AutoCompleteAddressResponseModel flagResponseModel = new AutoCompleteAddressResponseModel();
                flagResponseModel.setError(true);
                flagResponseModel.setStatus(AppController.getContext().getResources().getString(R.string.enter_valid_address));
                liveData.setValue(flagResponseModel);

            }
        });
        return liveData;
    }

    public LiveData<GetLatLongFromIdResponseModel> getLatLagAddress(InputForAPI inputForAPI) {
        final MutableLiveData<GetLatLongFromIdResponseModel> liveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {


            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GetLatLongFromIdResponseModel flagResponseModel = gson.fromJson(response.toString(), GetLatLongFromIdResponseModel.class);
                liveData.setValue(flagResponseModel);
            }

            @Override
            public void setResponseError(String error) {

                GetLatLongFromIdResponseModel flagResponseModel = new GetLatLongFromIdResponseModel();
                flagResponseModel.setStatus(error);
                liveData.setValue(flagResponseModel);

            }
        });
        return liveData;
    }


}
