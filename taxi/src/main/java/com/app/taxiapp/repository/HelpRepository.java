package com.app.taxiapp.repository;

import android.content.Context;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.HelpDetailModel;
import com.app.taxiapp.model.apiresponsemodel.HelpModel;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.model.apiresponsemodel.HelpModel;
import com.app.taxiapp.model.apiresponsemodel.ResendOTPResponse;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class HelpRepository {
private SharedHelper sharedHelper;

    public HelpRepository(Context context) {
        sharedHelper=new SharedHelper(context);
    }

    public LiveData<HelpModel> getHelpData(InputForAPI inputForAPI) {
        final MutableLiveData<HelpModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                HelpModel checkResponse = gson.fromJson(response.toString(), HelpModel.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                HelpModel checkResponse = new HelpModel();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }


    public LiveData<HelpDetailModel> getHelpDetail(InputForAPI inputForAPI) {
        final MutableLiveData<HelpDetailModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                HelpDetailModel checkResponse = gson.fromJson(response.toString(), HelpDetailModel.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                HelpDetailModel checkResponse = new HelpDetailModel();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }

}
