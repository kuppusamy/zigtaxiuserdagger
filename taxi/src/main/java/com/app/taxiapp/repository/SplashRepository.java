package com.app.taxiapp.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.content.Context;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AppConfiguration;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class SplashRepository {

private SharedHelper sharedHelper;
    public SplashRepository(Context applicationContext) {
        sharedHelper=new SharedHelper(applicationContext);
    }

    public LiveData<AppConfiguration> getAppConfiguration(InputForAPI inputForAPI) {
        final MutableLiveData<AppConfiguration> mutableLiveData = new MutableLiveData<>();
        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                AppConfiguration checkResponse = gson.fromJson(response.toString(), AppConfiguration.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                AppConfiguration checkResponse = new AppConfiguration();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }
    public boolean getIsLoggedIn()
    {
        return sharedHelper.getIsUserLoggedIn();
    }

}
