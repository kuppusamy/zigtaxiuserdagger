package com.app.taxiapp.repository;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.TripDetailsModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class TripDetailRepository {


    public LiveData<TripDetailsModel> getTripDetails(InputForAPI inputForAPI) {
        final MutableLiveData<TripDetailsModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                TripDetailsModel checkResponse = gson.fromJson(response.toString(), TripDetailsModel.class);
                mutableLiveData.setValue(checkResponse);
            }

            @Override
            public void setResponseError(String error) {
                TripDetailsModel checkResponse = new TripDetailsModel();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);
            }
        });
        return mutableLiveData;
    }

}
