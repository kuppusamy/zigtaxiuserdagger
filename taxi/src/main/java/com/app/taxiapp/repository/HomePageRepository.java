package com.app.taxiapp.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.taxiapp.R;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.RouteGenerate.Route;
import com.app.taxiapp.helpers.RouteGenerate.RouteException;
import com.app.taxiapp.helpers.RouteGenerate.Routing;
import com.app.taxiapp.helpers.RouteGenerate.RoutingListener;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.CancelReasonsModel;
import com.app.taxiapp.model.apiresponsemodel.CouponVerifyResponseModel;
import com.app.taxiapp.model.apiresponsemodel.CurrentBookingResponseModel;
import com.app.taxiapp.model.apiresponsemodel.GoogleLocationResponseModel.GooglePlaceResponseModel;
import com.app.taxiapp.model.apiresponsemodel.RideTypeResponseModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.List;

public class HomePageRepository {
    SharedHelper sharedHelper;
    Context context;

    public HomePageRepository(Context context) {

        sharedHelper = new SharedHelper(context);
        this.context = context;
    }


    public LiveData<CancelReasonsModel> getCancelReasons(InputForAPI inputForAPI) {
        final MutableLiveData<CancelReasonsModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CancelReasonsModel checkResponse = gson.fromJson(response.toString(), CancelReasonsModel.class);
                mutableLiveData.setValue(checkResponse);
            }

            @Override
            public void setResponseError(String error) {
                CancelReasonsModel checkResponse = new CancelReasonsModel();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);
            }
        });
        return mutableLiveData;
    }


    public LiveData<Route> getRouteDetails(LatLng srcLatLng, LatLng desLatLng) {
        final MutableLiveData<Route> liveData = new MutableLiveData<>();

        Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.DRIVING)
                .language(sharedHelper.getSelectedLanguage())
                .key(context.getResources().getString(R.string.GOOGLE_MAPS_KEY))
                .withListener(new RoutingListener() {
                    @Override
                    public void onRoutingFailure(RouteException e) {

                    }

                    @Override
                    public void onRoutingStart() {

                    }

                    @Override
                    public void onRoutingSuccess(List<Route> route, int shortestRouteIndex) {
                        liveData.setValue(route.get(shortestRouteIndex));

                    }

                    @Override
                    public void onRoutingCancelled() {

                    }
                })
                .waypoints(srcLatLng, desLatLng)
                .build();
        routing.execute();
        return liveData;
    }


    public LiveData<GooglePlaceResponseModel> getAddresDetails(InputForAPI inputForAPI) {
        final MutableLiveData<GooglePlaceResponseModel> liveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {

            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GooglePlaceResponseModel flagResponseModel = gson.fromJson(response.toString(), GooglePlaceResponseModel.class);
                liveData.setValue(flagResponseModel);
            }

            @Override
            public void setResponseError(String error) {

                GooglePlaceResponseModel flagResponseModel = new GooglePlaceResponseModel();
                flagResponseModel.setStatus("false");
                liveData.setValue(flagResponseModel);

            }
        });
        return liveData;
    }

    public LiveData<CouponVerifyResponseModel> verifyCoupon(InputForAPI inputForAPI) {
        final MutableLiveData<CouponVerifyResponseModel> liveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {

            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CouponVerifyResponseModel flagResponseModel = gson.fromJson(response.toString(), CouponVerifyResponseModel.class);
                liveData.setValue(flagResponseModel);
            }

            @Override
            public void setResponseError(String error) {

                CouponVerifyResponseModel flagResponseModel = new CouponVerifyResponseModel();
                flagResponseModel.setError(true);
                flagResponseModel.setMsg(error);
                liveData.setValue(flagResponseModel);

            }
        });
        return liveData;
    }


    public LiveData<CurrentBookingResponseModel> getCurrentBooking(InputForAPI inputForAPI) {
        final MutableLiveData<CurrentBookingResponseModel> liveData = new MutableLiveData<>();
        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {

            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new GsonBuilder().serializeNulls().create();
                CurrentBookingResponseModel currentBookingResponseModel = gson.fromJson(response.toString(), CurrentBookingResponseModel.class);
                liveData.setValue(currentBookingResponseModel);
            }

            @Override
            public void setResponseError(String error) {

                CurrentBookingResponseModel currentBookingResponseModel = new CurrentBookingResponseModel();
                currentBookingResponseModel.setError(true);
                currentBookingResponseModel.setMsg(error);
                liveData.setValue(currentBookingResponseModel);

            }
        });

        return liveData;
    }


    public LiveData<RideTypeResponseModel> getAvailableRideType(InputForAPI inputForAPI) {
        final MutableLiveData<RideTypeResponseModel> liveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {

            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                RideTypeResponseModel rideTypeResponseModel = gson.fromJson(response.toString(), RideTypeResponseModel.class);
                liveData.setValue(rideTypeResponseModel);
            }

            @Override
            public void setResponseError(String error) {

                RideTypeResponseModel rideTypeResponseModel = new RideTypeResponseModel();
                rideTypeResponseModel.setError(true);
                rideTypeResponseModel.setMsg(error);
                liveData.setValue(rideTypeResponseModel);

            }
        });

        return liveData;
    }


}
