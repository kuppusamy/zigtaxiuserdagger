package com.app.taxiapp.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.content.Context;


import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class CommonRepository {
    private SharedHelper sharedHelper;
    private Context context;

    public CommonRepository(Context context) {
        this.context = context;
        sharedHelper = new SharedHelper(context);
    }

    public String getUserMobile() {
        return sharedHelper.getMobile();
    }
    public String getFirstName() {
        return sharedHelper.getFirstName();
    }

    public String getLastName() {
        return sharedHelper.getLastName();
    }

    public String getRating() {
        return sharedHelper.getUserRating();
    }
    public String getUserImage() {
        return sharedHelper.getImage();
    }


    public AuthenticationResponse.Data getUserDetails() {
        AuthenticationResponse.Data data=new AuthenticationResponse.Data();
        data.setFirstname(getFirstName());
        data.setLastname(getLastName());
        data.setImage(getUserImage());
        data.setMobile(getUserMobile());
        return data;
    }

    public String getNotificationToken() {
        return sharedHelper.getNotificationToken();
    }

    public String getUserCountryCode() {
        return sharedHelper.getCountryCode();
    }
    public String getCountryNameCode() {
        return sharedHelper.getCountryNameCode();
    }


    public String getOTP() {
        return sharedHelper.getOTP();
    }
    public String getAccessToken() {
        return sharedHelper.getToken();
    }



    public LiveData<FlagCheckResponseModel> flagCheckResponse(InputForAPI inputForAPI) {
        final MutableLiveData<FlagCheckResponseModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                FlagCheckResponseModel checkResponse = gson.fromJson(response.toString(), FlagCheckResponseModel.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                FlagCheckResponseModel checkResponse = new FlagCheckResponseModel();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }

    public String getCurrentBookingID() {
        return sharedHelper.getBookingNumber();
    }
}
