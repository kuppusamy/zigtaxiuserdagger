package com.app.taxiapp.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.HelpDetailModel;
import com.app.taxiapp.model.apiresponsemodel.HelpModel;
import com.app.taxiapp.model.apiresponsemodel.SavedCardListResponseModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ChoosePaymentRepository {
    private SharedHelper sharedHelper;

    public ChoosePaymentRepository(Context context) {
        sharedHelper = new SharedHelper(context);
    }

    public LiveData<SavedCardListResponseModel> getCardLists(InputForAPI inputForAPI) {
        final MutableLiveData<SavedCardListResponseModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                SavedCardListResponseModel checkResponse = gson.fromJson(response.toString(), SavedCardListResponseModel.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                SavedCardListResponseModel checkResponse = new SavedCardListResponseModel();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }

}
