package com.app.taxiapp.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ChangePasswordRepository {


    public LiveData<AuthenticationResponse> changePassword(InputForAPI inputForAPI) {
        final MutableLiveData<AuthenticationResponse> mutableLiveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                AuthenticationResponse checkResponse = gson.fromJson(response.toString(), AuthenticationResponse.class);
                mutableLiveData.setValue(checkResponse);
            }

            @Override
            public void setResponseError(String error) {
                AuthenticationResponse checkResponse = new AuthenticationResponse();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);
            }
        });
        return mutableLiveData;
    }

}
