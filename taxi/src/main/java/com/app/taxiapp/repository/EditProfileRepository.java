package com.app.taxiapp.repository;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.ImageUploadResponseModel;
import com.app.taxiapp.model.apiresponsemodel.ProfileResponseModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class EditProfileRepository {


    public LiveData<ProfileResponseModel> getProfileData(InputForAPI inputForAPI) {
        final MutableLiveData<ProfileResponseModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ProfileResponseModel checkResponse = gson.fromJson(response.toString(), ProfileResponseModel.class);
                mutableLiveData.setValue(checkResponse);
            }

            @Override
            public void setResponseError(String error) {
                ProfileResponseModel checkResponse = new ProfileResponseModel();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);
            }
        });
        return mutableLiveData;
    }


    public LiveData<ImageUploadResponseModel> uploadImage(InputForAPI inputForAPI) {
        final MutableLiveData<ImageUploadResponseModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.fileUpload(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ImageUploadResponseModel checkResponse = gson.fromJson(response.toString(), ImageUploadResponseModel.class);
                mutableLiveData.setValue(checkResponse);
            }

            @Override
            public void setResponseError(String error) {
                ImageUploadResponseModel imageUploadResponseModel = new ImageUploadResponseModel();
                imageUploadResponseModel.setError(Constants.ApiKeys.TRUE);
                imageUploadResponseModel.setMsg(error);
                mutableLiveData.setValue(imageUploadResponseModel);
            }
        });
        return mutableLiveData;
    }


}
