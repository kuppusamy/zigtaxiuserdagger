package com.app.taxiapp.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.MobileNumberCheckResponse;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class PhoneNumberRepository {


    public LiveData<MobileNumberCheckResponse> checkMobileNumber(InputForAPI inputForAPI) {
        final MutableLiveData<MobileNumberCheckResponse> mutableLiveData = new MutableLiveData<>();
        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                MobileNumberCheckResponse checkResponse = gson.fromJson(response.toString(), MobileNumberCheckResponse.class);
                mutableLiveData.setValue(checkResponse);
            }

            @Override
            public void setResponseError(String error) {
                MobileNumberCheckResponse checkResponse = new MobileNumberCheckResponse();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }

}
