package com.app.taxiapp.repository;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.YourTripsModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class YourTripRepository {


    public LiveData<YourTripsModel> getTrips(InputForAPI inputForAPI) {
        final MutableLiveData<YourTripsModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                YourTripsModel yourTripsResponse = gson.fromJson(response.toString(), YourTripsModel.class);
                mutableLiveData.setValue(yourTripsResponse);

            }

            @Override
            public void setResponseError(String error) {
                YourTripsModel yourTripsResponse = new YourTripsModel();
                yourTripsResponse.setError(Constants.ApiKeys.TRUE);
                yourTripsResponse.setMsg(error);
                mutableLiveData.setValue(yourTripsResponse);

            }
        });
        return mutableLiveData;
    }

}
