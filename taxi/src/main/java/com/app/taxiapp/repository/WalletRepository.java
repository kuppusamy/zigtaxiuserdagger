package com.app.taxiapp.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.WalletResponseModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class WalletRepository {

private SharedHelper sharedHelper;

    public LiveData<WalletResponseModel> getWalletDetails(InputForAPI inputForAPI) {
        final MutableLiveData<WalletResponseModel> mutableLiveData = new MutableLiveData<>();
        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                WalletResponseModel checkResponse = gson.fromJson(response.toString(), WalletResponseModel.class);
                mutableLiveData.setValue(checkResponse);

            }

            @Override
            public void setResponseError(String error) {
                WalletResponseModel checkResponse = new WalletResponseModel();
                checkResponse.setError(Constants.ApiKeys.TRUE);
                checkResponse.setMsg(error);
                mutableLiveData.setValue(checkResponse);

            }
        });
        return mutableLiveData;
    }

}
