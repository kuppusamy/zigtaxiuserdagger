package com.app.taxiapp.backgroundservices;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;

import com.app.taxiapp.AppController;
import com.app.taxiapp.R;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.view.activity.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

/*
 * Created by user on 25-10-2017.
 */

public class FcmMessageService extends FirebaseMessagingService {

    private static final String TAG = FcmMessageService.class.getSimpleName();


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Utils.log(TAG, "Notification Received");
        if (remoteMessage.getData().size() > 0) {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(remoteMessage.getData());
            Utils.log(TAG, "getData: " + jsonObject);
            sendNotification(jsonObject);
            publishEvent(jsonObject.optString("data"));
        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        sendRegistrationToServer(s);
    }

    private void sendRegistrationToServer(final String token) {
        Utils.log(TAG, "sendRegistrationToServer: " + token);
        SharedHelper appSettings = new SharedHelper(FcmMessageService.this);
        appSettings.setNotificationToken(token);

    }

    private void publishEvent(String data) {
        ((AppController) AppController.getInstance())
                .getRxBus()
                .send(data);
    }

    private void sendNotification(JSONObject jsonObject) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        final String packageName = getPackageName();


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentTitle(jsonObject.optString("title"))
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(FcmMessageService.this, R.color.colorPrimary))
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX)
                .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/raw/notification_tone.mp3"))
                .setContentIntent(pendingIntent);


        if (jsonObject.has("body")) {
            notificationBuilder.setContentText(jsonObject.optString("body"));
        } else {
            notificationBuilder.setContentText(jsonObject.optString("message"));

        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;


                AudioAttributes attributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();

                String NOTIFICATION_CHANNEL_ID = "1";
                String NOTIFICATION_NAME = getResources().getString(R.string.app_name);
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                        NOTIFICATION_NAME, importance);
                notificationChannel.enableLights(true);

                notificationChannel.setLightColor(Color.WHITE);
                notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
                notificationManager.createNotificationChannel(notificationChannel);

            }
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }

}