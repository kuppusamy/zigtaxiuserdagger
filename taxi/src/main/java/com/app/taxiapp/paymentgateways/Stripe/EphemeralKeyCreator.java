package com.app.taxiapp.paymentgateways.Stripe;

import android.content.Context;

import androidx.annotation.NonNull;

import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.stripe.android.EphemeralKeyUpdateListener;
import com.stripe.android.EphemeralKeyProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.app.taxiapp.AppController.context;

public class EphemeralKeyCreator implements EphemeralKeyProvider {

    private ProgressListener mProgressListener;
    private Context context;

    @Override
    public void createEphemeralKey(@NonNull String apiVersion, @NonNull final EphemeralKeyUpdateListener keyUpdateListener) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("stripe_version", apiVersion);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiCall.PostMethod(getInputForApi(jsonObject), new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                if (response.optString("error").equalsIgnoreCase("false")) {
                    String rawKey = response.opt("data").toString();
                    keyUpdateListener.onKeyUpdate(rawKey);
                    mProgressListener.onStringResponse(rawKey);
                } else {
                    mProgressListener.onStringResponse(response.optString("msg"));

                }
            }

            @Override
            public void setResponseError(String error) {
                mProgressListener.onStringResponse(error);
            }
        });

    }

    public EphemeralKeyCreator(ProgressListener mProgressListener, Context context) {
        this.mProgressListener = mProgressListener;
        this.context = context;
    }

    public interface ProgressListener {
        void onStringResponse(String string);
    }


    private InputForAPI getInputForApi(JSONObject jsonObject) {
        InputForAPI inputForAPI = new InputForAPI(context);
        inputForAPI.setUrl(UrlHelper.EPHEMERAL_KEY_GENERATOR);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(ApiCall.getHeaders(context));
        return inputForAPI;
    }
}
