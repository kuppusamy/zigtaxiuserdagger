package com.app.taxiapp.viewmodel;

import android.app.Application;

import com.app.taxiapp.model.apiresponsemodel.HelpDetailModel;
import com.app.taxiapp.model.apiresponsemodel.HelpModel;
import com.app.taxiapp.model.apiresponsemodel.YourTripsModel;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.HelpRepository;
import com.app.taxiapp.repository.YourTripRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class HelpViewModel extends AndroidViewModel {
    private HelpRepository helpRepository;
    private LiveData<YourTripsModel> yourTripLiveData ;

    public HelpViewModel(@NonNull Application application) {
        super(application);
        helpRepository = new HelpRepository(application.getApplicationContext());
    }

    public LiveData<HelpModel> getHelpLists(InputForAPI inputForAPI) {
        return helpRepository.getHelpData(inputForAPI);
    }

    public LiveData<HelpDetailModel> getHelpDetail(InputForAPI inputForAPI) {
        return helpRepository.getHelpDetail(inputForAPI);
    }



}
