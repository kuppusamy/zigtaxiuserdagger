package com.app.taxiapp.viewmodel;

import android.app.Application;

import com.app.taxiapp.AppController;
import com.app.taxiapp.R;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.model.apiresponsemodel.AutoCompleteAddressResponseModel;
import com.app.taxiapp.model.apiresponsemodel.GetLatLongFromIdResponseModel;
import com.app.taxiapp.model.apiresponsemodel.GoogleLocationResponseModel.GooglePlaceResponseModel;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.MapRepository;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class MapViewModel extends AndroidViewModel {

    private MapRepository mapRepository;

    public MapViewModel(@NonNull Application application) {
        super(application);
        mapRepository = new MapRepository();

    }

    public LiveData<GooglePlaceResponseModel> getAddresDetails(InputForAPI inputs) {
        return mapRepository.getAddresDetails(inputs);
    }


    public LiveData<AutoCompleteAddressResponseModel> autoCompleteAddress(InputForAPI inputs) {
        return mapRepository.autoCompleteAddress(inputs);
    }

    public LiveData<GetLatLongFromIdResponseModel> getLatLagAddress(InputForAPI inputs) {
        return mapRepository.getLatLagAddress(inputs);
    }


    public String getAutoCompleteUrl(String input, String currentLat, String currentLong) {
        StringBuilder urlString = new StringBuilder();
        urlString.append(UrlHelper.GOOGLE_API_AUTOCOMPLETE_BASE_URL);
        urlString.append("input=");
        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        urlString.append("&language=en");
        urlString.append("&location="+currentLat+","+currentLong+"&radius=50");
        urlString.append("&key=").append(AppController.getContext().getString(R.string.GOOGLE_MAPS_KEY));
        Utils.log("FINAL URL:::   ", urlString.toString());
        return urlString.toString();
    }

    public String getPlaceDetails(String placeID) {
        return UrlHelper.GOOGLE_API_PLACE_DETAILS_BASE_URL + "placeid=" + placeID + "&key=" + AppController.getContext().getString(R.string.GOOGLE_MAPS_KEY);

    }
}
