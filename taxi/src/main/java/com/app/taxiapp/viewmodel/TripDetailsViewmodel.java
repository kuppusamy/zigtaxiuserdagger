package com.app.taxiapp.viewmodel;

import android.app.Application;

import com.app.taxiapp.model.apiresponsemodel.TripDetailsModel;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.TripDetailRepository;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class TripDetailsViewmodel extends AndroidViewModel {
    TripDetailRepository detailRepository;
    public TripDetailsViewmodel(@NonNull Application application) {
        super(application);
        detailRepository=new TripDetailRepository();
    }


    public LiveData<TripDetailsModel> getTripDetails(InputForAPI inputs) {
        return detailRepository.getTripDetails(inputs);
    }





}
