package com.app.taxiapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import androidx.lifecycle.LiveData;

import com.app.taxiapp.model.apiresponsemodel.WalletResponseModel;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.WalletRepository;

public class WalletViewModel extends AndroidViewModel {
    private WalletRepository walletRepository;

    public WalletViewModel(@NonNull Application application) {
        super(application);
        walletRepository = new WalletRepository();
    }

    public LiveData<WalletResponseModel> getWalletDetails(InputForAPI inputForAPI) {
        return walletRepository.getWalletDetails(inputForAPI);
    }


}
