package com.app.taxiapp.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.taxiapp.model.apiresponsemodel.AppConfiguration;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.SplashRepository;

public class SplashViewModel extends AndroidViewModel {
    private SplashRepository splashRepository;

    public SplashViewModel(@NonNull Application application) {
        super(application);
        splashRepository = new SplashRepository(application.getApplicationContext());
    }

    public LiveData<AppConfiguration> getAppConfiguration(InputForAPI inputForAPI) {
        return splashRepository.getAppConfiguration(inputForAPI);
    }

    public boolean getIsLoggedIn()
    {
        return splashRepository.getIsLoggedIn();
    }

}
