package com.app.taxiapp.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.MobileNumberCheckResponse;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.PhoneNumberRepository;

public class PhoneNumberViewModel extends AndroidViewModel {
    PhoneNumberRepository phoneNumberRepository;
    SharedHelper sharedHelper;

    public PhoneNumberViewModel(@NonNull Application application) {
        super(application);
        phoneNumberRepository = new PhoneNumberRepository();
        sharedHelper = new SharedHelper(application.getApplicationContext());
    }

    public LiveData<MobileNumberCheckResponse> checkMobileNumber(InputForAPI inputForAPI) {
        return phoneNumberRepository.checkMobileNumber(inputForAPI);
    }

    public String getConfigurationType() {
        return sharedHelper.getAppConfiguration().getData().getAuthConfig().getAuth_type();
    }
}
