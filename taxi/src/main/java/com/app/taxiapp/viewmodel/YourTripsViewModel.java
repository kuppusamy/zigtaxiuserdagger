package com.app.taxiapp.viewmodel;

import android.app.Application;

import com.app.taxiapp.model.apiresponsemodel.YourTripsModel;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.YourTripRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class YourTripsViewModel extends AndroidViewModel {
    private YourTripRepository yourTripRepository;
    private LiveData<YourTripsModel> yourTripLiveData ;

    public YourTripsViewModel(@NonNull Application application) {
        super(application);
        yourTripRepository = new YourTripRepository();
    }


    public LiveData<YourTripsModel> getTrips(InputForAPI inputForAPI) {
        yourTripLiveData= yourTripRepository.getTrips(inputForAPI);
        return yourTripLiveData;
    }

}
