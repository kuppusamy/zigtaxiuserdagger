package com.app.taxiapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.taxiapp.model.apiresponsemodel.HelpDetailModel;
import com.app.taxiapp.model.apiresponsemodel.HelpModel;
import com.app.taxiapp.model.apiresponsemodel.SavedCardListResponseModel;
import com.app.taxiapp.model.apiresponsemodel.YourTripsModel;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.ChoosePaymentRepository;
import com.app.taxiapp.repository.HelpRepository;

public class ChoosePaymentViewModel extends AndroidViewModel {
    private ChoosePaymentRepository choosePaymentRepository;
    private LiveData<YourTripsModel> yourTripLiveData ;

    public ChoosePaymentViewModel(@NonNull Application application) {
        super(application);
        choosePaymentRepository = new ChoosePaymentRepository(application.getApplicationContext());
    }

    public LiveData<SavedCardListResponseModel> getCardLists(InputForAPI inputForAPI) {
        return choosePaymentRepository.getCardLists(inputForAPI);
    }



}
