package com.app.taxiapp.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.CommonRepository;

public class CommonViewModel extends AndroidViewModel {
    CommonRepository commonRepository;
    SharedHelper sharedHelper;

    public CommonViewModel(@NonNull Application application) {
        super(application);
        commonRepository = new CommonRepository(application.getApplicationContext());
        sharedHelper = new SharedHelper(application.getApplicationContext());
    }

    public AuthenticationResponse.Data getUserDetails() {
        AuthenticationResponse.Data data=new AuthenticationResponse.Data();
        data.setFirstname(getFirstName());
        data.setLastname(getLastName());
        data.setImage(getUserImage());
        data.setMobile(getUserMobile());
        return data;
    }


    public String getNotificationToken() {
        return commonRepository.getNotificationToken();
    }

    public String getCombinedStrings(String... strings) {
        StringBuilder finalString = new StringBuilder();
        for (String string : strings) {
            finalString.append(string);
        }
        return finalString.toString();
    }



    public void setUserData(AuthenticationResponse authenticationResponse) {
        sharedHelper.setFirstName(authenticationResponse.getData().getFirstname());
        sharedHelper.setLastName(authenticationResponse.getData().getLastname());
        sharedHelper.setImage(authenticationResponse.getData().getImage());
        sharedHelper.setMobile(authenticationResponse.getData().getMobile());
        sharedHelper.setToken(authenticationResponse.getData().getToken());
        sharedHelper.setUserRating(authenticationResponse.getData().getRating());
        sharedHelper.setIsUserLoggedIn(true);
    }

    public String getUserMobile() {
        return commonRepository.getUserMobile();
    }

    public String getFirstName() {
        return commonRepository.getFirstName();
    }

    public String getLastName() {
        return commonRepository.getLastName();
    }



    public String getUserName() {
        return commonRepository.getFirstName()+" "+commonRepository.getLastName();
    }


    public String getUserRating() {
        return commonRepository.getRating();
    }

    public String getUserImage() {
        return commonRepository.getUserImage();
    }




    public String getUserCountryCode() {
        return commonRepository.getUserCountryCode();
    }
    public String getCountryNameCode() {
        return commonRepository.getCountryNameCode();
    }


    public String getOTP() {
        return commonRepository.getOTP();
    }

    public String getAccessToken() {
        return commonRepository.getAccessToken();
    }


    public LiveData<FlagCheckResponseModel> flagCheckResponse(InputForAPI inputs) {
        return commonRepository.flagCheckResponse(inputs);
    }


    public String getCurrentBookingID() {
        return commonRepository.getCurrentBookingID();
    }
}
