package com.app.taxiapp.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.annotation.NonNull;

import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AppConfiguration;

import java.util.List;

public class SignInPageViewModel extends AndroidViewModel {

    private SharedHelper sharedHelper;

    public SignInPageViewModel(@NonNull Application application) {
        super(application);
        sharedHelper=new SharedHelper(application.getApplicationContext());
    }

    public List<AppConfiguration.Slider> getHomepageContent() {
     return sharedHelper.getAppConfiguration().getData().getSlider();
    }


    public String getString(int stringId) {
        return getApplication().getApplicationContext().getString(stringId);
    }
}
