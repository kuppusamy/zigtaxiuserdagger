package com.app.taxiapp.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import android.content.Context;

import androidx.annotation.NonNull;

import com.app.taxiapp.AppController;
import com.app.taxiapp.R;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.RouteGenerate.Route;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.model.apiresponsemodel.CancelReasonsModel;
import com.app.taxiapp.model.apiresponsemodel.CouponVerifyResponseModel;
import com.app.taxiapp.model.apiresponsemodel.CurrentBookingResponseModel;
import com.app.taxiapp.model.apiresponsemodel.GoogleLocationResponseModel.AddressComponent;
import com.app.taxiapp.model.apiresponsemodel.GoogleLocationResponseModel.GooglePlaceResponseModel;
import com.app.taxiapp.model.apiresponsemodel.RideTypeResponseModel;
import com.app.taxiapp.model.socketresponsemodel.ProviderDetailResponse;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.CommonRepository;
import com.app.taxiapp.repository.HomePageRepository;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

public class HomePageViewModel extends AndroidViewModel {
    private HomePageRepository homePageRepository;
    private CommonRepository commonRepository;

    public HomePageViewModel(@NonNull Application application) {
        super(application);
        homePageRepository = new HomePageRepository(getContext());
        commonRepository = new CommonRepository(getContext());
    }

    public LiveData<CancelReasonsModel> getCancelReasons(InputForAPI inputs) {
        return homePageRepository.getCancelReasons(inputs);
    }


    public LiveData<GooglePlaceResponseModel> getAddresDetails(InputForAPI inputs) {
        return homePageRepository.getAddresDetails(inputs);
    }


    public LiveData<CurrentBookingResponseModel> getCurrentBooking(InputForAPI inputs) {
        return homePageRepository.getCurrentBooking(inputs);
    }

    public LiveData<CouponVerifyResponseModel> verifyCoupon(InputForAPI inputs) {
        return homePageRepository.verifyCoupon(inputs);
    }


    public LiveData<RideTypeResponseModel> getAvailableRideType(InputForAPI inputs) {
        return homePageRepository.getAvailableRideType(inputs);
    }

    public String getAddresDetailsUrl(double latitude, double longitude) {
        String lat = String.valueOf(latitude);
        String lngg = String.valueOf(longitude);
        return UrlHelper.GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + lat + ","
                + lngg + "&sensor=true&key=" + AppController.getContext().getResources().getString(R.string.GOOGLE_MAPS_KEY);
    }


    public String getAccessToken() {
        return commonRepository.getAccessToken();
    }


    public Context getContext() {
        return getApplication().getApplicationContext();
    }

    public String getCombinedStrings(String... strings) {
        StringBuilder finalString = new StringBuilder();
        for (String string : strings) {
            finalString.append(string);
        }
        return finalString.toString();
    }

    public String isProviderAvailable(List<Marker> markers, int providerId) {
        String isAvailable = "false";
        for (int i = 0; i < markers.size(); i++) {
            ProviderDetailResponse.Data data = (ProviderDetailResponse.Data) markers.get(i).getTag();
            if (data != null && data.getProviderId() == providerId) {
                isAvailable = "" + i;
            }
        }
        return isAvailable;
    }

    public LiveData<Route> getRouteDetails(LatLng srcLatLng, LatLng desLatLng) {
        return homePageRepository.getRouteDetails(srcLatLng, desLatLng);
    }


    public String getCountryShortCode(List<AddressComponent> addressComponents) {
        String countryShortCode = "";
        for (int i = 0; i < addressComponents.size(); i++) {
            if (addressComponents.get(i).getTypes().size() > 0) {
                if (addressComponents.get(i).getTypes().get(0).equalsIgnoreCase("country")) {
                    countryShortCode = addressComponents.get(i).getShortName();
                }
            }

        }

        return countryShortCode;
    }

    public String getSharePageContent(CurrentBookingResponseModel value) {
        CurrentBookingResponseModel.ProviderInfo providerInfo = value.getData().getProviderInfo();
        String driverName = providerInfo.getFirstName() + " " + providerInfo.getLastName();
        String driverMobile = providerInfo.getMobile();
        String vehicleName = providerInfo.getVechileName();
        String vehicleNumber = providerInfo.getVechileNo();
        String fromAdd = value.getData().getFromLocation();
        String toAddress = value.getData().getToLocation();
        String divider = " : ";
        String newLine = "\n";
        return getCombinedStrings(getString(R.string.trip_status_from), getString(R.string.app_name), newLine, getString(R.string.driver_name), divider, driverName, newLine,
                getString(R.string.driver_mobile), divider, driverMobile, newLine,
                getString(R.string.vehicle_name), divider, vehicleName, newLine,
                getString(R.string.vehicle_number), divider, vehicleNumber, newLine,
                getString(R.string.from_address), divider, fromAdd, newLine,
                getString(R.string.to_address), divider, toAddress);
    }

    public String getString(int stringId) {
        return getContext().getResources().getString(stringId);
    }

    public boolean isToSourceLocation(String data) {
        return data.equalsIgnoreCase(Constants.BookingStatus.ACCEPTED) || data.equalsIgnoreCase(Constants.BookingStatus.REACHED) || data.equalsIgnoreCase(Constants.BookingStatus.ARRIVED);
    }
}
