package com.app.taxiapp.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.OTPRepository;
import com.app.taxiapp.repository.RegisterRepository;

public class RegisterViewModel extends AndroidViewModel {
    private OTPRepository otpRepository;
    private RegisterRepository registerRepository;

    public RegisterViewModel(@NonNull Application application) {
        super(application);
        otpRepository = new OTPRepository(application.getApplicationContext());
        registerRepository = new RegisterRepository(application.getApplicationContext());
    }


    public String getConfigurationType() {
        return registerRepository.getConfigurationType();
    }


    public LiveData<AuthenticationResponse> registerUser(InputForAPI inputForAPI) {
        return registerRepository.registerUser(inputForAPI);
    }



}
