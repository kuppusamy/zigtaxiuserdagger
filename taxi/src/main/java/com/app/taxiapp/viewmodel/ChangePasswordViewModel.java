package com.app.taxiapp.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.ChangePasswordRepository;

public class ChangePasswordViewModel extends AndroidViewModel {
    ChangePasswordRepository changePasswordRepository;

    public ChangePasswordViewModel(@NonNull Application application) {
        super(application);
        changePasswordRepository = new ChangePasswordRepository();
    }

    public LiveData<AuthenticationResponse> changePassword(InputForAPI inputs) {
        return changePasswordRepository.changePassword(inputs);
    }


}
