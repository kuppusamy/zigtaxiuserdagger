package com.app.taxiapp.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.PasswordRepository;

public class PasswordViewModel extends AndroidViewModel {
    private PasswordRepository passwordRepository;

    public PasswordViewModel(@NonNull Application application) {
        super(application);
        passwordRepository = new PasswordRepository();
    }


    public LiveData<AuthenticationResponse> loginWithPassword(InputForAPI inputForAPI) {
        return passwordRepository.loginWithPassword(inputForAPI);
    }



}
