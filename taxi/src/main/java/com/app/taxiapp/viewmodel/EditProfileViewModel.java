package com.app.taxiapp.viewmodel;

import android.app.Application;

import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.ImageUploadResponseModel;
import com.app.taxiapp.model.apiresponsemodel.ProfileResponseModel;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.repository.EditProfileRepository;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class EditProfileViewModel extends AndroidViewModel {
    private EditProfileRepository editProfileRepository;

    private SharedHelper sharedHelper;

    public EditProfileViewModel(@NonNull Application application) {
        super(application);
        editProfileRepository = new EditProfileRepository();

        sharedHelper = new SharedHelper(application.getApplicationContext());

    }

    public LiveData<ProfileResponseModel> getProfileData(InputForAPI inputs) {
        return editProfileRepository.getProfileData(inputs);
    }

    public LiveData<ImageUploadResponseModel> uploadImage(InputForAPI inputs) {
        return editProfileRepository.uploadImage(inputs);
    }

    public void setUserData(ProfileResponseModel.Data data) {
        sharedHelper.setFirstName(data.getFirstname());
        sharedHelper.setLastName(data.getLastname());
        sharedHelper.setImage(data.getImage());
        sharedHelper.setMobile(data.getMobile());
    }


}
