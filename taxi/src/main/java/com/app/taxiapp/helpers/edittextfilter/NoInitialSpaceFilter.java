package com.app.taxiapp.helpers.edittextfilter;

import android.text.InputFilter;
import android.text.Spanned;

public class NoInitialSpaceFilter implements InputFilter {
    @Override
    public CharSequence filter(final CharSequence source, final int start, final int end, final Spanned dest, final int dstart, final int dend) {
        if (dstart == 0) {
            for (int i = start; i < end; i++) {
                if (Character.isSpaceChar(source.charAt(i))) {
                    return "";
                }
            }
        }
        return null;
    }
}
