package com.app.taxiapp.helpers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.airbnb.lottie.LottieAnimationView;
import com.app.taxiapp.R;

/**
 * Created by User on 11-02-2017.
 */
public class ProgressBarDialog extends Dialog {
    @SuppressWarnings("ConstantConditions")
    public ProgressBarDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.full_screen_progressbar);
        LottieAnimationView lottieAnimationView = findViewById(R.id.animation_view);
        lottieAnimationView.playAnimation();
    }
}
