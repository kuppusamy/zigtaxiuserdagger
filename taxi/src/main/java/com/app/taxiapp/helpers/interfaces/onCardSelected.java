package com.app.taxiapp.helpers.interfaces;

import com.app.taxiapp.model.apiresponsemodel.SavedCardListResponseModel;

/**
 * Created by user on 24-10-2017.
 */

public interface onCardSelected {
    void onSelected(SavedCardListResponseModel.Data value);
}