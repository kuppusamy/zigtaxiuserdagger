package com.app.taxiapp.helpers.interfaces;

/**
 * Created by user on 24-10-2017.
 */

public interface onSocialLoginPerformed<T> {
    void onSuccess(T t,String socialType);
}