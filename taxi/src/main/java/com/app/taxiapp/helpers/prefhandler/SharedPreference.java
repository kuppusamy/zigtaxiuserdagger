package com.app.taxiapp.helpers.prefhandler;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;


/**
 * Created by KrishnaDev on 1/10/17.
 */

public class SharedPreference {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    public Context context;

    SharedPreference(Context context) {
        this.context = context;
    }

    void putKey(String Key, String Value) {
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(Key, Value);
        editor.apply();

    }

    void putBoolean(String Key, boolean Value) {
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean(Key, Value);
        editor.apply();

    }


    void saveObjectToSharedPreference(String serializedObjectKey, Object object) {
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(object);
        sharedPreferencesEditor.putString(serializedObjectKey, serializedObject);
        sharedPreferencesEditor.apply();
    }


    <GenericClass> GenericClass getSavedObjectFromPreference(String preferenceKey, Class<GenericClass> classType) {
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        if (sharedPreferences.contains(preferenceKey)) {
            final Gson gson = new Gson();
            String value = sharedPreferences.getString(preferenceKey, "");
            if (value.length() > 0) {
                return gson.fromJson(value, classType);
            } else {
                return null;
            }

        }
        return null;
    }


    String getKey(String Key) {
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        return sharedPreferences.getString(Key, "");
    }


    boolean getBoolean(String Key) {
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(Key, false);
    }


}
