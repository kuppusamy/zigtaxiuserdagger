package com.app.taxiapp.helpers.interfaces;

/**
 * Created by user on 24-10-2017.
 */

public interface Callback {
    void onSuccess(String value);
}