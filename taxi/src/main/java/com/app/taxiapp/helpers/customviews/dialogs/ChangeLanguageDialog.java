package com.app.taxiapp.helpers.customviews.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.DialogWithRecyclerviewBinding;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.DialogChanged;
import com.app.taxiapp.helpers.interfaces.onClickListener;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.LanguageModel;
import com.app.taxiapp.view.adapter.LanguageListAdapter;
import com.google.gson.Gson;

public class ChangeLanguageDialog extends Dialog {
    private Context context;
    private String selectedLanguage;
    private DialogChanged dialogChanged;

    public ChangeLanguageDialog(@NonNull Context context) {
        super(context);
        this.context=context;
    }

    public void setDialogChanged(DialogChanged dialogChanged) {
        this.dialogChanged = dialogChanged;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String languageValues = "";
        final SharedHelper sharedHelper = new SharedHelper(context);
        try {
            languageValues = Utils.convertJsonFiletoString(context, R.raw.language_list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        final LanguageModel languageLists = gson.fromJson(languageValues, LanguageModel.class);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        DialogWithRecyclerviewBinding dialogWithRecyclerviewBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_with_recyclerview, null, false);
        setContentView(dialogWithRecyclerviewBinding.getRoot());
        dialogWithRecyclerviewBinding.updateButton.setVisibility(View.VISIBLE);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        LanguageListAdapter languageListAdapter = new LanguageListAdapter(context, languageLists.getLanguageLists());
        languageListAdapter.setOnClickListner(new onClickListener() {
            @Override
            public void onClicked(int position) {
                selectedLanguage = languageLists.getLanguageLists().get(position).getShortCode();
            }
        });
        dialogWithRecyclerviewBinding.listRecyclerView.setLayoutManager(linearLayoutManager);
        dialogWithRecyclerviewBinding.listRecyclerView.setAdapter(languageListAdapter);
        dialogWithRecyclerviewBinding.headingText.setText(context.getResources().getString(R.string.choose_language));
        dialogWithRecyclerviewBinding.updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                sharedHelper.setSelectedLanguage(selectedLanguage);
                dialogChanged.onLanguageChanged();
            }
        });
    }

}
