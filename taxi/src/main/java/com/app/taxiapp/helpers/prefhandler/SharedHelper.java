package com.app.taxiapp.helpers.prefhandler;

import android.content.Context;

import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.model.apiresponsemodel.AppConfiguration;
import com.app.taxiapp.model.apiresponsemodel.autocompleteresponse.Prediction;

import java.util.ArrayList;
import java.util.List;

public class SharedHelper {
    private AppConfiguration appConfiguration;
    private Context context;
    private SharedPreference sharedPreference;
    private String countryCode;
    private String selectedCardId;
    private String selectedLanguage;
    private String recentSearchcount;
    int maxSearchCount = 5;

    private String tutorialDone;

    public String getTutorialDone() {
        tutorialDone = sharedPreference.getKey("tutorialDone");
        return tutorialDone;
    }

    public void setTutorialDone(String tutorialDone) {
        this.sharedPreference.putKey("tutorialDone", tutorialDone);
        tutorialDone = tutorialDone;

    }

    private String TAG = SharedHelper.class.getSimpleName();

    public String getRecentSearchcount() {
        recentSearchcount = sharedPreference.getKey("recentSearchcount");
        return recentSearchcount;
    }

    public void setRecentSearchcount(String recentSearchcount) {
        this.sharedPreference.putKey("recentSearchcount", recentSearchcount);
        recentSearchcount = recentSearchcount;

    }

    public String getSelectedPaymentType() {
        selectedPaymentType = sharedPreference.getKey("selectedPaymentType");
        return selectedPaymentType;
    }

    public void setSelectedPaymentType(String selectedPaymentType) {
        this.sharedPreference.putKey("selectedPaymentType", selectedPaymentType);
        selectedPaymentType = selectedPaymentType;

    }

    private String selectedPaymentType;

    public String getSelectedLanguage() {
        selectedLanguage = sharedPreference.getKey("selectedLanguage");
        if (selectedLanguage.length() == 0) {
            selectedLanguage = "en";
        }
        return selectedLanguage;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        selectedLanguage = selectedLanguage;
        sharedPreference.putKey("selectedLanguage", selectedLanguage);

    }

    public String getSelectedCardId() {
        selectedCardId = sharedPreference.getKey("selectedCardId");
        return selectedCardId;
    }

    public void setSelectedCardId(String selectedCardId) {
        selectedCardId = selectedCardId;
        sharedPreference.putKey("selectedCardId", selectedCardId);

    }

    public boolean getIsUserLoggedIn() {
        isUserLoggedIn = sharedPreference.getBoolean("isUserLoggedIn");
        return isUserLoggedIn;
    }

    public String getNotificationToken() {
        notificationToken = sharedPreference.getKey("notificationToken");

        return notificationToken;
    }

    public String getBookingNumber() {
        bookingNumber = sharedPreference.getKey("bookingNumber");
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        sharedPreference.putKey("bookingNumber", bookingNumber);
        this.bookingNumber = bookingNumber;
    }

    private String bookingNumber;

    public void setNotificationToken(String notificationToken) {
        sharedPreference.putKey("notificationToken", notificationToken);
        this.notificationToken = notificationToken;
    }

    private String notificationToken;


    public void setIsUserLoggedIn(boolean isUserLoggedIn) {
        sharedPreference.putBoolean("isUserLoggedIn", isUserLoggedIn);
        this.isUserLoggedIn = isUserLoggedIn;
    }

    private boolean isUserLoggedIn;
    private boolean isTokenUpated;


    public String getOTP() {
        OTP = sharedPreference.getKey("OTP");
        return OTP;
    }

    public void setOTP(String OTP) {
        sharedPreference.putKey("OTP", OTP);

        this.OTP = OTP;
    }

    private String OTP;
    private String firstName;
    private String lastName;
    private String mobile;
    private String image;
    private String token;

    public String getCountryNameCode() {
        countryNameCode = sharedPreference.getKey("countryNameCode");
        return countryNameCode;
    }

    public void setCountryNameCode(String countryNameCode) {
        sharedPreference.putKey("countryNameCode", countryNameCode);

        this.countryNameCode = countryNameCode;
    }

    private String countryNameCode;

    public String getUserRating() {
        userRating = sharedPreference.getKey("userRating");

        return userRating;
    }

    public void setUserRating(String userRating) {
        sharedPreference.putKey("userRating", userRating);
        this.userRating = userRating;
    }

    private String userRating;

    public String getFirstName() {
        firstName = sharedPreference.getKey("firstName");

        return firstName;
    }

    public void setFirstName(String firstName) {

        sharedPreference.putKey("firstName", firstName);

        this.firstName = firstName;
    }

    public String getLastName() {
        lastName = sharedPreference.getKey("lastName");

        return lastName;
    }

    public void setLastName(String lastName) {
        sharedPreference.putKey("lastName", lastName);

        this.lastName = lastName;
    }

    public String getMobile() {

        mobile = sharedPreference.getKey("mobile");

        return mobile;
    }

    public void setMobile(String mobile) {
        sharedPreference.putKey("mobile", mobile);

        this.mobile = mobile;
    }

    public String getImage() {
        image = sharedPreference.getKey("image");
        return image;
    }

    public void setImage(String image) {
        sharedPreference.putKey("image", image);

        this.image = image;
    }

    public String getToken() {
        token = sharedPreference.getKey("token");
        return token;
    }

    public void setToken(String token) {
        sharedPreference.putKey("token", token);
        this.token = token;
    }


    public boolean isTokenUpated() {
        isTokenUpated = sharedPreference.getBoolean("isTokenUpated");
        return isTokenUpated;
    }

    public void setTokenUpated(boolean tokenUpated) {
        sharedPreference.putBoolean("isTokenUpated", isTokenUpated);
        isTokenUpated = tokenUpated;
    }


    public String getCountryCode() {
        countryCode = sharedPreference.getKey("countryCode");
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        sharedPreference.putKey("countryCode", countryCode);
        this.countryCode = countryCode;
    }


    public String getMap_key() {
        return getAppConfiguration().getData().getAuthConfig().getMap_api_key();
    }

    public SharedHelper(Context context) {
        this.context = context;
        sharedPreference = new SharedPreference(context);
    }

    public AppConfiguration getAppConfiguration() {
        appConfiguration = sharedPreference.getSavedObjectFromPreference("appConfiguration", AppConfiguration.class);
        return appConfiguration;
    }

    public void setAppConfiguration(AppConfiguration appConfiguration) {
        sharedPreference.saveObjectToSharedPreference("appConfiguration", appConfiguration);
        this.appConfiguration = appConfiguration;
    }


    public List<Prediction> getRecentSearchLists() {
        List<Prediction> predictionList = new ArrayList<>();


        int maxSavedCount;
        if (getRecentSearchcount().length() > 0) {
            maxSavedCount = Integer.parseInt(getRecentSearchcount());

            for (int i = 0; i <= maxSavedCount; i++) {
                String postFixCount = "" + i;
                predictionList.add(sharedPreference.getSavedObjectFromPreference("prediction_" + postFixCount, Prediction.class));
            }
        }

        return predictionList;
    }

    public void movePredictionListToNextPosition(int positiontoRemove) {
        List<Prediction> predictionList = getRecentSearchLists();
        Prediction prediction = getRecentSearchLists().get(positiontoRemove);
        predictionList.remove(positiontoRemove);
        predictionList.add(prediction);
        for (int i = 0; i < predictionList.size(); i++) {
            sharedPreference.saveObjectToSharedPreference("prediction_" + i, predictionList.get(i));
        }


    }

    public void addrecentSearch(Prediction prediction) {
        int searchCount;
        if (getRecentSearchcount().length() > 0) {
            searchCount = Integer.parseInt(getRecentSearchcount());
            String postFixCount;

            postFixCount = String.valueOf(searchCount + 1);


            String isInlist = predictionInList(getRecentSearchLists(), prediction);
            Utils.log(TAG, postFixCount);
            if (isInlist.equalsIgnoreCase("false")) {
                setRecentSearchcount(postFixCount);
                sharedPreference.saveObjectToSharedPreference("prediction_" + postFixCount, prediction);
            } else {
                movePredictionListToNextPosition(Integer.parseInt(isInlist));
            }
        } else {
            setRecentSearchcount("0");
            sharedPreference.saveObjectToSharedPreference("prediction_0", prediction);
        }

    }

    private String predictionInList(List<Prediction> predictionList, Prediction prediction) {
        String isInlist = "false";
        if (predictionList.size() > 0) {
            for (int i = 0; i < predictionList.size(); i++) {
                if (predictionList.get(i).getId().equalsIgnoreCase(prediction.getId())) {
                    isInlist = String.valueOf(i);
                }
            }
        }
        return isInlist;
    }
}
