package com.app.taxiapp.helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.taxiapp.AppController;
import com.app.taxiapp.BuildConfig;
import com.app.taxiapp.R;
import com.app.taxiapp.databinding.DialogUpdateProfileBinding;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.app.taxiapp.AppController.getContext;

public class Utils {

    public static boolean isPasswordVisible = false;
    private static Drawable icon;
    private Dialog noInternetdialog;

    public static boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() >= 6 && phone.length() <= 13) {
                check = true;
            }
        }
        return check;
    }

    public static byte[] base64ToByte(String base64) {
        return Base64.decode(base64, Base64.DEFAULT);

    }


    public static boolean isEmpty(String textEntered) {
        boolean check = false;
        if (TextUtils.isEmpty(textEntered)) {
            check = true;
        }
        return check;
    }


    public static String getRoundedOffSixDigits(double value, String format) {

        return String.format(Utils.getLocale(), format, value);
    }


    public static boolean isValidEmail(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static int getPrimaryColor(final Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

    }


    public static String capitalizeSentence(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    public static Drawable changeDrawableColor(int iconValue, int colorValue) {
        icon = ContextCompat.getDrawable(getContext(), iconValue).mutate();
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(colorValue, typedValue, true);
        icon.setColorFilter(typedValue.data, PorterDuff.Mode.SRC_ATOP);
        return icon;
    }


    public static String isValidPassword(String password) {
        Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
        Pattern lowerCasePatten = Pattern.compile("[a-z ]");
        Pattern digitCasePatten = Pattern.compile("[0-9 ]");

        if (password.length() < 8) {
            return getContext().getResources().getString(R.string.password_length_should_be_eight_characters);
        } else if (!specailCharPatten.matcher(password).find()) {
            return getContext().getResources().getString(R.string.password_should_contain_atleast_one_special_character);
        } else if (!UpperCasePatten.matcher(password).find()) {
            return getContext().getResources().getString(R.string.password_should_contain_atleast_one_uppercase_character);
        } else if (!lowerCasePatten.matcher(password).find()) {
            return getContext().getResources().getString(R.string.password_should_contain_atleast_one_lowercase_character);
        } else if (!digitCasePatten.matcher(password).find()) {
            return getContext().getResources().getString(R.string.password_should_contain_atleast_one_numeric);
        } else {
            return "true";
        }
    }

    public static String convertJsonFiletoString(Context context, int fileName) throws Exception {
        InputStream is = context.getResources().openRawResource(fileName);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        return new String(buffer, StandardCharsets.UTF_8);

    }

    public static float caluclatedistanceInMeters(LatLng src, LatLng des) {
        double lat_a = src.latitude;
        double lng_a = src.longitude;
        double lat_b = des.latitude;
        double lng_b = des.longitude;

        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b - lat_a);
        double lngDiff = Math.toRadians(lng_b - lng_a);
        double a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff / 2) * Math.sin(lngDiff / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return new Float(distance * meterConversion).floatValue();
    }

    public static Locale getLocale() {
        return Locale.ENGLISH;
    }

    public static String getConvertedTime(String createdTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Utils.getLocale());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String targetdatevalue = "";
        try {
            Date startDate = simpleDateFormat.parse(createdTime);
            SimpleDateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy. h:mm a", Utils.getLocale());
            targetFormat.setTimeZone(Calendar.getInstance().getTimeZone());
            targetdatevalue = targetFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetdatevalue;

    }

    public static String getVersionName(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }


    private void showInternetDialog(final Context context) {
        noInternetdialog = new Dialog(context, android.R.style.Theme_Dialog);
        noInternetdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        noInternetdialog.setContentView(R.layout.no_internet_dialog);
        noInternetdialog.setCanceledOnTouchOutside(true);
        TextView try_again = noInternetdialog.findViewById(R.id.try_again);
        try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkConnected(context)) {

                } else {

                }
            }
        });

    }

    private void removeInternetDialog() {
        if (noInternetdialog != null && noInternetdialog.isShowing()) {
            noInternetdialog.dismiss();
        }
    }

    public static void navigateToFragment(int id, Fragment fragmentName, FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_out_right, R.anim.enter_from_right, R.anim.exit_out_right);
        fragmentTransaction.replace(id, fragmentName);
        fragmentTransaction.addToBackStack(fragmentName.getClass().getSimpleName());
        fragmentTransaction.commit();
    }


    public static void showStatusBar(Activity activity) {
        if (Build.VERSION.SDK_INT > 16) {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = activity.getWindow().getDecorView();
            // Show Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
            int flags = decorView.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            decorView.setSystemUiVisibility(flags);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            activity.getWindow().setStatusBarColor(Color.WHITE);
        }
    }


    public static String convertSecondsToMinutes(long secondtTime) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat("mm:ss", Utils.getLocale());
        df.setTimeZone(tz);

        return df.format(new Date(secondtTime * 1000L));

    }

    public static void showKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    public static void dismissKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void togglePasswordvisibility(EditText passwordEdittext, ImageView passwordHolder) {
        if (!isPasswordVisible) {
            passwordHolder.setImageResource(R.drawable.hide_password);
            passwordEdittext.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            isPasswordVisible = true;
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        } else {
            isPasswordVisible = false;
            passwordHolder.setImageResource(R.drawable.show_password);
            passwordEdittext.setTransformationMethod(new PasswordTransformationMethod());
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        }
    }


    public static void log(String TAG, String content) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, content);
        }
    }

    public static boolean isNetworkConnected(Context context) {
        boolean isConnected;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager
                .getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            isConnected = true;
        } else {
            isConnected = false;
        }
        return isConnected;
    }

    public static void displayError(View view, String toastmsg) {
        Snackbar snackbar = Snackbar.make(view, toastmsg, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }


    public static void setViewAboveNavigationBarMargin(View view, Activity activity) {
        int pixel_bottom = getSoftButtonsBarSizePort(activity);
        ViewGroup.MarginLayoutParams s = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        s.setMargins(0, 0, 0, pixel_bottom);
        view.setLayoutParams(view.getLayoutParams());
    }

    public static void setViewAboveNavigationBarPadding(View view, Activity activity) {
        int pixel_bottom = getSoftButtonsBarSizePort(activity);
        view.setPadding(0, 0, 0, pixel_bottom);
    }


    private static int getSoftButtonsBarSizePort(Activity activity) {
        int result = 0;
        boolean hasMenuKey = ViewConfiguration.get(activity).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        boolean hasNavigationVisible = hasSoftKeys(activity.getWindowManager());


        if (hasNavigationVisible) {
            if (!hasMenuKey && !hasBackKey) {
                //The device has a navigation bar
                Resources resources = activity.getResources();

                int orientation = resources.getConfiguration().orientation;
                int resourceId;
                if (isTablet(activity)) {
                    resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_height_landscape", "dimen", "android");
                } else {
                    resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_width", "dimen", "android");
                }

                if (resourceId > 0) {
                    return resources.getDimensionPixelSize(resourceId);
                }
            }
        }
        return result;
    }

    private static boolean hasSoftKeys(WindowManager windowManager) {
        Display d = windowManager.getDefaultDisplay();

        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            d.getRealMetrics(realDisplayMetrics);
        }

        int realHeight = realDisplayMetrics.heightPixels;
        int realWidth = realDisplayMetrics.widthPixels;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        d.getMetrics(displayMetrics);

        int displayHeight = displayMetrics.heightPixels;
        int displayWidth = displayMetrics.widthPixels;

        return (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
    }


    private static boolean isTablet(Activity activity) {
        return (activity.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }


    public static String getDoubleDigit(int value) {
        DecimalFormat formatter = new DecimalFormat("00");
        return formatter.format(value);
    }



}
