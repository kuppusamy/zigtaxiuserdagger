package com.app.taxiapp.helpers.interfaces;

/**
 * Created by yuvaraj on 18/1/18.
 */

public interface onDeleteClickListener {
    void onDelete(int position);
}
