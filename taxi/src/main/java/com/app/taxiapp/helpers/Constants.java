package com.app.taxiapp.helpers;

public class Constants {

    public static class ValidationKey {
        public static final String FALSE = "false";
        public static final String TRUE = "true";
        public static final String MIN = "min";
        public static final String MAX = "max";

    }


    public static class ArgumentKeys {
        public static final String ARG_POSITION = "position";
        public static final String TOTAL_COUNT = "totalcount";
        public static final String ARG_WELCOME_MODEL = "welcomeModel";
        public static final String TYPE = "type";
        public static final String DATA = "data";
        public static final String LOGIN_TYPE = "login_type";
    }


    public enum SelectedPaymentType {
        CASH, CARD, WALLET
    }

    public enum BookingType {
        NOW, LATER
    }


    public static class IntentKeys {
        public static final String TRIP = "trip";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String AREA_NAME = "areaname";
        public static final String SHORT_CODE = "short_code";
        public static final String RIDE = "ride";
        public static final String REQUEST_CODE = "request_code";
        public static final String BOOKING_NUMBER = "bookingNo";
        public static final String STATIC_MAP_URL = "staticMapUrl";
        public static final String DATA = "DATA";
        public static final String TITLE = "TITLE";
        public static final String ID = "ID";
        public static final String LOGIN_TYPE = "login_type";
        public static final String TYPE = "type";
        public static final String SELECTED_PAYMENT_NAME = "payment_type";
        public static final String SKIP = "skip";
    }

    public static class TransactionType {
        public static final String DEBIT = "debit";
        public static final String CREDIT = "credit";
        public static final String FAILED = "failed";
    }


    public static class BookingStatus {
        public static final String ASSIGNED = "assigned";
        public static final String PROCESSING = "processing";

        public static final String ACCEPTED = "accepted";
        public static final String REACHED = "reached";
        public static final String ARRIVED = "arrived";
        public static final String PICKED_UP = "pickedup";

        public static final String CANCELLED = "cancelled";
        public static final String COMPLETED = "completed";
        public static final String RATING = "rating";
    }


    public static class NotificationKeys {
        public static final String BOOKING_CANCELLED = "booking_cancelled";

    }


    public static class ApiKeys {
        public static final String MOBILE = "mobile";
        public static final String EMAIL = "email";
        public static final String ACCEPT = "Accept";
        public static final String COUNTRY_CODE = "countryCode";
        public static final String PASSWORD = "password";
        public static final String VERSION = "version";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String LANG = "lang";
        public static final String TOKEN = "token";
        public static final String COUNTRY_ID = "countryId";
        public static final String LOGIN_TYPE = "loginType";
        public static final String SOCIAL_TOKEN = "socialToken";
        public static final String COUPON = "coupon";
        public static final boolean TRUE = true;
        public static final String OTP = "otp";
        public static final String TYPE = "type";
        public static final String UUID = "uuid";
        public static final String ACCESS_TOKEN = "access_token";
        public static final String CONTENT_TYPE = "Content-Type";
        public static final String ROLE = "Role";
        public static final String FILED_NAME = "fieldName";
        public static final String DATA = "data";
        public static final String PICKUP_LATITUDE = "pickUpLatitude";
        public static final String PICKUP_LONGITUDE = "pickUpLongitude";
        public static final String DESTINATION_LATITUDE = "destinyLatitude";
        public static final String DESTINATION_LONGITUDE = "destinyLongitude";
        public static final String COUNTRY_SHORT_CODE = "countryShortCode";
        public static final String PICKUP_LOCATION = "pickUpLocation";
        public static final String DROP_LOCATION = "dropLocation";
        public static final String RIDE_TYPE = "rideType";
        public static final String PAYMENT_MODE = "paymentMode";
        public static final String SEATS = "seats";
        public static final String CARD_ID = "cardId";
        public static final String BOOKING_TYPE = "bookingType";
        public static final String BOOKING_TIMESTAMP = "bookingTimestamp";
        public static final String OK = "OK";
        public static final String IMAGE = "image";
        public static final String STATUS = "status";
        public static final String BOOKING_NUMBER = "bookingNo";
        public static final String FCM_TOKEN = "fcmToken";
        public static final String BRAND = "brand";
        public static final String MODEL = "model";
        public static final String OS = "os";
        public static final String OSVERSION = "osVersion";
        public static final String APPVERSION = "appVersion";
        public static final String REASON = "reason";
        public static final String SOURCE = "source";
        public static final String AMOUNT = "amount";
        public static final String IS_COUPON_APPLIED = "isCouponApplied";
        public static final String DISCOUNT_AMOUNT = "discountAmount";
        public static final String REDEEMED_ID = "redeemedId";

        public static final String LANGUAGE_NAME = "languageName";

        public static final String COMMENTS = "comments";
        public static final String RATING = "rating";
        public static final String ACTION = "action";
        public static final String AUTHORIZATION = "Authorization";
    }


    public static class OtpType {
        public static final String REGISTER = "register";
        public static final String LOGIN = "login";
        public static final String RESET_PASSWORD = "resetPwd";
        public static final String CHANGE_MOBILE_NUMBER = "chaangeMobile";
    }


    public static class AuthType {
        public static final String PASSWORD = "PWD";
        public static final String OTP = "chaangeMobile";
    }

    public static class RatingType {
        public static final String SKIPPED = "skipped";
        public static final String RATED = "yes";
    }


    public static class TripState {
        public static final String YES = "yes";
    }

    public static class LoginType {
        public static final String FACEBOOK = "facebook";
        public static final String GOOGLE = "google";
        public static final String NORMAL = "manual";
    }

    public static class HomePageState {
        public static final int SOURCE_SELECTED = 0;
        public static final int SERVICE_TYPE_LISTED = 1;
        public static final int RIDE_CUSTOMSATION = 2;
        public static final int SEARCHING_DRIVER = 3;
    }


    public static class RideType {
        public static final int RIDE_TYPE_CAR = 1;
        public static final int RIDE_TYPE_BIKE = 2;
    }


    public static class AnimationKeys {

        public static final String EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X";
        public static final String EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y";

    }

    public static class IntentPermissionCode {
        public static final int COARSE_LOCATION_PERMISSIONS = 1;
        public static final int REQUEST_LOCATION = 2;
        public static final int SOURCE_REQUEST_CODE = 3;
        public static final int DESTINATION_REQUEST_CODE = 4;
        public static final int RATING_STATUS = 5;
        public static final int IMAGE_PICKER = 6;
        public static final int EXTERNAL_STORAGE_PERMISSION = 7;
        public static final int CALL_PHONE = 8;
        public static final int STRIPE_PAYMENT = 9;
        public static final int ADD_CARD = 10;
        public static final int PAYMENT_SELECTION = 11;
        public static final int UPDATE_CHECK = 12;

    }

}
