package com.app.taxiapp.helpers.rxbus;


import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBus {

    public RxBus() {
    }

    private PublishSubject<String> bus = PublishSubject.create();

    public void send(String o) {
        bus.onNext(o);
    }

    public Observable<String> toObservable() {
        return bus;
    }

}