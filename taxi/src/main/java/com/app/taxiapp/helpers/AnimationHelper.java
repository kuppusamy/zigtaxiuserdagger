package com.app.taxiapp.helpers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;

import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import java.util.ArrayList;
import java.util.List;

public class AnimationHelper {


    private Polyline blackPolyline, greyPolyline;
    private static String TAG = AnimationHelper.class.getSimpleName();
    private List<LatLng> listLatLng = new ArrayList();
    private ValueAnimator animator;

    public void viewSlideUp(final View view, int duration) {
        TranslateAnimation textAnimation = new TranslateAnimation(0, 0, 500, 0);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        float fromAlpha = (float) 0.2;
        float toAlpha = (float) 1.0;
        AlphaAnimation fadeInAnimation = new AlphaAnimation(fromAlpha, toAlpha);
        fadeInAnimation.setDuration(duration);
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(textAnimation);
        s.addAnimation(fadeInAnimation);
        view.setAnimation(s);
        view.setVisibility(View.VISIBLE);
    }


    public void slideAndHide(final View view, int duration, int fromX, int toX, int fromY, int toY) {
        TranslateAnimation textAnimation = new TranslateAnimation(fromX, toX, fromY, toY);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(textAnimation);
        view.setAnimation(s);
    }


    public void slideAndSHow(final View view, int duration, int fromX, int toX, int fromY, int toY) {
        TranslateAnimation textAnimation = new TranslateAnimation(fromX, toX, fromY, toY);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        AnimationSet s = new AnimationSet(false);
        s.addAnimation(textAnimation);
        view.setAnimation(s);
        view.setVisibility(View.VISIBLE);
    }


    public void animatePolyLine(final Polyline blackPolyLine, List<LatLng> points) {
        this.blackPolyline = blackPolyLine;
        this.listLatLng = points;
        stopPolylineanimation();
        animator = ValueAnimator.ofInt(0, 100);
        animator.setDuration(2500);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                List<LatLng> latLngList = blackPolyLine.getPoints();
                int initialPointSize = latLngList.size();
                int animatedValue = (int) animator.getAnimatedValue();
                int newPoints = (animatedValue * listLatLng.size()) / 100;

                if (initialPointSize < newPoints) {
                    latLngList.addAll(listLatLng.subList(initialPointSize, newPoints));
                    blackPolyLine.setPoints(latLngList);
                }


            }
        });

        animator.addListener(polyLineAnimationListener);
        animator.start();
    }

    public void stopPolylineanimation() {
        try {
            animator.cancel();
        } catch (Exception ignored) {

        }
    }

    private Animator.AnimatorListener polyLineAnimationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {

        }

        @Override
        public void onAnimationEnd(Animator animator) {

            List<LatLng> blackLatLng = blackPolyline.getPoints();
            blackLatLng.clear();
            blackPolyline.setPoints(blackLatLng);
            animator.start();
        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {


        }
    };


    public void viewSlideFromLeft(final View view, int duration) {
        TranslateAnimation textAnimation = new TranslateAnimation(-1500, 0, 0, 0);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        view.startAnimation(textAnimation);
    }


    public void viewSlideFromRight(final View view, int duration) {
        TranslateAnimation textAnimation = new TranslateAnimation(1500, 0, 0, 0);
        textAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        textAnimation.setDuration(duration);
        textAnimation.setFillAfter(true);
        view.startAnimation(textAnimation);
    }


    public interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    public static double getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);
        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void revealActivity(final View view, int x, int y) {
        float finalRadius = (float) (Math.max(view.getWidth(), view.getHeight()) * 1.1);
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(view, x, y, 0, finalRadius);
        circularReveal.setDuration(400);
        circularReveal.setInterpolator(new AccelerateInterpolator());
        view.setVisibility(View.VISIBLE);
        circularReveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }
        });
        circularReveal.start();
    }


    public void imageRotationAnimation(int from, int to, View view) {
        RotateAnimation rotate = new RotateAnimation(from, to, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(500);
        rotate.setInterpolator(new LinearInterpolator());
        view.startAnimation(rotate);
    }

    public static void presentActivity(Activity activity, View view, Class aClass) {
        try {


            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(activity, view, "transition");
            int revealX = (int) (view.getX() + view.getWidth() / 4);
            int revealY = (int) (view.getY() + view.getHeight() / 2);
            Intent intent = new Intent(activity, aClass);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(Constants.AnimationKeys.EXTRA_CIRCULAR_REVEAL_X, revealX);
            intent.putExtra(Constants.AnimationKeys.EXTRA_CIRCULAR_REVEAL_Y, revealY);
            ActivityCompat.startActivity(activity, intent, options.toBundle());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void imageSlide(View view) {
        TranslateAnimation imageAnimation =
                new TranslateAnimation(100, -100, 0, 0);
        imageAnimation.setDuration(10000);
        imageAnimation.setFillAfter(true);
        imageAnimation.setRepeatCount(-1);
        imageAnimation.setRepeatMode(Animation.REVERSE);
        view.setAnimation(imageAnimation);
        view.setVisibility(View.VISIBLE);

    }


    public static class LoaderZigZagMain {

        private static final String TAG = LoaderZigZagMain.class.getSimpleName();
        private View mloaderZigZagView;
        static TranslateAnimation moveAnimRightToLeft, moveAnimLeftToRight;
        private View parentViews;
        private int pos;
        private int[] width = new int[2];

        public LoaderZigZagMain(final View loaderZigZagView, final View parentView) {
            this.mloaderZigZagView = loaderZigZagView;
            this.parentViews = parentView;
            pos = 0;

            final ViewTreeObserver viewTreeObserver = parentView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        parentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        width[pos] = parentView.getWidth();
                        pos = 1;
                        Log.e("first", "onGlobalLayout: " + width[0]);
                    }
                });
            }

        }

        private void starting(Boolean status) {

            moveAnimRightToLeft = new TranslateAnimation(0, width[0] - mloaderZigZagView.getWidth(), 0, 0);
            moveAnimRightToLeft.setDuration(500);
            moveAnimLeftToRight = new TranslateAnimation(width[0] - mloaderZigZagView.getWidth(), 0, 0, 0);
            moveAnimLeftToRight.setDuration(500);
            parentViews.setVisibility(View.VISIBLE);
            moveAnimRightToLeft.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    mloaderZigZagView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mloaderZigZagView.startAnimation(moveAnimLeftToRight);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            moveAnimLeftToRight.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    mloaderZigZagView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mloaderZigZagView.startAnimation(moveAnimRightToLeft);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            if (status) {
                mloaderZigZagView.startAnimation(moveAnimLeftToRight);
            } else {
                mloaderZigZagView.clearAnimation();
            }


        }


        public void stop() {

            final ViewTreeObserver viewTreeObserver = parentViews.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        width[0] = parentViews.getWidth();
                        starting(false);
                        parentViews.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        parentViews.setVisibility(View.GONE);
                    }
                });
            }
            parentViews.setVisibility(View.GONE);
            mloaderZigZagView.setVisibility(View.GONE);
        }


        public void start() {
            width[0] = parentViews.getWidth();
            if (width[0] == 0) {
                width[0] = getScreenWidth();
            }
            Utils.log(TAG, "" + parentViews.getWidth());
            starting(true);

        }

        static int getScreenWidth() {
            return Resources.getSystem().getDisplayMetrics().widthPixels;
        }

    }

}
