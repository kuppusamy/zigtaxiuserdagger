package com.app.taxiapp.helpers;

import com.app.taxiapp.AppController;
import com.app.taxiapp.BuildConfig;
import com.app.taxiapp.R;

public class UrlHelper {

    public static final String BASE = getBaseUrl();

    private static String getBaseUrl() {
        if (!BuildConfig.DEBUG) {
            return BuildConfig.BASE_LIVE_URL;
        } else {
        return BuildConfig.BASE_DEMO_URL;
        }

    }


    //users
    private static final String BASE_USER_URL = BASE + "/api/users/";
    public static final String CHECK_MOBILE_EXISTENCE = BASE_USER_URL + "check";
    public static final String GET_APP_CONFIGURATION = BASE_USER_URL + "config";
    public static final String VERIFY_OTP = BASE_USER_URL + "otpVerify";
    public static final String RESEND_OTP = BASE_USER_URL + "resendOtp";
    public static final String SIGN_UP = BASE_USER_URL + "signup";
    public static final String VERIFY_PASSWORD = BASE_USER_URL + "pwdVerify";
    public static final String UPDATE_PASSWORD = BASE_USER_URL + "updatePwd";
    public static final String FORGOT_PASSWORD = BASE_USER_URL + "forgotPwdOtp";
    public static final String GET_TRIP_HISTORY = BASE_USER_URL + "tripHistory";
    public static final String VIEW_PROFILE = BASE_USER_URL + "profile";
    public static final String UPDATE_PROFILE = BASE_USER_URL + "profileUpdate";
    public static final String UPDATE_DEVICE_TOKEN = BASE_USER_URL + "deviceUpdate";
    public static final String FILE_UPLOAD = BASE_USER_URL + "fileUpload";
    public static final String GET_CURRENT_BOOKING = BASE_USER_URL + "myActiveBooking";
    public static final String GET_TRIP_DETAILS = BASE_USER_URL + "tripDetails";
    public static final String GET_CANCELLATION_AVAILABLE_REASONS = BASE_USER_URL + "cancelPolicy";
    public static final String CANCEL_B0OKING = BASE_USER_URL + "cancelBooking";
    public static final String UPDATE_RATING = BASE_USER_URL + "rating";
    public static final String GET_HELP = BASE_USER_URL + "staticPage";
    public static final String MY_WALLET_DETAILS = BASE_USER_URL + "myWallet";
    public static final String SOCIAL_TOKEN_CHECK = BASE_USER_URL + "usersSocialTokencheck";
    public static final String EPHEMERAL_KEY_GENERATOR = BASE_USER_URL + "generateEphemeralKeys";
    public static final String VERIFY_COUPON = BASE_USER_URL + "promoCodesRedeem";
    //bookings
    private static final String BASE_BOOKING_URL = BASE + "/api/booking/";
    public static final String GET_SERVICE_LIST = BASE_BOOKING_URL + "rideType";
    public static final String BOOK_RIDE = BASE_BOOKING_URL + "bookRide";

    //stripe
    public static final String ADD_CARD = BASE_USER_URL + "addCard";
    public static final String REMOVE_CARD = BASE_USER_URL + "removeCard";
    public static final String CARD_LIST = BASE_USER_URL + "cardList";
    public static final String ADD_MONEY_IN_WALLET = BASE_USER_URL + "addMoneyFromCard";
    public static final String PAY_BOOKING = BASE_USER_URL + "payBookingFromCard";


    //socket Events
    public static final String EVENT_GET_PROVIDER_LOCATION = "get_nearest_provider_location";
    public static final String GET_MY_CELL_ID = "get_my_cellId";
    public static final String JOIN_LIVE_TRACKING = "join_live_tracking";
    public static final String GET_LIVE_TRACKING = "live_tracking";
    public static final String SOCKET_UPDATE_PROVIDER_OFFLINE = "update_provider_offline";

    public static final String SOURCE_MARKER = "icon:http://139.59.55.166/images/source_marker.png";
    public static final String DESTINATION_MARKER = "icon:http://139.59.55.166/images/destiny_marker.png";

    //GOOGLE APIS
    public static final String GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/";
    public static final String GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?";
    public static final String GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?";
    public static final String GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?";
    public static final String GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?";
    public static final String GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?";


    public static String getPostFixUrlForStaticMap(String srclat, String srclng, String deslat, String deslng, String path) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("&markers=" + SOURCE_MARKER + "|");
        stringBuilder.append(srclat).append(",").append(srclng);
        stringBuilder.append("&markers=" + DESTINATION_MARKER + "|");
        stringBuilder.append(deslat).append(",").append(deslng);
        stringBuilder.append("&path=").append(path);
        stringBuilder.append("|color:black|weight:3&style=feature%3Aall%7Celement%3Alabels%7Cvisibility%3Aon%7C&style=feature%3Aadministrative%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Aadministrative%7Celement%3Ageometry%7Cvisibility%3Aon%7C&style=feature%3Aadministrative%7Celement%3Ageometry.fill%7Ccolor%3A0xd6e2e6%7C&style=feature%3Aadministrative%7Celement%3Ageometry.stroke%7Ccolor%3A0xcfd4d5%7C&style=feature%3Aadministrative%7Celement%3Alabels.text.fill%7Ccolor%3A0x7492a8%7C&style=feature%3Aadministrative.neighborhood%7Celement%3Alabels.text.fill%7Clightness%3A25%7C&style=feature%3Alandscape%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Alandscape.man_made%7Celement%3Ageometry.fill%7Ccolor%3A0xdde2e3%7C&style=feature%3Alandscape.man_made%7Celement%3Ageometry.stroke%7Ccolor%3A0xcfd4d5%7C&style=feature%3Alandscape.natural%7Celement%3Ageometry.fill%7Ccolor%3A0xdde2e3%7C&style=feature%3Alandscape.natural%7Celement%3Alabels.text.fill%7Ccolor%3A0x7492a8%7C&style=feature%3Alandscape.natural.terrain%7Celement%3Aall%7Cvisibility%3Aoff%7C&style=feature%3Apoi%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Apoi%7Celement%3Ageometry.fill%7Ccolor%3A0xdde2e3%7C&style=feature%3Apoi%7Celement%3Alabels.text.fill%7Ccolor%3A0x588ca4%7C&style=feature%3Apoi%7Celement%3Alabels.icon%7Csaturation%3A-100%7C&style=feature%3Apoi.park%7Celement%3Ageometry.fill%7Ccolor%3A0xa9de83%7C&style=feature%3Apoi.park%7Celement%3Ageometry.stroke%7Ccolor%3A0xbae6a1%7C&style=feature%3Apoi.sports_complex%7Celement%3Ageometry.fill%7Ccolor%3A0xc6e8b3%7C&style=feature%3Apoi.sports_complex%7Celement%3Ageometry.stroke%7Ccolor%3A0xbae6a1%7C&style=feature%3Aroad%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Aroad%7Celement%3Alabels.text.fill%7Ccolor%3A0x41626b%7C&style=feature%3Aroad%7Celement%3Alabels.icon%7Csaturation%3A-45%7Clightness%3A10%7Cvisibility%3Aon%7C&style=feature%3Aroad.highway%7Celement%3Ageometry.fill%7Ccolor%3A0xc1d1d6%7C&style=feature%3Aroad.highway%7Celement%3Ageometry.stroke%7Ccolor%3A0xa6b5bb%7C&style=feature%3Aroad.highway%7Celement%3Alabels.icon%7Cvisibility%3Aon%7C&style=feature%3Aroad.highway.controlled_access%7Celement%3Ageometry.fill%7Ccolor%3A0x9fb6bd%7C&style=feature%3Aroad.arterial%7Celement%3Ageometry.fill%7Ccolor%3A0xffffff%7C&style=feature%3Aroad.local%7Celement%3Ageometry.fill%7Ccolor%3A0xffffff%7C&style=feature%3Atransit%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Atransit%7Celement%3Alabels.icon%7Csaturation%3A-70%7C&style=feature%3Atransit.line%7Celement%3Ageometry.fill%7Ccolor%3A0xb4cbd4%7C&style=feature%3Atransit.line%7Celement%3Alabels.text.fill%7Ccolor%3A0x588ca4%7C&style=feature%3Atransit.station%7Celement%3Aall%7Cvisibility%3Aoff%7C&style=feature%3Atransit.station%7Celement%3Alabels.text.fill%7Ccolor%3A0x008cb5%7Cvisibility%3Aon%7C&style=feature%3Atransit.station.airport%7Celement%3Ageometry.fill%7Csaturation%3A-100%7Clightness%3A-5%7C&style=feature%3Awater%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Awater%7Celement%3Ageometry.fill%7Ccolor%3A0xa6cbe3%7C&key=AIzaSyDqdPKQqGBAi72_3lh-ewMghl0jzRpkiS8&key=" + AppController.getContext().getResources().getString(R.string.GOOGLE_MAPS_KEY));
        return stringBuilder.toString();

    }

    public static String getPreFixUrlForStaticMap(String width, String height) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(GOOGLE_API_STATIC_MAP_BASE_URL);
        stringBuilder.append("size=" + width + "x" + height);
        return stringBuilder.toString();

    }

}
