package com.app.taxiapp.view.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.LocaleList;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.DialogWithRecyclerviewBinding;
import com.app.taxiapp.helpers.AnimationHelper;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.Constants.ArgumentKeys;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.customviews.dialogs.ChangeLanguageDialog;
import com.app.taxiapp.helpers.interfaces.DialogChanged;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AppConfiguration;
import com.app.taxiapp.model.apiresponsemodel.AuthenticationResponse;
import com.app.taxiapp.model.apiresponsemodel.SocialLoginResponseModel;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.view.activity.MainActivity;
import com.app.taxiapp.view.adapter.BindingAdapter;
import com.app.taxiapp.viewmodel.CommonViewModel;
import com.app.taxiapp.viewmodel.OnBoardingSliderViewModel;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;


public class SignInPageSlidingFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener {

    private AnimationHelper animationHelper;
    View view;
    private Button nextButton;
    private TextView subtitleText;
    private AppConfiguration.Slider slider;
    private TextView titleText;
    private CardView loginParent;
    private CardView phoneNumberPlaceHolder;
    private ImageView backgroundImage;
    private LinearLayout chooseLanguage;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;
    private Bundle bundle;
    private LinearLayout googleParent, facebookParent;
    private LoginManager loginManager;
    private CallbackManager callbackManager;
    private OnBoardingSliderViewModel viewModel;
    private CommonViewModel commonViewModel;
    private String TAG = SignInPageSlidingFragment.class.getSimpleName();
    private GoogleSignInClient mGoogleApiClient;
    private int RC_SIGN_IN = 10;
    SharedHelper sharedHelper;
    boolean booleanExtra;
    String selectedLanguage = "";

    DialogWithRecyclerviewBinding dialogWithRecyclerviewBinding;

    public SignInPageSlidingFragment() {
        // Required empty public constructor
    }


    public static SignInPageSlidingFragment newInstance(AppConfiguration.Slider welcomePageModel, int position, int size, boolean booleanExtra) {
        SignInPageSlidingFragment fragment = new SignInPageSlidingFragment();
        Bundle args = new Bundle();
        args.putParcelable(ArgumentKeys.ARG_WELCOME_MODEL, welcomePageModel);
        args.putInt(ArgumentKeys.ARG_POSITION, position);
        args.putInt(ArgumentKeys.TOTAL_COUNT, size);
        args.putBoolean(Constants.IntentKeys.SKIP, booleanExtra);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            Bundle bundle = getArguments();

            if (bundle != null) {
                if (bundle.getInt(ArgumentKeys.TOTAL_COUNT) == (bundle.getInt(ArgumentKeys.ARG_POSITION) + 1)) {
                    buttonSlide();
                } else {
                    textSlide();
                }
            }

        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
            hideText();
        }
    }

    private void hideText() {
        subtitleText.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.GONE);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_welcome_page, container, false);
        sharedHelper = new SharedHelper(getActivity());
        generateKeyhash();
        viewModel = ViewModelProviders.of(this).get(OnBoardingSliderViewModel.class);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        initViews(rootView);
        bundle = getArguments();
        if (bundle != null) {
            doAnimations();
        }
        setData();
        initListners();
        initFacebookLogin();
        initGoogleLogin();
        return rootView;
    }

    private void initGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = GoogleSignIn.getClient(getActivity(), gso);

    }

    private void googlesign() {
        Intent signInIntent = mGoogleApiClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        mGoogleApiClient.signOut()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        googlesign();
                    }
                });
    }


    private void generateKeyhash() {
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    getActivity().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Utils.log("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception ignored) {
        }
    }

    private void initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();


        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getDataFromFacebook(loginResult);
            }

            @Override
            public void onCancel() {
                Utils.log(TAG, "Cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Utils.log(TAG, error.getLocalizedMessage());
            }
        });
    }

    private void getDataFromFacebook(LoginResult loginResult) {
        viewModel.getDataForFacebook(loginResult).observe(this, new Observer<JSONObject>() {
            @Override
            public void onChanged(JSONObject jsonObject) {
                if (jsonObject != null) {
                    handleFacebookResponse(jsonObject);
                }
            }
        });

    }

    private void handleFacebookResponse(JSONObject jsonObject) {
        Gson gson = new Gson();
        SocialLoginResponseModel model = gson.fromJson(jsonObject.toString(), SocialLoginResponseModel.class);
        checkSocialTokenExistence(Constants.LoginType.FACEBOOK, model);


    }

    private void checkSocialTokenExistence(final String type, final SocialLoginResponseModel model) {
        viewModel.checkSocialTokenExistence(getInputForSocialTokenExistence(type, model.getId())).observe(this, new Observer<AuthenticationResponse>() {
            @Override
            public void onChanged(AuthenticationResponse authenticationResponse) {
                if (authenticationResponse != null) {
                    if (authenticationResponse.getError()) {
                        movePhoneNumberActivity(model, type);
                    } else {
                        handleLoginSuccess(authenticationResponse);
                    }
                }
            }
        });
    }


    private void handleLoginSuccess(AuthenticationResponse authenticationResponse) {
        commonViewModel.setUserData(authenticationResponse);
        moveMainActivity();
    }

    private void moveMainActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private InputForAPI getInputForSocialTokenExistence(String type, String id) {
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.SOCIAL_TOKEN_CHECK);
        try {
            inputForAPI.setJsonObject(getParams(type, id));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return inputForAPI;
    }

    private JSONObject getParams(String type, String id) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.LOGIN_TYPE, type);
        jsonObject.put(Constants.ApiKeys.SOCIAL_TOKEN, id);
        jsonObject.put(Constants.ApiKeys.UUID, Utils.getDeviceId(getActivity()));
        return jsonObject;
    }


    private void doAnimations() {
        animationHelper = new AnimationHelper();
        if (!fragmentResume && fragmentVisible) {
            if (bundle.getInt(ArgumentKeys.TOTAL_COUNT) == (bundle.getInt(ArgumentKeys.ARG_POSITION) + 1)) {
                nextButton.setVisibility(View.VISIBLE);
                buttonSlide();
            } else {
                nextButton.setVisibility(View.GONE);
                textSlide();
            }
        }
        animationHelper.imageSlide(backgroundImage);

    }


    @SuppressLint("ClickableViewAccessibility")
    private void initListners() {
        nextButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("ASdasd", "onTouch: ");

                if (MotionEvent.ACTION_UP == event.getAction()) {
                    onNextClicked();

                }
                return false;
            }
        });

        phoneNumberPlaceHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movePhoneNumberActivity();
            }
        });

        chooseLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeLanguageDialog();
            }
        });

        googleParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGoogleClicked();
            }
        });

        facebookParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFacebookClicked();
            }
        });
    }

    private void showChangeLanguageDialog() {

        ChangeLanguageDialog changeLanguageDialog = new ChangeLanguageDialog(getActivity());
        changeLanguageDialog.setDialogChanged(new DialogChanged() {
            @Override
            public void onLanguageChanged() {
                setPhoneLanguage();
                reloadApp();
            }
        });
        changeLanguageDialog.show();
    }

    private void setPhoneLanguage() {

//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        if (sharedHelper.getSelectedLanguage().length() > 0) {
//            conf.setLocale(new Locale(sharedHelper.getSelectedLanguage().toLowerCase()));
//        } else {
//            conf.setLocale(new Locale("en"));
//        }
//        res.updateConfiguration(conf, dm);

        Resources res = getResources();
        Configuration conf = res.getConfiguration();
        Locale locale;
        if (sharedHelper.getSelectedLanguage().length() > 0) {
            locale = new Locale(sharedHelper.getSelectedLanguage().toLowerCase());
        } else {
            locale = new Locale("en");
        }

        Utils.log("Language :  ",sharedHelper.getSelectedLanguage().toLowerCase());

        Locale.setDefault(locale);
        conf.setLocale(locale);
        assert getActivity() != null;
        getActivity().getApplicationContext().createConfigurationContext(conf);


        DisplayMetrics dm = res.getDisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(new LocaleList(locale));
        } else {
            conf.locale = locale;
        }
        res.updateConfiguration(conf, dm);

    }

    private void reloadApp() {
//        Intent intent = new Intent(getActivity(), SplashActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            if (resultCode == RESULT_OK) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(task);
            }

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void handleSignInResult(Task<GoogleSignInAccount> result) {
        SocialLoginResponseModel socialLoginResponseModel = new SocialLoginResponseModel();
        socialLoginResponseModel.setEmail(result.getResult().getEmail());
        socialLoginResponseModel.setFirst_name(result.getResult().getGivenName());
        socialLoginResponseModel.setLast_name(result.getResult().getFamilyName());
        socialLoginResponseModel.setId(result.getResult().getId());
        checkSocialTokenExistence(Constants.LoginType.GOOGLE, socialLoginResponseModel);
    }


    private void onGoogleClicked() {
        signOut();

    }

    private void onFacebookClicked() {
        LoginManager.getInstance().logOut();
        loginManager.logInWithReadPermissions(SignInPageSlidingFragment.this, Arrays.asList("public_profile", "email"));
    }

    public void onNextClicked() {
//        hideText();
//        showLoginLayout();
//        SignInActivity.slidingViewPager.setPagingEnabled(false);
    }

    private void showLoginLayout() {
        sharedHelper.setTutorialDone("true");
        animationHelper.viewSlideUp(loginParent, 600);

    }


    private void movePhoneNumberActivity(SocialLoginResponseModel model, String type) {
//        Intent intent = new Intent(getActivity(), PhoneNumberActivity.class);
//        intent.putExtra(Constants.IntentKeys.DATA, model);
//        intent.putExtra(Constants.IntentKeys.LOGIN_TYPE, type);
//        getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
//        startActivity(intent);
    }


    private void movePhoneNumberActivity() {
//        Intent intent = new Intent(getActivity(), PhoneNumberActivity.class);
//        intent.putExtra(Constants.IntentKeys.LOGIN_TYPE, Constants.LoginType.NORMAL);
//        getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
//        startActivity(intent);
    }


    private void initViews(View view) {
        titleText = view.findViewById(R.id.titleText);
        subtitleText = view.findViewById(R.id.subtitleText);
        backgroundImage = view.findViewById(R.id.backgroundImage);
        nextButton = view.findViewById(R.id.nextButton);
        loginParent = view.findViewById(R.id.loginParent);
        chooseLanguage = view.findViewById(R.id.chooseLanguage);
        phoneNumberPlaceHolder = view.findViewById(R.id.phoneNumberPlaceHolder);
        facebookParent = view.findViewById(R.id.facebookParent);
        googleParent = view.findViewById(R.id.googleParent);
    }

    private void setData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            slider = bundle.getParcelable(ArgumentKeys.ARG_WELCOME_MODEL);
            if (slider != null) {
                titleText.setText(slider.getTitle());
            }
            if (slider != null) {
                subtitleText.setText(slider.getDescription());
            }
            if (slider != null) {
                BindingAdapter.loadImage(backgroundImage, slider.getImage(), getActivity().getResources().getDrawable(R.drawable.map_placeholder));
            }
        }

        if (bundle.getBoolean(Constants.IntentKeys.SKIP)) {
            onNextClicked();
        }
    }


    private void buttonSlide() {
        subtitleText.setVisibility(View.VISIBLE);
        animationHelper = new AnimationHelper();
        animationHelper.viewSlideUp(nextButton, 600);
    }

    private void textSlide() {
        animationHelper = new AnimationHelper();
        animationHelper.viewSlideUp(subtitleText, 600);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

