package com.app.taxiapp.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ItemHelpListBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.HelpModel;
import com.app.taxiapp.view.activity.HelpActivity;
import com.app.taxiapp.view.activity.HelpDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class HelpAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<HelpModel.Data> helpLists = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public HelpAdapter(Activity context, List<HelpModel.Data> trips) {
        this.context = context;
        this.helpLists = trips;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemHelpListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_help_list, parent, false);
        return new HelpViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final HelpViewHolder viewHolder = (HelpViewHolder) holder;
        viewHolder.binding.tripFareName.setText(helpLists.get(position).getPageName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveHelpDetailsActivity(helpLists.get(position).getId(), helpLists.get(position).getPageName());
            }
        });
    }

    private void moveHelpDetailsActivity(int id, String title) {
        Intent intent = new Intent(context, HelpDetailsActivity.class);
        intent.putExtra(Constants.IntentKeys.TITLE, title);
        intent.putExtra(Constants.IntentKeys.ID, "" + id);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return helpLists.size();
    }


    class HelpViewHolder extends RecyclerView.ViewHolder {
        ItemHelpListBinding binding;

        HelpViewHolder(@NonNull ItemHelpListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
