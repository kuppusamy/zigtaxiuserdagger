package com.app.taxiapp.view.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivityTripDetailsBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.model.apiresponsemodel.TripDetailsModel;
import com.app.taxiapp.networkcall.ImageLoader;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;
import com.app.taxiapp.view.adapter.TripFaresAdapter;
import com.app.taxiapp.viewmodel.TripDetailsViewmodel;

import org.json.JSONException;
import org.json.JSONObject;

public class TripDetailsActivity extends BaseActivity {
    ActivityTripDetailsBinding binding;
    ImageLoader imageLoader;

    RecyclerView tripFareRecyclerView;
    TripDetailsViewmodel tripDetailsViewmodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trip_details);
        initViews();
        getValues();
        initValues();
    }


    private void setRating(float rating, LinearLayout starContent) {

        ImageView imageView = null;
        try {
            starContent.removeAllViewsInLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int j = 1; j <= 5; j++) {
            imageView = new ImageView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(30, 30);
            if (1 < j) {
                layoutParams.setMargins(5, 0, 0, 0);
            }
            imageView.setLayoutParams(layoutParams);
            int s = Math.round(rating);
            if (j <= s) {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.golden_star));
            } else {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.grey_star));
            }

            starContent.addView(imageView);
        }
    }


    private void getValues() {
        tripDetailsViewmodel.getTripDetails(getInputForTripDetails()).observe(this, new Observer<TripDetailsModel>() {
            @Override
            public void onChanged(TripDetailsModel tripDetailsModel) {

                if (!tripDetailsModel.getError()) {
                    setData(tripDetailsModel.getData());
                } else {
                    onFailureResponse(tripDetailsModel.getMsg());
                }


            }
        });
    }



    private void onFailureResponse(String msg) {

        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }

    }

    private void displayError(String error_message) {
        Utils.displayError(findViewById(android.R.id.content), error_message);
    }

    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(TripDetailsActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                getValues();
            }
        });
    }

    private InputForAPI getInputForTripDetails() {
        InputForAPI inputForAPI = new InputForAPI(TripDetailsActivity.this);
        inputForAPI.setUrl(UrlHelper.GET_TRIP_DETAILS);
        inputForAPI.setJsonObject(getParamsForTripDetails());
        inputForAPI.setHeaders(ApiCall.getHeaders(TripDetailsActivity.this));
        return inputForAPI;
    }

    private JSONObject getParamsForTripDetails() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.ApiKeys.BOOKING_NUMBER, getIntent().getStringExtra(Constants.IntentKeys.BOOKING_NUMBER));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void initValues() {
        imageLoader = new ImageLoader(TripDetailsActivity.this);
        tripDetailsViewmodel = ViewModelProviders.of(this).get(TripDetailsViewmodel.class);
    }

    private void setData(TripDetailsModel.Data tripDetailsModel) {
        binding.setTrips(tripDetailsModel);
        initRecyclerView(tripDetailsModel);
        binding.bookingDate.setText(Utils.getConvertedTime(tripDetailsModel.getCreatedTime()));
        imageLoader.load(getIntent().getStringExtra(Constants.IntentKeys.STATIC_MAP_URL), binding.googleStaticMap, getResources().getDrawable(R.drawable.map_placeholder));
        if (tripDetailsModel.getStatus().equalsIgnoreCase(Constants.BookingStatus.CANCELLED)) {
            binding.providerInformationLayout.setVisibility(View.GONE);
            binding.receiptLayout.setVisibility(View.GONE);
        } else {

            if (tripDetailsModel.getProviderInfo() == null) {
                binding.providerInformationLayout.setVisibility(View.GONE);
                binding.receiptLayout.setVisibility(View.GONE);
            } else {
                binding.providerInformationLayout.setVisibility(View.VISIBLE);
                binding.receiptLayout.setVisibility(View.VISIBLE);
                setRating(Float.parseFloat(tripDetailsModel.getProviderInfo().getRating()), binding.ratingParent);
            }

        }
    }

    private void initViews() {
        tripFareRecyclerView = binding.tripFareRecyclerView;
        tripDetailsViewmodel = ViewModelProviders.of(this).get(TripDetailsViewmodel.class);
    }

    private void initRecyclerView(TripDetailsModel.Data tripDetailsModel) {
        if (tripDetailsModel != null) {
            TripFaresAdapter tripFareAdapter = new TripFaresAdapter(TripDetailsActivity.this, tripDetailsModel.getReceipt());
            tripFareRecyclerView.setLayoutManager(new LinearLayoutManager(TripDetailsActivity.this, RecyclerView.VERTICAL, false));
            tripFareRecyclerView.setNestedScrollingEnabled(false);
            tripFareRecyclerView.setAdapter(tripFareAdapter);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onCloseIconClicked(View view) {
        onBackPressed();
    }
}
