package com.app.taxiapp.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivitySearchPlacesBinding;
import com.app.taxiapp.helpers.AnimationHelper;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.helpers.interfaces.onClickListener;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.AutoCompleteAddressResponseModel;
import com.app.taxiapp.model.apiresponsemodel.GetLatLongFromIdResponseModel;
import com.app.taxiapp.model.apiresponsemodel.GoogleLocationResponseModel.AddressComponent;
import com.app.taxiapp.model.apiresponsemodel.GoogleLocationResponseModel.GooglePlaceResponseModel;
import com.app.taxiapp.model.apiresponsemodel.autocompleteresponse.Prediction;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;
import com.app.taxiapp.view.adapter.AutoCompleteAdapter;
import com.app.taxiapp.viewmodel.HomePageViewModel;
import com.app.taxiapp.viewmodel.MapViewModel;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.app.taxiapp.helpers.Constants.IntentPermissionCode.COARSE_LOCATION_PERMISSIONS;

public class SearchPlacesActivity extends BaseActivity implements OnMapReadyCallback {
    MapViewModel mapViewModel;
    HomePageViewModel viewModel;
    int defaultLayoutTransitionScreens = 500;

    ActivitySearchPlacesBinding searchPlacesBinding;
    RecyclerView autoCompleteList;
    private AutoCompleteAdapter autoCompleteAdapter;
    private String selectedLat;
    private String selectedLong;
    private String selectedAddressName;
    private String selectedAddressText;
    private double currentLatitude = 0, currentLongitude = 0;
    private String receivedLat, receivedLong;
    int requestCode;
    SharedHelper sharedHelper;
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    private int value = 0;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback locationCallback;
    LocationRequest locationRequest;
    private String TAG = SearchPlacesActivity.class.getSimpleName();
    boolean isPickFromMapEnabled = false;
    AnimationHelper animationHelper;
    boolean isUserTyping = true;
    List<AddressComponent> addressComponent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchPlacesBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_places);
        sharedHelper = new SharedHelper(SearchPlacesActivity.this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        animationHelper = new AnimationHelper();
        locationListner();
        getIntentData();
        requestCode = getIntent().getIntExtra(Constants.IntentKeys.REQUEST_CODE, 0);
        mapViewModel = ViewModelProviders.of(this).get(MapViewModel.class);
        viewModel = ViewModelProviders.of(this).get(HomePageViewModel.class);
        autoCompleteList = searchPlacesBinding.autoCompleteList;
        if (requestCode == Constants.IntentPermissionCode.SOURCE_REQUEST_CODE) {
            setHeadingText(getResources().getString(R.string.choose_pick_up_location));
        } else {
            setHeadingText(getResources().getString(R.string.choose_destination_location));
        }
        locationCheck();
        setCachedData();
        initViews();
        initListeners();
    }

    private void initViews() {
        setUpMap();

    }

    private void zoomToMyLocation(double latitude, double longitude) {
        CameraUpdate center = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15);
        mMap.animateCamera(center);
        if (isPickFromMapEnabled) {
            getCompleteAddress(latitude, longitude);
        }
    }

    public void setUpMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

    }

    public void promptEnableGpsDialog() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                getLastKnownLocation();
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(SearchPlacesActivity.this, Constants.IntentPermissionCode.REQUEST_LOCATION);
                    } catch (IntentSender.SendIntentException sendEx) {
                        sendEx.printStackTrace();
                    }
                }
            }
        });
    }

    private void getLastKnownLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                fusedLocationClient.requestLocationUpdates(mLocationRequest, null);
                Task locationResult = fusedLocationClient.getLastLocation();
                locationResult.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {

                            Location location = (Location) task.getResult();
                            if (location != null) {
                                printLog("Last location retrieved not null ");
                                currentLatitude = location.getLatitude();
                                currentLongitude = location.getLongitude();

                            } else {
                                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    Activity#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for Activity#requestPermissions for more details.
                                    return;
                                }
                                fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            }
                        }
                    }
                });
            }

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.IntentPermissionCode.REQUEST_LOCATION) {
            switch (resultCode) {
                case RESULT_OK:
                    getLastKnownLocation();
                    break;
                case RESULT_CANCELED:
                    statusCheck();
                    break;
                default:
                    break;
            }
        }
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLastKnownLocation();
        }
    }

    private void showMyLocationButton() {
//        binding.mainPageContentLayout.searchLayout.myLocationButton.setVisibility(View.VISIBLE);
    }


    public void showMyLocationIndicatorinMap() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        showMyLocationButton();
        if (mMap != null) {
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.setMyLocationEnabled(true);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        showMyLocationIndicatorinMap();
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setBuildingsEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);

        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (value == 0) {
                    printLog("mapGPS");
                    LatLng myLocation;
                    myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    value++;
                    if (isPickFromMapEnabled) {
                        isUserTyping = false;
                        getCompleteAddress(myLocation.latitude, myLocation.longitude);
                    }
                }
            }
        });

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (isPickFromMapEnabled) {
                    searchPlacesBinding.searchEditText.setText("");
                    searchPlacesBinding.searchEditText.setHint(getResources().getString(R.string.fetching));
                    isUserTyping = false;
                    double mylat = mMap.getCameraPosition().target.latitude;
                    double mylon = mMap.getCameraPosition().target.longitude;
                    getCompleteAddress(mylat, mylon);
                }

            }
        });

        mMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(this, R.raw.google_map_style));


        zoomToMyLocation(Double.parseDouble(receivedLat), Double.parseDouble(receivedLong));
    }


    public void printLog(String log) {
        Utils.log(TAG, log);
    }


    private void locationListner() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);
        locationCallback = new
                LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if (locationResult == null) {
                            return;
                        }
                        for (Location location : locationResult.getLocations()) {
                            if (location != null) {
                                if (currentLatitude == 0 || currentLongitude == 0) {
                                    getLastKnownLocation();
                                }
                            }
                        }
                    }
                };

    }


    private void locationCheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, COARSE_LOCATION_PERMISSIONS);
        } else {
            promptEnableGpsDialog();
        }
    }


    private void setCachedData() {
        List<Prediction> predictionList = sharedHelper.getRecentSearchLists();
        Collections.reverse(predictionList);
        if (predictionList != null)
            if (predictionList.size() > 0) {
                setAdapters(predictionList, "recent");
            }
    }

    private void getIntentData() {
        receivedLat = getIntent().getStringExtra(Constants.ApiKeys.LATITUDE);
        receivedLong = getIntent().getStringExtra(Constants.ApiKeys.LONGITUDE);
    }

    private void setHeadingText(String string) {
        searchPlacesBinding.toolbarTitle.setText(string);
    }

    private void searchPlaces() {
        String url = getPlaceAutoCompleteUrl(searchPlacesBinding.searchEditText.getText().toString(), receivedLat, receivedLong);
        InputForAPI inputForAPI = new InputForAPI(SearchPlacesActivity.this);
        inputForAPI.setUrl(url);
        inputForAPI.setFile(null);
        inputForAPI.setHeaders(new HashMap<String, String>());
        getSearchDetails(inputForAPI);
    }

    private String getPlaceAutoCompleteUrl(String toString, String currentLat, String currentLong) {

        return mapViewModel.getAutoCompleteUrl(toString, currentLat, currentLong);
    }


    private void getCompleteAddress(double latitude, double longitude) {
        String url = buildUrl(latitude, longitude);
        InputForAPI inputForAPI = new InputForAPI(SearchPlacesActivity.this);
        inputForAPI.setUrl(url);
        getAddressDetails(inputForAPI);

    }


    private String buildUrl(double latitude, double longitude) {
        return viewModel.getAddresDetailsUrl(latitude, longitude);
    }


    private void getAddressDetails(InputForAPI inputForAPI) {
        viewModel.getAddresDetails(inputForAPI).observe(
                this, new Observer<GooglePlaceResponseModel>() {
                    @Override
                    public void onChanged(@Nullable GooglePlaceResponseModel googlePlaceResponseModel) {
                        if (googlePlaceResponseModel != null) {
                            try {
                                if (!googlePlaceResponseModel.getResults().isEmpty()) {
                                    if (googlePlaceResponseModel.getResults().size() > 0) {
                                        handleAddressResponse(googlePlaceResponseModel);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        );
    }

    private void handleAddressResponse(GooglePlaceResponseModel googlePlaceResponseModel) {
        addressComponent = googlePlaceResponseModel.getResults().get(0).getAddressComponents();
        setAddressText(googlePlaceResponseModel.getResults().get(0).getFormattedAddress());

    }

    private void setAddressText(String formattedAddress) {
        searchPlacesBinding.searchEditText.setText(formattedAddress);
        isUserTyping = true;
    }


    private void animateCameraToLocation(double latitude, double longitude) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude));
        mMap.animateCamera(center);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == COARSE_LOCATION_PERMISSIONS) {
            try {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    promptEnableGpsDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void getSearchDetails(InputForAPI inputForAPI) {
        mapViewModel.autoCompleteAddress(inputForAPI).observe(
                this, new Observer<AutoCompleteAddressResponseModel>() {
                    @Override
                    public void onChanged(@Nullable AutoCompleteAddressResponseModel autoCompleteAddressResponseModel) {
                        if (autoCompleteAddressResponseModel != null) {
                            if (autoCompleteAddressResponseModel.getError()) {
                                Utils.displayError(findViewById(android.R.id.content), autoCompleteAddressResponseModel.getStatus());
                            } else {
                                if (autoCompleteAddressResponseModel.getPredictions().size() > 0) {
                                    if (requestCode == Constants.IntentPermissionCode.SOURCE_REQUEST_CODE) {
                                        setAdapters(autoCompleteAddressResponseModel.getPredictions(), "from");
                                    } else {
                                        setAdapters(autoCompleteAddressResponseModel.getPredictions(), "to");
                                    }
                                }
                            }
                        }
                    }

                });
    }


    private void setAdapters(final List<Prediction> predictions, String recent) {
        if (predictions.size() > 0) {
            autoCompleteList.setVisibility(View.VISIBLE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SearchPlacesActivity.this, RecyclerView.VERTICAL, false);
            autoCompleteList.setLayoutManager(linearLayoutManager);
            autoCompleteAdapter = new AutoCompleteAdapter(SearchPlacesActivity.this, predictions, recent);
            autoCompleteList.setAdapter(autoCompleteAdapter);
            autoCompleteAdapter.notifyDataSetChanged();
            autoCompleteAdapter.setOnClickListner(new onClickListener() {
                @Override
                public void onClicked(final int position) {
                    savePredictionsToPreference(predictions.get(position));
                    String url = mapViewModel.getPlaceDetails(predictions.get(position).getPlaceId());
                    InputForAPI inputForAPI = new InputForAPI(SearchPlacesActivity.this);
                    inputForAPI.setUrl(url);
                    inputForAPI.setFile(null);
                    inputForAPI.setHeaders(new HashMap<String, String>());
                    getLatLagDetails(inputForAPI, position, predictions);
                }
            });
        } else {
            autoCompleteList.setVisibility(View.GONE);
        }
    }

    private void savePredictionsToPreference(Prediction prediction) {
        sharedHelper.addrecentSearch(prediction);
    }


    private void getLatLagDetails(InputForAPI inputForAPI, final int position, final List<Prediction> predictions) {
        mapViewModel.getLatLagAddress(inputForAPI).observe(
                this, new Observer<GetLatLongFromIdResponseModel>() {
                    @Override
                    public void onChanged(@Nullable GetLatLongFromIdResponseModel getLatLongFromIdResponseModel) {
                        if (getLatLongFromIdResponseModel != null) {
                            if (getLatLongFromIdResponseModel.getStatus().equalsIgnoreCase("OK")) {
                                searchPlacesBinding.searchEditText.setText("");
                                String areaname = predictions.get(position).getDescription();
                                String desc_detail = predictions.get(position).getTerms().get(0).getValue();
                                selectedLat = String.valueOf(getLatLongFromIdResponseModel.getResult().getGeometry().getLocation().getLat());
                                selectedLong = String.valueOf(getLatLongFromIdResponseModel.getResult().getGeometry().getLocation().getLng());
                                selectedAddressName = areaname;
                                selectedAddressText = desc_detail;
                                finishIntent(selectedLat, selectedLong, selectedAddressName, viewModel.getCountryShortCode(getLatLongFromIdResponseModel.getResult().getAddressComponents()));
                            } else {
                                handleFailureResponse(getLatLongFromIdResponseModel.getStatus());
                            }
                        }

                    }
                }
        );

    }

    private void handleFailureResponse(String msg) {
        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }
    }

    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(SearchPlacesActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                noInternetDialog.dismiss();
            }
        });
    }


    private void displayError(String error_message) {
        Utils.displayError(findViewById(android.R.id.content), error_message);
    }


    private void finishIntent(String selectedLat, String selectedLong, String selectedAddressName, String countryShortCode) {
        Intent intent = new Intent();
        intent.putExtra(Constants.IntentKeys.LATITUDE, selectedLat);
        intent.putExtra(Constants.IntentKeys.LONGITUDE, selectedLong);
        intent.putExtra(Constants.IntentKeys.AREA_NAME, selectedAddressName);
        intent.putExtra(Constants.IntentKeys.SHORT_CODE, countryShortCode);
        setResult(RESULT_OK, intent);
        finish();
    }


    private void initListeners() {
        searchPlacesBinding.searchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    isPickFromMapEnabled = false;
                    isUserTyping = true;
                    searchPlacesBinding.recyclerParent.setVisibility(View.VISIBLE);
                }
            }
        });
        searchPlacesBinding.searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override

            public void afterTextChanged(Editable s) {
                if (!isPickFromMapEnabled) {
                    if (isUserTyping) {
                        searchPlacesBinding.recyclerParent.setVisibility(View.VISIBLE);
                        if (searchPlacesBinding.searchEditText.getText().toString().length() > 3) {
                            searchPlaces();
                        } else {
                            setCachedData();
                        }
                    }
                }
            }
        });
    }

    public void onCloseIconClicked(View view) {
        finish();
    }

    public void onPickFromMapClicked(View view) {
        Utils.dismissKeyboard(SearchPlacesActivity.this);
        isPickFromMapEnabled = true;
        searchPlacesBinding.recyclerParent.setVisibility(View.GONE);
        searchPlacesBinding.searchEditText.clearFocus();
        try {
            double mylat = mMap.getMyLocation().getLatitude();
            double mylon = mMap.getMyLocation().getLongitude();
            getCompleteAddress(mylat, mylon);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onConfirmBookingClicked(View view) {

        try {
            selectedLat = "" + mMap.getCameraPosition().target.latitude;
            selectedLong = "" + mMap.getCameraPosition().target.longitude;
            selectedAddressName = searchPlacesBinding.searchEditText.getText().toString();

            if (selectedAddressName.length() == 0 || selectedAddressName.equalsIgnoreCase(getResources().getString(R.string.fetching))) {
                Utils.displayError(findViewById(android.R.id.content), getResources().getString(R.string.address_not_fetched));
            } else {
                finishIntent(selectedLat, selectedLong, selectedAddressName, viewModel.getCountryShortCode(addressComponent));
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utils.displayError(findViewById(android.R.id.content), getResources().getString(R.string.fetching_location));
        }

    }


    public void onClearTextclicked(View view) {
        searchPlacesBinding.searchEditText.setText("");
    }
}
