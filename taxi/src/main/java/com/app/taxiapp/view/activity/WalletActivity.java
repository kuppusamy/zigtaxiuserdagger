package com.app.taxiapp.view.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivityWalletBinding;
import com.app.taxiapp.databinding.DialogAddMoneyBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.ProgressBarDialog;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.model.apiresponsemodel.WalletResponseModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;
import com.app.taxiapp.view.adapter.TransactionListAdapter;
import com.app.taxiapp.viewmodel.WalletViewModel;

import java.util.List;

public class WalletActivity extends BaseActivity {
    DialogAddMoneyBinding dialogBinding;
    private Dialog dialog;
    WalletViewModel viewModel;
    String availableBalance;
    ActivityWalletBinding walletBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        walletBinding = DataBindingUtil.setContentView(this, R.layout.activity_wallet);
        viewModel = ViewModelProviders.of(this).get(WalletViewModel.class);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getWalletDetails();
    }


    private void getWalletDetails() {
        showProgress();
        viewModel.getWalletDetails(getWalletDetailsInput()).observe(this, new Observer<WalletResponseModel>() {
            @Override
            public void onChanged(WalletResponseModel walletResponseModel) {
                dismissProgress();
                if (walletResponseModel != null) {
                    if (walletResponseModel.getError()) {
                        handleFailureResponse(walletResponseModel.getMsg());
                    } else {
                        walletBinding.setWalletDetails(walletResponseModel.getData());
                        availableBalance = walletResponseModel.getData().getBalance();
                        setTransactionAdapter(walletResponseModel.getData().getTransaction());
                    }
                }
            }
        });
    }

    private void setTransactionAdapter(List<WalletResponseModel.Transaction> transaction) {
        if (transaction.size() > 0) {
            walletBinding.emptyLayout.setVisibility(View.GONE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(WalletActivity.this, RecyclerView.VERTICAL, false);
            TransactionListAdapter transactionListAdapter = new TransactionListAdapter(WalletActivity.this, transaction);
            walletBinding.transactionRecyclerView.setLayoutManager(linearLayoutManager);
            walletBinding.transactionRecyclerView.setAdapter(transactionListAdapter);
        } else {
            walletBinding.emptyLayout.setVisibility(View.VISIBLE);
        }
    }

    private void handleFailureResponse(String msg) {
        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }
    }

    private InputForAPI getWalletDetailsInput() {
        InputForAPI inputForAPI = new InputForAPI(WalletActivity.this);
        inputForAPI.setUrl(UrlHelper.MY_WALLET_DETAILS);
        inputForAPI.setHeaders(ApiCall.getHeaders(WalletActivity.this));
        return inputForAPI;
    }

    public void onAddMoneyClicked(View view) {
        showAddMoneyDialog();
    }


    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(WalletActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                getWalletDetails();
                noInternetDialog.dismiss();
            }
        });
    }

    private void displayError(String msg) {
        Utils.displayError(findViewById(android.R.id.content), msg);
    }

    private void displayDialogError(String msg) {
        Utils.displayError(dialog.findViewById(android.R.id.content), msg);
    }


    private void showAddMoneyDialog() {
        dialog = new Dialog(WalletActivity.this, R.style.FullScreenDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialogBinding = DataBindingUtil.inflate(LayoutInflater.from(WalletActivity.this), R.layout.dialog_add_money, null, false);
        dialog.setContentView(dialogBinding.getRoot());
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.setGravity(Gravity.BOTTOM);
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        dialog.show();
        dialogBinding.availableBalance.setText(availableBalance);
    }

    public void onCloseIconClicked(View view) {
        dialog.dismiss();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_to_bottom);
    }


    public void onBackPressed(View view) {
        onBackPressed();
    }

    public void onChoosePaymentClicked(View view) {
        if (dialogBinding.inputText.getText().toString().length() > 0) {
            if (Integer.parseInt(dialogBinding.inputText.getText().toString()) > 0) {
                dialog.dismiss();
                moveChoosePaymentActivity(Integer.parseInt(dialogBinding.inputText.getText().toString()));
            } else {
                displayDialogError(getResources().getString(R.string.enter_valid_amount));
            }
        } else {
            displayDialogError(getResources().getString(R.string.enter_valid_amount));
        }
    }

    private void moveChoosePaymentActivity(int amount) {
        Intent intent = new Intent(WalletActivity.this, ChoosePaymentActivity.class);
        intent.putExtra(Constants.IntentKeys.TYPE, "wallet");
        intent.putExtra(Constants.IntentKeys.DATA, "" + amount);
        startActivity(intent);
    }
}
