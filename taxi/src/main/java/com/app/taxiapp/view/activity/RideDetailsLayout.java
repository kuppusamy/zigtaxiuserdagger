package com.app.taxiapp.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivityRideDetailsLayoutBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.RideTypeResponseModel;

public class RideDetailsLayout extends AppCompatActivity {

    RideTypeResponseModel.Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityRideDetailsLayoutBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_ride_details_layout);
        data = getIntent().getParcelableExtra(Constants.IntentKeys.RIDE);
        binding.setRide(data);
    }

    public void onDoneClicked(View view) {
        onBackPressed();
    }
}
