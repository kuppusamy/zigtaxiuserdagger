package com.app.taxiapp.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ItemLanguageListBinding;
import com.app.taxiapp.helpers.interfaces.onClickListener;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.LanguageModel;

import java.util.List;

public class LanguageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<LanguageModel.LanguageLists> languageLists;
    private LayoutInflater layoutInflater;
    private SharedHelper sharedHelper;

    private com.app.taxiapp.helpers.interfaces.onClickListener onClickListener;

    public LanguageListAdapter(Context context, List<LanguageModel.LanguageLists> languageLists) {
        this.languageLists = languageLists;
        sharedHelper = new SharedHelper(context);
    }

    public void setOnClickListner(onClickListener onClickListner) {
        this.onClickListener = onClickListner;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemLanguageListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_language_list, parent, false);
        return new VehcileViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final VehcileViewHolder viewHolder = (VehcileViewHolder) holder;
        viewHolder.binding.languageName.setText(languageLists.get(position).getDisplayName());

        if (languageLists.get(position).getShortCode().equalsIgnoreCase(sharedHelper.getSelectedLanguage())) {
            viewHolder.binding.isSelected.setVisibility(View.VISIBLE);
            viewHolder.binding.isNotSelected.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.binding.isSelected.setVisibility(View.INVISIBLE);
            viewHolder.binding.isNotSelected.setVisibility(View.VISIBLE);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedHelper.setSelectedLanguage(languageLists.get(position).getShortCode());
                onClickListener.onClicked(position);
                notifyDataSetChanged();
            }
        });


        if (position == getItemCount() - 1) {
            viewHolder.binding.divider.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.binding.divider.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public int getItemCount() {
        return languageLists.size();
    }


    class VehcileViewHolder extends RecyclerView.ViewHolder {
        ItemLanguageListBinding binding;

        VehcileViewHolder(@NonNull ItemLanguageListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
