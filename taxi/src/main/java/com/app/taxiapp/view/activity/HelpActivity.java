package com.app.taxiapp.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivityHelpBinding;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.model.apiresponsemodel.HelpModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;
import com.app.taxiapp.view.adapter.HelpAdapter;
import com.app.taxiapp.viewmodel.HelpViewModel;

import java.util.ArrayList;
import java.util.List;

public class HelpActivity extends BaseActivity {

    private ActivityHelpBinding binding;
    HelpViewModel helpViewModel;
    private HelpAdapter helpAdapter;
    List<HelpModel.Data> helpModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_help);
        helpViewModel = ViewModelProviders.of(this).get(HelpViewModel.class);
        getHelpData();
    }

    private void getHelpData() {
        helpViewModel.getHelpLists(getInputForApi()).observe(this, new Observer<HelpModel>() {
            @Override
            public void onChanged(HelpModel helpModel) {
                if (helpModel != null) {
                    if (!helpModel.getError()) {
                        helpModels = helpModel.getData();
                        initAdapter();
                    } else {
                        onFailureResponse(helpModel.getMsg());
                    }
                }
            }
        });

    }

    private void onFailureResponse(String msg) {

        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }

    }

    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(HelpActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                getHelpData();
            }
        });
    }

    private void displayError(String error_message) {
        Utils.displayError(findViewById(android.R.id.content), error_message);
    }


    private InputForAPI getInputForApi() {
        InputForAPI inputForAPI = new InputForAPI(HelpActivity.this);
        inputForAPI.setUrl(UrlHelper.GET_HELP);
        inputForAPI.setHeaders(ApiCall.getHeaders(HelpActivity.this));
        return inputForAPI;
    }

    private void initAdapter() {

        helpAdapter = new HelpAdapter(HelpActivity.this, helpModels);
        binding.helpRecyclerView.setLayoutManager(new LinearLayoutManager(HelpActivity.this, RecyclerView.VERTICAL, false));
        binding.helpRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.helpRecyclerView.setNestedScrollingEnabled(false);
        binding.helpRecyclerView.setAdapter(helpAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_to_bottom);
    }

    public void onCloseIconClicked(View view) {
        onBackPressed();
    }
}
