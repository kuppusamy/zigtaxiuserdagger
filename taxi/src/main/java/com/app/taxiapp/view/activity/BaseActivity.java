package com.app.taxiapp.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.AlertDialogCustomBinding;
import com.app.taxiapp.databinding.DialogWalletSuccesLayoutBinding;
import com.app.taxiapp.helpers.ProgressBarDialog;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;

import dagger.android.support.DaggerAppCompatActivity;


public class BaseActivity extends AppCompatActivity {
    public NoInternetDialog noInternetDialog;
    public ProgressBarDialog progressBarDialog;
    public AlertDialogCustomBinding alertDialogBinding;
    public Dialog successDialog;
    DialogWalletSuccesLayoutBinding succesLayoutBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createSuccessDialog();
    }

    private void createSuccessDialog() {
        successDialog = new Dialog(BaseActivity.this);
        successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        successDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        successDialog.setCancelable(false);
        successDialog.setCanceledOnTouchOutside(false);
        succesLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(BaseActivity.this), R.layout.dialog_wallet_succes_layout, null, false);
        successDialog.setContentView(succesLayoutBinding.getRoot());

        Window window = successDialog.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        succesLayoutBinding.closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();
            }
        });

    }

    public void dismissProgress() {
        progressBarDialog.dismiss();

    }

    public void showProgress() {
        progressBarDialog = new ProgressBarDialog(BaseActivity.this);
        progressBarDialog.show();
    }

    public Dialog getConfirmationDialog() {
        final Dialog dialog = new Dialog(BaseActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        alertDialogBinding = DataBindingUtil.inflate(LayoutInflater.from(BaseActivity.this), R.layout.alert_dialog_custom, null, false);
        dialog.setContentView(alertDialogBinding.getRoot());
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        dialog.show();

        return dialog;

    }


    public void showSuccessDialog(String description) {
        succesLayoutBinding.amountText.setText(description);
        successDialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                succesLayoutBinding.animationView.playAnimation();
            }
        }, 400);
    }

    public void dismissSuccessDialog() {
        successDialog.dismiss();
    }


}
