package com.app.taxiapp.view.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.app.taxiapp.R;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.model.apiresponsemodel.HelpDetailModel;
import com.app.taxiapp.model.apiresponsemodel.HelpModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;
import com.app.taxiapp.viewmodel.CommonViewModel;
import com.app.taxiapp.viewmodel.HelpViewModel;
import com.google.android.gms.common.internal.service.Common;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class HelpDetailsActivity extends BaseActivity {
    WebView webView;
    TextView titleText;
    HelpViewModel helpViewModel;
    CommonViewModel commonViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_details);
        helpViewModel = ViewModelProviders.of(this).get(HelpViewModel.class);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        webView = findViewById(R.id.webView);
        titleText = findViewById(R.id.titleText);
        titleText.setText(getIntent().getStringExtra(Constants.IntentKeys.TITLE));
        getData();
    }

    private void getData() {
        helpViewModel.getHelpDetail(getInputForAPi()).observe(this, new Observer<HelpDetailModel>() {
            @Override
            public void onChanged(HelpDetailModel helpModel) {
                if (helpModel != null) {
                    if (!helpModel.getError()) {
                        loadUrl(helpModel.getData().getContent());
                    } else {
                        onFailureResponse(helpModel.getMsg());
                    }
                }
            }
        });
    }

    private InputForAPI getInputForAPi() {
        InputForAPI inputForAPI = new InputForAPI(HelpDetailsActivity.this);
        inputForAPI.setUrl(commonViewModel.getCombinedStrings(UrlHelper.GET_HELP, "?id=" + getIntent().getStringExtra(Constants.IntentKeys.ID)));
        inputForAPI.setHeaders(ApiCall.getHeaders(HelpDetailsActivity.this));
        return inputForAPI;
    }

    private void loadUrl(String content) {
        webView.loadData(content, "text/html", "UTF-8");
    }


    private void onFailureResponse(String msg) {

        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }

    }

    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(HelpDetailsActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                getData();
            }
        });
    }

    private void displayError(String error_message) {
        Utils.displayError(findViewById(android.R.id.content), error_message);
    }


    public void onCloseIconClicked(View view) {
        onBackPressed();
    }
}
