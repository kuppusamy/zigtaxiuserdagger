package com.app.taxiapp.view.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.LocaleList;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GestureDetectorCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.taxiapp.AppController;
import com.app.taxiapp.BuildConfig;
import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivityMainBinding;
import com.app.taxiapp.databinding.DialogCancelReasonBinding;
import com.app.taxiapp.databinding.HomePageSearchLayoutBinding;
import com.app.taxiapp.helpers.AnimationHelper;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.ImageUtils;
import com.app.taxiapp.helpers.MapRippleHelper;
import com.app.taxiapp.helpers.OnSwipeListener;
import com.app.taxiapp.helpers.RouteGenerate.Route;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.customviews.dialogs.ChangeLanguageDialog;
import com.app.taxiapp.helpers.interfaces.DialogChanged;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.helpers.interfaces.onClickListener;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.ProviderOfflineResponse;
import com.app.taxiapp.model.apiresponsemodel.CancelReasonsModel;
import com.app.taxiapp.model.apiresponsemodel.CouponVerifyResponseModel;
import com.app.taxiapp.model.apiresponsemodel.CurrentBookingResponseModel;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.model.apiresponsemodel.GoogleLocationResponseModel.GooglePlaceResponseModel;
import com.app.taxiapp.model.apiresponsemodel.RideTypeResponseModel;
import com.app.taxiapp.model.socketresponsemodel.GetProviderLocationResponse;
import com.app.taxiapp.model.socketresponsemodel.ProviderDetailResponse;
import com.app.taxiapp.networkcall.ImageLoader;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;
import com.app.taxiapp.view.adapter.BindingAdapter;
import com.app.taxiapp.view.adapter.CancelReasonAdapter;
import com.app.taxiapp.view.adapter.RideTypeAdapter;
import com.app.taxiapp.viewmodel.CommonViewModel;
import com.app.taxiapp.viewmodel.HomePageViewModel;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.functions.Consumer;
import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.app.taxiapp.helpers.Constants.IntentPermissionCode.COARSE_LOCATION_PERMISSIONS;
import static com.app.taxiapp.helpers.Constants.IntentPermissionCode.DESTINATION_REQUEST_CODE;
import static com.app.taxiapp.helpers.Constants.IntentPermissionCode.SOURCE_REQUEST_CODE;
import static com.google.android.gms.maps.model.JointType.ROUND;

public class MainActivity extends BaseActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    ActivityMainBinding binding;
    HomePageViewModel viewModel;
    private int value = 0;
    public int currentState;
    HomePageSearchLayoutBinding searchLayout;
    private double currentLatitude = 0, currentLongitude = 0;
    private DrawerLayout mDrawerLayout;
    private static String TAG = MainActivity.class.getSimpleName();
    private int revealX;
    private int revealY;
    List<Marker> markers = new ArrayList<>();
    private Socket socket;
    CommonViewModel commonViewModel;
    ImageLoader imageLoader;
    TextView sourceText;
    TextView destinationText;
    boolean isSourceSelected = false;
    boolean isDestinationSelected = true;
    double srclat = 0, srclng = 0, deslat = 0, deslng = 0;
    private LatLng sourceLatLng, destinationLatLng;
    private Polyline blackPolyline;
    private Polyline blackTrackingPolyline;
    private RideTypeAdapter rideTypeAdapter;
    private RecyclerView rideTypeRecyclerView;
    AnimationHelper animationHelper;
    String srcCountryCode = "";
    List<RideTypeResponseModel.Data> initialRideTypeList = new ArrayList<>();
    List<RideTypeResponseModel.Data> remainingRideTypeList = new ArrayList<>();
    int defaultLayoutTransitionScreens = 500;
    String selectedrideType = "";
    String selectedbookingType = "";
    boolean isSearchDetailsExpanded = false;
    boolean isBookingAcceptedDetailsExpanded = false;
    private GestureDetectorCompat detector;
    private RideTypeResponseModel.Data selectedRideTypeDetails;
    private GestureDetectorCompat bookingAccepteddetector;
    MutableLiveData<CurrentBookingResponseModel> ongoingBookingResponse;
    public String currentBookingId = "";
    boolean isTrackRouteGenerating = false;
    boolean isJoinedRoom = false;
    private FusedLocationProviderClient fusedLocationClient;
    int fullServiceTypeCount = 0;
    MutableLiveData<GetProviderLocationResponse> mutableLocationResponseData;
    MutableLiveData<String> statusValue;
    OnSwipeListener onSwipeListener = new OnSwipeListener() {

        @Override
        public boolean onSwipe(Direction direction) {
            // Possible implementation
            if (direction == Direction.left || direction == Direction.right) {
                // Do something COOL like animation or whatever you want
                // Refer to your view if needed using a global reference
                return true;
            } else if (direction == Direction.up) {
                onSearchSwipeUp();
                return true;
            } else if (direction == Direction.down) {
                onSearchSwipeDown();
            }

            return super.onSwipe(direction);
        }
    };
    OnSwipeListener onSwipeAcceptedListener = new OnSwipeListener() {

        @Override
        public boolean onSwipe(Direction direction) {
            // Possible implementation
            if (direction == Direction.left || direction == Direction.right) {
                // Do something COOL like animation or whatever you want
                // Refer to your view if needed using a global reference
                return true;
            } else if (direction == Direction.up) {
                onBookingAcceptedSwipeUp();
                return true;
            } else if (direction == Direction.down) {
                onBookingAcceptedSwipeDown();
            }

            return super.onSwipe(direction);
        }
    };

    private Emitter.Listener trackingListner;
    private SharedHelper sharedHelper;
    private List<LatLng> currentRoute = new ArrayList<>();
    private Marker myVehicleMarker;
    private Dialog locationEnabledialog;
    private DialogCancelReasonBinding dialogBinding;
    private CancelReasonAdapter cancelReasonAdapter;
    private LocationRequest mLocationRequest;
    private LocationCallback locationCallback;
    LocationRequest locationRequest;


    String date_time = "";
    int mYear;
    int mMonth;
    int mDay;

    int mHour;
    int mMinute;
    private String mobileNo;
    private String redeemedCouponAmount;
    private String redeemedCouponCodeId;
    private String redeemedCouponCode;
    private String isCouponApplied = "no";
    private Marker destinationMarker;
    private Marker sourceMarker;
    private String expectedTextValue = "";
    private String sourceaddress = "";
    private String destinationaddress = "";
    private Calendar newCalendar;
    private Date mSelectDate;


    private void onSearchSwipeDown() {
        if (isSearchDetailsExpanded) {
            collapseSearchDetails();
        }
    }

    private void onSearchSwipeUp() {
        if (!isSearchDetailsExpanded) {
            expandSearchDetails();
        }
    }

    private void onBookingAcceptedSwipeDown() {
        if (isBookingAcceptedDetailsExpanded) {
            collapseSearchDetails();
        }
    }

    private void onBookingAcceptedSwipeUp() {
        if (!isBookingAcceptedDetailsExpanded) {
            expandSearchDetails();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        sharedHelper = new SharedHelper(MainActivity.this);
        locationListner();
        showEnableLocationDialog();
        mDrawerLayout = binding.drawerLayout;
        initAnimationListener(savedInstanceState);
        initViews();
        initViewListeners();
        locationCheck();
        initSockets();
        initObservers();
        setNavigationData();
        updateDeviceToken();
        listnerOfflineProviders();


    }


    private void showEnableLocationDialog() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationEnabledialog = getBounceDialog();
        locationEnabledialog.setCancelable(false);
        locationEnabledialog.setCanceledOnTouchOutside(false);

        locationEnabledialog.findViewById(R.id.enableGps).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationCheck();
            }
        });
        TranslateAnimation anim = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, -0.5f); // this is distance of top and bottom form current positiong

        anim.setDuration(800);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setRepeatMode(Animation.REVERSE);
        locationEnabledialog.findViewById(R.id.locationIcon).startAnimation(anim);

    }

    private void getOngoingBooking() {
        if (Utils.isNetworkConnected(this)) {
            viewModel.getCurrentBooking(getOngoingInput()).observe(this, new Observer<CurrentBookingResponseModel>() {
                @Override
                public void onChanged(CurrentBookingResponseModel currentBookingResponseModel) {
                    if (currentBookingResponseModel != null) {
                        ongoingBookingResponse.setValue(currentBookingResponseModel);
                    }
                }
            });
        } else {
            noInternetDialog = new NoInternetDialog(MainActivity.this);
            noInternetDialog.show();
            noInternetDialog.setOnConnected(new InternetCallback() {
                @Override
                public void onConnected() {
                    noInternetDialog.dismiss();
                    getOngoingBooking();
                }
            });
        }

    }


    private InputForAPI getOngoingInput() {

        InputForAPI inputForAPI = new InputForAPI(MainActivity.this);
        inputForAPI.setUrl(UrlHelper.GET_CURRENT_BOOKING);
        inputForAPI.setHeaders(ApiCall.getHeaders(MainActivity.this));
        return inputForAPI;
    }


    private void showCancelConfirmation() {

        final Dialog dialog = getConfirmationDialog();
        alertDialogBinding.dialogContent.setText(getResources().getString(R.string.are_you_sure_you_want_to_cancel_the_booking));
        alertDialogBinding.positiveButtonText.setText(getResources().getString(R.string.yes));
        alertDialogBinding.negativeButtonText.setText(getResources().getString(R.string.no));
        alertDialogBinding.positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    buildCancelBookingInput("", dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        alertDialogBinding.negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    private void showLogOutConfirmation() {

        final Dialog dialog = getConfirmationDialog();
        alertDialogBinding.dialogContent.setText(getResources().getString(R.string.are_you_sure_you_want_to_logout));
        alertDialogBinding.positiveButtonText.setText(getResources().getString(R.string.logout));
        alertDialogBinding.negativeButtonText.setText(getResources().getString(R.string.no));
        alertDialogBinding.positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutFromApp();
            }
        });

        alertDialogBinding.negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void placeSearchLayout() {
        hideServiceListLayout();
        hideDriverSearchingLayout();
        showSearchLayout();
        hideBookingAcceptedLayout();
        updateRemoveCouponUi();
        closeDrawer();
    }

    private void resetCouponValues() {
        binding.mainPageContentLayout.customisationLayout.couponCode.setText("");
        binding.mainPageContentLayout.customisationLayout.appliedText.setVisibility(View.GONE);

        hideVerifyIcon();
    }

    public void placeServiceLisLayout() {
        hideSearchLayout();
        hideDriverSearchingLayout();
        showServiceListLayout();
        setServiceListValues();
        hideRideCustomisatonLayout();
        stopRippleAnimation();
        currentState = Constants.HomePageState.SERVICE_TYPE_LISTED;
    }

    private void hideRideCustomisatonLayout() {

        if (binding.mainPageContentLayout.customisationLayout.getRoot().getVisibility() == View.VISIBLE) {
            animationHelper.slideAndHide(binding.mainPageContentLayout.customisationLayout.getRoot(), defaultLayoutTransitionScreens, 0, 0, 0, 500);
        } else {
            binding.mainPageContentLayout.customisationLayout.getRoot().setVisibility(View.GONE);
        }

    }

    private void stopRippleAnimation() {
        if (binding.mainPageContentLayout.driverSearchingLayout.rippleLayout.isRippleAnimationRunning()) {
            binding.mainPageContentLayout.driverSearchingLayout.rippleLayout.stopRippleAnimation();
        }
    }

    private void hideDriverSearchingLayout() {
        if (binding.mainPageContentLayout.driverSearchingLayout.getRoot().getVisibility() == View.VISIBLE) {
            animationHelper.slideAndHide(binding.mainPageContentLayout.driverSearchingLayout.getRoot(), defaultLayoutTransitionScreens, 0, 0, 0, 500);
        } else {
            binding.mainPageContentLayout.driverSearchingLayout.getRoot().setVisibility(View.GONE);
        }
    }


    public void hideServiceListLayout() {
        if (mMap != null) {
            mMap.setPadding(0, 0, 0, 0);
        }

        if (srclat > 0 && deslat > 0) {
            animationHelper.slideAndHide(binding.mainPageContentLayout.serviceListLayout.getRoot(), defaultLayoutTransitionScreens, 0, 0, 0, 500);
        } else {
            binding.mainPageContentLayout.serviceListLayout.getRoot().setVisibility(View.GONE);
        }

    }

    public void showSearchLayout() {
        sharedHelper.setSelectedCardId("");
        sharedHelper.setSelectedPaymentType(Constants.SelectedPaymentType.CASH.toString().toLowerCase());
        setPaymentDetails(sharedHelper.getSelectedPaymentType());
        animationHelper.slideAndSHow(binding.mainPageContentLayout.searchLayout.getRoot(), defaultLayoutTransitionScreens, 0, 0, -500, 0);
        deslat = 0;
        deslng = 0;
        isSourceSelected = false;
        isDestinationSelected = true;
        binding.mainPageContentLayout.searchLayout.destinationText.setText("");
        showMyLocationIndicatorinMap();
        if (mMap != null) {
            mMap.clear();
            printLog("showSearchLayout: " + currentLatitude + "/" + currentLongitude);
            zoomToMyLocation(currentLatitude, currentLongitude, 15);
            if (locationEnabledialog.isShowing()) {
                locationEnabledialog.dismiss();
            }
        }

        binding.menuIcon.setImageResource(R.drawable.menu_icon);
    }

    private void updateDeviceToken() {

        commonViewModel.flagCheckResponse(getUpdateDeviceTokenInput()).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(FlagCheckResponseModel flagCheckResponseModel) {
                printLog("Token Updated");
            }
        });
    }

    public void printLog(String log) {
        Utils.log(TAG, log);
    }


    private InputForAPI getUpdateDeviceTokenInput() {
        InputForAPI inputForAPI = new InputForAPI(MainActivity.this);

        inputForAPI.setUrl(UrlHelper.UPDATE_DEVICE_TOKEN);
        inputForAPI.setHeaders(ApiCall.getHeaders(MainActivity.this));
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.ApiKeys.FCM_TOKEN, commonViewModel.getNotificationToken());
            jsonObject.put(Constants.ApiKeys.BRAND, Build.MANUFACTURER);
            jsonObject.put(Constants.ApiKeys.MODEL, Build.MODEL);
            jsonObject.put(Constants.ApiKeys.OS, "android");
            jsonObject.put(Constants.ApiKeys.OSVERSION, Build.VERSION.RELEASE);
            jsonObject.put(Constants.ApiKeys.APPVERSION, BuildConfig.VERSION_CODE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        inputForAPI.setJsonObject(jsonObject);
        return inputForAPI;
    }


    private void zoomToMyLocation(double latitude, double longitude, int zoomvalue) {
        CameraUpdate center = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), zoomvalue);
        mMap.animateCamera(center);
        getCompleteAddress(latitude, longitude);

    }


    public void hideSearchLayout() {
        if (binding.mainPageContentLayout.searchLayout.getRoot().getVisibility() == View.VISIBLE) {
            animationHelper.slideAndHide(binding.mainPageContentLayout.searchLayout.getRoot(), defaultLayoutTransitionScreens, 0, 0, 0, -500);
        } else {
            binding.mainPageContentLayout.searchLayout.getRoot().setVisibility(View.GONE);
        }
    }

    public void showServiceListLayout() {
        animationHelper.slideAndSHow(binding.mainPageContentLayout.serviceListLayout.getRoot(), defaultLayoutTransitionScreens, 0, 0, 500, 0);
    }


    public void onProfileImageClicked(View view) {
        moveActivity(EditProfileActivity.class);
    }

    public void onYourTripsClicked(View view) {
        closeDrawer();
        moveActivity(YourTripsActivity.class);
    }

    public void onPaymentClicked(View view) {
        closeDrawer();
        movePaymentAcivity("navigationdrawer");
    }

    private void movePaymentAcivity(String type) {
        Intent intent = new Intent(MainActivity.this, ChoosePaymentActivity.class);
        intent.putExtra(Constants.IntentKeys.TYPE, type);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.no_animation);
    }

    private void moveChoosePaymentActivity(double amount) {
        Intent intent = new Intent(MainActivity.this, ChoosePaymentActivity.class);
        intent.putExtra(Constants.IntentKeys.TYPE, "booking");
        intent.putExtra(Constants.IntentKeys.DATA, "" + amount);
        startActivityForResult(intent, Constants.IntentPermissionCode.PAYMENT_SELECTION);

    }


    public void onTrustedContactsClicked(View view) {
        closeDrawer();
        moveActivity(TrustedContactsActivity.class);
    }

    public void onWalletClicked(View view) {
        moveActivity(WalletActivity.class);
    }


    public void onHelpClicked(View view) {
        closeDrawer();
        moveActivity(HelpActivity.class);
    }

    public void onSettingsClicked(View view) {
        closeDrawer();
        moveActivity(SettingsActivity.class);
    }

    public void onMenuIconClicked(View view) {
        if (currentState == Constants.HomePageState.SOURCE_SELECTED) {
            openDrawer();
        } else {
            onBackPressed();
        }
    }


    public Dialog getBounceDialog() {
        final Dialog dialog = new Dialog(MainActivity.this, R.style.FullScreenDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.enable_location_dialog);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        dialog.show();
        return dialog;

    }


    private void cancelBooking() {
        selectedrideType = "";
        placeServiceLisLayout();
        hideBookingAcceptedLayout();

    }


    public void onCloseIconClicked(View view) {
        closeDrawer();
    }


    private void initViewListeners() {

        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View view, float v) {
                printLog("" + v);
                if (v > 0.6) {
                    binding.menuIcon.setVisibility(View.GONE);
                } else {
                    binding.menuIcon.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onDrawerOpened(@NonNull View view) {
                binding.menuIcon.setVisibility(View.GONE);
            }

            @Override
            public void onDrawerClosed(@NonNull View view) {
                binding.menuIcon.setVisibility(View.VISIBLE);
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

        binding.mainPageContentLayout.driverSearchingLayout.searchDetailsHeader.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return detector.onTouchEvent(event);
            }
        });
        binding.mainPageContentLayout.bookingAcceptedLayout.acceptedBookingDetailsLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return bookingAccepteddetector.onTouchEvent(event);
            }
        });

        trackingListner = new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                printLog("tracking values response:" + args[0]);
                animationHelper.stopPolylineanimation();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateLiveStatusText(args[0].toString());
                    }
                });

                if (currentBookingId.length() > 0) {
                    Gson gson = new Gson();
                    final ProviderDetailResponse providerList = gson.fromJson(args[0].toString(), ProviderDetailResponse.class);
                    LatLng sourceLat = getConvertedLatlng(providerList.getData().getLatitude(), providerList.getData().getLongitude());
                    LatLng destinationLat;
                    if (viewModel.isToSourceLocation(getCurrentBookingStatus())) {
                        destinationLat = getCurrentBookingValue().getFromLatLng();
                    } else {
                        destinationLat = getCurrentBookingValue().getToLatLng();
                    }
                    if (currentRoute.size() == 0) {
                        generateTrackingRoute(sourceLat, destinationLat);
                    } else {
                        if (isPointOnTheROute(sourceLat)) {
                            int index = currentRoute.indexOf(sourceLat);
                            currentRoute = currentRoute.subList(index, currentRoute.size());
                            currentRoute.add(0, sourceLat);
                            handleTrackRouteResponse(sourceLat, destinationLat, currentRoute);
                        } else {
                            clearPonints();
                            generateTrackingRoute(sourceLat, destinationLat);
                        }
                    }
                    if (myVehicleMarker == null) {
                        addCustomMarker(destinationLat, R.drawable.source_tracker_icon, true);
                        addVehicleCustomMarker(sourceLat);
                    } else {
                        animateMarker(sourceLat, providerList.getData().getBearing());
                    }
                }
            }

        };
    }

    private void clearPonints() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    blackTrackingPolyline.remove();
                    blackTrackingPolyline = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                currentRoute.clear();
            }
        });

    }

    private void updateLiveStatusText(String s) {
        try {
            JSONObject jsonObject = new JSONObject(s);
            statusValue.setValue(jsonObject.optJSONObject(Constants.ApiKeys.DATA).optString(Constants.ApiKeys.STATUS));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private String getCurrentBookingStatus() {
        return ongoingBookingResponse.getValue().getData().getStatus();
    }


    private CurrentBookingResponseModel.Data getCurrentBookingValue() {
        return ongoingBookingResponse.getValue().getData();
    }


    private void clearMap() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                blackPolyline = null;
                printLog("Tracking Vehicle marker null");
                myVehicleMarker = null;
                mMap.clear();
            }
        });
    }

    private LatLngBounds getLatLngBounds(LatLng sourceLatLng, LatLng destinationLatLng) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(sourceLatLng);
        builder.include(destinationLatLng);
        return builder.build();
    }


    private void handleRouteResponse(final LatLng sourceLatLng, final LatLng destinationLatLng, final List<LatLng> route, final boolean isFromTracking) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                generateMarker(sourceLatLng, destinationLatLng, isFromTracking);
                hideMyLocationIndicatorinMap();
                route.add(0, sourceLatLng);
                route.add(destinationLatLng);
                addRouteinMap(route, getLatLngBounds(sourceLatLng, destinationLatLng));

            }
        });


    }


    private void handleTrackRouteResponse(final LatLng sourceLatLng, final LatLng destinationLatLng, final List<LatLng> route) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                isTrackRouteGenerating = false;
                addTrackRouteinMap(route, getLatLngBounds(sourceLatLng, destinationLatLng));
                hideMyLocationIndicatorinMap();
                generateTrackingMarker(sourceLatLng, destinationLatLng);
            }
        });


    }


    private void addRouteinMap(final List<LatLng> route, final LatLngBounds bounds) {
        if (blackPolyline != null) {
            if (blackPolyline.getPoints().size() == 0) {
                generateBlackPolyline(route);
            } else {
                blackPolyline.setPoints(route);
            }
        } else {
            generateBlackPolyline(route);
        }

        generateGreyPolyline(route);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 340));
                sourceMarker.setTag(true);
                destinationMarker.setTag(false);
//                mMap.setInfoWindowAdapter(new CustomWindowAdapter(MainActivity.this, expectedTextValue, "source", "destination"));
//                sourceMarker.showInfoWindow();
//                destinationMarker.showInfoWindow();

            }
        });
        animatePolyLine(route);

    }


    private void addTrackRouteinMap(List<LatLng> route, final LatLngBounds bounds) {
        if (blackTrackingPolyline != null) {
            if (blackTrackingPolyline.getPoints().size() == 0) {
                generateTrackBlackPolyline(route);
            } else {
                try {
                    blackTrackingPolyline.setPoints(route);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            generateTrackBlackPolyline(route);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.setPadding(60, 60, 60, 300);
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
            }
        });


    }


    public boolean isPointOnTheROute(LatLng currentLatLng) {
        return currentRoute.contains(currentLatLng);
    }


    public LatLng getConvertedLatlng(String lat, String lng) {
        return new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
    }


    public void openDrawer() {
        mDrawerLayout.openDrawer(GravityCompat.START, true);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(GravityCompat.START, true);
    }


    private void moveActivity(Class toActivity) {
        Intent intent = new Intent(MainActivity.this, toActivity);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.no_animation);
    }

    private void setNavigationData() {
        binding.navheader.userName.setText(commonViewModel.getUserName());
        binding.navheader.userRating.setText(commonViewModel.getUserRating());
        BindingAdapter.loadImage(binding.navheader.userImage, commonViewModel.getUserImage(), getResources().getDrawable(R.drawable.profile_placeholder));
        setRating(Float.parseFloat(commonViewModel.getUserRating()), binding.navheader.ratingParent, MapRippleHelper.dpToPx(10));
        binding.versionName.setText("v" + Utils.getVersionName(MainActivity.this));
    }

    private void locationCheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, COARSE_LOCATION_PERMISSIONS);
        } else {
            promptEnableGpsDialog();
        }
    }

    private void initSockets() {
        IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.reconnection = false;
        opts.query = Constants.ApiKeys.TOKEN + "=" + commonViewModel.getAccessToken();
        try {
            socket = IO.socket(UrlHelper.BASE, opts);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        socket.connect();

    }

    private void getMyCelldID(double latitude, double longitude) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.ApiKeys.LATITUDE, "" + latitude);
            jsonObject.put(Constants.ApiKeys.LONGITUDE, "" + longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        socket.emit(UrlHelper.GET_MY_CELL_ID, jsonObject, new Ack() {
            @Override
            public void call(Object... args) {
                printLog("cellID response:" + args[0]);
                startListeningSockets();
            }
        });

        emitMyLocation(jsonObject);

        startListeningSockets();
    }


    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(MainActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                noInternetDialog.dismiss();
//                getOngoingBooking();
            }
        });
    }


    private void displayError(String error_message) {
        Utils.displayError(findViewById(android.R.id.content), error_message);
    }


    @SuppressLint("CheckResult")
    private void initObservers() {
        mutableLocationResponseData.observe(this, new Observer<GetProviderLocationResponse>() {
            @Override
            public void onChanged(GetProviderLocationResponse getProviderLocationResponse) {
                if (getProviderLocationResponse != null) {
                    if (!getProviderLocationResponse.getError()) {
                        plotProvidersinMap(getProviderLocationResponse.getData().getProviderlocation());
                    }
                }
            }
        });
        statusValue.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.mainPageContentLayout.bookingAcceptedLayout.driverStatus.setText(s);
            }
        });
        ongoingBookingResponse.observe(this, new Observer<CurrentBookingResponseModel>() {
            @Override
            public void onChanged(CurrentBookingResponseModel responseModel) {
                if (responseModel != null) {

                    sharedHelper.setUserRating(responseModel.getData().getRating());
                    setNavigationData();
                    if (!responseModel.getError()) {
                        if (responseModel.getData() != null) {
                            currentBookingId = String.valueOf(responseModel.getData().getId());
                            updateCurrentBookingId();
                            handleStatus(responseModel.getData());
                        }
                    } else {
                        if (responseModel.getMsg().equalsIgnoreCase(getString(R.string.no_internet_connection))) {
                            showNoInternetDialog();
                        } else if (responseModel.getMsg().equalsIgnoreCase(getString(R.string.error_authentication))) {
                            logoutFromApp();
                        } else {
                            currentBookingId = "";
                            selectedrideType = "";
                            if (responseModel.getData() != null) {
                                try {
                                    if (responseModel.getData().getId() > 0) {
                                        placeRatingLayout();
                                    } else {
                                        placeSearchLayout();
                                    }
                                } catch (Exception e) {
                                    placeSearchLayout();
                                    e.printStackTrace();
                                }
                            } else {
                                placeSearchLayout();
                            }
                        }

                    }
                }

            }
        });


        ((AppController) AppController.getInstance()).getRxBus().toObservable().subscribe(new Consumer<String>() {
            @Override
            public void accept(final String value) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (value.equalsIgnoreCase(Constants.NotificationKeys.BOOKING_CANCELLED)) {
                            if (currentState == Constants.HomePageState.SEARCHING_DRIVER) {
                                cancelBooking();
                            } else {
                                getOngoingBooking();
                            }
                        } else {
                            getOngoingBooking();
                        }
                    }
                });

            }
        });

    }


    private void setNavigationRating() {
        binding.navheader.userRating.setText(ongoingBookingResponse.getValue().getData().getRating());
    }

    private void updateCurrentBookingId() {
        sharedHelper.setBookingNumber(currentBookingId);
    }


    private void handleStatus(CurrentBookingResponseModel.Data data) {
        joinRoom();
        sourceLatLng = new LatLng(Double.parseDouble(data.getSourceLat()), Double.parseDouble(data.getSourceLong()));
        if (data.getStatus().equalsIgnoreCase(Constants.BookingStatus.ASSIGNED) || data.getStatus().equalsIgnoreCase(Constants.BookingStatus.PROCESSING)) {
            placeDriverSearchingLayout(data);
        } else {
            clearMap();
            placeBookingAcceptedLayout(data);
        }

        if (locationEnabledialog.isShowing()) {
            locationEnabledialog.dismiss();
        }
    }

    private void joinRoom() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.ApiKeys.BOOKING_NUMBER, commonViewModel.getCurrentBookingID());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit(UrlHelper.JOIN_LIVE_TRACKING, jsonObject, new Ack() {
            @Override
            public void call(Object... args) {
                isJoinedRoom = true;
                printLog("New Tracking ack" + args[0]);
            }
        });
        startLocationTracking();

    }

    private void startLocationTracking() {
        socket.on(UrlHelper.GET_LIVE_TRACKING, trackingListner);
    }

    public void onLogoutClicked(View view) {
        showLogOutConfirmation();
    }

    private void logoutFromApp() {
//        SharedHelper sharedHelper = new SharedHelper(MainActivity.this);
//        sharedHelper.setIsUserLoggedIn(false);
//        Intent intent = new Intent(this, SignInActivity.class);
//        intent.putExtra(Constants.IntentKeys.SKIP, true);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
//        finish();
    }


    private void startListeningSockets() {

        socket.on(UrlHelper.EVENT_GET_PROVIDER_LOCATION, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                printLog("Provider Location " + args[0]);
                Gson gson = new Gson();
                final GetProviderLocationResponse providerList = gson.fromJson(args[0].toString(), GetProviderLocationResponse.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (currentBookingId.length() == 0) {
                            mutableLocationResponseData.setValue(providerList);
                        }
                    }
                });

            }
        });
    }


    private void plotProvidersinMap(final List<ProviderDetailResponse.Data> data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < data.size(); i++) {
                    ProviderDetailResponse.Data providerDetails = data.get(i);
                    plotIndividualProviders(providerDetails);
                }
            }
        });
    }

    private void plotIndividualProviders(ProviderDetailResponse.Data providerDetails) {

        final LatLng latLng = new LatLng(Double.parseDouble(providerDetails.getLatitude()), Double.parseDouble(providerDetails.getLongitude()));
        String isProviderAlreadyThere = viewModel.isProviderAvailable(markers, providerDetails.getProviderId());
        if (!isProviderAlreadyThere.equalsIgnoreCase("false")) {
            LatLng latitudeLongitude = new LatLng(Double.parseDouble(providerDetails.getLatitude()), Double.parseDouble(providerDetails.getLongitude()));
            animateMarker(markers.get(Integer.parseInt(isProviderAlreadyThere)), latitudeLongitude, Integer.parseInt(isProviderAlreadyThere), providerDetails.getBearing());
        } else {
            //changed marker style to static
            addMarker(latLng, 1, providerDetails);
        }
    }


    public void emitMyLocation(JSONObject location) {
        socket.emit(UrlHelper.EVENT_GET_PROVIDER_LOCATION, location);
    }


    public void animateMarker(final Marker marker, final LatLng toPosition, final int position, final int bearing) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            if (!startPosition.equals(toPosition)) {
                final AnimationHelper.LatLngInterpolatorNew latLngInterpolator = new AnimationHelper.LatLngInterpolatorNew.LinearFixed();
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(1000); // duration 3 second
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        try {
                            float v = animation.getAnimatedFraction();
                            LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, toPosition);
                            marker.setPosition(newPosition);
                            marker.setFlat(true);
                            marker.setAnchor(0.5f, 0.5f);
                            marker.setRotation((float) bearing);
                            markers.set(position, marker);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
                valueAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                    }
                });
                valueAnimator.start();
            } else {
                marker.setFlat(true);
                marker.setAnchor(0.5f, 0.5f);
                marker.setRotation((float) bearing);
                markers.set(position, marker);
            }
        }
    }


    public void animateMarker(final LatLng toPosition, final float bearing) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (myVehicleMarker != null) {
                    printLog("Tracking Vehicle marker == not null 2");
                    final LatLng startPosition = myVehicleMarker.getPosition();
//                    if (!startPosition.equals(toPosition)) {
                    final AnimationHelper.LatLngInterpolatorNew latLngInterpolator = new AnimationHelper.LatLngInterpolatorNew.LinearFixed();
                    ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                    valueAnimator.setDuration(1000); // duration 3 second
                    valueAnimator.setInterpolator(new LinearInterpolator());
                    valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animation) {
                            try {
                                float v = animation.getAnimatedFraction();
                                LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, toPosition);
                                myVehicleMarker.setPosition(newPosition);
                                myVehicleMarker.setAnchor(0.5f, 0.5f);
                                myVehicleMarker.setRotation(bearing);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    valueAnimator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);

                        }
                    });
                    valueAnimator.start();
//                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            printLog("Tracking Vehicle marker == null 3");
                            addVehicleCustomMarker(toPosition);
                        }
                    });
                }
            }
        });


    }


    private void initAnimationListener(Bundle savedInstanceState) {
        Intent intent = getIntent();
        if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                intent.hasExtra(Constants.AnimationKeys.EXTRA_CIRCULAR_REVEAL_X) &&
                intent.hasExtra(Constants.AnimationKeys.EXTRA_CIRCULAR_REVEAL_Y)) {
            mDrawerLayout.setVisibility(View.INVISIBLE);


            revealX = intent.getIntExtra(Constants.AnimationKeys.EXTRA_CIRCULAR_REVEAL_X, 0);
            revealY = intent.getIntExtra(Constants.AnimationKeys.EXTRA_CIRCULAR_REVEAL_Y, 0);

            ViewTreeObserver viewTreeObserver = mDrawerLayout.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mDrawerLayout.setBackgroundColor(getResources().getColor(R.color.black));
                        AnimationHelper.revealActivity(mDrawerLayout, revealX, revealY);
                        mDrawerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
        } else {
            mDrawerLayout.setVisibility(View.VISIBLE);
            mDrawerLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        }
    }


    private void initViews() {

        searchLayout = binding.mainPageContentLayout.searchLayout;
        sourceText = binding.mainPageContentLayout.searchLayout.sourceText;
        destinationText = binding.mainPageContentLayout.searchLayout.destinationText;
        rideTypeRecyclerView = binding.mainPageContentLayout.serviceListLayout.rideTypeRecyclerView;
        viewModel = ViewModelProviders.of(this).get(HomePageViewModel.class);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        imageLoader = new ImageLoader(MainActivity.this);
        animationHelper = new AnimationHelper();
        detector = new GestureDetectorCompat(MainActivity.this, onSwipeListener);
        bookingAccepteddetector = new GestureDetectorCompat(MainActivity.this, onSwipeAcceptedListener);
        mutableLocationResponseData = new MutableLiveData<>();
        statusValue = new MutableLiveData<>();
        ongoingBookingResponse = new MutableLiveData<>();
        setUpMap();

    }

    private void locationListner() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);
        locationCallback = new
                LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if (locationResult == null) {
                            return;
                        }
                        for (Location location : locationResult.getLocations()) {
                            if (location != null) {
                                if (currentLatitude == 0 || currentLongitude == 0) {
                                    getLastKnownLocation(true);
                                }
                            }
                        }
                    }
                };

    }

    private void placeRatingLayout() {

        hideSearchLayout();
        hideDriverSearchingLayout();
        hideBookingAcceptedLayout();
        showRating();
    }

    private void showRating() {
        Intent intent = new Intent(MainActivity.this, RatingActivity.class);
        intent.putExtra(Constants.IntentKeys.DATA, ongoingBookingResponse.getValue().getData());
        startActivityForResult(intent, Constants.IntentPermissionCode.RATING_STATUS);
    }


    private void hideBookingAcceptedLayout() {
        binding.mainPageContentLayout.bookingAcceptedLayout.getRoot().setVisibility(View.GONE);

    }

    private void placeBookingAcceptedLayout(CurrentBookingResponseModel.Data data) {
        hideMyLocationIndicatorinMap();
        showBookingAcceptedLayout(data);
        hideDriverSearchingLayout();
        hideSearchLayout();
        setBookingAcceptedValues(data);

    }

    private void setBookingAcceptedValues(CurrentBookingResponseModel.Data data) {

        binding.mainPageContentLayout.bookingAcceptedLayout.setBookingDetails(data);
        final LatLng src = new LatLng(currentLatitude, currentLongitude);

        final LatLng des;
        if (data.getStatus().equalsIgnoreCase(Constants.BookingStatus.PICKED_UP) || data.getStatus().equalsIgnoreCase(Constants.BookingStatus.COMPLETED)) {
            des = data.getToLatLng();
        } else {
            des = data.getFromLatLng();
        }

        try {
            setRating(Float.parseFloat(data.getProviderInfo().getRating()), binding.mainPageContentLayout.bookingAcceptedLayout.ratingParent, 30);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        cameraAngleForSourceAndDestination(src, des);
        generateTrackingRoute(src, des);
        generateTrackingMarker(src, des);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.setPadding(60, 60, 60, 300);
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(getLatLngBounds(src, des), 0));
            }
        });

    }

    private void showBookingAcceptedLayout(CurrentBookingResponseModel.Data data) {
        binding.mainPageContentLayout.bookingAcceptedLayout.getRoot().setVisibility(View.VISIBLE);
        if (data.getStatus().equalsIgnoreCase(Constants.BookingStatus.ACCEPTED)) {
            binding.mainPageContentLayout.bookingAcceptedLayout.cancelText.setVisibility(View.VISIBLE);
        } else {
            binding.mainPageContentLayout.bookingAcceptedLayout.cancelText.setVisibility(View.GONE);
        }
    }

    public void setUpMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        onDestinationSelected();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    public void promptEnableGpsDialog() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                getLastKnownLocation(true);
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(MainActivity.this, Constants.IntentPermissionCode.REQUEST_LOCATION);
                    } catch (IntentSender.SendIntentException sendEx) {
                        sendEx.printStackTrace();
                    }
                }
            }
        });
    }

    private void getLastKnownLocation(final boolean shouldReload) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                fusedLocationClient.requestLocationUpdates(mLocationRequest, null);
                Task locationResult = fusedLocationClient.getLastLocation();
                locationResult.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {

                            Location location = (Location) task.getResult();
                            if (location != null) {
                                printLog("Last location retrieved not null ");
                                currentLatitude = location.getLatitude();
                                currentLongitude = location.getLongitude();
                                JSONObject jsonObject = new JSONObject();


                                if (shouldReload) {
                                    getMyCelldID(currentLatitude, currentLongitude);
                                    getOngoingBooking();
                                }
                            } else {
                                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    Activity#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for Activity#requestPermissions for more details.
                                    return;
                                }
                                fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            }
                        }
                    }
                });
            }

        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.IntentPermissionCode.REQUEST_LOCATION) {
            switch (resultCode) {
                case RESULT_OK:
//                    startLocationUpdate();
                    getLastKnownLocation(true);
                    break;
                case RESULT_CANCELED:
                    statusCheck();
                    break;
                default:
                    break;
            }
        } else if (requestCode == Constants.IntentPermissionCode.SOURCE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                handleSourceActivityResult(data);
            }


        } else if (requestCode == Constants.IntentPermissionCode.DESTINATION_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                handleDestinationActivityResult(data);
            }
        } else if (requestCode == Constants.IntentPermissionCode.RATING_STATUS) {
            getOngoingBooking();
        } else if (requestCode == Constants.IntentPermissionCode.PAYMENT_SELECTION) {
            if (resultCode == RESULT_OK) {
                setPaymentDetails(data.getStringExtra(Constants.IntentKeys.SELECTED_PAYMENT_NAME));
            }
        }

    }

    private void setPaymentDetails(String paymentName) {
        binding.mainPageContentLayout.customisationLayout.selectedPaymentName.setText(Utils.capitalizeSentence(paymentName));
        if (paymentName.equalsIgnoreCase(Constants.SelectedPaymentType.CASH.toString())) {
            setSelectedPaymentImage(Utils.changeDrawableColor(R.drawable.cash_icon, R.color.green_color));
        }
        if (paymentName.equalsIgnoreCase(Constants.SelectedPaymentType.CARD.toString())) {
            binding.mainPageContentLayout.customisationLayout.selectedPaymentImage.setColorFilter(null);
            setSelectedPaymentImage(getResources().getDrawable(R.drawable.card_icon));
        }
        if (paymentName.equalsIgnoreCase(Constants.SelectedPaymentType.WALLET.toString())) {
            binding.mainPageContentLayout.customisationLayout.selectedPaymentImage.setColorFilter(null);
            setSelectedPaymentImage(getResources().getDrawable(R.drawable.wallet_icon_colored));
        }
    }

    public void setSelectedPaymentImage(Drawable drawableReferene) {
        binding.mainPageContentLayout.customisationLayout.selectedPaymentImage.setImageDrawable(drawableReferene);


    }

    private void handleDestinationActivityResult(Intent data) {
        onDestinationSelected();
        if (data != null) {
            setDestinationText(data.getStringExtra(Constants.IntentKeys.AREA_NAME));
            deslat = Double.parseDouble(data.getStringExtra(Constants.IntentKeys.LATITUDE));
            deslng = Double.parseDouble(data.getStringExtra(Constants.IntentKeys.LONGITUDE));
            srcCountryCode = data.getStringExtra(Constants.IntentKeys.SHORT_CODE);

        }


        if (srclat != 0 && deslat != 0) {
            sourceAndDestinationSelected();
        }
    }

    private void sourceAndDestinationSelected() {
        sourceLatLng = new LatLng(srclat, srclng);
        destinationLatLng = new LatLng(deslat, deslng);

        float distance = Utils.caluclatedistanceInMeters(sourceLatLng, destinationLatLng);
        if (distance > Integer.parseInt(sharedHelper.getAppConfiguration().getData().getAuthConfig().getMax_range())) {
            Utils.displayError(findViewById(android.R.id.content), getResources().getString(R.string.distance_too_long));
        } else {
            placeServiceLisLayout();
        }


    }

    private void handleSourceActivityResult(Intent data) {

        if (data != null) {
            setSourceText(data.getStringExtra(Constants.IntentKeys.AREA_NAME));
            srclat = Double.parseDouble(data.getStringExtra(Constants.IntentKeys.LATITUDE));
            srclng = Double.parseDouble(data.getStringExtra(Constants.IntentKeys.LONGITUDE));
            srcCountryCode = data.getStringExtra(Constants.IntentKeys.SHORT_CODE);
            clearMap();
            getMyCelldID(srclat, srclng);
        }
        onSourceSelected();

        if (srclat != 0 && deslat != 0) {
            sourceAndDestinationSelected();
        } else {
            animateCameraToLocation(srclat, srclng);
        }


    }

    private void setSourceText(String stringExtra) {
        sourceaddress = stringExtra;
        sourceText.setText(stringExtra);
        currentState = Constants.HomePageState.SOURCE_SELECTED;

    }

    private void setDestinationText(String stringExtra) {
        destinationaddress = stringExtra;
        destinationText.setText(stringExtra);
    }

    public void hideMyLocationIndicatorinMap() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        hideMyLocationButton();
        mMap.setMyLocationEnabled(false);
    }

    public void hideMyLocationButton() {
        binding.mainPageContentLayout.searchLayout.myLocationButton.setVisibility(View.GONE);
    }

    public void showMyLocationIndicatorinMap() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        showMyLocationButton();
        if (mMap != null) {
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.setMyLocationEnabled(true);
        }
    }

    private void showMyLocationButton() {
        binding.mainPageContentLayout.searchLayout.myLocationButton.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {
        if (currentState == Constants.HomePageState.SOURCE_SELECTED) {
            super.onBackPressed();
        } else if (currentState == Constants.HomePageState.SERVICE_TYPE_LISTED) {
            placeSearchLayout();
        } else if (currentState == Constants.HomePageState.RIDE_CUSTOMSATION) {
            placeServiceLisLayout();
        } else if (currentState == Constants.HomePageState.SEARCHING_DRIVER) {
            placeRideCustomisationLayout();

        }
    }

    private void placeRideCustomisationLayout() {
        hideDriverSearchingLayout();
        showRideCustomisationLayout();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        showMyLocationIndicatorinMap();
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setBuildingsEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);

        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (value == 0) {
                    printLog("mapGPS");
                    LatLng myLocation;
                    myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    value++;
                    getCompleteAddress(myLocation.latitude, myLocation.longitude);

                    getMyCelldID(location.getLatitude(), location.getLongitude());
                }


            }
        });
        mMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(this, R.raw.google_map_style));
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (destinationMarker != null) {
                    destinationMarker.showInfoWindow();
                }
            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Utils.log(TAG, "markers clicked");
                try {
                    if ((boolean) marker.getTag()) {
                        moveSearchPlacesActivity(SOURCE_REQUEST_CODE, sourceLatLng.latitude, sourceLatLng.longitude);
                    } else {
                        moveSearchPlacesActivity(DESTINATION_REQUEST_CODE, deslat, deslng);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true;
            }
        });
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLastKnownLocation(true);
        }

    }

    private void setServiceListValues() {
        binding.menuIcon.setImageResource(R.drawable.left_arrow);

        buildServiceListInputs();

    }

    private void generateRoute(final LatLng sourceLatLng, final LatLng destinationLatLng) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewModel.getRouteDetails(sourceLatLng, destinationLatLng).observe(MainActivity.this, new Observer<Route>() {
                    @Override
                    public void onChanged(Route route) {
                        expectedTextValue = route.getDurationText();
                        currentRoute = new ArrayList<>();
                        currentRoute.add(sourceLatLng);
                        currentRoute.addAll(route.getPoints());
                        currentRoute.add(destinationLatLng);
                        handleRouteResponse(sourceLatLng, destinationLatLng, route.getPoints(), false);
                    }
                });


            }
        });
    }


    private void generateTrackingRoute(final LatLng sourceLatLng, final LatLng destinationLatLng) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewModel.getRouteDetails(sourceLatLng, destinationLatLng).observe(MainActivity.this, new Observer<Route>() {
                    @Override
                    public void onChanged(Route route) {
                        currentRoute = new ArrayList<>();
                        currentRoute.add(sourceLatLng);
                        currentRoute.addAll(route.getPoints());
                        currentRoute.add(destinationLatLng);
                        handleTrackRouteResponse(sourceLatLng, destinationLatLng, route.getPoints());
                    }
                });


            }
        });


    }


    private void generateMarker(final LatLng sourceLatLng, final LatLng destinationLatLng, boolean isFromTracking) {


        addCustomMarker(sourceLatLng, R.drawable.source_tracker_icon, isFromTracking);
        addCustomMarker(destinationLatLng, R.drawable.destination_tracker_icon, isFromTracking);
    }

    private void generateTrackingMarker(final LatLng sourceLatLng, final LatLng destinationLatLng) {


        if (myVehicleMarker == null) {
            addVehicleCustomMarker(sourceLatLng);
            addCustomMarker(destinationLatLng, R.drawable.destination_tracker_icon, true);
        }


    }


    private void buildServiceListInputs() {
        InputForAPI inputForAPI = new InputForAPI(MainActivity.this);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = getGeneralInputs();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(ApiCall.getHeaders(MainActivity.this));
        inputForAPI.setUrl(UrlHelper.GET_SERVICE_LIST);
        getRideTypes(inputForAPI);
    }

    private void buildPlaceBookingInputs() {
        InputForAPI inputForAPI = new InputForAPI(MainActivity.this);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = getGeneralInputs();
            jsonObject.put(Constants.ApiKeys.RIDE_TYPE, selectedrideType);
            jsonObject.put(Constants.ApiKeys.PICKUP_LOCATION, binding.mainPageContentLayout.searchLayout.sourceText.getText().toString());
            jsonObject.put(Constants.ApiKeys.PAYMENT_MODE, sharedHelper.getSelectedPaymentType());
            jsonObject.put(Constants.ApiKeys.SEATS, binding.mainPageContentLayout.customisationLayout.seatCount.getText().toString());
            jsonObject.put(Constants.ApiKeys.BOOKING_TYPE, selectedbookingType);
            jsonObject.put(Constants.ApiKeys.DROP_LOCATION, binding.mainPageContentLayout.searchLayout.destinationText.getText().toString());
            jsonObject.put(Constants.ApiKeys.DROP_LOCATION, binding.mainPageContentLayout.searchLayout.destinationText.getText().toString());
            jsonObject.put(Constants.ApiKeys.IS_COUPON_APPLIED, isCouponApplied);
            if (sharedHelper.getSelectedPaymentType().equalsIgnoreCase(Constants.SelectedPaymentType.CARD.toString())) {
                jsonObject.put(Constants.ApiKeys.CARD_ID, sharedHelper.getSelectedCardId());
            }

            if (selectedbookingType.equalsIgnoreCase(Constants.BookingType.LATER.toString())) {
                jsonObject.put(Constants.ApiKeys.BOOKING_TIMESTAMP, getBookLaterTimeStamp());
            }

            if (isCouponApplied.equalsIgnoreCase("yes")) {
                jsonObject.put(Constants.ApiKeys.COUPON, redeemedCouponCode);
                jsonObject.put(Constants.ApiKeys.DISCOUNT_AMOUNT, redeemedCouponAmount);
                jsonObject.put(Constants.ApiKeys.REDEEMED_ID, redeemedCouponCodeId);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(ApiCall.getHeaders(MainActivity.this));
        inputForAPI.setUrl(UrlHelper.BOOK_RIDE);
        confirmBooking(inputForAPI);
    }

    private String getBookLaterTimeStamp() {
//        return commonViewModel.getCombinedStrings("" + mYear, "-", "" + mMonth, "-", "" + mDay, " ", "" + mHour, ":", "" + mMinute, ":00");
        return commonViewModel.getCombinedStrings(date_time + " " + Utils.getDoubleDigit(mHour) + ":" + Utils.getDoubleDigit(mMinute) + ":00");
    }


    public void createRippleMarker(final LatLng latLng) {

        binding.mainPageContentLayout.driverSearchingLayout.rippleLayout.startRippleAnimation();

    }


    private void confirmBooking(final InputForAPI inputForAPI) {
        showProgress();
        commonViewModel.flagCheckResponse(inputForAPI).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(FlagCheckResponseModel flagCheckResponseModel) {
                dismissProgress();
                if (flagCheckResponseModel.isError()) {
                    handleBookingFailureResponse(flagCheckResponseModel.getMsg());
                } else {
                    try {
                        binding.mainPageContentLayout.customisationLayout.getRoot().setVisibility(View.GONE);
                        handleBookingSuccessResponse(inputForAPI.getJsonObject().put(Constants.ApiKeys.BOOKING_NUMBER, flagCheckResponseModel.getData().getBookingNo()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void handleBookingFailureResponse(String msg) {
        Utils.displayError(findViewById(android.R.id.content), msg);

    }

    private void handleBookingSuccessResponse(JSONObject jsonObject) {

        if (selectedbookingType.equalsIgnoreCase(Constants.BookingType.LATER.toString())) {
            mMap.clear();
            showSuccessDialog(getResources().getString(R.string.your_booking_has_been_successfully_scheduled));
            successDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    placeSearchLayout();
                }
            });
        } else {
            placeDriverSearchingLayout(jsonObject);
        }

    }

    private void placeDriverSearchingLayout(JSONObject jsonObject) {
        binding.mainPageContentLayout.driverSearchingLayout.getRoot().setVisibility(View.VISIBLE);
        TextView pickUpLocationText = binding.mainPageContentLayout.driverSearchingLayout.pickUpLocationText;
        TextView dropLocationText = binding.mainPageContentLayout.driverSearchingLayout.dropLocationText;
        mMap.clear();
        hideServiceListLayout();
        hideSearchLayout();
        hideRideCustomisatonLayout();
        zoomToMyLocation(srclat, srclng, 14);
        addCustomMarker(sourceLatLng, R.drawable.source_tracker_icon, true);
        createRippleMarker(new LatLng(srclat, srclng));
        pickUpLocationText.setText(jsonObject.optString(Constants.ApiKeys.PICKUP_LOCATION));
        dropLocationText.setText(jsonObject.optString(Constants.ApiKeys.DROP_LOCATION));
        currentBookingId = jsonObject.optString(Constants.ApiKeys.BOOKING_NUMBER);
        currentState = Constants.HomePageState.SEARCHING_DRIVER;
    }

    private void placeDriverSearchingLayout(CurrentBookingResponseModel.Data data) {
        binding.mainPageContentLayout.driverSearchingLayout.getRoot().setVisibility(View.VISIBLE);
        TextView pickUpLocationText = binding.mainPageContentLayout.driverSearchingLayout.pickUpLocationText;
        TextView dropLocationText = binding.mainPageContentLayout.driverSearchingLayout.dropLocationText;

        mMap.clear();
        hideServiceListLayout();
        hideSearchLayout();
        zoomToMyLocation(sourceLatLng.latitude, sourceLatLng.longitude, 14);
        addCustomMarker(sourceLatLng, R.drawable.source_tracker_icon, true);
        createRippleMarker(new LatLng(srclat, srclng));
        pickUpLocationText.setText(data.getFromLocation());
        dropLocationText.setText(data.getToLocation());
        currentState = Constants.HomePageState.SEARCHING_DRIVER;
    }


    private JSONObject getGeneralInputs() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.PICKUP_LATITUDE, Utils.getRoundedOffSixDigits(srclat, "%.6f"));
        jsonObject.put(Constants.ApiKeys.PICKUP_LONGITUDE, Utils.getRoundedOffSixDigits(srclng, "%.6f"));
        jsonObject.put(Constants.ApiKeys.DESTINATION_LATITUDE, Utils.getRoundedOffSixDigits(deslat, "%.6f"));
        jsonObject.put(Constants.ApiKeys.DESTINATION_LONGITUDE, Utils.getRoundedOffSixDigits(deslng, "%.6f"));
        jsonObject.put(Constants.ApiKeys.COUNTRY_SHORT_CODE, srcCountryCode);
        return jsonObject;
    }

    private void getRideTypes(InputForAPI inputForAPI) {

        viewModel.getAvailableRideType(inputForAPI).observe(this, new Observer<RideTypeResponseModel>() {
            @Override
            public void onChanged(RideTypeResponseModel rideTypeResponseModel) {
                if (rideTypeResponseModel.getError()) {
                    handleRideTypeFailureResponse(rideTypeResponseModel.getMsg());
                } else {
                    handleRideTypeSuccessResponse(rideTypeResponseModel.getData());
                }
            }
        });

    }

    private void handleRideTypeFailureResponse(String msg) {
        Utils.displayError(findViewById(android.R.id.content), msg);
        initRideTyeAdapters(new ArrayList<RideTypeResponseModel.Data>());
        getOngoingBooking();
    }

    private void handleRideTypeSuccessResponse(List<RideTypeResponseModel.Data> data) {
        clearMap();
        fullServiceTypeCount = data.size();
        initialRideTypeList = new ArrayList<>();

        remainingRideTypeList = new ArrayList<>();
        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                if (i < 4) {
                    initialRideTypeList.add(data.get(i));
                } else {
                    remainingRideTypeList.add(data.get(i));
                }
            }
            initRideTyeAdapters(initialRideTypeList);
            hideMyLocationIndicatorinMap();
            generateMarker(sourceLatLng, destinationLatLng, false);
            generateRoute(sourceLatLng, destinationLatLng);


        }
    }

    private void initRideTyeAdapters(List<RideTypeResponseModel.Data> data) {
        final List<RideTypeResponseModel.Data> finalDat = data;
        if (fullServiceTypeCount > 4) {
            showExpandableButton();
        } else {
            hideExpandableButton();
        }
        rideTypeAdapter = new RideTypeAdapter(MainActivity.this, finalDat, binding.mainPageContentLayout.serviceListLayout.servicecontentLayout);
        rideTypeRecyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 4, RecyclerView.VERTICAL, false));
        rideTypeRecyclerView.setNestedScrollingEnabled(false);
        rideTypeRecyclerView.setAdapter(rideTypeAdapter);
        currentState = Constants.HomePageState.SERVICE_TYPE_LISTED;
        rideTypeAdapter.setOnClickListener(new onClickListener() {
            @Override
            public void onClicked(int position) {
                selectedRideTypeDetails = finalDat.get(position);
                selectedrideType = "" + finalDat.get(position).getId();
                if (position > 3) {
                    hideExpandableButton();
                } else {
                    showExpandableButton();
                }
            }
        });

    }

    private void showExpandableButton() {
        if (fullServiceTypeCount > 4)
            if (binding.mainPageContentLayout.serviceListLayout.expandToggleButton.getVisibility() == View.GONE)
                animationHelper.slideAndSHow(binding.mainPageContentLayout.serviceListLayout.expandToggleButton, defaultLayoutTransitionScreens, 0, 0, 300, 0);
    }

    private void hideExpandableButton() {
        if (binding.mainPageContentLayout.serviceListLayout.expandToggleButton.getVisibility() == View.VISIBLE)
            animationHelper.slideAndHide(binding.mainPageContentLayout.serviceListLayout.expandToggleButton, defaultLayoutTransitionScreens, 0, 0, 0, 500);
    }


    private void addCustomMarker(final LatLng point, final int icon, final boolean isFromTracking) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.anchor(0.5f, 0.5f);
                markerOptions.position(point);
                markerOptions.icon(ImageUtils.getMarkerIconFromDrawable(icon));
                if (icon == R.drawable.destination_tracker_icon) {
                    if (!isFromTracking) {

                        View myContentsView = LayoutInflater.from(MainActivity.this).inflate(R.layout.map_custom_info_window_with_eta, null);
                        TextView etaTime = myContentsView.findViewById(R.id.etaTime);
                        TextView address = myContentsView.findViewById(R.id.address);
                        ImageView markerIcon = myContentsView.findViewById(R.id.markerIcon);
                        View bgCardView = myContentsView.findViewById(R.id.bgCardView);
                        bgCardView.setVisibility(View.GONE);
                        etaTime.setVisibility(View.VISIBLE);

                        address.setText(destinationaddress);
                        etaTime.setText(expectedTextValue.replace(" ", "\n"));

                        markerIcon.setImageDrawable(getResources().getDrawable(R.drawable.destination_tracker_icon));
                        IconGenerator generator = new IconGenerator(MainActivity.this);
                        ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.transparent));
                        generator.setBackground(colorDrawable);
                        generator.setContentView(myContentsView);
                        Bitmap icon = generator.makeIcon();
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                        destinationMarker = mMap.addMarker(markerOptions);
                        destinationMarker.setTag(false);


                    } else {
                        destinationMarker = null;
                        mMap.addMarker(markerOptions);
                    }
                } else {

                    if (isFromTracking) {
                        destinationMarker = null;
                    }

                    View myContentsView = LayoutInflater.from(MainActivity.this).inflate(R.layout.map_custom_info_window_with_eta, null);

                    TextView etaTime = myContentsView.findViewById(R.id.etaTime);
                    TextView address = myContentsView.findViewById(R.id.address);
                    View bgCardView = myContentsView.findViewById(R.id.bgCardView);
                    bgCardView.setVisibility(View.VISIBLE);
                    etaTime.setVisibility(View.GONE);
                    address.setText(sourceaddress);
                    ImageView markerIcon = myContentsView.findViewById(R.id.markerIcon);

                    markerIcon.setImageDrawable(getResources().getDrawable(R.drawable.source_tracker_icon));
                    IconGenerator generator = new IconGenerator(MainActivity.this);
                    ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.transparent));
                    generator.setBackground(colorDrawable);
                    generator.setContentView(myContentsView);
                    Bitmap icon = generator.makeIcon();
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                    sourceMarker = mMap.addMarker(markerOptions);
                    sourceMarker.setTag(true);

                }
            }
        });
    }

    private void addVehicleCustomMarker(final LatLng point) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.anchor(0.5f, 0.5f);
                markerOptions.flat(true);
                markerOptions.position(point);
                markerOptions.icon(ImageUtils.getMarkerIconFromDrawable(R.drawable.ic_car_marker));
                myVehicleMarker = mMap.addMarker(markerOptions);
            }
        });

    }


    private void animatePolyLine(List<LatLng> route) {
        animationHelper.animatePolyLine(blackPolyline, route);
    }

    private void generateGreyPolyline(List<LatLng> route) {

        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.width(9);
        lineOptions.color(Color.GRAY);
        lineOptions.startCap(new SquareCap());
        lineOptions.endCap(new SquareCap());
        lineOptions.jointType(ROUND);
        lineOptions.addAll(route);
        mMap.addPolyline(lineOptions);
    }

    private void generateBlackPolyline(List<LatLng> route) {
        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.width(9);
        lineOptions.color(Color.BLACK);
        lineOptions.startCap(new SquareCap());
        lineOptions.endCap(new SquareCap());
        lineOptions.jointType(ROUND);
        if (currentBookingId.length() != 0) {
            lineOptions.addAll(route);
        }

        blackPolyline = mMap.addPolyline(lineOptions);
        blackPolyline.setZIndex(2);


    }


    private void generateTrackBlackPolyline(List<LatLng> route) {
        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.width(9);
        lineOptions.color(Color.BLACK);
        lineOptions.startCap(new SquareCap());
        lineOptions.endCap(new SquareCap());
        lineOptions.jointType(ROUND);
        lineOptions.addAll(route);
        blackTrackingPolyline = mMap.addPolyline(lineOptions);
        blackTrackingPolyline.setZIndex(2);


    }


    public void addMarker(LatLng latLng, int rideType, ProviderDetailResponse.Data providerDetails) {

        Marker mCarMarker;
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng).flat(true);
        if (rideType == Constants.RideType.RIDE_TYPE_BIKE) {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.getBitmapFromVectorDrawable(MainActivity.this, R.drawable.ic_car_marker)));
        } else if (rideType == Constants.RideType.RIDE_TYPE_CAR) {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageUtils.getBitmapFromVectorDrawable(MainActivity.this, R.drawable.ic_car_marker)));
        }
        mCarMarker = mMap.addMarker(markerOptions);
        mCarMarker.setAnchor(0.5f, 0.5f);
        mCarMarker.setRotation(providerDetails.getBearing());
        mCarMarker.setTag(providerDetails);
        markers.add(mCarMarker);
    }


    private void setRating(float rating, LinearLayout starContent, int size) {

        ImageView imageView = null;
        try {
            starContent.removeAllViewsInLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int j = 1; j <= 5; j++) {
            imageView = new ImageView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(size, size);
            if (1 < j) {
                layoutParams.setMargins(5, 0, 0, 0);
            }
            imageView.setLayoutParams(layoutParams);
            int s = Math.round(rating);
            if (j <= s) {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.golden_star));
            } else {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.grey_star));
            }

            starContent.addView(imageView);
        }

    }


    private void getCompleteAddress(double latitude, double longitude) {
        String url = buildUrl(latitude, longitude);

        InputForAPI inputForAPI = new InputForAPI(MainActivity.this);
        inputForAPI.setUrl(url);
        getAddressDetails(inputForAPI);
        srclat = latitude;
        srclng = longitude;
    }


    private String buildUrl(double latitude, double longitude) {
        return viewModel.getAddresDetailsUrl(latitude, longitude);
    }


    private void getAddressDetails(InputForAPI inputForAPI) {
        viewModel.getAddresDetails(inputForAPI).observe(
                this, new Observer<GooglePlaceResponseModel>() {
                    @Override
                    public void onChanged(@Nullable GooglePlaceResponseModel googlePlaceResponseModel) {
                        if (googlePlaceResponseModel != null) {
                            try {
                                if (!googlePlaceResponseModel.getResults().isEmpty()) {
                                    if (googlePlaceResponseModel.getResults().size() > 0) {
                                        handleAddressResponse(googlePlaceResponseModel);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        );
    }

    private void handleAddressResponse(GooglePlaceResponseModel googlePlaceResponseModel) {
        setSourceText(googlePlaceResponseModel.getResults().get(0).getFormattedAddress());
        srcCountryCode = viewModel.getCountryShortCode(googlePlaceResponseModel.getResults().get(0).getAddressComponents());
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == COARSE_LOCATION_PERMISSIONS) {
            try {

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    promptEnableGpsDialog();
                } else {
                    boolean showRationale = shouldShowRequestPermissionRationale(permissions[0]);
                    if (!showRationale) {
                        showPermissionDialog();
                    } else {
                        //not checked "never ask again"
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Constants.IntentPermissionCode.CALL_PHONE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callMobile();
            } else {
                Utils.displayError(findViewById(android.R.id.content), getString(R.string.need_call_permission));
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        setNavigationData();


    }


    private void animateCameraToLocation(double latitude, double longitude) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude));
        mMap.animateCamera(center);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }


    private void moveSearchPlacesActivity(int requestCode, double currentLatitude, double currentLongitude) {
        Intent intent = new Intent(MainActivity.this, SearchPlacesActivity.class);
        intent.putExtra(Constants.IntentKeys.LATITUDE, "" + currentLatitude);
        intent.putExtra(Constants.IntentKeys.LONGITUDE, "" + currentLongitude);
        intent.putExtra(Constants.IntentKeys.REQUEST_CODE, requestCode);
        startActivityForResult(intent, requestCode);
    }


    public void onSourceClicked(View view) {
        if (sourceText.getText().toString().length() == 0) {
            isSourceSelected = true;
            moveSearchPlacesActivity(Constants.IntentPermissionCode.SOURCE_REQUEST_CODE, currentLatitude, currentLongitude);
        } else {
            if (searchLayout.sourceParent.getCardElevation() > 0) {
                moveSearchPlacesActivity(Constants.IntentPermissionCode.SOURCE_REQUEST_CODE, currentLatitude, currentLongitude);
            } else {
                onSourceSelected();
            }
        }
    }

    public void onSourceSelected() {
        searchLayout.sourceParent.setCardElevation(40);
        searchLayout.destinationParent.setCardElevation(0);
        isSourceSelected = true;
    }


    public void onDestinationSelected() {
        searchLayout.destinationParent.setCardElevation(40);
        searchLayout.sourceParent.setCardElevation(0);
        isDestinationSelected = true;

    }


    public void onDestinationClicked(View view) {

        if (destinationText.getText().toString().length() == 0) {
            isDestinationSelected = true;
            moveSearchPlacesActivity(Constants.IntentPermissionCode.DESTINATION_REQUEST_CODE, currentLatitude, currentLongitude);
        } else {
            if (searchLayout.destinationParent.getCardElevation() > 0) {
                moveSearchPlacesActivity(Constants.IntentPermissionCode.DESTINATION_REQUEST_CODE, currentLatitude, currentLongitude);
            } else {
                onDestinationSelected();
            }
        }

    }

    public void onExpandClicked(View view) {
        if (rideTypeAdapter != null) {
            if (rideTypeAdapter.getItemCount() > 0) {
                if (binding.mainPageContentLayout.serviceListLayout.expandText.getText().toString().equalsIgnoreCase(getResources().getString(R.string.more))) {
                    expandRideTypeRecyclerView();
                } else {
                    collapseServiceTypeRecyclerView();
                }
            }
        }
    }

    private void collapseServiceTypeRecyclerView() {
        binding.mainPageContentLayout.serviceListLayout.expandText.setText(getResources().getString(R.string.more));
        binding.mainPageContentLayout.serviceListLayout.expandImage.animate().rotation(360).start();
        rideTypeAdapter.removeValues();
    }

    private void expandRideTypeRecyclerView() {
        binding.mainPageContentLayout.serviceListLayout.expandText.setText(getResources().getString(R.string.less));
        binding.mainPageContentLayout.serviceListLayout.expandImage.animate().rotation(180).start();
        List<RideTypeResponseModel.Data> finalDat = remainingRideTypeList;
        rideTypeAdapter.addValues(finalDat);

    }

    public void onBookNowClicked(View view) {
        if (selectedrideType.length() > 0) {
            if (view != null) {
                selectedbookingType = Constants.BookingType.NOW.toString().toLowerCase();
            } else {
                selectedbookingType = Constants.BookingType.LATER.toString().toLowerCase();
            }
            hideServiceListLayout();
            showRideCustomisationLayout();
            setRideCustomisationValues();
        } else {
            displayError(getResources().getString(R.string.please_choose_valid_service_type));
        }
    }

    public void confirmBooking(View view) {
        if (selectedrideType.length() > 0) {
            buildPlaceBookingInputs();
        } else {
            Utils.displayError(findViewById(android.R.id.content), getResources().getString(R.string.please_choose_valid_service_type));
        }
    }

    private void setRideCustomisationValues() {
        binding.mainPageContentLayout.customisationLayout.estimatedAmount.setText(selectedRideTypeDetails.getPricetext());
        binding.mainPageContentLayout.customisationLayout.seatCount.setText("" + selectedRideTypeDetails.getCapacity());

    }

    public void showRideCustomisationLayout() {
        currentState = Constants.HomePageState.RIDE_CUSTOMSATION;
        animationHelper.slideAndSHow(binding.mainPageContentLayout.customisationLayout.getRoot(), defaultLayoutTransitionScreens, 0, 0, 300, 0);

    }

    public void onBookLaterClicked(View view) {
        if (selectedrideType.length() > 0) {
            showDatePicker();
        } else {
            displayError(getResources().getString(R.string.please_choose_valid_service_type));
        }
    }


    private void datePicker() {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        newCalendar = Calendar.getInstance(Locale.getDefault());

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        date_time = year + "-" + Utils.getDoubleDigit((monthOfYear + 1)) + "-" + Utils.getDoubleDigit(dayOfMonth);
                        newCalendar.set(Calendar.YEAR, year);
                        newCalendar.set(Calendar.MONTH, monthOfYear);
                        newCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        tiemPicker();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 10000);
        datePickerDialog.show();
    }


    private void tiemPicker() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker viedw, int hourOfDay, int minute) {

                        mHour = hourOfDay;
                        mMinute = minute;

                        Calendar datetime = Calendar.getInstance();
                        Calendar c = Calendar.getInstance();
                        datetime.set(Calendar.MONTH, newCalendar.get(Calendar.MONTH));
                        datetime.set(Calendar.DAY_OF_MONTH, newCalendar.get(Calendar.DAY_OF_MONTH));
                        datetime.set(Calendar.YEAR, newCalendar.get(Calendar.YEAR));
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.SECOND, 00);
                        datetime.set(Calendar.MINUTE, minute);
                        mSelectDate = new Date(datetime.getTimeInMillis());
                        Date mCurrentDate = new Date(c.getTimeInMillis());

                        if (mSelectDate.after(mCurrentDate)) {
                            newCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            newCalendar.set(Calendar.MINUTE, minute);
                            newCalendar.set(Calendar.SECOND, 00);
                            onBookNowClicked(null);
                        } else {
                            Toast.makeText(MainActivity.this, getResources().getString(R.string.selected_time_should_be_atleast_ten_minutes), Toast.LENGTH_SHORT).show();
                        }


                    }
                }, mHour, mMinute, false);


        timePickerDialog.show();
    }

    private void showDatePicker() {
        datePicker();
    }

    public void onSearchingClicked(View view) {
        if (isSearchDetailsExpanded) {
            collapseSearchDetails();
        } else {
            expandSearchDetails();
        }
    }

    private void expandSearchDetails() {
        isSearchDetailsExpanded = true;
        binding.mainPageContentLayout.driverSearchingLayout.searchDetailsContentLayout.setVisibility(View.VISIBLE);
        binding.mainPageContentLayout.driverSearchingLayout.searchDetailsHeader.setBackgroundColor(getResources().getColor(R.color.page_default_bg_color));

    }

    private void collapseSearchDetails() {
        isSearchDetailsExpanded = false;
        binding.mainPageContentLayout.driverSearchingLayout.searchDetailsContentLayout.setVisibility(View.GONE);
        binding.mainPageContentLayout.driverSearchingLayout.searchDetailsHeader.setBackgroundColor(Utils.getPrimaryColor(MainActivity.this));
    }


    public void onCancelClicked(View view) {
        showCancelConfirmation();
    }

    public void onMyLocationbuttonClicked(View view) {
        getLastKnownLocation(false);
        zoomToMyLocation(currentLatitude, currentLongitude, 15);
        getMyCelldID(currentLatitude, currentLongitude);
        if (locationEnabledialog.isShowing()) {
            locationEnabledialog.dismiss();
        }
    }

    public void onCallClicked(View view) {
        mobileNo = ongoingBookingResponse.getValue().getData().getProviderInfo().getMobile();
        placeCall();
    }

    private void placeCall() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, Constants.IntentPermissionCode.CALL_PHONE);
        } else {
            callMobile();
        }

    }

    private void callMobile() {
        Utils.log(TAG, "Mobile Number " + mobileNo.trim());
        String uri = "tel:" + mobileNo.trim();
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    public void onBookingAcceptedLayoutClicked(View view) {
        toggleTripAddressDetails();
    }

    public void hideTripDetails() {
        binding.mainPageContentLayout.bookingAcceptedLayout.tripAddressDetails.setVisibility(View.GONE);
//        binding.mainPageContentLayout.bookingAcceptedLayout.topArrow.animate().rotation(360).start();

    }


    public void showTripDetails() {
        binding.mainPageContentLayout.bookingAcceptedLayout.tripAddressDetails.setVisibility(View.VISIBLE);
//        binding.mainPageContentLayout.bookingAcceptedLayout.topArrow.animate().rotation(180).start();

    }

    private void toggleTripAddressDetails() {
        if (binding.mainPageContentLayout.bookingAcceptedLayout.tripAddressDetails.getVisibility() == View.VISIBLE) {
            hideTripDetails();
        } else {
            showTripDetails();

        }
    }

    public void onMessageClicked(View view) {
        sendMessage();

    }

    private void sendMessage() {

        String number = ongoingBookingResponse.getValue().getData().getProviderInfo().getMobile();
        String body = getString(R.string.where_are_you);
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setData(Uri.parse("smsto:" + Uri.encode(number)));
        smsIntent.putExtra("sms_body", body);
        startActivity(smsIntent);
    }

    private void shareTrip() {
        String shareBody = viewModel.getSharePageContent(ongoingBookingResponse.getValue());
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
    }

    public void onShareClicked(View view) {
        shareTrip();
    }

    public void onCancelClickedAfterAccepted(View view) {
        getCancelReasons();

    }

    private InputForAPI getCancelReasonsInput() {
        InputForAPI inputForAPI = new InputForAPI(MainActivity.this);
        inputForAPI.setUrl(UrlHelper.GET_CANCELLATION_AVAILABLE_REASONS);
        inputForAPI.setHeaders(ApiCall.getHeaders(MainActivity.this));
        return inputForAPI;
    }

    private void getCancelReasons() {
        viewModel.getCancelReasons(getCancelReasonsInput()).observe(this, new Observer<CancelReasonsModel>() {
            @Override
            public void onChanged(CancelReasonsModel cancelReasonsModel) {
                if (cancelReasonsModel != null) {
                    if (cancelReasonsModel.getError()) {
                        Utils.displayError(findViewById(android.R.id.content), cancelReasonsModel.getMsg());
                    } else {
                        showCancelReasonDialog(cancelReasonsModel);
                    }
                }
            }
        });
    }


    private void showCancelReasonDialog(CancelReasonsModel cancelReasonsModel) {
        final Dialog dialog = new Dialog(this, R.style.FullScreenDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialogBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_cancel_reason, null, false);
        dialog.setContentView(dialogBinding.getRoot());
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.setGravity(Gravity.BOTTOM);
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        dialogBinding.closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialogBinding.updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    buildCancelBookingInput(cancelReasonAdapter.getSelectedReason(), dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
        List<CancelReasonsModel.Data> data = cancelReasonsModel.getData();
        data.get(0).setSelected(true);
        for (int i = 1; i < data.size(); i++) {
            data.get(i).setSelected(false);
        }
        cancelReasonAdapter = new CancelReasonAdapter(this, cancelReasonsModel.getData());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        dialogBinding.cancelReasonRecyclerView.setLayoutManager(linearLayoutManager);
        dialogBinding.cancelReasonRecyclerView.setAdapter(cancelReasonAdapter);

    }


    private void buildCancelBookingInput(String reason, final Dialog dialog) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.BOOKING_NUMBER, currentBookingId);
        jsonObject.put(Constants.ApiKeys.REASON, reason);
        InputForAPI inputForAPI = new InputForAPI(this);
        inputForAPI.setUrl(UrlHelper.CANCEL_B0OKING);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(ApiCall.getHeaders(this));
        commonViewModel.flagCheckResponse(inputForAPI).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(FlagCheckResponseModel flagCheckResponseModel) {
                if (flagCheckResponseModel != null) {
                    if (flagCheckResponseModel.isError()) {
                        Utils.displayError(dialog.findViewById(android.R.id.content), flagCheckResponseModel.getMsg());
                    } else {
                        selectedrideType = "";
                        dialog.dismiss();
                        getOngoingBooking();
                    }
                }
            }
        });
    }


    public void onCouponVerifiedClick(View view) {
        Utils.dismissKeyboard(MainActivity.this);
        if (binding.mainPageContentLayout.customisationLayout.verifyText.getText().toString().equalsIgnoreCase(getString(R.string.verify))) {
            if (binding.mainPageContentLayout.customisationLayout.couponCode.getText().toString().trim().length() > 0) {
                verifyCouponCode();
            } else {
                displayError(getResources().getString(R.string.please_enter_valid_promo_code));
            }
        } else {
            removeCouponCode();

        }
    }

    private void updateVerifiedCouponUi(String message) {
        showVerifyIcon();
        binding.mainPageContentLayout.customisationLayout.verifyText.setText(getString(R.string.remove));
        binding.mainPageContentLayout.customisationLayout.couponSuccessMessage.setVisibility(View.VISIBLE);
        binding.mainPageContentLayout.customisationLayout.couponSuccessMessage.setText(message);
    }

    private void updateRemoveCouponUi() {
        binding.mainPageContentLayout.customisationLayout.couponSuccessMessage.setVisibility(View.GONE);
        binding.mainPageContentLayout.customisationLayout.couponSuccessMessage.setText("");
        hideVerifyIcon();
        binding.mainPageContentLayout.customisationLayout.verifyText.setText(getString(R.string.verify));
    }

    private void removeCouponCode() {

        setCouponData("", "", "");
        updateRemoveCouponUi();
    }

    private void verifyCouponCode() {
        final InputForAPI inputForAPI = new InputForAPI(MainActivity.this);
        inputForAPI.setJsonObject(getParamsForVerifyCoupon());
        inputForAPI.setHeaders(ApiCall.getHeaders(MainActivity.this));
        inputForAPI.setUrl(UrlHelper.VERIFY_COUPON);
        viewModel.verifyCoupon(inputForAPI).observe(this, new Observer<CouponVerifyResponseModel>() {
            @Override
            public void onChanged(CouponVerifyResponseModel couponSuccessResponseModel) {
                if (couponSuccessResponseModel.getError()) {
                    handleFailureResponse(couponSuccessResponseModel.getMsg());
                } else {
                    setCouponData(couponSuccessResponseModel.getData().getAmount(), "" + couponSuccessResponseModel.getData().getPromoCodeRedeemId(), binding.mainPageContentLayout.customisationLayout.couponCode.getText().toString());
                    updateVerifiedCouponUi(commonViewModel.getCombinedStrings(couponSuccessResponseModel.getData().getAmount(), " ", getResources().getString(R.string.will_be_deducted)));
                }
            }
        });
    }

    public void setCouponData(String amount, String id, String code) {
        redeemedCouponAmount = amount;
        redeemedCouponCodeId = id;
        redeemedCouponCode = code;
        if (id.length() > 0) {
            isCouponApplied = "yes";
        } else {
            isCouponApplied = "no";
        }
    }


    private void handleFailureResponse(String msg) {
        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }
    }


    private JSONObject getParamsForVerifyCoupon() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.ApiKeys.COUPON, binding.mainPageContentLayout.customisationLayout.couponCode.getText().toString());
            jsonObject.put(Constants.ApiKeys.AMOUNT, "" + selectedRideTypeDetails.getPricevalue());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void showVerifyIcon() {
        binding.mainPageContentLayout.customisationLayout.verifiedSymbol.setVisibility(View.VISIBLE);
        binding.mainPageContentLayout.customisationLayout.appliedText.setVisibility(View.VISIBLE);


    }

    private void hideVerifyIcon() {
        binding.mainPageContentLayout.customisationLayout.verifiedSymbol.setVisibility(View.GONE);
        binding.mainPageContentLayout.customisationLayout.appliedText.setVisibility(View.GONE);
    }

    public void onChoosePaymentClicked(View view) {
        moveChoosePaymentActivity(selectedRideTypeDetails.getPricevalue());
    }

    public void onSosClicked(View view) {
        mobileNo = sharedHelper.getAppConfiguration().getData().getAuthConfig().getSos_number();
        placeCall();
    }

    public void onChangeLanguage(View view) {
        onChangeLanguageClicked();
    }

    private void reloadApp() {
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void setPhoneLanguage() {


        Resources res = getResources();
        Configuration conf = res.getConfiguration();
        Locale locale;
        if (sharedHelper.getSelectedLanguage().length() > 0) {
            locale = new Locale(sharedHelper.getSelectedLanguage().toLowerCase());
        } else {
            locale = new Locale("en");
        }

        Locale.setDefault(locale);
        conf.setLocale(locale);
        getApplicationContext().createConfigurationContext(conf);


        DisplayMetrics dm = res.getDisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(new LocaleList(locale));
        } else {
            conf.locale = locale;
        }
        res.updateConfiguration(conf, dm);
    }


    private void onChangeLanguageClicked() {


        ChangeLanguageDialog changeLanguageDialog = new ChangeLanguageDialog(MainActivity.this);
        changeLanguageDialog.setDialogChanged(new DialogChanged() {
            @Override
            public void onLanguageChanged() {
                setPhoneLanguage();
                reloadApp();
            }
        });
        changeLanguageDialog.show();


    }


    private void showPermissionDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(true);
        alertDialog.setMessage(getResources().getString(R.string.alert_content));
        alertDialog.setTitle(getResources().getString(R.string.alert_header));
        alertDialog.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                intentToPermissionSettings();
            }
        });

        alertDialog.show();

    }


    private void intentToPermissionSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    private void listnerOfflineProviders() {
        socket.on(UrlHelper.SOCKET_UPDATE_PROVIDER_OFFLINE, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (args != null) {
                            Gson gson = new Gson();
                            ProviderOfflineResponse checkResponse = gson.fromJson(args[0].toString(), ProviderOfflineResponse.class);
                            handleOfflineResponse(checkResponse.getData().getValue());
                        }
                    }
                });
            }
        });
    }

    private void handleOfflineResponse(ArrayList<ProviderOfflineResponse.providerLocation> checkResponse) {
        if (checkResponse != null && checkResponse.size() != 0) {
            for (ProviderOfflineResponse.providerLocation value : checkResponse) {
                if (value.getProviderid() != 0) {
                    for (Marker marker : markers) {
                        ProviderDetailResponse.Data data = (ProviderDetailResponse.Data) marker.getTag();
                        if (data != null) {
                            if (value.getProviderid() == data.getProviderId()) {
                                marker.remove();
                            }
                        }
                    }
                }
            }
        }
    }
}
