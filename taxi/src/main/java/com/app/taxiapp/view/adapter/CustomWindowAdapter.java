package com.app.taxiapp.view.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.app.taxiapp.R;
import com.app.taxiapp.helpers.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import okhttp3.internal.Util;


public class CustomWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private View myContentsView;
    private String expectedTextValue;
    private String sourceValue;
    private String destinationValue;
    private String TAG = CustomWindowAdapter.class.getSimpleName();
    private boolean isSource;
    LayoutInflater inflater;

    public CustomWindowAdapter(Activity context, String expectedTextValue, String sourceValue, String destinationValue) {
        inflater = context.getLayoutInflater();
        this.expectedTextValue = expectedTextValue;
        this.sourceValue = sourceValue;
        this.destinationValue = destinationValue;


    }

    @Override
    public View getInfoWindow(Marker marker) {
        isSource = (boolean) marker.getTag();
        if (isSource) {
            myContentsView = inflater.inflate(R.layout.map_custom_info_window_with_eta, null);
            TextView etaTime = myContentsView.findViewById(R.id.etaTime);
            TextView address = myContentsView.findViewById(R.id.address);
            View bgCardView = myContentsView.findViewById(R.id.bgCardView);
            bgCardView.setVisibility(View.VISIBLE);
            etaTime.setVisibility(View.GONE);
            address.setText(sourceValue);

            return myContentsView;
        } else {
            myContentsView = inflater.inflate(R.layout.map_custom_info_window_with_eta, null);


            TextView etaTime = myContentsView.findViewById(R.id.etaTime);
            TextView address = myContentsView.findViewById(R.id.address);
            View bgCardView = myContentsView.findViewById(R.id.bgCardView);
            bgCardView.setVisibility(View.GONE);
            etaTime.setVisibility(View.VISIBLE);
            etaTime.setText(expectedTextValue);
            address.setText(destinationValue);
            return myContentsView;

        }

    }

    @Override
    public View getInfoContents(Marker marker) {

        return null;
    }
}
