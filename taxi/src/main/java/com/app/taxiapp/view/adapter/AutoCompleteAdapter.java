package com.app.taxiapp.view.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.taxiapp.R;
import com.app.taxiapp.helpers.interfaces.onClickListener;
import com.app.taxiapp.model.apiresponsemodel.autocompleteresponse.Prediction;

import java.util.List;


public class AutoCompleteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private onClickListener onClickListener;
    private List<Prediction> Places;
    private String fromWhere;


    public AutoCompleteAdapter(Context context, List<Prediction> modelsArrayList, String fromWhere) {
        this.context = context;
        Places = modelsArrayList;
        this.fromWhere = fromWhere;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_location_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new Location(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

        Location holder = (Location) viewHolder;


        holder.name.setText(Places.get(position).getTerms().get(0).getValue());
        holder.location.setText(Places.get(position).getDescription());

        if (fromWhere.equalsIgnoreCase("from")) {
            holder.typeImage.setImageResource(R.drawable.map_marker);
            holder.typeImage.setColorFilter(ContextCompat.getColor(context, R.color.positive_button_bg_color), PorterDuff.Mode.SRC_IN);
        } else if (fromWhere.equalsIgnoreCase("to")) {
            holder.typeImage.setImageResource(R.drawable.map_marker);
            holder.typeImage.setColorFilter(ContextCompat.getColor(context, R.color.negative_button_bg_color), android.graphics.PorterDuff.Mode.SRC_IN);
        } else {
            holder.typeImage.setImageResource(R.drawable.recent);
            holder.typeImage.setColorFilter(ContextCompat.getColor(context, R.color.action_text_color), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return Places.size();
    }

    public void clear() {
        Places.clear();
        notifyDataSetChanged();
    }

    public void setOnClickListner(onClickListener onClickListner) {
        this.onClickListener = onClickListner;
    }


    class Location extends RecyclerView.ViewHolder {
        TextView name, location;
        ImageView typeImage;

        Location(View view) {
            super(view);
            name = view.findViewById(R.id.place_name);
            location = view.findViewById(R.id.place_detail);
            typeImage = view.findViewById(R.id.typeImage);
        }
    }
}