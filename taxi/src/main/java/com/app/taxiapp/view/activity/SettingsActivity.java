package com.app.taxiapp.view.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivitySettingsBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.viewmodel.CommonViewModel;

public class SettingsActivity extends BaseActivity {

    private ActivitySettingsBinding binding;
    CommonViewModel commonViewModel;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        sharedHelper = new SharedHelper(SettingsActivity.this);
        setData();
    }

    private void setData() {
        binding.setUser(commonViewModel.getUserDetails());
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_to_bottom);
    }


    public void onLogoutClicked(View view) {
        showLogOutConfirmation();
    }

    private void logOutFromAPP() {
//        sharedHelper.setIsUserLoggedIn(false);
//        Intent intent = new Intent(this, SignInActivity.class);
//        intent.putExtra(Constants.IntentKeys.SKIP,true);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//        finish();
    }


    private void showLogOutConfirmation() {

        final Dialog dialog = getConfirmationDialog();
        alertDialogBinding.dialogContent.setText(getResources().getString(R.string.are_you_sure_you_want_to_logout));
        alertDialogBinding.positiveButtonText.setText(getResources().getString(R.string.logout));
        alertDialogBinding.negativeButtonText.setText(getResources().getString(R.string.no));
        alertDialogBinding.positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                logOutFromAPP();
            }
        });

        alertDialogBinding.negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    public void onBackPressed(View view) {
        onBackPressed();
    }

    public void onViewProfileClicked(View view) {
        Intent intent = new Intent(SettingsActivity.this, EditProfileActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.no_animation);
    }
}
