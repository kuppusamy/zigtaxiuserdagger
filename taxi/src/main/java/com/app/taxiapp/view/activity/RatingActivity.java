package com.app.taxiapp.view.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivityRatingBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.model.apiresponsemodel.CurrentBookingResponseModel;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.networkcall.ImageLoader;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.viewmodel.CommonViewModel;

import org.json.JSONException;
import org.json.JSONObject;

public class RatingActivity extends AppCompatActivity {
    ActivityRatingBinding binding;
    CurrentBookingResponseModel.Data data;
    CommonViewModel commonViewModel;
    ImageLoader imageLoader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(RatingActivity.this, R.layout.activity_rating);
        data = getIntent().getParcelableExtra(Constants.IntentKeys.DATA);
        imageLoader = new ImageLoader(RatingActivity.this);
        binding.setCurrentBooking(data);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        imageLoader.load(data.getProviderInfo().getImage(), binding.userImage, getResources().getDrawable(R.drawable.profile_placeholder));
    }

    public void onRatingDone(View view) {
        try {
            buildUpdateRatingInputs(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        onSkipClicked(null);
    }

    private void onRatingCompleted() {
        setResult(RESULT_OK);
        finish();
    }


    private void buildUpdateRatingInputs(boolean includeRating) throws JSONException {
        InputForAPI inputForAPI = new InputForAPI(RatingActivity.this);
        inputForAPI.setUrl(UrlHelper.UPDATE_RATING);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.BOOKING_NUMBER, data.getId());

        if (includeRating) {
            jsonObject.put(Constants.ApiKeys.COMMENTS, binding.comments.getText().toString());
            jsonObject.put(Constants.ApiKeys.RATING, "" + binding.ratingBar.getRating());
            jsonObject.put(Constants.ApiKeys.ACTION, Constants.RatingType.RATED);
        } else {
            jsonObject.put(Constants.ApiKeys.ACTION, Constants.RatingType.SKIPPED);
        }

        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(ApiCall.getHeaders(RatingActivity.this));
        updateIncomingBookingStatus(inputForAPI);
    }

    private void updateIncomingBookingStatus(final InputForAPI inputForAPI) {
        commonViewModel.flagCheckResponse(inputForAPI).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(FlagCheckResponseModel flagCheckResponseModel) {
                if (flagCheckResponseModel.isError()) {
                    onRatingFailed(flagCheckResponseModel.getMsg());
                } else {
                    onRatingCompleted();
                }
            }
        });
    }

    private void onRatingFailed(String message) {
        Utils.displayError(findViewById(android.R.id.content), message);
    }


    public void onSkipClicked(View view) {
        try {
            buildUpdateRatingInputs(false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
