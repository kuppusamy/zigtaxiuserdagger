package com.app.taxiapp.view.adapter;

import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;

import com.app.taxiapp.networkcall.ImageLoader;

public class BindingAdapter {
    @androidx.databinding.BindingAdapter(value = {"loadImage", "error"}, requireAll = false)
    public static void loadImage(View view, String url, Drawable error) {
        ImageLoader imageLoader = new ImageLoader(view.getContext());
        imageLoader.load(url, (ImageView) view, error);
    }

    @androidx.databinding.BindingAdapter(value = {"loadByteImage", "error"}, requireAll = false)
    public static void loadByteImage(View view, String url, Drawable error) {
        try {
            ImageLoader imageLoader = new ImageLoader(view.getContext());
            byte[] imageByteArray = Base64.decode(url, Base64.DEFAULT);
            imageLoader.loadBitmap(imageByteArray, (ImageView) view, error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
