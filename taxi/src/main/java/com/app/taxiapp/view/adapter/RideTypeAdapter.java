package com.app.taxiapp.view.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Handler;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.taxiapp.helpers.interfaces.onClickListener;
import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ItemRideTypeListBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.model.apiresponsemodel.RideTypeResponseModel;
import com.app.taxiapp.view.activity.RideDetailsLayout;

import java.util.List;


import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class RideTypeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<RideTypeResponseModel.Data> rideTypeList;
    private LayoutInflater layoutInflater;
    private onClickListener onClickListener;
    private boolean isClickTriggered = false;
    private CardView parentCard;

    public RideTypeAdapter(Activity context, List<RideTypeResponseModel.Data> trips, CardView parentCard) {
        this.context = context;
        this.rideTypeList = trips;
        this.parentCard = parentCard;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemRideTypeListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_ride_type_list, parent, false);
        return new RideTypeViewHolder(binding);
    }

    public void setOnClickListener(onClickListener onClickListner) {
        this.onClickListener = onClickListner;
    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final RideTypeViewHolder viewHolder = (RideTypeViewHolder) holder;
        final RideTypeResponseModel.Data rideType = rideTypeList.get(position);


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rideType.isSelected()) {
                    if (!isClickTriggered) {
                        isClickTriggered = true;
                        openRideActivity(viewHolder, rideType);
                    }
                } else {
                    onClickListener.onClicked(position);
                    for (int i = 0; i < rideTypeList.size(); i++) {
                        if (i == position) {
                            rideTypeList.get(position).setSelected(true);
                        } else {
                            if (rideTypeList.get(i).isSelected()) {
                                rideTypeList.get(i).setSelected(false);
                            }
                        }
                    }
                }
            }
        });

        viewHolder.binding.setRide(rideType);
        viewHolder.binding.executePendingBindings();
    }

    private void openRideActivity(RideTypeViewHolder viewHolder, RideTypeResponseModel.Data rideType) {
        Intent intent = new Intent(context, RideDetailsLayout.class);
        intent.putExtra(Constants.IntentKeys.RIDE, rideType);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Pair imageTransition = Pair.create(viewHolder.binding.imageLayout, context.getResources().getString(R.string.transition_image));
            Pair parentCardTransition = Pair.create(parentCard, context.getResources().getString(R.string.transition_card_parent));
            Pair nameTransition= Pair.create(viewHolder.binding.serviceName, context.getResources().getString(R.string.transition_name));
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(context, imageTransition, parentCardTransition,nameTransition);
            context.startActivity(intent, options.toBundle());
        } else {
            context.startActivity(intent);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isClickTriggered = false;
            }
        }, 500);

    }


    @Override
    public int getItemCount() {
        return rideTypeList.size();
    }

    public void addValues(List<RideTypeResponseModel.Data> remainingRideTypeList) {
        rideTypeList.addAll(remainingRideTypeList);
        notifyDataSetChanged();
    }

    public void removeValues() {
        if (rideTypeList.size() > 4) {
            rideTypeList.subList(4, rideTypeList.size()).clear();
        }
        notifyDataSetChanged();
    }


    class RideTypeViewHolder extends RecyclerView.ViewHolder {
        ItemRideTypeListBinding binding;

        RideTypeViewHolder(@NonNull ItemRideTypeListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
