package com.app.taxiapp.view.activity;

import android.os.Bundle;

import com.app.taxiapp.R;

public class TrustedContactsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trusted_contacts);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_to_bottom);
    }

}
