package com.app.taxiapp.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ItemsTripFareListBinding;
import com.app.taxiapp.model.apiresponsemodel.TripDetailsModel;
import com.app.taxiapp.view.activity.TripDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class TripFaresAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<TripDetailsModel.Receipt> trips = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public TripFaresAdapter(Context context, List<TripDetailsModel.Receipt> trips) {
        this.context = context;
        this.trips = trips;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemsTripFareListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.items_trip_fare_list, parent, false);
        return new YourTripsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        YourTripsViewHolder viewHolder = (YourTripsViewHolder) holder;
        viewHolder.binding.setFares(trips.get(position));
    }

    private void moveTripDetailsActivity() {
        Intent intent = new Intent(context, TripDetailsActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    class YourTripsViewHolder extends RecyclerView.ViewHolder {
        ItemsTripFareListBinding binding;

        YourTripsViewHolder(@NonNull ItemsTripFareListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
