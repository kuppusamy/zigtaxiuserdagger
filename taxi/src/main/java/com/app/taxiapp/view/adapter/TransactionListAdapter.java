package com.app.taxiapp.view.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ItemTransactionListBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.model.apiresponsemodel.WalletResponseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TransactionListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<WalletResponseModel.Transaction> transactionList;
    private LayoutInflater layoutInflater;
    Activity context;

    public TransactionListAdapter(Activity context, List<WalletResponseModel.Transaction> trips) {
        this.transactionList = trips;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemTransactionListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_transaction_list, parent, false);
        return new TransactionListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TransactionListViewHolder viewHolder = (TransactionListViewHolder) holder;
        viewHolder.binding.setValues(transactionList.get(position));
        if (transactionList.get(position).getTransactionType().equalsIgnoreCase(Constants.TransactionType.CREDIT)) {
            viewHolder.binding.transactionAmount.setTextColor(context.getResources().getColor(R.color.positive_button_bg_color));
            viewHolder.binding.transactionAmount.setText("+" +transactionList.get(position).getTransactionAmount());
        } else {
            viewHolder.binding.transactionAmount.setTextColor(context.getResources().getColor(R.color.negative_button_bg_color));
            viewHolder.binding.transactionAmount.setText("-" +transactionList.get(position).getTransactionAmount());
        }


        if (transactionList.get(position).getStatus().equalsIgnoreCase(Constants.TransactionType.FAILED)) {
            viewHolder.binding.failedStatus.setVisibility(View.VISIBLE);
        } else {
            viewHolder.binding.failedStatus.setVisibility(View.GONE);

        }
        if (position > 0) {
            if (sameDate(transactionList.get(position).getDate(), transactionList.get(position - 1).getDate())) {
                viewHolder.binding.dateTextView.setVisibility(View.GONE);
            } else {
                viewHolder.binding.dateTextView.setVisibility(View.VISIBLE);
            }

        } else {
            viewHolder.binding.dateTextView.setVisibility(View.VISIBLE);

        }


    }

    private boolean sameDate(String startDate, String endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date start = sdf.parse(startDate);
            Date end = sdf.parse(endDate);
            return start.equals(end);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }


    class TransactionListViewHolder extends RecyclerView.ViewHolder {
        ItemTransactionListBinding binding;

        TransactionListViewHolder(@NonNull ItemTransactionListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
