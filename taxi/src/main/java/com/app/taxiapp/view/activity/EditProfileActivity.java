package com.app.taxiapp.view.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivityEditProfileBinding;
import com.app.taxiapp.databinding.DialogUpdateProfileBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.ImagePicker;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.Callback;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.model.apiresponsemodel.ImageUploadResponseModel;
import com.app.taxiapp.model.apiresponsemodel.ProfileResponseModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;
import com.app.taxiapp.view.adapter.BindingAdapter;
import com.app.taxiapp.view.fragment.OTPFragment;
import com.app.taxiapp.viewmodel.CommonViewModel;
import com.app.taxiapp.viewmodel.EditProfileViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class EditProfileActivity extends BaseActivity {
    ActivityEditProfileBinding binding;
    DialogUpdateProfileBinding dialogBinding;
    EditProfileViewModel viewModel;
    CommonViewModel commonViewModel;
    private Dialog dialog;
    private boolean isPasswordVisibleOne = false;
    private boolean isPasswordVisibleTwo = false;
    private String TAG = EditProfileActivity.class.getSimpleName();
    private String imageUploadpath;
    SharedHelper sharedHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        viewModel = ViewModelProviders.of(this).get(EditProfileViewModel.class);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        sharedHelper = new SharedHelper(EditProfileActivity.this);
        getProfileData();
    }


    private void getProfileData() {
        InputForAPI inputForAPI = new InputForAPI(EditProfileActivity.this);
        inputForAPI.setUrl(UrlHelper.VIEW_PROFILE);
        inputForAPI.setHeaders(ApiCall.getHeaders(EditProfileActivity.this));
        viewModel.getProfileData(inputForAPI).observe(this, new Observer<ProfileResponseModel>() {
            @Override
            public void onChanged(ProfileResponseModel profileResponseModel) {
                handleGetProfileSuccessResponse(profileResponseModel);
            }
        });
    }

    private void handleGetProfileSuccessResponse(ProfileResponseModel profileResponseModel) {
        if (!profileResponseModel.getError()) {
            dismissDialog();
            viewModel.setUserData(profileResponseModel.getData());
            setData(profileResponseModel.getData());
        } else {
            onFailureResponse(profileResponseModel.getMsg());
        }
    }


    private void onFailureResponse(String msg) {

        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }

    }

    private void displayError(String error_message) {
        Utils.displayError(findViewById(android.R.id.content), error_message);
    }

    private void displayDialogError(String error_message) {
        if (dialog != null) {
            if (dialog.isShowing()) {
                Utils.displayError(dialog.findViewById(android.R.id.content), error_message);
            }
        }
    }

    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(EditProfileActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                getProfileData();
            }
        });
    }

    public Dialog getDialog() {
        Dialog dialog = new Dialog(EditProfileActivity.this, R.style.FullScreenDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialogBinding = DataBindingUtil.inflate(LayoutInflater.from(EditProfileActivity.this), R.layout.dialog_update_profile, null, false);
        dialog.setContentView(dialogBinding.getRoot());
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.setGravity(Gravity.BOTTOM);
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        dialog.show();
        return dialog;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_to_bottom);
    }

    private void setData(ProfileResponseModel.Data data) {
        binding.setUser(data);
        if (!sharedHelper.getAppConfiguration().getData().getAuthConfig().getAuth_type().equalsIgnoreCase(Constants.AuthType.PASSWORD)) {
            binding.passwordParent.setVisibility(View.GONE);
        } else {
            binding.passwordParent.setVisibility(View.VISIBLE);

        }

    }

    public void onBackPressed(View view) {
        onBackPressed();
    }


    public void onFirstNameClicked(View view) {
        showFirstNameDialog();
    }

    private void showFirstNameDialog() {
        dialog = getDialog();
        dialogBinding.textParent.setVisibility(View.VISIBLE);
        dialogBinding.phoneNumberParent.setVisibility(View.GONE);
        dialogBinding.passwordParent.setVisibility(View.GONE);
        dialogBinding.headingText.setText(getResources().getString(R.string.change_first_name));
        dialogBinding.titleText.setText(getResources().getString(R.string.first_name));
        dialogBinding.inputText.setText(commonViewModel.getFirstName());
        CardView updateButton = dialog.findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validInputs().equalsIgnoreCase(Constants.ValidationKey.TRUE)) {
                    if (dialogBinding.inputText.getText().toString().equalsIgnoreCase(commonViewModel.getFirstName())) {
                        dismissDialog();
                    } else {
                        updateFirstName(dialogBinding.inputText.getText().toString());
                    }
                } else {
                    if (validInputs().equalsIgnoreCase(Constants.ValidationKey.FALSE)) {
                        displayDialogError(getResources().getString(R.string.error_enter_first_name));
                    } else if (validInputs().equalsIgnoreCase(Constants.ValidationKey.MIN)) {
                        displayDialogError(getResources().getString(R.string.error_enter_first_name_min));
                    } else if (validInputs().equalsIgnoreCase(Constants.ValidationKey.MAX)) {
                        displayDialogError(getResources().getString(R.string.error_enter_first_name_max));
                    }
                }
            }
        });
    }

    public String validInputs() {
        String returnVal;
        if (dialogBinding.inputText.getText().toString().trim().length() == 0) {
            returnVal = Constants.ValidationKey.FALSE;
        } else if (dialogBinding.inputText.getText().toString().trim().length() < 3) {
            returnVal = Constants.ValidationKey.MIN;
        } else if (dialogBinding.inputText.getText().toString().trim().length() > 15) {
            returnVal = Constants.ValidationKey.MAX;
        } else {
            returnVal = Constants.ValidationKey.TRUE;
        }
        return returnVal;
    }


    private void showLastNameDialog() {

        dialog = getDialog();

        dialogBinding.textParent.setVisibility(View.VISIBLE);
        dialogBinding.phoneNumberParent.setVisibility(View.GONE);
        dialogBinding.passwordParent.setVisibility(View.GONE);
        dialogBinding.headingText.setText(getResources().getString(R.string.change_last_name));
        dialogBinding.titleText.setText(getResources().getString(R.string.last_name));
        dialogBinding.inputText.setText(commonViewModel.getLastName());
        CardView updateButton = dialog.findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validInputs().equalsIgnoreCase(Constants.ValidationKey.TRUE)) {
                    if (dialogBinding.inputText.getText().toString().trim().length() != 0) {
                        if (dialogBinding.inputText.getText().toString().equalsIgnoreCase(commonViewModel.getLastName())) {
                            dismissDialog();
                        } else {
                            updateLastName(dialogBinding.inputText.getText().toString());
                        }
                    } else {
                        displayDialogError(getResources().getString(R.string.error_enter_last_name));
                    }
                } else {

                    if (validInputs().equalsIgnoreCase(Constants.ValidationKey.FALSE)) {
                        displayDialogError(getResources().getString(R.string.error_enter_first_name));
                    } else if (validInputs().equalsIgnoreCase(Constants.ValidationKey.MIN)) {
                        displayDialogError(getResources().getString(R.string.error_enter_first_name_min));
                    } else if (validInputs().equalsIgnoreCase(Constants.ValidationKey.MAX)) {
                        displayDialogError(getResources().getString(R.string.error_enter_first_name_max));
                    }
                }
            }
        });
    }

    private void togglePasswordvisibilityOne(EditText passwordEdittext, ImageView passwordHolder) {
        if (!isPasswordVisibleOne) {
            passwordHolder.setImageResource(R.drawable.hide_password);
            passwordEdittext.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            isPasswordVisibleOne = true;
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        } else {
            isPasswordVisibleOne = false;
            passwordHolder.setImageResource(R.drawable.show_password);
            passwordEdittext.setTransformationMethod(new PasswordTransformationMethod());
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        }
    }

    private void togglePasswordvisibilityTwo(EditText passwordEdittext, ImageView passwordHolder) {
        if (!isPasswordVisibleTwo) {
            passwordHolder.setImageResource(R.drawable.hide_password);
            passwordEdittext.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            isPasswordVisibleTwo = true;
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        } else {
            isPasswordVisibleTwo = false;
            passwordHolder.setImageResource(R.drawable.show_password);
            passwordEdittext.setTransformationMethod(new PasswordTransformationMethod());
            passwordEdittext.setSelection(passwordEdittext.getText().length());
        }
    }


    private void showPhoneNumberDialog() {

        dialog = getDialog();

        dialogBinding.textParent.setVisibility(View.GONE);
        dialogBinding.phoneNumberParent.setVisibility(View.VISIBLE);
        dialogBinding.passwordParent.setVisibility(View.GONE);
        dialogBinding.headingText.setText(getResources().getString(R.string.change_phone_number));
        dialogBinding.phoneNumber.setText(commonViewModel.getUserMobile());
        dialogBinding.countryCodePicker.setCountryForPhoneCode(Integer.parseInt(commonViewModel.getUserCountryCode()));
        CardView updateButton = dialog.findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
                moveOTPFragment();
            }
        });
    }

    private void moveOTPFragment() {

        OTPFragment otpFragment = new OTPFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.ArgumentKeys.TYPE, Constants.OtpType.CHANGE_MOBILE_NUMBER);
        otpFragment.setArguments(bundle);
        otpFragment.replaceListener(new Callback() {
            @Override
            public void onSuccess(String type) {
                onBackPressed();
            }
        });
        Utils.navigateToFragment(R.id.fragmentContainer, otpFragment, getSupportFragmentManager());
    }

    private void showEmailDialog() {

        dialog = getDialog();
        dialogBinding.headingText.setText(getResources().getString(R.string.change_email));
        dialogBinding.titleText.setText(getResources().getString(R.string.email));
        dialogBinding.inputText.setText(binding.userEmail.getText().toString());

        dialogBinding.updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogBinding.inputText.getText().toString().equalsIgnoreCase(binding.userEmail.getText().toString())) {
                    dismissDialog();
                } else {
                    if (Utils.isValidEmail(dialogBinding.inputText.getText().toString())) {
                        updateEmail(dialogBinding.inputText.getText().toString());
                    } else {
                        displayDialogError(getResources().getString(R.string.error_enter_valid_email));
                    }
                }
            }
        });

        dialogBinding.textParent.setVisibility(View.VISIBLE);
        dialogBinding.phoneNumberParent.setVisibility(View.GONE);
        dialogBinding.passwordParent.setVisibility(View.GONE);
    }


    private void showChangePasswordDialog() {

        dialog = getDialog();
        dialogBinding.headingText.setText(getResources().getString(R.string.change_password));
        dialogBinding.titleText.setText(getResources().getString(R.string.email));
        dialogBinding.inputText.setText(commonViewModel.getUserName());
        dialogBinding.textParent.setVisibility(View.GONE);
        dialogBinding.phoneNumberParent.setVisibility(View.GONE);
        dialogBinding.passwordParent.setVisibility(View.VISIBLE);

        dialogBinding.oldPasswordHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                togglePasswordvisibilityOne(dialogBinding.oldPassword, dialogBinding.oldPasswordHolder);
            }
        });

        dialogBinding.newPasswordHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                togglePasswordvisibilityTwo(dialogBinding.newPassword, dialogBinding.newPasswordHolder);
            }
        });

        dialogBinding.updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isValidPassword(dialogBinding.oldPassword.getText().toString()).equalsIgnoreCase("true")) {
                    if (Utils.isValidPassword(dialogBinding.newPassword.getText().toString()).equalsIgnoreCase("true")) {
                        updatePassword(dialogBinding.oldPassword.getText().toString(), dialogBinding.newPassword.getText().toString());
                    } else {
                        dialogBinding.newPassword.setError(Utils.isValidPassword(dialogBinding.newPassword.getText().toString()));
                    }
                } else {
                    dialogBinding.oldPassword.setError(Utils.isValidPassword(dialogBinding.oldPassword.getText().toString()));
                }
            }
        });


    }


    private void updateEmail(String email) {
        updateInputs(getInputForAPI(Constants.ApiKeys.EMAIL, email), false);

    }

    private void updatePassword(String oldPassword, String newPassword) {
        isPasswordVisibleOne = false;
        isPasswordVisibleTwo = false;
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(oldPassword);
        jsonArray.put(newPassword);
        updateInputs(getInputForAPI(Constants.ApiKeys.PASSWORD, jsonArray.toString()), false);
    }


    private void dismissDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }

    }

    private void updateLastName(String lastName) {
        updateInputs(getInputForAPI(Constants.ApiKeys.LAST_NAME, lastName), false);
    }


    public void updateInputs(InputForAPI inputForAPI, final boolean isImage) {
        commonViewModel.flagCheckResponse(inputForAPI).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(FlagCheckResponseModel flagCheckResponseModel) {
                if (flagCheckResponseModel.isError()) {
                    if (isImage) {
                        displayError(flagCheckResponseModel.getMsg());
                    } else {
                        displayError(flagCheckResponseModel.getMsg());
                    }
                } else {
                    if (isImage) {
                        displayError(flagCheckResponseModel.getMsg());
                    } else {
                        displayError(flagCheckResponseModel.getMsg());
                    }
                    getProfileData();
                }
            }
        });
    }


    private void updateFirstName(String firstName) {
        updateInputs(getInputForAPI(Constants.ApiKeys.FIRST_NAME, firstName), false);

    }

    private JSONObject getInputData(String key, String value) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.ApiKeys.FILED_NAME, key);
        jsonObject.put(Constants.ApiKeys.DATA, value);
        return jsonObject;
    }


    private JSONObject getInputData(String key, JSONArray value) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.ApiKeys.FILED_NAME, key);
            jsonObject.put(Constants.ApiKeys.DATA, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    private InputForAPI getInputForAPI(String key, String value) {
        InputForAPI inputForAPI = new InputForAPI(EditProfileActivity.this);
        inputForAPI.setUrl(UrlHelper.UPDATE_PROFILE);
        inputForAPI.setHeaders(ApiCall.getHeaders(EditProfileActivity.this));
        try {
            inputForAPI.setJsonObject(getInputData(key, value));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return inputForAPI;
    }


    public void onLastNameClicked(View view) {
        showLastNameDialog();
    }

    public void onPhoneNumberClicked(View view) {
//        showPhoneNumberDialog();
        displayError(getResources().getString(R.string.phone_number_cannot_be_updated));

    }

    public void onEmailClicked(View view) {
        showEmailDialog();

    }

    public void onPasswordClicked(View view) {
        showChangePasswordDialog();

    }

    public void onCloseIconClicked(View view) {
        dismissDialog();
    }

    public void onProfileImageClicked(View view) {
        checkExternalPermission();

    }

    private void openPickerDialog() {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(this);
        startActivityForResult(chooseImageIntent, Constants.IntentPermissionCode.IMAGE_PICKER);
    }

    private void checkExternalPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                openPickerDialog();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.IntentPermissionCode.EXTERNAL_STORAGE_PERMISSION);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.IntentPermissionCode.EXTERNAL_STORAGE_PERMISSION)
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openPickerDialog();
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.IntentPermissionCode.IMAGE_PICKER:
                if (resultCode == RESULT_OK) {
                    imageUploadpath = ImagePicker.getImagePath(this, resultCode, data);
                    if (imageUploadpath != null) {
                        BindingAdapter.loadImage(binding.userImage, imageUploadpath, getResources().getDrawable(R.drawable.profile_placeholder));
                        uploadImage();
                    } else {
                        displayError(getResources().getString(R.string.unable_to_select_image));
                    }
                    break;
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }


    private void uploadImage() {
        viewModel.uploadImage(getInputForAPIUploadImage()).observe(this, new Observer<ImageUploadResponseModel>() {
            @Override
            public void onChanged(ImageUploadResponseModel imageUploadResponseModel) {
                if (imageUploadResponseModel.getError()) {
                    onFailureResponse(imageUploadResponseModel.getMsg());
                } else {
                    onUploadImageSuccessRespone(imageUploadResponseModel.getData().getImageUrl());
                }
            }
        });
    }

    private void onUploadImageSuccessRespone(String imageUrl) {
        updateInputs(getInputForAPI(Constants.ApiKeys.IMAGE, imageUrl), true);
    }

    private InputForAPI getInputForAPIUploadImage() {
        InputForAPI inputForAPI = new InputForAPI(EditProfileActivity.this);
        inputForAPI.setUrl(UrlHelper.FILE_UPLOAD);
        inputForAPI.setFile(new File(imageUploadpath));
        try {
            inputForAPI.setJsonObject(new JSONObject().put("type", "profileImage"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return inputForAPI;
    }


}
