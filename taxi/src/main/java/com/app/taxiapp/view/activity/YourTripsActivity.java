package com.app.taxiapp.view.activity;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ActivityYourTripsBinding;
import com.app.taxiapp.helpers.AnimationHelper;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.model.apiresponsemodel.YourTripsModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;
import com.app.taxiapp.view.adapter.YourTripsAdapter;
import com.app.taxiapp.viewmodel.YourTripsViewModel;

import java.util.ArrayList;
import java.util.List;

public class YourTripsActivity extends BaseActivity {
    private static final String TAG = YourTripsActivity.class.getSimpleName();
    ActivityYourTripsBinding binding;
    YourTripsViewModel yourTripsViewModel;
    YourTripsAdapter tripsAdapter;
    private List<YourTripsModel.Data> activeTripsModels = new ArrayList<>();
    private List<YourTripsModel.Data> pastTripsModels = new ArrayList<>();
    private List<YourTripsModel.Data> yourTripsModel = new ArrayList<>();
    RecyclerView tripsRecyclerView;
    AnimationHelper animationHelper;
    AnimationHelper.LoaderZigZagMain progressbar;
    boolean isPastTripsLoaded=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_your_trips);
        tripsRecyclerView = binding.tripsRecyclerView;
        yourTripsViewModel = ViewModelProviders.of(this).get(YourTripsViewModel.class);
        animationHelper = new AnimationHelper();
        initProgressBar();
        startProgress();
        getData();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    private void stopProgress() {
        progressbar.stop();
    }

    private void startProgress() {
        progressbar.start();
    }

    private void initProgressBar() {
        progressbar = new AnimationHelper.LoaderZigZagMain(binding.progressBarLay.progressBar, binding.progressBarLay.loadingParent);
    }

    private void getData() {
        yourTripsViewModel.getTrips(getInputForTripsLists()).observe(this, new Observer<YourTripsModel>() {
            @Override
            public void onChanged(YourTripsModel yourTripsModel) {
                stopProgress();
                if (yourTripsModel.getError()) {
                    onFailureResponse(yourTripsModel.getMsg());
                    handleEmptylayouts();
                } else {
                    onSuccessResponseReceived(yourTripsModel);
                }
            }
        });

    }

    private void onFailureResponse(String msg) {

        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }

    }

    private void displayError(String error_message) {
        Utils.displayError(findViewById(android.R.id.content), error_message);
    }

    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(YourTripsActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                getData();
            }
        });
    }

    private InputForAPI getInputForTripsLists() {
        InputForAPI inputForAPI = new InputForAPI(YourTripsActivity.this);
        inputForAPI.setUrl(UrlHelper.GET_TRIP_HISTORY);
        inputForAPI.setHeaders(ApiCall.getHeaders(YourTripsActivity.this));
        return inputForAPI;
    }

    private void onSuccessResponseReceived(YourTripsModel yourTripsModel) {

        if (yourTripsModel.getData().size() > 0) {
            handleValues(yourTripsModel);
        } else {
            handleEmptylayouts();
        }

    }

    private void handleEmptylayouts() {
        binding.tripsRecyclerView.setVisibility(View.GONE);
        binding.emptyLayout.setVisibility(View.VISIBLE);
    }

    private void handleValues(YourTripsModel yourTripsModel) {
        handleValueLayouts();
        for (int i = 0; i < yourTripsModel.getData().size(); i++) {
            if (yourTripsModel.getData().get(i).getIsActive().equalsIgnoreCase(Constants.TripState.YES)) {
                activeTripsModels.add(yourTripsModel.getData().get(i));
            } else {
                pastTripsModels.add(yourTripsModel.getData().get(i));
            }
        }

        initAdapters();
    }

    private void handleValueLayouts() {
        binding.tripsRecyclerView.setVisibility(View.VISIBLE);
        binding.emptyLayout.setVisibility(View.GONE);
    }

    private void initAdapters() {
        tripsAdapter = new YourTripsAdapter(YourTripsActivity.this, yourTripsModel);
        tripsRecyclerView.setLayoutManager(new LinearLayoutManager(YourTripsActivity.this, RecyclerView.VERTICAL, false));
        tripsRecyclerView.setNestedScrollingEnabled(false);
        tripsRecyclerView.setAdapter(tripsAdapter);
        tripsAdapter.changeValues(activeTripsModels);
        if (activeTripsModels.size() > 0) {
            handleValueLayouts();
        } else {
            handleEmptylayouts();
        }
    }

    private void setActiveData() {
        if (activeTripsModels.size() > 0) {
            handleValueLayouts();
            tripsAdapter.changeValues(activeTripsModels);
            if (isPastTripsLoaded) {
                isPastTripsLoaded = false;
                animationHelper.viewSlideFromRight(tripsRecyclerView, 600);
            }
        } else {
            handleEmptylayouts();
        }
    }

    private void setInActiveData() {
        if (pastTripsModels.size() > 0) {
            handleValueLayouts();
            tripsAdapter.changeValues(pastTripsModels);
            if (!isPastTripsLoaded) {
                isPastTripsLoaded = true;
                animationHelper.viewSlideFromRight(tripsRecyclerView, 600);
            }
        } else {
            handleEmptylayouts();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_to_bottom);
    }

    public void onCloseIconClicked(View view) {
        onBackPressed();
    }

    public void onActiveClicked(View view) {
        isPastTripsLoaded=false;
        binding.activeText.setAlpha(1.0f);
        binding.inActiveText.setAlpha(0.3f);
        setActiveData();
    }

    public void onInActiveClicked(View view) {

        binding.inActiveText.setAlpha(1.0f);
        binding.activeText.setAlpha(0.3f);
        setInActiveData();
    }
}
