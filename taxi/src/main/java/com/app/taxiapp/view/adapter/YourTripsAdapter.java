package com.app.taxiapp.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ItemTripsListBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.model.apiresponsemodel.YourTripsModel;
import com.app.taxiapp.networkcall.ImageLoader;
import com.app.taxiapp.view.activity.TripDetailsActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class YourTripsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<YourTripsModel.Data> trips;
    private LayoutInflater layoutInflater;
    private ImageLoader imageLoader;
    private String TAG = YourTripsAdapter.class.getSimpleName();
    boolean isClicked = false;

    public YourTripsAdapter(Activity context, List<YourTripsModel.Data> trips) {
        this.context = context;
        imageLoader = new ImageLoader(context);
        this.trips = trips;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemTripsListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_trips_list, parent, false);
        return new YourTripsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final YourTripsViewHolder viewHolder = (YourTripsViewHolder) holder;
        viewHolder.binding.setTrips(trips.get(position));
        viewHolder.binding.googleStaticMap.post(new Runnable() {
            @Override
            public void run() {
                int width = viewHolder.binding.googleStaticMap.getWidth();
                int height = viewHolder.binding.googleStaticMap.getHeight();
                imageLoader.load(getStaticUrl(trips.get(position), width, height),
                        viewHolder.binding.googleStaticMap, context.getResources().getDrawable(R.drawable.map_placeholder));
            }
        });

        viewHolder.binding.bookingDate.setText(Utils.getConvertedTime(trips.get(position).getCreatedTime()));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isClicked)
                    isClicked = true;
                viewHolder.binding.googleStaticMap.post(new Runnable() {
                    @Override
                    public void run() {
                        int width = viewHolder.binding.googleStaticMap.getWidth();
                        int height = viewHolder.binding.googleStaticMap.getHeight();
                        moveTripDetailsActivity(viewHolder, trips.get(position), getStaticUrl(trips.get(position), width, height));
                    }
                });


            }
        });
    }

    private String getStaticUrl(final YourTripsModel.Data data, final int width, final int height) {
        String value = UrlHelper.getPreFixUrlForStaticMap("" + width, "" +
                height) + data.getPostFixStaticMap();
        Utils.log(TAG, "MAp url:" + value);
        return value;
    }

    private void moveTripDetailsActivity(YourTripsViewHolder viewHolder, YourTripsModel.Data data, String staticUrl) {
        if (trips.size() != 0) {
            Intent intent = new Intent(context, TripDetailsActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(context, (View) viewHolder.binding.parentLayout, context.getResources().getString(R.string.static_map));
            intent.putExtra(Constants.IntentKeys.BOOKING_NUMBER, "" + data.getBookingNo());
            intent.putExtra(Constants.IntentKeys.STATIC_MAP_URL, staticUrl);
            context.startActivity(intent, options.toBundle());
            isClicked = false;
        } else {
            Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    public void changeValues(List<YourTripsModel.Data> newValues) {
        trips.clear();
        trips.addAll(newValues);
        notifyDataSetChanged();
    }

    class YourTripsViewHolder extends RecyclerView.ViewHolder {
        ItemTripsListBinding binding;

        YourTripsViewHolder(@NonNull ItemTripsListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }


}
