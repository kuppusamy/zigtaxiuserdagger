package com.app.taxiapp.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.app.taxiapp.NavigationInterFaceTaxi
import com.app.taxiapp.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_backtomain.*
import javax.inject.Inject


class EmptyTaxiActviity : DaggerAppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, EmptyTaxiActviity::class.java)
    }

    @Inject
    lateinit var navigationInterFaceTaxi: NavigationInterFaceTaxi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_backtomain)

        backToMain.setOnClickListener { navigationInterFaceTaxi.goToMain(this) }
    }

}