package com.app.taxiapp.view.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.DialogWalletSuccesLayoutBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.InternetCallback;
import com.app.taxiapp.helpers.interfaces.onCardSelected;
import com.app.taxiapp.helpers.interfaces.onDeleteClickListener;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.FlagCheckResponseModel;
import com.app.taxiapp.model.apiresponsemodel.SavedCardListResponseModel;
import com.app.taxiapp.model.apiresponsemodel.WalletResponseModel;
import com.app.taxiapp.networkcall.apicall.ApiCall;
import com.app.taxiapp.networkcall.apicall.InputForAPI;
import com.app.taxiapp.networkcall.apicall.NoInternetDialog;
import com.app.taxiapp.paymentgateways.Stripe.EphemeralKeyCreator;
import com.app.taxiapp.view.adapter.CardListsAdapter;
import com.app.taxiapp.viewmodel.ChoosePaymentViewModel;
import com.app.taxiapp.viewmodel.CommonViewModel;
import com.app.taxiapp.viewmodel.WalletViewModel;
import com.cooltechworks.creditcarddesign.CardEditActivity;
import com.cooltechworks.creditcarddesign.CreditCardUtils;
import com.stripe.android.CustomerSession;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import com.stripe.android.StripeError;
import com.stripe.android.model.Card;
import com.stripe.android.model.Customer;
import com.stripe.android.model.Token;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ChoosePaymentActivity extends BaseActivity {

    private String TAG = ChoosePaymentActivity.class.getSimpleName();
    CommonViewModel commonViewModel;
    ChoosePaymentViewModel choosePaymentViewModel;
    RecyclerView cardListRecyclerView;
    String fromWhere;
    ImageView isCashSelectedImage, isWalletSelectedImage;
    CircleImageView isCashNotSelectedImage, isWalletNotSelectedImage;
    TextView amountToBePaid, amountText, walletBalance;
    LinearLayout bookingPaymentLayout;
    CardView makePayment;
    SharedHelper sharedHelper;
    String selectedPaymentType;
    WalletViewModel viewModel;
    private CardListsAdapter cardListsAdapter;
    private float availableBalance;
    float booking_amount = 0;
    private boolean isPaymentTriggered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_payment);
        initViewsAndModels();
        initPaymentGateways();
        getValues();
        getWalletDetails();

    }

    private void getWalletDetails() {
        viewModel.getWalletDetails(getWalletDetailsInput()).observe(this, new Observer<WalletResponseModel>() {
            @Override
            public void onChanged(WalletResponseModel walletResponseModel) {
                if (walletResponseModel != null) {
                    if (walletResponseModel.getError()) {
                        handleFailureResponse(walletResponseModel.getMsg());
                    } else {
                        availableBalance = Float.parseFloat(walletResponseModel.getData().getBalance());
                        walletBalance.setText(commonViewModel.getCombinedStrings(getResources().getString(R.string.balance), "" + availableBalance));
                        if (booking_amount > availableBalance) {
                            walletBalance.setTextColor(getResources().getColor(R.color.negative_button_bg_color));
                        } else {
                            walletBalance.setTextColor(getResources().getColor(R.color.accent_background_color));
                        }
                    }
                }
            }
        });
    }

    private InputForAPI getWalletDetailsInput() {
        InputForAPI inputForAPI = new InputForAPI(ChoosePaymentActivity.this);
        inputForAPI.setUrl(UrlHelper.MY_WALLET_DETAILS);
        inputForAPI.setHeaders(ApiCall.getHeaders(ChoosePaymentActivity.this));
        return inputForAPI;
    }


    private void getValues() {
        fromWhere = getIntent().getStringExtra(Constants.IntentKeys.TYPE);
        if (fromWhere != null) {
            if (fromWhere.equalsIgnoreCase("wallet")) {
                booking_amount = Float.parseFloat(getIntent().getStringExtra(Constants.IntentKeys.DATA));
                amountText.setVisibility(View.VISIBLE);
                amountToBePaid.setVisibility(View.VISIBLE);
                bookingPaymentLayout.setVisibility(View.GONE);
                makePayment.setVisibility(View.VISIBLE);
                amountText.setText(getIntent().getStringExtra(Constants.IntentKeys.DATA));
            } else if (fromWhere.equalsIgnoreCase("navigationdrawer")) {
                amountText.setVisibility(View.GONE);
                amountToBePaid.setVisibility(View.GONE);
                bookingPaymentLayout.setVisibility(View.GONE);
                makePayment.setVisibility(View.GONE);
            } else if (fromWhere.equalsIgnoreCase("booking")) {
                booking_amount = Float.parseFloat(getIntent().getStringExtra(Constants.IntentKeys.DATA));
                amountText.setVisibility(View.VISIBLE);
                amountToBePaid.setVisibility(View.VISIBLE);
                bookingPaymentLayout.setVisibility(View.VISIBLE);
                makePayment.setVisibility(View.VISIBLE);
                amountText.setText(getIntent().getStringExtra(Constants.IntentKeys.DATA));
            }
        } else {
            amountText.setVisibility(View.GONE);
            amountToBePaid.setVisibility(View.GONE);
        }
    }

    private void initPaymentGateways() {
        initStripeCustomerSession();
    }

    private void initViewsAndModels() {
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        viewModel = ViewModelProviders.of(this).get(WalletViewModel.class);
        choosePaymentViewModel = ViewModelProviders.of(this).get(ChoosePaymentViewModel.class);
        cardListRecyclerView = findViewById(R.id.cardListRecyclerView);
        walletBalance = findViewById(R.id.walletBalance);
        amountToBePaid = findViewById(R.id.amountToBePaid);
        amountText = findViewById(R.id.amountText);
        isCashSelectedImage = findViewById(R.id.isCashSelected);
        isCashNotSelectedImage = findViewById(R.id.isCashNotSelected);
        isWalletNotSelectedImage = findViewById(R.id.isWalletNotSelected);
        isWalletSelectedImage = findViewById(R.id.isWalletSelected);
        makePayment = findViewById(R.id.makePayment);
        bookingPaymentLayout = findViewById(R.id.bookingPaymentLayout);
        sharedHelper = new SharedHelper(ChoosePaymentActivity.this);

        selectedPaymentType = sharedHelper.getSelectedPaymentType();
        if (selectedPaymentType.length() == 0) {
            selectedPaymentType = Constants.SelectedPaymentType.CASH.toString().toLowerCase();
        }
        showProgress();
        setLayouts();
    }

    private void setLayouts() {
        if (selectedPaymentType.equalsIgnoreCase(Constants.SelectedPaymentType.CASH.toString().toLowerCase())) {
            onCashSelected();
        } else if (selectedPaymentType.equalsIgnoreCase(Constants.SelectedPaymentType.WALLET.toString().toLowerCase())) {
            onWalletSelected();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_animation, R.anim.slide_down_to_bottom);
    }

    public void initStripeCustomerSession() {
        PaymentConfiguration.init(getResources().getString(R.string.stripe_key));
        CustomerSession.initCustomerSession(ChoosePaymentActivity.this,
                new EphemeralKeyCreator(
                        new EphemeralKeyCreator.ProgressListener() {
                            @Override
                            public void onStringResponse(String string) {
                                if (string.startsWith("Error: ")) {
                                    Utils.log("RETROFIT_TAG", "error: " + string);
                                    dismissProgress();
                                }

                            }
                        }, ChoosePaymentActivity.this));

        CustomerSession.getInstance().retrieveCurrentCustomer(
                new CustomerSession.CustomerRetrievalListener() {
                    @Override
                    public void onError(int errorCode, @NonNull String errorMessage, @Nullable StripeError stripeError) {
                        Utils.log("chek", "error: ");
                        dismissProgress();
                    }

                    @Override
                    public void onCustomerRetrieved(@NonNull Customer customer) {
                        getCardList();
                    }


                });

    }

    private void getCardList() {
        choosePaymentViewModel.getCardLists(getInputForCardListing()).observe(this, new Observer<SavedCardListResponseModel>() {
            @Override
            public void onChanged(SavedCardListResponseModel savedCardListResponseModel) {
                dismissProgress();
                if (savedCardListResponseModel.getError()) {
                    onFailureResponse(savedCardListResponseModel.getMsg());
                    if (cardListsAdapter != null) {
                        handleCardDetails(new ArrayList<SavedCardListResponseModel.Data>());
                    }
                } else {
                    handleCardDetails(savedCardListResponseModel.getData());
                }
            }
        });
    }

    private void handleCardDetails(final List<SavedCardListResponseModel.Data> data) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChoosePaymentActivity.this, RecyclerView.VERTICAL, false);
        cardListsAdapter = new CardListsAdapter(ChoosePaymentActivity.this, data, fromWhere);
        cardListRecyclerView.setLayoutManager(linearLayoutManager);
        cardListRecyclerView.setAdapter(cardListsAdapter);
        cardListsAdapter.setOnCardSelected(new onCardSelected() {
            @Override
            public void onSelected(SavedCardListResponseModel.Data value) {
                onCardSelected();
            }
        });
        cardListsAdapter.onCardDeleted(new onDeleteClickListener() {
            @Override
            public void onDelete(int position) {

                showDeleteConfirmation(data.get(position).getId());
            }
        });

    }


    private void showDeleteConfirmation(final String id) {

        final Dialog dialog = getConfirmationDialog();
        alertDialogBinding.dialogContent.setText(getResources().getString(R.string.are_you_sure_you_want_to_delete_the_card));
        alertDialogBinding.positiveButtonText.setText(getResources().getString(R.string.delete));
        alertDialogBinding.negativeButtonText.setText(getResources().getString(R.string.cancel));
        alertDialogBinding.positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showProgress();
                deleteCard(id);
            }
        });

        alertDialogBinding.negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    private void deleteCard(String id) {
        InputForAPI inputForAPI = new InputForAPI(ChoosePaymentActivity.this);
        inputForAPI.setUrl(UrlHelper.REMOVE_CARD);
        inputForAPI.setHeaders(ApiCall.getHeaders(ChoosePaymentActivity.this));
        try {
            inputForAPI.setJsonObject(new JSONObject().put(Constants.ApiKeys.SOURCE, id));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        commonViewModel.flagCheckResponse(inputForAPI).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(FlagCheckResponseModel flagCheckResponseModel) {
                dismissProgress();
                if (flagCheckResponseModel.isError()) {
                    handleFailureResponse(flagCheckResponseModel.getMsg());
                } else {
                    displayError(getResources().getString(R.string.card_deleted_successfully));
                    getCardList();
                }
            }
        });
    }


    private void handleFailureResponse(String msg) {
        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }
    }


    private InputForAPI getInputForCardListing() {
        InputForAPI inputForAPI = new InputForAPI(ChoosePaymentActivity.this);
        inputForAPI.setUrl(UrlHelper.CARD_LIST);
        inputForAPI.setHeaders(ApiCall.getHeaders(ChoosePaymentActivity.this));
        return inputForAPI;
    }


    @SuppressLint("CheckResult")
    private void generateCardToken(final String cardNumber, final String expMonth, final String expYear, final String cvc) {
        final Stripe stripe = new Stripe(ChoosePaymentActivity.this, getString(R.string.stripe_key));

        Observable<Token> tokenObservable =
                Observable.fromCallable(
                        new Callable<Token>() {
                            @Override
                            public Token call() throws Exception {
                                Card card = new Card.Builder(cardNumber, Integer.parseInt(expMonth), Integer.parseInt(expYear), cvc).build();
                                return stripe.createTokenSynchronous(card);
                            }
                        });


        tokenObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).doOnSubscribe(new Consumer<Disposable>() {
            @Override
            public void accept(Disposable disposable) throws Exception {
                showProgress();
            }
        }).subscribe(new Consumer<Token>() {
            @Override
            public void accept(Token token) {
                dismissProgress();
                addCard(token.getId());

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) {
                dismissProgress();
                displayError(throwable.getMessage());
            }
        });
    }


    private void onFailureResponse(String msg) {

        if (msg.equalsIgnoreCase(getResources().getString(R.string.error_no_internet_connection))) {
            showNoInternetDialog();
        } else {
            displayError(msg);
        }

    }

    private void showNoInternetDialog() {
        noInternetDialog = new NoInternetDialog(ChoosePaymentActivity.this);
        noInternetDialog.show();
        noInternetDialog.setOnConnected(new InternetCallback() {
            @Override
            public void onConnected() {
                getCardList();
            }
        });
    }


    private void addCard(String id) {
        InputForAPI inputForAPI = new InputForAPI(ChoosePaymentActivity.this);
        inputForAPI.setJsonObject(getAddCardParams(id));
        inputForAPI.setUrl(UrlHelper.ADD_CARD);
        inputForAPI.setHeaders(ApiCall.getHeaders(ChoosePaymentActivity.this));
        commonViewModel.flagCheckResponse(inputForAPI).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(FlagCheckResponseModel flagCheckResponseModel) {
                if (flagCheckResponseModel.isError()) {
                    displayError(flagCheckResponseModel.getMsg());
                } else {
                    displayError(getString(R.string.card_added_successfully));
                    getCardList();
                }
            }
        });
    }

    private void displayError(String error_message) {
        Utils.displayError(findViewById(android.R.id.content), error_message);
    }


    private JSONObject getAddCardParams(String id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.ApiKeys.SOURCE, id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.IntentPermissionCode.ADD_CARD) {
            if (resultCode == RESULT_OK) {
                String expdate = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY);
                try {
                    generateCardToken(data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER), expdate.split("/")[0],
                            expdate.split("/")[1], data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV));
                } catch (Exception e) {
                    displayError(getResources().getString(R.string.invalid_card_details));
                }
            }
        }
    }


    public void onAddCardClicked(View view) {
//        showSuccessDialog();
        moveAddCard();
    }

    private void moveAddCard() {
        Intent intent = new Intent(ChoosePaymentActivity.this, CardEditActivity.class);

        startActivityForResult(intent, Constants.IntentPermissionCode.ADD_CARD);
    }

    public void onBackPressed(View view) {
        onBackPressed();
    }

    public void onCashClicked(View view) {
        if (!selectedPaymentType.equalsIgnoreCase(Constants.SelectedPaymentType.CASH.toString().toLowerCase())) {
            onCashSelected();
        }
    }

    public void onWalletClicked(View view) {

        if (booking_amount < availableBalance) {
            if (!selectedPaymentType.equalsIgnoreCase(Constants.SelectedPaymentType.WALLET.toString().toLowerCase())) {
                onWalletSelected();
            }
        } else {
            displayError(getResources().getString(R.string.low_money));
        }

    }


    public void disableCashMethod() {
        isCashSelectedImage.setVisibility(View.GONE);
        isCashNotSelectedImage.setVisibility(View.VISIBLE);
    }


    public void disableWalletMethod() {
        isWalletSelectedImage.setVisibility(View.VISIBLE);
        isWalletNotSelectedImage.setVisibility(View.VISIBLE);
    }

    public void disableCardMethod() {
        sharedHelper.setSelectedCardId("");
        if (cardListsAdapter != null) {
            cardListsAdapter.notifyDataSetChanged();
        }

    }


    public void enableCashMethod() {
        isCashSelectedImage.setVisibility(View.VISIBLE);
        isCashNotSelectedImage.setVisibility(View.GONE);
    }


    public void enableWalletMethod() {
        isWalletSelectedImage.setVisibility(View.VISIBLE);
        isWalletNotSelectedImage.setVisibility(View.GONE);
    }


    private void onCashSelected() {
        selectedPaymentType = Constants.SelectedPaymentType.CASH.toString().toLowerCase();
        sharedHelper.setSelectedPaymentType(selectedPaymentType);
        sharedHelper.setSelectedCardId("");
        enableCashMethod();
        disableWalletMethod();
        disableCardMethod();
    }

    private void onCardSelected() {
        selectedPaymentType = Constants.SelectedPaymentType.CARD.toString().toLowerCase();
        sharedHelper.setSelectedPaymentType(selectedPaymentType);
        disableCashMethod();
        disableWalletMethod();
    }


    private void onWalletSelected() {
        selectedPaymentType = Constants.SelectedPaymentType.WALLET.toString().toLowerCase();
        sharedHelper.setSelectedPaymentType(selectedPaymentType);
        sharedHelper.setSelectedCardId("");
        enableWalletMethod();
        disableCashMethod();
        disableCardMethod();
    }

    public void onPaymentClicked(View view) {

        if (fromWhere.equalsIgnoreCase("wallet")) {
            if (sharedHelper.getSelectedCardId().length() > 0) {
                if (!isPaymentTriggered)
                    addMoneyToWallet();

            } else {
                displayError(getResources().getString(R.string.please_select_any_card));
            }
        } else if (fromWhere.equalsIgnoreCase("booking")) {
            Intent intent = getIntent();
            intent.putExtra(Constants.IntentKeys.SELECTED_PAYMENT_NAME, selectedPaymentType);
            setResult(RESULT_OK, intent);
            finish();
        }

    }


    public void showSuccessDialog() {
        final Dialog dialog = new Dialog(ChoosePaymentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        final DialogWalletSuccesLayoutBinding dialogBinding = DataBindingUtil.inflate(LayoutInflater.from(ChoosePaymentActivity.this), R.layout.dialog_wallet_succes_layout, null, false);
        dialog.setContentView(dialogBinding.getRoot());

        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            window.getAttributes().windowAnimations = R.style.DialogAnimation;
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                isPaymentTriggered = false;
            }
        });

        dialog.show();
        dialogBinding.amountText.setText(commonViewModel.getCombinedStrings(getResources().getString(R.string.money_symbol), getIntent().getStringExtra(Constants.IntentKeys.DATA), " ", getString(R.string.added_to_wallet)));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialogBinding.animationView.playAnimation();
            }
        }, 400);

        dialogBinding.closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });


    }

    private void addMoneyToWallet() {
        isPaymentTriggered = true;
        commonViewModel.flagCheckResponse(getInputForAddMoney()).observe(this, new Observer<FlagCheckResponseModel>() {
            @Override
            public void onChanged(FlagCheckResponseModel flagCheckResponseModel) {
                if (flagCheckResponseModel.isError()) {
                    onFailureResponse(flagCheckResponseModel.getMsg());
                } else {
                    showSuccessDialog();
//                    displayError(getResources().getString(R.string.amount_added_successfully));
//                    finish();
                }
            }
        });
    }

    private InputForAPI getInputForAddMoney() {
        InputForAPI inputForAPI = new InputForAPI(ChoosePaymentActivity.this);
        inputForAPI.setUrl(UrlHelper.ADD_MONEY_IN_WALLET);
        inputForAPI.setJsonObject(getParamsForAddMoneyWallet());
        inputForAPI.setHeaders(ApiCall.getHeaders(ChoosePaymentActivity.this));
        return inputForAPI;
    }

    private JSONObject getParamsForAddMoneyWallet() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.ApiKeys.AMOUNT, getIntent().getStringExtra(Constants.IntentKeys.DATA));
            jsonObject.put(Constants.ApiKeys.CARD_ID, sharedHelper.getSelectedCardId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}
