package com.app.taxiapp.view.adapter;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ItemCancelReasonBinding;
import com.app.taxiapp.model.apiresponsemodel.CancelReasonsModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class CancelReasonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<CancelReasonsModel.Data> cancelReasonsModels = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private String selectedReason;

    public CancelReasonAdapter(Activity context, List<CancelReasonsModel.Data> trips) {
        this.context = context;
        this.cancelReasonsModels = trips;
    }

    public String getSelectedReason() {
        return selectedReason;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemCancelReasonBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_cancel_reason, parent, false);
        return new CancelReasonViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final CancelReasonViewHolder viewHolder = (CancelReasonViewHolder) holder;
        viewHolder.binding.reasonName.setText(cancelReasonsModels.get(position).getDescription());
        if (cancelReasonsModels.get(position).isSelected()) {
            if (cancelReasonsModels.get(position).getDescription().equalsIgnoreCase(context.getResources().getString(R.string.others))) {
                selectedReason = "";
                viewHolder.binding.reasonParent.setVisibility(View.VISIBLE);
            } else {
                selectedReason = cancelReasonsModels.get(position).getDescription();
            }
            viewHolder.binding.isSelected.setVisibility(View.VISIBLE);
        } else {
            viewHolder.binding.reasonParent.setVisibility(View.GONE);
            viewHolder.binding.isSelected.setVisibility(View.GONE);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < cancelReasonsModels.size(); i++) {
                    if (i == position) {
                        selectedReason = "";
                        cancelReasonsModels.get(i).setSelected(true);
                    } else
                        cancelReasonsModels.get(i).setSelected(false);
                }
                notifyDataSetChanged();
            }
        });

        viewHolder.binding.reasonComments.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedReason = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return cancelReasonsModels.size();

    }


    class CancelReasonViewHolder extends RecyclerView.ViewHolder {
        ItemCancelReasonBinding binding;

        CancelReasonViewHolder(@NonNull ItemCancelReasonBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
