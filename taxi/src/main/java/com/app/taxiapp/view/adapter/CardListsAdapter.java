package com.app.taxiapp.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.taxiapp.R;
import com.app.taxiapp.databinding.ItemTripsListBinding;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.interfaces.onCardSelected;
import com.app.taxiapp.helpers.interfaces.onDeleteClickListener;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;
import com.app.taxiapp.model.apiresponsemodel.SavedCardListResponseModel;
import com.app.taxiapp.model.apiresponsemodel.YourTripsModel;
import com.app.taxiapp.networkcall.ImageLoader;
import com.app.taxiapp.view.activity.TripDetailsActivity;
import com.cooltechworks.creditcarddesign.CreditCardUtils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CardListsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<SavedCardListResponseModel.Data> savedCards;
    private onCardSelected onCardSelected;
    onDeleteClickListener onDeleteClickListener;
    private String TAG = CardListsAdapter.class.getSimpleName();
    private SharedHelper sharedHelper;
    private String fromWhere;


    public CardListsAdapter(Activity context, List<SavedCardListResponseModel.Data> savedCards, String fromWhere) {
        this.context = context;
        this.savedCards = savedCards;
        this.fromWhere = fromWhere;
        sharedHelper = new SharedHelper(context);
    }

    public void setOnCardSelected(onCardSelected onCardSelected) {
        this.onCardSelected = onCardSelected;
    }

    public void onCardDeleted(onDeleteClickListener onDeleteClickListener) {
        this.onDeleteClickListener = onDeleteClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_card_list, parent, false);
        return new YourTripsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final YourTripsViewHolder viewHolder = (YourTripsViewHolder) holder;
        final SavedCardListResponseModel.Data data = savedCards.get(position);
        viewHolder.cardNumber.setText(getCardPrefix() + data.getLast4());
        viewHolder.cardIcon.setImageResource(CreditCardUtils.getIcon(data.getBrand()));
        if (!fromWhere.equalsIgnoreCase("navigationdrawer")) {
            viewHolder.deleteIcon.setVisibility(View.GONE);
            if (sharedHelper.getSelectedCardId().equalsIgnoreCase(data.getId())) {
                viewHolder.isSelected.setVisibility(View.VISIBLE);
                viewHolder.isNotSelected.setVisibility(View.GONE);
            } else {
                viewHolder.isSelected.setVisibility(View.GONE);
                viewHolder.isNotSelected.setVisibility(View.VISIBLE);
            }
        } else {
            viewHolder.isSelected.setVisibility(View.GONE);
            viewHolder.isNotSelected.setVisibility(View.GONE);
            viewHolder.deleteIcon.setVisibility(View.VISIBLE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fromWhere.equalsIgnoreCase("navigationdrawer")) {
                    onCardSelected.onSelected(data);
                    sharedHelper.setSelectedCardId(data.getId());
                    notifyDataSetChanged();
                }
            }
        });

        viewHolder.deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteClickListener.onDelete(position);
            }
        });
    }

    private String getCardPrefix() {
        return "**** **** **** ";
    }


    @Override
    public int getItemCount() {
        return savedCards.size();
    }


    class YourTripsViewHolder extends RecyclerView.ViewHolder {
        ImageView cardIcon, isSelected, isNotSelected, deleteIcon;
        TextView cardNumber;

        YourTripsViewHolder(@NonNull View view) {
            super(view);
            cardIcon = view.findViewById(R.id.cardIcon);
            deleteIcon = view.findViewById(R.id.deleteIcon);
            cardNumber = view.findViewById(R.id.cardNumber);
            isSelected = view.findViewById(R.id.isSelected);
            isNotSelected = view.findViewById(R.id.isNotSelected);

        }
    }


}
