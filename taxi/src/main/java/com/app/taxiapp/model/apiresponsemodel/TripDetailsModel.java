package com.app.taxiapp.model.apiresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TripDetailsModel {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data {
        @Expose
        @SerializedName("providerInfo")
        private ProviderInfo providerInfo;
        @Expose
        @SerializedName("receipt")
        private List<Receipt> receipt;
        @Expose
        @SerializedName("isActive")
        private String isActive;
        @Expose
        @SerializedName("vehilceName")
        private String vehilceName;
        @Expose
        @SerializedName("paymentMode")
        private String paymentMode;
        @Expose
        @SerializedName("totalAmt")
        private String totalAmt;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("destinyLong")
        private String destinyLong;
        @Expose
        @SerializedName("destinyLat")
        private String destinyLat;
        @Expose
        @SerializedName("sourceLong")
        private String sourceLong;
        @Expose
        @SerializedName("sourceLat")
        private String sourceLat;
        @Expose
        @SerializedName("toLocation")
        private String toLocation;
        @Expose
        @SerializedName("fromLocation")
        private String fromLocation;
        @Expose
        @SerializedName("createdTime")
        private String createdTime;

        public ProviderInfo getProviderInfo() {
            return providerInfo;
        }

        public void setProviderInfo(ProviderInfo providerInfo) {
            this.providerInfo = providerInfo;
        }

        public List<Receipt> getReceipt() {
            return receipt;
        }

        public void setReceipt(List<Receipt> receipt) {
            this.receipt = receipt;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getVehilceName() {
            return vehilceName;
        }

        public void setVehilceName(String vehilceName) {
            this.vehilceName = vehilceName;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getTotalAmt() {
            return totalAmt;
        }

        public void setTotalAmt(String totalAmt) {
            this.totalAmt = totalAmt;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDestinyLong() {
            return destinyLong;
        }

        public void setDestinyLong(String destinyLong) {
            this.destinyLong = destinyLong;
        }

        public String getDestinyLat() {
            return destinyLat;
        }

        public void setDestinyLat(String destinyLat) {
            this.destinyLat = destinyLat;
        }

        public String getSourceLong() {
            return sourceLong;
        }

        public void setSourceLong(String sourceLong) {
            this.sourceLong = sourceLong;
        }

        public String getSourceLat() {
            return sourceLat;
        }

        public void setSourceLat(String sourceLat) {
            this.sourceLat = sourceLat;
        }

        public String getToLocation() {
            return toLocation;
        }

        public void setToLocation(String toLocation) {
            this.toLocation = toLocation;
        }

        public String getFromLocation() {
            return fromLocation;
        }

        public void setFromLocation(String fromLocation) {
            this.fromLocation = fromLocation;
        }

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }


        String getLatlongPath()
        {
            return getSourceLat()+","+getSourceLong()+"|"+getDestinyLat()+","+getDestinyLong();
        }
        public String getStaticMap()
        {
//            return UrlHelper.getCustomisedMapUrl(getSourceLat(),getSourceLong(),getDestinyLat(),getDestinyLong(),getLatlongPath());
            return "";
        }
    }

    public static class ProviderInfo {
        @Expose
        @SerializedName("rating")
        private String rating;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("name")
        private String name;

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class Receipt {
        @Expose
        @SerializedName("value")
        private String value;
        @Expose
        @SerializedName("fieldName")
        private String fieldName;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getFieldName() {
            return fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }
    }
}
