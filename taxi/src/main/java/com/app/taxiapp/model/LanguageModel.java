package com.app.taxiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LanguageModel {

    @Expose
    @SerializedName("languageLists")
    private List<LanguageLists> languageLists;


    public List<LanguageLists> getLanguageLists() {
        return languageLists;
    }

    public void setLanguageLists(List<LanguageLists> languageLists) {
        this.languageLists = languageLists;
    }


    public static class LanguageLists {
        @Expose
        @SerializedName("shortCode")
        private String shortCode;
        @Expose
        @SerializedName("displayName")
        private String displayName;

        public String getShortCode() {
            return shortCode;
        }

        public void setShortCode(String shortCode) {
            this.shortCode = shortCode;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }
    }
}
