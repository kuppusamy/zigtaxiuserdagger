package com.app.taxiapp.model.apiresponsemodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.taxiapp.BR;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class RideTypeResponseModel {

    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data extends BaseObservable implements Parcelable {


        @Expose
        @SerializedName("longDesc")
        private String longdesc;
        @Expose
        @SerializedName("shortDesc")
        private String shortdesc;
        @Expose
        @SerializedName("capacity")
        private int capacity;
        @Expose
        @SerializedName("waitingCharge")
        private String waitingcharge;
        @Expose
        @SerializedName("priceText")
        private String pricetext;
        @Expose
        @SerializedName("priceValue")
        private double pricevalue;
        @Expose
        @SerializedName("iconActive")
        private String iconactive;
        @Expose
        @SerializedName("iconPassive")
        private String iconpassive;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;


        @Bindable
        public String getImageUrltoDisplay() {
            return imageUrltoDisplay;
        }

        private void setImageUrltoDisplay(String imageUrltoDisplay) {
            this.imageUrltoDisplay = imageUrltoDisplay;

        }

        private String imageUrltoDisplay;

        private boolean isSelected = false;

        @Bindable
        public boolean isSelected() {
            if (isSelected) {
                imageUrltoDisplay = iconactive;
            } else {
                imageUrltoDisplay = iconpassive;
            }
            notifyPropertyChanged(BR.imageUrltoDisplay);
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
            if (isSelected) {
                imageUrltoDisplay = iconactive;
            } else {
                imageUrltoDisplay = iconpassive;
            }
            notifyPropertyChanged(BR.imageUrltoDisplay);
            notifyPropertyChanged(BR.selected);
        }

        public String getLongdesc() {
            return longdesc;
        }

        public void setLongdesc(String longdesc) {
            this.longdesc = longdesc;
        }

        public String getShortdesc() {
            return shortdesc;
        }

        public void setShortdesc(String shortdesc) {
            this.shortdesc = shortdesc;
        }

        public int getCapacity() {
            return capacity;
        }

        public void setCapacity(int capacity) {
            this.capacity = capacity;
        }

        public String getWaitingcharge() {
            return waitingcharge;
        }

        public void setWaitingcharge(String waitingcharge) {
            this.waitingcharge = waitingcharge;
        }

        public String getPricetext() {
            return pricetext;
        }

        public void setPricetext(String pricetext) {
            this.pricetext = pricetext;
        }

        public double getPricevalue() {
            return pricevalue;
        }

        public void setPricevalue(double pricevalue) {
            this.pricevalue = pricevalue;
        }

        public String getIconactive() {
            return iconactive;
        }

        public void setIconactive(String iconactive) {
            this.iconactive = iconactive;
        }

        public String getIconpassive() {
            return iconpassive;
        }

        public void setIconpassive(String iconpassive) {
            this.iconpassive = iconpassive;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.longdesc);
            dest.writeString(this.shortdesc);
            dest.writeInt(this.capacity);
            dest.writeString(this.waitingcharge);
            dest.writeString(this.pricetext);
            dest.writeDouble(this.pricevalue);
            dest.writeString(this.iconactive);
            dest.writeString(this.iconpassive);
            dest.writeString(this.name);
            dest.writeInt(this.id);
            dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
        }

        public Data() {
        }

        protected Data(Parcel in) {
            this.longdesc = in.readString();
            this.shortdesc = in.readString();
            this.capacity = in.readInt();
            this.waitingcharge = in.readString();
            this.pricetext = in.readString();
            this.pricevalue = in.readDouble();
            this.iconactive = in.readString();
            this.iconpassive = in.readString();
            this.name = in.readString();
            this.id = in.readInt();
            this.isSelected = in.readByte() != 0;
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel source) {
                return new Data(source);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };
    }
}
