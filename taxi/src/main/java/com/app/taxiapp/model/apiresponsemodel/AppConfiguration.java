package com.app.taxiapp.model.apiresponsemodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppConfiguration {

    @Expose
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public static class Data {
        public List<Slider> getSlider() {
            return slider;
        }

        public void setSlider(List<Slider> slider) {
            this.slider = slider;
        }

        public AuthConfig getAuthConfig() {
            return authConfig;
        }

        public void setAuthConfig(AuthConfig authConfig) {
            this.authConfig = authConfig;
        }

        @Expose
        @SerializedName("slider")
        private List<Slider> slider;
        @Expose
        @SerializedName("authConfig")
        private AuthConfig authConfig;
    }

    public static class Slider implements Parcelable {
        @Expose
        @SerializedName("image")
        private String image;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("id")
        private int id;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.image);
            dest.writeString(this.description);
            dest.writeString(this.title);
            dest.writeInt(this.id);
        }

        public Slider() {
        }

        protected Slider(Parcel in) {
            this.image = in.readString();
            this.description = in.readString();
            this.title = in.readString();
            this.id = in.readInt();
        }

        public static final Parcelable.Creator<Slider> CREATOR = new Parcelable.Creator<Slider>() {
            @Override
            public Slider createFromParcel(Parcel source) {
                return new Slider(source);
            }

            @Override
            public Slider[] newArray(int size) {
                return new Slider[size];
            }
        };
    }

    public static class AuthConfig {
        public String getOtp_timer() {
            return otp_timer;
        }

        public void setOtp_timer(String otp_timer) {
            this.otp_timer = otp_timer;
        }

        public String getAuth_type() {
            return auth_type;
        }

        public void setAuth_type(String auth_type) {
            this.auth_type = auth_type;
        }

        @Expose
        @SerializedName("otp_timer")
        private String otp_timer;

        public String getSos_number() {
            return sos_number;
        }

        public void setSos_number(String sos_number) {
            this.sos_number = sos_number;

        }

        @Expose
        @SerializedName("sos_number")
        private String sos_number;


        @Expose
        @SerializedName("auth_type")
        private String auth_type;

        public String getMax_range() {
            return max_range;
        }

        public void setMax_range(String max_range) {
            this.max_range = max_range;
        }

        @Expose
        @SerializedName("max_range")
        private String max_range;

        public String getMap_api_key() {
            return map_api_key;
        }

        public void setMap_api_key(String map_api_key) {
            this.map_api_key = map_api_key;
        }

        @Expose
        @SerializedName("map_api_key")
        private String map_api_key;
    }
}
