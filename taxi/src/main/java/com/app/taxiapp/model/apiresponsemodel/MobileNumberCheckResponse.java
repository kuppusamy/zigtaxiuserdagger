package com.app.taxiapp.model.apiresponsemodel;

public class MobileNumberCheckResponse {

    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("msg")
    public String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("error")
    public boolean error;
}
