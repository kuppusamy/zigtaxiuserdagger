package com.app.taxiapp.model.apiresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SavedCardListResponseModel {

    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data {
        @Expose
        @SerializedName("metadata")
        private Metadata metadata;
        @Expose
        @SerializedName("last4")
        private String last4;
        @Expose
        @SerializedName("funding")
        private String funding;
        @Expose
        @SerializedName("fingerprint")
        private String fingerprint;
        @Expose
        @SerializedName("exp_year")
        private int exp_year;
        @Expose
        @SerializedName("exp_month")
        private int exp_month;
        @Expose
        @SerializedName("cvc_check")
        private String cvc_check;
        @Expose
        @SerializedName("customer")
        private String customer;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("brand")
        private String brand;
        @Expose
        @SerializedName("object")
        private String object;
        @Expose
        @SerializedName("id")
        private String id;

        public Metadata getMetadata() {
            return metadata;
        }

        public void setMetadata(Metadata metadata) {
            this.metadata = metadata;
        }

        public String getLast4() {
            return last4;
        }

        public void setLast4(String last4) {
            this.last4 = last4;
        }

        public String getFunding() {
            return funding;
        }

        public void setFunding(String funding) {
            this.funding = funding;
        }

        public String getFingerprint() {
            return fingerprint;
        }

        public void setFingerprint(String fingerprint) {
            this.fingerprint = fingerprint;
        }

        public int getExp_year() {
            return exp_year;
        }

        public void setExp_year(int exp_year) {
            this.exp_year = exp_year;
        }

        public int getExp_month() {
            return exp_month;
        }

        public void setExp_month(int exp_month) {
            this.exp_month = exp_month;
        }

        public String getCvc_check() {
            return cvc_check;
        }

        public void setCvc_check(String cvc_check) {
            this.cvc_check = cvc_check;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Metadata {
    }
}
