package com.app.taxiapp.model.apiresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HelpDetailModel {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data {
        @Expose
        @SerializedName("content")
        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
