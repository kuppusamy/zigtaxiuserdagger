package com.app.taxiapp.model.apiresponsemodel;

import com.app.taxiapp.helpers.Utils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WalletResponseModel {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data {
        @Expose
        @SerializedName("transaction")
        private List<Transaction> transaction;
        @Expose
        @SerializedName("balance")
        private String balance;

        public List<Transaction> getTransaction() {
            return transaction;
        }

        public void setTransaction(List<Transaction> transaction) {
            this.transaction = transaction;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }
    }

    public static class Transaction {
        @Expose
        @SerializedName("description")
        private String transactionName;
        @Expose
        @SerializedName("type")
        private String transactionType;
        @Expose
        @SerializedName("amount")
        private String transactionAmount;
        @Expose
        @SerializedName("createdAt")
        private String createdAt;
        @Expose
        @SerializedName("date")
        private String date;

        @Expose
        @SerializedName("status")
        private String status;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFormattedDate() {
            return Utils.getConvertedTime(createdAt);
        }


        public String getTransactionName() {
            return transactionName;
        }

        public void setTransactionName(String transactionName) {
            this.transactionName = transactionName;
        }

        public String getTransactionType() {
            return transactionType;
        }

        public void setTransactionType(String transactionType) {
            this.transactionType = transactionType;
        }

        public String getTransactionAmount() {
            return transactionAmount;
        }

        public void setTransactionAmount(String transactionAmount) {
            this.transactionAmount = transactionAmount;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }
}
