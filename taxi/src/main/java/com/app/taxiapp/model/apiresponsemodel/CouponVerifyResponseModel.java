package com.app.taxiapp.model.apiresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponVerifyResponseModel {


    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data {
        @Expose
        @SerializedName("promoCodeRedeemId")
        private int promoCodeRedeemId;
        @Expose
        @SerializedName("amount")
        private String amount;

        public int getPromoCodeRedeemId() {
            return promoCodeRedeemId;
        }

        public void setPromoCodeRedeemId(int promoCodeRedeemId) {
            this.promoCodeRedeemId = promoCodeRedeemId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }
}
