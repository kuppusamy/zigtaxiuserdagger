package com.app.taxiapp.model.apiresponsemodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentBookingResponseModel {
    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data implements Parcelable {
        @Expose
        @SerializedName("rating")
        private String rating = "";

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        @Expose
        @SerializedName("providerInfo")
        private ProviderInfo providerInfo;
        @Expose
        @SerializedName("totalAmt")
        private String totalAmt = "";
        @Expose
        @SerializedName("destinyLong")
        private String destinyLong = "";
        @Expose
        @SerializedName("destinyLat")
        private String destinyLat = "";
        @Expose
        @SerializedName("sourceLong")
        private String sourceLong = "";
        @Expose
        @SerializedName("sourceLat")
        private String sourceLat = "";
        @Expose
        @SerializedName("currency")
        private String currency = "";
        @Expose
        @SerializedName("estimation")
        private String estimation = "";
        @Expose
        @SerializedName("status")
        private String status = "";

        @Expose
        @SerializedName("toLocation")
        private String toLocation = "";

        @Expose
        @SerializedName("fromLocation")
        private String fromLocation = "";

        @Expose
        @SerializedName("rideName")
        private String rideName = "";

        public String getRideName() {
            return rideName;
        }

        public void setRideName(String rideName) {
            this.rideName = rideName;
        }

        public String getRideImage() {
            return rideImage;
        }

        public void setRideImage(String rideImage) {
            this.rideImage = rideImage;
        }

        @Expose
        @SerializedName("rideImage")
        private String rideImage = "";

        @Expose
        @SerializedName("id")
        private int id = 0;
        private LatLng fromLatLng;
        private LatLng toLatLng;


        public LatLng getFromLatLng() {
            fromLatLng = new LatLng(Double.parseDouble(sourceLat), Double.parseDouble(sourceLong));
            return fromLatLng;
        }

        public LatLng getToLatLng() {
            toLatLng = new LatLng(Double.parseDouble(destinyLat), Double.parseDouble(destinyLong));
            return toLatLng;
        }

        public ProviderInfo getProviderInfo() {
            return providerInfo;
        }

        public void setProviderInfo(ProviderInfo providerInfo) {
            this.providerInfo = providerInfo;
        }

        public String getTotalAmt() {
            return totalAmt;
        }

        public void setTotalAmt(String totalAmt) {
            this.totalAmt = totalAmt;
        }

        public String getDestinyLong() {
            return destinyLong;
        }

        public void setDestinyLong(String destinyLong) {
            this.destinyLong = destinyLong;
        }

        public String getDestinyLat() {
            return destinyLat;
        }

        public void setDestinyLat(String destinyLat) {
            this.destinyLat = destinyLat;
        }

        public String getSourceLong() {
            return sourceLong;
        }

        public void setSourceLong(String sourceLong) {
            this.sourceLong = sourceLong;
        }

        public String getSourceLat() {
            return sourceLat;
        }

        public void setSourceLat(String sourceLat) {
            this.sourceLat = sourceLat;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getEstimation() {
            return estimation;
        }

        public void setEstimation(String estimation) {
            this.estimation = estimation;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getToLocation() {
            return toLocation;
        }

        public void setToLocation(String toLocation) {
            this.toLocation = toLocation;
        }

        public String getFromLocation() {
            return fromLocation;
        }

        public void setFromLocation(String fromLocation) {
            this.fromLocation = fromLocation;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.providerInfo, flags);
            dest.writeString(this.totalAmt);
            dest.writeString(this.destinyLong);
            dest.writeString(this.destinyLat);
            dest.writeString(this.sourceLong);
            dest.writeString(this.sourceLat);
            dest.writeString(this.currency);
            dest.writeString(this.estimation);
            dest.writeString(this.status);
            dest.writeString(this.toLocation);
            dest.writeString(this.fromLocation);
            dest.writeString(this.rideName);
            dest.writeString(this.rideImage);
            dest.writeInt(this.id);
            dest.writeParcelable(this.fromLatLng, flags);
            dest.writeParcelable(this.toLatLng, flags);
        }

        public Data() {
        }

        protected Data(Parcel in) {
            this.providerInfo = in.readParcelable(ProviderInfo.class.getClassLoader());
            this.totalAmt = in.readString();
            this.destinyLong = in.readString();
            this.destinyLat = in.readString();
            this.sourceLong = in.readString();
            this.sourceLat = in.readString();
            this.currency = in.readString();
            this.estimation = in.readString();
            this.status = in.readString();
            this.toLocation = in.readString();
            this.fromLocation = in.readString();
            this.rideName = in.readString();
            this.rideImage = in.readString();
            this.id = in.readInt();
            this.fromLatLng = in.readParcelable(LatLng.class.getClassLoader());
            this.toLatLng = in.readParcelable(LatLng.class.getClassLoader());
        }

        public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel source) {
                return new Data(source);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };
    }

    public static class ProviderInfo implements Parcelable {

        @Expose
        @SerializedName("tripCount")
        private String tripCount = "";
        @Expose
        @SerializedName("vechileNo")
        private String vechileNo = "";
        @Expose
        @SerializedName("vechileName")
        private String vechileName = "";
        @Expose
        @SerializedName("rating")
        private String rating = "";
        @Expose
        @SerializedName("countryCode")
        private String countryCode = "";
        @Expose
        @SerializedName("mobile")
        private String mobile = "";
        @Expose
        @SerializedName("image")
        private String image = "";
        @Expose
        @SerializedName("lastName")
        private String lastName = "";
        @Expose
        @SerializedName("firstName")
        private String firstName = "";

        public String getTripCount() {
            return tripCount;
        }

        public void setTripCount(String tripCount) {
            this.tripCount = tripCount;
        }

        public String getVechileNo() {
            return vechileNo;
        }

        public void setVechileNo(String vechileNo) {
            this.vechileNo = vechileNo;
        }

        public String getVechileName() {
            return vechileName;
        }

        public void setVechileName(String vechileName) {
            this.vechileName = vechileName;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.tripCount);
            dest.writeString(this.vechileNo);
            dest.writeString(this.vechileName);
            dest.writeString(this.rating);
            dest.writeString(this.countryCode);
            dest.writeString(this.mobile);
            dest.writeString(this.image);
            dest.writeString(this.lastName);
            dest.writeString(this.firstName);
        }

        public ProviderInfo() {
        }

        protected ProviderInfo(Parcel in) {
            this.tripCount = in.readString();
            this.vechileNo = in.readString();
            this.vechileName = in.readString();
            this.rating = in.readString();
            this.countryCode = in.readString();
            this.mobile = in.readString();
            this.image = in.readString();
            this.lastName = in.readString();
            this.firstName = in.readString();
        }

        public static final Parcelable.Creator<ProviderInfo> CREATOR = new Parcelable.Creator<ProviderInfo>() {
            @Override
            public ProviderInfo createFromParcel(Parcel source) {
                return new ProviderInfo(source);
            }

            @Override
            public ProviderInfo[] newArray(int size) {
                return new ProviderInfo[size];
            }
        };
    }
}
