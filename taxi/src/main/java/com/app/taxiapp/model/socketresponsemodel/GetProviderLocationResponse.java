package com.app.taxiapp.model.socketresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProviderLocationResponse {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data {
        @Expose
        @SerializedName("active")
        private List<Integer> active;
        @Expose
        @SerializedName("providerLocation")
        private List<ProviderDetailResponse.Data> providerlocation;

        public List<Integer> getActive() {
            return active;
        }

        public void setActive(List<Integer> active) {
            this.active = active;
        }

        public List<ProviderDetailResponse.Data> getProviderlocation() {
            return providerlocation;
        }

        public void setProviderlocation(List<ProviderDetailResponse.Data> providerlocation) {
            this.providerlocation = providerlocation;
        }
    }

}
