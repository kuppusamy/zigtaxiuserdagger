package com.app.taxiapp.model.apiresponsemodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialLoginResponseModel implements Parcelable {


    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("last_name")
    private String last_name;
    @Expose
    @SerializedName("first_name")
    private String first_name;
    @Expose
    @SerializedName("id")
    private String id;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.email);
        dest.writeString(this.last_name);
        dest.writeString(this.first_name);
        dest.writeString(this.id);
    }

    public SocialLoginResponseModel() {
    }

    protected SocialLoginResponseModel(Parcel in) {
        this.email = in.readString();
        this.last_name = in.readString();
        this.first_name = in.readString();
        this.id = in.readString();
    }

    public static final Creator<SocialLoginResponseModel> CREATOR = new Creator<SocialLoginResponseModel>() {
        @Override
        public SocialLoginResponseModel createFromParcel(Parcel source) {
            return new SocialLoginResponseModel(source);
        }

        @Override
        public SocialLoginResponseModel[] newArray(int size) {
            return new SocialLoginResponseModel[size];
        }
    };
}
