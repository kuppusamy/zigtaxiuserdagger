package com.app.taxiapp.model.apiresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlagCheckResponseModel {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data {
        @Expose
        @SerializedName("bookingNo")
        private int bookingNo;

        public int getBookingNo() {
            return bookingNo;
        }

        public void setBookingNo(int bookingNo) {
            this.bookingNo = bookingNo;
        }
    }
}
