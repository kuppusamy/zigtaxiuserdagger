package com.app.taxiapp.model.apiresponsemodel;

import com.app.taxiapp.helpers.UrlHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YourTripsModel {
    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data {
        @Expose
        @SerializedName("createdTime")
        private String createdTime;
        @Expose
        @SerializedName("paymentMode")
        private String paymentMode;
        @Expose
        @SerializedName("vehicleName")
        private String vehicleName;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("isActive")
        private String isActive;
        @Expose
        @SerializedName("total")
        private String total;
        @Expose
        @SerializedName("estimation")
        private String estimation;
        @Expose
        @SerializedName("destinyLong")
        private String destinyLong;
        @Expose
        @SerializedName("destinyLat")
        private String destinyLat;
        @Expose
        @SerializedName("soruceLong")
        private String soruceLong;
        @Expose
        @SerializedName("sourceLat")
        private String sourceLat;
        @Expose
        @SerializedName("bookingNo")
        private int bookingNo;

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getVehicleName() {
            return vehicleName;
        }

        public void setVehicleName(String vehicleName) {
            this.vehicleName = vehicleName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getEstimation() {
            return estimation;
        }

        public void setEstimation(String estimation) {
            this.estimation = estimation;
        }

        public String getDestinyLong() {
            return destinyLong;
        }

        public void setDestinyLong(String destinyLong) {
            this.destinyLong = destinyLong;
        }

        public String getDestinyLat() {
            return destinyLat;
        }

        public void setDestinyLat(String destinyLat) {
            this.destinyLat = destinyLat;
        }

        public String getSoruceLong() {
            return soruceLong;
        }

        public void setSoruceLong(String soruceLong) {
            this.soruceLong = soruceLong;
        }

        public String getSourceLat() {
            return sourceLat;
        }

        public void setSourceLat(String sourceLat) {
            this.sourceLat = sourceLat;
        }

        public int getBookingNo() {
            return bookingNo;
        }

        public void setBookingNo(int bookingNo) {
            this.bookingNo = bookingNo;
        }

        String getLatlongPath() {
            return getSourceLat() + "," + getSoruceLong() + "|" + getDestinyLat() + "," + getDestinyLong();
        }

        public String getPostFixStaticMap() {
            return UrlHelper.getPostFixUrlForStaticMap(getSourceLat(), getSoruceLong(), getDestinyLat(), getDestinyLong(), getLatlongPath());
        }


    }
}
