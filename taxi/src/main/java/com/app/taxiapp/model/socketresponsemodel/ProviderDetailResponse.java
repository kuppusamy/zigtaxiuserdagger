package com.app.taxiapp.model.socketresponsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ProviderDetailResponse {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("error")
    private boolean error;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class Data {

        @Expose
        @SerializedName("bearing")
        private int bearing;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("providerId")
        private int providerId;

        public int getBearing() {
            return bearing;
        }

        public void setBearing(int bearing) {
            this.bearing = bearing;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public int getProviderId() {
            return providerId;
        }

        public void setProviderId(int providerId) {
            this.providerId = providerId;
        }
    }
}
