package com.app.taxiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ProviderOfflineResponse {


    @Expose
    @SerializedName("data")
    private Data data = new Data();
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("error")
    private boolean error;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public class providerLocation implements Serializable {

        @Expose
        @SerializedName("rideTypeId")
        private int ridetypeid;
        @Expose
        @SerializedName("bearing")
        private int bearing;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("providerId")
        private int providerid;

        public int getRidetypeid() {
            return ridetypeid;
        }

        public void setRidetypeid(int ridetypeid) {
            this.ridetypeid = ridetypeid;
        }

        public int getBearing() {
            return bearing;
        }

        public void setBearing(int bearing) {
            this.bearing = bearing;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public int getProviderid() {
            return providerid;
        }

        public void setProviderid(int providerid) {
            this.providerid = providerid;
        }

    }

    public class Data implements Serializable {

        @SerializedName("providerLocation")
        ArrayList<providerLocation> value = new ArrayList<>();

        public ArrayList<providerLocation> getValue() {
            return value;
        }

        public void setValue(ArrayList<providerLocation> value) {
            this.value = value;
        }
    }
}