package com.app.taxiapp.networkcall;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class ImageLoader {
    public Context context;
    public ImageLoader(Context contextvalue) {
        context = contextvalue;
    }

    public void load(String url, ImageView imageView,Drawable drawable) {
        Glide.with(context).load(url).apply(new RequestOptions().placeholder(drawable).error(drawable)).into(imageView);
    }

    public void loadBitmap(byte[] url, ImageView imageView, Drawable drawable) {
        Glide.with(context).asBitmap().load(url).apply(new RequestOptions().placeholder(drawable).error(drawable)).into(imageView);
    }


    public interface ImageLoaded
    {
        public void onImageLoaded();
    }





}
