package com.app.taxiapp.networkcall.apicall;


import android.content.Context;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.app.taxiapp.R;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.taxiapp.AppController;
import com.app.taxiapp.helpers.Constants;
import com.app.taxiapp.helpers.ImageUtils;
import com.app.taxiapp.helpers.UrlHelper;
import com.app.taxiapp.helpers.Utils;
import com.app.taxiapp.helpers.prefhandler.SharedHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ApiCall {

    private static String TAG = ApiCall.class.getSimpleName();
    private static int MY_SOCKET_TIMEOUT_MS = 5000;


    public static void PostMethod(InputForAPI input, final ResponseHandler volleyCallback) {
        final String url = input.getUrl();
        final Context context = input.getContext();
        JSONObject params = input.getJsonObject();
        final HashMap<String, String> headers = input.getHeaders();

        if (Utils.isNetworkConnected(context)) {

            Utils.log(TAG, "url:" + url + "--input: " + params + "--headers: " + headers.toString());
            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);
                            if (response.has("error")) {
                                if (response.optString("error").equalsIgnoreCase("false")) {
                                    volleyCallback.setDataResponse(response);
                                } else {
                                    volleyCallback.setResponseError(response.optString("msg"));

                                }
                            } else if (response.has("status")) {
                                if (response.optString("status").equalsIgnoreCase("OK")) {
                                    volleyCallback.setDataResponse(response);
                                } else {
                                    volleyCallback.setResponseError(response.optString("error_message"));

                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.log(TAG, "url:" + url + ", onErrorResponse: " + error);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
                    } else if (error instanceof AuthFailureError) {
                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_authentication));
                        exitApp(context);
                    } else if (error instanceof ServerError) {
                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_server));
                    } else if (error instanceof NetworkError) {
                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_network));
                    } else if (error instanceof ParseError) {
                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_parse));
                    } else {
                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_time_out));
                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);

        } else {
            volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
        }
    }

    public static HashMap<String, String> getHeaders(Context context) {
        SharedHelper sharedHelper = new SharedHelper(context);
        HashMap<String, String> headers = new HashMap<>();
        headers.put(Constants.ApiKeys.ACCESS_TOKEN, sharedHelper.getToken());
        headers.put(Constants.ApiKeys.CONTENT_TYPE, "application/json");
        headers.put(Constants.ApiKeys.ROLE, "user");
        headers.put(Constants.ApiKeys.LANG, sharedHelper.getSelectedLanguage());
        return headers;
    }


    public static void GetMethod(InputForAPI input, final ResponseHandler volleyCallback) {
        final String url = input.getUrl();
        final Context context = input.getContext();
        final HashMap<String, String> headers = input.getHeaders();
        if (Utils.isNetworkConnected(context)) {
            Utils.log(TAG, "url:" + url + "--headers: " + headers.toString());

            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);

                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.setDataResponse(response);
                            } else {
                                if (url.equalsIgnoreCase(UrlHelper.GET_CURRENT_BOOKING)) {
                                    volleyCallback.setDataResponse(response);
                                } else {
                                    volleyCallback.setResponseError(response.optString("msg"));
                                }
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.log(TAG, "url:" + url + ", onErrorResponse: " + error);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));

                    } else if (error instanceof AuthFailureError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_authentication));
                        exitApp(context);

                    } else if (error instanceof ServerError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_server));

                    } else if (error instanceof NetworkError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_network));

                    } else if (error instanceof ParseError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_parse));

                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            AppController.getInstance().addToRequestQueue(jsonObjReq);

        } else {
            volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
        }
    }


    public static void fileUpload(final InputForAPI input, final ResponseHandler volleyCallback) {

        final String url = input.getUrl();
        final Context context = input.getContext();

        Utils.log(TAG, "--method:post " + "--url:" + url + " --input: " + input.getJsonObject() + " --headers: " + input.getHeaders().toString());

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject result = new JSONObject(resultResponse);
                    Utils.log(TAG, "url:" + url + ",response: " + result);

                    volleyCallback.setDataResponse(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));

                } else if (error instanceof AuthFailureError) {

                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_authentication));
                    exitApp(context);
                } else if (error instanceof ServerError) {

                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_server));

                } else if (error instanceof NetworkError) {

                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_network));

                } else if (error instanceof ParseError) {

                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_parse));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("type", input.getJsonObject().optString("type"));
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                try {
                    params.put("file", new DataPart(input.getFile().getName(), ImageUtils.convertImagetoByteData(input.getFile())));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(multipartRequest);
    }


    private static void exitApp(Context context) {
//        SharedHelper sharedHelper = new SharedHelper(context);
//        sharedHelper.setIsUserLoggedIn(false);
//        Intent intent = new Intent(context, SignInActivity.class);
//        intent.putExtra(Constants.IntentKeys.SKIP, true);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        context.startActivity(intent);
    }


    public interface ResponseHandler {

        public void setDataResponse(JSONObject response);

        public void setResponseError(String error);

    }

}
