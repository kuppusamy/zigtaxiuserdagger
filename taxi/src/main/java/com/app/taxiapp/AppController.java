package com.app.taxiapp;

import android.content.Context;
import android.text.TextUtils;

import androidx.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.app.taxiapp.helpers.rxbus.RxBus;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AppController {

    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    public static Context context;
    public static final String TAG = AppController.class.getSimpleName();
    RxBus rxBus;


    AppController(Context context) {
        MultiDex.install(context);
        rxBus = new RxBus();

    }

    public static synchronized AppController getInstance() {
        if (mInstance == null) {
            mInstance = new AppController(getApplicationContext());
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static Context getContext() {
        return context;
    }


    public RxBus getRxBus() {
        return rxBus;
    }

}
