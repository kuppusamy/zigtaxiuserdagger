// Generated code from Butter Knife gradle plugin. Do not modify!
package com.app.foodappuser;

import androidx.annotation.AnimRes;
import androidx.annotation.AttrRes;
import androidx.annotation.BoolRes;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.IntegerRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.MenuRes;
import androidx.annotation.PluralsRes;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import androidx.annotation.StyleableRes;

public final class R2 {
  public static final class anim {
    @AnimRes
    public static final int abc_fade_in = 1;

    @AnimRes
    public static final int abc_fade_out = 2;

    @AnimRes
    public static final int abc_grow_fade_in_from_bottom = 3;

    @AnimRes
    public static final int abc_popup_enter = 4;

    @AnimRes
    public static final int abc_popup_exit = 5;

    @AnimRes
    public static final int abc_shrink_fade_out_from_bottom = 6;

    @AnimRes
    public static final int abc_slide_in_bottom = 7;

    @AnimRes
    public static final int abc_slide_in_top = 8;

    @AnimRes
    public static final int abc_slide_out_bottom = 9;

    @AnimRes
    public static final int abc_slide_out_top = 10;

    @AnimRes
    public static final int abc_tooltip_enter = 11;

    @AnimRes
    public static final int abc_tooltip_exit = 12;

    @AnimRes
    public static final int bottom_down = 13;

    @AnimRes
    public static final int bottom_up = 14;

    @AnimRes
    public static final int btn_checkbox_to_checked_box_inner_merged_animation = 15;

    @AnimRes
    public static final int btn_checkbox_to_checked_box_outer_merged_animation = 16;

    @AnimRes
    public static final int btn_checkbox_to_checked_icon_null_animation = 17;

    @AnimRes
    public static final int btn_checkbox_to_unchecked_box_inner_merged_animation = 18;

    @AnimRes
    public static final int btn_checkbox_to_unchecked_check_path_merged_animation = 19;

    @AnimRes
    public static final int btn_checkbox_to_unchecked_icon_null_animation = 20;

    @AnimRes
    public static final int btn_radio_to_off_mtrl_dot_group_animation = 21;

    @AnimRes
    public static final int btn_radio_to_off_mtrl_ring_outer_animation = 22;

    @AnimRes
    public static final int btn_radio_to_off_mtrl_ring_outer_path_animation = 23;

    @AnimRes
    public static final int btn_radio_to_on_mtrl_dot_group_animation = 24;

    @AnimRes
    public static final int btn_radio_to_on_mtrl_ring_outer_animation = 25;

    @AnimRes
    public static final int btn_radio_to_on_mtrl_ring_outer_path_animation = 26;

    @AnimRes
    public static final int close_menu = 27;

    @AnimRes
    public static final int design_bottom_sheet_slide_in = 28;

    @AnimRes
    public static final int design_bottom_sheet_slide_out = 29;

    @AnimRes
    public static final int design_snackbar_in = 30;

    @AnimRes
    public static final int design_snackbar_out = 31;

    @AnimRes
    public static final int fade_in = 32;

    @AnimRes
    public static final int fade_out = 33;

    @AnimRes
    public static final int fragment_close_enter = 34;

    @AnimRes
    public static final int fragment_close_exit = 35;

    @AnimRes
    public static final int fragment_fade_enter = 36;

    @AnimRes
    public static final int fragment_fade_exit = 37;

    @AnimRes
    public static final int fragment_fast_out_extra_slow_in = 38;

    @AnimRes
    public static final int fragment_open_enter = 39;

    @AnimRes
    public static final int fragment_open_exit = 40;

    @AnimRes
    public static final int layout_animation_from_bottom = 41;

    @AnimRes
    public static final int nothing = 42;

    @AnimRes
    public static final int recycler_right_to_left = 43;

    @AnimRes
    public static final int right_to_left = 44;

    @AnimRes
    public static final int slide_bottom = 45;

    @AnimRes
    public static final int slide_from_bottom = 46;

    @AnimRes
    public static final int slide_in_left = 47;

    @AnimRes
    public static final int slide_in_right = 48;

    @AnimRes
    public static final int slide_out_left = 49;

    @AnimRes
    public static final int slide_out_right = 50;

    @AnimRes
    public static final int slide_up = 51;

    @AnimRes
    public static final int tooltip_enter = 52;

    @AnimRes
    public static final int tooltip_exit = 53;
  }

  public static final class attr {
    @AttrRes
    public static final int actionBarDivider = 54;

    @AttrRes
    public static final int actionBarItemBackground = 55;

    @AttrRes
    public static final int actionBarPopupTheme = 56;

    @AttrRes
    public static final int actionBarSize = 57;

    @AttrRes
    public static final int actionBarSplitStyle = 58;

    @AttrRes
    public static final int actionBarStyle = 59;

    @AttrRes
    public static final int actionBarTabBarStyle = 60;

    @AttrRes
    public static final int actionBarTabStyle = 61;

    @AttrRes
    public static final int actionBarTabTextStyle = 62;

    @AttrRes
    public static final int actionBarTheme = 63;

    @AttrRes
    public static final int actionBarWidgetTheme = 64;

    @AttrRes
    public static final int actionButtonStyle = 65;

    @AttrRes
    public static final int actionDropDownStyle = 66;

    @AttrRes
    public static final int actionLayout = 67;

    @AttrRes
    public static final int actionMenuTextAppearance = 68;

    @AttrRes
    public static final int actionMenuTextColor = 69;

    @AttrRes
    public static final int actionModeBackground = 70;

    @AttrRes
    public static final int actionModeCloseButtonStyle = 71;

    @AttrRes
    public static final int actionModeCloseDrawable = 72;

    @AttrRes
    public static final int actionModeCopyDrawable = 73;

    @AttrRes
    public static final int actionModeCutDrawable = 74;

    @AttrRes
    public static final int actionModeFindDrawable = 75;

    @AttrRes
    public static final int actionModePasteDrawable = 76;

    @AttrRes
    public static final int actionModePopupWindowStyle = 77;

    @AttrRes
    public static final int actionModeSelectAllDrawable = 78;

    @AttrRes
    public static final int actionModeShareDrawable = 79;

    @AttrRes
    public static final int actionModeSplitBackground = 80;

    @AttrRes
    public static final int actionModeStyle = 81;

    @AttrRes
    public static final int actionModeWebSearchDrawable = 82;

    @AttrRes
    public static final int actionOverflowButtonStyle = 83;

    @AttrRes
    public static final int actionOverflowMenuStyle = 84;

    @AttrRes
    public static final int actionProviderClass = 85;

    @AttrRes
    public static final int actionViewClass = 86;

    @AttrRes
    public static final int activityChooserViewStyle = 87;

    @AttrRes
    public static final int alertDialogButtonGroupStyle = 88;

    @AttrRes
    public static final int alertDialogCenterButtons = 89;

    @AttrRes
    public static final int alertDialogStyle = 90;

    @AttrRes
    public static final int alertDialogTheme = 91;

    @AttrRes
    public static final int allowStacking = 92;

    @AttrRes
    public static final int alpha = 93;

    @AttrRes
    public static final int alphabeticModifiers = 94;

    @AttrRes
    public static final int ambientEnabled = 95;

    @AttrRes
    public static final int arrowHeadLength = 96;

    @AttrRes
    public static final int arrowShaftLength = 97;

    @AttrRes
    public static final int autoCompleteTextViewStyle = 98;

    @AttrRes
    public static final int autoSizeMaxTextSize = 99;

    @AttrRes
    public static final int autoSizeMinTextSize = 100;

    @AttrRes
    public static final int autoSizePresetSizes = 101;

    @AttrRes
    public static final int autoSizeStepGranularity = 102;

    @AttrRes
    public static final int autoSizeTextType = 103;

    @AttrRes
    public static final int avi_animation_enable = 104;

    @AttrRes
    public static final int avi_animator = 105;

    @AttrRes
    public static final int avi_animator_reverse = 106;

    @AttrRes
    public static final int avi_drawable = 107;

    @AttrRes
    public static final int avi_drawable_unselected = 108;

    @AttrRes
    public static final int avi_gravity = 109;

    @AttrRes
    public static final int avi_height = 110;

    @AttrRes
    public static final int avi_margin = 111;

    @AttrRes
    public static final int avi_orientation = 112;

    @AttrRes
    public static final int avi_width = 113;

    @AttrRes
    public static final int background = 114;

    @AttrRes
    public static final int backgroundColor = 115;

    @AttrRes
    public static final int backgroundSplit = 116;

    @AttrRes
    public static final int backgroundStacked = 117;

    @AttrRes
    public static final int backgroundTint = 118;

    @AttrRes
    public static final int backgroundTintMode = 119;

    @AttrRes
    public static final int barLength = 120;

    @AttrRes
    public static final int barrierAllowsGoneWidgets = 121;

    @AttrRes
    public static final int barrierDirection = 122;

    @AttrRes
    public static final int behavior_autoHide = 123;

    @AttrRes
    public static final int behavior_fitToContents = 124;

    @AttrRes
    public static final int behavior_hideable = 125;

    @AttrRes
    public static final int behavior_overlapTop = 126;

    @AttrRes
    public static final int behavior_peekHeight = 127;

    @AttrRes
    public static final int behavior_skipCollapsed = 128;

    @AttrRes
    public static final int borderWidth = 129;

    @AttrRes
    public static final int borderlessButtonStyle = 130;

    @AttrRes
    public static final int bottomAppBarStyle = 131;

    @AttrRes
    public static final int bottomNavigationStyle = 132;

    @AttrRes
    public static final int bottomSheetDialogTheme = 133;

    @AttrRes
    public static final int bottomSheetStyle = 134;

    @AttrRes
    public static final int boxBackgroundColor = 135;

    @AttrRes
    public static final int boxBackgroundMode = 136;

    @AttrRes
    public static final int boxCollapsedPaddingTop = 137;

    @AttrRes
    public static final int boxCornerRadiusBottomEnd = 138;

    @AttrRes
    public static final int boxCornerRadiusBottomStart = 139;

    @AttrRes
    public static final int boxCornerRadiusTopEnd = 140;

    @AttrRes
    public static final int boxCornerRadiusTopStart = 141;

    @AttrRes
    public static final int boxStrokeColor = 142;

    @AttrRes
    public static final int boxStrokeWidth = 143;

    @AttrRes
    public static final int buttonBarButtonStyle = 144;

    @AttrRes
    public static final int buttonBarNegativeButtonStyle = 145;

    @AttrRes
    public static final int buttonBarNeutralButtonStyle = 146;

    @AttrRes
    public static final int buttonBarPositiveButtonStyle = 147;

    @AttrRes
    public static final int buttonBarStyle = 148;

    @AttrRes
    public static final int buttonCompat = 149;

    @AttrRes
    public static final int buttonGravity = 150;

    @AttrRes
    public static final int buttonIconDimen = 151;

    @AttrRes
    public static final int buttonPanelSideLayout = 152;

    @AttrRes
    public static final int buttonSize = 153;

    @AttrRes
    public static final int buttonStyle = 154;

    @AttrRes
    public static final int buttonStyleSmall = 155;

    @AttrRes
    public static final int buttonTint = 156;

    @AttrRes
    public static final int buttonTintMode = 157;

    @AttrRes
    public static final int cameraBearing = 158;

    @AttrRes
    public static final int cameraMaxZoomPreference = 159;

    @AttrRes
    public static final int cameraMinZoomPreference = 160;

    @AttrRes
    public static final int cameraTargetLat = 161;

    @AttrRes
    public static final int cameraTargetLng = 162;

    @AttrRes
    public static final int cameraTilt = 163;

    @AttrRes
    public static final int cameraZoom = 164;

    @AttrRes
    public static final int canExpand = 165;

    @AttrRes
    public static final int cardBackgroundColor = 166;

    @AttrRes
    public static final int cardCornerRadius = 167;

    @AttrRes
    public static final int cardElevation = 168;

    @AttrRes
    public static final int cardHintText = 169;

    @AttrRes
    public static final int cardMaxElevation = 170;

    @AttrRes
    public static final int cardPreventCornerOverlap = 171;

    @AttrRes
    public static final int cardTextErrorColor = 172;

    @AttrRes
    public static final int cardTint = 173;

    @AttrRes
    public static final int cardUseCompatPadding = 174;

    @AttrRes
    public static final int cardViewStyle = 175;

    @AttrRes
    public static final int ccpDialog_allowSearch = 176;

    @AttrRes
    public static final int ccpDialog_backgroundColor = 177;

    @AttrRes
    public static final int ccpDialog_fastScroller_bubbleColor = 178;

    @AttrRes
    public static final int ccpDialog_fastScroller_bubbleTextAppearance = 179;

    @AttrRes
    public static final int ccpDialog_fastScroller_handleColor = 180;

    @AttrRes
    public static final int ccpDialog_initialScrollToSelection = 181;

    @AttrRes
    public static final int ccpDialog_keyboardAutoPopup = 182;

    @AttrRes
    public static final int ccpDialog_searchEditTextTint = 183;

    @AttrRes
    public static final int ccpDialog_showCloseIcon = 184;

    @AttrRes
    public static final int ccpDialog_showFastScroller = 185;

    @AttrRes
    public static final int ccpDialog_showFlag = 186;

    @AttrRes
    public static final int ccpDialog_showNameCode = 187;

    @AttrRes
    public static final int ccpDialog_showPhoneCode = 188;

    @AttrRes
    public static final int ccpDialog_showTitle = 189;

    @AttrRes
    public static final int ccpDialog_textColor = 190;

    @AttrRes
    public static final int ccp_areaCodeDetectedCountry = 191;

    @AttrRes
    public static final int ccp_arrowColor = 192;

    @AttrRes
    public static final int ccp_arrowSize = 193;

    @AttrRes
    public static final int ccp_autoDetectCountry = 194;

    @AttrRes
    public static final int ccp_autoDetectLanguage = 195;

    @AttrRes
    public static final int ccp_autoFormatNumber = 196;

    @AttrRes
    public static final int ccp_clickable = 197;

    @AttrRes
    public static final int ccp_contentColor = 198;

    @AttrRes
    public static final int ccp_countryAutoDetectionPref = 199;

    @AttrRes
    public static final int ccp_countryPreference = 200;

    @AttrRes
    public static final int ccp_customMasterCountries = 201;

    @AttrRes
    public static final int ccp_defaultLanguage = 202;

    @AttrRes
    public static final int ccp_defaultNameCode = 203;

    @AttrRes
    public static final int ccp_defaultPhoneCode = 204;

    @AttrRes
    public static final int ccp_excludedCountries = 205;

    @AttrRes
    public static final int ccp_flagBorderColor = 206;

    @AttrRes
    public static final int ccp_hintExampleNumber = 207;

    @AttrRes
    public static final int ccp_hintExampleNumberType = 208;

    @AttrRes
    public static final int ccp_internationalFormattingOnly = 209;

    @AttrRes
    public static final int ccp_rememberLastSelection = 210;

    @AttrRes
    public static final int ccp_selectionMemoryTag = 211;

    @AttrRes
    public static final int ccp_showArrow = 212;

    @AttrRes
    public static final int ccp_showFlag = 213;

    @AttrRes
    public static final int ccp_showFullName = 214;

    @AttrRes
    public static final int ccp_showNameCode = 215;

    @AttrRes
    public static final int ccp_showPhoneCode = 216;

    @AttrRes
    public static final int ccp_textGravity = 217;

    @AttrRes
    public static final int ccp_textSize = 218;

    @AttrRes
    public static final int ccp_useDummyEmojiForPreview = 219;

    @AttrRes
    public static final int ccp_useFlagEmoji = 220;

    @AttrRes
    public static final int chainUseRtl = 221;

    @AttrRes
    public static final int checkboxStyle = 222;

    @AttrRes
    public static final int checkedChip = 223;

    @AttrRes
    public static final int checkedIcon = 224;

    @AttrRes
    public static final int checkedIconEnabled = 225;

    @AttrRes
    public static final int checkedIconVisible = 226;

    @AttrRes
    public static final int checkedTextViewStyle = 227;

    @AttrRes
    public static final int chipBackgroundColor = 228;

    @AttrRes
    public static final int chipCornerRadius = 229;

    @AttrRes
    public static final int chipEndPadding = 230;

    @AttrRes
    public static final int chipGroupStyle = 231;

    @AttrRes
    public static final int chipIcon = 232;

    @AttrRes
    public static final int chipIconEnabled = 233;

    @AttrRes
    public static final int chipIconSize = 234;

    @AttrRes
    public static final int chipIconTint = 235;

    @AttrRes
    public static final int chipIconVisible = 236;

    @AttrRes
    public static final int chipMinHeight = 237;

    @AttrRes
    public static final int chipSpacing = 238;

    @AttrRes
    public static final int chipSpacingHorizontal = 239;

    @AttrRes
    public static final int chipSpacingVertical = 240;

    @AttrRes
    public static final int chipStandaloneStyle = 241;

    @AttrRes
    public static final int chipStartPadding = 242;

    @AttrRes
    public static final int chipStrokeColor = 243;

    @AttrRes
    public static final int chipStrokeWidth = 244;

    @AttrRes
    public static final int chipStyle = 245;

    @AttrRes
    public static final int ci_animator = 246;

    @AttrRes
    public static final int ci_animator_reverse = 247;

    @AttrRes
    public static final int ci_drawable = 248;

    @AttrRes
    public static final int ci_drawable_unselected = 249;

    @AttrRes
    public static final int ci_gravity = 250;

    @AttrRes
    public static final int ci_height = 251;

    @AttrRes
    public static final int ci_margin = 252;

    @AttrRes
    public static final int ci_orientation = 253;

    @AttrRes
    public static final int ci_width = 254;

    @AttrRes
    public static final int circleCrop = 255;

    @AttrRes
    public static final int civ_border_color = 256;

    @AttrRes
    public static final int civ_border_overlay = 257;

    @AttrRes
    public static final int civ_border_width = 258;

    @AttrRes
    public static final int civ_circle_background_color = 259;

    @AttrRes
    public static final int closeIcon = 260;

    @AttrRes
    public static final int closeIconEnabled = 261;

    @AttrRes
    public static final int closeIconEndPadding = 262;

    @AttrRes
    public static final int closeIconSize = 263;

    @AttrRes
    public static final int closeIconStartPadding = 264;

    @AttrRes
    public static final int closeIconTint = 265;

    @AttrRes
    public static final int closeIconVisible = 266;

    @AttrRes
    public static final int closeItemLayout = 267;

    @AttrRes
    public static final int collapseContentDescription = 268;

    @AttrRes
    public static final int collapseIcon = 269;

    @AttrRes
    public static final int collapsedTitleGravity = 270;

    @AttrRes
    public static final int collapsedTitleTextAppearance = 271;

    @AttrRes
    public static final int color = 272;

    @AttrRes
    public static final int colorAccent = 273;

    @AttrRes
    public static final int colorBackgroundFloating = 274;

    @AttrRes
    public static final int colorButtonNormal = 275;

    @AttrRes
    public static final int colorControlActivated = 276;

    @AttrRes
    public static final int colorControlHighlight = 277;

    @AttrRes
    public static final int colorControlNormal = 278;

    @AttrRes
    public static final int colorError = 279;

    @AttrRes
    public static final int colorPrimary = 280;

    @AttrRes
    public static final int colorPrimaryDark = 281;

    @AttrRes
    public static final int colorScheme = 282;

    @AttrRes
    public static final int colorSecondary = 283;

    @AttrRes
    public static final int colorSwitchThumbNormal = 284;

    @AttrRes
    public static final int com_facebook_auxiliary_view_position = 285;

    @AttrRes
    public static final int com_facebook_confirm_logout = 286;

    @AttrRes
    public static final int com_facebook_foreground_color = 287;

    @AttrRes
    public static final int com_facebook_horizontal_alignment = 288;

    @AttrRes
    public static final int com_facebook_is_cropped = 289;

    @AttrRes
    public static final int com_facebook_login_text = 290;

    @AttrRes
    public static final int com_facebook_logout_text = 291;

    @AttrRes
    public static final int com_facebook_object_id = 292;

    @AttrRes
    public static final int com_facebook_object_type = 293;

    @AttrRes
    public static final int com_facebook_preset_size = 294;

    @AttrRes
    public static final int com_facebook_style = 295;

    @AttrRes
    public static final int com_facebook_tooltip_mode = 296;

    @AttrRes
    public static final int commitIcon = 297;

    @AttrRes
    public static final int constraintSet = 298;

    @AttrRes
    public static final int constraint_referenced_ids = 299;

    @AttrRes
    public static final int content = 300;

    @AttrRes
    public static final int contentDescription = 301;

    @AttrRes
    public static final int contentInsetEnd = 302;

    @AttrRes
    public static final int contentInsetEndWithActions = 303;

    @AttrRes
    public static final int contentInsetLeft = 304;

    @AttrRes
    public static final int contentInsetRight = 305;

    @AttrRes
    public static final int contentInsetStart = 306;

    @AttrRes
    public static final int contentInsetStartWithNavigation = 307;

    @AttrRes
    public static final int contentPadding = 308;

    @AttrRes
    public static final int contentPaddingBottom = 309;

    @AttrRes
    public static final int contentPaddingLeft = 310;

    @AttrRes
    public static final int contentPaddingRight = 311;

    @AttrRes
    public static final int contentPaddingTop = 312;

    @AttrRes
    public static final int contentScrim = 313;

    @AttrRes
    public static final int controlBackground = 314;

    @AttrRes
    public static final int coordinatorLayoutStyle = 315;

    @AttrRes
    public static final int cornerRadius = 316;

    @AttrRes
    public static final int cornerRadiusBL = 317;

    @AttrRes
    public static final int cornerRadiusBR = 318;

    @AttrRes
    public static final int cornerRadiusTL = 319;

    @AttrRes
    public static final int cornerRadiusTR = 320;

    @AttrRes
    public static final int counterEnabled = 321;

    @AttrRes
    public static final int counterMaxLength = 322;

    @AttrRes
    public static final int counterOverflowTextAppearance = 323;

    @AttrRes
    public static final int counterTextAppearance = 324;

    @AttrRes
    public static final int cursorColor = 325;

    @AttrRes
    public static final int cursorWidth = 326;

    @AttrRes
    public static final int customNavigationLayout = 327;

    @AttrRes
    public static final int dashGap = 328;

    @AttrRes
    public static final int dashLength = 329;

    @AttrRes
    public static final int dashThickness = 330;

    @AttrRes
    public static final int defaultQueryHint = 331;

    @AttrRes
    public static final int dialogCornerRadius = 332;

    @AttrRes
    public static final int dialogPreferredPadding = 333;

    @AttrRes
    public static final int dialogTheme = 334;

    @AttrRes
    public static final int displayOptions = 335;

    @AttrRes
    public static final int divider = 336;

    @AttrRes
    public static final int dividerHorizontal = 337;

    @AttrRes
    public static final int dividerPadding = 338;

    @AttrRes
    public static final int dividerVertical = 339;

    @AttrRes
    public static final int drawableBottomCompat = 340;

    @AttrRes
    public static final int drawableEndCompat = 341;

    @AttrRes
    public static final int drawableLeftCompat = 342;

    @AttrRes
    public static final int drawableRightCompat = 343;

    @AttrRes
    public static final int drawableSize = 344;

    @AttrRes
    public static final int drawableStartCompat = 345;

    @AttrRes
    public static final int drawableTint = 346;

    @AttrRes
    public static final int drawableTintMode = 347;

    @AttrRes
    public static final int drawableTopCompat = 348;

    @AttrRes
    public static final int drawerArrowStyle = 349;

    @AttrRes
    public static final int dropDownListViewStyle = 350;

    @AttrRes
    public static final int dropdownListPreferredItemHeight = 351;

    @AttrRes
    public static final int editTextBackground = 352;

    @AttrRes
    public static final int editTextColor = 353;

    @AttrRes
    public static final int editTextStyle = 354;

    @AttrRes
    public static final int elevation = 355;

    @AttrRes
    public static final int emptyVisibility = 356;

    @AttrRes
    public static final int enforceMaterialTheme = 357;

    @AttrRes
    public static final int enforceTextAppearance = 358;

    @AttrRes
    public static final int errorEnabled = 359;

    @AttrRes
    public static final int errorTextAppearance = 360;

    @AttrRes
    public static final int expandActivityOverflowButtonDrawable = 361;

    @AttrRes
    public static final int expanded = 362;

    @AttrRes
    public static final int expandedTitleGravity = 363;

    @AttrRes
    public static final int expandedTitleMargin = 364;

    @AttrRes
    public static final int expandedTitleMarginBottom = 365;

    @AttrRes
    public static final int expandedTitleMarginEnd = 366;

    @AttrRes
    public static final int expandedTitleMarginStart = 367;

    @AttrRes
    public static final int expandedTitleMarginTop = 368;

    @AttrRes
    public static final int expandedTitleTextAppearance = 369;

    @AttrRes
    public static final int fabAlignmentMode = 370;

    @AttrRes
    public static final int fabCradleMargin = 371;

    @AttrRes
    public static final int fabCradleRoundedCornerRadius = 372;

    @AttrRes
    public static final int fabCradleVerticalOffset = 373;

    @AttrRes
    public static final int fabCustomSize = 374;

    @AttrRes
    public static final int fabSize = 375;

    @AttrRes
    public static final int fastScrollEnabled = 376;

    @AttrRes
    public static final int fastScrollHorizontalThumbDrawable = 377;

    @AttrRes
    public static final int fastScrollHorizontalTrackDrawable = 378;

    @AttrRes
    public static final int fastScrollVerticalThumbDrawable = 379;

    @AttrRes
    public static final int fastScrollVerticalTrackDrawable = 380;

    @AttrRes
    public static final int fastscroll__bubbleColor = 381;

    @AttrRes
    public static final int fastscroll__bubbleTextAppearance = 382;

    @AttrRes
    public static final int fastscroll__handleColor = 383;

    @AttrRes
    public static final int fastscroll__style = 384;

    @AttrRes
    public static final int firstBaselineToTopHeight = 385;

    @AttrRes
    public static final int floatingActionButtonStyle = 386;

    @AttrRes
    public static final int font = 387;

    @AttrRes
    public static final int fontFamily = 388;

    @AttrRes
    public static final int fontProviderAuthority = 389;

    @AttrRes
    public static final int fontProviderCerts = 390;

    @AttrRes
    public static final int fontProviderFetchStrategy = 391;

    @AttrRes
    public static final int fontProviderFetchTimeout = 392;

    @AttrRes
    public static final int fontProviderPackage = 393;

    @AttrRes
    public static final int fontProviderQuery = 394;

    @AttrRes
    public static final int fontStyle = 395;

    @AttrRes
    public static final int fontVariationSettings = 396;

    @AttrRes
    public static final int fontWeight = 397;

    @AttrRes
    public static final int font_name = 398;

    @AttrRes
    public static final int foregroundColor = 399;

    @AttrRes
    public static final int foregroundInsidePadding = 400;

    @AttrRes
    public static final int gapBetweenBars = 401;

    @AttrRes
    public static final int goIcon = 402;

    @AttrRes
    public static final int headerLayout = 403;

    @AttrRes
    public static final int height = 404;

    @AttrRes
    public static final int helperText = 405;

    @AttrRes
    public static final int helperTextEnabled = 406;

    @AttrRes
    public static final int helperTextTextAppearance = 407;

    @AttrRes
    public static final int hideLineWhenFilled = 408;

    @AttrRes
    public static final int hideMotionSpec = 409;

    @AttrRes
    public static final int hideOnContentScroll = 410;

    @AttrRes
    public static final int hideOnScroll = 411;

    @AttrRes
    public static final int hintAnimationEnabled = 412;

    @AttrRes
    public static final int hintEnabled = 413;

    @AttrRes
    public static final int hintTextAppearance = 414;

    @AttrRes
    public static final int homeAsUpIndicator = 415;

    @AttrRes
    public static final int homeLayout = 416;

    @AttrRes
    public static final int hoveredFocusedTranslationZ = 417;

    @AttrRes
    public static final int icon = 418;

    @AttrRes
    public static final int iconEndPadding = 419;

    @AttrRes
    public static final int iconGravity = 420;

    @AttrRes
    public static final int iconPadding = 421;

    @AttrRes
    public static final int iconSize = 422;

    @AttrRes
    public static final int iconStartPadding = 423;

    @AttrRes
    public static final int iconTint = 424;

    @AttrRes
    public static final int iconTintMode = 425;

    @AttrRes
    public static final int iconifiedByDefault = 426;

    @AttrRes
    public static final int imageAspectRatio = 427;

    @AttrRes
    public static final int imageAspectRatioAdjust = 428;

    @AttrRes
    public static final int imageButtonStyle = 429;

    @AttrRes
    public static final int indeterminateProgressStyle = 430;

    @AttrRes
    public static final int initialActivityCount = 431;

    @AttrRes
    public static final int insetForeground = 432;

    @AttrRes
    public static final int isLightTheme = 433;

    @AttrRes
    public static final int itemBackground = 434;

    @AttrRes
    public static final int itemCount = 435;

    @AttrRes
    public static final int itemHeight = 436;

    @AttrRes
    public static final int itemHorizontalPadding = 437;

    @AttrRes
    public static final int itemHorizontalTranslationEnabled = 438;

    @AttrRes
    public static final int itemIconPadding = 439;

    @AttrRes
    public static final int itemIconSize = 440;

    @AttrRes
    public static final int itemIconTint = 441;

    @AttrRes
    public static final int itemPadding = 442;

    @AttrRes
    public static final int itemRadius = 443;

    @AttrRes
    public static final int itemSpacing = 444;

    @AttrRes
    public static final int itemTextAppearance = 445;

    @AttrRes
    public static final int itemTextAppearanceActive = 446;

    @AttrRes
    public static final int itemTextAppearanceInactive = 447;

    @AttrRes
    public static final int itemTextColor = 448;

    @AttrRes
    public static final int itemWidth = 449;

    @AttrRes
    public static final int keylines = 450;

    @AttrRes
    public static final int labelVisibilityMode = 451;

    @AttrRes
    public static final int lastBaselineToBottomHeight = 452;

    @AttrRes
    public static final int latLngBoundsNorthEastLatitude = 453;

    @AttrRes
    public static final int latLngBoundsNorthEastLongitude = 454;

    @AttrRes
    public static final int latLngBoundsSouthWestLatitude = 455;

    @AttrRes
    public static final int latLngBoundsSouthWestLongitude = 456;

    @AttrRes
    public static final int layout = 457;

    @AttrRes
    public static final int layoutManager = 458;

    @AttrRes
    public static final int layout_anchor = 459;

    @AttrRes
    public static final int layout_anchorGravity = 460;

    @AttrRes
    public static final int layout_behavior = 461;

    @AttrRes
    public static final int layout_collapseMode = 462;

    @AttrRes
    public static final int layout_collapseParallaxMultiplier = 463;

    @AttrRes
    public static final int layout_constrainedHeight = 464;

    @AttrRes
    public static final int layout_constrainedWidth = 465;

    @AttrRes
    public static final int layout_constraintBaseline_creator = 466;

    @AttrRes
    public static final int layout_constraintBaseline_toBaselineOf = 467;

    @AttrRes
    public static final int layout_constraintBottom_creator = 468;

    @AttrRes
    public static final int layout_constraintBottom_toBottomOf = 469;

    @AttrRes
    public static final int layout_constraintBottom_toTopOf = 470;

    @AttrRes
    public static final int layout_constraintCircle = 471;

    @AttrRes
    public static final int layout_constraintCircleAngle = 472;

    @AttrRes
    public static final int layout_constraintCircleRadius = 473;

    @AttrRes
    public static final int layout_constraintDimensionRatio = 474;

    @AttrRes
    public static final int layout_constraintEnd_toEndOf = 475;

    @AttrRes
    public static final int layout_constraintEnd_toStartOf = 476;

    @AttrRes
    public static final int layout_constraintGuide_begin = 477;

    @AttrRes
    public static final int layout_constraintGuide_end = 478;

    @AttrRes
    public static final int layout_constraintGuide_percent = 479;

    @AttrRes
    public static final int layout_constraintHeight_default = 480;

    @AttrRes
    public static final int layout_constraintHeight_max = 481;

    @AttrRes
    public static final int layout_constraintHeight_min = 482;

    @AttrRes
    public static final int layout_constraintHeight_percent = 483;

    @AttrRes
    public static final int layout_constraintHorizontal_bias = 484;

    @AttrRes
    public static final int layout_constraintHorizontal_chainStyle = 485;

    @AttrRes
    public static final int layout_constraintHorizontal_weight = 486;

    @AttrRes
    public static final int layout_constraintLeft_creator = 487;

    @AttrRes
    public static final int layout_constraintLeft_toLeftOf = 488;

    @AttrRes
    public static final int layout_constraintLeft_toRightOf = 489;

    @AttrRes
    public static final int layout_constraintRight_creator = 490;

    @AttrRes
    public static final int layout_constraintRight_toLeftOf = 491;

    @AttrRes
    public static final int layout_constraintRight_toRightOf = 492;

    @AttrRes
    public static final int layout_constraintStart_toEndOf = 493;

    @AttrRes
    public static final int layout_constraintStart_toStartOf = 494;

    @AttrRes
    public static final int layout_constraintTop_creator = 495;

    @AttrRes
    public static final int layout_constraintTop_toBottomOf = 496;

    @AttrRes
    public static final int layout_constraintTop_toTopOf = 497;

    @AttrRes
    public static final int layout_constraintVertical_bias = 498;

    @AttrRes
    public static final int layout_constraintVertical_chainStyle = 499;

    @AttrRes
    public static final int layout_constraintVertical_weight = 500;

    @AttrRes
    public static final int layout_constraintWidth_default = 501;

    @AttrRes
    public static final int layout_constraintWidth_max = 502;

    @AttrRes
    public static final int layout_constraintWidth_min = 503;

    @AttrRes
    public static final int layout_constraintWidth_percent = 504;

    @AttrRes
    public static final int layout_dodgeInsetEdges = 505;

    @AttrRes
    public static final int layout_editor_absoluteX = 506;

    @AttrRes
    public static final int layout_editor_absoluteY = 507;

    @AttrRes
    public static final int layout_goneMarginBottom = 508;

    @AttrRes
    public static final int layout_goneMarginEnd = 509;

    @AttrRes
    public static final int layout_goneMarginLeft = 510;

    @AttrRes
    public static final int layout_goneMarginRight = 511;

    @AttrRes
    public static final int layout_goneMarginStart = 512;

    @AttrRes
    public static final int layout_goneMarginTop = 513;

    @AttrRes
    public static final int layout_gravity = 514;

    @AttrRes
    public static final int layout_insetEdge = 515;

    @AttrRes
    public static final int layout_keyline = 516;

    @AttrRes
    public static final int layout_optimizationLevel = 517;

    @AttrRes
    public static final int layout_scrollFlags = 518;

    @AttrRes
    public static final int layout_scrollInterpolator = 519;

    @AttrRes
    public static final int liftOnScroll = 520;

    @AttrRes
    public static final int lineColor = 521;

    @AttrRes
    public static final int lineHeight = 522;

    @AttrRes
    public static final int lineSpacing = 523;

    @AttrRes
    public static final int lineWidth = 524;

    @AttrRes
    public static final int listChoiceBackgroundIndicator = 525;

    @AttrRes
    public static final int listChoiceIndicatorMultipleAnimated = 526;

    @AttrRes
    public static final int listChoiceIndicatorSingleAnimated = 527;

    @AttrRes
    public static final int listDividerAlertDialog = 528;

    @AttrRes
    public static final int listItemLayout = 529;

    @AttrRes
    public static final int listLayout = 530;

    @AttrRes
    public static final int listMenuViewStyle = 531;

    @AttrRes
    public static final int listPopupWindowStyle = 532;

    @AttrRes
    public static final int listPreferredItemHeight = 533;

    @AttrRes
    public static final int listPreferredItemHeightLarge = 534;

    @AttrRes
    public static final int listPreferredItemHeightSmall = 535;

    @AttrRes
    public static final int listPreferredItemPaddingEnd = 536;

    @AttrRes
    public static final int listPreferredItemPaddingLeft = 537;

    @AttrRes
    public static final int listPreferredItemPaddingRight = 538;

    @AttrRes
    public static final int listPreferredItemPaddingStart = 539;

    @AttrRes
    public static final int liteMode = 540;

    @AttrRes
    public static final int logo = 541;

    @AttrRes
    public static final int logoDescription = 542;

    @AttrRes
    public static final int mapType = 543;

    @AttrRes
    public static final int materialButtonStyle = 544;

    @AttrRes
    public static final int materialCardViewStyle = 545;

    @AttrRes
    public static final int maxActionInlineWidth = 546;

    @AttrRes
    public static final int maxButtonHeight = 547;

    @AttrRes
    public static final int maxImageSize = 548;

    @AttrRes
    public static final int measureWithLargestChild = 549;

    @AttrRes
    public static final int menu = 550;

    @AttrRes
    public static final int multiChoiceItemLayout = 551;

    @AttrRes
    public static final int mv_backgroundColor = 552;

    @AttrRes
    public static final int mv_cornerRadius = 553;

    @AttrRes
    public static final int mv_isRadiusHalfHeight = 554;

    @AttrRes
    public static final int mv_isWidthHeightEqual = 555;

    @AttrRes
    public static final int mv_strokeColor = 556;

    @AttrRes
    public static final int mv_strokeWidth = 557;

    @AttrRes
    public static final int navigationContentDescription = 558;

    @AttrRes
    public static final int navigationIcon = 559;

    @AttrRes
    public static final int navigationMode = 560;

    @AttrRes
    public static final int navigationViewStyle = 561;

    @AttrRes
    public static final int numericModifiers = 562;

    @AttrRes
    public static final int orientation = 563;

    @AttrRes
    public static final int otpViewStyle = 564;

    @AttrRes
    public static final int overlapAnchor = 565;

    @AttrRes
    public static final int paddingBottomNoButtons = 566;

    @AttrRes
    public static final int paddingEnd = 567;

    @AttrRes
    public static final int paddingStart = 568;

    @AttrRes
    public static final int paddingTopNoTitle = 569;

    @AttrRes
    public static final int panelBackground = 570;

    @AttrRes
    public static final int panelMenuListTheme = 571;

    @AttrRes
    public static final int panelMenuListWidth = 572;

    @AttrRes
    public static final int passwordToggleContentDescription = 573;

    @AttrRes
    public static final int passwordToggleDrawable = 574;

    @AttrRes
    public static final int passwordToggleEnabled = 575;

    @AttrRes
    public static final int passwordToggleTint = 576;

    @AttrRes
    public static final int passwordToggleTintMode = 577;

    @AttrRes
    public static final int popupMenuStyle = 578;

    @AttrRes
    public static final int popupTheme = 579;

    @AttrRes
    public static final int popupWindowStyle = 580;

    @AttrRes
    public static final int preserveIconSpacing = 581;

    @AttrRes
    public static final int pressedTranslationZ = 582;

    @AttrRes
    public static final int progressBarPadding = 583;

    @AttrRes
    public static final int progressBarStyle = 584;

    @AttrRes
    public static final int queryBackground = 585;

    @AttrRes
    public static final int queryHint = 586;

    @AttrRes
    public static final int radioButtonStyle = 587;

    @AttrRes
    public static final int ratingBarStyle = 588;

    @AttrRes
    public static final int ratingBarStyleIndicator = 589;

    @AttrRes
    public static final int ratingBarStyleSmall = 590;

    @AttrRes
    public static final int reverseLayout = 591;

    @AttrRes
    public static final int rippleColor = 592;

    @AttrRes
    public static final int riv_border_color = 593;

    @AttrRes
    public static final int riv_border_width = 594;

    @AttrRes
    public static final int riv_corner_radius = 595;

    @AttrRes
    public static final int riv_corner_radius_bottom_left = 596;

    @AttrRes
    public static final int riv_corner_radius_bottom_right = 597;

    @AttrRes
    public static final int riv_corner_radius_top_left = 598;

    @AttrRes
    public static final int riv_corner_radius_top_right = 599;

    @AttrRes
    public static final int riv_mutate_background = 600;

    @AttrRes
    public static final int riv_oval = 601;

    @AttrRes
    public static final int riv_tile_mode = 602;

    @AttrRes
    public static final int riv_tile_mode_x = 603;

    @AttrRes
    public static final int riv_tile_mode_y = 604;

    @AttrRes
    public static final int rvp_flingFactor = 605;

    @AttrRes
    public static final int rvp_inertia = 606;

    @AttrRes
    public static final int rvp_millisecondsPerInch = 607;

    @AttrRes
    public static final int rvp_singlePageFling = 608;

    @AttrRes
    public static final int rvp_triggerOffset = 609;

    @AttrRes
    public static final int sb_background = 610;

    @AttrRes
    public static final int sb_border_width = 611;

    @AttrRes
    public static final int sb_button_color = 612;

    @AttrRes
    public static final int sb_checked = 613;

    @AttrRes
    public static final int sb_checked_color = 614;

    @AttrRes
    public static final int sb_checkline_color = 615;

    @AttrRes
    public static final int sb_checkline_width = 616;

    @AttrRes
    public static final int sb_effect_duration = 617;

    @AttrRes
    public static final int sb_enable_effect = 618;

    @AttrRes
    public static final int sb_shadow_color = 619;

    @AttrRes
    public static final int sb_shadow_effect = 620;

    @AttrRes
    public static final int sb_shadow_offset = 621;

    @AttrRes
    public static final int sb_shadow_radius = 622;

    @AttrRes
    public static final int sb_show_indicator = 623;

    @AttrRes
    public static final int sb_uncheck_color = 624;

    @AttrRes
    public static final int sb_uncheckcircle_color = 625;

    @AttrRes
    public static final int sb_uncheckcircle_radius = 626;

    @AttrRes
    public static final int sb_uncheckcircle_width = 627;

    @AttrRes
    public static final int scopeUris = 628;

    @AttrRes
    public static final int scrimAnimationDuration = 629;

    @AttrRes
    public static final int scrimBackground = 630;

    @AttrRes
    public static final int scrimVisibleHeightTrigger = 631;

    @AttrRes
    public static final int searchHintIcon = 632;

    @AttrRes
    public static final int searchIcon = 633;

    @AttrRes
    public static final int searchViewStyle = 634;

    @AttrRes
    public static final int seekBarStyle = 635;

    @AttrRes
    public static final int selectableItemBackground = 636;

    @AttrRes
    public static final int selectableItemBackgroundBorderless = 637;

    @AttrRes
    public static final int shadowColor = 638;

    @AttrRes
    public static final int shadowDx = 639;

    @AttrRes
    public static final int shadowDy = 640;

    @AttrRes
    public static final int shadowMargin = 641;

    @AttrRes
    public static final int shadowMarginBottom = 642;

    @AttrRes
    public static final int shadowMarginLeft = 643;

    @AttrRes
    public static final int shadowMarginRight = 644;

    @AttrRes
    public static final int shadowMarginTop = 645;

    @AttrRes
    public static final int shadowRadius = 646;

    @AttrRes
    public static final int shimmer_angle = 647;

    @AttrRes
    public static final int shimmer_auto_start = 648;

    @AttrRes
    public static final int shimmer_base_alpha = 649;

    @AttrRes
    public static final int shimmer_base_color = 650;

    @AttrRes
    public static final int shimmer_clip_to_children = 651;

    @AttrRes
    public static final int shimmer_colored = 652;

    @AttrRes
    public static final int shimmer_demo_child_count = 653;

    @AttrRes
    public static final int shimmer_demo_grid_child_count = 654;

    @AttrRes
    public static final int shimmer_demo_layout = 655;

    @AttrRes
    public static final int shimmer_demo_layout_manager_type = 656;

    @AttrRes
    public static final int shimmer_direction = 657;

    @AttrRes
    public static final int shimmer_dropoff = 658;

    @AttrRes
    public static final int shimmer_duration = 659;

    @AttrRes
    public static final int shimmer_fixed_height = 660;

    @AttrRes
    public static final int shimmer_fixed_width = 661;

    @AttrRes
    public static final int shimmer_group = 662;

    @AttrRes
    public static final int shimmer_height_ratio = 663;

    @AttrRes
    public static final int shimmer_highlight_alpha = 664;

    @AttrRes
    public static final int shimmer_highlight_color = 665;

    @AttrRes
    public static final int shimmer_intensity = 666;

    @AttrRes
    public static final int shimmer_relative_height = 667;

    @AttrRes
    public static final int shimmer_relative_width = 668;

    @AttrRes
    public static final int shimmer_repeat_count = 669;

    @AttrRes
    public static final int shimmer_repeat_delay = 670;

    @AttrRes
    public static final int shimmer_repeat_mode = 671;

    @AttrRes
    public static final int shimmer_shape = 672;

    @AttrRes
    public static final int shimmer_tilt = 673;

    @AttrRes
    public static final int shimmer_width_ratio = 674;

    @AttrRes
    public static final int shouldShowPostalCode = 675;

    @AttrRes
    public static final int showAsAction = 676;

    @AttrRes
    public static final int showDividers = 677;

    @AttrRes
    public static final int showMotionSpec = 678;

    @AttrRes
    public static final int showText = 679;

    @AttrRes
    public static final int showTitle = 680;

    @AttrRes
    public static final int singleChoiceItemLayout = 681;

    @AttrRes
    public static final int singleLine = 682;

    @AttrRes
    public static final int singleSelection = 683;

    @AttrRes
    public static final int snackbarButtonStyle = 684;

    @AttrRes
    public static final int snackbarStyle = 685;

    @AttrRes
    public static final int spanCount = 686;

    @AttrRes
    public static final int sparkbutton_activeImage = 687;

    @AttrRes
    public static final int sparkbutton_activeImageTint = 688;

    @AttrRes
    public static final int sparkbutton_animationSpeed = 689;

    @AttrRes
    public static final int sparkbutton_iconSize = 690;

    @AttrRes
    public static final int sparkbutton_inActiveImage = 691;

    @AttrRes
    public static final int sparkbutton_inActiveImageTint = 692;

    @AttrRes
    public static final int sparkbutton_pressOnTouch = 693;

    @AttrRes
    public static final int sparkbutton_primaryColor = 694;

    @AttrRes
    public static final int sparkbutton_secondaryColor = 695;

    @AttrRes
    public static final int spinBars = 696;

    @AttrRes
    public static final int spinnerDropDownItemStyle = 697;

    @AttrRes
    public static final int spinnerStyle = 698;

    @AttrRes
    public static final int splitTrack = 699;

    @AttrRes
    public static final int srb_clearRatingEnabled = 700;

    @AttrRes
    public static final int srb_clickable = 701;

    @AttrRes
    public static final int srb_drawableEmpty = 702;

    @AttrRes
    public static final int srb_drawableFilled = 703;

    @AttrRes
    public static final int srb_isIndicator = 704;

    @AttrRes
    public static final int srb_minimumStars = 705;

    @AttrRes
    public static final int srb_numStars = 706;

    @AttrRes
    public static final int srb_rating = 707;

    @AttrRes
    public static final int srb_scrollable = 708;

    @AttrRes
    public static final int srb_starHeight = 709;

    @AttrRes
    public static final int srb_starPadding = 710;

    @AttrRes
    public static final int srb_starWidth = 711;

    @AttrRes
    public static final int srb_stepSize = 712;

    @AttrRes
    public static final int srcCompat = 713;

    @AttrRes
    public static final int stackFromEnd = 714;

    @AttrRes
    public static final int startExpanded = 715;

    @AttrRes
    public static final int state_above_anchor = 716;

    @AttrRes
    public static final int state_collapsed = 717;

    @AttrRes
    public static final int state_collapsible = 718;

    @AttrRes
    public static final int state_liftable = 719;

    @AttrRes
    public static final int state_lifted = 720;

    @AttrRes
    public static final int statusBarBackground = 721;

    @AttrRes
    public static final int statusBarScrim = 722;

    @AttrRes
    public static final int strokeColor = 723;

    @AttrRes
    public static final int strokeWidth = 724;

    @AttrRes
    public static final int subMenuArrow = 725;

    @AttrRes
    public static final int submitBackground = 726;

    @AttrRes
    public static final int subtitle = 727;

    @AttrRes
    public static final int subtitleTextAppearance = 728;

    @AttrRes
    public static final int subtitleTextColor = 729;

    @AttrRes
    public static final int subtitleTextStyle = 730;

    @AttrRes
    public static final int suggestionRowLayout = 731;

    @AttrRes
    public static final int switchMinWidth = 732;

    @AttrRes
    public static final int switchPadding = 733;

    @AttrRes
    public static final int switchStyle = 734;

    @AttrRes
    public static final int switchTextAppearance = 735;

    @AttrRes
    public static final int tabBackground = 736;

    @AttrRes
    public static final int tabContentStart = 737;

    @AttrRes
    public static final int tabGravity = 738;

    @AttrRes
    public static final int tabIconTint = 739;

    @AttrRes
    public static final int tabIconTintMode = 740;

    @AttrRes
    public static final int tabIndicator = 741;

    @AttrRes
    public static final int tabIndicatorAnimationDuration = 742;

    @AttrRes
    public static final int tabIndicatorColor = 743;

    @AttrRes
    public static final int tabIndicatorFullWidth = 744;

    @AttrRes
    public static final int tabIndicatorGravity = 745;

    @AttrRes
    public static final int tabIndicatorHeight = 746;

    @AttrRes
    public static final int tabInlineLabel = 747;

    @AttrRes
    public static final int tabMaxWidth = 748;

    @AttrRes
    public static final int tabMinWidth = 749;

    @AttrRes
    public static final int tabMode = 750;

    @AttrRes
    public static final int tabPadding = 751;

    @AttrRes
    public static final int tabPaddingBottom = 752;

    @AttrRes
    public static final int tabPaddingEnd = 753;

    @AttrRes
    public static final int tabPaddingStart = 754;

    @AttrRes
    public static final int tabPaddingTop = 755;

    @AttrRes
    public static final int tabRippleColor = 756;

    @AttrRes
    public static final int tabSelectedTextColor = 757;

    @AttrRes
    public static final int tabStyle = 758;

    @AttrRes
    public static final int tabTextAppearance = 759;

    @AttrRes
    public static final int tabTextColor = 760;

    @AttrRes
    public static final int tabUnboundedRipple = 761;

    @AttrRes
    public static final int textAllCaps = 762;

    @AttrRes
    public static final int textAppearanceBody1 = 763;

    @AttrRes
    public static final int textAppearanceBody2 = 764;

    @AttrRes
    public static final int textAppearanceButton = 765;

    @AttrRes
    public static final int textAppearanceCaption = 766;

    @AttrRes
    public static final int textAppearanceHeadline1 = 767;

    @AttrRes
    public static final int textAppearanceHeadline2 = 768;

    @AttrRes
    public static final int textAppearanceHeadline3 = 769;

    @AttrRes
    public static final int textAppearanceHeadline4 = 770;

    @AttrRes
    public static final int textAppearanceHeadline5 = 771;

    @AttrRes
    public static final int textAppearanceHeadline6 = 772;

    @AttrRes
    public static final int textAppearanceLargePopupMenu = 773;

    @AttrRes
    public static final int textAppearanceListItem = 774;

    @AttrRes
    public static final int textAppearanceListItemSecondary = 775;

    @AttrRes
    public static final int textAppearanceListItemSmall = 776;

    @AttrRes
    public static final int textAppearanceOverline = 777;

    @AttrRes
    public static final int textAppearancePopupMenuHeader = 778;

    @AttrRes
    public static final int textAppearanceSearchResultSubtitle = 779;

    @AttrRes
    public static final int textAppearanceSearchResultTitle = 780;

    @AttrRes
    public static final int textAppearanceSmallPopupMenu = 781;

    @AttrRes
    public static final int textAppearanceSubtitle1 = 782;

    @AttrRes
    public static final int textAppearanceSubtitle2 = 783;

    @AttrRes
    public static final int textColorAlertDialogListItem = 784;

    @AttrRes
    public static final int textColorError = 785;

    @AttrRes
    public static final int textColorSearchUrl = 786;

    @AttrRes
    public static final int textEndPadding = 787;

    @AttrRes
    public static final int textInputStyle = 788;

    @AttrRes
    public static final int textLocale = 789;

    @AttrRes
    public static final int textStartPadding = 790;

    @AttrRes
    public static final int theme = 791;

    @AttrRes
    public static final int thickness = 792;

    @AttrRes
    public static final int thumbTextPadding = 793;

    @AttrRes
    public static final int thumbTint = 794;

    @AttrRes
    public static final int thumbTintMode = 795;

    @AttrRes
    public static final int tickMark = 796;

    @AttrRes
    public static final int tickMarkTint = 797;

    @AttrRes
    public static final int tickMarkTintMode = 798;

    @AttrRes
    public static final int ticker_animateMeasurementChange = 799;

    @AttrRes
    public static final int ticker_animationDuration = 800;

    @AttrRes
    public static final int ticker_defaultCharacterList = 801;

    @AttrRes
    public static final int tint = 802;

    @AttrRes
    public static final int tintMode = 803;

    @AttrRes
    public static final int title = 804;

    @AttrRes
    public static final int titleEnabled = 805;

    @AttrRes
    public static final int titleMargin = 806;

    @AttrRes
    public static final int titleMarginBottom = 807;

    @AttrRes
    public static final int titleMarginEnd = 808;

    @AttrRes
    public static final int titleMarginStart = 809;

    @AttrRes
    public static final int titleMarginTop = 810;

    @AttrRes
    public static final int titleMargins = 811;

    @AttrRes
    public static final int titleTextAppearance = 812;

    @AttrRes
    public static final int titleTextColor = 813;

    @AttrRes
    public static final int titleTextStyle = 814;

    @AttrRes
    public static final int tl_bar_color = 815;

    @AttrRes
    public static final int tl_bar_stroke_color = 816;

    @AttrRes
    public static final int tl_bar_stroke_width = 817;

    @AttrRes
    public static final int tl_divider_color = 818;

    @AttrRes
    public static final int tl_divider_padding = 819;

    @AttrRes
    public static final int tl_divider_width = 820;

    @AttrRes
    public static final int tl_iconGravity = 821;

    @AttrRes
    public static final int tl_iconHeight = 822;

    @AttrRes
    public static final int tl_iconMargin = 823;

    @AttrRes
    public static final int tl_iconVisible = 824;

    @AttrRes
    public static final int tl_iconWidth = 825;

    @AttrRes
    public static final int tl_indicator_anim_duration = 826;

    @AttrRes
    public static final int tl_indicator_anim_enable = 827;

    @AttrRes
    public static final int tl_indicator_bounce_enable = 828;

    @AttrRes
    public static final int tl_indicator_color = 829;

    @AttrRes
    public static final int tl_indicator_corner_radius = 830;

    @AttrRes
    public static final int tl_indicator_gravity = 831;

    @AttrRes
    public static final int tl_indicator_height = 832;

    @AttrRes
    public static final int tl_indicator_margin_bottom = 833;

    @AttrRes
    public static final int tl_indicator_margin_left = 834;

    @AttrRes
    public static final int tl_indicator_margin_right = 835;

    @AttrRes
    public static final int tl_indicator_margin_top = 836;

    @AttrRes
    public static final int tl_indicator_style = 837;

    @AttrRes
    public static final int tl_indicator_width = 838;

    @AttrRes
    public static final int tl_indicator_width_equal_title = 839;

    @AttrRes
    public static final int tl_tab_padding = 840;

    @AttrRes
    public static final int tl_tab_space_equal = 841;

    @AttrRes
    public static final int tl_tab_width = 842;

    @AttrRes
    public static final int tl_textAllCaps = 843;

    @AttrRes
    public static final int tl_textBold = 844;

    @AttrRes
    public static final int tl_textSelectColor = 845;

    @AttrRes
    public static final int tl_textUnselectColor = 846;

    @AttrRes
    public static final int tl_textsize = 847;

    @AttrRes
    public static final int tl_underline_color = 848;

    @AttrRes
    public static final int tl_underline_gravity = 849;

    @AttrRes
    public static final int tl_underline_height = 850;

    @AttrRes
    public static final int toolbarId = 851;

    @AttrRes
    public static final int toolbarNavigationButtonStyle = 852;

    @AttrRes
    public static final int toolbarStyle = 853;

    @AttrRes
    public static final int tooltipForegroundColor = 854;

    @AttrRes
    public static final int tooltipFrameBackground = 855;

    @AttrRes
    public static final int tooltipText = 856;

    @AttrRes
    public static final int track = 857;

    @AttrRes
    public static final int trackTint = 858;

    @AttrRes
    public static final int trackTintMode = 859;

    @AttrRes
    public static final int ttcIndex = 860;

    @AttrRes
    public static final int uiCompass = 861;

    @AttrRes
    public static final int uiMapToolbar = 862;

    @AttrRes
    public static final int uiRotateGestures = 863;

    @AttrRes
    public static final int uiScrollGestures = 864;

    @AttrRes
    public static final int uiScrollGesturesDuringRotateOrZoom = 865;

    @AttrRes
    public static final int uiTiltGestures = 866;

    @AttrRes
    public static final int uiZoomControls = 867;

    @AttrRes
    public static final int uiZoomGestures = 868;

    @AttrRes
    public static final int useCompatPadding = 869;

    @AttrRes
    public static final int useViewLifecycle = 870;

    @AttrRes
    public static final int viewInflaterClass = 871;

    @AttrRes
    public static final int viewType = 872;

    @AttrRes
    public static final int voiceIcon = 873;

    @AttrRes
    public static final int windowActionBar = 874;

    @AttrRes
    public static final int windowActionBarOverlay = 875;

    @AttrRes
    public static final int windowActionModeOverlay = 876;

    @AttrRes
    public static final int windowFixedHeightMajor = 877;

    @AttrRes
    public static final int windowFixedHeightMinor = 878;

    @AttrRes
    public static final int windowFixedWidthMajor = 879;

    @AttrRes
    public static final int windowFixedWidthMinor = 880;

    @AttrRes
    public static final int windowMinWidthMajor = 881;

    @AttrRes
    public static final int windowMinWidthMinor = 882;

    @AttrRes
    public static final int windowNoTitle = 883;

    @AttrRes
    public static final int zOrderOnTop = 884;
  }

  public static final class bool {
    @BoolRes
    public static final int abc_action_bar_embed_tabs = 885;

    @BoolRes
    public static final int abc_action_bar_embed_tabs_pre_jb = 886;

    @BoolRes
    public static final int abc_action_bar_expanded_action_views_exclusive = 887;

    @BoolRes
    public static final int abc_allow_stacked_button_bar = 888;

    @BoolRes
    public static final int abc_config_actionMenuItemAllCaps = 889;

    @BoolRes
    public static final int abc_config_allowActionMenuItemTextWithIcon = 890;

    @BoolRes
    public static final int abc_config_closeDialogWhenTouchOutside = 891;

    @BoolRes
    public static final int abc_config_showMenuShortcutsWhenKeyboardPresent = 892;

    @BoolRes
    public static final int mtrl_btn_textappearance_all_caps = 893;
  }

  public static final class color {
    @ColorRes
    public static final int abc_background_cache_hint_selector_material_dark = 894;

    @ColorRes
    public static final int abc_background_cache_hint_selector_material_light = 895;

    @ColorRes
    public static final int abc_btn_colored_borderless_text_material = 896;

    @ColorRes
    public static final int abc_btn_colored_text_material = 897;

    @ColorRes
    public static final int abc_color_highlight_material = 898;

    @ColorRes
    public static final int abc_hint_foreground_material_dark = 899;

    @ColorRes
    public static final int abc_hint_foreground_material_light = 900;

    @ColorRes
    public static final int abc_input_method_navigation_guard = 901;

    @ColorRes
    public static final int abc_primary_text_disable_only_material_dark = 902;

    @ColorRes
    public static final int abc_primary_text_disable_only_material_light = 903;

    @ColorRes
    public static final int abc_primary_text_material_dark = 904;

    @ColorRes
    public static final int abc_primary_text_material_light = 905;

    @ColorRes
    public static final int abc_search_url_text = 906;

    @ColorRes
    public static final int abc_search_url_text_normal = 907;

    @ColorRes
    public static final int abc_search_url_text_pressed = 908;

    @ColorRes
    public static final int abc_search_url_text_selected = 909;

    @ColorRes
    public static final int abc_secondary_text_material_dark = 910;

    @ColorRes
    public static final int abc_secondary_text_material_light = 911;

    @ColorRes
    public static final int abc_tint_btn_checkable = 912;

    @ColorRes
    public static final int abc_tint_default = 913;

    @ColorRes
    public static final int abc_tint_edittext = 914;

    @ColorRes
    public static final int abc_tint_seek_thumb = 915;

    @ColorRes
    public static final int abc_tint_spinner = 916;

    @ColorRes
    public static final int abc_tint_switch_thumb = 917;

    @ColorRes
    public static final int abc_tint_switch_track = 918;

    @ColorRes
    public static final int accent_color_default = 919;

    @ColorRes
    public static final int accent_material_dark = 920;

    @ColorRes
    public static final int accent_material_light = 921;

    @ColorRes
    public static final int account_text = 922;

    @ColorRes
    public static final int background_floating_material_dark = 923;

    @ColorRes
    public static final int background_floating_material_light = 924;

    @ColorRes
    public static final int background_material_dark = 925;

    @ColorRes
    public static final int background_material_light = 926;

    @ColorRes
    public static final int blue_color = 927;

    @ColorRes
    public static final int bright_foreground_disabled_material_dark = 928;

    @ColorRes
    public static final int bright_foreground_disabled_material_light = 929;

    @ColorRes
    public static final int bright_foreground_inverse_material_dark = 930;

    @ColorRes
    public static final int bright_foreground_inverse_material_light = 931;

    @ColorRes
    public static final int bright_foreground_material_dark = 932;

    @ColorRes
    public static final int bright_foreground_material_light = 933;

    @ColorRes
    public static final int browser_actions_bg_grey = 934;

    @ColorRes
    public static final int browser_actions_divider_color = 935;

    @ColorRes
    public static final int browser_actions_text_color = 936;

    @ColorRes
    public static final int browser_actions_title_color = 937;

    @ColorRes
    public static final int button_material_dark = 938;

    @ColorRes
    public static final int button_material_light = 939;

    @ColorRes
    public static final int cardview_dark_background = 940;

    @ColorRes
    public static final int cardview_light_background = 941;

    @ColorRes
    public static final int cardview_shadow_end_color = 942;

    @ColorRes
    public static final int cardview_shadow_start_color = 943;

    @ColorRes
    public static final int colorAccent = 944;

    @ColorRes
    public static final int colorPrimary = 945;

    @ColorRes
    public static final int colorPrimaryDark = 946;

    @ColorRes
    public static final int color_text_secondary_default = 947;

    @ColorRes
    public static final int color_text_unselected_primary_default = 948;

    @ColorRes
    public static final int color_text_unselected_secondary_default = 949;

    @ColorRes
    public static final int com_facebook_blue = 950;

    @ColorRes
    public static final int com_facebook_button_background_color = 951;

    @ColorRes
    public static final int com_facebook_button_background_color_disabled = 952;

    @ColorRes
    public static final int com_facebook_button_background_color_pressed = 953;

    @ColorRes
    public static final int com_facebook_button_text_color = 954;

    @ColorRes
    public static final int com_facebook_device_auth_text = 955;

    @ColorRes
    public static final int com_facebook_likeboxcountview_border_color = 956;

    @ColorRes
    public static final int com_facebook_likeboxcountview_text_color = 957;

    @ColorRes
    public static final int com_facebook_likeview_text_color = 958;

    @ColorRes
    public static final int com_facebook_primary_button_disabled_text_color = 959;

    @ColorRes
    public static final int com_facebook_primary_button_pressed_text_color = 960;

    @ColorRes
    public static final int com_facebook_primary_button_text_color = 961;

    @ColorRes
    public static final int com_smart_login_code = 962;

    @ColorRes
    public static final int common_google_signin_btn_text_dark = 963;

    @ColorRes
    public static final int common_google_signin_btn_text_dark_default = 964;

    @ColorRes
    public static final int common_google_signin_btn_text_dark_disabled = 965;

    @ColorRes
    public static final int common_google_signin_btn_text_dark_focused = 966;

    @ColorRes
    public static final int common_google_signin_btn_text_dark_pressed = 967;

    @ColorRes
    public static final int common_google_signin_btn_text_light = 968;

    @ColorRes
    public static final int common_google_signin_btn_text_light_default = 969;

    @ColorRes
    public static final int common_google_signin_btn_text_light_disabled = 970;

    @ColorRes
    public static final int common_google_signin_btn_text_light_focused = 971;

    @ColorRes
    public static final int common_google_signin_btn_text_light_pressed = 972;

    @ColorRes
    public static final int common_google_signin_btn_tint = 973;

    @ColorRes
    public static final int control_normal_color_default = 974;

    @ColorRes
    public static final int cuisine_type_color = 975;

    @ColorRes
    public static final int cuisine_type_color_80 = 976;

    @ColorRes
    public static final int defaultBorderFlagColor = 977;

    @ColorRes
    public static final int defaultContentColor = 978;

    @ColorRes
    public static final int demo_dark_transparent = 979;

    @ColorRes
    public static final int demo_light_transparent = 980;

    @ColorRes
    public static final int design_bottom_navigation_shadow_color = 981;

    @ColorRes
    public static final int design_default_color_primary = 982;

    @ColorRes
    public static final int design_default_color_primary_dark = 983;

    @ColorRes
    public static final int design_error = 984;

    @ColorRes
    public static final int design_fab_shadow_end_color = 985;

    @ColorRes
    public static final int design_fab_shadow_mid_color = 986;

    @ColorRes
    public static final int design_fab_shadow_start_color = 987;

    @ColorRes
    public static final int design_fab_stroke_end_inner_color = 988;

    @ColorRes
    public static final int design_fab_stroke_end_outer_color = 989;

    @ColorRes
    public static final int design_fab_stroke_top_inner_color = 990;

    @ColorRes
    public static final int design_fab_stroke_top_outer_color = 991;

    @ColorRes
    public static final int design_snackbar_background_color = 992;

    @ColorRes
    public static final int design_tint_password_toggle = 993;

    @ColorRes
    public static final int dialog_header = 994;

    @ColorRes
    public static final int dim_foreground_disabled_material_dark = 995;

    @ColorRes
    public static final int dim_foreground_disabled_material_light = 996;

    @ColorRes
    public static final int dim_foreground_material_dark = 997;

    @ColorRes
    public static final int dim_foreground_material_light = 998;

    @ColorRes
    public static final int error_color_material = 999;

    @ColorRes
    public static final int error_color_material_dark = 1000;

    @ColorRes
    public static final int error_color_material_light = 1001;

    @ColorRes
    public static final int error_text_dark_theme = 1002;

    @ColorRes
    public static final int error_text_light_theme = 1003;

    @ColorRes
    public static final int explore_edit_bg = 1004;

    @ColorRes
    public static final int foreground_material_dark = 1005;

    @ColorRes
    public static final int foreground_material_light = 1006;

    @ColorRes
    public static final int green_color = 1007;

    @ColorRes
    public static final int grey_color_line = 1008;

    @ColorRes
    public static final int grey_color_semi_bold = 1009;

    @ColorRes
    public static final int heading_text_color_black = 1010;

    @ColorRes
    public static final int heart_colour = 1011;

    @ColorRes
    public static final int highlighted_text_material_dark = 1012;

    @ColorRes
    public static final int highlighted_text_material_light = 1013;

    @ColorRes
    public static final int hint_foreground_material_dark = 1014;

    @ColorRes
    public static final int hint_foreground_material_light = 1015;

    @ColorRes
    public static final int material_blue_grey_800 = 1016;

    @ColorRes
    public static final int material_blue_grey_900 = 1017;

    @ColorRes
    public static final int material_blue_grey_950 = 1018;

    @ColorRes
    public static final int material_deep_teal_200 = 1019;

    @ColorRes
    public static final int material_deep_teal_500 = 1020;

    @ColorRes
    public static final int material_grey_100 = 1021;

    @ColorRes
    public static final int material_grey_300 = 1022;

    @ColorRes
    public static final int material_grey_50 = 1023;

    @ColorRes
    public static final int material_grey_600 = 1024;

    @ColorRes
    public static final int material_grey_800 = 1025;

    @ColorRes
    public static final int material_grey_850 = 1026;

    @ColorRes
    public static final int material_grey_900 = 1027;

    @ColorRes
    public static final int mtrl_bottom_nav_colored_item_tint = 1028;

    @ColorRes
    public static final int mtrl_bottom_nav_item_tint = 1029;

    @ColorRes
    public static final int mtrl_btn_bg_color_disabled = 1030;

    @ColorRes
    public static final int mtrl_btn_bg_color_selector = 1031;

    @ColorRes
    public static final int mtrl_btn_ripple_color = 1032;

    @ColorRes
    public static final int mtrl_btn_stroke_color_selector = 1033;

    @ColorRes
    public static final int mtrl_btn_text_btn_ripple_color = 1034;

    @ColorRes
    public static final int mtrl_btn_text_color_disabled = 1035;

    @ColorRes
    public static final int mtrl_btn_text_color_selector = 1036;

    @ColorRes
    public static final int mtrl_btn_transparent_bg_color = 1037;

    @ColorRes
    public static final int mtrl_chip_background_color = 1038;

    @ColorRes
    public static final int mtrl_chip_close_icon_tint = 1039;

    @ColorRes
    public static final int mtrl_chip_ripple_color = 1040;

    @ColorRes
    public static final int mtrl_chip_text_color = 1041;

    @ColorRes
    public static final int mtrl_fab_ripple_color = 1042;

    @ColorRes
    public static final int mtrl_scrim_color = 1043;

    @ColorRes
    public static final int mtrl_tabs_colored_ripple_color = 1044;

    @ColorRes
    public static final int mtrl_tabs_icon_color_selector = 1045;

    @ColorRes
    public static final int mtrl_tabs_icon_color_selector_colored = 1046;

    @ColorRes
    public static final int mtrl_tabs_legacy_text_color_selector = 1047;

    @ColorRes
    public static final int mtrl_tabs_ripple_color = 1048;

    @ColorRes
    public static final int mtrl_text_btn_text_color_selector = 1049;

    @ColorRes
    public static final int mtrl_textinput_default_box_stroke_color = 1050;

    @ColorRes
    public static final int mtrl_textinput_disabled_color = 1051;

    @ColorRes
    public static final int mtrl_textinput_filled_box_default_background_color = 1052;

    @ColorRes
    public static final int mtrl_textinput_hovered_box_stroke_color = 1053;

    @ColorRes
    public static final int notification_action_color_filter = 1054;

    @ColorRes
    public static final int notification_icon_bg_color = 1055;

    @ColorRes
    public static final int notification_material_background_media_default_color = 1056;

    @ColorRes
    public static final int place_autocomplete_prediction_primary_text = 1057;

    @ColorRes
    public static final int place_autocomplete_prediction_primary_text_highlight = 1058;

    @ColorRes
    public static final int place_autocomplete_prediction_secondary_text = 1059;

    @ColorRes
    public static final int place_autocomplete_search_hint = 1060;

    @ColorRes
    public static final int place_autocomplete_search_text = 1061;

    @ColorRes
    public static final int place_autocomplete_separator = 1062;

    @ColorRes
    public static final int pressed_bg = 1063;

    @ColorRes
    public static final int primary_dark_material_dark = 1064;

    @ColorRes
    public static final int primary_dark_material_light = 1065;

    @ColorRes
    public static final int primary_material_dark = 1066;

    @ColorRes
    public static final int primary_material_light = 1067;

    @ColorRes
    public static final int primary_text_default_material_dark = 1068;

    @ColorRes
    public static final int primary_text_default_material_light = 1069;

    @ColorRes
    public static final int primary_text_disabled_material_dark = 1070;

    @ColorRes
    public static final int primary_text_disabled_material_light = 1071;

    @ColorRes
    public static final int rating_bg = 1072;

    @ColorRes
    public static final int restaurant_count_text_color = 1073;

    @ColorRes
    public static final int restaurant_count_text_color_20 = 1074;

    @ColorRes
    public static final int restaurant_rating_background = 1075;

    @ColorRes
    public static final int ripple_material_dark = 1076;

    @ColorRes
    public static final int ripple_material_light = 1077;

    @ColorRes
    public static final int secondary_text_default_material_dark = 1078;

    @ColorRes
    public static final int secondary_text_default_material_light = 1079;

    @ColorRes
    public static final int secondary_text_disabled_material_dark = 1080;

    @ColorRes
    public static final int secondary_text_disabled_material_light = 1081;

    @ColorRes
    public static final int shadow_view_default_shadow_color = 1082;

    @ColorRes
    public static final int shadow_view_foreground_color_dark = 1083;

    @ColorRes
    public static final int shadow_view_foreground_color_light = 1084;

    @ColorRes
    public static final int slate_50 = 1085;

    @ColorRes
    public static final int spark_image_tint = 1086;

    @ColorRes
    public static final int spark_primary_color = 1087;

    @ColorRes
    public static final int spark_secondary_color = 1088;

    @ColorRes
    public static final int splash_green = 1089;

    @ColorRes
    public static final int switch_thumb_disabled_material_dark = 1090;

    @ColorRes
    public static final int switch_thumb_disabled_material_light = 1091;

    @ColorRes
    public static final int switch_thumb_material_dark = 1092;

    @ColorRes
    public static final int switch_thumb_material_light = 1093;

    @ColorRes
    public static final int switch_thumb_normal_material_dark = 1094;

    @ColorRes
    public static final int switch_thumb_normal_material_light = 1095;

    @ColorRes
    public static final int test = 1096;

    @ColorRes
    public static final int text_color_dark_blue = 1097;

    @ColorRes
    public static final int text_color_grey = 1098;

    @ColorRes
    public static final int text_color_grey_80 = 1099;

    @ColorRes
    public static final int text_light = 1100;

    @ColorRes
    public static final int text_light_grey = 1101;

    @ColorRes
    public static final int toolbar_color_default = 1102;

    @ColorRes
    public static final int tooltip_background_dark = 1103;

    @ColorRes
    public static final int tooltip_background_light = 1104;

    @ColorRes
    public static final int white = 1105;
  }

  public static final class dimen {
    @DimenRes
    public static final int abc_action_bar_content_inset_material = 1106;

    @DimenRes
    public static final int abc_action_bar_content_inset_with_nav = 1107;

    @DimenRes
    public static final int abc_action_bar_default_height_material = 1108;

    @DimenRes
    public static final int abc_action_bar_default_padding_end_material = 1109;

    @DimenRes
    public static final int abc_action_bar_default_padding_start_material = 1110;

    @DimenRes
    public static final int abc_action_bar_elevation_material = 1111;

    @DimenRes
    public static final int abc_action_bar_icon_vertical_padding_material = 1112;

    @DimenRes
    public static final int abc_action_bar_overflow_padding_end_material = 1113;

    @DimenRes
    public static final int abc_action_bar_overflow_padding_start_material = 1114;

    @DimenRes
    public static final int abc_action_bar_progress_bar_size = 1115;

    @DimenRes
    public static final int abc_action_bar_stacked_max_height = 1116;

    @DimenRes
    public static final int abc_action_bar_stacked_tab_max_width = 1117;

    @DimenRes
    public static final int abc_action_bar_subtitle_bottom_margin_material = 1118;

    @DimenRes
    public static final int abc_action_bar_subtitle_top_margin_material = 1119;

    @DimenRes
    public static final int abc_action_button_min_height_material = 1120;

    @DimenRes
    public static final int abc_action_button_min_width_material = 1121;

    @DimenRes
    public static final int abc_action_button_min_width_overflow_material = 1122;

    @DimenRes
    public static final int abc_alert_dialog_button_bar_height = 1123;

    @DimenRes
    public static final int abc_alert_dialog_button_dimen = 1124;

    @DimenRes
    public static final int abc_button_inset_horizontal_material = 1125;

    @DimenRes
    public static final int abc_button_inset_vertical_material = 1126;

    @DimenRes
    public static final int abc_button_padding_horizontal_material = 1127;

    @DimenRes
    public static final int abc_button_padding_vertical_material = 1128;

    @DimenRes
    public static final int abc_cascading_menus_min_smallest_width = 1129;

    @DimenRes
    public static final int abc_config_prefDialogWidth = 1130;

    @DimenRes
    public static final int abc_control_corner_material = 1131;

    @DimenRes
    public static final int abc_control_inset_material = 1132;

    @DimenRes
    public static final int abc_control_padding_material = 1133;

    @DimenRes
    public static final int abc_dialog_corner_radius_material = 1134;

    @DimenRes
    public static final int abc_dialog_fixed_height_major = 1135;

    @DimenRes
    public static final int abc_dialog_fixed_height_minor = 1136;

    @DimenRes
    public static final int abc_dialog_fixed_width_major = 1137;

    @DimenRes
    public static final int abc_dialog_fixed_width_minor = 1138;

    @DimenRes
    public static final int abc_dialog_list_padding_bottom_no_buttons = 1139;

    @DimenRes
    public static final int abc_dialog_list_padding_top_no_title = 1140;

    @DimenRes
    public static final int abc_dialog_list_padding_vertical_material = 1141;

    @DimenRes
    public static final int abc_dialog_min_width_major = 1142;

    @DimenRes
    public static final int abc_dialog_min_width_minor = 1143;

    @DimenRes
    public static final int abc_dialog_padding_material = 1144;

    @DimenRes
    public static final int abc_dialog_padding_top_material = 1145;

    @DimenRes
    public static final int abc_dialog_title_divider_material = 1146;

    @DimenRes
    public static final int abc_disabled_alpha_material_dark = 1147;

    @DimenRes
    public static final int abc_disabled_alpha_material_light = 1148;

    @DimenRes
    public static final int abc_dropdownitem_icon_width = 1149;

    @DimenRes
    public static final int abc_dropdownitem_text_padding_left = 1150;

    @DimenRes
    public static final int abc_dropdownitem_text_padding_right = 1151;

    @DimenRes
    public static final int abc_edit_text_inset_bottom_material = 1152;

    @DimenRes
    public static final int abc_edit_text_inset_horizontal_material = 1153;

    @DimenRes
    public static final int abc_edit_text_inset_top_material = 1154;

    @DimenRes
    public static final int abc_floating_window_z = 1155;

    @DimenRes
    public static final int abc_list_item_height_large_material = 1156;

    @DimenRes
    public static final int abc_list_item_height_material = 1157;

    @DimenRes
    public static final int abc_list_item_height_small_material = 1158;

    @DimenRes
    public static final int abc_list_item_padding_horizontal_material = 1159;

    @DimenRes
    public static final int abc_panel_menu_list_width = 1160;

    @DimenRes
    public static final int abc_progress_bar_height_material = 1161;

    @DimenRes
    public static final int abc_search_view_preferred_height = 1162;

    @DimenRes
    public static final int abc_search_view_preferred_width = 1163;

    @DimenRes
    public static final int abc_search_view_text_min_width = 1164;

    @DimenRes
    public static final int abc_seekbar_track_background_height_material = 1165;

    @DimenRes
    public static final int abc_seekbar_track_progress_height_material = 1166;

    @DimenRes
    public static final int abc_select_dialog_padding_start_material = 1167;

    @DimenRes
    public static final int abc_switch_padding = 1168;

    @DimenRes
    public static final int abc_text_size_body_1_material = 1169;

    @DimenRes
    public static final int abc_text_size_body_2_material = 1170;

    @DimenRes
    public static final int abc_text_size_button_material = 1171;

    @DimenRes
    public static final int abc_text_size_caption_material = 1172;

    @DimenRes
    public static final int abc_text_size_display_1_material = 1173;

    @DimenRes
    public static final int abc_text_size_display_2_material = 1174;

    @DimenRes
    public static final int abc_text_size_display_3_material = 1175;

    @DimenRes
    public static final int abc_text_size_display_4_material = 1176;

    @DimenRes
    public static final int abc_text_size_headline_material = 1177;

    @DimenRes
    public static final int abc_text_size_large_material = 1178;

    @DimenRes
    public static final int abc_text_size_medium_material = 1179;

    @DimenRes
    public static final int abc_text_size_menu_header_material = 1180;

    @DimenRes
    public static final int abc_text_size_menu_material = 1181;

    @DimenRes
    public static final int abc_text_size_small_material = 1182;

    @DimenRes
    public static final int abc_text_size_subhead_material = 1183;

    @DimenRes
    public static final int abc_text_size_subtitle_material_toolbar = 1184;

    @DimenRes
    public static final int abc_text_size_title_material = 1185;

    @DimenRes
    public static final int abc_text_size_title_material_toolbar = 1186;

    @DimenRes
    public static final int activity_total_margin = 1187;

    @DimenRes
    public static final int add_address_vertical_margin = 1188;

    @DimenRes
    public static final int add_card_element_vertical_margin = 1189;

    @DimenRes
    public static final int add_card_expiry_middle_margin = 1190;

    @DimenRes
    public static final int add_card_total_margin = 1191;

    @DimenRes
    public static final int add_payment_method_vertical_padding = 1192;

    @DimenRes
    public static final int android_pay_button_layout_margin = 1193;

    @DimenRes
    public static final int android_pay_button_separation = 1194;

    @DimenRes
    public static final int android_pay_confirmation_margin = 1195;

    @DimenRes
    public static final int browser_actions_context_menu_max_width = 1196;

    @DimenRes
    public static final int browser_actions_context_menu_min_padding = 1197;

    @DimenRes
    public static final int card_cvc_initial_margin = 1198;

    @DimenRes
    public static final int card_expiry_initial_margin = 1199;

    @DimenRes
    public static final int card_icon_multiline_padding = 1200;

    @DimenRes
    public static final int card_icon_multiline_padding_bottom = 1201;

    @DimenRes
    public static final int card_icon_multiline_width = 1202;

    @DimenRes
    public static final int card_icon_padding = 1203;

    @DimenRes
    public static final int card_widget_min_width = 1204;

    @DimenRes
    public static final int cardview_compat_inset_shadow = 1205;

    @DimenRes
    public static final int cardview_default_elevation = 1206;

    @DimenRes
    public static final int cardview_default_radius = 1207;

    @DimenRes
    public static final int com_facebook_auth_dialog_corner_radius = 1208;

    @DimenRes
    public static final int com_facebook_auth_dialog_corner_radius_oversized = 1209;

    @DimenRes
    public static final int com_facebook_button_corner_radius = 1210;

    @DimenRes
    public static final int com_facebook_button_login_corner_radius = 1211;

    @DimenRes
    public static final int com_facebook_likeboxcountview_border_radius = 1212;

    @DimenRes
    public static final int com_facebook_likeboxcountview_border_width = 1213;

    @DimenRes
    public static final int com_facebook_likeboxcountview_caret_height = 1214;

    @DimenRes
    public static final int com_facebook_likeboxcountview_caret_width = 1215;

    @DimenRes
    public static final int com_facebook_likeboxcountview_text_padding = 1216;

    @DimenRes
    public static final int com_facebook_likeboxcountview_text_size = 1217;

    @DimenRes
    public static final int com_facebook_likeview_edge_padding = 1218;

    @DimenRes
    public static final int com_facebook_likeview_internal_padding = 1219;

    @DimenRes
    public static final int com_facebook_likeview_text_size = 1220;

    @DimenRes
    public static final int com_facebook_profilepictureview_preset_size_large = 1221;

    @DimenRes
    public static final int com_facebook_profilepictureview_preset_size_normal = 1222;

    @DimenRes
    public static final int com_facebook_profilepictureview_preset_size_small = 1223;

    @DimenRes
    public static final int compat_button_inset_horizontal_material = 1224;

    @DimenRes
    public static final int compat_button_inset_vertical_material = 1225;

    @DimenRes
    public static final int compat_button_padding_horizontal_material = 1226;

    @DimenRes
    public static final int compat_button_padding_vertical_material = 1227;

    @DimenRes
    public static final int compat_control_corner_material = 1228;

    @DimenRes
    public static final int compat_notification_large_icon_max_height = 1229;

    @DimenRes
    public static final int compat_notification_large_icon_max_width = 1230;

    @DimenRes
    public static final int connection_error_button = 1231;

    @DimenRes
    public static final int connection_error_descri = 1232;

    @DimenRes
    public static final int connection_error_heading = 1233;

    @DimenRes
    public static final int design_appbar_elevation = 1234;

    @DimenRes
    public static final int design_bottom_navigation_active_item_max_width = 1235;

    @DimenRes
    public static final int design_bottom_navigation_active_item_min_width = 1236;

    @DimenRes
    public static final int design_bottom_navigation_active_text_size = 1237;

    @DimenRes
    public static final int design_bottom_navigation_elevation = 1238;

    @DimenRes
    public static final int design_bottom_navigation_height = 1239;

    @DimenRes
    public static final int design_bottom_navigation_icon_size = 1240;

    @DimenRes
    public static final int design_bottom_navigation_item_max_width = 1241;

    @DimenRes
    public static final int design_bottom_navigation_item_min_width = 1242;

    @DimenRes
    public static final int design_bottom_navigation_margin = 1243;

    @DimenRes
    public static final int design_bottom_navigation_shadow_height = 1244;

    @DimenRes
    public static final int design_bottom_navigation_text_size = 1245;

    @DimenRes
    public static final int design_bottom_sheet_modal_elevation = 1246;

    @DimenRes
    public static final int design_bottom_sheet_peek_height_min = 1247;

    @DimenRes
    public static final int design_fab_border_width = 1248;

    @DimenRes
    public static final int design_fab_elevation = 1249;

    @DimenRes
    public static final int design_fab_image_size = 1250;

    @DimenRes
    public static final int design_fab_size_mini = 1251;

    @DimenRes
    public static final int design_fab_size_normal = 1252;

    @DimenRes
    public static final int design_fab_translation_z_hovered_focused = 1253;

    @DimenRes
    public static final int design_fab_translation_z_pressed = 1254;

    @DimenRes
    public static final int design_navigation_elevation = 1255;

    @DimenRes
    public static final int design_navigation_icon_padding = 1256;

    @DimenRes
    public static final int design_navigation_icon_size = 1257;

    @DimenRes
    public static final int design_navigation_item_horizontal_padding = 1258;

    @DimenRes
    public static final int design_navigation_item_icon_padding = 1259;

    @DimenRes
    public static final int design_navigation_max_width = 1260;

    @DimenRes
    public static final int design_navigation_padding_bottom = 1261;

    @DimenRes
    public static final int design_navigation_separator_vertical_padding = 1262;

    @DimenRes
    public static final int design_snackbar_action_inline_max_width = 1263;

    @DimenRes
    public static final int design_snackbar_background_corner_radius = 1264;

    @DimenRes
    public static final int design_snackbar_elevation = 1265;

    @DimenRes
    public static final int design_snackbar_extra_spacing_horizontal = 1266;

    @DimenRes
    public static final int design_snackbar_max_width = 1267;

    @DimenRes
    public static final int design_snackbar_min_width = 1268;

    @DimenRes
    public static final int design_snackbar_padding_horizontal = 1269;

    @DimenRes
    public static final int design_snackbar_padding_vertical = 1270;

    @DimenRes
    public static final int design_snackbar_padding_vertical_2lines = 1271;

    @DimenRes
    public static final int design_snackbar_text_size = 1272;

    @DimenRes
    public static final int design_tab_max_width = 1273;

    @DimenRes
    public static final int design_tab_scrollable_min_width = 1274;

    @DimenRes
    public static final int design_tab_text_size = 1275;

    @DimenRes
    public static final int design_tab_text_size_2line = 1276;

    @DimenRes
    public static final int design_textinput_caption_translate_y = 1277;

    @DimenRes
    public static final int disabled_alpha_material_dark = 1278;

    @DimenRes
    public static final int disabled_alpha_material_light = 1279;

    @DimenRes
    public static final int fastscroll__bubble_corner = 1280;

    @DimenRes
    public static final int fastscroll__bubble_size = 1281;

    @DimenRes
    public static final int fastscroll__handle_clickable_width = 1282;

    @DimenRes
    public static final int fastscroll__handle_corner = 1283;

    @DimenRes
    public static final int fastscroll__handle_height = 1284;

    @DimenRes
    public static final int fastscroll__handle_inset = 1285;

    @DimenRes
    public static final int fastscroll__handle_padding = 1286;

    @DimenRes
    public static final int fastscroll__handle_width = 1287;

    @DimenRes
    public static final int fastscroll_default_thickness = 1288;

    @DimenRes
    public static final int fastscroll_margin = 1289;

    @DimenRes
    public static final int fastscroll_minimum_range = 1290;

    @DimenRes
    public static final int google_1x = 1291;

    @DimenRes
    public static final int highlight_alpha_material_colored = 1292;

    @DimenRes
    public static final int highlight_alpha_material_dark = 1293;

    @DimenRes
    public static final int highlight_alpha_material_light = 1294;

    @DimenRes
    public static final int hint_alpha_material_dark = 1295;

    @DimenRes
    public static final int hint_alpha_material_light = 1296;

    @DimenRes
    public static final int hint_pressed_alpha_material_dark = 1297;

    @DimenRes
    public static final int hint_pressed_alpha_material_light = 1298;

    @DimenRes
    public static final int item_touch_helper_max_drag_scroll_per_frame = 1299;

    @DimenRes
    public static final int item_touch_helper_swipe_escape_max_velocity = 1300;

    @DimenRes
    public static final int item_touch_helper_swipe_escape_velocity = 1301;

    @DimenRes
    public static final int masked_card_icon_width = 1302;

    @DimenRes
    public static final int masked_card_list_top_margin = 1303;

    @DimenRes
    public static final int masked_card_row_end_padding = 1304;

    @DimenRes
    public static final int masked_card_row_height = 1305;

    @DimenRes
    public static final int masked_card_row_start_padding = 1306;

    @DimenRes
    public static final int masked_card_vertical_padding = 1307;

    @DimenRes
    public static final int mtrl_bottomappbar_fabOffsetEndMode = 1308;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_cradle_margin = 1309;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_cradle_rounded_corner_radius = 1310;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_cradle_vertical_offset = 1311;

    @DimenRes
    public static final int mtrl_bottomappbar_height = 1312;

    @DimenRes
    public static final int mtrl_btn_corner_radius = 1313;

    @DimenRes
    public static final int mtrl_btn_dialog_btn_min_width = 1314;

    @DimenRes
    public static final int mtrl_btn_disabled_elevation = 1315;

    @DimenRes
    public static final int mtrl_btn_disabled_z = 1316;

    @DimenRes
    public static final int mtrl_btn_elevation = 1317;

    @DimenRes
    public static final int mtrl_btn_focused_z = 1318;

    @DimenRes
    public static final int mtrl_btn_hovered_z = 1319;

    @DimenRes
    public static final int mtrl_btn_icon_btn_padding_left = 1320;

    @DimenRes
    public static final int mtrl_btn_icon_padding = 1321;

    @DimenRes
    public static final int mtrl_btn_inset = 1322;

    @DimenRes
    public static final int mtrl_btn_letter_spacing = 1323;

    @DimenRes
    public static final int mtrl_btn_padding_bottom = 1324;

    @DimenRes
    public static final int mtrl_btn_padding_left = 1325;

    @DimenRes
    public static final int mtrl_btn_padding_right = 1326;

    @DimenRes
    public static final int mtrl_btn_padding_top = 1327;

    @DimenRes
    public static final int mtrl_btn_pressed_z = 1328;

    @DimenRes
    public static final int mtrl_btn_stroke_size = 1329;

    @DimenRes
    public static final int mtrl_btn_text_btn_icon_padding = 1330;

    @DimenRes
    public static final int mtrl_btn_text_btn_padding_left = 1331;

    @DimenRes
    public static final int mtrl_btn_text_btn_padding_right = 1332;

    @DimenRes
    public static final int mtrl_btn_text_size = 1333;

    @DimenRes
    public static final int mtrl_btn_z = 1334;

    @DimenRes
    public static final int mtrl_card_elevation = 1335;

    @DimenRes
    public static final int mtrl_card_spacing = 1336;

    @DimenRes
    public static final int mtrl_chip_pressed_translation_z = 1337;

    @DimenRes
    public static final int mtrl_chip_text_size = 1338;

    @DimenRes
    public static final int mtrl_fab_elevation = 1339;

    @DimenRes
    public static final int mtrl_fab_translation_z_hovered_focused = 1340;

    @DimenRes
    public static final int mtrl_fab_translation_z_pressed = 1341;

    @DimenRes
    public static final int mtrl_navigation_elevation = 1342;

    @DimenRes
    public static final int mtrl_navigation_item_horizontal_padding = 1343;

    @DimenRes
    public static final int mtrl_navigation_item_icon_padding = 1344;

    @DimenRes
    public static final int mtrl_snackbar_background_corner_radius = 1345;

    @DimenRes
    public static final int mtrl_snackbar_margin = 1346;

    @DimenRes
    public static final int mtrl_textinput_box_bottom_offset = 1347;

    @DimenRes
    public static final int mtrl_textinput_box_corner_radius_medium = 1348;

    @DimenRes
    public static final int mtrl_textinput_box_corner_radius_small = 1349;

    @DimenRes
    public static final int mtrl_textinput_box_label_cutout_padding = 1350;

    @DimenRes
    public static final int mtrl_textinput_box_padding_end = 1351;

    @DimenRes
    public static final int mtrl_textinput_box_stroke_width_default = 1352;

    @DimenRes
    public static final int mtrl_textinput_box_stroke_width_focused = 1353;

    @DimenRes
    public static final int mtrl_textinput_outline_box_expanded_padding = 1354;

    @DimenRes
    public static final int mtrl_toolbar_default_height = 1355;

    @DimenRes
    public static final int notification_action_icon_size = 1356;

    @DimenRes
    public static final int notification_action_text_size = 1357;

    @DimenRes
    public static final int notification_big_circle_margin = 1358;

    @DimenRes
    public static final int notification_content_margin_start = 1359;

    @DimenRes
    public static final int notification_large_icon_height = 1360;

    @DimenRes
    public static final int notification_large_icon_width = 1361;

    @DimenRes
    public static final int notification_main_column_padding_top = 1362;

    @DimenRes
    public static final int notification_media_narrow_margin = 1363;

    @DimenRes
    public static final int notification_right_icon_size = 1364;

    @DimenRes
    public static final int notification_right_side_padding_top = 1365;

    @DimenRes
    public static final int notification_small_icon_background_padding = 1366;

    @DimenRes
    public static final int notification_small_icon_size_as_large = 1367;

    @DimenRes
    public static final int notification_subtext_size = 1368;

    @DimenRes
    public static final int notification_text_size = 1369;

    @DimenRes
    public static final int notification_top_pad = 1370;

    @DimenRes
    public static final int notification_top_pad_large_text = 1371;

    @DimenRes
    public static final int otp_view_cursor_width = 1372;

    @DimenRes
    public static final int otp_view_item_line_width = 1373;

    @DimenRes
    public static final int otp_view_item_radius = 1374;

    @DimenRes
    public static final int otp_view_item_size = 1375;

    @DimenRes
    public static final int otp_view_item_spacing = 1376;

    @DimenRes
    public static final int place_autocomplete_button_padding = 1377;

    @DimenRes
    public static final int place_autocomplete_powered_by_google_height = 1378;

    @DimenRes
    public static final int place_autocomplete_powered_by_google_start = 1379;

    @DimenRes
    public static final int place_autocomplete_prediction_height = 1380;

    @DimenRes
    public static final int place_autocomplete_prediction_horizontal_margin = 1381;

    @DimenRes
    public static final int place_autocomplete_prediction_primary_text = 1382;

    @DimenRes
    public static final int place_autocomplete_prediction_secondary_text = 1383;

    @DimenRes
    public static final int place_autocomplete_progress_horizontal_margin = 1384;

    @DimenRes
    public static final int place_autocomplete_progress_size = 1385;

    @DimenRes
    public static final int place_autocomplete_separator_start = 1386;

    @DimenRes
    public static final int shipping_check_icon_width = 1387;

    @DimenRes
    public static final int shipping_widget_horizontal_margin = 1388;

    @DimenRes
    public static final int shipping_widget_outer_margin = 1389;

    @DimenRes
    public static final int shipping_widget_vertical_margin = 1390;

    @DimenRes
    public static final int subtitle_corner_radius = 1391;

    @DimenRes
    public static final int subtitle_outline_width = 1392;

    @DimenRes
    public static final int subtitle_shadow_offset = 1393;

    @DimenRes
    public static final int subtitle_shadow_radius = 1394;

    @DimenRes
    public static final int toolbar_elevation = 1395;

    @DimenRes
    public static final int tooltip_corner_radius = 1396;

    @DimenRes
    public static final int tooltip_horizontal_padding = 1397;

    @DimenRes
    public static final int tooltip_margin = 1398;

    @DimenRes
    public static final int tooltip_precise_anchor_extra_offset = 1399;

    @DimenRes
    public static final int tooltip_precise_anchor_threshold = 1400;

    @DimenRes
    public static final int tooltip_vertical_padding = 1401;

    @DimenRes
    public static final int tooltip_y_offset_non_touch = 1402;

    @DimenRes
    public static final int tooltip_y_offset_touch = 1403;
  }

  public static final class drawable {
    @DrawableRes
    public static final int abc_ab_share_pack_mtrl_alpha = 1404;

    @DrawableRes
    public static final int abc_action_bar_item_background_material = 1405;

    @DrawableRes
    public static final int abc_btn_borderless_material = 1406;

    @DrawableRes
    public static final int abc_btn_check_material = 1407;

    @DrawableRes
    public static final int abc_btn_check_material_anim = 1408;

    @DrawableRes
    public static final int abc_btn_check_to_on_mtrl_000 = 1409;

    @DrawableRes
    public static final int abc_btn_check_to_on_mtrl_015 = 1410;

    @DrawableRes
    public static final int abc_btn_colored_material = 1411;

    @DrawableRes
    public static final int abc_btn_default_mtrl_shape = 1412;

    @DrawableRes
    public static final int abc_btn_radio_material = 1413;

    @DrawableRes
    public static final int abc_btn_radio_material_anim = 1414;

    @DrawableRes
    public static final int abc_btn_radio_to_on_mtrl_000 = 1415;

    @DrawableRes
    public static final int abc_btn_radio_to_on_mtrl_015 = 1416;

    @DrawableRes
    public static final int abc_btn_rating_star_off_mtrl_alpha = 1417;

    @DrawableRes
    public static final int abc_btn_rating_star_on_mtrl_alpha = 1418;

    @DrawableRes
    public static final int abc_btn_switch_to_on_mtrl_00001 = 1419;

    @DrawableRes
    public static final int abc_btn_switch_to_on_mtrl_00012 = 1420;

    @DrawableRes
    public static final int abc_cab_background_internal_bg = 1421;

    @DrawableRes
    public static final int abc_cab_background_top_material = 1422;

    @DrawableRes
    public static final int abc_cab_background_top_mtrl_alpha = 1423;

    @DrawableRes
    public static final int abc_control_background_material = 1424;

    @DrawableRes
    public static final int abc_dialog_material_background = 1425;

    @DrawableRes
    public static final int abc_dialog_material_background_dark = 1426;

    @DrawableRes
    public static final int abc_dialog_material_background_light = 1427;

    @DrawableRes
    public static final int abc_edit_text_material = 1428;

    @DrawableRes
    public static final int abc_ic_ab_back_material = 1429;

    @DrawableRes
    public static final int abc_ic_ab_back_mtrl_am_alpha = 1430;

    @DrawableRes
    public static final int abc_ic_arrow_drop_right_black_24dp = 1431;

    @DrawableRes
    public static final int abc_ic_clear_material = 1432;

    @DrawableRes
    public static final int abc_ic_clear_mtrl_alpha = 1433;

    @DrawableRes
    public static final int abc_ic_commit_search_api_mtrl_alpha = 1434;

    @DrawableRes
    public static final int abc_ic_go_search_api_material = 1435;

    @DrawableRes
    public static final int abc_ic_go_search_api_mtrl_alpha = 1436;

    @DrawableRes
    public static final int abc_ic_menu_copy_mtrl_am_alpha = 1437;

    @DrawableRes
    public static final int abc_ic_menu_cut_mtrl_alpha = 1438;

    @DrawableRes
    public static final int abc_ic_menu_moreoverflow_mtrl_alpha = 1439;

    @DrawableRes
    public static final int abc_ic_menu_overflow_material = 1440;

    @DrawableRes
    public static final int abc_ic_menu_paste_mtrl_am_alpha = 1441;

    @DrawableRes
    public static final int abc_ic_menu_selectall_mtrl_alpha = 1442;

    @DrawableRes
    public static final int abc_ic_menu_share_mtrl_alpha = 1443;

    @DrawableRes
    public static final int abc_ic_search_api_material = 1444;

    @DrawableRes
    public static final int abc_ic_search_api_mtrl_alpha = 1445;

    @DrawableRes
    public static final int abc_ic_star_black_16dp = 1446;

    @DrawableRes
    public static final int abc_ic_star_black_36dp = 1447;

    @DrawableRes
    public static final int abc_ic_star_black_48dp = 1448;

    @DrawableRes
    public static final int abc_ic_star_half_black_16dp = 1449;

    @DrawableRes
    public static final int abc_ic_star_half_black_36dp = 1450;

    @DrawableRes
    public static final int abc_ic_star_half_black_48dp = 1451;

    @DrawableRes
    public static final int abc_ic_voice_search_api_material = 1452;

    @DrawableRes
    public static final int abc_ic_voice_search_api_mtrl_alpha = 1453;

    @DrawableRes
    public static final int abc_item_background_holo_dark = 1454;

    @DrawableRes
    public static final int abc_item_background_holo_light = 1455;

    @DrawableRes
    public static final int abc_list_divider_material = 1456;

    @DrawableRes
    public static final int abc_list_divider_mtrl_alpha = 1457;

    @DrawableRes
    public static final int abc_list_focused_holo = 1458;

    @DrawableRes
    public static final int abc_list_longpressed_holo = 1459;

    @DrawableRes
    public static final int abc_list_pressed_holo_dark = 1460;

    @DrawableRes
    public static final int abc_list_pressed_holo_light = 1461;

    @DrawableRes
    public static final int abc_list_selector_background_transition_holo_dark = 1462;

    @DrawableRes
    public static final int abc_list_selector_background_transition_holo_light = 1463;

    @DrawableRes
    public static final int abc_list_selector_disabled_holo_dark = 1464;

    @DrawableRes
    public static final int abc_list_selector_disabled_holo_light = 1465;

    @DrawableRes
    public static final int abc_list_selector_holo_dark = 1466;

    @DrawableRes
    public static final int abc_list_selector_holo_light = 1467;

    @DrawableRes
    public static final int abc_menu_hardkey_panel_mtrl_mult = 1468;

    @DrawableRes
    public static final int abc_popup_background_mtrl_mult = 1469;

    @DrawableRes
    public static final int abc_ratingbar_full_material = 1470;

    @DrawableRes
    public static final int abc_ratingbar_indicator_material = 1471;

    @DrawableRes
    public static final int abc_ratingbar_material = 1472;

    @DrawableRes
    public static final int abc_ratingbar_small_material = 1473;

    @DrawableRes
    public static final int abc_scrubber_control_off_mtrl_alpha = 1474;

    @DrawableRes
    public static final int abc_scrubber_control_to_pressed_mtrl_000 = 1475;

    @DrawableRes
    public static final int abc_scrubber_control_to_pressed_mtrl_005 = 1476;

    @DrawableRes
    public static final int abc_scrubber_primary_mtrl_alpha = 1477;

    @DrawableRes
    public static final int abc_scrubber_track_mtrl_alpha = 1478;

    @DrawableRes
    public static final int abc_seekbar_thumb_material = 1479;

    @DrawableRes
    public static final int abc_seekbar_tick_mark_material = 1480;

    @DrawableRes
    public static final int abc_seekbar_track_material = 1481;

    @DrawableRes
    public static final int abc_spinner_mtrl_am_alpha = 1482;

    @DrawableRes
    public static final int abc_spinner_textfield_background_material = 1483;

    @DrawableRes
    public static final int abc_switch_thumb_material = 1484;

    @DrawableRes
    public static final int abc_switch_track_mtrl_alpha = 1485;

    @DrawableRes
    public static final int abc_tab_indicator_material = 1486;

    @DrawableRes
    public static final int abc_tab_indicator_mtrl_alpha = 1487;

    @DrawableRes
    public static final int abc_text_cursor_material = 1488;

    @DrawableRes
    public static final int abc_text_select_handle_left_mtrl_dark = 1489;

    @DrawableRes
    public static final int abc_text_select_handle_left_mtrl_light = 1490;

    @DrawableRes
    public static final int abc_text_select_handle_middle_mtrl_dark = 1491;

    @DrawableRes
    public static final int abc_text_select_handle_middle_mtrl_light = 1492;

    @DrawableRes
    public static final int abc_text_select_handle_right_mtrl_dark = 1493;

    @DrawableRes
    public static final int abc_text_select_handle_right_mtrl_light = 1494;

    @DrawableRes
    public static final int abc_textfield_activated_mtrl_alpha = 1495;

    @DrawableRes
    public static final int abc_textfield_default_mtrl_alpha = 1496;

    @DrawableRes
    public static final int abc_textfield_search_activated_mtrl_alpha = 1497;

    @DrawableRes
    public static final int abc_textfield_search_default_mtrl_alpha = 1498;

    @DrawableRes
    public static final int abc_textfield_search_material = 1499;

    @DrawableRes
    public static final int abc_vector_test = 1500;

    @DrawableRes
    public static final int amu_bubble_mask = 1501;

    @DrawableRes
    public static final int amu_bubble_shadow = 1502;

    @DrawableRes
    public static final int app_icon = 1503;

    @DrawableRes
    public static final int avd_hide_password = 1504;

    @DrawableRes
    public static final int avd_show_password = 1505;

    @DrawableRes
    public static final int back_arrow = 1506;

    @DrawableRes
    public static final int back_button = 1507;

    @DrawableRes
    public static final int blue_tick = 1508;

    @DrawableRes
    public static final int bottomsheet_round_bg = 1509;

    @DrawableRes
    public static final int btn_checkbox_checked_mtrl = 1510;

    @DrawableRes
    public static final int btn_checkbox_checked_to_unchecked_mtrl_animation = 1511;

    @DrawableRes
    public static final int btn_checkbox_unchecked_mtrl = 1512;

    @DrawableRes
    public static final int btn_checkbox_unchecked_to_checked_mtrl_animation = 1513;

    @DrawableRes
    public static final int btn_radio_off_mtrl = 1514;

    @DrawableRes
    public static final int btn_radio_off_to_on_mtrl_animation = 1515;

    @DrawableRes
    public static final int btn_radio_on_mtrl = 1516;

    @DrawableRes
    public static final int btn_radio_on_to_off_mtrl_animation = 1517;

    @DrawableRes
    public static final int ccp_down_arrow = 1518;

    @DrawableRes
    public static final int ccp_ic_arrow_drop_down = 1519;

    @DrawableRes
    public static final int ccp_selectable_bg = 1520;

    @DrawableRes
    public static final int change_location_icon = 1521;

    @DrawableRes
    public static final int checkbox_checked = 1522;

    @DrawableRes
    public static final int chevron_arrow_down = 1523;

    @DrawableRes
    public static final int circle_shape_border = 1524;

    @DrawableRes
    public static final int clock_icon_grey_border = 1525;

    @DrawableRes
    public static final int close_icon = 1526;

    @DrawableRes
    public static final int com_facebook_auth_dialog_background = 1527;

    @DrawableRes
    public static final int com_facebook_auth_dialog_cancel_background = 1528;

    @DrawableRes
    public static final int com_facebook_auth_dialog_header_background = 1529;

    @DrawableRes
    public static final int com_facebook_button_background = 1530;

    @DrawableRes
    public static final int com_facebook_button_icon = 1531;

    @DrawableRes
    public static final int com_facebook_button_like_background = 1532;

    @DrawableRes
    public static final int com_facebook_button_like_icon_selected = 1533;

    @DrawableRes
    public static final int com_facebook_close = 1534;

    @DrawableRes
    public static final int com_facebook_favicon_blue = 1535;

    @DrawableRes
    public static final int com_facebook_profile_picture_blank_portrait = 1536;

    @DrawableRes
    public static final int com_facebook_profile_picture_blank_square = 1537;

    @DrawableRes
    public static final int com_facebook_tooltip_black_background = 1538;

    @DrawableRes
    public static final int com_facebook_tooltip_black_bottomnub = 1539;

    @DrawableRes
    public static final int com_facebook_tooltip_black_topnub = 1540;

    @DrawableRes
    public static final int com_facebook_tooltip_black_xout = 1541;

    @DrawableRes
    public static final int com_facebook_tooltip_blue_background = 1542;

    @DrawableRes
    public static final int com_facebook_tooltip_blue_bottomnub = 1543;

    @DrawableRes
    public static final int com_facebook_tooltip_blue_topnub = 1544;

    @DrawableRes
    public static final int com_facebook_tooltip_blue_xout = 1545;

    @DrawableRes
    public static final int common_full_open_on_phone = 1546;

    @DrawableRes
    public static final int common_google_signin_btn_icon_dark = 1547;

    @DrawableRes
    public static final int common_google_signin_btn_icon_dark_disabled = 1548;

    @DrawableRes
    public static final int common_google_signin_btn_icon_dark_focused = 1549;

    @DrawableRes
    public static final int common_google_signin_btn_icon_dark_normal = 1550;

    @DrawableRes
    public static final int common_google_signin_btn_icon_dark_normal_background = 1551;

    @DrawableRes
    public static final int common_google_signin_btn_icon_dark_pressed = 1552;

    @DrawableRes
    public static final int common_google_signin_btn_icon_disabled = 1553;

    @DrawableRes
    public static final int common_google_signin_btn_icon_light = 1554;

    @DrawableRes
    public static final int common_google_signin_btn_icon_light_disabled = 1555;

    @DrawableRes
    public static final int common_google_signin_btn_icon_light_focused = 1556;

    @DrawableRes
    public static final int common_google_signin_btn_icon_light_normal = 1557;

    @DrawableRes
    public static final int common_google_signin_btn_icon_light_normal_background = 1558;

    @DrawableRes
    public static final int common_google_signin_btn_icon_light_pressed = 1559;

    @DrawableRes
    public static final int common_google_signin_btn_text_dark = 1560;

    @DrawableRes
    public static final int common_google_signin_btn_text_dark_disabled = 1561;

    @DrawableRes
    public static final int common_google_signin_btn_text_dark_focused = 1562;

    @DrawableRes
    public static final int common_google_signin_btn_text_dark_normal = 1563;

    @DrawableRes
    public static final int common_google_signin_btn_text_dark_normal_background = 1564;

    @DrawableRes
    public static final int common_google_signin_btn_text_dark_pressed = 1565;

    @DrawableRes
    public static final int common_google_signin_btn_text_disabled = 1566;

    @DrawableRes
    public static final int common_google_signin_btn_text_light = 1567;

    @DrawableRes
    public static final int common_google_signin_btn_text_light_disabled = 1568;

    @DrawableRes
    public static final int common_google_signin_btn_text_light_focused = 1569;

    @DrawableRes
    public static final int common_google_signin_btn_text_light_normal = 1570;

    @DrawableRes
    public static final int common_google_signin_btn_text_light_normal_background = 1571;

    @DrawableRes
    public static final int common_google_signin_btn_text_light_pressed = 1572;

    @DrawableRes
    public static final int common_ic_googleplayservices = 1573;

    @DrawableRes
    public static final int croped = 1574;

    @DrawableRes
    public static final int cropped_one = 1575;

    @DrawableRes
    public static final int cropped_three = 1576;

    @DrawableRes
    public static final int cropped_two = 1577;

    @DrawableRes
    public static final int customer_marker = 1578;

    @DrawableRes
    public static final int design_bottom_navigation_item_background = 1579;

    @DrawableRes
    public static final int design_fab_background = 1580;

    @DrawableRes
    public static final int design_ic_visibility = 1581;

    @DrawableRes
    public static final int design_ic_visibility_off = 1582;

    @DrawableRes
    public static final int design_password_eye = 1583;

    @DrawableRes
    public static final int design_snackbar_background = 1584;

    @DrawableRes
    public static final int dialog_cylinder_shape = 1585;

    @DrawableRes
    public static final int dialog_header_shape = 1586;

    @DrawableRes
    public static final int dotted_line = 1587;

    @DrawableRes
    public static final int driver_marker = 1588;

    @DrawableRes
    public static final int edit_text_feedback_bg_shape = 1589;

    @DrawableRes
    public static final int edit_text_search_bg_shape = 1590;

    @DrawableRes
    public static final int explore_tab_icon_grey = 1591;

    @DrawableRes
    public static final int fastscroll__default_bubble = 1592;

    @DrawableRes
    public static final int fastscroll__default_handle = 1593;

    @DrawableRes
    public static final int favourites_icon = 1594;

    @DrawableRes
    public static final int feed_placeholder = 1595;

    @DrawableRes
    public static final int filter_icon = 1596;

    @DrawableRes
    public static final int flag_afghanistan = 1597;

    @DrawableRes
    public static final int flag_aland = 1598;

    @DrawableRes
    public static final int flag_albania = 1599;

    @DrawableRes
    public static final int flag_algeria = 1600;

    @DrawableRes
    public static final int flag_american_samoa = 1601;

    @DrawableRes
    public static final int flag_andorra = 1602;

    @DrawableRes
    public static final int flag_angola = 1603;

    @DrawableRes
    public static final int flag_anguilla = 1604;

    @DrawableRes
    public static final int flag_antarctica = 1605;

    @DrawableRes
    public static final int flag_antigua_and_barbuda = 1606;

    @DrawableRes
    public static final int flag_argentina = 1607;

    @DrawableRes
    public static final int flag_armenia = 1608;

    @DrawableRes
    public static final int flag_aruba = 1609;

    @DrawableRes
    public static final int flag_australia = 1610;

    @DrawableRes
    public static final int flag_austria = 1611;

    @DrawableRes
    public static final int flag_azerbaijan = 1612;

    @DrawableRes
    public static final int flag_bahamas = 1613;

    @DrawableRes
    public static final int flag_bahrain = 1614;

    @DrawableRes
    public static final int flag_bangladesh = 1615;

    @DrawableRes
    public static final int flag_barbados = 1616;

    @DrawableRes
    public static final int flag_belarus = 1617;

    @DrawableRes
    public static final int flag_belgium = 1618;

    @DrawableRes
    public static final int flag_belize = 1619;

    @DrawableRes
    public static final int flag_benin = 1620;

    @DrawableRes
    public static final int flag_bermuda = 1621;

    @DrawableRes
    public static final int flag_bhutan = 1622;

    @DrawableRes
    public static final int flag_bolivia = 1623;

    @DrawableRes
    public static final int flag_bosnia = 1624;

    @DrawableRes
    public static final int flag_botswana = 1625;

    @DrawableRes
    public static final int flag_brazil = 1626;

    @DrawableRes
    public static final int flag_british_indian_ocean_territory = 1627;

    @DrawableRes
    public static final int flag_british_virgin_islands = 1628;

    @DrawableRes
    public static final int flag_brunei = 1629;

    @DrawableRes
    public static final int flag_bulgaria = 1630;

    @DrawableRes
    public static final int flag_burkina_faso = 1631;

    @DrawableRes
    public static final int flag_burundi = 1632;

    @DrawableRes
    public static final int flag_cambodia = 1633;

    @DrawableRes
    public static final int flag_cameroon = 1634;

    @DrawableRes
    public static final int flag_canada = 1635;

    @DrawableRes
    public static final int flag_cape_verde = 1636;

    @DrawableRes
    public static final int flag_cayman_islands = 1637;

    @DrawableRes
    public static final int flag_central_african_republic = 1638;

    @DrawableRes
    public static final int flag_chad = 1639;

    @DrawableRes
    public static final int flag_chile = 1640;

    @DrawableRes
    public static final int flag_china = 1641;

    @DrawableRes
    public static final int flag_christmas_island = 1642;

    @DrawableRes
    public static final int flag_cocos = 1643;

    @DrawableRes
    public static final int flag_colombia = 1644;

    @DrawableRes
    public static final int flag_comoros = 1645;

    @DrawableRes
    public static final int flag_cook_islands = 1646;

    @DrawableRes
    public static final int flag_costa_rica = 1647;

    @DrawableRes
    public static final int flag_cote_divoire = 1648;

    @DrawableRes
    public static final int flag_croatia = 1649;

    @DrawableRes
    public static final int flag_cuba = 1650;

    @DrawableRes
    public static final int flag_curacao = 1651;

    @DrawableRes
    public static final int flag_cyprus = 1652;

    @DrawableRes
    public static final int flag_czech_republic = 1653;

    @DrawableRes
    public static final int flag_democratic_republic_of_the_congo = 1654;

    @DrawableRes
    public static final int flag_denmark = 1655;

    @DrawableRes
    public static final int flag_djibouti = 1656;

    @DrawableRes
    public static final int flag_dominica = 1657;

    @DrawableRes
    public static final int flag_dominican_republic = 1658;

    @DrawableRes
    public static final int flag_ecuador = 1659;

    @DrawableRes
    public static final int flag_egypt = 1660;

    @DrawableRes
    public static final int flag_el_salvador = 1661;

    @DrawableRes
    public static final int flag_equatorial_guinea = 1662;

    @DrawableRes
    public static final int flag_eritrea = 1663;

    @DrawableRes
    public static final int flag_estonia = 1664;

    @DrawableRes
    public static final int flag_ethiopia = 1665;

    @DrawableRes
    public static final int flag_falkland_islands = 1666;

    @DrawableRes
    public static final int flag_faroe_islands = 1667;

    @DrawableRes
    public static final int flag_fiji = 1668;

    @DrawableRes
    public static final int flag_finland = 1669;

    @DrawableRes
    public static final int flag_france = 1670;

    @DrawableRes
    public static final int flag_french_polynesia = 1671;

    @DrawableRes
    public static final int flag_gabon = 1672;

    @DrawableRes
    public static final int flag_gambia = 1673;

    @DrawableRes
    public static final int flag_georgia = 1674;

    @DrawableRes
    public static final int flag_germany = 1675;

    @DrawableRes
    public static final int flag_ghana = 1676;

    @DrawableRes
    public static final int flag_gibraltar = 1677;

    @DrawableRes
    public static final int flag_greece = 1678;

    @DrawableRes
    public static final int flag_greenland = 1679;

    @DrawableRes
    public static final int flag_grenada = 1680;

    @DrawableRes
    public static final int flag_guadeloupe = 1681;

    @DrawableRes
    public static final int flag_guam = 1682;

    @DrawableRes
    public static final int flag_guatemala = 1683;

    @DrawableRes
    public static final int flag_guernsey = 1684;

    @DrawableRes
    public static final int flag_guinea = 1685;

    @DrawableRes
    public static final int flag_guinea_bissau = 1686;

    @DrawableRes
    public static final int flag_guyana = 1687;

    @DrawableRes
    public static final int flag_guyane = 1688;

    @DrawableRes
    public static final int flag_haiti = 1689;

    @DrawableRes
    public static final int flag_honduras = 1690;

    @DrawableRes
    public static final int flag_hong_kong = 1691;

    @DrawableRes
    public static final int flag_hungary = 1692;

    @DrawableRes
    public static final int flag_iceland = 1693;

    @DrawableRes
    public static final int flag_india = 1694;

    @DrawableRes
    public static final int flag_indonesia = 1695;

    @DrawableRes
    public static final int flag_iran = 1696;

    @DrawableRes
    public static final int flag_iraq = 1697;

    @DrawableRes
    public static final int flag_iraq_new = 1698;

    @DrawableRes
    public static final int flag_ireland = 1699;

    @DrawableRes
    public static final int flag_isleof_man = 1700;

    @DrawableRes
    public static final int flag_israel = 1701;

    @DrawableRes
    public static final int flag_italy = 1702;

    @DrawableRes
    public static final int flag_jamaica = 1703;

    @DrawableRes
    public static final int flag_japan = 1704;

    @DrawableRes
    public static final int flag_jersey = 1705;

    @DrawableRes
    public static final int flag_jordan = 1706;

    @DrawableRes
    public static final int flag_kazakhstan = 1707;

    @DrawableRes
    public static final int flag_kenya = 1708;

    @DrawableRes
    public static final int flag_kiribati = 1709;

    @DrawableRes
    public static final int flag_kosovo = 1710;

    @DrawableRes
    public static final int flag_kuwait = 1711;

    @DrawableRes
    public static final int flag_kyrgyzstan = 1712;

    @DrawableRes
    public static final int flag_laos = 1713;

    @DrawableRes
    public static final int flag_latvia = 1714;

    @DrawableRes
    public static final int flag_lebanon = 1715;

    @DrawableRes
    public static final int flag_lesotho = 1716;

    @DrawableRes
    public static final int flag_liberia = 1717;

    @DrawableRes
    public static final int flag_libya = 1718;

    @DrawableRes
    public static final int flag_liechtenstein = 1719;

    @DrawableRes
    public static final int flag_lithuania = 1720;

    @DrawableRes
    public static final int flag_luxembourg = 1721;

    @DrawableRes
    public static final int flag_macao = 1722;

    @DrawableRes
    public static final int flag_macedonia = 1723;

    @DrawableRes
    public static final int flag_madagascar = 1724;

    @DrawableRes
    public static final int flag_malawi = 1725;

    @DrawableRes
    public static final int flag_malaysia = 1726;

    @DrawableRes
    public static final int flag_maldives = 1727;

    @DrawableRes
    public static final int flag_mali = 1728;

    @DrawableRes
    public static final int flag_malta = 1729;

    @DrawableRes
    public static final int flag_marshall_islands = 1730;

    @DrawableRes
    public static final int flag_martinique = 1731;

    @DrawableRes
    public static final int flag_mauritania = 1732;

    @DrawableRes
    public static final int flag_mauritius = 1733;

    @DrawableRes
    public static final int flag_mexico = 1734;

    @DrawableRes
    public static final int flag_micronesia = 1735;

    @DrawableRes
    public static final int flag_moldova = 1736;

    @DrawableRes
    public static final int flag_monaco = 1737;

    @DrawableRes
    public static final int flag_mongolia = 1738;

    @DrawableRes
    public static final int flag_montserrat = 1739;

    @DrawableRes
    public static final int flag_morocco = 1740;

    @DrawableRes
    public static final int flag_mozambique = 1741;

    @DrawableRes
    public static final int flag_myanmar = 1742;

    @DrawableRes
    public static final int flag_namibia = 1743;

    @DrawableRes
    public static final int flag_nauru = 1744;

    @DrawableRes
    public static final int flag_nepal = 1745;

    @DrawableRes
    public static final int flag_netherlands = 1746;

    @DrawableRes
    public static final int flag_netherlands_antilles = 1747;

    @DrawableRes
    public static final int flag_new_caledonia = 1748;

    @DrawableRes
    public static final int flag_new_zealand = 1749;

    @DrawableRes
    public static final int flag_nicaragua = 1750;

    @DrawableRes
    public static final int flag_niger = 1751;

    @DrawableRes
    public static final int flag_nigeria = 1752;

    @DrawableRes
    public static final int flag_niue = 1753;

    @DrawableRes
    public static final int flag_norfolk_island = 1754;

    @DrawableRes
    public static final int flag_north_korea = 1755;

    @DrawableRes
    public static final int flag_northern_mariana_islands = 1756;

    @DrawableRes
    public static final int flag_norway = 1757;

    @DrawableRes
    public static final int flag_of_montenegro = 1758;

    @DrawableRes
    public static final int flag_oman = 1759;

    @DrawableRes
    public static final int flag_pakistan = 1760;

    @DrawableRes
    public static final int flag_palau = 1761;

    @DrawableRes
    public static final int flag_palestine = 1762;

    @DrawableRes
    public static final int flag_panama = 1763;

    @DrawableRes
    public static final int flag_papua_new_guinea = 1764;

    @DrawableRes
    public static final int flag_paraguay = 1765;

    @DrawableRes
    public static final int flag_peru = 1766;

    @DrawableRes
    public static final int flag_philippines = 1767;

    @DrawableRes
    public static final int flag_pitcairn_islands = 1768;

    @DrawableRes
    public static final int flag_poland = 1769;

    @DrawableRes
    public static final int flag_portugal = 1770;

    @DrawableRes
    public static final int flag_puerto_rico = 1771;

    @DrawableRes
    public static final int flag_qatar = 1772;

    @DrawableRes
    public static final int flag_republic_of_the_congo = 1773;

    @DrawableRes
    public static final int flag_romania = 1774;

    @DrawableRes
    public static final int flag_russian_federation = 1775;

    @DrawableRes
    public static final int flag_rwanda = 1776;

    @DrawableRes
    public static final int flag_saint_barthelemy = 1777;

    @DrawableRes
    public static final int flag_saint_helena = 1778;

    @DrawableRes
    public static final int flag_saint_kitts_and_nevis = 1779;

    @DrawableRes
    public static final int flag_saint_lucia = 1780;

    @DrawableRes
    public static final int flag_saint_martin = 1781;

    @DrawableRes
    public static final int flag_saint_pierre = 1782;

    @DrawableRes
    public static final int flag_saint_vicent_and_the_grenadines = 1783;

    @DrawableRes
    public static final int flag_samoa = 1784;

    @DrawableRes
    public static final int flag_san_marino = 1785;

    @DrawableRes
    public static final int flag_sao_tome_and_principe = 1786;

    @DrawableRes
    public static final int flag_saudi_arabia = 1787;

    @DrawableRes
    public static final int flag_senegal = 1788;

    @DrawableRes
    public static final int flag_serbia = 1789;

    @DrawableRes
    public static final int flag_serbia_and_montenegro = 1790;

    @DrawableRes
    public static final int flag_seychelles = 1791;

    @DrawableRes
    public static final int flag_sierra_leone = 1792;

    @DrawableRes
    public static final int flag_singapore = 1793;

    @DrawableRes
    public static final int flag_sint_maarten = 1794;

    @DrawableRes
    public static final int flag_slovakia = 1795;

    @DrawableRes
    public static final int flag_slovenia = 1796;

    @DrawableRes
    public static final int flag_soloman_islands = 1797;

    @DrawableRes
    public static final int flag_somalia = 1798;

    @DrawableRes
    public static final int flag_south_africa = 1799;

    @DrawableRes
    public static final int flag_south_georgia = 1800;

    @DrawableRes
    public static final int flag_south_korea = 1801;

    @DrawableRes
    public static final int flag_south_sudan = 1802;

    @DrawableRes
    public static final int flag_soviet_union = 1803;

    @DrawableRes
    public static final int flag_spain = 1804;

    @DrawableRes
    public static final int flag_sri_lanka = 1805;

    @DrawableRes
    public static final int flag_sudan = 1806;

    @DrawableRes
    public static final int flag_suriname = 1807;

    @DrawableRes
    public static final int flag_swaziland = 1808;

    @DrawableRes
    public static final int flag_sweden = 1809;

    @DrawableRes
    public static final int flag_switzerland = 1810;

    @DrawableRes
    public static final int flag_syria = 1811;

    @DrawableRes
    public static final int flag_taiwan = 1812;

    @DrawableRes
    public static final int flag_tajikistan = 1813;

    @DrawableRes
    public static final int flag_tanzania = 1814;

    @DrawableRes
    public static final int flag_thailand = 1815;

    @DrawableRes
    public static final int flag_tibet = 1816;

    @DrawableRes
    public static final int flag_timor_leste = 1817;

    @DrawableRes
    public static final int flag_togo = 1818;

    @DrawableRes
    public static final int flag_tokelau = 1819;

    @DrawableRes
    public static final int flag_tonga = 1820;

    @DrawableRes
    public static final int flag_transparent = 1821;

    @DrawableRes
    public static final int flag_trinidad_and_tobago = 1822;

    @DrawableRes
    public static final int flag_tunisia = 1823;

    @DrawableRes
    public static final int flag_turkey = 1824;

    @DrawableRes
    public static final int flag_turkmenistan = 1825;

    @DrawableRes
    public static final int flag_turks_and_caicos_islands = 1826;

    @DrawableRes
    public static final int flag_tuvalu = 1827;

    @DrawableRes
    public static final int flag_uae = 1828;

    @DrawableRes
    public static final int flag_uganda = 1829;

    @DrawableRes
    public static final int flag_ukraine = 1830;

    @DrawableRes
    public static final int flag_united_kingdom = 1831;

    @DrawableRes
    public static final int flag_united_states_of_america = 1832;

    @DrawableRes
    public static final int flag_uruguay = 1833;

    @DrawableRes
    public static final int flag_us_virgin_islands = 1834;

    @DrawableRes
    public static final int flag_uzbekistan = 1835;

    @DrawableRes
    public static final int flag_vanuatu = 1836;

    @DrawableRes
    public static final int flag_vatican_city = 1837;

    @DrawableRes
    public static final int flag_venezuela = 1838;

    @DrawableRes
    public static final int flag_vietnam = 1839;

    @DrawableRes
    public static final int flag_wallis_and_futuna = 1840;

    @DrawableRes
    public static final int flag_yemen = 1841;

    @DrawableRes
    public static final int flag_zambia = 1842;

    @DrawableRes
    public static final int flag_zimbabwe = 1843;

    @DrawableRes
    public static final int forgot_pass_bg = 1844;

    @DrawableRes
    public static final int googleg_disabled_color_18 = 1845;

    @DrawableRes
    public static final int googleg_standard_color_18 = 1846;

    @DrawableRes
    public static final int gps_icon = 1847;

    @DrawableRes
    public static final int green_round_corner = 1848;

    @DrawableRes
    public static final int heart_filled = 1849;

    @DrawableRes
    public static final int heart_un_filled = 1850;

    @DrawableRes
    public static final int home_icon = 1851;

    @DrawableRes
    public static final int ic_add_black_32dp = 1852;

    @DrawableRes
    public static final int ic_amex = 1853;

    @DrawableRes
    public static final int ic_amex_template_32 = 1854;

    @DrawableRes
    public static final int ic_backspace_black_24dp = 1855;

    @DrawableRes
    public static final int ic_checkmark = 1856;

    @DrawableRes
    public static final int ic_checkmark_tinted = 1857;

    @DrawableRes
    public static final int ic_clear_black_24dp = 1858;

    @DrawableRes
    public static final int ic_cvc = 1859;

    @DrawableRes
    public static final int ic_cvc_amex = 1860;

    @DrawableRes
    public static final int ic_diners = 1861;

    @DrawableRes
    public static final int ic_diners_template_32 = 1862;

    @DrawableRes
    public static final int ic_discover = 1863;

    @DrawableRes
    public static final int ic_discover_template_32 = 1864;

    @DrawableRes
    public static final int ic_edit = 1865;

    @DrawableRes
    public static final int ic_error = 1866;

    @DrawableRes
    public static final int ic_error_amex = 1867;

    @DrawableRes
    public static final int ic_jcb = 1868;

    @DrawableRes
    public static final int ic_jcb_template_32 = 1869;

    @DrawableRes
    public static final int ic_launcher_background = 1870;

    @DrawableRes
    public static final int ic_launcher_foreground = 1871;

    @DrawableRes
    public static final int ic_mastercard = 1872;

    @DrawableRes
    public static final int ic_mastercard_template_32 = 1873;

    @DrawableRes
    public static final int ic_mtrl_chip_checked_black = 1874;

    @DrawableRes
    public static final int ic_mtrl_chip_checked_circle = 1875;

    @DrawableRes
    public static final int ic_mtrl_chip_close_circle = 1876;

    @DrawableRes
    public static final int ic_star = 1877;

    @DrawableRes
    public static final int ic_unknown = 1878;

    @DrawableRes
    public static final int ic_visa = 1879;

    @DrawableRes
    public static final int ic_visa_template_32 = 1880;

    @DrawableRes
    public static final int location_icon = 1881;

    @DrawableRes
    public static final int location_marker_icon = 1882;

    @DrawableRes
    public static final int location_marker_white_icon = 1883;

    @DrawableRes
    public static final int logo = 1884;

    @DrawableRes
    public static final int manage_address_icon = 1885;

    @DrawableRes
    public static final int map_marker_icon = 1886;

    @DrawableRes
    public static final int menu_background_shape = 1887;

    @DrawableRes
    public static final int menu_button_shape = 1888;

    @DrawableRes
    public static final int minus_icon = 1889;

    @DrawableRes
    public static final int mtrl_snackbar_background = 1890;

    @DrawableRes
    public static final int mtrl_tabs_default_indicator = 1891;

    @DrawableRes
    public static final int navigation_empty_icon = 1892;

    @DrawableRes
    public static final int near_you_tab_icon_grey = 1893;

    @DrawableRes
    public static final int no_cart_items_icon = 1894;

    @DrawableRes
    public static final int no_connection_image = 1895;

    @DrawableRes
    public static final int no_restaurant_icon = 1896;

    @DrawableRes
    public static final int non_veg_icon = 1897;

    @DrawableRes
    public static final int notification_action_background = 1898;

    @DrawableRes
    public static final int notification_bg = 1899;

    @DrawableRes
    public static final int notification_bg_low = 1900;

    @DrawableRes
    public static final int notification_bg_low_normal = 1901;

    @DrawableRes
    public static final int notification_bg_low_pressed = 1902;

    @DrawableRes
    public static final int notification_bg_normal = 1903;

    @DrawableRes
    public static final int notification_bg_normal_pressed = 1904;

    @DrawableRes
    public static final int notification_icon_background = 1905;

    @DrawableRes
    public static final int notification_template_icon_bg = 1906;

    @DrawableRes
    public static final int notification_template_icon_low_bg = 1907;

    @DrawableRes
    public static final int notification_tile_bg = 1908;

    @DrawableRes
    public static final int notify_panel_notification_icon_bg = 1909;

    @DrawableRes
    public static final int offer_icon = 1910;

    @DrawableRes
    public static final int offer_icon_with_border = 1911;

    @DrawableRes
    public static final int offers_icon = 1912;

    @DrawableRes
    public static final int orders_tab_icon_grey = 1913;

    @DrawableRes
    public static final int otp_background = 1914;

    @DrawableRes
    public static final int oval_corner_button_primary = 1915;

    @DrawableRes
    public static final int oval_corner_primary = 1916;

    @DrawableRes
    public static final int pager_select = 1917;

    @DrawableRes
    public static final int pager_unselect = 1918;

    @DrawableRes
    public static final int payments_icon = 1919;

    @DrawableRes
    public static final int places_ic_clear = 1920;

    @DrawableRes
    public static final int places_ic_search = 1921;

    @DrawableRes
    public static final int plus = 1922;

    @DrawableRes
    public static final int powered_by_google_dark = 1923;

    @DrawableRes
    public static final int powered_by_google_light = 1924;

    @DrawableRes
    public static final int price_tag = 1925;

    @DrawableRes
    public static final int profile_tab_icon_grey = 1926;

    @DrawableRes
    public static final int rating_close_icon = 1927;

    @DrawableRes
    public static final int refer_and_earn_icon = 1928;

    @DrawableRes
    public static final int remove_icon = 1929;

    @DrawableRes
    public static final int restaurant_marker = 1930;

    @DrawableRes
    public static final int rounded_corner = 1931;

    @DrawableRes
    public static final int rounded_corner_shape_fill = 1932;

    @DrawableRes
    public static final int search_icon = 1933;

    @DrawableRes
    public static final int set_delivery_location_icon = 1934;

    @DrawableRes
    public static final int sign_in_bg = 1935;

    @DrawableRes
    public static final int simple_button_background = 1936;

    @DrawableRes
    public static final int slected_star_icon = 1937;

    @DrawableRes
    public static final int splash = 1938;

    @DrawableRes
    public static final int splash_icon = 1939;

    @DrawableRes
    public static final int star_icon_grey_border = 1940;

    @DrawableRes
    public static final int tooltip_frame_dark = 1941;

    @DrawableRes
    public static final int tooltip_frame_light = 1942;

    @DrawableRes
    public static final int transparent_background = 1943;

    @DrawableRes
    public static final int unslected_star_icon = 1944;

    @DrawableRes
    public static final int veg_icon = 1945;

    @DrawableRes
    public static final int walkthrough_image_one = 1946;

    @DrawableRes
    public static final int walkthrough_image_three = 1947;

    @DrawableRes
    public static final int walkthrough_image_two = 1948;

    @DrawableRes
    public static final int walkthrough_tick_icon = 1949;

    @DrawableRes
    public static final int warning_icon = 1950;

    @DrawableRes
    public static final int warning_icon_with_border = 1951;

    @DrawableRes
    public static final int white_radius = 1952;

    @DrawableRes
    public static final int work_icon = 1953;
  }

  public static final class id {
    @IdRes
    public static final int AFRIKAANS = 1954;

    @IdRes
    public static final int ARABIC = 1955;

    @IdRes
    public static final int BENGALI = 1956;

    @IdRes
    public static final int BLOCK = 1957;

    @IdRes
    public static final int BOTH = 1958;

    @IdRes
    public static final int BOTTOM = 1959;

    @IdRes
    public static final int CENTER = 1960;

    @IdRes
    public static final int CHINESE_SIMPLIFIED = 1961;

    @IdRes
    public static final int CHINESE_TRADITIONAL = 1962;

    @IdRes
    public static final int CZECH = 1963;

    @IdRes
    public static final int DANISH = 1964;

    @IdRes
    public static final int DUTCH = 1965;

    @IdRes
    public static final int ENGLISH = 1966;

    @IdRes
    public static final int FARSI = 1967;

    @IdRes
    public static final int FIXED_LINE = 1968;

    @IdRes
    public static final int FIXED_LINE_OR_MOBILE = 1969;

    @IdRes
    public static final int FRENCH = 1970;

    @IdRes
    public static final int GERMAN = 1971;

    @IdRes
    public static final int GREEK = 1972;

    @IdRes
    public static final int GUJARATI = 1973;

    @IdRes
    public static final int HEBREW = 1974;

    @IdRes
    public static final int HINDI = 1975;

    @IdRes
    public static final int INDONESIA = 1976;

    @IdRes
    public static final int ITALIAN = 1977;

    @IdRes
    public static final int JAPANESE = 1978;

    @IdRes
    public static final int KOREAN = 1979;

    @IdRes
    public static final int LEFT = 1980;

    @IdRes
    public static final int LOCALE_NETWORK = 1981;

    @IdRes
    public static final int LOCALE_NETWORK_SIM = 1982;

    @IdRes
    public static final int LOCALE_ONLY = 1983;

    @IdRes
    public static final int LOCALE_SIM = 1984;

    @IdRes
    public static final int LOCALE_SIM_NETWORK = 1985;

    @IdRes
    public static final int MOBILE = 1986;

    @IdRes
    public static final int NETWORK_LOCALE = 1987;

    @IdRes
    public static final int NETWORK_LOCALE_SIM = 1988;

    @IdRes
    public static final int NETWORK_ONLY = 1989;

    @IdRes
    public static final int NETWORK_SIM = 1990;

    @IdRes
    public static final int NETWORK_SIM_LOCALE = 1991;

    @IdRes
    public static final int NONE = 1992;

    @IdRes
    public static final int NORMAL = 1993;

    @IdRes
    public static final int PAGER = 1994;

    @IdRes
    public static final int PERSONAL_NUMBER = 1995;

    @IdRes
    public static final int POLISH = 1996;

    @IdRes
    public static final int PORTUGUESE = 1997;

    @IdRes
    public static final int PREMIUM_RATE = 1998;

    @IdRes
    public static final int PUNJABI = 1999;

    @IdRes
    public static final int RIGHT = 2000;

    @IdRes
    public static final int RUSSIAN = 2001;

    @IdRes
    public static final int SELECT = 2002;

    @IdRes
    public static final int SHARED_COST = 2003;

    @IdRes
    public static final int SIM_LOCALE = 2004;

    @IdRes
    public static final int SIM_LOCALE_NETWORK = 2005;

    @IdRes
    public static final int SIM_NETWORK = 2006;

    @IdRes
    public static final int SIM_NETWORK_LOCALE = 2007;

    @IdRes
    public static final int SIM_ONLY = 2008;

    @IdRes
    public static final int SLOVAK = 2009;

    @IdRes
    public static final int SPANISH = 2010;

    @IdRes
    public static final int SWEDISH = 2011;

    @IdRes
    public static final int TOLL_FREE = 2012;

    @IdRes
    public static final int TOP = 2013;

    @IdRes
    public static final int TRIANGLE = 2014;

    @IdRes
    public static final int TURKISH = 2015;

    @IdRes
    public static final int UAN = 2016;

    @IdRes
    public static final int UKRAINIAN = 2017;

    @IdRes
    public static final int UNKNOWN = 2018;

    @IdRes
    public static final int UZBEK = 2019;

    @IdRes
    public static final int VIETNAMESE = 2020;

    @IdRes
    public static final int VOICEMAIL = 2021;

    @IdRes
    public static final int VOIP = 2022;

    @IdRes
    public static final int accessibility_action_clickable_span = 2023;

    @IdRes
    public static final int accessibility_custom_action_0 = 2024;

    @IdRes
    public static final int accessibility_custom_action_1 = 2025;

    @IdRes
    public static final int accessibility_custom_action_10 = 2026;

    @IdRes
    public static final int accessibility_custom_action_11 = 2027;

    @IdRes
    public static final int accessibility_custom_action_12 = 2028;

    @IdRes
    public static final int accessibility_custom_action_13 = 2029;

    @IdRes
    public static final int accessibility_custom_action_14 = 2030;

    @IdRes
    public static final int accessibility_custom_action_15 = 2031;

    @IdRes
    public static final int accessibility_custom_action_16 = 2032;

    @IdRes
    public static final int accessibility_custom_action_17 = 2033;

    @IdRes
    public static final int accessibility_custom_action_18 = 2034;

    @IdRes
    public static final int accessibility_custom_action_19 = 2035;

    @IdRes
    public static final int accessibility_custom_action_2 = 2036;

    @IdRes
    public static final int accessibility_custom_action_20 = 2037;

    @IdRes
    public static final int accessibility_custom_action_21 = 2038;

    @IdRes
    public static final int accessibility_custom_action_22 = 2039;

    @IdRes
    public static final int accessibility_custom_action_23 = 2040;

    @IdRes
    public static final int accessibility_custom_action_24 = 2041;

    @IdRes
    public static final int accessibility_custom_action_25 = 2042;

    @IdRes
    public static final int accessibility_custom_action_26 = 2043;

    @IdRes
    public static final int accessibility_custom_action_27 = 2044;

    @IdRes
    public static final int accessibility_custom_action_28 = 2045;

    @IdRes
    public static final int accessibility_custom_action_29 = 2046;

    @IdRes
    public static final int accessibility_custom_action_3 = 2047;

    @IdRes
    public static final int accessibility_custom_action_30 = 2048;

    @IdRes
    public static final int accessibility_custom_action_31 = 2049;

    @IdRes
    public static final int accessibility_custom_action_4 = 2050;

    @IdRes
    public static final int accessibility_custom_action_5 = 2051;

    @IdRes
    public static final int accessibility_custom_action_6 = 2052;

    @IdRes
    public static final int accessibility_custom_action_7 = 2053;

    @IdRes
    public static final int accessibility_custom_action_8 = 2054;

    @IdRes
    public static final int accessibility_custom_action_9 = 2055;

    @IdRes
    public static final int action0 = 2056;

    @IdRes
    public static final int actionLayout = 2057;

    @IdRes
    public static final int action_bar = 2058;

    @IdRes
    public static final int action_bar_activity_content = 2059;

    @IdRes
    public static final int action_bar_container = 2060;

    @IdRes
    public static final int action_bar_root = 2061;

    @IdRes
    public static final int action_bar_spinner = 2062;

    @IdRes
    public static final int action_bar_subtitle = 2063;

    @IdRes
    public static final int action_bar_title = 2064;

    @IdRes
    public static final int action_container = 2065;

    @IdRes
    public static final int action_context_bar = 2066;

    @IdRes
    public static final int action_divider = 2067;

    @IdRes
    public static final int action_image = 2068;

    @IdRes
    public static final int action_menu_divider = 2069;

    @IdRes
    public static final int action_menu_presenter = 2070;

    @IdRes
    public static final int action_mode_bar = 2071;

    @IdRes
    public static final int action_mode_bar_stub = 2072;

    @IdRes
    public static final int action_mode_close_button = 2073;

    @IdRes
    public static final int action_save = 2074;

    @IdRes
    public static final int action_text = 2075;

    @IdRes
    public static final int actions = 2076;

    @IdRes
    public static final int activity_chooser_view_content = 2077;

    @IdRes
    public static final int add = 2078;

    @IdRes
    public static final int addCustomizationButton = 2079;

    @IdRes
    public static final int addNewAddres = 2080;

    @IdRes
    public static final int addNewAddressButton = 2081;

    @IdRes
    public static final int addText = 2082;

    @IdRes
    public static final int add_source_card_entry_widget = 2083;

    @IdRes
    public static final int add_source_error_container = 2084;

    @IdRes
    public static final int addressList = 2085;

    @IdRes
    public static final int addressLocation = 2086;

    @IdRes
    public static final int addressName = 2087;

    @IdRes
    public static final int addressText = 2088;

    @IdRes
    public static final int addressTypeName = 2089;

    @IdRes
    public static final int addresslayout = 2090;

    @IdRes
    public static final int adjust_height = 2091;

    @IdRes
    public static final int adjust_width = 2092;

    @IdRes
    public static final int alertTitle = 2093;

    @IdRes
    public static final int alphabet = 2094;

    @IdRes
    public static final int always = 2095;

    @IdRes
    public static final int amount = 2096;

    @IdRes
    public static final int amu_text = 2097;

    @IdRes
    public static final int applyCouponLayout = 2098;

    @IdRes
    public static final int apply_coupon = 2099;

    @IdRes
    public static final int async = 2100;

    @IdRes
    public static final int auto = 2101;

    @IdRes
    public static final int autoCompleteList = 2102;

    @IdRes
    public static final int autocomplete_country_cat = 2103;

    @IdRes
    public static final int automatic = 2104;

    @IdRes
    public static final int backButton = 2105;

    @IdRes
    public static final int backToMain = 2106;

    @IdRes
    public static final int back_button = 2107;

    @IdRes
    public static final int backimg = 2108;

    @IdRes
    public static final int bannerImageItem = 2109;

    @IdRes
    public static final int bannerViewPager = 2110;

    @IdRes
    public static final int bar_view_below_back = 2111;

    @IdRes
    public static final int bar_view_below_delivery_rating = 2112;

    @IdRes
    public static final int bar_view_below_restaurant_rating = 2113;

    @IdRes
    public static final int beginning = 2114;

    @IdRes
    public static final int bgimage = 2115;

    @IdRes
    public static final int billDetailsRecycler = 2116;

    @IdRes
    public static final int blockImage = 2117;

    @IdRes
    public static final int blocking = 2118;

    @IdRes
    public static final int bottom = 2119;

    @IdRes
    public static final int bottomNavigation = 2120;

    @IdRes
    public static final int bottomSheet = 2121;

    @IdRes
    public static final int bottomView = 2122;

    @IdRes
    public static final int bottom_to_top = 2123;

    @IdRes
    public static final int box_count = 2124;

    @IdRes
    public static final int browser_actions_header_text = 2125;

    @IdRes
    public static final int browser_actions_menu_item_icon = 2126;

    @IdRes
    public static final int browser_actions_menu_item_text = 2127;

    @IdRes
    public static final int browser_actions_menu_items = 2128;

    @IdRes
    public static final int browser_actions_menu_view = 2129;

    @IdRes
    public static final int button = 2130;

    @IdRes
    public static final int buttonPanel = 2131;

    @IdRes
    public static final int cancel_action = 2132;

    @IdRes
    public static final int cancel_button = 2133;

    @IdRes
    public static final int cardName = 2134;

    @IdRes
    public static final int card_number_label = 2135;

    @IdRes
    public static final int cartLayout = 2136;

    @IdRes
    public static final int cart_tool_bar = 2137;

    @IdRes
    public static final int categoryName = 2138;

    @IdRes
    public static final int categoryRecyclerView = 2139;

    @IdRes
    public static final int center = 2140;

    @IdRes
    public static final int center_horizontal = 2141;

    @IdRes
    public static final int center_vertical = 2142;

    @IdRes
    public static final int changeAddressButton = 2143;

    @IdRes
    public static final int changePassword = 2144;

    @IdRes
    public static final int changePasswordLayout = 2145;

    @IdRes
    public static final int chargesCost = 2146;

    @IdRes
    public static final int chargesRecycler = 2147;

    @IdRes
    public static final int chargesText = 2148;

    @IdRes
    public static final int checkPayment = 2149;

    @IdRes
    public static final int checkbox = 2150;

    @IdRes
    public static final int checked = 2151;

    @IdRes
    public static final int chooseCustomization = 2152;

    @IdRes
    public static final int chronometer = 2153;

    @IdRes
    public static final int clamp = 2154;

    @IdRes
    public static final int clearText = 2155;

    @IdRes
    public static final int clip_horizontal = 2156;

    @IdRes
    public static final int clip_vertical = 2157;

    @IdRes
    public static final int clock = 2158;

    @IdRes
    public static final int clock_icon = 2159;

    @IdRes
    public static final int collapseActionView = 2160;

    @IdRes
    public static final int com_facebook_body_frame = 2161;

    @IdRes
    public static final int com_facebook_button_xout = 2162;

    @IdRes
    public static final int com_facebook_device_auth_instructions = 2163;

    @IdRes
    public static final int com_facebook_fragment_container = 2164;

    @IdRes
    public static final int com_facebook_login_fragment_progress_bar = 2165;

    @IdRes
    public static final int com_facebook_smart_instructions_0 = 2166;

    @IdRes
    public static final int com_facebook_smart_instructions_or = 2167;

    @IdRes
    public static final int com_facebook_tooltip_bubble_view_bottom_pointer = 2168;

    @IdRes
    public static final int com_facebook_tooltip_bubble_view_text_body = 2169;

    @IdRes
    public static final int com_facebook_tooltip_bubble_view_top_pointer = 2170;

    @IdRes
    public static final int confirmPassword = 2171;

    @IdRes
    public static final int confirmPasswordContainer = 2172;

    @IdRes
    public static final int confirmPasswordWrapper = 2173;

    @IdRes
    public static final int confirmation_code = 2174;

    @IdRes
    public static final int connectionLostLayout = 2175;

    @IdRes
    public static final int constraintLayout = 2176;

    @IdRes
    public static final int container = 2177;

    @IdRes
    public static final int content = 2178;

    @IdRes
    public static final int contentLayout = 2179;

    @IdRes
    public static final int contentPanel = 2180;

    @IdRes
    public static final int coordinator = 2181;

    @IdRes
    public static final int coordinator_layout = 2182;

    @IdRes
    public static final int countryCodeHolder = 2183;

    @IdRes
    public static final int countryCodePicker = 2184;

    @IdRes
    public static final int country_autocomplete_aaw = 2185;

    @IdRes
    public static final int couponAppliedLayout = 2186;

    @IdRes
    public static final int couponCode = 2187;

    @IdRes
    public static final int couponErrorMessage = 2188;

    @IdRes
    public static final int couponErrortitle = 2189;

    @IdRes
    public static final int couponLayout = 2190;

    @IdRes
    public static final int coupon_code = 2191;

    @IdRes
    public static final int coupon_description = 2192;

    @IdRes
    public static final int coupon_layout = 2193;

    @IdRes
    public static final int currentLocationLayout = 2194;

    @IdRes
    public static final int currentLocationWithAddressLayout = 2195;

    @IdRes
    public static final int cusine_text = 2196;

    @IdRes
    public static final int custom = 2197;

    @IdRes
    public static final int customPanel = 2198;

    @IdRes
    public static final int customizableDishItemsRecycler = 2199;

    @IdRes
    public static final int customizableElementsRecycler = 2200;

    @IdRes
    public static final int customizableItemLayout = 2201;

    @IdRes
    public static final int customizableLayout = 2202;

    @IdRes
    public static final int customizationText = 2203;

    @IdRes
    public static final int cw_0 = 2204;

    @IdRes
    public static final int cw_180 = 2205;

    @IdRes
    public static final int cw_270 = 2206;

    @IdRes
    public static final int cw_90 = 2207;

    @IdRes
    public static final int dark = 2208;

    @IdRes
    public static final int dateAndTime = 2209;

    @IdRes
    public static final int decor_content_parent = 2210;

    @IdRes
    public static final int decreaseCart = 2211;

    @IdRes
    public static final int default_activity_button = 2212;

    @IdRes
    public static final int deleteAddress = 2213;

    @IdRes
    public static final int deleteItem = 2214;

    @IdRes
    public static final int deliverAddressLayout = 2215;

    @IdRes
    public static final int deliverAddressText = 2216;

    @IdRes
    public static final int deliverImage = 2217;

    @IdRes
    public static final int deliverMinsText = 2218;

    @IdRes
    public static final int deliverToText = 2219;

    @IdRes
    public static final int deliverTypeImage = 2220;

    @IdRes
    public static final int deliveryAddress = 2221;

    @IdRes
    public static final int delivery_boy_rating = 2222;

    @IdRes
    public static final int description = 2223;

    @IdRes
    public static final int design_bottom_sheet = 2224;

    @IdRes
    public static final int design_menu_item_action_area = 2225;

    @IdRes
    public static final int design_menu_item_action_area_stub = 2226;

    @IdRes
    public static final int design_menu_item_text = 2227;

    @IdRes
    public static final int design_navigation_view = 2228;

    @IdRes
    public static final int dialog_button = 2229;

    @IdRes
    public static final int disableHome = 2230;

    @IdRes
    public static final int dishCheckBox = 2231;

    @IdRes
    public static final int dishCost = 2232;

    @IdRes
    public static final int dishImage = 2233;

    @IdRes
    public static final int dishItemsOrdered = 2234;

    @IdRes
    public static final int dishName = 2235;

    @IdRes
    public static final int dish_name = 2236;

    @IdRes
    public static final int dish_price = 2237;

    @IdRes
    public static final int dishesRecycler = 2238;

    @IdRes
    public static final int dishes_explore_recycler = 2239;

    @IdRes
    public static final int displayPrice = 2240;

    @IdRes
    public static final int display_always = 2241;

    @IdRes
    public static final int earning_details_txt = 2242;

    @IdRes
    public static final int editButton = 2243;

    @IdRes
    public static final int editCloseButton = 2244;

    @IdRes
    public static final int editText_search = 2245;

    @IdRes
    public static final int edit_query = 2246;

    @IdRes
    public static final int email = 2247;

    @IdRes
    public static final int emailAddress = 2248;

    @IdRes
    public static final int emailText = 2249;

    @IdRes
    public static final int emailWrapper = 2250;

    @IdRes
    public static final int end = 2251;

    @IdRes
    public static final int end_padder = 2252;

    @IdRes
    public static final int enter_otp_text = 2253;

    @IdRes
    public static final int estimatedTimeLayout = 2254;

    @IdRes
    public static final int et_add_source_card_number_ml = 2255;

    @IdRes
    public static final int et_add_source_cvc_ml = 2256;

    @IdRes
    public static final int et_add_source_expiry_ml = 2257;

    @IdRes
    public static final int et_add_source_postal_ml = 2258;

    @IdRes
    public static final int et_address_line_one_aaw = 2259;

    @IdRes
    public static final int et_address_line_two_aaw = 2260;

    @IdRes
    public static final int et_card_number = 2261;

    @IdRes
    public static final int et_city_aaw = 2262;

    @IdRes
    public static final int et_cvc_number = 2263;

    @IdRes
    public static final int et_expiry_date = 2264;

    @IdRes
    public static final int et_name_aaw = 2265;

    @IdRes
    public static final int et_phone_number_aaw = 2266;

    @IdRes
    public static final int et_postal_code_aaw = 2267;

    @IdRes
    public static final int et_state_aaw = 2268;

    @IdRes
    public static final int eta_time = 2269;

    @IdRes
    public static final int expand_activities_button = 2270;

    @IdRes
    public static final int expandableLayout = 2271;

    @IdRes
    public static final int expanded_menu = 2272;

    @IdRes
    public static final int expiry_date_label = 2273;

    @IdRes
    public static final int exploreEditText = 2274;

    @IdRes
    public static final int explore_item = 2275;

    @IdRes
    public static final int failureLayout = 2276;

    @IdRes
    public static final int faqLinksButton = 2277;

    @IdRes
    public static final int fastscroll = 2278;

    @IdRes
    public static final int favouriteButton = 2279;

    @IdRes
    public static final int fill = 2280;

    @IdRes
    public static final int fill_horizontal = 2281;

    @IdRes
    public static final int fill_vertical = 2282;

    @IdRes
    public static final int filled = 2283;

    @IdRes
    public static final int fixed = 2284;

    @IdRes
    public static final int focus_thief = 2285;

    @IdRes
    public static final int forever = 2286;

    @IdRes
    public static final int forgotPassword = 2287;

    @IdRes
    public static final int fourSemiBold = 2288;

    @IdRes
    public static final int fragment_container_view_tag = 2289;

    @IdRes
    public static final int frame_container = 2290;

    @IdRes
    public static final int from_delivery = 2291;

    @IdRes
    public static final int from_outlet = 2292;

    @IdRes
    public static final int ghost_view = 2293;

    @IdRes
    public static final int glide_custom_view_target_tag = 2294;

    @IdRes
    public static final int goNextButton = 2295;

    @IdRes
    public static final int gone = 2296;

    @IdRes
    public static final int grid = 2297;

    @IdRes
    public static final int group_divider = 2298;

    @IdRes
    public static final int guidlineEnd = 2299;

    @IdRes
    public static final int guidlineStart = 2300;

    @IdRes
    public static final int headingText = 2301;

    @IdRes
    public static final int headsLayout = 2302;

    @IdRes
    public static final int home = 2303;

    @IdRes
    public static final int homeAsUp = 2304;

    @IdRes
    public static final int homeBottomView = 2305;

    @IdRes
    public static final int homeIcon = 2306;

    @IdRes
    public static final int homeLayout = 2307;

    @IdRes
    public static final int homeText = 2308;

    @IdRes
    public static final int horizontal = 2309;

    @IdRes
    public static final int house_number = 2310;

    @IdRes
    public static final int hybrid = 2311;

    @IdRes
    public static final int icon = 2312;

    @IdRes
    public static final int icon_group = 2313;

    @IdRes
    public static final int icon_only = 2314;

    @IdRes
    public static final int ifRoom = 2315;

    @IdRes
    public static final int image = 2316;

    @IdRes
    public static final int imageMarker = 2317;

    @IdRes
    public static final int imageView_arrow = 2318;

    @IdRes
    public static final int image_card = 2319;

    @IdRes
    public static final int image_flag = 2320;

    @IdRes
    public static final int img_clear_query = 2321;

    @IdRes
    public static final int img_dismiss = 2322;

    @IdRes
    public static final int increaseCart = 2323;

    @IdRes
    public static final int indicator = 2324;

    @IdRes
    public static final int info = 2325;

    @IdRes
    public static final int inline = 2326;

    @IdRes
    public static final int invisible = 2327;

    @IdRes
    public static final int isVegView = 2328;

    @IdRes
    public static final int italic = 2329;

    @IdRes
    public static final int itemTotalText = 2330;

    @IdRes
    public static final int item_touch_helper_previous_elevation = 2331;

    @IdRes
    public static final int ivImage = 2332;

    @IdRes
    public static final int iv_card_icon = 2333;

    @IdRes
    public static final int iv_marker_right = 2334;

    @IdRes
    public static final int iv_selected_icon = 2335;

    @IdRes
    public static final int iv_tab_icon = 2336;

    @IdRes
    public static final int labeled = 2337;

    @IdRes
    public static final int landmark = 2338;

    @IdRes
    public static final int large = 2339;

    @IdRes
    public static final int largeLabel = 2340;

    @IdRes
    public static final int layout = 2341;

    @IdRes
    public static final int left = 2342;

    @IdRes
    public static final int left_to_right = 2343;

    @IdRes
    public static final int light = 2344;

    @IdRes
    public static final int line = 2345;

    @IdRes
    public static final int line1 = 2346;

    @IdRes
    public static final int line3 = 2347;

    @IdRes
    public static final int linear = 2348;

    @IdRes
    public static final int linear_flag_border = 2349;

    @IdRes
    public static final int linear_flag_holder = 2350;

    @IdRes
    public static final int linear_horizontal = 2351;

    @IdRes
    public static final int linear_vertical = 2352;

    @IdRes
    public static final int listMode = 2353;

    @IdRes
    public static final int list_item = 2354;

    @IdRes
    public static final int livetrackingContentLayout = 2355;

    @IdRes
    public static final int livetrackingLoadingLayout = 2356;

    @IdRes
    public static final int livetrackingNointernetConnection = 2357;

    @IdRes
    public static final int ll_marker_right = 2358;

    @IdRes
    public static final int ll_tap = 2359;

    @IdRes
    public static final int loadingLayout = 2360;

    @IdRes
    public static final int locationMarker = 2361;

    @IdRes
    public static final int locationMarkertext = 2362;

    @IdRes
    public static final int locationName = 2363;

    @IdRes
    public static final int locationTypeLayout = 2364;

    @IdRes
    public static final int locationlayout = 2365;

    @IdRes
    public static final int loginButton = 2366;

    @IdRes
    public static final int loginCardView = 2367;

    @IdRes
    public static final int login_button = 2368;

    @IdRes
    public static final int logoutButton = 2369;

    @IdRes
    public static final int main_content = 2370;

    @IdRes
    public static final int manageAddressButton = 2371;

    @IdRes
    public static final int map = 2372;

    @IdRes
    public static final int mapFragment = 2373;

    @IdRes
    public static final int masked = 2374;

    @IdRes
    public static final int masked_card_info_view = 2375;

    @IdRes
    public static final int masked_card_item = 2376;

    @IdRes
    public static final int masked_check_icon = 2377;

    @IdRes
    public static final int masked_icon_view = 2378;

    @IdRes
    public static final int meatContaining = 2379;

    @IdRes
    public static final int media_actions = 2380;

    @IdRes
    public static final int menuButton = 2381;

    @IdRes
    public static final int menuItem = 2382;

    @IdRes
    public static final int menuRecycler = 2383;

    @IdRes
    public static final int menu_layout = 2384;

    @IdRes
    public static final int message = 2385;

    @IdRes
    public static final int middle = 2386;

    @IdRes
    public static final int middleView = 2387;

    @IdRes
    public static final int mini = 2388;

    @IdRes
    public static final int mirror = 2389;

    @IdRes
    public static final int mobileNumberText = 2390;

    @IdRes
    public static final int mtrl_child_content_container = 2391;

    @IdRes
    public static final int mtrl_internal_children_alpha_tag = 2392;

    @IdRes
    public static final int multiply = 2393;

    @IdRes
    public static final int myAccountButton = 2394;

    @IdRes
    public static final int myAccountLayout = 2395;

    @IdRes
    public static final int name = 2396;

    @IdRes
    public static final int nameEditText = 2397;

    @IdRes
    public static final int nameText = 2398;

    @IdRes
    public static final int nameWrapper = 2399;

    @IdRes
    public static final int navigation_header_container = 2400;

    @IdRes
    public static final int near_you_item = 2401;

    @IdRes
    public static final int nestedScrollView = 2402;

    @IdRes
    public static final int never = 2403;

    @IdRes
    public static final int never_display = 2404;

    @IdRes
    public static final int newPassword = 2405;

    @IdRes
    public static final int newPasswordContainer = 2406;

    @IdRes
    public static final int newPasswordWrapper = 2407;

    @IdRes
    public static final int noAddressLayout = 2408;

    @IdRes
    public static final int noButton = 2409;

    @IdRes
    public static final int noCartItems = 2410;

    @IdRes
    public static final int noInternetLayout = 2411;

    @IdRes
    public static final int noRestaurantAvailable = 2412;

    @IdRes
    public static final int none = 2413;

    @IdRes
    public static final int normal = 2414;

    @IdRes
    public static final int notification_background = 2415;

    @IdRes
    public static final int notification_main_column = 2416;

    @IdRes
    public static final int notification_main_column_container = 2417;

    @IdRes
    public static final int number = 2418;

    @IdRes
    public static final int off = 2419;

    @IdRes
    public static final int okButton = 2420;

    @IdRes
    public static final int okCouponText = 2421;

    @IdRes
    public static final int on = 2422;

    @IdRes
    public static final int oneBold = 2423;

    @IdRes
    public static final int oneMedium = 2424;

    @IdRes
    public static final int oneNormal = 2425;

    @IdRes
    public static final int open_graph = 2426;

    @IdRes
    public static final int orderConfirmedLayout = 2427;

    @IdRes
    public static final int orderConfirmedNotTick = 2428;

    @IdRes
    public static final int orderConfirmedText = 2429;

    @IdRes
    public static final int orderConfirmedTextDescri = 2430;

    @IdRes
    public static final int orderConfirmedTick = 2431;

    @IdRes
    public static final int orderConfirmedTime = 2432;

    @IdRes
    public static final int orderDeliveredLayout = 2433;

    @IdRes
    public static final int orderNumber = 2434;

    @IdRes
    public static final int orderPickedUpNotTick = 2435;

    @IdRes
    public static final int orderPickedUpText = 2436;

    @IdRes
    public static final int orderPickedUpTick = 2437;

    @IdRes
    public static final int orderPickedUpTime = 2438;

    @IdRes
    public static final int orderPickedupLayout = 2439;

    @IdRes
    public static final int orderReceivedLayout = 2440;

    @IdRes
    public static final int orderReceivedNotTick = 2441;

    @IdRes
    public static final int orderReceivedText = 2442;

    @IdRes
    public static final int orderReceivedTick = 2443;

    @IdRes
    public static final int orderReceivedTime = 2444;

    @IdRes
    public static final int orderRejectedLayout = 2445;

    @IdRes
    public static final int orders_item = 2446;

    @IdRes
    public static final int otherAddress = 2447;

    @IdRes
    public static final int otherBottomView = 2448;

    @IdRes
    public static final int otherIcon = 2449;

    @IdRes
    public static final int otherLayout = 2450;

    @IdRes
    public static final int otherText = 2451;

    @IdRes
    public static final int othersAddresLayout = 2452;

    @IdRes
    public static final int otpText = 2453;

    @IdRes
    public static final int otp_view = 2454;

    @IdRes
    public static final int outLetLayout = 2455;

    @IdRes
    public static final int outLetRecyclerView = 2456;

    @IdRes
    public static final int outletLocation = 2457;

    @IdRes
    public static final int outletName = 2458;

    @IdRes
    public static final int outlet_image = 2459;

    @IdRes
    public static final int outline = 2460;

    @IdRes
    public static final int packed = 2461;

    @IdRes
    public static final int page = 2462;

    @IdRes
    public static final int pager = 2463;

    @IdRes
    public static final int parallax = 2464;

    @IdRes
    public static final int parent = 2465;

    @IdRes
    public static final int parentLayout = 2466;

    @IdRes
    public static final int parentPanel = 2467;

    @IdRes
    public static final int parent_matrix = 2468;

    @IdRes
    public static final int parnetLayout = 2469;

    @IdRes
    public static final int password = 2470;

    @IdRes
    public static final int passwordContainer = 2471;

    @IdRes
    public static final int passwordLayout = 2472;

    @IdRes
    public static final int passwordWrapper = 2473;

    @IdRes
    public static final int pastOrdersLayout = 2474;

    @IdRes
    public static final int pastOrdersRecycler = 2475;

    @IdRes
    public static final int paymentImage = 2476;

    @IdRes
    public static final int paymentMethodsRecycler = 2477;

    @IdRes
    public static final int paymentName = 2478;

    @IdRes
    public static final int payment_methods_add_payment_container = 2479;

    @IdRes
    public static final int payment_methods_progress_bar = 2480;

    @IdRes
    public static final int payment_methods_recycler = 2481;

    @IdRes
    public static final int payment_methods_toolbar = 2482;

    @IdRes
    public static final int percent = 2483;

    @IdRes
    public static final int phoneNumber = 2484;

    @IdRes
    public static final int phoneNumberHint = 2485;

    @IdRes
    public static final int pin = 2486;

    @IdRes
    public static final int place_autocomplete_clear_button = 2487;

    @IdRes
    public static final int place_autocomplete_powered_by_google = 2488;

    @IdRes
    public static final int place_autocomplete_prediction_primary_text = 2489;

    @IdRes
    public static final int place_autocomplete_prediction_secondary_text = 2490;

    @IdRes
    public static final int place_autocomplete_progress = 2491;

    @IdRes
    public static final int place_autocomplete_search_button = 2492;

    @IdRes
    public static final int place_autocomplete_search_input = 2493;

    @IdRes
    public static final int place_autocomplete_separator = 2494;

    @IdRes
    public static final int place_detail = 2495;

    @IdRes
    public static final int place_name = 2496;

    @IdRes
    public static final int preferenceDivider = 2497;

    @IdRes
    public static final int price = 2498;

    @IdRes
    public static final int proceedButton = 2499;

    @IdRes
    public static final int proceedPayButton = 2500;

    @IdRes
    public static final int profile_item = 2501;

    @IdRes
    public static final int progressBar = 2502;

    @IdRes
    public static final int progressBarAccount = 2503;

    @IdRes
    public static final int progressBarChangePassword = 2504;

    @IdRes
    public static final int progressBarOTP = 2505;

    @IdRes
    public static final int progress_bar = 2506;

    @IdRes
    public static final int progress_bar_as = 2507;

    @IdRes
    public static final int progress_circular = 2508;

    @IdRes
    public static final int progress_horizontal = 2509;

    @IdRes
    public static final int quantity = 2510;

    @IdRes
    public static final int radial = 2511;

    @IdRes
    public static final int radio = 2512;

    @IdRes
    public static final int rate_your_delivery_text = 2513;

    @IdRes
    public static final int rate_your_food_text = 2514;

    @IdRes
    public static final int ratingLayout = 2515;

    @IdRes
    public static final int rating_close = 2516;

    @IdRes
    public static final int rating_icon = 2517;

    @IdRes
    public static final int reOrderTrackButton = 2518;

    @IdRes
    public static final int recentSearchRecycler = 2519;

    @IdRes
    public static final int rectangle = 2520;

    @IdRes
    public static final int recycler_countryDialog = 2521;

    @IdRes
    public static final int referralCode = 2522;

    @IdRes
    public static final int referralCodeCheckbox = 2523;

    @IdRes
    public static final int referralCodeParent = 2524;

    @IdRes
    public static final int referralCodeWrapper = 2525;

    @IdRes
    public static final int relatedTo = 2526;

    @IdRes
    public static final int removeCoupon = 2527;

    @IdRes
    public static final int repeat = 2528;

    @IdRes
    public static final int repeatCustomization = 2529;

    @IdRes
    public static final int resendOtp = 2530;

    @IdRes
    public static final int restart = 2531;

    @IdRes
    public static final int restaurantApiContentLayout = 2532;

    @IdRes
    public static final int restaurantCount = 2533;

    @IdRes
    public static final int restaurantCuisineType = 2534;

    @IdRes
    public static final int restaurantDeliveryTime = 2535;

    @IdRes
    public static final int restaurantImage = 2536;

    @IdRes
    public static final int restaurantLayout = 2537;

    @IdRes
    public static final int restaurantLocaleContentLayout = 2538;

    @IdRes
    public static final int restaurantLocation = 2539;

    @IdRes
    public static final int restaurantName = 2540;

    @IdRes
    public static final int restaurantNameToolbar = 2541;

    @IdRes
    public static final int restaurantOutletsCount = 2542;

    @IdRes
    public static final int restaurantOutletsCountLayout = 2543;

    @IdRes
    public static final int restaurantOutletsName = 2544;

    @IdRes
    public static final int restaurantOutletsRating = 2545;

    @IdRes
    public static final int restaurantOutletsTime = 2546;

    @IdRes
    public static final int restaurantPrice = 2547;

    @IdRes
    public static final int restaurantRating = 2548;

    @IdRes
    public static final int restaurantRecyclerView = 2549;

    @IdRes
    public static final int restaurant_image = 2550;

    @IdRes
    public static final int restaurant_rating = 2551;

    @IdRes
    public static final int restaurant_view = 2552;

    @IdRes
    public static final int retryButton = 2553;

    @IdRes
    public static final int reverse = 2554;

    @IdRes
    public static final int right = 2555;

    @IdRes
    public static final int right_icon = 2556;

    @IdRes
    public static final int right_side = 2557;

    @IdRes
    public static final int right_to_left = 2558;

    @IdRes
    public static final int rlClickConsumer = 2559;

    @IdRes
    public static final int rl_holder = 2560;

    @IdRes
    public static final int rl_query_holder = 2561;

    @IdRes
    public static final int rl_title = 2562;

    @IdRes
    public static final int rtv_msg_tip = 2563;

    @IdRes
    public static final int rv_shipping_methods_ssmw = 2564;

    @IdRes
    public static final int rvp_fragment_container = 2565;

    @IdRes
    public static final int satellite = 2566;

    @IdRes
    public static final int saveButton = 2567;

    @IdRes
    public static final int save_image_matrix = 2568;

    @IdRes
    public static final int save_non_transition_alpha = 2569;

    @IdRes
    public static final int save_scale_type = 2570;

    @IdRes
    public static final int savedAddressLayout = 2571;

    @IdRes
    public static final int savedAddressRecycler = 2572;

    @IdRes
    public static final int screen = 2573;

    @IdRes
    public static final int scrollIndicatorDown = 2574;

    @IdRes
    public static final int scrollIndicatorUp = 2575;

    @IdRes
    public static final int scrollView = 2576;

    @IdRes
    public static final int scrollable = 2577;

    @IdRes
    public static final int searchBox = 2578;

    @IdRes
    public static final int searchButton = 2579;

    @IdRes
    public static final int search_badge = 2580;

    @IdRes
    public static final int search_bar = 2581;

    @IdRes
    public static final int search_button = 2582;

    @IdRes
    public static final int search_close_btn = 2583;

    @IdRes
    public static final int search_edit_frame = 2584;

    @IdRes
    public static final int search_go_btn = 2585;

    @IdRes
    public static final int search_mag_icon = 2586;

    @IdRes
    public static final int search_plate = 2587;

    @IdRes
    public static final int search_src_text = 2588;

    @IdRes
    public static final int search_voice_btn = 2589;

    @IdRes
    public static final int second_row_layout = 2590;

    @IdRes
    public static final int select_dialog_listview = 2591;

    @IdRes
    public static final int select_shipping_method_widget = 2592;

    @IdRes
    public static final int selected = 2593;

    @IdRes
    public static final int selectedCustomizationsRecycler = 2594;

    @IdRes
    public static final int selectedItems = 2595;

    @IdRes
    public static final int setDeliveryLocationButton = 2596;

    @IdRes
    public static final int setDeliveryLocationLayout = 2597;

    @IdRes
    public static final int shadow_view = 2598;

    @IdRes
    public static final int shipping_flow_viewpager = 2599;

    @IdRes
    public static final int shipping_info_widget = 2600;

    @IdRes
    public static final int shortcut = 2601;

    @IdRes
    public static final int showCustom = 2602;

    @IdRes
    public static final int showHome = 2603;

    @IdRes
    public static final int showMore = 2604;

    @IdRes
    public static final int showTitle = 2605;

    @IdRes
    public static final int signUpButton = 2606;

    @IdRes
    public static final int signUpText = 2607;

    @IdRes
    public static final int sign_in_button = 2608;

    @IdRes
    public static final int singleSelect = 2609;

    @IdRes
    public static final int slashPrice = 2610;

    @IdRes
    public static final int slideDownLayout = 2611;

    @IdRes
    public static final int small = 2612;

    @IdRes
    public static final int smallLabel = 2613;

    @IdRes
    public static final int snackbar_action = 2614;

    @IdRes
    public static final int snackbar_text = 2615;

    @IdRes
    public static final int sortType = 2616;

    @IdRes
    public static final int spacer = 2617;

    @IdRes
    public static final int splashLayout = 2618;

    @IdRes
    public static final int split_action_bar = 2619;

    @IdRes
    public static final int spread = 2620;

    @IdRes
    public static final int spread_inside = 2621;

    @IdRes
    public static final int src_atop = 2622;

    @IdRes
    public static final int src_in = 2623;

    @IdRes
    public static final int src_over = 2624;

    @IdRes
    public static final int standard = 2625;

    @IdRes
    public static final int start = 2626;

    @IdRes
    public static final int statusImage = 2627;

    @IdRes
    public static final int status_bar_latest_event_content = 2628;

    @IdRes
    public static final int stretch = 2629;

    @IdRes
    public static final int subCategoryLayout = 2630;

    @IdRes
    public static final int subCategoryName = 2631;

    @IdRes
    public static final int subCategoryRecyclerView = 2632;

    @IdRes
    public static final int subCategroyNameItems = 2633;

    @IdRes
    public static final int submenuarrow = 2634;

    @IdRes
    public static final int submitButton = 2635;

    @IdRes
    public static final int submit_area = 2636;

    @IdRes
    public static final int successLayout = 2637;

    @IdRes
    public static final int suggestions = 2638;

    @IdRes
    public static final int suggestions_feedback = 2639;

    @IdRes
    public static final int swipeView = 2640;

    @IdRes
    public static final int tabLayout = 2641;

    @IdRes
    public static final int tabMode = 2642;

    @IdRes
    public static final int tag_accessibility_actions = 2643;

    @IdRes
    public static final int tag_accessibility_clickable_spans = 2644;

    @IdRes
    public static final int tag_accessibility_heading = 2645;

    @IdRes
    public static final int tag_accessibility_pane_title = 2646;

    @IdRes
    public static final int tag_screen_reader_focusable = 2647;

    @IdRes
    public static final int tag_transition_group = 2648;

    @IdRes
    public static final int tag_unhandled_key_event_manager = 2649;

    @IdRes
    public static final int tag_unhandled_key_listeners = 2650;

    @IdRes
    public static final int terms_and_text = 2651;

    @IdRes
    public static final int terrain = 2652;

    @IdRes
    public static final int text = 2653;

    @IdRes
    public static final int text2 = 2654;

    @IdRes
    public static final int textSpacerNoButtons = 2655;

    @IdRes
    public static final int textSpacerNoTitle = 2656;

    @IdRes
    public static final int textView_code = 2657;

    @IdRes
    public static final int textView_countryName = 2658;

    @IdRes
    public static final int textView_noresult = 2659;

    @IdRes
    public static final int textView_selectedCountry = 2660;

    @IdRes
    public static final int textView_title = 2661;

    @IdRes
    public static final int text_input_password_toggle = 2662;

    @IdRes
    public static final int textinput_counter = 2663;

    @IdRes
    public static final int textinput_error = 2664;

    @IdRes
    public static final int textinput_helper_text = 2665;

    @IdRes
    public static final int threeRegular = 2666;

    @IdRes
    public static final int time = 2667;

    @IdRes
    public static final int title = 2668;

    @IdRes
    public static final int titleDividerNoCustom = 2669;

    @IdRes
    public static final int title_template = 2670;

    @IdRes
    public static final int tl_add_source_card_number_ml = 2671;

    @IdRes
    public static final int tl_add_source_cvc_ml = 2672;

    @IdRes
    public static final int tl_add_source_expiry_ml = 2673;

    @IdRes
    public static final int tl_add_source_postal_ml = 2674;

    @IdRes
    public static final int tl_address_line1_aaw = 2675;

    @IdRes
    public static final int tl_address_line2_aaw = 2676;

    @IdRes
    public static final int tl_city_aaw = 2677;

    @IdRes
    public static final int tl_country_cat = 2678;

    @IdRes
    public static final int tl_name_aaw = 2679;

    @IdRes
    public static final int tl_phone_number_aaw = 2680;

    @IdRes
    public static final int tl_postal_code_aaw = 2681;

    @IdRes
    public static final int tl_state_aaw = 2682;

    @IdRes
    public static final int toPayAmount = 2683;

    @IdRes
    public static final int tooManyItemsLayout = 2684;

    @IdRes
    public static final int toolbar_as = 2685;

    @IdRes
    public static final int top = 2686;

    @IdRes
    public static final int topCard = 2687;

    @IdRes
    public static final int topLayout = 2688;

    @IdRes
    public static final int topPanel = 2689;

    @IdRes
    public static final int top_to_bottom = 2690;

    @IdRes
    public static final int totalAmount = 2691;

    @IdRes
    public static final int totalItem = 2692;

    @IdRes
    public static final int totalItems = 2693;

    @IdRes
    public static final int totalPrice = 2694;

    @IdRes
    public static final int touch_outside = 2695;

    @IdRes
    public static final int transition_current_scene = 2696;

    @IdRes
    public static final int transition_layout_save = 2697;

    @IdRes
    public static final int transition_position = 2698;

    @IdRes
    public static final int transition_scene_layoutid_cache = 2699;

    @IdRes
    public static final int transition_transform = 2700;

    @IdRes
    public static final int transparentLayout = 2701;

    @IdRes
    public static final int tv_add_source_error = 2702;

    @IdRes
    public static final int tv_amount_smv = 2703;

    @IdRes
    public static final int tv_detail_smv = 2704;

    @IdRes
    public static final int tv_label_smv = 2705;

    @IdRes
    public static final int tv_tab_title = 2706;

    @IdRes
    public static final int twoBold = 2707;

    @IdRes
    public static final int twoRegular = 2708;

    @IdRes
    public static final int twoSemiBold = 2709;

    @IdRes
    public static final int typeImage = 2710;

    @IdRes
    public static final int typeText = 2711;

    @IdRes
    public static final int unAvailableText = 2712;

    @IdRes
    public static final int unchecked = 2713;

    @IdRes
    public static final int uniform = 2714;

    @IdRes
    public static final int unknown = 2715;

    @IdRes
    public static final int unlabeled = 2716;

    @IdRes
    public static final int up = 2717;

    @IdRes
    public static final int useLogo = 2718;

    @IdRes
    public static final int vCircle = 2719;

    @IdRes
    public static final int vDotsView = 2720;

    @IdRes
    public static final int vegOnlySwitch = 2721;

    @IdRes
    public static final int veg_nv_icon = 2722;

    @IdRes
    public static final int vertical = 2723;

    @IdRes
    public static final int viewCart = 2724;

    @IdRes
    public static final int viewPager = 2725;

    @IdRes
    public static final int viewPagerContainer = 2726;

    @IdRes
    public static final int view_menu = 2727;

    @IdRes
    public static final int view_offset_helper = 2728;

    @IdRes
    public static final int visible = 2729;

    @IdRes
    public static final int visible_removing_fragment_view_tag = 2730;

    @IdRes
    public static final int walkThroughBgImage = 2731;

    @IdRes
    public static final int webView = 2732;

    @IdRes
    public static final int webview = 2733;

    @IdRes
    public static final int wide = 2734;

    @IdRes
    public static final int widget_viewstub_as = 2735;

    @IdRes
    public static final int window = 2736;

    @IdRes
    public static final int withText = 2737;

    @IdRes
    public static final int workBottomView = 2738;

    @IdRes
    public static final int workIcon = 2739;

    @IdRes
    public static final int workLayout = 2740;

    @IdRes
    public static final int workText = 2741;

    @IdRes
    public static final int wrap = 2742;

    @IdRes
    public static final int wrap_content = 2743;

    @IdRes
    public static final int yayText = 2744;

    @IdRes
    public static final int yesButton = 2745;
  }

  public static final class integer {
    @IntegerRes
    public static final int abc_config_activityDefaultDur = 2746;

    @IntegerRes
    public static final int abc_config_activityShortDur = 2747;

    @IntegerRes
    public static final int abc_max_action_buttons = 2748;

    @IntegerRes
    public static final int app_bar_elevation_anim_duration = 2749;

    @IntegerRes
    public static final int bottom_sheet_slide_duration = 2750;

    @IntegerRes
    public static final int cancel_button_image_alpha = 2751;

    @IntegerRes
    public static final int config_tooltipAnimTime = 2752;

    @IntegerRes
    public static final int date_length = 2753;

    @IntegerRes
    public static final int design_snackbar_text_max_lines = 2754;

    @IntegerRes
    public static final int design_tab_indicator_anim_duration_ms = 2755;

    @IntegerRes
    public static final int google_play_services_version = 2756;

    @IntegerRes
    public static final int hide_password_duration = 2757;

    @IntegerRes
    public static final int light_text_alpha_hex = 2758;

    @IntegerRes
    public static final int mtrl_btn_anim_delay_ms = 2759;

    @IntegerRes
    public static final int mtrl_btn_anim_duration_ms = 2760;

    @IntegerRes
    public static final int mtrl_chip_anim_duration = 2761;

    @IntegerRes
    public static final int mtrl_tab_indicator_anim_duration_ms = 2762;

    @IntegerRes
    public static final int show_password_duration = 2763;

    @IntegerRes
    public static final int status_bar_notification_info_maxnum = 2764;
  }

  public static final class layout {
    @LayoutRes
    public static final int abc_action_bar_title_item = 2765;

    @LayoutRes
    public static final int abc_action_bar_up_container = 2766;

    @LayoutRes
    public static final int abc_action_bar_view_list_nav_layout = 2767;

    @LayoutRes
    public static final int abc_action_menu_item_layout = 2768;

    @LayoutRes
    public static final int abc_action_menu_layout = 2769;

    @LayoutRes
    public static final int abc_action_mode_bar = 2770;

    @LayoutRes
    public static final int abc_action_mode_close_item_material = 2771;

    @LayoutRes
    public static final int abc_activity_chooser_view = 2772;

    @LayoutRes
    public static final int abc_activity_chooser_view_list_item = 2773;

    @LayoutRes
    public static final int abc_alert_dialog_button_bar_material = 2774;

    @LayoutRes
    public static final int abc_alert_dialog_material = 2775;

    @LayoutRes
    public static final int abc_alert_dialog_title_material = 2776;

    @LayoutRes
    public static final int abc_cascading_menu_item_layout = 2777;

    @LayoutRes
    public static final int abc_dialog_title_material = 2778;

    @LayoutRes
    public static final int abc_expanded_menu_layout = 2779;

    @LayoutRes
    public static final int abc_list_menu_item_checkbox = 2780;

    @LayoutRes
    public static final int abc_list_menu_item_icon = 2781;

    @LayoutRes
    public static final int abc_list_menu_item_layout = 2782;

    @LayoutRes
    public static final int abc_list_menu_item_radio = 2783;

    @LayoutRes
    public static final int abc_popup_menu_header_item_layout = 2784;

    @LayoutRes
    public static final int abc_popup_menu_item_layout = 2785;

    @LayoutRes
    public static final int abc_screen_content_include = 2786;

    @LayoutRes
    public static final int abc_screen_simple = 2787;

    @LayoutRes
    public static final int abc_screen_simple_overlay_action_mode = 2788;

    @LayoutRes
    public static final int abc_screen_toolbar = 2789;

    @LayoutRes
    public static final int abc_search_dropdown_item_icons_2line = 2790;

    @LayoutRes
    public static final int abc_search_view = 2791;

    @LayoutRes
    public static final int abc_select_dialog_material = 2792;

    @LayoutRes
    public static final int abc_tooltip = 2793;

    @LayoutRes
    public static final int activity_add_source = 2794;

    @LayoutRes
    public static final int activity_apply_coupon = 2795;

    @LayoutRes
    public static final int activity_backtomain = 2796;

    @LayoutRes
    public static final int activity_choose_location = 2797;

    @LayoutRes
    public static final int activity_enter_shipping_info = 2798;

    @LayoutRes
    public static final int activity_forgot_password = 2799;

    @LayoutRes
    public static final int activity_live_tracking = 2800;

    @LayoutRes
    public static final int activity_main = 2801;

    @LayoutRes
    public static final int activity_manage_address = 2802;

    @LayoutRes
    public static final int activity_order_detail = 2803;

    @LayoutRes
    public static final int activity_payment_method = 2804;

    @LayoutRes
    public static final int activity_payment_methods = 2805;

    @LayoutRes
    public static final int activity_rating = 2806;

    @LayoutRes
    public static final int activity_restaurant_details = 2807;

    @LayoutRes
    public static final int activity_save_location = 2808;

    @LayoutRes
    public static final int activity_select_shipping_method = 2809;

    @LayoutRes
    public static final int activity_shipping_flow = 2810;

    @LayoutRes
    public static final int activity_sign_in = 2811;

    @LayoutRes
    public static final int activity_sign_up = 2812;

    @LayoutRes
    public static final int activity_splash = 2813;

    @LayoutRes
    public static final int activity_stripe = 2814;

    @LayoutRes
    public static final int activity_tutorial = 2815;

    @LayoutRes
    public static final int activity_validate_otp = 2816;

    @LayoutRes
    public static final int activity_view_cart = 2817;

    @LayoutRes
    public static final int activity_walk_through = 2818;

    @LayoutRes
    public static final int activity_web_view = 2819;

    @LayoutRes
    public static final int add_address_widget = 2820;

    @LayoutRes
    public static final int amu_info_window = 2821;

    @LayoutRes
    public static final int amu_text_bubble = 2822;

    @LayoutRes
    public static final int amu_webview = 2823;

    @LayoutRes
    public static final int browser_actions_context_menu_page = 2824;

    @LayoutRes
    public static final int browser_actions_context_menu_row = 2825;

    @LayoutRes
    public static final int card_input_widget = 2826;

    @LayoutRes
    public static final int card_multiline_widget = 2827;

    @LayoutRes
    public static final int change_password = 2828;

    @LayoutRes
    public static final int clear_cart_dialog = 2829;

    @LayoutRes
    public static final int com_facebook_activity_layout = 2830;

    @LayoutRes
    public static final int com_facebook_device_auth_dialog_fragment = 2831;

    @LayoutRes
    public static final int com_facebook_login_fragment = 2832;

    @LayoutRes
    public static final int com_facebook_smart_device_dialog_fragment = 2833;

    @LayoutRes
    public static final int com_facebook_tooltip_bubble = 2834;

    @LayoutRes
    public static final int connection_lost_retry_layout = 2835;

    @LayoutRes
    public static final int country_autocomplete_textview = 2836;

    @LayoutRes
    public static final int coupon_dialog = 2837;

    @LayoutRes
    public static final int custom_dialog = 2838;

    @LayoutRes
    public static final int customizable_dialog = 2839;

    @LayoutRes
    public static final int customizable_dish_element = 2840;

    @LayoutRes
    public static final int customizable_dish_multiple_item = 2841;

    @LayoutRes
    public static final int customizable_dish_single_item = 2842;

    @LayoutRes
    public static final int decrease_customizable_dialog = 2843;

    @LayoutRes
    public static final int deliver_address_layout_view = 2844;

    @LayoutRes
    public static final int delivery_address_item = 2845;

    @LayoutRes
    public static final int design_bottom_navigation_item = 2846;

    @LayoutRes
    public static final int design_bottom_sheet_dialog = 2847;

    @LayoutRes
    public static final int design_layout_snackbar = 2848;

    @LayoutRes
    public static final int design_layout_snackbar_include = 2849;

    @LayoutRes
    public static final int design_layout_tab_icon = 2850;

    @LayoutRes
    public static final int design_layout_tab_text = 2851;

    @LayoutRes
    public static final int design_menu_item_action_area = 2852;

    @LayoutRes
    public static final int design_navigation_item = 2853;

    @LayoutRes
    public static final int design_navigation_item_header = 2854;

    @LayoutRes
    public static final int design_navigation_item_separator = 2855;

    @LayoutRes
    public static final int design_navigation_item_subheader = 2856;

    @LayoutRes
    public static final int design_navigation_menu = 2857;

    @LayoutRes
    public static final int design_navigation_menu_item = 2858;

    @LayoutRes
    public static final int design_text_input_password_icon = 2859;

    @LayoutRes
    public static final int dish_category_item = 2860;

    @LayoutRes
    public static final int dish_category_item_loading_layout = 2861;

    @LayoutRes
    public static final int dish_item = 2862;

    @LayoutRes
    public static final int dish_item_loading_layout = 2863;

    @LayoutRes
    public static final int dish_sub_category_item = 2864;

    @LayoutRes
    public static final int dishes_detail_explore_item = 2865;

    @LayoutRes
    public static final int dishes_explore_item = 2866;

    @LayoutRes
    public static final int explore_content_layout = 2867;

    @LayoutRes
    public static final int explore_locale_content_layout = 2868;

    @LayoutRes
    public static final int extra_charges_adapter = 2869;

    @LayoutRes
    public static final int fastscroll__default_bubble = 2870;

    @LayoutRes
    public static final int fragment_account = 2871;

    @LayoutRes
    public static final int fragment_dishes_explore = 2872;

    @LayoutRes
    public static final int fragment_explore = 2873;

    @LayoutRes
    public static final int fragment_mycart = 2874;

    @LayoutRes
    public static final int fragment_near_me = 2875;

    @LayoutRes
    public static final int fragment_walk_through = 2876;

    @LayoutRes
    public static final int home_banner_items = 2877;

    @LayoutRes
    public static final int home_content_layout = 2878;

    @LayoutRes
    public static final int home_loading_layout = 2879;

    @LayoutRes
    public static final int layout_code_picker = 2880;

    @LayoutRes
    public static final int layout_full_width_code_picker = 2881;

    @LayoutRes
    public static final int layout_picker_dialog = 2882;

    @LayoutRes
    public static final int layout_recycler_country_tile = 2883;

    @LayoutRes
    public static final int layout_sample_view = 2884;

    @LayoutRes
    public static final int layout_spark_button = 2885;

    @LayoutRes
    public static final int layout_tab = 2886;

    @LayoutRes
    public static final int layout_tab_bottom = 2887;

    @LayoutRes
    public static final int layout_tab_left = 2888;

    @LayoutRes
    public static final int layout_tab_right = 2889;

    @LayoutRes
    public static final int layout_tab_segment = 2890;

    @LayoutRes
    public static final int layout_tab_top = 2891;

    @LayoutRes
    public static final int livetracking_content = 2892;

    @LayoutRes
    public static final int livetracking_loading = 2893;

    @LayoutRes
    public static final int location_item = 2894;

    @LayoutRes
    public static final int login_bottom_sheet = 2895;

    @LayoutRes
    public static final int manage_address_content_layout = 2896;

    @LayoutRes
    public static final int manage_address_item = 2897;

    @LayoutRes
    public static final int manage_address_loading_layout = 2898;

    @LayoutRes
    public static final int map_info_window = 2899;

    @LayoutRes
    public static final int masked_card_row = 2900;

    @LayoutRes
    public static final int masked_card_view = 2901;

    @LayoutRes
    public static final int menu_adapter_item = 2902;

    @LayoutRes
    public static final int menu_text_view = 2903;

    @LayoutRes
    public static final int mtrl_layout_snackbar = 2904;

    @LayoutRes
    public static final int mtrl_layout_snackbar_include = 2905;

    @LayoutRes
    public static final int my_cart_content_layout = 2906;

    @LayoutRes
    public static final int mycart_loading_layout = 2907;

    @LayoutRes
    public static final int no_address_layout_view = 2908;

    @LayoutRes
    public static final int no_cart_items_layout = 2909;

    @LayoutRes
    public static final int notification_action = 2910;

    @LayoutRes
    public static final int notification_action_tombstone = 2911;

    @LayoutRes
    public static final int notification_media_action = 2912;

    @LayoutRes
    public static final int notification_media_cancel_action = 2913;

    @LayoutRes
    public static final int notification_template_big_media = 2914;

    @LayoutRes
    public static final int notification_template_big_media_custom = 2915;

    @LayoutRes
    public static final int notification_template_big_media_narrow = 2916;

    @LayoutRes
    public static final int notification_template_big_media_narrow_custom = 2917;

    @LayoutRes
    public static final int notification_template_custom_big = 2918;

    @LayoutRes
    public static final int notification_template_icon_group = 2919;

    @LayoutRes
    public static final int notification_template_lines = 2920;

    @LayoutRes
    public static final int notification_template_lines_media = 2921;

    @LayoutRes
    public static final int notification_template_media = 2922;

    @LayoutRes
    public static final int notification_template_media_custom = 2923;

    @LayoutRes
    public static final int notification_template_part_chronometer = 2924;

    @LayoutRes
    public static final int notification_template_part_time = 2925;

    @LayoutRes
    public static final int order_confirm_loading_dialog = 2926;

    @LayoutRes
    public static final int outlet_items = 2927;

    @LayoutRes
    public static final int past_order_charges_item = 2928;

    @LayoutRes
    public static final int past_order_dishes_item = 2929;

    @LayoutRes
    public static final int past_order_item = 2930;

    @LayoutRes
    public static final int payment_method_content_layout = 2931;

    @LayoutRes
    public static final int payment_method_item = 2932;

    @LayoutRes
    public static final int place_autocomplete_fragment = 2933;

    @LayoutRes
    public static final int place_autocomplete_item_powered_by_google = 2934;

    @LayoutRes
    public static final int place_autocomplete_item_prediction = 2935;

    @LayoutRes
    public static final int place_autocomplete_progress = 2936;

    @LayoutRes
    public static final int rating_layout = 2937;

    @LayoutRes
    public static final int recent_search_item = 2938;

    @LayoutRes
    public static final int recommended_dish_item = 2939;

    @LayoutRes
    public static final int repeat_customizable_dialog = 2940;

    @LayoutRes
    public static final int restaurant_details_content_layout = 2941;

    @LayoutRes
    public static final int restaurant_details_loading_layout = 2942;

    @LayoutRes
    public static final int restaurant_item = 2943;

    @LayoutRes
    public static final int restaurant_item_2 = 2944;

    @LayoutRes
    public static final int rvp_fragment_container = 2945;

    @LayoutRes
    public static final int select_dialog_item_material = 2946;

    @LayoutRes
    public static final int select_dialog_multichoice_material = 2947;

    @LayoutRes
    public static final int select_dialog_singlechoice_material = 2948;

    @LayoutRes
    public static final int select_shipping_method_widget = 2949;

    @LayoutRes
    public static final int selected_customization_dish_item = 2950;

    @LayoutRes
    public static final int set_delivery_location_dialog = 2951;

    @LayoutRes
    public static final int set_delivery_location_layout = 2952;

    @LayoutRes
    public static final int shipping_method_view = 2953;

    @LayoutRes
    public static final int support_simple_spinner_dropdown_item = 2954;

    @LayoutRes
    public static final int test_layout = 2955;

    @LayoutRes
    public static final int to_delete = 2956;

    @LayoutRes
    public static final int too_many_items_view = 2957;

    @LayoutRes
    public static final int tooltip = 2958;

    @LayoutRes
    public static final int view_cart_dishes_item = 2959;

    @LayoutRes
    public static final int viewholder_shimmer = 2960;
  }

  public static final class menu {
    @MenuRes
    public static final int add_source_menu = 2961;

    @MenuRes
    public static final int menu = 2962;
  }

  public static final class plurals {
    @PluralsRes
    public static final int joda_time_android_abbrev_in_num_days = 2963;

    @PluralsRes
    public static final int joda_time_android_abbrev_in_num_hours = 2964;

    @PluralsRes
    public static final int joda_time_android_abbrev_in_num_minutes = 2965;

    @PluralsRes
    public static final int joda_time_android_abbrev_in_num_seconds = 2966;

    @PluralsRes
    public static final int joda_time_android_abbrev_num_days_ago = 2967;

    @PluralsRes
    public static final int joda_time_android_abbrev_num_hours_ago = 2968;

    @PluralsRes
    public static final int joda_time_android_abbrev_num_minutes_ago = 2969;

    @PluralsRes
    public static final int joda_time_android_abbrev_num_seconds_ago = 2970;

    @PluralsRes
    public static final int joda_time_android_duration_hours = 2971;

    @PluralsRes
    public static final int joda_time_android_duration_minutes = 2972;

    @PluralsRes
    public static final int joda_time_android_duration_seconds = 2973;

    @PluralsRes
    public static final int joda_time_android_in_num_days = 2974;

    @PluralsRes
    public static final int joda_time_android_in_num_hours = 2975;

    @PluralsRes
    public static final int joda_time_android_in_num_minutes = 2976;

    @PluralsRes
    public static final int joda_time_android_in_num_seconds = 2977;

    @PluralsRes
    public static final int joda_time_android_num_days_ago = 2978;

    @PluralsRes
    public static final int joda_time_android_num_hours_ago = 2979;

    @PluralsRes
    public static final int joda_time_android_num_minutes_ago = 2980;

    @PluralsRes
    public static final int joda_time_android_num_seconds_ago = 2981;
  }

  public static final class string {
    @StringRes
    public static final int PORT_NUMBER = 2982;

    @StringRes
    public static final int STRIPE_KEY = 2983;

    @StringRes
    public static final int abc_action_bar_home_description = 2984;

    @StringRes
    public static final int abc_action_bar_home_description_format = 2985;

    @StringRes
    public static final int abc_action_bar_home_subtitle_description_format = 2986;

    @StringRes
    public static final int abc_action_bar_up_description = 2987;

    @StringRes
    public static final int abc_action_menu_overflow_description = 2988;

    @StringRes
    public static final int abc_action_mode_done = 2989;

    @StringRes
    public static final int abc_activity_chooser_view_see_all = 2990;

    @StringRes
    public static final int abc_activitychooserview_choose_application = 2991;

    @StringRes
    public static final int abc_capital_off = 2992;

    @StringRes
    public static final int abc_capital_on = 2993;

    @StringRes
    public static final int abc_font_family_body_1_material = 2994;

    @StringRes
    public static final int abc_font_family_body_2_material = 2995;

    @StringRes
    public static final int abc_font_family_button_material = 2996;

    @StringRes
    public static final int abc_font_family_caption_material = 2997;

    @StringRes
    public static final int abc_font_family_display_1_material = 2998;

    @StringRes
    public static final int abc_font_family_display_2_material = 2999;

    @StringRes
    public static final int abc_font_family_display_3_material = 3000;

    @StringRes
    public static final int abc_font_family_display_4_material = 3001;

    @StringRes
    public static final int abc_font_family_headline_material = 3002;

    @StringRes
    public static final int abc_font_family_menu_material = 3003;

    @StringRes
    public static final int abc_font_family_subhead_material = 3004;

    @StringRes
    public static final int abc_font_family_title_material = 3005;

    @StringRes
    public static final int abc_menu_alt_shortcut_label = 3006;

    @StringRes
    public static final int abc_menu_ctrl_shortcut_label = 3007;

    @StringRes
    public static final int abc_menu_delete_shortcut_label = 3008;

    @StringRes
    public static final int abc_menu_enter_shortcut_label = 3009;

    @StringRes
    public static final int abc_menu_function_shortcut_label = 3010;

    @StringRes
    public static final int abc_menu_meta_shortcut_label = 3011;

    @StringRes
    public static final int abc_menu_shift_shortcut_label = 3012;

    @StringRes
    public static final int abc_menu_space_shortcut_label = 3013;

    @StringRes
    public static final int abc_menu_sym_shortcut_label = 3014;

    @StringRes
    public static final int abc_prepend_shortcut_label = 3015;

    @StringRes
    public static final int abc_search_hint = 3016;

    @StringRes
    public static final int abc_searchview_description_clear = 3017;

    @StringRes
    public static final int abc_searchview_description_query = 3018;

    @StringRes
    public static final int abc_searchview_description_search = 3019;

    @StringRes
    public static final int abc_searchview_description_submit = 3020;

    @StringRes
    public static final int abc_searchview_description_voice = 3021;

    @StringRes
    public static final int abc_shareactionprovider_share_with = 3022;

    @StringRes
    public static final int abc_shareactionprovider_share_with_application = 3023;

    @StringRes
    public static final int abc_toolbar_collapse_description = 3024;

    @StringRes
    public static final int acc_label_card_number = 3025;

    @StringRes
    public static final int acc_label_expiry_date = 3026;

    @StringRes
    public static final int acc_label_zip = 3027;

    @StringRes
    public static final int acc_label_zip_short = 3028;

    @StringRes
    public static final int account = 3029;

    @StringRes
    public static final int add = 3030;

    @StringRes
    public static final int add_card = 3031;

    @StringRes
    public static final int add_item = 3032;

    @StringRes
    public static final int add_new_addresses = 3033;

    @StringRes
    public static final int addressPaymentsEtc = 3034;

    @StringRes
    public static final int address_city_required = 3035;

    @StringRes
    public static final int address_county_required = 3036;

    @StringRes
    public static final int address_label_address = 3037;

    @StringRes
    public static final int address_label_address_line1 = 3038;

    @StringRes
    public static final int address_label_address_line1_optional = 3039;

    @StringRes
    public static final int address_label_address_line2 = 3040;

    @StringRes
    public static final int address_label_address_line2_optional = 3041;

    @StringRes
    public static final int address_label_address_optional = 3042;

    @StringRes
    public static final int address_label_apt = 3043;

    @StringRes
    public static final int address_label_apt_optional = 3044;

    @StringRes
    public static final int address_label_city = 3045;

    @StringRes
    public static final int address_label_city_optional = 3046;

    @StringRes
    public static final int address_label_country = 3047;

    @StringRes
    public static final int address_label_county = 3048;

    @StringRes
    public static final int address_label_county_optional = 3049;

    @StringRes
    public static final int address_label_name = 3050;

    @StringRes
    public static final int address_label_name_optional = 3051;

    @StringRes
    public static final int address_label_phone_number = 3052;

    @StringRes
    public static final int address_label_phone_number_optional = 3053;

    @StringRes
    public static final int address_label_postal_code = 3054;

    @StringRes
    public static final int address_label_postal_code_optional = 3055;

    @StringRes
    public static final int address_label_postcode = 3056;

    @StringRes
    public static final int address_label_postcode_optional = 3057;

    @StringRes
    public static final int address_label_province = 3058;

    @StringRes
    public static final int address_label_province_optional = 3059;

    @StringRes
    public static final int address_label_region_generic = 3060;

    @StringRes
    public static final int address_label_region_generic_optional = 3061;

    @StringRes
    public static final int address_label_state = 3062;

    @StringRes
    public static final int address_label_state_optional = 3063;

    @StringRes
    public static final int address_label_zip_code = 3064;

    @StringRes
    public static final int address_label_zip_code_optional = 3065;

    @StringRes
    public static final int address_label_zip_postal_code = 3066;

    @StringRes
    public static final int address_label_zip_postal_code_optional = 3067;

    @StringRes
    public static final int address_name_required = 3068;

    @StringRes
    public static final int address_phone_number_required = 3069;

    @StringRes
    public static final int address_postal_code_invalid = 3070;

    @StringRes
    public static final int address_postcode_invalid = 3071;

    @StringRes
    public static final int address_province_required = 3072;

    @StringRes
    public static final int address_region_generic_required = 3073;

    @StringRes
    public static final int address_required = 3074;

    @StringRes
    public static final int address_shipping_address = 3075;

    @StringRes
    public static final int address_state_required = 3076;

    @StringRes
    public static final int address_zip_invalid = 3077;

    @StringRes
    public static final int address_zip_postal_invalid = 3078;

    @StringRes
    public static final int amex_short = 3079;

    @StringRes
    public static final int anySuggestions = 3080;

    @StringRes
    public static final int app_name = 3081;

    @StringRes
    public static final int appbar_scrolling_view_behavior = 3082;

    @StringRes
    public static final int apply = 3083;

    @StringRes
    public static final int apply_coupon = 3084;

    @StringRes
    public static final int arriving_in = 3085;

    @StringRes
    public static final int authentication_error = 3086;

    @StringRes
    public static final int awesome_thanks = 3087;

    @StringRes
    public static final int billDetail = 3088;

    @StringRes
    public static final int bottom_sheet_behavior = 3089;

    @StringRes
    public static final int by = 3090;

    @StringRes
    public static final int by_creating_account = 3091;

    @StringRes
    public static final int cancelled = 3092;

    @StringRes
    public static final int card_number_hint = 3093;

    @StringRes
    public static final int cart = 3094;

    @StringRes
    public static final int change = 3095;

    @StringRes
    public static final int change_password = 3096;

    @StringRes
    public static final int character_counter_content_description = 3097;

    @StringRes
    public static final int character_counter_pattern = 3098;

    @StringRes
    public static final int choose_delivery_address = 3099;

    @StringRes
    public static final int choose_type = 3100;

    @StringRes
    public static final int clear_cart = 3101;

    @StringRes
    public static final int clear_cart_description = 3102;

    @StringRes
    public static final int clear_cart_title = 3103;

    @StringRes
    public static final int com_facebook_device_auth_instructions = 3104;

    @StringRes
    public static final int com_facebook_image_download_unknown_error = 3105;

    @StringRes
    public static final int com_facebook_internet_permission_error_message = 3106;

    @StringRes
    public static final int com_facebook_internet_permission_error_title = 3107;

    @StringRes
    public static final int com_facebook_like_button_liked = 3108;

    @StringRes
    public static final int com_facebook_like_button_not_liked = 3109;

    @StringRes
    public static final int com_facebook_loading = 3110;

    @StringRes
    public static final int com_facebook_loginview_cancel_action = 3111;

    @StringRes
    public static final int com_facebook_loginview_log_in_button = 3112;

    @StringRes
    public static final int com_facebook_loginview_log_in_button_continue = 3113;

    @StringRes
    public static final int com_facebook_loginview_log_in_button_long = 3114;

    @StringRes
    public static final int com_facebook_loginview_log_out_action = 3115;

    @StringRes
    public static final int com_facebook_loginview_log_out_button = 3116;

    @StringRes
    public static final int com_facebook_loginview_logged_in_as = 3117;

    @StringRes
    public static final int com_facebook_loginview_logged_in_using_facebook = 3118;

    @StringRes
    public static final int com_facebook_send_button_text = 3119;

    @StringRes
    public static final int com_facebook_share_button_text = 3120;

    @StringRes
    public static final int com_facebook_smart_device_instructions = 3121;

    @StringRes
    public static final int com_facebook_smart_device_instructions_or = 3122;

    @StringRes
    public static final int com_facebook_smart_login_confirmation_cancel = 3123;

    @StringRes
    public static final int com_facebook_smart_login_confirmation_continue_as = 3124;

    @StringRes
    public static final int com_facebook_smart_login_confirmation_title = 3125;

    @StringRes
    public static final int com_facebook_tooltip_default = 3126;

    @StringRes
    public static final int common_google_play_services_enable_button = 3127;

    @StringRes
    public static final int common_google_play_services_enable_text = 3128;

    @StringRes
    public static final int common_google_play_services_enable_title = 3129;

    @StringRes
    public static final int common_google_play_services_install_button = 3130;

    @StringRes
    public static final int common_google_play_services_install_text = 3131;

    @StringRes
    public static final int common_google_play_services_install_text_phone = 3132;

    @StringRes
    public static final int common_google_play_services_install_text_tablet = 3133;

    @StringRes
    public static final int common_google_play_services_install_title = 3134;

    @StringRes
    public static final int common_google_play_services_notification_channel_name = 3135;

    @StringRes
    public static final int common_google_play_services_notification_ticker = 3136;

    @StringRes
    public static final int common_google_play_services_unknown_issue = 3137;

    @StringRes
    public static final int common_google_play_services_unsupported_text = 3138;

    @StringRes
    public static final int common_google_play_services_unsupported_title = 3139;

    @StringRes
    public static final int common_google_play_services_update_button = 3140;

    @StringRes
    public static final int common_google_play_services_update_text = 3141;

    @StringRes
    public static final int common_google_play_services_update_title = 3142;

    @StringRes
    public static final int common_google_play_services_updating_text = 3143;

    @StringRes
    public static final int common_google_play_services_updating_title = 3144;

    @StringRes
    public static final int common_google_play_services_wear_update_text = 3145;

    @StringRes
    public static final int common_open_on_phone = 3146;

    @StringRes
    public static final int common_signin_button_text = 3147;

    @StringRes
    public static final int common_signin_button_text_long = 3148;

    @StringRes
    public static final int confirm_new_password = 3149;

    @StringRes
    public static final int connection_error = 3150;

    @StringRes
    public static final int connection_error_descri = 3151;

    @StringRes
    public static final int continue_text = 3152;

    @StringRes
    public static final int country_code_default = 3153;

    @StringRes
    public static final int cuisine_type = 3154;

    @StringRes
    public static final int currency_unit = 3155;

    @StringRes
    public static final int current_location = 3156;

    @StringRes
    public static final int customization_available = 3157;

    @StringRes
    public static final int customized = 3158;

    @StringRes
    public static final int cvc_amex_hint = 3159;

    @StringRes
    public static final int cvc_multiline_helper = 3160;

    @StringRes
    public static final int cvc_multiline_helper_amex = 3161;

    @StringRes
    public static final int cvc_number_hint = 3162;

    @StringRes
    public static final int decrease_customization_detail = 3163;

    @StringRes
    public static final int define_roundedimageview = 3164;

    @StringRes
    public static final int dishes = 3165;

    @StringRes
    public static final int dishes_not_available = 3166;

    @StringRes
    public static final int dummy_addresss = 3167;

    @StringRes
    public static final int dummy_outlets = 3168;

    @StringRes
    public static final int dummy_rating = 3169;

    @StringRes
    public static final int dummy_restaurant_name = 3170;

    @StringRes
    public static final int dummy_restaurant_offer_value = 3171;

    @StringRes
    public static final int dummy_restaurant_price = 3172;

    @StringRes
    public static final int dummy_restaurant_rating = 3173;

    @StringRes
    public static final int dummy_restaurant_time = 3174;

    @StringRes
    public static final int dummy_time = 3175;

    @StringRes
    public static final int edit = 3176;

    @StringRes
    public static final int edit_account = 3177;

    @StringRes
    public static final int email_address_all_caps = 3178;

    @StringRes
    public static final int ending_in = 3179;

    @StringRes
    public static final int enter_coupon = 3180;

    @StringRes
    public static final int enter_house_flat_no = 3181;

    @StringRes
    public static final int enter_landmark = 3182;

    @StringRes
    public static final int enter_location = 3183;

    @StringRes
    public static final int enter_name = 3184;

    @StringRes
    public static final int enter_otp_caps_all = 3185;

    @StringRes
    public static final int enter_password = 3186;

    @StringRes
    public static final int enter_valid_email = 3187;

    @StringRes
    public static final int enter_valid_otp = 3188;

    @StringRes
    public static final int enter_valid_password = 3189;

    @StringRes
    public static final int enter_your_phone_number_to_proceed = 3190;

    @StringRes
    public static final int example_other_name = 3191;

    @StringRes
    public static final int expiry_date_hint = 3192;

    @StringRes
    public static final int expiry_label_short = 3193;

    @StringRes
    public static final int explore = 3194;

    @StringRes
    public static final int explore_hint = 3195;

    @StringRes
    public static final int extraChargesMayApply = 3196;

    @StringRes
    public static final int fab_transformation_scrim_behavior = 3197;

    @StringRes
    public static final int fab_transformation_sheet_behavior = 3198;

    @StringRes
    public static final int facebook = 3199;

    @StringRes
    public static final int facebook_app_id = 3200;

    @StringRes
    public static final int faq_and_address = 3201;

    @StringRes
    public static final int fastscroll__app_name = 3202;

    @StringRes
    public static final int favourites = 3203;

    @StringRes
    public static final int fb_login_protocol_scheme = 3204;

    @StringRes
    public static final int fcm_fallback_notification_channel_label = 3205;

    @StringRes
    public static final int filter = 3206;

    @StringRes
    public static final int forgot_passowrd_all_caps = 3207;

    @StringRes
    public static final int forgotall_caps = 3208;

    @StringRes
    public static final int from = 3209;

    @StringRes
    public static final int giveYourValuableFeedback = 3210;

    @StringRes
    public static final int google = 3211;

    @StringRes
    public static final int hello_blank_fragment = 3212;

    @StringRes
    public static final int help = 3213;

    @StringRes
    public static final int hide_bottom_view_on_scroll_behavior = 3214;

    @StringRes
    public static final int home = 3215;

    @StringRes
    public static final int house_flat_number = 3216;

    @StringRes
    public static final int i_have_a_referrral = 3217;

    @StringRes
    public static final int i_will_choose = 3218;

    @StringRes
    public static final int invalid_card_number = 3219;

    @StringRes
    public static final int invalid_cvc = 3220;

    @StringRes
    public static final int invalid_expiry_month = 3221;

    @StringRes
    public static final int invalid_expiry_year = 3222;

    @StringRes
    public static final int invalid_shipping_information = 3223;

    @StringRes
    public static final int invalid_zip = 3224;

    @StringRes
    public static final int itemTotal = 3225;

    @StringRes
    public static final int items = 3226;

    @StringRes
    public static final int joda_time_android_date_time = 3227;

    @StringRes
    public static final int joda_time_android_preposition_for_date = 3228;

    @StringRes
    public static final int joda_time_android_preposition_for_time = 3229;

    @StringRes
    public static final int joda_time_android_relative_time = 3230;

    @StringRes
    public static final int landmark = 3231;

    @StringRes
    public static final int landmark_optional = 3232;

    @StringRes
    public static final int library_roundedimageview_author = 3233;

    @StringRes
    public static final int library_roundedimageview_authorWebsite = 3234;

    @StringRes
    public static final int library_roundedimageview_isOpenSource = 3235;

    @StringRes
    public static final int library_roundedimageview_libraryDescription = 3236;

    @StringRes
    public static final int library_roundedimageview_libraryName = 3237;

    @StringRes
    public static final int library_roundedimageview_libraryVersion = 3238;

    @StringRes
    public static final int library_roundedimageview_libraryWebsite = 3239;

    @StringRes
    public static final int library_roundedimageview_licenseId = 3240;

    @StringRes
    public static final int library_roundedimageview_repositoryLink = 3241;

    @StringRes
    public static final int locating = 3242;

    @StringRes
    public static final int location = 3243;

    @StringRes
    public static final int login = 3244;

    @StringRes
    public static final int login_content_text = 3245;

    @StringRes
    public static final int logout = 3246;

    @StringRes
    public static final int manage_address = 3247;

    @StringRes
    public static final int map_key = 3248;

    @StringRes
    public static final int menu = 3249;

    @StringRes
    public static final int min = 3250;

    @StringRes
    public static final int minus = 3251;

    @StringRes
    public static final int minutes = 3252;

    @StringRes
    public static final int more = 3253;

    @StringRes
    public static final int mtrl_chip_close_icon_content_description = 3254;

    @StringRes
    public static final int my_account = 3255;

    @StringRes
    public static final int name_all_caps = 3256;

    @StringRes
    public static final int near_you = 3257;

    @StringRes
    public static final int network_error = 3258;

    @StringRes
    public static final int new_password = 3259;

    @StringRes
    public static final int no = 3260;

    @StringRes
    public static final int no_address_add_address = 3261;

    @StringRes
    public static final int no_cart_items = 3262;

    @StringRes
    public static final int no_cart_items_title = 3263;

    @StringRes
    public static final int no_connection = 3264;

    @StringRes
    public static final int no_internet_connection = 3265;

    @StringRes
    public static final int no_payment_methods = 3266;

    @StringRes
    public static final int no_restaurant_available = 3267;

    @StringRes
    public static final int no_restaurant_available_location = 3268;

    @StringRes
    public static final int no_restaurant_match = 3269;

    @StringRes
    public static final int no_result_found = 3270;

    @StringRes
    public static final int normal = 3271;

    @StringRes
    public static final int offers = 3272;

    @StringRes
    public static final int ok = 3273;

    @StringRes
    public static final int oops = 3274;

    @StringRes
    public static final int orderConfirmed = 3275;

    @StringRes
    public static final int orderDelivered = 3276;

    @StringRes
    public static final int orderPickedUp = 3277;

    @StringRes
    public static final int orderReceived = 3278;

    @StringRes
    public static final int orderRejected = 3279;

    @StringRes
    public static final int orderStatus = 3280;

    @StringRes
    public static final int orderStatusDescri = 3281;

    @StringRes
    public static final int orders = 3282;

    @StringRes
    public static final int other = 3283;

    @StringRes
    public static final int otp_sent_to = 3284;

    @StringRes
    public static final int outlet_count_template = 3285;

    @StringRes
    public static final int parse_error = 3286;

    @StringRes
    public static final int passowrd_cant_be_empty = 3287;

    @StringRes
    public static final int passowrd_mismatch = 3288;

    @StringRes
    public static final int password_caps_all = 3289;

    @StringRes
    public static final int password_toggle_content_description = 3290;

    @StringRes
    public static final int pastOrders = 3291;

    @StringRes
    public static final int path_password_eye = 3292;

    @StringRes
    public static final int path_password_eye_mask_strike_through = 3293;

    @StringRes
    public static final int path_password_eye_mask_visible = 3294;

    @StringRes
    public static final int path_password_strike_through = 3295;

    @StringRes
    public static final int payment_method_add_new_card = 3296;

    @StringRes
    public static final int payments = 3297;

    @StringRes
    public static final int phone_number_caps_all = 3298;

    @StringRes
    public static final int place_autocomplete_clear_button = 3299;

    @StringRes
    public static final int place_autocomplete_search_hint = 3300;

    @StringRes
    public static final int please_enter_other = 3301;

    @StringRes
    public static final int please_remove_items = 3302;

    @StringRes
    public static final int plus = 3303;

    @StringRes
    public static final int price_free = 3304;

    @StringRes
    public static final int price_text = 3305;

    @StringRes
    public static final int proceed_to_pay = 3306;

    @StringRes
    public static final int rate_your_delivery = 3307;

    @StringRes
    public static final int rate_your_food = 3308;

    @StringRes
    public static final int rating = 3309;

    @StringRes
    public static final int recentSearches = 3310;

    @StringRes
    public static final int refer_and_earn = 3311;

    @StringRes
    public static final int referral_code = 3312;

    @StringRes
    public static final int related_to = 3313;

    @StringRes
    public static final int repeat = 3314;

    @StringRes
    public static final int resendOtp = 3315;

    @StringRes
    public static final int resendOtpDescription = 3316;

    @StringRes
    public static final int restaurant = 3317;

    @StringRes
    public static final int restaurant_count_text = 3318;

    @StringRes
    public static final int restaurant_for_two_template = 3319;

    @StringRes
    public static final int resturant_Rating = 3320;

    @StringRes
    public static final int retry = 3321;

    @StringRes
    public static final int save = 3322;

    @StringRes
    public static final int save_as = 3323;

    @StringRes
    public static final int saved_address = 3324;

    @StringRes
    public static final int saved_addresses = 3325;

    @StringRes
    public static final int savings_with_this_coupon = 3326;

    @StringRes
    public static final int search_for_area = 3327;

    @StringRes
    public static final int search_hint = 3328;

    @StringRes
    public static final int search_menu_title = 3329;

    @StringRes
    public static final int sec = 3330;

    @StringRes
    public static final int seconds = 3331;

    @StringRes
    public static final int select_country = 3332;

    @StringRes
    public static final int server_error = 3333;

    @StringRes
    public static final int set_delivery_location = 3334;

    @StringRes
    public static final int showMore = 3335;

    @StringRes
    public static final int sign_up = 3336;

    @StringRes
    public static final int social = 3337;

    @StringRes
    public static final int something_went_wrong = 3338;

    @StringRes
    public static final int splash_text = 3339;

    @StringRes
    public static final int status_bar_notification_info_overflow = 3340;

    @StringRes
    public static final int submit_feedback = 3341;

    @StringRes
    public static final int tag_as = 3342;

    @StringRes
    public static final int tell_us_u_r_suggestions = 3343;

    @StringRes
    public static final int temp_address = 3344;

    @StringRes
    public static final int terms_and_conditions = 3345;

    @StringRes
    public static final int thanks_for_rating = 3346;

    @StringRes
    public static final int time_text = 3347;

    @StringRes
    public static final int title_add_a_card = 3348;

    @StringRes
    public static final int title_add_an_address = 3349;

    @StringRes
    public static final int title_payment_method = 3350;

    @StringRes
    public static final int title_select_shipping_method = 3351;

    @StringRes
    public static final int to_pay = 3352;

    @StringRes
    public static final int too_many_items = 3353;

    @StringRes
    public static final int unavailable_text_template = 3354;

    @StringRes
    public static final int using_gps = 3355;

    @StringRes
    public static final int valid_digits = 3356;

    @StringRes
    public static final int veg_only = 3357;

    @StringRes
    public static final int verify_and_proceed = 3358;

    @StringRes
    public static final int viewCart = 3359;

    @StringRes
    public static final int view_menu = 3360;

    @StringRes
    public static final int we_have_sent_you = 3361;

    @StringRes
    public static final int work = 3362;

    @StringRes
    public static final int yay = 3363;

    @StringRes
    public static final int yes = 3364;

    @StringRes
    public static final int your_previous_customization = 3365;

    @StringRes
    public static final int zip_helper = 3366;
  }

  public static final class style {
    @StyleRes
    public static final int AlertDialog_AppCompat = 3367;

    @StyleRes
    public static final int AlertDialog_AppCompat_Light = 3368;

    @StyleRes
    public static final int Animation_AppCompat_Dialog = 3369;

    @StyleRes
    public static final int Animation_AppCompat_DropDownUp = 3370;

    @StyleRes
    public static final int Animation_AppCompat_Tooltip = 3371;

    @StyleRes
    public static final int Animation_Design_BottomSheetDialog = 3372;

    @StyleRes
    public static final int AppTheme = 3373;

    @StyleRes
    public static final int AppTheme_WithActionBar = 3374;

    @StyleRes
    public static final int AppTheme_WithoutFullScreen = 3375;

    @StyleRes
    public static final int Base_AlertDialog_AppCompat = 3376;

    @StyleRes
    public static final int Base_AlertDialog_AppCompat_Light = 3377;

    @StyleRes
    public static final int Base_Animation_AppCompat_Dialog = 3378;

    @StyleRes
    public static final int Base_Animation_AppCompat_DropDownUp = 3379;

    @StyleRes
    public static final int Base_Animation_AppCompat_Tooltip = 3380;

    @StyleRes
    public static final int Base_CardView = 3381;

    @StyleRes
    public static final int Base_DialogWindowTitleBackground_AppCompat = 3382;

    @StyleRes
    public static final int Base_DialogWindowTitle_AppCompat = 3383;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat = 3384;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Body1 = 3385;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Body2 = 3386;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Button = 3387;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Caption = 3388;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Display1 = 3389;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Display2 = 3390;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Display3 = 3391;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Display4 = 3392;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Headline = 3393;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Inverse = 3394;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Large = 3395;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Large_Inverse = 3396;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large = 3397;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small = 3398;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Medium = 3399;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Medium_Inverse = 3400;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Menu = 3401;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_SearchResult = 3402;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_SearchResult_Subtitle = 3403;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_SearchResult_Title = 3404;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Small = 3405;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Small_Inverse = 3406;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Subhead = 3407;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Subhead_Inverse = 3408;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Title = 3409;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Title_Inverse = 3410;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Tooltip = 3411;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Menu = 3412;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle = 3413;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse = 3414;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Title = 3415;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse = 3416;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle = 3417;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_ActionMode_Title = 3418;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Button = 3419;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored = 3420;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Button_Colored = 3421;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Button_Inverse = 3422;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_DropDownItem = 3423;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_PopupMenu_Header = 3424;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_PopupMenu_Large = 3425;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_PopupMenu_Small = 3426;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_Switch = 3427;

    @StyleRes
    public static final int Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem = 3428;

    @StyleRes
    public static final int Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item = 3429;

    @StyleRes
    public static final int Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle = 3430;

    @StyleRes
    public static final int Base_TextAppearance_Widget_AppCompat_Toolbar_Title = 3431;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat = 3432;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_ActionBar = 3433;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Dark = 3434;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Dark_ActionBar = 3435;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Dialog = 3436;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Dialog_Alert = 3437;

    @StyleRes
    public static final int Base_ThemeOverlay_AppCompat_Light = 3438;

    @StyleRes
    public static final int Base_ThemeOverlay_MaterialComponents_Dialog = 3439;

    @StyleRes
    public static final int Base_ThemeOverlay_MaterialComponents_Dialog_Alert = 3440;

    @StyleRes
    public static final int Base_Theme_AppCompat = 3441;

    @StyleRes
    public static final int Base_Theme_AppCompat_CompactMenu = 3442;

    @StyleRes
    public static final int Base_Theme_AppCompat_Dialog = 3443;

    @StyleRes
    public static final int Base_Theme_AppCompat_DialogWhenLarge = 3444;

    @StyleRes
    public static final int Base_Theme_AppCompat_Dialog_Alert = 3445;

    @StyleRes
    public static final int Base_Theme_AppCompat_Dialog_FixedSize = 3446;

    @StyleRes
    public static final int Base_Theme_AppCompat_Dialog_MinWidth = 3447;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light = 3448;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_DarkActionBar = 3449;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_Dialog = 3450;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_DialogWhenLarge = 3451;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_Dialog_Alert = 3452;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_Dialog_FixedSize = 3453;

    @StyleRes
    public static final int Base_Theme_AppCompat_Light_Dialog_MinWidth = 3454;

    @StyleRes
    public static final int Base_Theme_MaterialComponents = 3455;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Bridge = 3456;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_CompactMenu = 3457;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Dialog = 3458;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_DialogWhenLarge = 3459;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Dialog_Alert = 3460;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Dialog_FixedSize = 3461;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Dialog_MinWidth = 3462;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light = 3463;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Bridge = 3464;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_DarkActionBar = 3465;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_DarkActionBar_Bridge = 3466;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Dialog = 3467;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_DialogWhenLarge = 3468;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Dialog_Alert = 3469;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Dialog_FixedSize = 3470;

    @StyleRes
    public static final int Base_Theme_MaterialComponents_Light_Dialog_MinWidth = 3471;

    @StyleRes
    public static final int Base_V11_ThemeOverlay_AppCompat_Dialog = 3472;

    @StyleRes
    public static final int Base_V11_Theme_AppCompat_Dialog = 3473;

    @StyleRes
    public static final int Base_V11_Theme_AppCompat_Light_Dialog = 3474;

    @StyleRes
    public static final int Base_V12_Widget_AppCompat_AutoCompleteTextView = 3475;

    @StyleRes
    public static final int Base_V12_Widget_AppCompat_EditText = 3476;

    @StyleRes
    public static final int Base_V14_ThemeOverlay_MaterialComponents_Dialog = 3477;

    @StyleRes
    public static final int Base_V14_ThemeOverlay_MaterialComponents_Dialog_Alert = 3478;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents = 3479;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Bridge = 3480;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Dialog = 3481;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Light = 3482;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Light_Bridge = 3483;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Light_DarkActionBar_Bridge = 3484;

    @StyleRes
    public static final int Base_V14_Theme_MaterialComponents_Light_Dialog = 3485;

    @StyleRes
    public static final int Base_V14_Widget_Design_AppBarLayout = 3486;

    @StyleRes
    public static final int Base_V21_ThemeOverlay_AppCompat_Dialog = 3487;

    @StyleRes
    public static final int Base_V21_Theme_AppCompat = 3488;

    @StyleRes
    public static final int Base_V21_Theme_AppCompat_Dialog = 3489;

    @StyleRes
    public static final int Base_V21_Theme_AppCompat_Light = 3490;

    @StyleRes
    public static final int Base_V21_Theme_AppCompat_Light_Dialog = 3491;

    @StyleRes
    public static final int Base_V21_Widget_Design_AppBarLayout = 3492;

    @StyleRes
    public static final int Base_V22_Theme_AppCompat = 3493;

    @StyleRes
    public static final int Base_V22_Theme_AppCompat_Light = 3494;

    @StyleRes
    public static final int Base_V23_Theme_AppCompat = 3495;

    @StyleRes
    public static final int Base_V23_Theme_AppCompat_Light = 3496;

    @StyleRes
    public static final int Base_V26_Theme_AppCompat = 3497;

    @StyleRes
    public static final int Base_V26_Theme_AppCompat_Light = 3498;

    @StyleRes
    public static final int Base_V26_Widget_AppCompat_Toolbar = 3499;

    @StyleRes
    public static final int Base_V26_Widget_Design_AppBarLayout = 3500;

    @StyleRes
    public static final int Base_V28_Theme_AppCompat = 3501;

    @StyleRes
    public static final int Base_V28_Theme_AppCompat_Light = 3502;

    @StyleRes
    public static final int Base_V7_ThemeOverlay_AppCompat_Dialog = 3503;

    @StyleRes
    public static final int Base_V7_Theme_AppCompat = 3504;

    @StyleRes
    public static final int Base_V7_Theme_AppCompat_Dialog = 3505;

    @StyleRes
    public static final int Base_V7_Theme_AppCompat_Light = 3506;

    @StyleRes
    public static final int Base_V7_Theme_AppCompat_Light_Dialog = 3507;

    @StyleRes
    public static final int Base_V7_Widget_AppCompat_AutoCompleteTextView = 3508;

    @StyleRes
    public static final int Base_V7_Widget_AppCompat_EditText = 3509;

    @StyleRes
    public static final int Base_V7_Widget_AppCompat_Toolbar = 3510;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar = 3511;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar_Solid = 3512;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar_TabBar = 3513;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar_TabText = 3514;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionBar_TabView = 3515;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionButton = 3516;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionButton_CloseMode = 3517;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionButton_Overflow = 3518;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActionMode = 3519;

    @StyleRes
    public static final int Base_Widget_AppCompat_ActivityChooserView = 3520;

    @StyleRes
    public static final int Base_Widget_AppCompat_AutoCompleteTextView = 3521;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button = 3522;

    @StyleRes
    public static final int Base_Widget_AppCompat_ButtonBar = 3523;

    @StyleRes
    public static final int Base_Widget_AppCompat_ButtonBar_AlertDialog = 3524;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_Borderless = 3525;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_Borderless_Colored = 3526;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_ButtonBar_AlertDialog = 3527;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_Colored = 3528;

    @StyleRes
    public static final int Base_Widget_AppCompat_Button_Small = 3529;

    @StyleRes
    public static final int Base_Widget_AppCompat_CompoundButton_CheckBox = 3530;

    @StyleRes
    public static final int Base_Widget_AppCompat_CompoundButton_RadioButton = 3531;

    @StyleRes
    public static final int Base_Widget_AppCompat_CompoundButton_Switch = 3532;

    @StyleRes
    public static final int Base_Widget_AppCompat_DrawerArrowToggle = 3533;

    @StyleRes
    public static final int Base_Widget_AppCompat_DrawerArrowToggle_Common = 3534;

    @StyleRes
    public static final int Base_Widget_AppCompat_DropDownItem_Spinner = 3535;

    @StyleRes
    public static final int Base_Widget_AppCompat_EditText = 3536;

    @StyleRes
    public static final int Base_Widget_AppCompat_ImageButton = 3537;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar = 3538;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_Solid = 3539;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_TabBar = 3540;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_TabText = 3541;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse = 3542;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_ActionBar_TabView = 3543;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_PopupMenu = 3544;

    @StyleRes
    public static final int Base_Widget_AppCompat_Light_PopupMenu_Overflow = 3545;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListMenuView = 3546;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListPopupWindow = 3547;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListView = 3548;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListView_DropDown = 3549;

    @StyleRes
    public static final int Base_Widget_AppCompat_ListView_Menu = 3550;

    @StyleRes
    public static final int Base_Widget_AppCompat_PopupMenu = 3551;

    @StyleRes
    public static final int Base_Widget_AppCompat_PopupMenu_Overflow = 3552;

    @StyleRes
    public static final int Base_Widget_AppCompat_PopupWindow = 3553;

    @StyleRes
    public static final int Base_Widget_AppCompat_ProgressBar = 3554;

    @StyleRes
    public static final int Base_Widget_AppCompat_ProgressBar_Horizontal = 3555;

    @StyleRes
    public static final int Base_Widget_AppCompat_RatingBar = 3556;

    @StyleRes
    public static final int Base_Widget_AppCompat_RatingBar_Indicator = 3557;

    @StyleRes
    public static final int Base_Widget_AppCompat_RatingBar_Small = 3558;

    @StyleRes
    public static final int Base_Widget_AppCompat_SearchView = 3559;

    @StyleRes
    public static final int Base_Widget_AppCompat_SearchView_ActionBar = 3560;

    @StyleRes
    public static final int Base_Widget_AppCompat_SeekBar = 3561;

    @StyleRes
    public static final int Base_Widget_AppCompat_SeekBar_Discrete = 3562;

    @StyleRes
    public static final int Base_Widget_AppCompat_Spinner = 3563;

    @StyleRes
    public static final int Base_Widget_AppCompat_Spinner_Underlined = 3564;

    @StyleRes
    public static final int Base_Widget_AppCompat_TextView = 3565;

    @StyleRes
    public static final int Base_Widget_AppCompat_TextView_SpinnerItem = 3566;

    @StyleRes
    public static final int Base_Widget_AppCompat_Toolbar = 3567;

    @StyleRes
    public static final int Base_Widget_AppCompat_Toolbar_Button_Navigation = 3568;

    @StyleRes
    public static final int Base_Widget_Design_AppBarLayout = 3569;

    @StyleRes
    public static final int Base_Widget_Design_TabLayout = 3570;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_Chip = 3571;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_TextInputEditText = 3572;

    @StyleRes
    public static final int Base_Widget_MaterialComponents_TextInputLayout = 3573;

    @StyleRes
    public static final int CardView = 3574;

    @StyleRes
    public static final int CardView_Dark = 3575;

    @StyleRes
    public static final int CardView_Light = 3576;

    @StyleRes
    public static final int DialogAnimation = 3577;

    @StyleRes
    public static final int DialogAnimationFade = 3578;

    @StyleRes
    public static final int DialogStyle = 3579;

    @StyleRes
    public static final int DialogStyleBottomSheet = 3580;

    @StyleRes
    public static final int MyCheckBoxStyle = 3581;

    @StyleRes
    public static final int MyRadioButtonStyle = 3582;

    @StyleRes
    public static final int OtpWidget = 3583;

    @StyleRes
    public static final int OtpWidget_OtpView = 3584;

    @StyleRes
    public static final int Platform_AppCompat = 3585;

    @StyleRes
    public static final int Platform_AppCompat_Light = 3586;

    @StyleRes
    public static final int Platform_MaterialComponents = 3587;

    @StyleRes
    public static final int Platform_MaterialComponents_Dialog = 3588;

    @StyleRes
    public static final int Platform_MaterialComponents_Light = 3589;

    @StyleRes
    public static final int Platform_MaterialComponents_Light_Dialog = 3590;

    @StyleRes
    public static final int Platform_ThemeOverlay_AppCompat = 3591;

    @StyleRes
    public static final int Platform_ThemeOverlay_AppCompat_Dark = 3592;

    @StyleRes
    public static final int Platform_ThemeOverlay_AppCompat_Light = 3593;

    @StyleRes
    public static final int Platform_V11_AppCompat = 3594;

    @StyleRes
    public static final int Platform_V11_AppCompat_Light = 3595;

    @StyleRes
    public static final int Platform_V14_AppCompat = 3596;

    @StyleRes
    public static final int Platform_V14_AppCompat_Light = 3597;

    @StyleRes
    public static final int Platform_V21_AppCompat = 3598;

    @StyleRes
    public static final int Platform_V21_AppCompat_Light = 3599;

    @StyleRes
    public static final int Platform_V25_AppCompat = 3600;

    @StyleRes
    public static final int Platform_V25_AppCompat_Light = 3601;

    @StyleRes
    public static final int Platform_Widget_AppCompat_Spinner = 3602;

    @StyleRes
    public static final int RatingBar = 3603;

    @StyleRes
    public static final int RtlOverlay_DialogWindowTitle_AppCompat = 3604;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_ActionBar_TitleItem = 3605;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_DialogTitle_Icon = 3606;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem = 3607;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup = 3608;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut = 3609;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow = 3610;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_Text = 3611;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_PopupMenuItem_Title = 3612;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_SearchView_MagIcon = 3613;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown = 3614;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1 = 3615;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2 = 3616;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown_Query = 3617;

    @StyleRes
    public static final int RtlOverlay_Widget_AppCompat_Search_DropDown_Text = 3618;

    @StyleRes
    public static final int RtlUnderlay_Widget_AppCompat_ActionButton = 3619;

    @StyleRes
    public static final int RtlUnderlay_Widget_AppCompat_ActionButton_Overflow = 3620;

    @StyleRes
    public static final int SplashScreen = 3621;

    @StyleRes
    public static final int StripeDefaultTheme = 3622;

    @StyleRes
    public static final int StripeErrorTextStyle = 3623;

    @StyleRes
    public static final int StripeToolBarStyle = 3624;

    @StyleRes
    public static final int TextAppearance_AppCompat = 3625;

    @StyleRes
    public static final int TextAppearance_AppCompat_Body1 = 3626;

    @StyleRes
    public static final int TextAppearance_AppCompat_Body2 = 3627;

    @StyleRes
    public static final int TextAppearance_AppCompat_Button = 3628;

    @StyleRes
    public static final int TextAppearance_AppCompat_Caption = 3629;

    @StyleRes
    public static final int TextAppearance_AppCompat_Display1 = 3630;

    @StyleRes
    public static final int TextAppearance_AppCompat_Display2 = 3631;

    @StyleRes
    public static final int TextAppearance_AppCompat_Display3 = 3632;

    @StyleRes
    public static final int TextAppearance_AppCompat_Display4 = 3633;

    @StyleRes
    public static final int TextAppearance_AppCompat_Headline = 3634;

    @StyleRes
    public static final int TextAppearance_AppCompat_Inverse = 3635;

    @StyleRes
    public static final int TextAppearance_AppCompat_Large = 3636;

    @StyleRes
    public static final int TextAppearance_AppCompat_Large_Inverse = 3637;

    @StyleRes
    public static final int TextAppearance_AppCompat_Light_SearchResult_Subtitle = 3638;

    @StyleRes
    public static final int TextAppearance_AppCompat_Light_SearchResult_Title = 3639;

    @StyleRes
    public static final int TextAppearance_AppCompat_Light_Widget_PopupMenu_Large = 3640;

    @StyleRes
    public static final int TextAppearance_AppCompat_Light_Widget_PopupMenu_Small = 3641;

    @StyleRes
    public static final int TextAppearance_AppCompat_Medium = 3642;

    @StyleRes
    public static final int TextAppearance_AppCompat_Medium_Inverse = 3643;

    @StyleRes
    public static final int TextAppearance_AppCompat_Menu = 3644;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification = 3645;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification_Info = 3646;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification_Info_Media = 3647;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification_Line2 = 3648;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification_Line2_Media = 3649;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification_Media = 3650;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification_Time = 3651;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification_Time_Media = 3652;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification_Title = 3653;

    @StyleRes
    public static final int TextAppearance_AppCompat_Notification_Title_Media = 3654;

    @StyleRes
    public static final int TextAppearance_AppCompat_SearchResult_Subtitle = 3655;

    @StyleRes
    public static final int TextAppearance_AppCompat_SearchResult_Title = 3656;

    @StyleRes
    public static final int TextAppearance_AppCompat_Small = 3657;

    @StyleRes
    public static final int TextAppearance_AppCompat_Small_Inverse = 3658;

    @StyleRes
    public static final int TextAppearance_AppCompat_Subhead = 3659;

    @StyleRes
    public static final int TextAppearance_AppCompat_Subhead_Inverse = 3660;

    @StyleRes
    public static final int TextAppearance_AppCompat_Title = 3661;

    @StyleRes
    public static final int TextAppearance_AppCompat_Title_Inverse = 3662;

    @StyleRes
    public static final int TextAppearance_AppCompat_Tooltip = 3663;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Menu = 3664;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Subtitle = 3665;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse = 3666;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Title = 3667;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse = 3668;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionMode_Subtitle = 3669;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse = 3670;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionMode_Title = 3671;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse = 3672;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Button = 3673;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Button_Borderless_Colored = 3674;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Button_Colored = 3675;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Button_Inverse = 3676;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_DropDownItem = 3677;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_PopupMenu_Header = 3678;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_PopupMenu_Large = 3679;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_PopupMenu_Small = 3680;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_Switch = 3681;

    @StyleRes
    public static final int TextAppearance_AppCompat_Widget_TextView_SpinnerItem = 3682;

    @StyleRes
    public static final int TextAppearance_Compat_Notification = 3683;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Info = 3684;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Info_Media = 3685;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Line2 = 3686;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Line2_Media = 3687;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Media = 3688;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Time = 3689;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Time_Media = 3690;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Title = 3691;

    @StyleRes
    public static final int TextAppearance_Compat_Notification_Title_Media = 3692;

    @StyleRes
    public static final int TextAppearance_Design_CollapsingToolbar_Expanded = 3693;

    @StyleRes
    public static final int TextAppearance_Design_Counter = 3694;

    @StyleRes
    public static final int TextAppearance_Design_Counter_Overflow = 3695;

    @StyleRes
    public static final int TextAppearance_Design_Error = 3696;

    @StyleRes
    public static final int TextAppearance_Design_HelperText = 3697;

    @StyleRes
    public static final int TextAppearance_Design_Hint = 3698;

    @StyleRes
    public static final int TextAppearance_Design_Snackbar_Message = 3699;

    @StyleRes
    public static final int TextAppearance_Design_Tab = 3700;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Body1 = 3701;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Body2 = 3702;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Button = 3703;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Caption = 3704;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Chip = 3705;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline1 = 3706;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline2 = 3707;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline3 = 3708;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline4 = 3709;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline5 = 3710;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Headline6 = 3711;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Overline = 3712;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Subtitle1 = 3713;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Subtitle2 = 3714;

    @StyleRes
    public static final int TextAppearance_MaterialComponents_Tab = 3715;

    @StyleRes
    public static final int TextAppearance_StatusBar_EventContent = 3716;

    @StyleRes
    public static final int TextAppearance_StatusBar_EventContent_Info = 3717;

    @StyleRes
    public static final int TextAppearance_StatusBar_EventContent_Line2 = 3718;

    @StyleRes
    public static final int TextAppearance_StatusBar_EventContent_Time = 3719;

    @StyleRes
    public static final int TextAppearance_StatusBar_EventContent_Title = 3720;

    @StyleRes
    public static final int TextAppearance_Widget_AppCompat_ExpandedMenu_Item = 3721;

    @StyleRes
    public static final int TextAppearance_Widget_AppCompat_Toolbar_Subtitle = 3722;

    @StyleRes
    public static final int TextAppearance_Widget_AppCompat_Toolbar_Title = 3723;

    @StyleRes
    public static final int ThemeOverlay_AppCompat = 3724;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_ActionBar = 3725;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Dark = 3726;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Dark_ActionBar = 3727;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_DayNight = 3728;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_DayNight_ActionBar = 3729;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Dialog = 3730;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Dialog_Alert = 3731;

    @StyleRes
    public static final int ThemeOverlay_AppCompat_Light = 3732;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents = 3733;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_ActionBar = 3734;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Dark = 3735;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Dark_ActionBar = 3736;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Dialog = 3737;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Dialog_Alert = 3738;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_Light = 3739;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText = 3740;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox = 3741;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox_Dense = 3742;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox = 3743;

    @StyleRes
    public static final int ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox_Dense = 3744;

    @StyleRes
    public static final int Theme_AppCompat = 3745;

    @StyleRes
    public static final int Theme_AppCompat_CompactMenu = 3746;

    @StyleRes
    public static final int Theme_AppCompat_DayNight = 3747;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_DarkActionBar = 3748;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_Dialog = 3749;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_DialogWhenLarge = 3750;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_Dialog_Alert = 3751;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_Dialog_MinWidth = 3752;

    @StyleRes
    public static final int Theme_AppCompat_DayNight_NoActionBar = 3753;

    @StyleRes
    public static final int Theme_AppCompat_Dialog = 3754;

    @StyleRes
    public static final int Theme_AppCompat_DialogWhenLarge = 3755;

    @StyleRes
    public static final int Theme_AppCompat_Dialog_Alert = 3756;

    @StyleRes
    public static final int Theme_AppCompat_Dialog_MinWidth = 3757;

    @StyleRes
    public static final int Theme_AppCompat_Light = 3758;

    @StyleRes
    public static final int Theme_AppCompat_Light_DarkActionBar = 3759;

    @StyleRes
    public static final int Theme_AppCompat_Light_Dialog = 3760;

    @StyleRes
    public static final int Theme_AppCompat_Light_DialogWhenLarge = 3761;

    @StyleRes
    public static final int Theme_AppCompat_Light_Dialog_Alert = 3762;

    @StyleRes
    public static final int Theme_AppCompat_Light_Dialog_MinWidth = 3763;

    @StyleRes
    public static final int Theme_AppCompat_Light_NoActionBar = 3764;

    @StyleRes
    public static final int Theme_AppCompat_NoActionBar = 3765;

    @StyleRes
    public static final int Theme_Design = 3766;

    @StyleRes
    public static final int Theme_Design_BottomSheetDialog = 3767;

    @StyleRes
    public static final int Theme_Design_Light = 3768;

    @StyleRes
    public static final int Theme_Design_Light_BottomSheetDialog = 3769;

    @StyleRes
    public static final int Theme_Design_Light_NoActionBar = 3770;

    @StyleRes
    public static final int Theme_Design_NoActionBar = 3771;

    @StyleRes
    public static final int Theme_MaterialComponents = 3772;

    @StyleRes
    public static final int Theme_MaterialComponents_BottomSheetDialog = 3773;

    @StyleRes
    public static final int Theme_MaterialComponents_Bridge = 3774;

    @StyleRes
    public static final int Theme_MaterialComponents_CompactMenu = 3775;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog = 3776;

    @StyleRes
    public static final int Theme_MaterialComponents_DialogWhenLarge = 3777;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog_Alert = 3778;

    @StyleRes
    public static final int Theme_MaterialComponents_Dialog_MinWidth = 3779;

    @StyleRes
    public static final int Theme_MaterialComponents_Light = 3780;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_BottomSheetDialog = 3781;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Bridge = 3782;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_DarkActionBar = 3783;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_DarkActionBar_Bridge = 3784;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog = 3785;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_DialogWhenLarge = 3786;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog_Alert = 3787;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_Dialog_MinWidth = 3788;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_NoActionBar = 3789;

    @StyleRes
    public static final int Theme_MaterialComponents_Light_NoActionBar_Bridge = 3790;

    @StyleRes
    public static final int Theme_MaterialComponents_NoActionBar = 3791;

    @StyleRes
    public static final int Theme_MaterialComponents_NoActionBar_Bridge = 3792;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar = 3793;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar_Solid = 3794;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar_TabBar = 3795;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar_TabText = 3796;

    @StyleRes
    public static final int Widget_AppCompat_ActionBar_TabView = 3797;

    @StyleRes
    public static final int Widget_AppCompat_ActionButton = 3798;

    @StyleRes
    public static final int Widget_AppCompat_ActionButton_CloseMode = 3799;

    @StyleRes
    public static final int Widget_AppCompat_ActionButton_Overflow = 3800;

    @StyleRes
    public static final int Widget_AppCompat_ActionMode = 3801;

    @StyleRes
    public static final int Widget_AppCompat_ActivityChooserView = 3802;

    @StyleRes
    public static final int Widget_AppCompat_AutoCompleteTextView = 3803;

    @StyleRes
    public static final int Widget_AppCompat_Button = 3804;

    @StyleRes
    public static final int Widget_AppCompat_ButtonBar = 3805;

    @StyleRes
    public static final int Widget_AppCompat_ButtonBar_AlertDialog = 3806;

    @StyleRes
    public static final int Widget_AppCompat_Button_Borderless = 3807;

    @StyleRes
    public static final int Widget_AppCompat_Button_Borderless_Colored = 3808;

    @StyleRes
    public static final int Widget_AppCompat_Button_ButtonBar_AlertDialog = 3809;

    @StyleRes
    public static final int Widget_AppCompat_Button_Colored = 3810;

    @StyleRes
    public static final int Widget_AppCompat_Button_Small = 3811;

    @StyleRes
    public static final int Widget_AppCompat_CompoundButton_CheckBox = 3812;

    @StyleRes
    public static final int Widget_AppCompat_CompoundButton_RadioButton = 3813;

    @StyleRes
    public static final int Widget_AppCompat_CompoundButton_Switch = 3814;

    @StyleRes
    public static final int Widget_AppCompat_DrawerArrowToggle = 3815;

    @StyleRes
    public static final int Widget_AppCompat_DropDownItem_Spinner = 3816;

    @StyleRes
    public static final int Widget_AppCompat_EditText = 3817;

    @StyleRes
    public static final int Widget_AppCompat_ImageButton = 3818;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar = 3819;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_Solid = 3820;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_Solid_Inverse = 3821;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabBar = 3822;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabBar_Inverse = 3823;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabText = 3824;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabText_Inverse = 3825;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabView = 3826;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionBar_TabView_Inverse = 3827;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionButton = 3828;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionButton_CloseMode = 3829;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionButton_Overflow = 3830;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActionMode_Inverse = 3831;

    @StyleRes
    public static final int Widget_AppCompat_Light_ActivityChooserView = 3832;

    @StyleRes
    public static final int Widget_AppCompat_Light_AutoCompleteTextView = 3833;

    @StyleRes
    public static final int Widget_AppCompat_Light_DropDownItem_Spinner = 3834;

    @StyleRes
    public static final int Widget_AppCompat_Light_ListPopupWindow = 3835;

    @StyleRes
    public static final int Widget_AppCompat_Light_ListView_DropDown = 3836;

    @StyleRes
    public static final int Widget_AppCompat_Light_PopupMenu = 3837;

    @StyleRes
    public static final int Widget_AppCompat_Light_PopupMenu_Overflow = 3838;

    @StyleRes
    public static final int Widget_AppCompat_Light_SearchView = 3839;

    @StyleRes
    public static final int Widget_AppCompat_Light_Spinner_DropDown_ActionBar = 3840;

    @StyleRes
    public static final int Widget_AppCompat_ListMenuView = 3841;

    @StyleRes
    public static final int Widget_AppCompat_ListPopupWindow = 3842;

    @StyleRes
    public static final int Widget_AppCompat_ListView = 3843;

    @StyleRes
    public static final int Widget_AppCompat_ListView_DropDown = 3844;

    @StyleRes
    public static final int Widget_AppCompat_ListView_Menu = 3845;

    @StyleRes
    public static final int Widget_AppCompat_NotificationActionContainer = 3846;

    @StyleRes
    public static final int Widget_AppCompat_NotificationActionText = 3847;

    @StyleRes
    public static final int Widget_AppCompat_PopupMenu = 3848;

    @StyleRes
    public static final int Widget_AppCompat_PopupMenu_Overflow = 3849;

    @StyleRes
    public static final int Widget_AppCompat_PopupWindow = 3850;

    @StyleRes
    public static final int Widget_AppCompat_ProgressBar = 3851;

    @StyleRes
    public static final int Widget_AppCompat_ProgressBar_Horizontal = 3852;

    @StyleRes
    public static final int Widget_AppCompat_RatingBar = 3853;

    @StyleRes
    public static final int Widget_AppCompat_RatingBar_Indicator = 3854;

    @StyleRes
    public static final int Widget_AppCompat_RatingBar_Small = 3855;

    @StyleRes
    public static final int Widget_AppCompat_SearchView = 3856;

    @StyleRes
    public static final int Widget_AppCompat_SearchView_ActionBar = 3857;

    @StyleRes
    public static final int Widget_AppCompat_SeekBar = 3858;

    @StyleRes
    public static final int Widget_AppCompat_SeekBar_Discrete = 3859;

    @StyleRes
    public static final int Widget_AppCompat_Spinner = 3860;

    @StyleRes
    public static final int Widget_AppCompat_Spinner_DropDown = 3861;

    @StyleRes
    public static final int Widget_AppCompat_Spinner_DropDown_ActionBar = 3862;

    @StyleRes
    public static final int Widget_AppCompat_Spinner_Underlined = 3863;

    @StyleRes
    public static final int Widget_AppCompat_TextView = 3864;

    @StyleRes
    public static final int Widget_AppCompat_TextView_SpinnerItem = 3865;

    @StyleRes
    public static final int Widget_AppCompat_Toolbar = 3866;

    @StyleRes
    public static final int Widget_AppCompat_Toolbar_Button_Navigation = 3867;

    @StyleRes
    public static final int Widget_Compat_NotificationActionContainer = 3868;

    @StyleRes
    public static final int Widget_Compat_NotificationActionText = 3869;

    @StyleRes
    public static final int Widget_Design_AppBarLayout = 3870;

    @StyleRes
    public static final int Widget_Design_BottomNavigationView = 3871;

    @StyleRes
    public static final int Widget_Design_BottomSheet_Modal = 3872;

    @StyleRes
    public static final int Widget_Design_CollapsingToolbar = 3873;

    @StyleRes
    public static final int Widget_Design_CoordinatorLayout = 3874;

    @StyleRes
    public static final int Widget_Design_FloatingActionButton = 3875;

    @StyleRes
    public static final int Widget_Design_NavigationView = 3876;

    @StyleRes
    public static final int Widget_Design_ScrimInsetsFrameLayout = 3877;

    @StyleRes
    public static final int Widget_Design_Snackbar = 3878;

    @StyleRes
    public static final int Widget_Design_TabLayout = 3879;

    @StyleRes
    public static final int Widget_Design_TextInputLayout = 3880;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomAppBar = 3881;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomAppBar_Colored = 3882;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomNavigationView = 3883;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomNavigationView_Colored = 3884;

    @StyleRes
    public static final int Widget_MaterialComponents_BottomSheet_Modal = 3885;

    @StyleRes
    public static final int Widget_MaterialComponents_Button = 3886;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_Icon = 3887;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_OutlinedButton = 3888;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_OutlinedButton_Icon = 3889;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton = 3890;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton_Dialog = 3891;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton_Dialog_Icon = 3892;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_TextButton_Icon = 3893;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_UnelevatedButton = 3894;

    @StyleRes
    public static final int Widget_MaterialComponents_Button_UnelevatedButton_Icon = 3895;

    @StyleRes
    public static final int Widget_MaterialComponents_CardView = 3896;

    @StyleRes
    public static final int Widget_MaterialComponents_ChipGroup = 3897;

    @StyleRes
    public static final int Widget_MaterialComponents_Chip_Action = 3898;

    @StyleRes
    public static final int Widget_MaterialComponents_Chip_Choice = 3899;

    @StyleRes
    public static final int Widget_MaterialComponents_Chip_Entry = 3900;

    @StyleRes
    public static final int Widget_MaterialComponents_Chip_Filter = 3901;

    @StyleRes
    public static final int Widget_MaterialComponents_FloatingActionButton = 3902;

    @StyleRes
    public static final int Widget_MaterialComponents_NavigationView = 3903;

    @StyleRes
    public static final int Widget_MaterialComponents_Snackbar = 3904;

    @StyleRes
    public static final int Widget_MaterialComponents_Snackbar_FullWidth = 3905;

    @StyleRes
    public static final int Widget_MaterialComponents_TabLayout = 3906;

    @StyleRes
    public static final int Widget_MaterialComponents_TabLayout_Colored = 3907;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputEditText_FilledBox = 3908;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputEditText_FilledBox_Dense = 3909;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputEditText_OutlinedBox = 3910;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputEditText_OutlinedBox_Dense = 3911;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_FilledBox = 3912;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_FilledBox_Dense = 3913;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_OutlinedBox = 3914;

    @StyleRes
    public static final int Widget_MaterialComponents_TextInputLayout_OutlinedBox_Dense = 3915;

    @StyleRes
    public static final int Widget_MaterialComponents_Toolbar = 3916;

    @StyleRes
    public static final int Widget_Support_CoordinatorLayout = 3917;

    @StyleRes
    public static final int amu_Bubble_TextAppearance_Dark = 3918;

    @StyleRes
    public static final int amu_Bubble_TextAppearance_Light = 3919;

    @StyleRes
    public static final int amu_ClusterIcon_TextAppearance = 3920;

    @StyleRes
    public static final int com_facebook_activity_theme = 3921;

    @StyleRes
    public static final int com_facebook_auth_dialog = 3922;

    @StyleRes
    public static final int com_facebook_auth_dialog_instructions_textview = 3923;

    @StyleRes
    public static final int com_facebook_button = 3924;

    @StyleRes
    public static final int com_facebook_button_like = 3925;

    @StyleRes
    public static final int com_facebook_loginview_default_style = 3926;

    @StyleRes
    public static final int tooltip_bubble_text = 3927;
  }

  public static final class styleable {
    @StyleableRes
    public static final int ActionBar_background = 3928;

    @StyleableRes
    public static final int ActionBar_backgroundSplit = 3929;

    @StyleableRes
    public static final int ActionBar_backgroundStacked = 3930;

    @StyleableRes
    public static final int ActionBar_contentInsetEnd = 3931;

    @StyleableRes
    public static final int ActionBar_contentInsetEndWithActions = 3932;

    @StyleableRes
    public static final int ActionBar_contentInsetLeft = 3933;

    @StyleableRes
    public static final int ActionBar_contentInsetRight = 3934;

    @StyleableRes
    public static final int ActionBar_contentInsetStart = 3935;

    @StyleableRes
    public static final int ActionBar_contentInsetStartWithNavigation = 3936;

    @StyleableRes
    public static final int ActionBar_customNavigationLayout = 3937;

    @StyleableRes
    public static final int ActionBar_displayOptions = 3938;

    @StyleableRes
    public static final int ActionBar_divider = 3939;

    @StyleableRes
    public static final int ActionBar_elevation = 3940;

    @StyleableRes
    public static final int ActionBar_height = 3941;

    @StyleableRes
    public static final int ActionBar_hideOnContentScroll = 3942;

    @StyleableRes
    public static final int ActionBar_homeAsUpIndicator = 3943;

    @StyleableRes
    public static final int ActionBar_homeLayout = 3944;

    @StyleableRes
    public static final int ActionBar_icon = 3945;

    @StyleableRes
    public static final int ActionBar_indeterminateProgressStyle = 3946;

    @StyleableRes
    public static final int ActionBar_itemPadding = 3947;

    @StyleableRes
    public static final int ActionBar_logo = 3948;

    @StyleableRes
    public static final int ActionBar_navigationMode = 3949;

    @StyleableRes
    public static final int ActionBar_popupTheme = 3950;

    @StyleableRes
    public static final int ActionBar_progressBarPadding = 3951;

    @StyleableRes
    public static final int ActionBar_progressBarStyle = 3952;

    @StyleableRes
    public static final int ActionBar_subtitle = 3953;

    @StyleableRes
    public static final int ActionBar_subtitleTextStyle = 3954;

    @StyleableRes
    public static final int ActionBar_title = 3955;

    @StyleableRes
    public static final int ActionBar_titleTextStyle = 3956;

    @StyleableRes
    public static final int ActionBarLayout_android_layout_gravity = 3957;

    @StyleableRes
    public static final int ActionMenuItemView_android_minWidth = 3958;

    @StyleableRes
    public static final int ActionMode_background = 3959;

    @StyleableRes
    public static final int ActionMode_backgroundSplit = 3960;

    @StyleableRes
    public static final int ActionMode_closeItemLayout = 3961;

    @StyleableRes
    public static final int ActionMode_height = 3962;

    @StyleableRes
    public static final int ActionMode_subtitleTextStyle = 3963;

    @StyleableRes
    public static final int ActionMode_titleTextStyle = 3964;

    @StyleableRes
    public static final int ActivityChooserView_expandActivityOverflowButtonDrawable = 3965;

    @StyleableRes
    public static final int ActivityChooserView_initialActivityCount = 3966;

    @StyleableRes
    public static final int AlertDialog_android_layout = 3967;

    @StyleableRes
    public static final int AlertDialog_buttonIconDimen = 3968;

    @StyleableRes
    public static final int AlertDialog_buttonPanelSideLayout = 3969;

    @StyleableRes
    public static final int AlertDialog_listItemLayout = 3970;

    @StyleableRes
    public static final int AlertDialog_listLayout = 3971;

    @StyleableRes
    public static final int AlertDialog_multiChoiceItemLayout = 3972;

    @StyleableRes
    public static final int AlertDialog_showTitle = 3973;

    @StyleableRes
    public static final int AlertDialog_singleChoiceItemLayout = 3974;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_constantSize = 3975;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_dither = 3976;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_enterFadeDuration = 3977;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_exitFadeDuration = 3978;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_variablePadding = 3979;

    @StyleableRes
    public static final int AnimatedStateListDrawableCompat_android_visible = 3980;

    @StyleableRes
    public static final int AnimatedStateListDrawableItem_android_drawable = 3981;

    @StyleableRes
    public static final int AnimatedStateListDrawableItem_android_id = 3982;

    @StyleableRes
    public static final int AnimatedStateListDrawableTransition_android_drawable = 3983;

    @StyleableRes
    public static final int AnimatedStateListDrawableTransition_android_fromId = 3984;

    @StyleableRes
    public static final int AnimatedStateListDrawableTransition_android_reversible = 3985;

    @StyleableRes
    public static final int AnimatedStateListDrawableTransition_android_toId = 3986;

    @StyleableRes
    public static final int AnyViewIndicator_avi_animation_enable = 3987;

    @StyleableRes
    public static final int AnyViewIndicator_avi_animator = 3988;

    @StyleableRes
    public static final int AnyViewIndicator_avi_animator_reverse = 3989;

    @StyleableRes
    public static final int AnyViewIndicator_avi_drawable = 3990;

    @StyleableRes
    public static final int AnyViewIndicator_avi_drawable_unselected = 3991;

    @StyleableRes
    public static final int AnyViewIndicator_avi_gravity = 3992;

    @StyleableRes
    public static final int AnyViewIndicator_avi_height = 3993;

    @StyleableRes
    public static final int AnyViewIndicator_avi_margin = 3994;

    @StyleableRes
    public static final int AnyViewIndicator_avi_orientation = 3995;

    @StyleableRes
    public static final int AnyViewIndicator_avi_width = 3996;

    @StyleableRes
    public static final int AppBarLayout_android_background = 3997;

    @StyleableRes
    public static final int AppBarLayout_android_keyboardNavigationCluster = 3998;

    @StyleableRes
    public static final int AppBarLayout_android_touchscreenBlocksFocus = 3999;

    @StyleableRes
    public static final int AppBarLayout_elevation = 4000;

    @StyleableRes
    public static final int AppBarLayout_expanded = 4001;

    @StyleableRes
    public static final int AppBarLayout_liftOnScroll = 4002;

    @StyleableRes
    public static final int AppBarLayoutStates_state_collapsed = 4003;

    @StyleableRes
    public static final int AppBarLayoutStates_state_collapsible = 4004;

    @StyleableRes
    public static final int AppBarLayoutStates_state_liftable = 4005;

    @StyleableRes
    public static final int AppBarLayoutStates_state_lifted = 4006;

    @StyleableRes
    public static final int AppBarLayout_Layout_layout_scrollFlags = 4007;

    @StyleableRes
    public static final int AppBarLayout_Layout_layout_scrollInterpolator = 4008;

    @StyleableRes
    public static final int AppCompatImageView_android_src = 4009;

    @StyleableRes
    public static final int AppCompatImageView_srcCompat = 4010;

    @StyleableRes
    public static final int AppCompatImageView_tint = 4011;

    @StyleableRes
    public static final int AppCompatImageView_tintMode = 4012;

    @StyleableRes
    public static final int AppCompatSeekBar_android_thumb = 4013;

    @StyleableRes
    public static final int AppCompatSeekBar_tickMark = 4014;

    @StyleableRes
    public static final int AppCompatSeekBar_tickMarkTint = 4015;

    @StyleableRes
    public static final int AppCompatSeekBar_tickMarkTintMode = 4016;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableBottom = 4017;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableEnd = 4018;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableLeft = 4019;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableRight = 4020;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableStart = 4021;

    @StyleableRes
    public static final int AppCompatTextHelper_android_drawableTop = 4022;

    @StyleableRes
    public static final int AppCompatTextHelper_android_textAppearance = 4023;

    @StyleableRes
    public static final int AppCompatTextView_android_textAppearance = 4024;

    @StyleableRes
    public static final int AppCompatTextView_autoSizeMaxTextSize = 4025;

    @StyleableRes
    public static final int AppCompatTextView_autoSizeMinTextSize = 4026;

    @StyleableRes
    public static final int AppCompatTextView_autoSizePresetSizes = 4027;

    @StyleableRes
    public static final int AppCompatTextView_autoSizeStepGranularity = 4028;

    @StyleableRes
    public static final int AppCompatTextView_autoSizeTextType = 4029;

    @StyleableRes
    public static final int AppCompatTextView_drawableBottomCompat = 4030;

    @StyleableRes
    public static final int AppCompatTextView_drawableEndCompat = 4031;

    @StyleableRes
    public static final int AppCompatTextView_drawableLeftCompat = 4032;

    @StyleableRes
    public static final int AppCompatTextView_drawableRightCompat = 4033;

    @StyleableRes
    public static final int AppCompatTextView_drawableStartCompat = 4034;

    @StyleableRes
    public static final int AppCompatTextView_drawableTint = 4035;

    @StyleableRes
    public static final int AppCompatTextView_drawableTintMode = 4036;

    @StyleableRes
    public static final int AppCompatTextView_drawableTopCompat = 4037;

    @StyleableRes
    public static final int AppCompatTextView_firstBaselineToTopHeight = 4038;

    @StyleableRes
    public static final int AppCompatTextView_fontFamily = 4039;

    @StyleableRes
    public static final int AppCompatTextView_fontVariationSettings = 4040;

    @StyleableRes
    public static final int AppCompatTextView_lastBaselineToBottomHeight = 4041;

    @StyleableRes
    public static final int AppCompatTextView_lineHeight = 4042;

    @StyleableRes
    public static final int AppCompatTextView_textAllCaps = 4043;

    @StyleableRes
    public static final int AppCompatTextView_textLocale = 4044;

    @StyleableRes
    public static final int AppCompatTheme_actionBarDivider = 4045;

    @StyleableRes
    public static final int AppCompatTheme_actionBarItemBackground = 4046;

    @StyleableRes
    public static final int AppCompatTheme_actionBarPopupTheme = 4047;

    @StyleableRes
    public static final int AppCompatTheme_actionBarSize = 4048;

    @StyleableRes
    public static final int AppCompatTheme_actionBarSplitStyle = 4049;

    @StyleableRes
    public static final int AppCompatTheme_actionBarStyle = 4050;

    @StyleableRes
    public static final int AppCompatTheme_actionBarTabBarStyle = 4051;

    @StyleableRes
    public static final int AppCompatTheme_actionBarTabStyle = 4052;

    @StyleableRes
    public static final int AppCompatTheme_actionBarTabTextStyle = 4053;

    @StyleableRes
    public static final int AppCompatTheme_actionBarTheme = 4054;

    @StyleableRes
    public static final int AppCompatTheme_actionBarWidgetTheme = 4055;

    @StyleableRes
    public static final int AppCompatTheme_actionButtonStyle = 4056;

    @StyleableRes
    public static final int AppCompatTheme_actionDropDownStyle = 4057;

    @StyleableRes
    public static final int AppCompatTheme_actionMenuTextAppearance = 4058;

    @StyleableRes
    public static final int AppCompatTheme_actionMenuTextColor = 4059;

    @StyleableRes
    public static final int AppCompatTheme_actionModeBackground = 4060;

    @StyleableRes
    public static final int AppCompatTheme_actionModeCloseButtonStyle = 4061;

    @StyleableRes
    public static final int AppCompatTheme_actionModeCloseDrawable = 4062;

    @StyleableRes
    public static final int AppCompatTheme_actionModeCopyDrawable = 4063;

    @StyleableRes
    public static final int AppCompatTheme_actionModeCutDrawable = 4064;

    @StyleableRes
    public static final int AppCompatTheme_actionModeFindDrawable = 4065;

    @StyleableRes
    public static final int AppCompatTheme_actionModePasteDrawable = 4066;

    @StyleableRes
    public static final int AppCompatTheme_actionModePopupWindowStyle = 4067;

    @StyleableRes
    public static final int AppCompatTheme_actionModeSelectAllDrawable = 4068;

    @StyleableRes
    public static final int AppCompatTheme_actionModeShareDrawable = 4069;

    @StyleableRes
    public static final int AppCompatTheme_actionModeSplitBackground = 4070;

    @StyleableRes
    public static final int AppCompatTheme_actionModeStyle = 4071;

    @StyleableRes
    public static final int AppCompatTheme_actionModeWebSearchDrawable = 4072;

    @StyleableRes
    public static final int AppCompatTheme_actionOverflowButtonStyle = 4073;

    @StyleableRes
    public static final int AppCompatTheme_actionOverflowMenuStyle = 4074;

    @StyleableRes
    public static final int AppCompatTheme_activityChooserViewStyle = 4075;

    @StyleableRes
    public static final int AppCompatTheme_alertDialogButtonGroupStyle = 4076;

    @StyleableRes
    public static final int AppCompatTheme_alertDialogCenterButtons = 4077;

    @StyleableRes
    public static final int AppCompatTheme_alertDialogStyle = 4078;

    @StyleableRes
    public static final int AppCompatTheme_alertDialogTheme = 4079;

    @StyleableRes
    public static final int AppCompatTheme_android_windowAnimationStyle = 4080;

    @StyleableRes
    public static final int AppCompatTheme_android_windowIsFloating = 4081;

    @StyleableRes
    public static final int AppCompatTheme_autoCompleteTextViewStyle = 4082;

    @StyleableRes
    public static final int AppCompatTheme_borderlessButtonStyle = 4083;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarButtonStyle = 4084;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarNegativeButtonStyle = 4085;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarNeutralButtonStyle = 4086;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarPositiveButtonStyle = 4087;

    @StyleableRes
    public static final int AppCompatTheme_buttonBarStyle = 4088;

    @StyleableRes
    public static final int AppCompatTheme_buttonStyle = 4089;

    @StyleableRes
    public static final int AppCompatTheme_buttonStyleSmall = 4090;

    @StyleableRes
    public static final int AppCompatTheme_checkboxStyle = 4091;

    @StyleableRes
    public static final int AppCompatTheme_checkedTextViewStyle = 4092;

    @StyleableRes
    public static final int AppCompatTheme_colorAccent = 4093;

    @StyleableRes
    public static final int AppCompatTheme_colorBackgroundFloating = 4094;

    @StyleableRes
    public static final int AppCompatTheme_colorButtonNormal = 4095;

    @StyleableRes
    public static final int AppCompatTheme_colorControlActivated = 4096;

    @StyleableRes
    public static final int AppCompatTheme_colorControlHighlight = 4097;

    @StyleableRes
    public static final int AppCompatTheme_colorControlNormal = 4098;

    @StyleableRes
    public static final int AppCompatTheme_colorError = 4099;

    @StyleableRes
    public static final int AppCompatTheme_colorPrimary = 4100;

    @StyleableRes
    public static final int AppCompatTheme_colorPrimaryDark = 4101;

    @StyleableRes
    public static final int AppCompatTheme_colorSwitchThumbNormal = 4102;

    @StyleableRes
    public static final int AppCompatTheme_controlBackground = 4103;

    @StyleableRes
    public static final int AppCompatTheme_dialogCornerRadius = 4104;

    @StyleableRes
    public static final int AppCompatTheme_dialogPreferredPadding = 4105;

    @StyleableRes
    public static final int AppCompatTheme_dialogTheme = 4106;

    @StyleableRes
    public static final int AppCompatTheme_dividerHorizontal = 4107;

    @StyleableRes
    public static final int AppCompatTheme_dividerVertical = 4108;

    @StyleableRes
    public static final int AppCompatTheme_dropDownListViewStyle = 4109;

    @StyleableRes
    public static final int AppCompatTheme_dropdownListPreferredItemHeight = 4110;

    @StyleableRes
    public static final int AppCompatTheme_editTextBackground = 4111;

    @StyleableRes
    public static final int AppCompatTheme_editTextColor = 4112;

    @StyleableRes
    public static final int AppCompatTheme_editTextStyle = 4113;

    @StyleableRes
    public static final int AppCompatTheme_homeAsUpIndicator = 4114;

    @StyleableRes
    public static final int AppCompatTheme_imageButtonStyle = 4115;

    @StyleableRes
    public static final int AppCompatTheme_listChoiceBackgroundIndicator = 4116;

    @StyleableRes
    public static final int AppCompatTheme_listChoiceIndicatorMultipleAnimated = 4117;

    @StyleableRes
    public static final int AppCompatTheme_listChoiceIndicatorSingleAnimated = 4118;

    @StyleableRes
    public static final int AppCompatTheme_listDividerAlertDialog = 4119;

    @StyleableRes
    public static final int AppCompatTheme_listMenuViewStyle = 4120;

    @StyleableRes
    public static final int AppCompatTheme_listPopupWindowStyle = 4121;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemHeight = 4122;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemHeightLarge = 4123;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemHeightSmall = 4124;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemPaddingEnd = 4125;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemPaddingLeft = 4126;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemPaddingRight = 4127;

    @StyleableRes
    public static final int AppCompatTheme_listPreferredItemPaddingStart = 4128;

    @StyleableRes
    public static final int AppCompatTheme_panelBackground = 4129;

    @StyleableRes
    public static final int AppCompatTheme_panelMenuListTheme = 4130;

    @StyleableRes
    public static final int AppCompatTheme_panelMenuListWidth = 4131;

    @StyleableRes
    public static final int AppCompatTheme_popupMenuStyle = 4132;

    @StyleableRes
    public static final int AppCompatTheme_popupWindowStyle = 4133;

    @StyleableRes
    public static final int AppCompatTheme_radioButtonStyle = 4134;

    @StyleableRes
    public static final int AppCompatTheme_ratingBarStyle = 4135;

    @StyleableRes
    public static final int AppCompatTheme_ratingBarStyleIndicator = 4136;

    @StyleableRes
    public static final int AppCompatTheme_ratingBarStyleSmall = 4137;

    @StyleableRes
    public static final int AppCompatTheme_searchViewStyle = 4138;

    @StyleableRes
    public static final int AppCompatTheme_seekBarStyle = 4139;

    @StyleableRes
    public static final int AppCompatTheme_selectableItemBackground = 4140;

    @StyleableRes
    public static final int AppCompatTheme_selectableItemBackgroundBorderless = 4141;

    @StyleableRes
    public static final int AppCompatTheme_spinnerDropDownItemStyle = 4142;

    @StyleableRes
    public static final int AppCompatTheme_spinnerStyle = 4143;

    @StyleableRes
    public static final int AppCompatTheme_switchStyle = 4144;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceLargePopupMenu = 4145;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceListItem = 4146;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceListItemSecondary = 4147;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceListItemSmall = 4148;

    @StyleableRes
    public static final int AppCompatTheme_textAppearancePopupMenuHeader = 4149;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceSearchResultSubtitle = 4150;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceSearchResultTitle = 4151;

    @StyleableRes
    public static final int AppCompatTheme_textAppearanceSmallPopupMenu = 4152;

    @StyleableRes
    public static final int AppCompatTheme_textColorAlertDialogListItem = 4153;

    @StyleableRes
    public static final int AppCompatTheme_textColorSearchUrl = 4154;

    @StyleableRes
    public static final int AppCompatTheme_toolbarNavigationButtonStyle = 4155;

    @StyleableRes
    public static final int AppCompatTheme_toolbarStyle = 4156;

    @StyleableRes
    public static final int AppCompatTheme_tooltipForegroundColor = 4157;

    @StyleableRes
    public static final int AppCompatTheme_tooltipFrameBackground = 4158;

    @StyleableRes
    public static final int AppCompatTheme_viewInflaterClass = 4159;

    @StyleableRes
    public static final int AppCompatTheme_windowActionBar = 4160;

    @StyleableRes
    public static final int AppCompatTheme_windowActionBarOverlay = 4161;

    @StyleableRes
    public static final int AppCompatTheme_windowActionModeOverlay = 4162;

    @StyleableRes
    public static final int AppCompatTheme_windowFixedHeightMajor = 4163;

    @StyleableRes
    public static final int AppCompatTheme_windowFixedHeightMinor = 4164;

    @StyleableRes
    public static final int AppCompatTheme_windowFixedWidthMajor = 4165;

    @StyleableRes
    public static final int AppCompatTheme_windowFixedWidthMinor = 4166;

    @StyleableRes
    public static final int AppCompatTheme_windowMinWidthMajor = 4167;

    @StyleableRes
    public static final int AppCompatTheme_windowMinWidthMinor = 4168;

    @StyleableRes
    public static final int AppCompatTheme_windowNoTitle = 4169;

    @StyleableRes
    public static final int BaseRatingBar_srb_clearRatingEnabled = 4170;

    @StyleableRes
    public static final int BaseRatingBar_srb_clickable = 4171;

    @StyleableRes
    public static final int BaseRatingBar_srb_drawableEmpty = 4172;

    @StyleableRes
    public static final int BaseRatingBar_srb_drawableFilled = 4173;

    @StyleableRes
    public static final int BaseRatingBar_srb_isIndicator = 4174;

    @StyleableRes
    public static final int BaseRatingBar_srb_minimumStars = 4175;

    @StyleableRes
    public static final int BaseRatingBar_srb_numStars = 4176;

    @StyleableRes
    public static final int BaseRatingBar_srb_rating = 4177;

    @StyleableRes
    public static final int BaseRatingBar_srb_scrollable = 4178;

    @StyleableRes
    public static final int BaseRatingBar_srb_starHeight = 4179;

    @StyleableRes
    public static final int BaseRatingBar_srb_starPadding = 4180;

    @StyleableRes
    public static final int BaseRatingBar_srb_starWidth = 4181;

    @StyleableRes
    public static final int BaseRatingBar_srb_stepSize = 4182;

    @StyleableRes
    public static final int BottomAppBar_backgroundTint = 4183;

    @StyleableRes
    public static final int BottomAppBar_fabAlignmentMode = 4184;

    @StyleableRes
    public static final int BottomAppBar_fabCradleMargin = 4185;

    @StyleableRes
    public static final int BottomAppBar_fabCradleRoundedCornerRadius = 4186;

    @StyleableRes
    public static final int BottomAppBar_fabCradleVerticalOffset = 4187;

    @StyleableRes
    public static final int BottomAppBar_hideOnScroll = 4188;

    @StyleableRes
    public static final int BottomNavigationView_elevation = 4189;

    @StyleableRes
    public static final int BottomNavigationView_itemBackground = 4190;

    @StyleableRes
    public static final int BottomNavigationView_itemHorizontalTranslationEnabled = 4191;

    @StyleableRes
    public static final int BottomNavigationView_itemIconSize = 4192;

    @StyleableRes
    public static final int BottomNavigationView_itemIconTint = 4193;

    @StyleableRes
    public static final int BottomNavigationView_itemTextAppearanceActive = 4194;

    @StyleableRes
    public static final int BottomNavigationView_itemTextAppearanceInactive = 4195;

    @StyleableRes
    public static final int BottomNavigationView_itemTextColor = 4196;

    @StyleableRes
    public static final int BottomNavigationView_labelVisibilityMode = 4197;

    @StyleableRes
    public static final int BottomNavigationView_menu = 4198;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_fitToContents = 4199;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_hideable = 4200;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_peekHeight = 4201;

    @StyleableRes
    public static final int BottomSheetBehavior_Layout_behavior_skipCollapsed = 4202;

    @StyleableRes
    public static final int ButtonBarLayout_allowStacking = 4203;

    @StyleableRes
    public static final int CardInputView_cardHintText = 4204;

    @StyleableRes
    public static final int CardInputView_cardTextErrorColor = 4205;

    @StyleableRes
    public static final int CardInputView_cardTint = 4206;

    @StyleableRes
    public static final int CardMultilineWidget_shouldShowPostalCode = 4207;

    @StyleableRes
    public static final int CardView_android_minHeight = 4208;

    @StyleableRes
    public static final int CardView_android_minWidth = 4209;

    @StyleableRes
    public static final int CardView_cardBackgroundColor = 4210;

    @StyleableRes
    public static final int CardView_cardCornerRadius = 4211;

    @StyleableRes
    public static final int CardView_cardElevation = 4212;

    @StyleableRes
    public static final int CardView_cardMaxElevation = 4213;

    @StyleableRes
    public static final int CardView_cardPreventCornerOverlap = 4214;

    @StyleableRes
    public static final int CardView_cardUseCompatPadding = 4215;

    @StyleableRes
    public static final int CardView_contentPadding = 4216;

    @StyleableRes
    public static final int CardView_contentPaddingBottom = 4217;

    @StyleableRes
    public static final int CardView_contentPaddingLeft = 4218;

    @StyleableRes
    public static final int CardView_contentPaddingRight = 4219;

    @StyleableRes
    public static final int CardView_contentPaddingTop = 4220;

    @StyleableRes
    public static final int Chip_android_checkable = 4221;

    @StyleableRes
    public static final int Chip_android_ellipsize = 4222;

    @StyleableRes
    public static final int Chip_android_maxWidth = 4223;

    @StyleableRes
    public static final int Chip_android_text = 4224;

    @StyleableRes
    public static final int Chip_android_textAppearance = 4225;

    @StyleableRes
    public static final int Chip_checkedIcon = 4226;

    @StyleableRes
    public static final int Chip_checkedIconEnabled = 4227;

    @StyleableRes
    public static final int Chip_checkedIconVisible = 4228;

    @StyleableRes
    public static final int Chip_chipBackgroundColor = 4229;

    @StyleableRes
    public static final int Chip_chipCornerRadius = 4230;

    @StyleableRes
    public static final int Chip_chipEndPadding = 4231;

    @StyleableRes
    public static final int Chip_chipIcon = 4232;

    @StyleableRes
    public static final int Chip_chipIconEnabled = 4233;

    @StyleableRes
    public static final int Chip_chipIconSize = 4234;

    @StyleableRes
    public static final int Chip_chipIconTint = 4235;

    @StyleableRes
    public static final int Chip_chipIconVisible = 4236;

    @StyleableRes
    public static final int Chip_chipMinHeight = 4237;

    @StyleableRes
    public static final int Chip_chipStartPadding = 4238;

    @StyleableRes
    public static final int Chip_chipStrokeColor = 4239;

    @StyleableRes
    public static final int Chip_chipStrokeWidth = 4240;

    @StyleableRes
    public static final int Chip_closeIcon = 4241;

    @StyleableRes
    public static final int Chip_closeIconEnabled = 4242;

    @StyleableRes
    public static final int Chip_closeIconEndPadding = 4243;

    @StyleableRes
    public static final int Chip_closeIconSize = 4244;

    @StyleableRes
    public static final int Chip_closeIconStartPadding = 4245;

    @StyleableRes
    public static final int Chip_closeIconTint = 4246;

    @StyleableRes
    public static final int Chip_closeIconVisible = 4247;

    @StyleableRes
    public static final int Chip_hideMotionSpec = 4248;

    @StyleableRes
    public static final int Chip_iconEndPadding = 4249;

    @StyleableRes
    public static final int Chip_iconStartPadding = 4250;

    @StyleableRes
    public static final int Chip_rippleColor = 4251;

    @StyleableRes
    public static final int Chip_showMotionSpec = 4252;

    @StyleableRes
    public static final int Chip_textEndPadding = 4253;

    @StyleableRes
    public static final int Chip_textStartPadding = 4254;

    @StyleableRes
    public static final int ChipGroup_checkedChip = 4255;

    @StyleableRes
    public static final int ChipGroup_chipSpacing = 4256;

    @StyleableRes
    public static final int ChipGroup_chipSpacingHorizontal = 4257;

    @StyleableRes
    public static final int ChipGroup_chipSpacingVertical = 4258;

    @StyleableRes
    public static final int ChipGroup_singleLine = 4259;

    @StyleableRes
    public static final int ChipGroup_singleSelection = 4260;

    @StyleableRes
    public static final int CircleImageView_civ_border_color = 4261;

    @StyleableRes
    public static final int CircleImageView_civ_border_overlay = 4262;

    @StyleableRes
    public static final int CircleImageView_civ_border_width = 4263;

    @StyleableRes
    public static final int CircleImageView_civ_circle_background_color = 4264;

    @StyleableRes
    public static final int CircleIndicator_ci_animator = 4265;

    @StyleableRes
    public static final int CircleIndicator_ci_animator_reverse = 4266;

    @StyleableRes
    public static final int CircleIndicator_ci_drawable = 4267;

    @StyleableRes
    public static final int CircleIndicator_ci_drawable_unselected = 4268;

    @StyleableRes
    public static final int CircleIndicator_ci_gravity = 4269;

    @StyleableRes
    public static final int CircleIndicator_ci_height = 4270;

    @StyleableRes
    public static final int CircleIndicator_ci_margin = 4271;

    @StyleableRes
    public static final int CircleIndicator_ci_orientation = 4272;

    @StyleableRes
    public static final int CircleIndicator_ci_width = 4273;

    @StyleableRes
    public static final int CollapsingToolbarLayout_collapsedTitleGravity = 4274;

    @StyleableRes
    public static final int CollapsingToolbarLayout_collapsedTitleTextAppearance = 4275;

    @StyleableRes
    public static final int CollapsingToolbarLayout_contentScrim = 4276;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleGravity = 4277;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMargin = 4278;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMarginBottom = 4279;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMarginEnd = 4280;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMarginStart = 4281;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleMarginTop = 4282;

    @StyleableRes
    public static final int CollapsingToolbarLayout_expandedTitleTextAppearance = 4283;

    @StyleableRes
    public static final int CollapsingToolbarLayout_scrimAnimationDuration = 4284;

    @StyleableRes
    public static final int CollapsingToolbarLayout_scrimVisibleHeightTrigger = 4285;

    @StyleableRes
    public static final int CollapsingToolbarLayout_statusBarScrim = 4286;

    @StyleableRes
    public static final int CollapsingToolbarLayout_title = 4287;

    @StyleableRes
    public static final int CollapsingToolbarLayout_titleEnabled = 4288;

    @StyleableRes
    public static final int CollapsingToolbarLayout_toolbarId = 4289;

    @StyleableRes
    public static final int CollapsingToolbarLayout_Layout_layout_collapseMode = 4290;

    @StyleableRes
    public static final int CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier = 4291;

    @StyleableRes
    public static final int ColorStateListItem_alpha = 4292;

    @StyleableRes
    public static final int ColorStateListItem_android_alpha = 4293;

    @StyleableRes
    public static final int ColorStateListItem_android_color = 4294;

    @StyleableRes
    public static final int CommonTabLayout_tl_divider_color = 4295;

    @StyleableRes
    public static final int CommonTabLayout_tl_divider_padding = 4296;

    @StyleableRes
    public static final int CommonTabLayout_tl_divider_width = 4297;

    @StyleableRes
    public static final int CommonTabLayout_tl_iconGravity = 4298;

    @StyleableRes
    public static final int CommonTabLayout_tl_iconHeight = 4299;

    @StyleableRes
    public static final int CommonTabLayout_tl_iconMargin = 4300;

    @StyleableRes
    public static final int CommonTabLayout_tl_iconVisible = 4301;

    @StyleableRes
    public static final int CommonTabLayout_tl_iconWidth = 4302;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_anim_duration = 4303;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_anim_enable = 4304;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_bounce_enable = 4305;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_color = 4306;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_corner_radius = 4307;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_gravity = 4308;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_height = 4309;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_margin_bottom = 4310;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_margin_left = 4311;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_margin_right = 4312;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_margin_top = 4313;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_style = 4314;

    @StyleableRes
    public static final int CommonTabLayout_tl_indicator_width = 4315;

    @StyleableRes
    public static final int CommonTabLayout_tl_tab_padding = 4316;

    @StyleableRes
    public static final int CommonTabLayout_tl_tab_space_equal = 4317;

    @StyleableRes
    public static final int CommonTabLayout_tl_tab_width = 4318;

    @StyleableRes
    public static final int CommonTabLayout_tl_textAllCaps = 4319;

    @StyleableRes
    public static final int CommonTabLayout_tl_textBold = 4320;

    @StyleableRes
    public static final int CommonTabLayout_tl_textSelectColor = 4321;

    @StyleableRes
    public static final int CommonTabLayout_tl_textUnselectColor = 4322;

    @StyleableRes
    public static final int CommonTabLayout_tl_textsize = 4323;

    @StyleableRes
    public static final int CommonTabLayout_tl_underline_color = 4324;

    @StyleableRes
    public static final int CommonTabLayout_tl_underline_gravity = 4325;

    @StyleableRes
    public static final int CommonTabLayout_tl_underline_height = 4326;

    @StyleableRes
    public static final int CompoundButton_android_button = 4327;

    @StyleableRes
    public static final int CompoundButton_buttonCompat = 4328;

    @StyleableRes
    public static final int CompoundButton_buttonTint = 4329;

    @StyleableRes
    public static final int CompoundButton_buttonTintMode = 4330;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_maxHeight = 4331;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_maxWidth = 4332;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_minHeight = 4333;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_minWidth = 4334;

    @StyleableRes
    public static final int ConstraintLayout_Layout_android_orientation = 4335;

    @StyleableRes
    public static final int ConstraintLayout_Layout_barrierAllowsGoneWidgets = 4336;

    @StyleableRes
    public static final int ConstraintLayout_Layout_barrierDirection = 4337;

    @StyleableRes
    public static final int ConstraintLayout_Layout_chainUseRtl = 4338;

    @StyleableRes
    public static final int ConstraintLayout_Layout_constraintSet = 4339;

    @StyleableRes
    public static final int ConstraintLayout_Layout_constraint_referenced_ids = 4340;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constrainedHeight = 4341;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constrainedWidth = 4342;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBaseline_creator = 4343;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf = 4344;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBottom_creator = 4345;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBottom_toBottomOf = 4346;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintBottom_toTopOf = 4347;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintCircle = 4348;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintCircleAngle = 4349;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintCircleRadius = 4350;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintDimensionRatio = 4351;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintEnd_toEndOf = 4352;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintEnd_toStartOf = 4353;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintGuide_begin = 4354;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintGuide_end = 4355;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintGuide_percent = 4356;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHeight_default = 4357;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHeight_max = 4358;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHeight_min = 4359;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHeight_percent = 4360;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHorizontal_bias = 4361;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle = 4362;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintHorizontal_weight = 4363;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintLeft_creator = 4364;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintLeft_toLeftOf = 4365;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintLeft_toRightOf = 4366;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintRight_creator = 4367;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintRight_toLeftOf = 4368;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintRight_toRightOf = 4369;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintStart_toEndOf = 4370;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintStart_toStartOf = 4371;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintTop_creator = 4372;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintTop_toBottomOf = 4373;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintTop_toTopOf = 4374;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintVertical_bias = 4375;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintVertical_chainStyle = 4376;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintVertical_weight = 4377;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintWidth_default = 4378;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintWidth_max = 4379;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintWidth_min = 4380;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_constraintWidth_percent = 4381;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_editor_absoluteX = 4382;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_editor_absoluteY = 4383;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginBottom = 4384;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginEnd = 4385;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginLeft = 4386;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginRight = 4387;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginStart = 4388;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_goneMarginTop = 4389;

    @StyleableRes
    public static final int ConstraintLayout_Layout_layout_optimizationLevel = 4390;

    @StyleableRes
    public static final int ConstraintLayout_placeholder_content = 4391;

    @StyleableRes
    public static final int ConstraintLayout_placeholder_emptyVisibility = 4392;

    @StyleableRes
    public static final int ConstraintSet_android_alpha = 4393;

    @StyleableRes
    public static final int ConstraintSet_android_elevation = 4394;

    @StyleableRes
    public static final int ConstraintSet_android_id = 4395;

    @StyleableRes
    public static final int ConstraintSet_android_layout_height = 4396;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginBottom = 4397;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginEnd = 4398;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginLeft = 4399;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginRight = 4400;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginStart = 4401;

    @StyleableRes
    public static final int ConstraintSet_android_layout_marginTop = 4402;

    @StyleableRes
    public static final int ConstraintSet_android_layout_width = 4403;

    @StyleableRes
    public static final int ConstraintSet_android_maxHeight = 4404;

    @StyleableRes
    public static final int ConstraintSet_android_maxWidth = 4405;

    @StyleableRes
    public static final int ConstraintSet_android_minHeight = 4406;

    @StyleableRes
    public static final int ConstraintSet_android_minWidth = 4407;

    @StyleableRes
    public static final int ConstraintSet_android_orientation = 4408;

    @StyleableRes
    public static final int ConstraintSet_android_rotation = 4409;

    @StyleableRes
    public static final int ConstraintSet_android_rotationX = 4410;

    @StyleableRes
    public static final int ConstraintSet_android_rotationY = 4411;

    @StyleableRes
    public static final int ConstraintSet_android_scaleX = 4412;

    @StyleableRes
    public static final int ConstraintSet_android_scaleY = 4413;

    @StyleableRes
    public static final int ConstraintSet_android_transformPivotX = 4414;

    @StyleableRes
    public static final int ConstraintSet_android_transformPivotY = 4415;

    @StyleableRes
    public static final int ConstraintSet_android_translationX = 4416;

    @StyleableRes
    public static final int ConstraintSet_android_translationY = 4417;

    @StyleableRes
    public static final int ConstraintSet_android_translationZ = 4418;

    @StyleableRes
    public static final int ConstraintSet_android_visibility = 4419;

    @StyleableRes
    public static final int ConstraintSet_barrierAllowsGoneWidgets = 4420;

    @StyleableRes
    public static final int ConstraintSet_barrierDirection = 4421;

    @StyleableRes
    public static final int ConstraintSet_chainUseRtl = 4422;

    @StyleableRes
    public static final int ConstraintSet_constraint_referenced_ids = 4423;

    @StyleableRes
    public static final int ConstraintSet_layout_constrainedHeight = 4424;

    @StyleableRes
    public static final int ConstraintSet_layout_constrainedWidth = 4425;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBaseline_creator = 4426;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBaseline_toBaselineOf = 4427;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBottom_creator = 4428;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBottom_toBottomOf = 4429;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintBottom_toTopOf = 4430;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintCircle = 4431;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintCircleAngle = 4432;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintCircleRadius = 4433;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintDimensionRatio = 4434;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintEnd_toEndOf = 4435;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintEnd_toStartOf = 4436;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintGuide_begin = 4437;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintGuide_end = 4438;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintGuide_percent = 4439;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHeight_default = 4440;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHeight_max = 4441;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHeight_min = 4442;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHeight_percent = 4443;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHorizontal_bias = 4444;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHorizontal_chainStyle = 4445;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintHorizontal_weight = 4446;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintLeft_creator = 4447;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintLeft_toLeftOf = 4448;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintLeft_toRightOf = 4449;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintRight_creator = 4450;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintRight_toLeftOf = 4451;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintRight_toRightOf = 4452;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintStart_toEndOf = 4453;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintStart_toStartOf = 4454;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintTop_creator = 4455;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintTop_toBottomOf = 4456;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintTop_toTopOf = 4457;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintVertical_bias = 4458;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintVertical_chainStyle = 4459;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintVertical_weight = 4460;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintWidth_default = 4461;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintWidth_max = 4462;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintWidth_min = 4463;

    @StyleableRes
    public static final int ConstraintSet_layout_constraintWidth_percent = 4464;

    @StyleableRes
    public static final int ConstraintSet_layout_editor_absoluteX = 4465;

    @StyleableRes
    public static final int ConstraintSet_layout_editor_absoluteY = 4466;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginBottom = 4467;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginEnd = 4468;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginLeft = 4469;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginRight = 4470;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginStart = 4471;

    @StyleableRes
    public static final int ConstraintSet_layout_goneMarginTop = 4472;

    @StyleableRes
    public static final int CoordinatorLayout_keylines = 4473;

    @StyleableRes
    public static final int CoordinatorLayout_statusBarBackground = 4474;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_android_layout_gravity = 4475;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_anchor = 4476;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_anchorGravity = 4477;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_behavior = 4478;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4479;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_insetEdge = 4480;

    @StyleableRes
    public static final int CoordinatorLayout_Layout_layout_keyline = 4481;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_allowSearch = 4482;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_backgroundColor = 4483;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_fastScroller_bubbleColor = 4484;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_fastScroller_bubbleTextAppearance = 4485;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_fastScroller_handleColor = 4486;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_initialScrollToSelection = 4487;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_keyboardAutoPopup = 4488;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_searchEditTextTint = 4489;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_showCloseIcon = 4490;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_showFastScroller = 4491;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_showFlag = 4492;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_showNameCode = 4493;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_showPhoneCode = 4494;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_showTitle = 4495;

    @StyleableRes
    public static final int CountryCodePicker_ccpDialog_textColor = 4496;

    @StyleableRes
    public static final int CountryCodePicker_ccp_areaCodeDetectedCountry = 4497;

    @StyleableRes
    public static final int CountryCodePicker_ccp_arrowColor = 4498;

    @StyleableRes
    public static final int CountryCodePicker_ccp_arrowSize = 4499;

    @StyleableRes
    public static final int CountryCodePicker_ccp_autoDetectCountry = 4500;

    @StyleableRes
    public static final int CountryCodePicker_ccp_autoDetectLanguage = 4501;

    @StyleableRes
    public static final int CountryCodePicker_ccp_autoFormatNumber = 4502;

    @StyleableRes
    public static final int CountryCodePicker_ccp_clickable = 4503;

    @StyleableRes
    public static final int CountryCodePicker_ccp_contentColor = 4504;

    @StyleableRes
    public static final int CountryCodePicker_ccp_countryAutoDetectionPref = 4505;

    @StyleableRes
    public static final int CountryCodePicker_ccp_countryPreference = 4506;

    @StyleableRes
    public static final int CountryCodePicker_ccp_customMasterCountries = 4507;

    @StyleableRes
    public static final int CountryCodePicker_ccp_defaultLanguage = 4508;

    @StyleableRes
    public static final int CountryCodePicker_ccp_defaultNameCode = 4509;

    @StyleableRes
    public static final int CountryCodePicker_ccp_defaultPhoneCode = 4510;

    @StyleableRes
    public static final int CountryCodePicker_ccp_excludedCountries = 4511;

    @StyleableRes
    public static final int CountryCodePicker_ccp_flagBorderColor = 4512;

    @StyleableRes
    public static final int CountryCodePicker_ccp_hintExampleNumber = 4513;

    @StyleableRes
    public static final int CountryCodePicker_ccp_hintExampleNumberType = 4514;

    @StyleableRes
    public static final int CountryCodePicker_ccp_internationalFormattingOnly = 4515;

    @StyleableRes
    public static final int CountryCodePicker_ccp_rememberLastSelection = 4516;

    @StyleableRes
    public static final int CountryCodePicker_ccp_selectionMemoryTag = 4517;

    @StyleableRes
    public static final int CountryCodePicker_ccp_showArrow = 4518;

    @StyleableRes
    public static final int CountryCodePicker_ccp_showFlag = 4519;

    @StyleableRes
    public static final int CountryCodePicker_ccp_showFullName = 4520;

    @StyleableRes
    public static final int CountryCodePicker_ccp_showNameCode = 4521;

    @StyleableRes
    public static final int CountryCodePicker_ccp_showPhoneCode = 4522;

    @StyleableRes
    public static final int CountryCodePicker_ccp_textGravity = 4523;

    @StyleableRes
    public static final int CountryCodePicker_ccp_textSize = 4524;

    @StyleableRes
    public static final int CountryCodePicker_ccp_useDummyEmojiForPreview = 4525;

    @StyleableRes
    public static final int CountryCodePicker_ccp_useFlagEmoji = 4526;

    @StyleableRes
    public static final int CustomTextView_font_name = 4527;

    @StyleableRes
    public static final int DesignTheme_bottomSheetDialogTheme = 4528;

    @StyleableRes
    public static final int DesignTheme_bottomSheetStyle = 4529;

    @StyleableRes
    public static final int DesignTheme_textColorError = 4530;

    @StyleableRes
    public static final int DividerView_color = 4531;

    @StyleableRes
    public static final int DividerView_dashGap = 4532;

    @StyleableRes
    public static final int DividerView_dashLength = 4533;

    @StyleableRes
    public static final int DividerView_dashThickness = 4534;

    @StyleableRes
    public static final int DividerView_orientation = 4535;

    @StyleableRes
    public static final int DrawerArrowToggle_arrowHeadLength = 4536;

    @StyleableRes
    public static final int DrawerArrowToggle_arrowShaftLength = 4537;

    @StyleableRes
    public static final int DrawerArrowToggle_barLength = 4538;

    @StyleableRes
    public static final int DrawerArrowToggle_color = 4539;

    @StyleableRes
    public static final int DrawerArrowToggle_drawableSize = 4540;

    @StyleableRes
    public static final int DrawerArrowToggle_gapBetweenBars = 4541;

    @StyleableRes
    public static final int DrawerArrowToggle_spinBars = 4542;

    @StyleableRes
    public static final int DrawerArrowToggle_thickness = 4543;

    @StyleableRes
    public static final int ExpandableLayout_canExpand = 4544;

    @StyleableRes
    public static final int ExpandableLayout_startExpanded = 4545;

    @StyleableRes
    public static final int FloatingActionButton_backgroundTint = 4546;

    @StyleableRes
    public static final int FloatingActionButton_backgroundTintMode = 4547;

    @StyleableRes
    public static final int FloatingActionButton_borderWidth = 4548;

    @StyleableRes
    public static final int FloatingActionButton_elevation = 4549;

    @StyleableRes
    public static final int FloatingActionButton_fabCustomSize = 4550;

    @StyleableRes
    public static final int FloatingActionButton_fabSize = 4551;

    @StyleableRes
    public static final int FloatingActionButton_hideMotionSpec = 4552;

    @StyleableRes
    public static final int FloatingActionButton_hoveredFocusedTranslationZ = 4553;

    @StyleableRes
    public static final int FloatingActionButton_maxImageSize = 4554;

    @StyleableRes
    public static final int FloatingActionButton_pressedTranslationZ = 4555;

    @StyleableRes
    public static final int FloatingActionButton_rippleColor = 4556;

    @StyleableRes
    public static final int FloatingActionButton_showMotionSpec = 4557;

    @StyleableRes
    public static final int FloatingActionButton_useCompatPadding = 4558;

    @StyleableRes
    public static final int FloatingActionButton_Behavior_Layout_behavior_autoHide = 4559;

    @StyleableRes
    public static final int FlowLayout_itemSpacing = 4560;

    @StyleableRes
    public static final int FlowLayout_lineSpacing = 4561;

    @StyleableRes
    public static final int FontFamily_fontProviderAuthority = 4562;

    @StyleableRes
    public static final int FontFamily_fontProviderCerts = 4563;

    @StyleableRes
    public static final int FontFamily_fontProviderFetchStrategy = 4564;

    @StyleableRes
    public static final int FontFamily_fontProviderFetchTimeout = 4565;

    @StyleableRes
    public static final int FontFamily_fontProviderPackage = 4566;

    @StyleableRes
    public static final int FontFamily_fontProviderQuery = 4567;

    @StyleableRes
    public static final int FontFamilyFont_android_font = 4568;

    @StyleableRes
    public static final int FontFamilyFont_android_fontStyle = 4569;

    @StyleableRes
    public static final int FontFamilyFont_android_fontVariationSettings = 4570;

    @StyleableRes
    public static final int FontFamilyFont_android_fontWeight = 4571;

    @StyleableRes
    public static final int FontFamilyFont_android_ttcIndex = 4572;

    @StyleableRes
    public static final int FontFamilyFont_font = 4573;

    @StyleableRes
    public static final int FontFamilyFont_fontStyle = 4574;

    @StyleableRes
    public static final int FontFamilyFont_fontVariationSettings = 4575;

    @StyleableRes
    public static final int FontFamilyFont_fontWeight = 4576;

    @StyleableRes
    public static final int FontFamilyFont_ttcIndex = 4577;

    @StyleableRes
    public static final int ForegroundLinearLayout_android_foreground = 4578;

    @StyleableRes
    public static final int ForegroundLinearLayout_android_foregroundGravity = 4579;

    @StyleableRes
    public static final int ForegroundLinearLayout_foregroundInsidePadding = 4580;

    @StyleableRes
    public static final int Fragment_android_id = 4581;

    @StyleableRes
    public static final int Fragment_android_name = 4582;

    @StyleableRes
    public static final int Fragment_android_tag = 4583;

    @StyleableRes
    public static final int FragmentContainerView_android_name = 4584;

    @StyleableRes
    public static final int FragmentContainerView_android_tag = 4585;

    @StyleableRes
    public static final int GradientColor_android_centerColor = 4586;

    @StyleableRes
    public static final int GradientColor_android_centerX = 4587;

    @StyleableRes
    public static final int GradientColor_android_centerY = 4588;

    @StyleableRes
    public static final int GradientColor_android_endColor = 4589;

    @StyleableRes
    public static final int GradientColor_android_endX = 4590;

    @StyleableRes
    public static final int GradientColor_android_endY = 4591;

    @StyleableRes
    public static final int GradientColor_android_gradientRadius = 4592;

    @StyleableRes
    public static final int GradientColor_android_startColor = 4593;

    @StyleableRes
    public static final int GradientColor_android_startX = 4594;

    @StyleableRes
    public static final int GradientColor_android_startY = 4595;

    @StyleableRes
    public static final int GradientColor_android_tileMode = 4596;

    @StyleableRes
    public static final int GradientColor_android_type = 4597;

    @StyleableRes
    public static final int GradientColorItem_android_color = 4598;

    @StyleableRes
    public static final int GradientColorItem_android_offset = 4599;

    @StyleableRes
    public static final int LinearConstraintLayout_android_orientation = 4600;

    @StyleableRes
    public static final int LinearLayoutCompat_android_baselineAligned = 4601;

    @StyleableRes
    public static final int LinearLayoutCompat_android_baselineAlignedChildIndex = 4602;

    @StyleableRes
    public static final int LinearLayoutCompat_android_gravity = 4603;

    @StyleableRes
    public static final int LinearLayoutCompat_android_orientation = 4604;

    @StyleableRes
    public static final int LinearLayoutCompat_android_weightSum = 4605;

    @StyleableRes
    public static final int LinearLayoutCompat_divider = 4606;

    @StyleableRes
    public static final int LinearLayoutCompat_dividerPadding = 4607;

    @StyleableRes
    public static final int LinearLayoutCompat_measureWithLargestChild = 4608;

    @StyleableRes
    public static final int LinearLayoutCompat_showDividers = 4609;

    @StyleableRes
    public static final int LinearLayoutCompat_Layout_android_layout_gravity = 4610;

    @StyleableRes
    public static final int LinearLayoutCompat_Layout_android_layout_height = 4611;

    @StyleableRes
    public static final int LinearLayoutCompat_Layout_android_layout_weight = 4612;

    @StyleableRes
    public static final int LinearLayoutCompat_Layout_android_layout_width = 4613;

    @StyleableRes
    public static final int ListPopupWindow_android_dropDownHorizontalOffset = 4614;

    @StyleableRes
    public static final int ListPopupWindow_android_dropDownVerticalOffset = 4615;

    @StyleableRes
    public static final int LoadingImageView_circleCrop = 4616;

    @StyleableRes
    public static final int LoadingImageView_imageAspectRatio = 4617;

    @StyleableRes
    public static final int LoadingImageView_imageAspectRatioAdjust = 4618;

    @StyleableRes
    public static final int MapAttrs_ambientEnabled = 4619;

    @StyleableRes
    public static final int MapAttrs_cameraBearing = 4620;

    @StyleableRes
    public static final int MapAttrs_cameraMaxZoomPreference = 4621;

    @StyleableRes
    public static final int MapAttrs_cameraMinZoomPreference = 4622;

    @StyleableRes
    public static final int MapAttrs_cameraTargetLat = 4623;

    @StyleableRes
    public static final int MapAttrs_cameraTargetLng = 4624;

    @StyleableRes
    public static final int MapAttrs_cameraTilt = 4625;

    @StyleableRes
    public static final int MapAttrs_cameraZoom = 4626;

    @StyleableRes
    public static final int MapAttrs_latLngBoundsNorthEastLatitude = 4627;

    @StyleableRes
    public static final int MapAttrs_latLngBoundsNorthEastLongitude = 4628;

    @StyleableRes
    public static final int MapAttrs_latLngBoundsSouthWestLatitude = 4629;

    @StyleableRes
    public static final int MapAttrs_latLngBoundsSouthWestLongitude = 4630;

    @StyleableRes
    public static final int MapAttrs_liteMode = 4631;

    @StyleableRes
    public static final int MapAttrs_mapType = 4632;

    @StyleableRes
    public static final int MapAttrs_uiCompass = 4633;

    @StyleableRes
    public static final int MapAttrs_uiMapToolbar = 4634;

    @StyleableRes
    public static final int MapAttrs_uiRotateGestures = 4635;

    @StyleableRes
    public static final int MapAttrs_uiScrollGestures = 4636;

    @StyleableRes
    public static final int MapAttrs_uiScrollGesturesDuringRotateOrZoom = 4637;

    @StyleableRes
    public static final int MapAttrs_uiTiltGestures = 4638;

    @StyleableRes
    public static final int MapAttrs_uiZoomControls = 4639;

    @StyleableRes
    public static final int MapAttrs_uiZoomGestures = 4640;

    @StyleableRes
    public static final int MapAttrs_useViewLifecycle = 4641;

    @StyleableRes
    public static final int MapAttrs_zOrderOnTop = 4642;

    @StyleableRes
    public static final int MaterialButton_android_insetBottom = 4643;

    @StyleableRes
    public static final int MaterialButton_android_insetLeft = 4644;

    @StyleableRes
    public static final int MaterialButton_android_insetRight = 4645;

    @StyleableRes
    public static final int MaterialButton_android_insetTop = 4646;

    @StyleableRes
    public static final int MaterialButton_backgroundTint = 4647;

    @StyleableRes
    public static final int MaterialButton_backgroundTintMode = 4648;

    @StyleableRes
    public static final int MaterialButton_cornerRadius = 4649;

    @StyleableRes
    public static final int MaterialButton_icon = 4650;

    @StyleableRes
    public static final int MaterialButton_iconGravity = 4651;

    @StyleableRes
    public static final int MaterialButton_iconPadding = 4652;

    @StyleableRes
    public static final int MaterialButton_iconSize = 4653;

    @StyleableRes
    public static final int MaterialButton_iconTint = 4654;

    @StyleableRes
    public static final int MaterialButton_iconTintMode = 4655;

    @StyleableRes
    public static final int MaterialButton_rippleColor = 4656;

    @StyleableRes
    public static final int MaterialButton_strokeColor = 4657;

    @StyleableRes
    public static final int MaterialButton_strokeWidth = 4658;

    @StyleableRes
    public static final int MaterialCardView_strokeColor = 4659;

    @StyleableRes
    public static final int MaterialCardView_strokeWidth = 4660;

    @StyleableRes
    public static final int MaterialComponentsTheme_bottomSheetDialogTheme = 4661;

    @StyleableRes
    public static final int MaterialComponentsTheme_bottomSheetStyle = 4662;

    @StyleableRes
    public static final int MaterialComponentsTheme_chipGroupStyle = 4663;

    @StyleableRes
    public static final int MaterialComponentsTheme_chipStandaloneStyle = 4664;

    @StyleableRes
    public static final int MaterialComponentsTheme_chipStyle = 4665;

    @StyleableRes
    public static final int MaterialComponentsTheme_colorAccent = 4666;

    @StyleableRes
    public static final int MaterialComponentsTheme_colorBackgroundFloating = 4667;

    @StyleableRes
    public static final int MaterialComponentsTheme_colorPrimary = 4668;

    @StyleableRes
    public static final int MaterialComponentsTheme_colorPrimaryDark = 4669;

    @StyleableRes
    public static final int MaterialComponentsTheme_colorSecondary = 4670;

    @StyleableRes
    public static final int MaterialComponentsTheme_editTextStyle = 4671;

    @StyleableRes
    public static final int MaterialComponentsTheme_floatingActionButtonStyle = 4672;

    @StyleableRes
    public static final int MaterialComponentsTheme_materialButtonStyle = 4673;

    @StyleableRes
    public static final int MaterialComponentsTheme_materialCardViewStyle = 4674;

    @StyleableRes
    public static final int MaterialComponentsTheme_navigationViewStyle = 4675;

    @StyleableRes
    public static final int MaterialComponentsTheme_scrimBackground = 4676;

    @StyleableRes
    public static final int MaterialComponentsTheme_snackbarButtonStyle = 4677;

    @StyleableRes
    public static final int MaterialComponentsTheme_tabStyle = 4678;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceBody1 = 4679;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceBody2 = 4680;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceButton = 4681;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceCaption = 4682;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceHeadline1 = 4683;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceHeadline2 = 4684;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceHeadline3 = 4685;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceHeadline4 = 4686;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceHeadline5 = 4687;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceHeadline6 = 4688;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceOverline = 4689;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceSubtitle1 = 4690;

    @StyleableRes
    public static final int MaterialComponentsTheme_textAppearanceSubtitle2 = 4691;

    @StyleableRes
    public static final int MaterialComponentsTheme_textInputStyle = 4692;

    @StyleableRes
    public static final int MenuGroup_android_checkableBehavior = 4693;

    @StyleableRes
    public static final int MenuGroup_android_enabled = 4694;

    @StyleableRes
    public static final int MenuGroup_android_id = 4695;

    @StyleableRes
    public static final int MenuGroup_android_menuCategory = 4696;

    @StyleableRes
    public static final int MenuGroup_android_orderInCategory = 4697;

    @StyleableRes
    public static final int MenuGroup_android_visible = 4698;

    @StyleableRes
    public static final int MenuItem_actionLayout = 4699;

    @StyleableRes
    public static final int MenuItem_actionProviderClass = 4700;

    @StyleableRes
    public static final int MenuItem_actionViewClass = 4701;

    @StyleableRes
    public static final int MenuItem_alphabeticModifiers = 4702;

    @StyleableRes
    public static final int MenuItem_android_alphabeticShortcut = 4703;

    @StyleableRes
    public static final int MenuItem_android_checkable = 4704;

    @StyleableRes
    public static final int MenuItem_android_checked = 4705;

    @StyleableRes
    public static final int MenuItem_android_enabled = 4706;

    @StyleableRes
    public static final int MenuItem_android_icon = 4707;

    @StyleableRes
    public static final int MenuItem_android_id = 4708;

    @StyleableRes
    public static final int MenuItem_android_menuCategory = 4709;

    @StyleableRes
    public static final int MenuItem_android_numericShortcut = 4710;

    @StyleableRes
    public static final int MenuItem_android_onClick = 4711;

    @StyleableRes
    public static final int MenuItem_android_orderInCategory = 4712;

    @StyleableRes
    public static final int MenuItem_android_title = 4713;

    @StyleableRes
    public static final int MenuItem_android_titleCondensed = 4714;

    @StyleableRes
    public static final int MenuItem_android_visible = 4715;

    @StyleableRes
    public static final int MenuItem_contentDescription = 4716;

    @StyleableRes
    public static final int MenuItem_iconTint = 4717;

    @StyleableRes
    public static final int MenuItem_iconTintMode = 4718;

    @StyleableRes
    public static final int MenuItem_numericModifiers = 4719;

    @StyleableRes
    public static final int MenuItem_showAsAction = 4720;

    @StyleableRes
    public static final int MenuItem_tooltipText = 4721;

    @StyleableRes
    public static final int MenuView_android_headerBackground = 4722;

    @StyleableRes
    public static final int MenuView_android_horizontalDivider = 4723;

    @StyleableRes
    public static final int MenuView_android_itemBackground = 4724;

    @StyleableRes
    public static final int MenuView_android_itemIconDisabledAlpha = 4725;

    @StyleableRes
    public static final int MenuView_android_itemTextAppearance = 4726;

    @StyleableRes
    public static final int MenuView_android_verticalDivider = 4727;

    @StyleableRes
    public static final int MenuView_android_windowAnimationStyle = 4728;

    @StyleableRes
    public static final int MenuView_preserveIconSpacing = 4729;

    @StyleableRes
    public static final int MenuView_subMenuArrow = 4730;

    @StyleableRes
    public static final int MsgView_mv_backgroundColor = 4731;

    @StyleableRes
    public static final int MsgView_mv_cornerRadius = 4732;

    @StyleableRes
    public static final int MsgView_mv_isRadiusHalfHeight = 4733;

    @StyleableRes
    public static final int MsgView_mv_isWidthHeightEqual = 4734;

    @StyleableRes
    public static final int MsgView_mv_strokeColor = 4735;

    @StyleableRes
    public static final int MsgView_mv_strokeWidth = 4736;

    @StyleableRes
    public static final int NavigationView_android_background = 4737;

    @StyleableRes
    public static final int NavigationView_android_fitsSystemWindows = 4738;

    @StyleableRes
    public static final int NavigationView_android_maxWidth = 4739;

    @StyleableRes
    public static final int NavigationView_elevation = 4740;

    @StyleableRes
    public static final int NavigationView_headerLayout = 4741;

    @StyleableRes
    public static final int NavigationView_itemBackground = 4742;

    @StyleableRes
    public static final int NavigationView_itemHorizontalPadding = 4743;

    @StyleableRes
    public static final int NavigationView_itemIconPadding = 4744;

    @StyleableRes
    public static final int NavigationView_itemIconTint = 4745;

    @StyleableRes
    public static final int NavigationView_itemTextAppearance = 4746;

    @StyleableRes
    public static final int NavigationView_itemTextColor = 4747;

    @StyleableRes
    public static final int NavigationView_menu = 4748;

    @StyleableRes
    public static final int OtpView_android_cursorVisible = 4749;

    @StyleableRes
    public static final int OtpView_android_itemBackground = 4750;

    @StyleableRes
    public static final int OtpView_cursorColor = 4751;

    @StyleableRes
    public static final int OtpView_cursorWidth = 4752;

    @StyleableRes
    public static final int OtpView_hideLineWhenFilled = 4753;

    @StyleableRes
    public static final int OtpView_itemCount = 4754;

    @StyleableRes
    public static final int OtpView_itemHeight = 4755;

    @StyleableRes
    public static final int OtpView_itemRadius = 4756;

    @StyleableRes
    public static final int OtpView_itemSpacing = 4757;

    @StyleableRes
    public static final int OtpView_itemWidth = 4758;

    @StyleableRes
    public static final int OtpView_lineColor = 4759;

    @StyleableRes
    public static final int OtpView_lineWidth = 4760;

    @StyleableRes
    public static final int OtpView_viewType = 4761;

    @StyleableRes
    public static final int OtpViewTheme_otpViewStyle = 4762;

    @StyleableRes
    public static final int PopupWindow_android_popupAnimationStyle = 4763;

    @StyleableRes
    public static final int PopupWindow_android_popupBackground = 4764;

    @StyleableRes
    public static final int PopupWindow_overlapAnchor = 4765;

    @StyleableRes
    public static final int PopupWindowBackgroundState_state_above_anchor = 4766;

    @StyleableRes
    public static final int RecycleListView_paddingBottomNoButtons = 4767;

    @StyleableRes
    public static final int RecycleListView_paddingTopNoTitle = 4768;

    @StyleableRes
    public static final int RecyclerView_android_descendantFocusability = 4769;

    @StyleableRes
    public static final int RecyclerView_android_orientation = 4770;

    @StyleableRes
    public static final int RecyclerView_fastScrollEnabled = 4771;

    @StyleableRes
    public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 4772;

    @StyleableRes
    public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 4773;

    @StyleableRes
    public static final int RecyclerView_fastScrollVerticalThumbDrawable = 4774;

    @StyleableRes
    public static final int RecyclerView_fastScrollVerticalTrackDrawable = 4775;

    @StyleableRes
    public static final int RecyclerView_layoutManager = 4776;

    @StyleableRes
    public static final int RecyclerView_reverseLayout = 4777;

    @StyleableRes
    public static final int RecyclerView_spanCount = 4778;

    @StyleableRes
    public static final int RecyclerView_stackFromEnd = 4779;

    @StyleableRes
    public static final int RecyclerViewPager_rvp_flingFactor = 4780;

    @StyleableRes
    public static final int RecyclerViewPager_rvp_inertia = 4781;

    @StyleableRes
    public static final int RecyclerViewPager_rvp_millisecondsPerInch = 4782;

    @StyleableRes
    public static final int RecyclerViewPager_rvp_singlePageFling = 4783;

    @StyleableRes
    public static final int RecyclerViewPager_rvp_triggerOffset = 4784;

    @StyleableRes
    public static final int RoundedImageView_android_scaleType = 4785;

    @StyleableRes
    public static final int RoundedImageView_riv_border_color = 4786;

    @StyleableRes
    public static final int RoundedImageView_riv_border_width = 4787;

    @StyleableRes
    public static final int RoundedImageView_riv_corner_radius = 4788;

    @StyleableRes
    public static final int RoundedImageView_riv_corner_radius_bottom_left = 4789;

    @StyleableRes
    public static final int RoundedImageView_riv_corner_radius_bottom_right = 4790;

    @StyleableRes
    public static final int RoundedImageView_riv_corner_radius_top_left = 4791;

    @StyleableRes
    public static final int RoundedImageView_riv_corner_radius_top_right = 4792;

    @StyleableRes
    public static final int RoundedImageView_riv_mutate_background = 4793;

    @StyleableRes
    public static final int RoundedImageView_riv_oval = 4794;

    @StyleableRes
    public static final int RoundedImageView_riv_tile_mode = 4795;

    @StyleableRes
    public static final int RoundedImageView_riv_tile_mode_x = 4796;

    @StyleableRes
    public static final int RoundedImageView_riv_tile_mode_y = 4797;

    @StyleableRes
    public static final int ScrimInsetsFrameLayout_insetForeground = 4798;

    @StyleableRes
    public static final int ScrollingViewBehavior_Layout_behavior_overlapTop = 4799;

    @StyleableRes
    public static final int SearchView_android_focusable = 4800;

    @StyleableRes
    public static final int SearchView_android_imeOptions = 4801;

    @StyleableRes
    public static final int SearchView_android_inputType = 4802;

    @StyleableRes
    public static final int SearchView_android_maxWidth = 4803;

    @StyleableRes
    public static final int SearchView_closeIcon = 4804;

    @StyleableRes
    public static final int SearchView_commitIcon = 4805;

    @StyleableRes
    public static final int SearchView_defaultQueryHint = 4806;

    @StyleableRes
    public static final int SearchView_goIcon = 4807;

    @StyleableRes
    public static final int SearchView_iconifiedByDefault = 4808;

    @StyleableRes
    public static final int SearchView_layout = 4809;

    @StyleableRes
    public static final int SearchView_queryBackground = 4810;

    @StyleableRes
    public static final int SearchView_queryHint = 4811;

    @StyleableRes
    public static final int SearchView_searchHintIcon = 4812;

    @StyleableRes
    public static final int SearchView_searchIcon = 4813;

    @StyleableRes
    public static final int SearchView_submitBackground = 4814;

    @StyleableRes
    public static final int SearchView_suggestionRowLayout = 4815;

    @StyleableRes
    public static final int SearchView_voiceIcon = 4816;

    @StyleableRes
    public static final int SegmentTabLayout_tl_bar_color = 4817;

    @StyleableRes
    public static final int SegmentTabLayout_tl_bar_stroke_color = 4818;

    @StyleableRes
    public static final int SegmentTabLayout_tl_bar_stroke_width = 4819;

    @StyleableRes
    public static final int SegmentTabLayout_tl_divider_color = 4820;

    @StyleableRes
    public static final int SegmentTabLayout_tl_divider_padding = 4821;

    @StyleableRes
    public static final int SegmentTabLayout_tl_divider_width = 4822;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_anim_duration = 4823;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_anim_enable = 4824;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_bounce_enable = 4825;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_color = 4826;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_corner_radius = 4827;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_height = 4828;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_margin_bottom = 4829;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_margin_left = 4830;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_margin_right = 4831;

    @StyleableRes
    public static final int SegmentTabLayout_tl_indicator_margin_top = 4832;

    @StyleableRes
    public static final int SegmentTabLayout_tl_tab_padding = 4833;

    @StyleableRes
    public static final int SegmentTabLayout_tl_tab_space_equal = 4834;

    @StyleableRes
    public static final int SegmentTabLayout_tl_tab_width = 4835;

    @StyleableRes
    public static final int SegmentTabLayout_tl_textAllCaps = 4836;

    @StyleableRes
    public static final int SegmentTabLayout_tl_textBold = 4837;

    @StyleableRes
    public static final int SegmentTabLayout_tl_textSelectColor = 4838;

    @StyleableRes
    public static final int SegmentTabLayout_tl_textUnselectColor = 4839;

    @StyleableRes
    public static final int SegmentTabLayout_tl_textsize = 4840;

    @StyleableRes
    public static final int ShadowView_android_foreground = 4841;

    @StyleableRes
    public static final int ShadowView_backgroundColor = 4842;

    @StyleableRes
    public static final int ShadowView_cornerRadius = 4843;

    @StyleableRes
    public static final int ShadowView_cornerRadiusBL = 4844;

    @StyleableRes
    public static final int ShadowView_cornerRadiusBR = 4845;

    @StyleableRes
    public static final int ShadowView_cornerRadiusTL = 4846;

    @StyleableRes
    public static final int ShadowView_cornerRadiusTR = 4847;

    @StyleableRes
    public static final int ShadowView_foregroundColor = 4848;

    @StyleableRes
    public static final int ShadowView_shadowColor = 4849;

    @StyleableRes
    public static final int ShadowView_shadowDx = 4850;

    @StyleableRes
    public static final int ShadowView_shadowDy = 4851;

    @StyleableRes
    public static final int ShadowView_shadowMargin = 4852;

    @StyleableRes
    public static final int ShadowView_shadowMarginBottom = 4853;

    @StyleableRes
    public static final int ShadowView_shadowMarginLeft = 4854;

    @StyleableRes
    public static final int ShadowView_shadowMarginRight = 4855;

    @StyleableRes
    public static final int ShadowView_shadowMarginTop = 4856;

    @StyleableRes
    public static final int ShadowView_shadowRadius = 4857;

    @StyleableRes
    public static final int ShadowView_Layout_layout_gravity = 4858;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_angle = 4859;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_auto_start = 4860;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_base_alpha = 4861;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_base_color = 4862;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_clip_to_children = 4863;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_colored = 4864;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_direction = 4865;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_dropoff = 4866;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_duration = 4867;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_fixed_height = 4868;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_fixed_width = 4869;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_group = 4870;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_height_ratio = 4871;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_highlight_alpha = 4872;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_highlight_color = 4873;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_intensity = 4874;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_relative_height = 4875;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_relative_width = 4876;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_repeat_count = 4877;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_repeat_delay = 4878;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_repeat_mode = 4879;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_shape = 4880;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_tilt = 4881;

    @StyleableRes
    public static final int ShimmerFrameLayout_shimmer_width_ratio = 4882;

    @StyleableRes
    public static final int ShimmerRecyclerView_shimmer_demo_child_count = 4883;

    @StyleableRes
    public static final int ShimmerRecyclerView_shimmer_demo_grid_child_count = 4884;

    @StyleableRes
    public static final int ShimmerRecyclerView_shimmer_demo_layout = 4885;

    @StyleableRes
    public static final int ShimmerRecyclerView_shimmer_demo_layout_manager_type = 4886;

    @StyleableRes
    public static final int SignInButton_buttonSize = 4887;

    @StyleableRes
    public static final int SignInButton_colorScheme = 4888;

    @StyleableRes
    public static final int SignInButton_scopeUris = 4889;

    @StyleableRes
    public static final int SlidingTabLayout_tl_divider_color = 4890;

    @StyleableRes
    public static final int SlidingTabLayout_tl_divider_padding = 4891;

    @StyleableRes
    public static final int SlidingTabLayout_tl_divider_width = 4892;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_color = 4893;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_corner_radius = 4894;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_gravity = 4895;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_height = 4896;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_margin_bottom = 4897;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_margin_left = 4898;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_margin_right = 4899;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_margin_top = 4900;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_style = 4901;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_width = 4902;

    @StyleableRes
    public static final int SlidingTabLayout_tl_indicator_width_equal_title = 4903;

    @StyleableRes
    public static final int SlidingTabLayout_tl_tab_padding = 4904;

    @StyleableRes
    public static final int SlidingTabLayout_tl_tab_space_equal = 4905;

    @StyleableRes
    public static final int SlidingTabLayout_tl_tab_width = 4906;

    @StyleableRes
    public static final int SlidingTabLayout_tl_textAllCaps = 4907;

    @StyleableRes
    public static final int SlidingTabLayout_tl_textBold = 4908;

    @StyleableRes
    public static final int SlidingTabLayout_tl_textSelectColor = 4909;

    @StyleableRes
    public static final int SlidingTabLayout_tl_textUnselectColor = 4910;

    @StyleableRes
    public static final int SlidingTabLayout_tl_textsize = 4911;

    @StyleableRes
    public static final int SlidingTabLayout_tl_underline_color = 4912;

    @StyleableRes
    public static final int SlidingTabLayout_tl_underline_gravity = 4913;

    @StyleableRes
    public static final int SlidingTabLayout_tl_underline_height = 4914;

    @StyleableRes
    public static final int Snackbar_snackbarButtonStyle = 4915;

    @StyleableRes
    public static final int Snackbar_snackbarStyle = 4916;

    @StyleableRes
    public static final int SnackbarLayout_android_maxWidth = 4917;

    @StyleableRes
    public static final int SnackbarLayout_elevation = 4918;

    @StyleableRes
    public static final int SnackbarLayout_maxActionInlineWidth = 4919;

    @StyleableRes
    public static final int Spinner_android_dropDownWidth = 4920;

    @StyleableRes
    public static final int Spinner_android_entries = 4921;

    @StyleableRes
    public static final int Spinner_android_popupBackground = 4922;

    @StyleableRes
    public static final int Spinner_android_prompt = 4923;

    @StyleableRes
    public static final int Spinner_popupTheme = 4924;

    @StyleableRes
    public static final int StateListDrawable_android_constantSize = 4925;

    @StyleableRes
    public static final int StateListDrawable_android_dither = 4926;

    @StyleableRes
    public static final int StateListDrawable_android_enterFadeDuration = 4927;

    @StyleableRes
    public static final int StateListDrawable_android_exitFadeDuration = 4928;

    @StyleableRes
    public static final int StateListDrawable_android_variablePadding = 4929;

    @StyleableRes
    public static final int StateListDrawable_android_visible = 4930;

    @StyleableRes
    public static final int StateListDrawableItem_android_drawable = 4931;

    @StyleableRes
    public static final int SwitchButton_sb_background = 4932;

    @StyleableRes
    public static final int SwitchButton_sb_border_width = 4933;

    @StyleableRes
    public static final int SwitchButton_sb_button_color = 4934;

    @StyleableRes
    public static final int SwitchButton_sb_checked = 4935;

    @StyleableRes
    public static final int SwitchButton_sb_checked_color = 4936;

    @StyleableRes
    public static final int SwitchButton_sb_checkline_color = 4937;

    @StyleableRes
    public static final int SwitchButton_sb_checkline_width = 4938;

    @StyleableRes
    public static final int SwitchButton_sb_effect_duration = 4939;

    @StyleableRes
    public static final int SwitchButton_sb_enable_effect = 4940;

    @StyleableRes
    public static final int SwitchButton_sb_shadow_color = 4941;

    @StyleableRes
    public static final int SwitchButton_sb_shadow_effect = 4942;

    @StyleableRes
    public static final int SwitchButton_sb_shadow_offset = 4943;

    @StyleableRes
    public static final int SwitchButton_sb_shadow_radius = 4944;

    @StyleableRes
    public static final int SwitchButton_sb_show_indicator = 4945;

    @StyleableRes
    public static final int SwitchButton_sb_uncheck_color = 4946;

    @StyleableRes
    public static final int SwitchButton_sb_uncheckcircle_color = 4947;

    @StyleableRes
    public static final int SwitchButton_sb_uncheckcircle_radius = 4948;

    @StyleableRes
    public static final int SwitchButton_sb_uncheckcircle_width = 4949;

    @StyleableRes
    public static final int SwitchCompat_android_textOff = 4950;

    @StyleableRes
    public static final int SwitchCompat_android_textOn = 4951;

    @StyleableRes
    public static final int SwitchCompat_android_thumb = 4952;

    @StyleableRes
    public static final int SwitchCompat_showText = 4953;

    @StyleableRes
    public static final int SwitchCompat_splitTrack = 4954;

    @StyleableRes
    public static final int SwitchCompat_switchMinWidth = 4955;

    @StyleableRes
    public static final int SwitchCompat_switchPadding = 4956;

    @StyleableRes
    public static final int SwitchCompat_switchTextAppearance = 4957;

    @StyleableRes
    public static final int SwitchCompat_thumbTextPadding = 4958;

    @StyleableRes
    public static final int SwitchCompat_thumbTint = 4959;

    @StyleableRes
    public static final int SwitchCompat_thumbTintMode = 4960;

    @StyleableRes
    public static final int SwitchCompat_track = 4961;

    @StyleableRes
    public static final int SwitchCompat_trackTint = 4962;

    @StyleableRes
    public static final int SwitchCompat_trackTintMode = 4963;

    @StyleableRes
    public static final int TabItem_android_icon = 4964;

    @StyleableRes
    public static final int TabItem_android_layout = 4965;

    @StyleableRes
    public static final int TabItem_android_text = 4966;

    @StyleableRes
    public static final int TabLayout_tabBackground = 4967;

    @StyleableRes
    public static final int TabLayout_tabContentStart = 4968;

    @StyleableRes
    public static final int TabLayout_tabGravity = 4969;

    @StyleableRes
    public static final int TabLayout_tabIconTint = 4970;

    @StyleableRes
    public static final int TabLayout_tabIconTintMode = 4971;

    @StyleableRes
    public static final int TabLayout_tabIndicator = 4972;

    @StyleableRes
    public static final int TabLayout_tabIndicatorAnimationDuration = 4973;

    @StyleableRes
    public static final int TabLayout_tabIndicatorColor = 4974;

    @StyleableRes
    public static final int TabLayout_tabIndicatorFullWidth = 4975;

    @StyleableRes
    public static final int TabLayout_tabIndicatorGravity = 4976;

    @StyleableRes
    public static final int TabLayout_tabIndicatorHeight = 4977;

    @StyleableRes
    public static final int TabLayout_tabInlineLabel = 4978;

    @StyleableRes
    public static final int TabLayout_tabMaxWidth = 4979;

    @StyleableRes
    public static final int TabLayout_tabMinWidth = 4980;

    @StyleableRes
    public static final int TabLayout_tabMode = 4981;

    @StyleableRes
    public static final int TabLayout_tabPadding = 4982;

    @StyleableRes
    public static final int TabLayout_tabPaddingBottom = 4983;

    @StyleableRes
    public static final int TabLayout_tabPaddingEnd = 4984;

    @StyleableRes
    public static final int TabLayout_tabPaddingStart = 4985;

    @StyleableRes
    public static final int TabLayout_tabPaddingTop = 4986;

    @StyleableRes
    public static final int TabLayout_tabRippleColor = 4987;

    @StyleableRes
    public static final int TabLayout_tabSelectedTextColor = 4988;

    @StyleableRes
    public static final int TabLayout_tabTextAppearance = 4989;

    @StyleableRes
    public static final int TabLayout_tabTextColor = 4990;

    @StyleableRes
    public static final int TabLayout_tabUnboundedRipple = 4991;

    @StyleableRes
    public static final int TextAppearance_android_fontFamily = 4992;

    @StyleableRes
    public static final int TextAppearance_android_shadowColor = 4993;

    @StyleableRes
    public static final int TextAppearance_android_shadowDx = 4994;

    @StyleableRes
    public static final int TextAppearance_android_shadowDy = 4995;

    @StyleableRes
    public static final int TextAppearance_android_shadowRadius = 4996;

    @StyleableRes
    public static final int TextAppearance_android_textColor = 4997;

    @StyleableRes
    public static final int TextAppearance_android_textColorHint = 4998;

    @StyleableRes
    public static final int TextAppearance_android_textColorLink = 4999;

    @StyleableRes
    public static final int TextAppearance_android_textFontWeight = 5000;

    @StyleableRes
    public static final int TextAppearance_android_textSize = 5001;

    @StyleableRes
    public static final int TextAppearance_android_textStyle = 5002;

    @StyleableRes
    public static final int TextAppearance_android_typeface = 5003;

    @StyleableRes
    public static final int TextAppearance_fontFamily = 5004;

    @StyleableRes
    public static final int TextAppearance_fontVariationSettings = 5005;

    @StyleableRes
    public static final int TextAppearance_textAllCaps = 5006;

    @StyleableRes
    public static final int TextAppearance_textLocale = 5007;

    @StyleableRes
    public static final int TextInputLayout_android_hint = 5008;

    @StyleableRes
    public static final int TextInputLayout_android_textColorHint = 5009;

    @StyleableRes
    public static final int TextInputLayout_boxBackgroundColor = 5010;

    @StyleableRes
    public static final int TextInputLayout_boxBackgroundMode = 5011;

    @StyleableRes
    public static final int TextInputLayout_boxCollapsedPaddingTop = 5012;

    @StyleableRes
    public static final int TextInputLayout_boxCornerRadiusBottomEnd = 5013;

    @StyleableRes
    public static final int TextInputLayout_boxCornerRadiusBottomStart = 5014;

    @StyleableRes
    public static final int TextInputLayout_boxCornerRadiusTopEnd = 5015;

    @StyleableRes
    public static final int TextInputLayout_boxCornerRadiusTopStart = 5016;

    @StyleableRes
    public static final int TextInputLayout_boxStrokeColor = 5017;

    @StyleableRes
    public static final int TextInputLayout_boxStrokeWidth = 5018;

    @StyleableRes
    public static final int TextInputLayout_counterEnabled = 5019;

    @StyleableRes
    public static final int TextInputLayout_counterMaxLength = 5020;

    @StyleableRes
    public static final int TextInputLayout_counterOverflowTextAppearance = 5021;

    @StyleableRes
    public static final int TextInputLayout_counterTextAppearance = 5022;

    @StyleableRes
    public static final int TextInputLayout_errorEnabled = 5023;

    @StyleableRes
    public static final int TextInputLayout_errorTextAppearance = 5024;

    @StyleableRes
    public static final int TextInputLayout_helperText = 5025;

    @StyleableRes
    public static final int TextInputLayout_helperTextEnabled = 5026;

    @StyleableRes
    public static final int TextInputLayout_helperTextTextAppearance = 5027;

    @StyleableRes
    public static final int TextInputLayout_hintAnimationEnabled = 5028;

    @StyleableRes
    public static final int TextInputLayout_hintEnabled = 5029;

    @StyleableRes
    public static final int TextInputLayout_hintTextAppearance = 5030;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleContentDescription = 5031;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleDrawable = 5032;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleEnabled = 5033;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleTint = 5034;

    @StyleableRes
    public static final int TextInputLayout_passwordToggleTintMode = 5035;

    @StyleableRes
    public static final int ThemeEnforcement_android_textAppearance = 5036;

    @StyleableRes
    public static final int ThemeEnforcement_enforceMaterialTheme = 5037;

    @StyleableRes
    public static final int ThemeEnforcement_enforceTextAppearance = 5038;

    @StyleableRes
    public static final int TickerView_android_gravity = 5039;

    @StyleableRes
    public static final int TickerView_android_shadowColor = 5040;

    @StyleableRes
    public static final int TickerView_android_shadowDx = 5041;

    @StyleableRes
    public static final int TickerView_android_shadowDy = 5042;

    @StyleableRes
    public static final int TickerView_android_shadowRadius = 5043;

    @StyleableRes
    public static final int TickerView_android_text = 5044;

    @StyleableRes
    public static final int TickerView_android_textAppearance = 5045;

    @StyleableRes
    public static final int TickerView_android_textColor = 5046;

    @StyleableRes
    public static final int TickerView_android_textSize = 5047;

    @StyleableRes
    public static final int TickerView_android_textStyle = 5048;

    @StyleableRes
    public static final int TickerView_ticker_animateMeasurementChange = 5049;

    @StyleableRes
    public static final int TickerView_ticker_animationDuration = 5050;

    @StyleableRes
    public static final int TickerView_ticker_defaultCharacterList = 5051;

    @StyleableRes
    public static final int Toolbar_android_gravity = 5052;

    @StyleableRes
    public static final int Toolbar_android_minHeight = 5053;

    @StyleableRes
    public static final int Toolbar_buttonGravity = 5054;

    @StyleableRes
    public static final int Toolbar_collapseContentDescription = 5055;

    @StyleableRes
    public static final int Toolbar_collapseIcon = 5056;

    @StyleableRes
    public static final int Toolbar_contentInsetEnd = 5057;

    @StyleableRes
    public static final int Toolbar_contentInsetEndWithActions = 5058;

    @StyleableRes
    public static final int Toolbar_contentInsetLeft = 5059;

    @StyleableRes
    public static final int Toolbar_contentInsetRight = 5060;

    @StyleableRes
    public static final int Toolbar_contentInsetStart = 5061;

    @StyleableRes
    public static final int Toolbar_contentInsetStartWithNavigation = 5062;

    @StyleableRes
    public static final int Toolbar_logo = 5063;

    @StyleableRes
    public static final int Toolbar_logoDescription = 5064;

    @StyleableRes
    public static final int Toolbar_maxButtonHeight = 5065;

    @StyleableRes
    public static final int Toolbar_menu = 5066;

    @StyleableRes
    public static final int Toolbar_navigationContentDescription = 5067;

    @StyleableRes
    public static final int Toolbar_navigationIcon = 5068;

    @StyleableRes
    public static final int Toolbar_popupTheme = 5069;

    @StyleableRes
    public static final int Toolbar_subtitle = 5070;

    @StyleableRes
    public static final int Toolbar_subtitleTextAppearance = 5071;

    @StyleableRes
    public static final int Toolbar_subtitleTextColor = 5072;

    @StyleableRes
    public static final int Toolbar_title = 5073;

    @StyleableRes
    public static final int Toolbar_titleMargin = 5074;

    @StyleableRes
    public static final int Toolbar_titleMarginBottom = 5075;

    @StyleableRes
    public static final int Toolbar_titleMarginEnd = 5076;

    @StyleableRes
    public static final int Toolbar_titleMarginStart = 5077;

    @StyleableRes
    public static final int Toolbar_titleMarginTop = 5078;

    @StyleableRes
    public static final int Toolbar_titleMargins = 5079;

    @StyleableRes
    public static final int Toolbar_titleTextAppearance = 5080;

    @StyleableRes
    public static final int Toolbar_titleTextColor = 5081;

    @StyleableRes
    public static final int View_android_focusable = 5082;

    @StyleableRes
    public static final int View_android_theme = 5083;

    @StyleableRes
    public static final int View_paddingEnd = 5084;

    @StyleableRes
    public static final int View_paddingStart = 5085;

    @StyleableRes
    public static final int View_theme = 5086;

    @StyleableRes
    public static final int ViewBackgroundHelper_android_background = 5087;

    @StyleableRes
    public static final int ViewBackgroundHelper_backgroundTint = 5088;

    @StyleableRes
    public static final int ViewBackgroundHelper_backgroundTintMode = 5089;

    @StyleableRes
    public static final int ViewStubCompat_android_id = 5090;

    @StyleableRes
    public static final int ViewStubCompat_android_inflatedId = 5091;

    @StyleableRes
    public static final int ViewStubCompat_android_layout = 5092;

    @StyleableRes
    public static final int com_facebook_like_view_com_facebook_auxiliary_view_position = 5093;

    @StyleableRes
    public static final int com_facebook_like_view_com_facebook_foreground_color = 5094;

    @StyleableRes
    public static final int com_facebook_like_view_com_facebook_horizontal_alignment = 5095;

    @StyleableRes
    public static final int com_facebook_like_view_com_facebook_object_id = 5096;

    @StyleableRes
    public static final int com_facebook_like_view_com_facebook_object_type = 5097;

    @StyleableRes
    public static final int com_facebook_like_view_com_facebook_style = 5098;

    @StyleableRes
    public static final int com_facebook_login_view_com_facebook_confirm_logout = 5099;

    @StyleableRes
    public static final int com_facebook_login_view_com_facebook_login_text = 5100;

    @StyleableRes
    public static final int com_facebook_login_view_com_facebook_logout_text = 5101;

    @StyleableRes
    public static final int com_facebook_login_view_com_facebook_tooltip_mode = 5102;

    @StyleableRes
    public static final int com_facebook_profile_picture_view_com_facebook_is_cropped = 5103;

    @StyleableRes
    public static final int com_facebook_profile_picture_view_com_facebook_preset_size = 5104;

    @StyleableRes
    public static final int fastscroll__fastScroller_fastscroll__bubbleColor = 5105;

    @StyleableRes
    public static final int fastscroll__fastScroller_fastscroll__bubbleTextAppearance = 5106;

    @StyleableRes
    public static final int fastscroll__fastScroller_fastscroll__handleColor = 5107;

    @StyleableRes
    public static final int sparkbutton_sparkbutton_activeImage = 5108;

    @StyleableRes
    public static final int sparkbutton_sparkbutton_activeImageTint = 5109;

    @StyleableRes
    public static final int sparkbutton_sparkbutton_animationSpeed = 5110;

    @StyleableRes
    public static final int sparkbutton_sparkbutton_iconSize = 5111;

    @StyleableRes
    public static final int sparkbutton_sparkbutton_inActiveImage = 5112;

    @StyleableRes
    public static final int sparkbutton_sparkbutton_inActiveImageTint = 5113;

    @StyleableRes
    public static final int sparkbutton_sparkbutton_pressOnTouch = 5114;

    @StyleableRes
    public static final int sparkbutton_sparkbutton_primaryColor = 5115;

    @StyleableRes
    public static final int sparkbutton_sparkbutton_secondaryColor = 5116;
  }
}
