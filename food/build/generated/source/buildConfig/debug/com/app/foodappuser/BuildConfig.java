/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.app.foodappuser;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.app.foodappuser";
  /**
   * @deprecated APPLICATION_ID is misleading in libraries. For the library package name use LIBRARY_PACKAGE_NAME
   */
  @Deprecated
  public static final String APPLICATION_ID = "com.app.foodappuser";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from default config.
  public static final String BASE_URL = "http://134.209.147.250:3000";
  public static final String TERMS_URL = "http://139.59.55.166:3000";
}
