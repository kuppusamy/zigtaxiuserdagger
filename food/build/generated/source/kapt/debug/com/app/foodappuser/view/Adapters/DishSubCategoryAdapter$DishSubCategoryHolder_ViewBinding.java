// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.UiUtils.ExpandableLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DishSubCategoryAdapter$DishSubCategoryHolder_ViewBinding implements Unbinder {
  private DishSubCategoryAdapter.DishSubCategoryHolder target;

  @UiThread
  public DishSubCategoryAdapter$DishSubCategoryHolder_ViewBinding(
      DishSubCategoryAdapter.DishSubCategoryHolder target, View source) {
    this.target = target;

    target.subCategoryName = Utils.findRequiredViewAsType(source, R.id.subCategoryName, "field 'subCategoryName'", TextView.class);
    target.subCategroyNameItems = Utils.findRequiredViewAsType(source, R.id.subCategroyNameItems, "field 'subCategroyNameItems'", TextView.class);
    target.topLayout = Utils.findRequiredViewAsType(source, R.id.topLayout, "field 'topLayout'", LinearLayout.class);
    target.subCategoryRecyclerView = Utils.findRequiredViewAsType(source, R.id.subCategoryRecyclerView, "field 'subCategoryRecyclerView'", RecyclerView.class);
    target.expandableLayout = Utils.findRequiredViewAsType(source, R.id.expandableLayout, "field 'expandableLayout'", ExpandableLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DishSubCategoryAdapter.DishSubCategoryHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.subCategoryName = null;
    target.subCategroyNameItems = null;
    target.topLayout = null;
    target.subCategoryRecyclerView = null;
    target.expandableLayout = null;
  }
}
