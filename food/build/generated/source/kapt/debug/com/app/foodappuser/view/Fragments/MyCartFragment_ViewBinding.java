// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Fragments;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MyCartFragment_ViewBinding implements Unbinder {
  private MyCartFragment target;

  @UiThread
  public MyCartFragment_ViewBinding(MyCartFragment target, View source) {
    this.target = target;

    target.restaurantName = Utils.findRequiredViewAsType(source, R.id.restaurantName, "field 'restaurantName'", TextView.class);
    target.restaurantImage = Utils.findRequiredViewAsType(source, R.id.restaurantImage, "field 'restaurantImage'", ImageView.class);
    target.restaurantLocation = Utils.findRequiredViewAsType(source, R.id.restaurantLocation, "field 'restaurantLocation'", TextView.class);
    target.dishesRecycler = Utils.findRequiredViewAsType(source, R.id.dishesRecycler, "field 'dishesRecycler'", RecyclerView.class);
    target.chargesRecycler = Utils.findRequiredViewAsType(source, R.id.chargesRecycler, "field 'chargesRecycler'", RecyclerView.class);
    target.tooManyItemsLayout = Utils.findRequiredView(source, R.id.tooManyItemsLayout, "field 'tooManyItemsLayout'");
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
    target.contentLayout = Utils.findRequiredView(source, R.id.contentLayout, "field 'contentLayout'");
    target.deliverAddressLayout = Utils.findRequiredView(source, R.id.deliverAddressLayout, "field 'deliverAddressLayout'");
    target.deliverImage = Utils.findRequiredViewAsType(source, R.id.deliverImage, "field 'deliverImage'", ImageView.class);
    target.deliverToText = Utils.findRequiredViewAsType(source, R.id.deliverToText, "field 'deliverToText'", TextView.class);
    target.deliverAddressText = Utils.findRequiredViewAsType(source, R.id.deliverAddressText, "field 'deliverAddressText'", TextView.class);
    target.deliverMinsText = Utils.findRequiredViewAsType(source, R.id.deliverMinsText, "field 'deliverMinsText'", TextView.class);
    target.changeAddressButton = Utils.findRequiredViewAsType(source, R.id.changeAddressButton, "field 'changeAddressButton'", RelativeLayout.class);
    target.proceedPayButton = Utils.findRequiredViewAsType(source, R.id.proceedPayButton, "field 'proceedPayButton'", RelativeLayout.class);
    target.noCartItems = Utils.findRequiredView(source, R.id.noCartItems, "field 'noCartItems'");
    target.noAddressLayout = Utils.findRequiredView(source, R.id.noAddressLayout, "field 'noAddressLayout'");
    target.connectionLostLayout = Utils.findRequiredView(source, R.id.connectionLostLayout, "field 'connectionLostLayout'");
    target.retryButton = Utils.findRequiredViewAsType(source, R.id.retryButton, "field 'retryButton'", RelativeLayout.class);
    target.applyCouponLayout = Utils.findRequiredViewAsType(source, R.id.applyCouponLayout, "field 'applyCouponLayout'", RelativeLayout.class);
    target.couponAppliedLayout = Utils.findRequiredViewAsType(source, R.id.couponAppliedLayout, "field 'couponAppliedLayout'", RelativeLayout.class);
    target.couponCode = Utils.findRequiredViewAsType(source, R.id.couponCode, "field 'couponCode'", TextView.class);
    target.suggestions = Utils.findRequiredViewAsType(source, R.id.suggestions, "field 'suggestions'", TextView.class);
    target.removeCoupon = Utils.findRequiredViewAsType(source, R.id.removeCoupon, "field 'removeCoupon'", ImageView.class);
    target.spacer = Utils.findRequiredViewAsType(source, R.id.spacer, "field 'spacer'", Space.class);
    target.addNewAddres = Utils.findRequiredViewAsType(source, R.id.addNewAddres, "field 'addNewAddres'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MyCartFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.restaurantName = null;
    target.restaurantImage = null;
    target.restaurantLocation = null;
    target.dishesRecycler = null;
    target.chargesRecycler = null;
    target.tooManyItemsLayout = null;
    target.progressBar = null;
    target.contentLayout = null;
    target.deliverAddressLayout = null;
    target.deliverImage = null;
    target.deliverToText = null;
    target.deliverAddressText = null;
    target.deliverMinsText = null;
    target.changeAddressButton = null;
    target.proceedPayButton = null;
    target.noCartItems = null;
    target.noAddressLayout = null;
    target.connectionLostLayout = null;
    target.retryButton = null;
    target.applyCouponLayout = null;
    target.couponAppliedLayout = null;
    target.couponCode = null;
    target.suggestions = null;
    target.removeCoupon = null;
    target.spacer = null;
    target.addNewAddres = null;
  }
}
