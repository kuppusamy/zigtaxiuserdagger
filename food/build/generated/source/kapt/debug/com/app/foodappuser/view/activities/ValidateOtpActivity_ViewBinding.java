// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.OTPListener.OtpView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ValidateOtpActivity_ViewBinding implements Unbinder {
  private ValidateOtpActivity target;

  @UiThread
  public ValidateOtpActivity_ViewBinding(ValidateOtpActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ValidateOtpActivity_ViewBinding(ValidateOtpActivity target, View source) {
    this.target = target;

    target.proceedButton = Utils.findRequiredViewAsType(source, R.id.proceedButton, "field 'proceedButton'", CardView.class);
    target.constraintLayout = Utils.findRequiredViewAsType(source, R.id.constraintLayout, "field 'constraintLayout'", ConstraintLayout.class);
    target.resendOtp = Utils.findRequiredViewAsType(source, R.id.resendOtp, "field 'resendOtp'", TextView.class);
    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", ImageView.class);
    target.otp_view = Utils.findRequiredViewAsType(source, R.id.otp_view, "field 'otp_view'", OtpView.class);
    target.progressBarOTP = Utils.findRequiredViewAsType(source, R.id.progressBarOTP, "field 'progressBarOTP'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ValidateOtpActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.proceedButton = null;
    target.constraintLayout = null;
    target.resendOtp = null;
    target.backButton = null;
    target.otp_view = null;
    target.progressBarOTP = null;
  }
}
