// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.OTPListener.OtpView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ForgotPasswordActivity_ViewBinding implements Unbinder {
  private ForgotPasswordActivity target;

  @UiThread
  public ForgotPasswordActivity_ViewBinding(ForgotPasswordActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ForgotPasswordActivity_ViewBinding(ForgotPasswordActivity target, View source) {
    this.target = target;

    target.otp_view = Utils.findRequiredViewAsType(source, R.id.otp_view, "field 'otp_view'", OtpView.class);
    target.proceedButton = Utils.findRequiredViewAsType(source, R.id.proceedButton, "field 'proceedButton'", CardView.class);
    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", ImageView.class);
    target.blockImage = Utils.findRequiredViewAsType(source, R.id.blockImage, "field 'blockImage'", ImageView.class);
    target.progressBarOTP = Utils.findRequiredViewAsType(source, R.id.progressBarOTP, "field 'progressBarOTP'", ProgressBar.class);
    target.changePassword = Utils.findRequiredViewAsType(source, R.id.changePassword, "field 'changePassword'", CardView.class);
    target.newPassword = Utils.findRequiredViewAsType(source, R.id.newPassword, "field 'newPassword'", EditText.class);
    target.confirmPassword = Utils.findRequiredViewAsType(source, R.id.confirmPassword, "field 'confirmPassword'", EditText.class);
    target.changePasswordLayout = Utils.findRequiredViewAsType(source, R.id.changePasswordLayout, "field 'changePasswordLayout'", RelativeLayout.class);
    target.slideDownLayout = Utils.findRequiredViewAsType(source, R.id.slideDownLayout, "field 'slideDownLayout'", RelativeLayout.class);
    target.parentLayout = Utils.findRequiredViewAsType(source, R.id.parentLayout, "field 'parentLayout'", ConstraintLayout.class);
    target.resendOtp = Utils.findRequiredViewAsType(source, R.id.resendOtp, "field 'resendOtp'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ForgotPasswordActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.otp_view = null;
    target.proceedButton = null;
    target.backButton = null;
    target.blockImage = null;
    target.progressBarOTP = null;
    target.changePassword = null;
    target.newPassword = null;
    target.confirmPassword = null;
    target.changePasswordLayout = null;
    target.slideDownLayout = null;
    target.parentLayout = null;
    target.resendOtp = null;
  }
}
