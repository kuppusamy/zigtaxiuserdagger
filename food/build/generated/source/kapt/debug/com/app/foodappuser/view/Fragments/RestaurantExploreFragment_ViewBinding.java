// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Fragments;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RestaurantExploreFragment_ViewBinding implements Unbinder {
  private RestaurantExploreFragment target;

  @UiThread
  public RestaurantExploreFragment_ViewBinding(RestaurantExploreFragment target, View source) {
    this.target = target;

    target.restaurantRecyclerView = Utils.findRequiredViewAsType(source, R.id.restaurantRecyclerView, "field 'restaurantRecyclerView'", RecyclerView.class);
    target.relatedTo = Utils.findRequiredViewAsType(source, R.id.relatedTo, "field 'relatedTo'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RestaurantExploreFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.restaurantRecyclerView = null;
    target.relatedTo = null;
  }
}
