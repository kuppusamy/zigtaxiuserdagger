// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PastOrdersAdapter$ViewHolderItem_ViewBinding implements Unbinder {
  private PastOrdersAdapter.ViewHolderItem target;

  @UiThread
  public PastOrdersAdapter$ViewHolderItem_ViewBinding(PastOrdersAdapter.ViewHolderItem target,
      View source) {
    this.target = target;

    target.outletName = Utils.findRequiredViewAsType(source, R.id.outletName, "field 'outletName'", TextView.class);
    target.outletLocation = Utils.findRequiredViewAsType(source, R.id.outletLocation, "field 'outletLocation'", TextView.class);
    target.totalPrice = Utils.findRequiredViewAsType(source, R.id.totalPrice, "field 'totalPrice'", TextView.class);
    target.dishItemsOrdered = Utils.findRequiredViewAsType(source, R.id.dishItemsOrdered, "field 'dishItemsOrdered'", TextView.class);
    target.dateAndTime = Utils.findRequiredViewAsType(source, R.id.dateAndTime, "field 'dateAndTime'", TextView.class);
    target.reOrderTrackButton = Utils.findRequiredViewAsType(source, R.id.reOrderTrackButton, "field 'reOrderTrackButton'", TextView.class);
    target.showMore = Utils.findRequiredViewAsType(source, R.id.showMore, "field 'showMore'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PastOrdersAdapter.ViewHolderItem target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.outletName = null;
    target.outletLocation = null;
    target.totalPrice = null;
    target.dishItemsOrdered = null;
    target.dateAndTime = null;
    target.reOrderTrackButton = null;
    target.showMore = null;
  }
}
