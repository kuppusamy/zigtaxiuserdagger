// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.UiUtils.SwitchButton.SwitchButton;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RestaurantDetailsActivity_ViewBinding implements Unbinder {
  private RestaurantDetailsActivity target;

  @UiThread
  public RestaurantDetailsActivity_ViewBinding(RestaurantDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RestaurantDetailsActivity_ViewBinding(RestaurantDetailsActivity target, View source) {
    this.target = target;

    target.categoryRecyclerView = Utils.findRequiredViewAsType(source, R.id.categoryRecyclerView, "field 'categoryRecyclerView'", RecyclerView.class);
    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", ImageView.class);
    target.nestedScrollView = Utils.findRequiredViewAsType(source, R.id.nestedScrollView, "field 'nestedScrollView'", NestedScrollView.class);
    target.swipeView = Utils.findRequiredViewAsType(source, R.id.swipeView, "field 'swipeView'", SwipeRefreshLayout.class);
    target.contentLayout = Utils.findRequiredView(source, R.id.contentLayout, "field 'contentLayout'");
    target.loadingLayout = Utils.findRequiredView(source, R.id.loadingLayout, "field 'loadingLayout'");
    target.noInternetLayout = Utils.findRequiredView(source, R.id.noInternetLayout, "field 'noInternetLayout'");
    target.restaurantName = Utils.findRequiredViewAsType(source, R.id.restaurantName, "field 'restaurantName'", TextView.class);
    target.restaurantCuisineType = Utils.findRequiredViewAsType(source, R.id.restaurantCuisineType, "field 'restaurantCuisineType'", TextView.class);
    target.restaurantDeliveryTime = Utils.findRequiredViewAsType(source, R.id.restaurantDeliveryTime, "field 'restaurantDeliveryTime'", TextView.class);
    target.restaurantPrice = Utils.findRequiredViewAsType(source, R.id.restaurantPrice, "field 'restaurantPrice'", TextView.class);
    target.restaurantRating = Utils.findRequiredViewAsType(source, R.id.restaurantRating, "field 'restaurantRating'", TextView.class);
    target.menuButton = Utils.findRequiredViewAsType(source, R.id.menuButton, "field 'menuButton'", RelativeLayout.class);
    target.transparentLayout = Utils.findRequiredViewAsType(source, R.id.transparentLayout, "field 'transparentLayout'", RelativeLayout.class);
    target.menu_layout = Utils.findRequiredViewAsType(source, R.id.menu_layout, "field 'menu_layout'", LinearLayout.class);
    target.cartLayout = Utils.findRequiredViewAsType(source, R.id.cartLayout, "field 'cartLayout'", RelativeLayout.class);
    target.totalItems = Utils.findRequiredViewAsType(source, R.id.totalItems, "field 'totalItems'", TextView.class);
    target.totalAmount = Utils.findRequiredViewAsType(source, R.id.totalAmount, "field 'totalAmount'", TextView.class);
    target.viewCart = Utils.findRequiredViewAsType(source, R.id.viewCart, "field 'viewCart'", TextView.class);
    target.restaurantNameToolbar = Utils.findRequiredViewAsType(source, R.id.restaurantNameToolbar, "field 'restaurantNameToolbar'", TextView.class);
    target.menuRecycler = Utils.findRequiredViewAsType(source, R.id.menuRecycler, "field 'menuRecycler'", RecyclerView.class);
    target.vegOnlySwitch = Utils.findRequiredViewAsType(source, R.id.vegOnlySwitch, "field 'vegOnlySwitch'", SwitchButton.class);
    target.estimatedTimeLayout = Utils.findRequiredViewAsType(source, R.id.estimatedTimeLayout, "field 'estimatedTimeLayout'", LinearLayout.class);
    target.retryButton = Utils.findRequiredViewAsType(source, R.id.retryButton, "field 'retryButton'", RelativeLayout.class);
    target.coupon_description = Utils.findRequiredViewAsType(source, R.id.coupon_description, "field 'coupon_description'", TextView.class);
    target.couponLayout = Utils.findRequiredViewAsType(source, R.id.couponLayout, "field 'couponLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RestaurantDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.categoryRecyclerView = null;
    target.backButton = null;
    target.nestedScrollView = null;
    target.swipeView = null;
    target.contentLayout = null;
    target.loadingLayout = null;
    target.noInternetLayout = null;
    target.restaurantName = null;
    target.restaurantCuisineType = null;
    target.restaurantDeliveryTime = null;
    target.restaurantPrice = null;
    target.restaurantRating = null;
    target.menuButton = null;
    target.transparentLayout = null;
    target.menu_layout = null;
    target.cartLayout = null;
    target.totalItems = null;
    target.totalAmount = null;
    target.viewCart = null;
    target.restaurantNameToolbar = null;
    target.menuRecycler = null;
    target.vegOnlySwitch = null;
    target.estimatedTimeLayout = null;
    target.retryButton = null;
    target.coupon_description = null;
    target.couponLayout = null;
  }
}
