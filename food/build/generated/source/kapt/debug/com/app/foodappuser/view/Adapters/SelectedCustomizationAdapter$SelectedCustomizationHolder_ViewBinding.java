// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelectedCustomizationAdapter$SelectedCustomizationHolder_ViewBinding implements Unbinder {
  private SelectedCustomizationAdapter.SelectedCustomizationHolder target;

  @UiThread
  public SelectedCustomizationAdapter$SelectedCustomizationHolder_ViewBinding(
      SelectedCustomizationAdapter.SelectedCustomizationHolder target, View source) {
    this.target = target;

    target.dishName = Utils.findRequiredViewAsType(source, R.id.dishName, "field 'dishName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SelectedCustomizationAdapter.SelectedCustomizationHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.dishName = null;
  }
}
