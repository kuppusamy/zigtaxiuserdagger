// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Fragments;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.DialogUtils.CustomTextInputEditText;
import com.app.foodappuser.Utilities.UiUtils.ExpandableLayout;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AccountFragment_ViewBinding implements Unbinder {
  private AccountFragment target;

  @UiThread
  public AccountFragment_ViewBinding(AccountFragment target, View source) {
    this.target = target;

    target.nameText = Utils.findRequiredViewAsType(source, R.id.nameText, "field 'nameText'", TextView.class);
    target.mobileNumberText = Utils.findRequiredViewAsType(source, R.id.mobileNumberText, "field 'mobileNumberText'", TextView.class);
    target.emailText = Utils.findRequiredViewAsType(source, R.id.emailText, "field 'emailText'", TextView.class);
    target.editButton = Utils.findRequiredViewAsType(source, R.id.editButton, "field 'editButton'", RelativeLayout.class);
    target.myAccountButton = Utils.findRequiredViewAsType(source, R.id.myAccountButton, "field 'myAccountButton'", LinearLayout.class);
    target.myAccountLayout = Utils.findRequiredViewAsType(source, R.id.myAccountLayout, "field 'myAccountLayout'", ExpandableLayout.class);
    target.manageAddressButton = Utils.findRequiredViewAsType(source, R.id.manageAddressButton, "field 'manageAddressButton'", LinearLayout.class);
    target.blockImage = Utils.findRequiredViewAsType(source, R.id.blockImage, "field 'blockImage'", ImageView.class);
    target.focusThief = Utils.findRequiredView(source, R.id.focus_thief, "field 'focusThief'");
    target.progressBarChangePassword = Utils.findRequiredViewAsType(source, R.id.progressBarChangePassword, "field 'progressBarChangePassword'", ProgressBar.class);
    target.progressBarAccount = Utils.findRequiredViewAsType(source, R.id.progressBarAccount, "field 'progressBarAccount'", ProgressBar.class);
    target.editCloseButton = Utils.findRequiredViewAsType(source, R.id.editCloseButton, "field 'editCloseButton'", ImageView.class);
    target.saveButton = Utils.findRequiredViewAsType(source, R.id.saveButton, "field 'saveButton'", RelativeLayout.class);
    target.nameEditText = Utils.findRequiredViewAsType(source, R.id.nameEditText, "field 'nameEditText'", CustomTextInputEditText.class);
    target.newPasswordWrapper = Utils.findRequiredViewAsType(source, R.id.newPasswordWrapper, "field 'newPasswordWrapper'", TextInputLayout.class);
    target.newPasswordContainer = Utils.findRequiredViewAsType(source, R.id.newPasswordContainer, "field 'newPasswordContainer'", LinearLayout.class);
    target.emailAddress = Utils.findRequiredViewAsType(source, R.id.emailAddress, "field 'emailAddress'", CustomTextInputEditText.class);
    target.confirmPasswordWrapper = Utils.findRequiredViewAsType(source, R.id.confirmPasswordWrapper, "field 'confirmPasswordWrapper'", TextInputLayout.class);
    target.confirmPasswordContainer = Utils.findRequiredViewAsType(source, R.id.confirmPasswordContainer, "field 'confirmPasswordContainer'", LinearLayout.class);
    target.slideDownLayout = Utils.findRequiredViewAsType(source, R.id.slideDownLayout, "field 'slideDownLayout'", RelativeLayout.class);
    target.changePasswordLayout = Utils.findRequiredViewAsType(source, R.id.changePasswordLayout, "field 'changePasswordLayout'", RelativeLayout.class);
    target.logoutButton = Utils.findRequiredViewAsType(source, R.id.logoutButton, "field 'logoutButton'", CardView.class);
    target.faqLinksButton = Utils.findRequiredViewAsType(source, R.id.faqLinksButton, "field 'faqLinksButton'", LinearLayout.class);
    target.pastOrdersRecycler = Utils.findRequiredViewAsType(source, R.id.pastOrdersRecycler, "field 'pastOrdersRecycler'", RecyclerView.class);
    target.pastOrdersLayout = Utils.findRequiredViewAsType(source, R.id.pastOrdersLayout, "field 'pastOrdersLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AccountFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.nameText = null;
    target.mobileNumberText = null;
    target.emailText = null;
    target.editButton = null;
    target.myAccountButton = null;
    target.myAccountLayout = null;
    target.manageAddressButton = null;
    target.blockImage = null;
    target.focusThief = null;
    target.progressBarChangePassword = null;
    target.progressBarAccount = null;
    target.editCloseButton = null;
    target.saveButton = null;
    target.nameEditText = null;
    target.newPasswordWrapper = null;
    target.newPasswordContainer = null;
    target.emailAddress = null;
    target.confirmPasswordWrapper = null;
    target.confirmPasswordContainer = null;
    target.slideDownLayout = null;
    target.changePasswordLayout = null;
    target.logoutButton = null;
    target.faqLinksButton = null;
    target.pastOrdersRecycler = null;
    target.pastOrdersLayout = null;
  }
}
