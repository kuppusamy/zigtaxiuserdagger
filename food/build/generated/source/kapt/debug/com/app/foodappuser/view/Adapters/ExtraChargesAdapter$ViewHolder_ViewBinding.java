// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ExtraChargesAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ExtraChargesAdapter.ViewHolder target;

  @UiThread
  public ExtraChargesAdapter$ViewHolder_ViewBinding(ExtraChargesAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.chargesText = Utils.findRequiredViewAsType(source, R.id.chargesText, "field 'chargesText'", TextView.class);
    target.chargesCost = Utils.findRequiredViewAsType(source, R.id.chargesCost, "field 'chargesCost'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ExtraChargesAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.chargesText = null;
    target.chargesCost = null;
  }
}
