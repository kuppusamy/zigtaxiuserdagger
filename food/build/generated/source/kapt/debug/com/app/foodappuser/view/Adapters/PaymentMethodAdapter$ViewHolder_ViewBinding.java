// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PaymentMethodAdapter$ViewHolder_ViewBinding implements Unbinder {
  private PaymentMethodAdapter.ViewHolder target;

  @UiThread
  public PaymentMethodAdapter$ViewHolder_ViewBinding(PaymentMethodAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.paymentName = Utils.findRequiredViewAsType(source, R.id.paymentName, "field 'paymentName'", TextView.class);
    target.cardName = Utils.findRequiredViewAsType(source, R.id.cardName, "field 'cardName'", TextView.class);
    target.checkPayment = Utils.findRequiredViewAsType(source, R.id.checkPayment, "field 'checkPayment'", RadioButton.class);
    target.paymentImage = Utils.findRequiredViewAsType(source, R.id.paymentImage, "field 'paymentImage'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PaymentMethodAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.paymentName = null;
    target.cardName = null;
    target.checkPayment = null;
    target.paymentImage = null;
  }
}
