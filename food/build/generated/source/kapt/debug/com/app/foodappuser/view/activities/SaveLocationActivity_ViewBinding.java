// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SaveLocationActivity_ViewBinding implements Unbinder {
  private SaveLocationActivity target;

  @UiThread
  public SaveLocationActivity_ViewBinding(SaveLocationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SaveLocationActivity_ViewBinding(SaveLocationActivity target, View source) {
    this.target = target;

    target.addresslayout = Utils.findRequiredViewAsType(source, R.id.addresslayout, "field 'addresslayout'", LinearLayout.class);
    target.backimg = Utils.findRequiredViewAsType(source, R.id.backimg, "field 'backimg'", ImageView.class);
    target.topPanel = Utils.findRequiredViewAsType(source, R.id.topPanel, "field 'topPanel'", RelativeLayout.class);
    target.bottomSheet = Utils.findRequiredViewAsType(source, R.id.bottomSheet, "field 'bottomSheet'", FrameLayout.class);
    target.homeIcon = Utils.findRequiredViewAsType(source, R.id.homeIcon, "field 'homeIcon'", ImageView.class);
    target.homeLayout = Utils.findRequiredViewAsType(source, R.id.homeLayout, "field 'homeLayout'", RelativeLayout.class);
    target.homeText = Utils.findRequiredViewAsType(source, R.id.homeText, "field 'homeText'", TextView.class);
    target.homeBottomView = Utils.findRequiredView(source, R.id.homeBottomView, "field 'homeBottomView'");
    target.workIcon = Utils.findRequiredViewAsType(source, R.id.workIcon, "field 'workIcon'", ImageView.class);
    target.workLayout = Utils.findRequiredViewAsType(source, R.id.workLayout, "field 'workLayout'", RelativeLayout.class);
    target.workText = Utils.findRequiredViewAsType(source, R.id.workText, "field 'workText'", TextView.class);
    target.workBottomView = Utils.findRequiredView(source, R.id.workBottomView, "field 'workBottomView'");
    target.otherIcon = Utils.findRequiredViewAsType(source, R.id.otherIcon, "field 'otherIcon'", ImageView.class);
    target.otherLayout = Utils.findRequiredViewAsType(source, R.id.otherLayout, "field 'otherLayout'", RelativeLayout.class);
    target.otherText = Utils.findRequiredViewAsType(source, R.id.otherText, "field 'otherText'", TextView.class);
    target.otherBottomView = Utils.findRequiredView(source, R.id.otherBottomView, "field 'otherBottomView'");
    target.otherAddress = Utils.findRequiredViewAsType(source, R.id.otherAddress, "field 'otherAddress'", EditText.class);
    target.locationName = Utils.findRequiredViewAsType(source, R.id.locationName, "field 'locationName'", EditText.class);
    target.house_number = Utils.findRequiredViewAsType(source, R.id.house_number, "field 'house_number'", EditText.class);
    target.landmark = Utils.findRequiredViewAsType(source, R.id.landmark, "field 'landmark'", EditText.class);
    target.saveButton = Utils.findRequiredViewAsType(source, R.id.saveButton, "field 'saveButton'", Button.class);
    target.progressBarOTP = Utils.findRequiredViewAsType(source, R.id.progressBarOTP, "field 'progressBarOTP'", ProgressBar.class);
    target.othersAddresLayout = Utils.findRequiredViewAsType(source, R.id.othersAddresLayout, "field 'othersAddresLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SaveLocationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.addresslayout = null;
    target.backimg = null;
    target.topPanel = null;
    target.bottomSheet = null;
    target.homeIcon = null;
    target.homeLayout = null;
    target.homeText = null;
    target.homeBottomView = null;
    target.workIcon = null;
    target.workLayout = null;
    target.workText = null;
    target.workBottomView = null;
    target.otherIcon = null;
    target.otherLayout = null;
    target.otherText = null;
    target.otherBottomView = null;
    target.otherAddress = null;
    target.locationName = null;
    target.house_number = null;
    target.landmark = null;
    target.saveButton = null;
    target.progressBarOTP = null;
    target.othersAddresLayout = null;
  }
}
