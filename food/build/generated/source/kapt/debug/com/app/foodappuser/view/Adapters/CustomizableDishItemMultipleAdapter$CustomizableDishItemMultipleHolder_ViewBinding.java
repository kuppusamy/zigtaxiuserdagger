// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomizableDishItemMultipleAdapter$CustomizableDishItemMultipleHolder_ViewBinding implements Unbinder {
  private CustomizableDishItemMultipleAdapter.CustomizableDishItemMultipleHolder target;

  @UiThread
  public CustomizableDishItemMultipleAdapter$CustomizableDishItemMultipleHolder_ViewBinding(
      CustomizableDishItemMultipleAdapter.CustomizableDishItemMultipleHolder target, View source) {
    this.target = target;

    target.dishCheckBox = Utils.findRequiredViewAsType(source, R.id.dishCheckBox, "field 'dishCheckBox'", CheckBox.class);
    target.displayPrice = Utils.findRequiredViewAsType(source, R.id.displayPrice, "field 'displayPrice'", TextView.class);
    target.isVegView = Utils.findRequiredViewAsType(source, R.id.isVegView, "field 'isVegView'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomizableDishItemMultipleAdapter.CustomizableDishItemMultipleHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.dishCheckBox = null;
    target.displayPrice = null;
    target.isVegView = null;
  }
}
