// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderDetailActivity_ViewBinding implements Unbinder {
  private OrderDetailActivity target;

  @UiThread
  public OrderDetailActivity_ViewBinding(OrderDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderDetailActivity_ViewBinding(OrderDetailActivity target, View source) {
    this.target = target;

    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", ImageView.class);
    target.orderNumber = Utils.findRequiredViewAsType(source, R.id.orderNumber, "field 'orderNumber'", TextView.class);
    target.totalItem = Utils.findRequiredViewAsType(source, R.id.totalItem, "field 'totalItem'", TextView.class);
    target.toPayAmount = Utils.findRequiredViewAsType(source, R.id.toPayAmount, "field 'toPayAmount'", TextView.class);
    target.outletName = Utils.findRequiredViewAsType(source, R.id.outletName, "field 'outletName'", TextView.class);
    target.outletLocation = Utils.findRequiredViewAsType(source, R.id.outletLocation, "field 'outletLocation'", TextView.class);
    target.addressTypeName = Utils.findRequiredViewAsType(source, R.id.addressTypeName, "field 'addressTypeName'", TextView.class);
    target.addressLocation = Utils.findRequiredViewAsType(source, R.id.addressLocation, "field 'addressLocation'", TextView.class);
    target.dishesRecycler = Utils.findRequiredViewAsType(source, R.id.dishesRecycler, "field 'dishesRecycler'", RecyclerView.class);
    target.billDetailsRecycler = Utils.findRequiredViewAsType(source, R.id.billDetailsRecycler, "field 'billDetailsRecycler'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backButton = null;
    target.orderNumber = null;
    target.totalItem = null;
    target.toPayAmount = null;
    target.outletName = null;
    target.outletLocation = null;
    target.addressTypeName = null;
    target.addressLocation = null;
    target.dishesRecycler = null;
    target.billDetailsRecycler = null;
  }
}
