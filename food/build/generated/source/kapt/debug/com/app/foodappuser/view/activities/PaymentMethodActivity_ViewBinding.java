// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PaymentMethodActivity_ViewBinding implements Unbinder {
  private PaymentMethodActivity target;

  @UiThread
  public PaymentMethodActivity_ViewBinding(PaymentMethodActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PaymentMethodActivity_ViewBinding(PaymentMethodActivity target, View source) {
    this.target = target;

    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", ImageView.class);
    target.totalItem = Utils.findRequiredViewAsType(source, R.id.totalItem, "field 'totalItem'", TextView.class);
    target.toPayAmount = Utils.findRequiredViewAsType(source, R.id.toPayAmount, "field 'toPayAmount'", TextView.class);
    target.paymentMethodsRecycler = Utils.findRequiredViewAsType(source, R.id.paymentMethodsRecycler, "field 'paymentMethodsRecycler'", RecyclerView.class);
    target.proceedPayButton = Utils.findRequiredViewAsType(source, R.id.proceedPayButton, "field 'proceedPayButton'", RelativeLayout.class);
    target.loadingLayout = Utils.findRequiredView(source, R.id.loadingLayout, "field 'loadingLayout'");
    target.contentLayout = Utils.findRequiredView(source, R.id.contentLayout, "field 'contentLayout'");
  }

  @Override
  @CallSuper
  public void unbind() {
    PaymentMethodActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backButton = null;
    target.totalItem = null;
    target.toPayAmount = null;
    target.paymentMethodsRecycler = null;
    target.proceedPayButton = null;
    target.loadingLayout = null;
    target.contentLayout = null;
  }
}
