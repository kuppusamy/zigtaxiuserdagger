// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OutLetsAdapter$Restaurant_ViewBinding implements Unbinder {
  private OutLetsAdapter.Restaurant target;

  @UiThread
  public OutLetsAdapter$Restaurant_ViewBinding(OutLetsAdapter.Restaurant target, View source) {
    this.target = target;

    target.restaurantOutletsName = Utils.findRequiredViewAsType(source, R.id.restaurantOutletsName, "field 'restaurantOutletsName'", TextView.class);
    target.restaurantOutletsRating = Utils.findRequiredViewAsType(source, R.id.restaurantOutletsRating, "field 'restaurantOutletsRating'", TextView.class);
    target.restaurantOutletsTime = Utils.findRequiredViewAsType(source, R.id.restaurantOutletsTime, "field 'restaurantOutletsTime'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OutLetsAdapter.Restaurant target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.restaurantOutletsName = null;
    target.restaurantOutletsRating = null;
    target.restaurantOutletsTime = null;
  }
}
