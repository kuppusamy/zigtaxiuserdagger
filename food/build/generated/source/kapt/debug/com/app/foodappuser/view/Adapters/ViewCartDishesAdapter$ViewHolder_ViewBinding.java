// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.Ticker.TickerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewCartDishesAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ViewCartDishesAdapter.ViewHolder target;

  @UiThread
  public ViewCartDishesAdapter$ViewHolder_ViewBinding(ViewCartDishesAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.isVegView = Utils.findRequiredViewAsType(source, R.id.isVegView, "field 'isVegView'", ImageView.class);
    target.dishName = Utils.findRequiredViewAsType(source, R.id.dishName, "field 'dishName'", TextView.class);
    target.unAvailableText = Utils.findRequiredViewAsType(source, R.id.unAvailableText, "field 'unAvailableText'", TextView.class);
    target.decreaseCart = Utils.findRequiredViewAsType(source, R.id.decreaseCart, "field 'decreaseCart'", RelativeLayout.class);
    target.quantity = Utils.findRequiredViewAsType(source, R.id.quantity, "field 'quantity'", TickerView.class);
    target.increaseCart = Utils.findRequiredViewAsType(source, R.id.increaseCart, "field 'increaseCart'", RelativeLayout.class);
    target.actionLayout = Utils.findRequiredViewAsType(source, R.id.actionLayout, "field 'actionLayout'", LinearLayout.class);
    target.deleteItem = Utils.findRequiredViewAsType(source, R.id.deleteItem, "field 'deleteItem'", ImageView.class);
    target.displayPrice = Utils.findRequiredViewAsType(source, R.id.displayPrice, "field 'displayPrice'", TextView.class);
    target.customizableLayout = Utils.findRequiredViewAsType(source, R.id.customizableLayout, "field 'customizableLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ViewCartDishesAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.isVegView = null;
    target.dishName = null;
    target.unAvailableText = null;
    target.decreaseCart = null;
    target.quantity = null;
    target.increaseCart = null;
    target.actionLayout = null;
    target.deleteItem = null;
    target.displayPrice = null;
    target.customizableLayout = null;
  }
}
