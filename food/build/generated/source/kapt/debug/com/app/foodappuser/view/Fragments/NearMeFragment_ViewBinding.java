// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Fragments;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NearMeFragment_ViewBinding implements Unbinder {
  private NearMeFragment target;

  @UiThread
  public NearMeFragment_ViewBinding(NearMeFragment target, View source) {
    this.target = target;

    target.bannerViewPager = Utils.findRequiredViewAsType(source, R.id.bannerViewPager, "field 'bannerViewPager'", RecyclerView.class);
    target.swipeView = Utils.findRequiredViewAsType(source, R.id.swipeView, "field 'swipeView'", SwipeRefreshLayout.class);
    target.contentLayout = Utils.findRequiredView(source, R.id.contentLayout, "field 'contentLayout'");
    target.loadingLayout = Utils.findRequiredView(source, R.id.loadingLayout, "field 'loadingLayout'");
    target.setDeliveryLocationLayout = Utils.findRequiredView(source, R.id.setDeliveryLocationLayout, "field 'setDeliveryLocationLayout'");
    target.locationlayout = Utils.findRequiredViewAsType(source, R.id.locationlayout, "field 'locationlayout'", LinearLayout.class);
    target.restaurantRecyclerView = Utils.findRequiredViewAsType(source, R.id.restaurantRecyclerView, "field 'restaurantRecyclerView'", RecyclerView.class);
    target.restaurantCount = Utils.findRequiredViewAsType(source, R.id.restaurantCount, "field 'restaurantCount'", TextView.class);
    target.addressName = Utils.findRequiredViewAsType(source, R.id.addressName, "field 'addressName'", TextView.class);
    target.addressText = Utils.findRequiredViewAsType(source, R.id.addressText, "field 'addressText'", TextView.class);
    target.locationTypeLayout = Utils.findRequiredViewAsType(source, R.id.locationTypeLayout, "field 'locationTypeLayout'", LinearLayout.class);
    target.setDeliveryLocationButton = Utils.findRequiredViewAsType(source, R.id.setDeliveryLocationButton, "field 'setDeliveryLocationButton'", RelativeLayout.class);
    target.restaurantLayout = Utils.findRequiredViewAsType(source, R.id.restaurantLayout, "field 'restaurantLayout'", NestedScrollView.class);
    target.noRestaurantAvailable = Utils.findRequiredViewAsType(source, R.id.noRestaurantAvailable, "field 'noRestaurantAvailable'", LinearLayout.class);
    target.ratingLayout = Utils.findRequiredView(source, R.id.ratingLayout, "field 'ratingLayout'");
    target.rating_close = Utils.findRequiredViewAsType(source, R.id.rating_close, "field 'rating_close'", ImageView.class);
    target.retryButton = Utils.findRequiredViewAsType(source, R.id.retryButton, "field 'retryButton'", RelativeLayout.class);
    target.connectionLostLayout = Utils.findRequiredView(source, R.id.connectionLostLayout, "field 'connectionLostLayout'");
  }

  @Override
  @CallSuper
  public void unbind() {
    NearMeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.bannerViewPager = null;
    target.swipeView = null;
    target.contentLayout = null;
    target.loadingLayout = null;
    target.setDeliveryLocationLayout = null;
    target.locationlayout = null;
    target.restaurantRecyclerView = null;
    target.restaurantCount = null;
    target.addressName = null;
    target.addressText = null;
    target.locationTypeLayout = null;
    target.setDeliveryLocationButton = null;
    target.restaurantLayout = null;
    target.noRestaurantAvailable = null;
    target.ratingLayout = null;
    target.rating_close = null;
    target.retryButton = null;
    target.connectionLostLayout = null;
  }
}
