// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RestaurantBannerImageAdapter$Restaurant_ViewBinding implements Unbinder {
  private RestaurantBannerImageAdapter.Restaurant target;

  @UiThread
  public RestaurantBannerImageAdapter$Restaurant_ViewBinding(
      RestaurantBannerImageAdapter.Restaurant target, View source) {
    this.target = target;

    target.bannerImageItem = Utils.findRequiredViewAsType(source, R.id.bannerImageItem, "field 'bannerImageItem'", ImageView.class);
    target.button = Utils.findRequiredViewAsType(source, R.id.button, "field 'button'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RestaurantBannerImageAdapter.Restaurant target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.bannerImageItem = null;
    target.button = null;
  }
}
