// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LiveTrackingActivity_ViewBinding implements Unbinder {
  private LiveTrackingActivity target;

  @UiThread
  public LiveTrackingActivity_ViewBinding(LiveTrackingActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LiveTrackingActivity_ViewBinding(LiveTrackingActivity target, View source) {
    this.target = target;

    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", ImageView.class);
    target.orderNumber = Utils.findRequiredViewAsType(source, R.id.orderNumber, "field 'orderNumber'", TextView.class);
    target.totalItem = Utils.findRequiredViewAsType(source, R.id.totalItem, "field 'totalItem'", TextView.class);
    target.toPayAmount = Utils.findRequiredViewAsType(source, R.id.toPayAmount, "field 'toPayAmount'", TextView.class);
    target.orderReceivedTime = Utils.findRequiredViewAsType(source, R.id.orderReceivedTime, "field 'orderReceivedTime'", TextView.class);
    target.orderReceivedTick = Utils.findRequiredViewAsType(source, R.id.orderReceivedTick, "field 'orderReceivedTick'", ImageView.class);
    target.orderReceivedNotTick = Utils.findRequiredViewAsType(source, R.id.orderReceivedNotTick, "field 'orderReceivedNotTick'", ImageView.class);
    target.orderReceivedText = Utils.findRequiredViewAsType(source, R.id.orderReceivedText, "field 'orderReceivedText'", TextView.class);
    target.orderConfirmedTime = Utils.findRequiredViewAsType(source, R.id.orderConfirmedTime, "field 'orderConfirmedTime'", TextView.class);
    target.orderConfirmedTick = Utils.findRequiredViewAsType(source, R.id.orderConfirmedTick, "field 'orderConfirmedTick'", ImageView.class);
    target.orderConfirmedNotTick = Utils.findRequiredViewAsType(source, R.id.orderConfirmedNotTick, "field 'orderConfirmedNotTick'", ImageView.class);
    target.orderConfirmedTextDescri = Utils.findRequiredViewAsType(source, R.id.orderConfirmedTextDescri, "field 'orderConfirmedTextDescri'", TextView.class);
    target.orderPickedUpTime = Utils.findRequiredViewAsType(source, R.id.orderPickedUpTime, "field 'orderPickedUpTime'", TextView.class);
    target.orderPickedUpTick = Utils.findRequiredViewAsType(source, R.id.orderPickedUpTick, "field 'orderPickedUpTick'", ImageView.class);
    target.orderPickedUpNotTick = Utils.findRequiredViewAsType(source, R.id.orderPickedUpNotTick, "field 'orderPickedUpNotTick'", ImageView.class);
    target.orderPickedUpText = Utils.findRequiredViewAsType(source, R.id.orderPickedUpText, "field 'orderPickedUpText'", TextView.class);
    target.orderReceivedLayout = Utils.findRequiredViewAsType(source, R.id.orderReceivedLayout, "field 'orderReceivedLayout'", LinearLayout.class);
    target.orderConfirmedLayout = Utils.findRequiredViewAsType(source, R.id.orderConfirmedLayout, "field 'orderConfirmedLayout'", LinearLayout.class);
    target.orderPickedupLayout = Utils.findRequiredViewAsType(source, R.id.orderPickedupLayout, "field 'orderPickedupLayout'", LinearLayout.class);
    target.orderRejectedLayout = Utils.findRequiredViewAsType(source, R.id.orderRejectedLayout, "field 'orderRejectedLayout'", LinearLayout.class);
    target.orderDeliveredLayout = Utils.findRequiredViewAsType(source, R.id.orderDeliveredLayout, "field 'orderDeliveredLayout'", LinearLayout.class);
    target.livetrackingLoadingLayout = Utils.findRequiredView(source, R.id.livetrackingLoadingLayout, "field 'livetrackingLoadingLayout'");
    target.livetrackingContentLayout = Utils.findRequiredView(source, R.id.livetrackingContentLayout, "field 'livetrackingContentLayout'");
    target.eta_time = Utils.findRequiredViewAsType(source, R.id.eta_time, "field 'eta_time'", TextView.class);
    target.orderConfirmedText = Utils.findRequiredViewAsType(source, R.id.orderConfirmedText, "field 'orderConfirmedText'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LiveTrackingActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backButton = null;
    target.orderNumber = null;
    target.totalItem = null;
    target.toPayAmount = null;
    target.orderReceivedTime = null;
    target.orderReceivedTick = null;
    target.orderReceivedNotTick = null;
    target.orderReceivedText = null;
    target.orderConfirmedTime = null;
    target.orderConfirmedTick = null;
    target.orderConfirmedNotTick = null;
    target.orderConfirmedTextDescri = null;
    target.orderPickedUpTime = null;
    target.orderPickedUpTick = null;
    target.orderPickedUpNotTick = null;
    target.orderPickedUpText = null;
    target.orderReceivedLayout = null;
    target.orderConfirmedLayout = null;
    target.orderPickedupLayout = null;
    target.orderRejectedLayout = null;
    target.orderDeliveredLayout = null;
    target.livetrackingLoadingLayout = null;
    target.livetrackingContentLayout = null;
    target.eta_time = null;
    target.orderConfirmedText = null;
  }
}
