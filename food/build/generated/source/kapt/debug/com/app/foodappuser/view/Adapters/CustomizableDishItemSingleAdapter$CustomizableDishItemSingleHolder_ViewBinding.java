// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomizableDishItemSingleAdapter$CustomizableDishItemSingleHolder_ViewBinding implements Unbinder {
  private CustomizableDishItemSingleAdapter.CustomizableDishItemSingleHolder target;

  @UiThread
  public CustomizableDishItemSingleAdapter$CustomizableDishItemSingleHolder_ViewBinding(
      CustomizableDishItemSingleAdapter.CustomizableDishItemSingleHolder target, View source) {
    this.target = target;

    target.singleSelect = Utils.findRequiredViewAsType(source, R.id.singleSelect, "field 'singleSelect'", RadioButton.class);
    target.displayPrice = Utils.findRequiredViewAsType(source, R.id.displayPrice, "field 'displayPrice'", TextView.class);
    target.isVegView = Utils.findRequiredViewAsType(source, R.id.isVegView, "field 'isVegView'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomizableDishItemSingleAdapter.CustomizableDishItemSingleHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.singleSelect = null;
    target.displayPrice = null;
    target.isVegView = null;
  }
}
