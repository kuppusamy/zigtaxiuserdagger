// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChooseLocationActivity_ViewBinding implements Unbinder {
  private ChooseLocationActivity target;

  @UiThread
  public ChooseLocationActivity_ViewBinding(ChooseLocationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChooseLocationActivity_ViewBinding(ChooseLocationActivity target, View source) {
    this.target = target;

    target.autoCompleteList = Utils.findRequiredViewAsType(source, R.id.autoCompleteList, "field 'autoCompleteList'", RecyclerView.class);
    target.addressList = Utils.findRequiredViewAsType(source, R.id.addressList, "field 'addressList'", RecyclerView.class);
    target.searchBox = Utils.findRequiredViewAsType(source, R.id.searchBox, "field 'searchBox'", EditText.class);
    target.currentLocationLayout = Utils.findRequiredViewAsType(source, R.id.currentLocationLayout, "field 'currentLocationLayout'", LinearLayout.class);
    target.currentLocationWithAddressLayout = Utils.findRequiredViewAsType(source, R.id.currentLocationWithAddressLayout, "field 'currentLocationWithAddressLayout'", LinearLayout.class);
    target.savedAddressLayout = Utils.findRequiredViewAsType(source, R.id.savedAddressLayout, "field 'savedAddressLayout'", LinearLayout.class);
    target.progressBarOTP = Utils.findRequiredViewAsType(source, R.id.progressBarOTP, "field 'progressBarOTP'", ProgressBar.class);
    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChooseLocationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.autoCompleteList = null;
    target.addressList = null;
    target.searchBox = null;
    target.currentLocationLayout = null;
    target.currentLocationWithAddressLayout = null;
    target.savedAddressLayout = null;
    target.progressBarOTP = null;
    target.backButton = null;
  }
}
