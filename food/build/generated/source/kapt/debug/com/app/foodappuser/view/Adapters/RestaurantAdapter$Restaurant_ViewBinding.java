// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.UiUtils.ExpandableLayout;
import com.app.foodappuser.Utilities.UiUtils.ShadowView.ShadowView;
import com.makeramen.roundedimageview.RoundedImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RestaurantAdapter$Restaurant_ViewBinding implements Unbinder {
  private RestaurantAdapter.Restaurant target;

  @UiThread
  public RestaurantAdapter$Restaurant_ViewBinding(RestaurantAdapter.Restaurant target,
      View source) {
    this.target = target;

    target.restaurantImage = Utils.findRequiredViewAsType(source, R.id.restaurantImage, "field 'restaurantImage'", RoundedImageView.class);
    target.restaurantName = Utils.findRequiredViewAsType(source, R.id.restaurantName, "field 'restaurantName'", TextView.class);
    target.restaurantCuisineType = Utils.findRequiredViewAsType(source, R.id.restaurantCuisineType, "field 'restaurantCuisineType'", TextView.class);
    target.restaurantRating = Utils.findRequiredViewAsType(source, R.id.restaurantRating, "field 'restaurantRating'", TextView.class);
    target.restaurantDeliveryTime = Utils.findRequiredViewAsType(source, R.id.restaurantDeliveryTime, "field 'restaurantDeliveryTime'", TextView.class);
    target.restaurantPrice = Utils.findRequiredViewAsType(source, R.id.restaurantPrice, "field 'restaurantPrice'", TextView.class);
    target.restaurantOutletsCount = Utils.findRequiredViewAsType(source, R.id.restaurantOutletsCount, "field 'restaurantOutletsCount'", TextView.class);
    target.restaurantOutletsCountLayout = Utils.findRequiredViewAsType(source, R.id.restaurantOutletsCountLayout, "field 'restaurantOutletsCountLayout'", LinearLayout.class);
    target.shadowView = Utils.findRequiredViewAsType(source, R.id.shadow_view, "field 'shadowView'", ShadowView.class);
    target.outLetRecyclerView = Utils.findRequiredViewAsType(source, R.id.outLetRecyclerView, "field 'outLetRecyclerView'", RecyclerView.class);
    target.outLetLayout = Utils.findRequiredViewAsType(source, R.id.outLetLayout, "field 'outLetLayout'", ShadowView.class);
    target.expandableLayout = Utils.findRequiredViewAsType(source, R.id.expandableLayout, "field 'expandableLayout'", ExpandableLayout.class);
    target.layout = Utils.findRequiredViewAsType(source, R.id.layout, "field 'layout'", LinearLayout.class);
    target.coupon_description = Utils.findRequiredViewAsType(source, R.id.coupon_description, "field 'coupon_description'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RestaurantAdapter.Restaurant target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.restaurantImage = null;
    target.restaurantName = null;
    target.restaurantCuisineType = null;
    target.restaurantRating = null;
    target.restaurantDeliveryTime = null;
    target.restaurantPrice = null;
    target.restaurantOutletsCount = null;
    target.restaurantOutletsCountLayout = null;
    target.shadowView = null;
    target.outLetRecyclerView = null;
    target.outLetLayout = null;
    target.expandableLayout = null;
    target.layout = null;
    target.coupon_description = null;
  }
}
