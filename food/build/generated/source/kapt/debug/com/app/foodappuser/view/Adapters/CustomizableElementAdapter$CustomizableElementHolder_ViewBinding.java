// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomizableElementAdapter$CustomizableElementHolder_ViewBinding implements Unbinder {
  private CustomizableElementAdapter.CustomizableElementHolder target;

  @UiThread
  public CustomizableElementAdapter$CustomizableElementHolder_ViewBinding(
      CustomizableElementAdapter.CustomizableElementHolder target, View source) {
    this.target = target;

    target.dishName = Utils.findRequiredViewAsType(source, R.id.dishName, "field 'dishName'", TextView.class);
    target.customizableDishItemsRecycler = Utils.findRequiredViewAsType(source, R.id.customizableDishItemsRecycler, "field 'customizableDishItemsRecycler'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomizableElementAdapter.CustomizableElementHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.dishName = null;
    target.customizableDishItemsRecycler = null;
  }
}
