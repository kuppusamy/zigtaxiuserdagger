// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignUpActivity_ViewBinding implements Unbinder {
  private SignUpActivity target;

  @UiThread
  public SignUpActivity_ViewBinding(SignUpActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SignUpActivity_ViewBinding(SignUpActivity target, View source) {
    this.target = target;

    target.progressBarOTP = Utils.findRequiredViewAsType(source, R.id.progressBarOTP, "field 'progressBarOTP'", ProgressBar.class);
    target.phoneNumber = Utils.findRequiredViewAsType(source, R.id.phoneNumber, "field 'phoneNumber'", TextView.class);
    target.signUpText = Utils.findRequiredViewAsType(source, R.id.signUpText, "field 'signUpText'", TextView.class);
    target.terms_and_text = Utils.findRequiredViewAsType(source, R.id.terms_and_text, "field 'terms_and_text'", TextView.class);
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", EditText.class);
    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", EditText.class);
    target.password = Utils.findRequiredViewAsType(source, R.id.password, "field 'password'", EditText.class);
    target.referralCodeCheckbox = Utils.findRequiredViewAsType(source, R.id.referralCodeCheckbox, "field 'referralCodeCheckbox'", CheckBox.class);
    target.referralCode = Utils.findRequiredViewAsType(source, R.id.referralCode, "field 'referralCode'", EditText.class);
    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", ImageView.class);
    target.referralCodeParent = Utils.findRequiredViewAsType(source, R.id.referralCodeParent, "field 'referralCodeParent'", LinearLayout.class);
    target.passwordWrapper = Utils.findRequiredViewAsType(source, R.id.passwordWrapper, "field 'passwordWrapper'", TextInputLayout.class);
    target.signUpButton = Utils.findRequiredViewAsType(source, R.id.signUpButton, "field 'signUpButton'", CardView.class);
    target.passwordLayout = Utils.findRequiredViewAsType(source, R.id.passwordLayout, "field 'passwordLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SignUpActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.progressBarOTP = null;
    target.phoneNumber = null;
    target.signUpText = null;
    target.terms_and_text = null;
    target.email = null;
    target.name = null;
    target.password = null;
    target.referralCodeCheckbox = null;
    target.referralCode = null;
    target.backButton = null;
    target.referralCodeParent = null;
    target.passwordWrapper = null;
    target.signUpButton = null;
    target.passwordLayout = null;
  }
}
