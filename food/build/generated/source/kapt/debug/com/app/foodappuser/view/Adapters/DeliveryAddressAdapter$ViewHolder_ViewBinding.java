// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DeliveryAddressAdapter$ViewHolder_ViewBinding implements Unbinder {
  private DeliveryAddressAdapter.ViewHolder target;

  @UiThread
  public DeliveryAddressAdapter$ViewHolder_ViewBinding(DeliveryAddressAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.deliverTypeImage = Utils.findRequiredViewAsType(source, R.id.deliverTypeImage, "field 'deliverTypeImage'", ImageView.class);
    target.typeText = Utils.findRequiredViewAsType(source, R.id.typeText, "field 'typeText'", TextView.class);
    target.deliverAddressText = Utils.findRequiredViewAsType(source, R.id.deliverAddressText, "field 'deliverAddressText'", TextView.class);
    target.deliverMinsText = Utils.findRequiredViewAsType(source, R.id.deliverMinsText, "field 'deliverMinsText'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DeliveryAddressAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.deliverTypeImage = null;
    target.typeText = null;
    target.deliverAddressText = null;
    target.deliverMinsText = null;
  }
}
