// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MenuListAdapter$MenuViewHolder_ViewBinding implements Unbinder {
  private MenuListAdapter.MenuViewHolder target;

  @UiThread
  public MenuListAdapter$MenuViewHolder_ViewBinding(MenuListAdapter.MenuViewHolder target,
      View source) {
    this.target = target;

    target.menuItem = Utils.findRequiredViewAsType(source, R.id.menuItem, "field 'menuItem'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MenuListAdapter.MenuViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.menuItem = null;
  }
}
