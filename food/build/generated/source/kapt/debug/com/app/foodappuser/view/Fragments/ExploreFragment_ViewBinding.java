// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Fragments;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import com.flyco.tablayout.SlidingTabLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ExploreFragment_ViewBinding implements Unbinder {
  private ExploreFragment target;

  @UiThread
  public ExploreFragment_ViewBinding(ExploreFragment target, View source) {
    this.target = target;

    target.tabLayout = Utils.findRequiredViewAsType(source, R.id.tabLayout, "field 'tabLayout'", SlidingTabLayout.class);
    target.pager = Utils.findRequiredViewAsType(source, R.id.pager, "field 'pager'", ViewPager.class);
    target.exploreEditText = Utils.findRequiredViewAsType(source, R.id.exploreEditText, "field 'exploreEditText'", EditText.class);
    target.restaurantApiContentLayout = Utils.findRequiredView(source, R.id.restaurantApiContentLayout, "field 'restaurantApiContentLayout'");
    target.restaurantLocaleContentLayout = Utils.findRequiredView(source, R.id.restaurantLocaleContentLayout, "field 'restaurantLocaleContentLayout'");
    target.recentSearchRecycler = Utils.findRequiredViewAsType(source, R.id.recentSearchRecycler, "field 'recentSearchRecycler'", RecyclerView.class);
    target.clearText = Utils.findRequiredViewAsType(source, R.id.clearText, "field 'clearText'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ExploreFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tabLayout = null;
    target.pager = null;
    target.exploreEditText = null;
    target.restaurantApiContentLayout = null;
    target.restaurantLocaleContentLayout = null;
    target.recentSearchRecycler = null;
    target.clearText = null;
  }
}
