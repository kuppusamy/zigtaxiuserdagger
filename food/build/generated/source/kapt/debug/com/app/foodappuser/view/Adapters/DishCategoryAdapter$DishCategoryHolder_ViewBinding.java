// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DishCategoryAdapter$DishCategoryHolder_ViewBinding implements Unbinder {
  private DishCategoryAdapter.DishCategoryHolder target;

  @UiThread
  public DishCategoryAdapter$DishCategoryHolder_ViewBinding(
      DishCategoryAdapter.DishCategoryHolder target, View source) {
    this.target = target;

    target.categoryName = Utils.findRequiredViewAsType(source, R.id.categoryName, "field 'categoryName'", TextView.class);
    target.categoryRecyclerView = Utils.findRequiredViewAsType(source, R.id.categoryRecyclerView, "field 'categoryRecyclerView'", RecyclerView.class);
    target.bottomView = Utils.findRequiredView(source, R.id.bottomView, "field 'bottomView'");
  }

  @Override
  @CallSuper
  public void unbind() {
    DishCategoryAdapter.DishCategoryHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.categoryName = null;
    target.categoryRecyclerView = null;
    target.bottomView = null;
  }
}
