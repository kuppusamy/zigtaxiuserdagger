// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewCartActivity_ViewBinding implements Unbinder {
  private ViewCartActivity target;

  @UiThread
  public ViewCartActivity_ViewBinding(ViewCartActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ViewCartActivity_ViewBinding(ViewCartActivity target, View source) {
    this.target = target;

    target.container = Utils.findRequiredViewAsType(source, R.id.container, "field 'container'", FrameLayout.class);
    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ViewCartActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.container = null;
    target.backButton = null;
  }
}
