// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PastOrderDishesAdapter$DishesViewHolder_ViewBinding implements Unbinder {
  private PastOrderDishesAdapter.DishesViewHolder target;

  @UiThread
  public PastOrderDishesAdapter$DishesViewHolder_ViewBinding(
      PastOrderDishesAdapter.DishesViewHolder target, View source) {
    this.target = target;

    target.isVegView = Utils.findRequiredViewAsType(source, R.id.isVegView, "field 'isVegView'", ImageView.class);
    target.dishName = Utils.findRequiredViewAsType(source, R.id.dishName, "field 'dishName'", TextView.class);
    target.deleteItem = Utils.findRequiredViewAsType(source, R.id.deleteItem, "field 'deleteItem'", ImageView.class);
    target.displayPrice = Utils.findRequiredViewAsType(source, R.id.displayPrice, "field 'displayPrice'", TextView.class);
    target.customizableLayout = Utils.findRequiredViewAsType(source, R.id.customizableLayout, "field 'customizableLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PastOrderDishesAdapter.DishesViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.isVegView = null;
    target.dishName = null;
    target.deleteItem = null;
    target.displayPrice = null;
    target.customizableLayout = null;
  }
}
