// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RecentSearchesAdapter$Recent_ViewBinding implements Unbinder {
  private RecentSearchesAdapter.Recent target;

  @UiThread
  public RecentSearchesAdapter$Recent_ViewBinding(RecentSearchesAdapter.Recent target,
      View source) {
    this.target = target;

    target.restaurantName = Utils.findRequiredViewAsType(source, R.id.restaurantName, "field 'restaurantName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RecentSearchesAdapter.Recent target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.restaurantName = null;
  }
}
