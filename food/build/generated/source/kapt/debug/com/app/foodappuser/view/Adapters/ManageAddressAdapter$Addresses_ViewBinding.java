// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.Adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ManageAddressAdapter$Addresses_ViewBinding implements Unbinder {
  private ManageAddressAdapter.Addresses target;

  @UiThread
  public ManageAddressAdapter$Addresses_ViewBinding(ManageAddressAdapter.Addresses target,
      View source) {
    this.target = target;

    target.typeImage = Utils.findRequiredViewAsType(source, R.id.typeImage, "field 'typeImage'", ImageView.class);
    target.typeText = Utils.findRequiredViewAsType(source, R.id.typeText, "field 'typeText'", TextView.class);
    target.addressText = Utils.findRequiredViewAsType(source, R.id.addressText, "field 'addressText'", TextView.class);
    target.deleteAddress = Utils.findRequiredViewAsType(source, R.id.deleteAddress, "field 'deleteAddress'", ImageView.class);
    target.editButton = Utils.findRequiredViewAsType(source, R.id.editButton, "field 'editButton'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ManageAddressAdapter.Addresses target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.typeImage = null;
    target.typeText = null;
    target.addressText = null;
    target.deleteAddress = null;
    target.editButton = null;
  }
}
