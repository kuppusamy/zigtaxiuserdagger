// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;
import me.relex.circleindicator.CircleIndicator;

public class WalkThroughActivity_ViewBinding implements Unbinder {
  private WalkThroughActivity target;

  @UiThread
  public WalkThroughActivity_ViewBinding(WalkThroughActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WalkThroughActivity_ViewBinding(WalkThroughActivity target, View source) {
    this.target = target;

    target.viewPagerContainer = Utils.findRequiredViewAsType(source, R.id.viewPagerContainer, "field 'viewPagerContainer'", ViewPager.class);
    target.goNextButton = Utils.findRequiredViewAsType(source, R.id.goNextButton, "field 'goNextButton'", ImageView.class);
    target.indicator = Utils.findRequiredViewAsType(source, R.id.indicator, "field 'indicator'", CircleIndicator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WalkThroughActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewPagerContainer = null;
    target.goNextButton = null;
    target.indicator = null;
  }
}
