// Generated code from Butter Knife. Do not modify!
package com.app.foodappuser.view.activities;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.foodappuser.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ManageAddressActivity_ViewBinding implements Unbinder {
  private ManageAddressActivity target;

  @UiThread
  public ManageAddressActivity_ViewBinding(ManageAddressActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ManageAddressActivity_ViewBinding(ManageAddressActivity target, View source) {
    this.target = target;

    target.backButton = Utils.findRequiredViewAsType(source, R.id.backButton, "field 'backButton'", ImageView.class);
    target.savedAddressRecycler = Utils.findRequiredViewAsType(source, R.id.savedAddressRecycler, "field 'savedAddressRecycler'", RecyclerView.class);
    target.addNewAddressButton = Utils.findRequiredViewAsType(source, R.id.addNewAddressButton, "field 'addNewAddressButton'", CardView.class);
    target.connectionLostLayout = Utils.findRequiredView(source, R.id.connectionLostLayout, "field 'connectionLostLayout'");
    target.contentLayout = Utils.findRequiredView(source, R.id.contentLayout, "field 'contentLayout'");
    target.loadingLayout = Utils.findRequiredView(source, R.id.loadingLayout, "field 'loadingLayout'");
  }

  @Override
  @CallSuper
  public void unbind() {
    ManageAddressActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backButton = null;
    target.savedAddressRecycler = null;
    target.addNewAddressButton = null;
    target.connectionLostLayout = null;
    target.contentLayout = null;
    target.loadingLayout = null;
  }
}
