package com.app.chat.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import com.app.chat.socket.EmitSocket;
import com.app.foodappdriver.Utilities.Constant;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.client.Url;
import io.socket.emitter.Emitter;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u0000  2\u00020\u0001:\u0001 B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\b\u0010\u0017\u001a\u00020\u0018H\u0016J \u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u001aH\u0016J\b\u0010\u001d\u001a\u00020\u0018H\u0002J\b\u0010\u001e\u001a\u00020\u0018H\u0002J\b\u0010\u001f\u001a\u00020\u0018H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0003\u0010\u0005\"\u0004\b\u0006\u0010\u0007R\u001a\u0010\b\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u001a\u0010\u0010\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u000b\"\u0004\b\u0012\u0010\r\u00a8\u0006!"}, d2 = {"Lcom/app/chat/service/ServiceClass;", "Landroid/app/Service;", "()V", "isConnect", "", "()Z", "setConnect", "(Z)V", "onConnect", "Lio/socket/emitter/Emitter$Listener;", "getOnConnect", "()Lio/socket/emitter/Emitter$Listener;", "setOnConnect", "(Lio/socket/emitter/Emitter$Listener;)V", "onConnectError", "getOnConnectError", "onDisconnect", "getOnDisconnect", "setOnDisconnect", "onBind", "Landroid/os/IBinder;", "intent", "Landroid/content/Intent;", "onDestroy", "", "onStartCommand", "", "flags", "startId", "socketConnection", "socketDisConnection", "socketInitialization", "Companion", "food_debug"})
public final class ServiceClass extends android.app.Service {
    private boolean isConnect;
    @org.jetbrains.annotations.NotNull()
    private final io.socket.emitter.Emitter.Listener onConnectError = null;
    @org.jetbrains.annotations.NotNull()
    private io.socket.emitter.Emitter.Listener onConnect;
    @org.jetbrains.annotations.NotNull()
    private io.socket.emitter.Emitter.Listener onDisconnect;
    private static io.socket.client.Socket socket;
    public static final com.app.chat.service.ServiceClass.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.os.IBinder onBind(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
        return null;
    }
    
    @java.lang.Override()
    public int onStartCommand(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent, int flags, int startId) {
        return 0;
    }
    
    private final void socketInitialization() {
    }
    
    private final void socketConnection() {
    }
    
    private final void socketDisConnection() {
    }
    
    public final boolean isConnect() {
        return false;
    }
    
    public final void setConnect(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.socket.emitter.Emitter.Listener getOnConnectError() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.socket.emitter.Emitter.Listener getOnConnect() {
        return null;
    }
    
    public final void setOnConnect(@org.jetbrains.annotations.NotNull()
    io.socket.emitter.Emitter.Listener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.socket.emitter.Emitter.Listener getOnDisconnect() {
        return null;
    }
    
    public final void setOnDisconnect(@org.jetbrains.annotations.NotNull()
    io.socket.emitter.Emitter.Listener p0) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public ServiceClass() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/app/chat/service/ServiceClass$Companion;", "", "()V", "socket", "Lio/socket/client/Socket;", "getSocketInstance", "food_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final io.socket.client.Socket getSocketInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}