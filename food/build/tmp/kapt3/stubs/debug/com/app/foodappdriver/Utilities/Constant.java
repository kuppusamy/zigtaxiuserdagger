package com.app.foodappdriver.Utilities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/app/foodappdriver/Utilities/Constant;", "", "()V", "Companion", "food_debug"})
public final class Constant {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ONLINE = "userOnline";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SEND_MESSAGE = "sendMessage";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UPDATE_UNREAD_COUNT = "updateUnreadCount";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RECEIVE_MESSAGE = "receiveTracking";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RECEIVE_PROVIDER_LOCATION = "providerLocation";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RECEIVE_BROADCAST_MESSAGE = "receiveMessageBroadcast";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ID = "id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_ID = "id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHATROOM_ID = "chatroomId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TEXT = "text";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMAGE = "image";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VIDEO = "video";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DOCUMENT = "document";
    public static final com.app.foodappdriver.Utilities.Constant.Companion Companion = null;
    
    public Constant() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\r\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/app/foodappdriver/Utilities/Constant$Companion;", "", "()V", "CHATROOM_ID", "", "DOCUMENT", "ID", "IMAGE", "ONLINE", "RECEIVE_BROADCAST_MESSAGE", "RECEIVE_MESSAGE", "RECEIVE_PROVIDER_LOCATION", "SEND_MESSAGE", "TEXT", "UPDATE_UNREAD_COUNT", "USER_ID", "VIDEO", "food_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}