package com.app.chat.socket;

import com.app.foodappdriver.Utilities.ChatListener;
import com.app.foodappuser.Model.Response.LiveTrackingSocketResponseModel;
import com.example.chat.model.ChatObserver;
import com.google.gson.Gson;
import io.socket.emitter.Emitter;
import org.json.JSONObject;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0004R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000eX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0013\u001a\u00020\u000eX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0010\"\u0004\b\u0015\u0010\u0012R\u001a\u0010\u0016\u001a\u00020\u000eX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0010\"\u0004\b\u0018\u0010\u0012\u00a8\u0006\u001c"}, d2 = {"Lcom/app/chat/socket/ListenSocket;", "", "()V", "chatListener", "Lcom/app/foodappdriver/Utilities/ChatListener;", "getChatListener", "()Lcom/app/foodappdriver/Utilities/ChatListener;", "setChatListener", "(Lcom/app/foodappdriver/Utilities/ChatListener;)V", "gSon", "Lcom/google/gson/Gson;", "getGSon", "()Lcom/google/gson/Gson;", "onReceive", "Lio/socket/emitter/Emitter$Listener;", "getOnReceive$food_debug", "()Lio/socket/emitter/Emitter$Listener;", "setOnReceive$food_debug", "(Lio/socket/emitter/Emitter$Listener;)V", "providerReceive", "getProviderReceive$food_debug", "setProviderReceive$food_debug", "receiveMessageBroadcast", "getReceiveMessageBroadcast$food_debug", "setReceiveMessageBroadcast$food_debug", "onReceiveMsgCallBack", "", "listener", "food_debug"})
public final class ListenSocket {
    @org.jetbrains.annotations.NotNull()
    private static final com.google.gson.Gson gSon = null;
    @org.jetbrains.annotations.Nullable()
    private static com.app.foodappdriver.Utilities.ChatListener chatListener;
    @org.jetbrains.annotations.NotNull()
    private static io.socket.emitter.Emitter.Listener onReceive;
    @org.jetbrains.annotations.NotNull()
    private static io.socket.emitter.Emitter.Listener providerReceive;
    @org.jetbrains.annotations.NotNull()
    private static io.socket.emitter.Emitter.Listener receiveMessageBroadcast;
    public static final com.app.chat.socket.ListenSocket INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.gson.Gson getGSon() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.app.foodappdriver.Utilities.ChatListener getChatListener() {
        return null;
    }
    
    public final void setChatListener(@org.jetbrains.annotations.Nullable()
    com.app.foodappdriver.Utilities.ChatListener p0) {
    }
    
    public final void onReceiveMsgCallBack(@org.jetbrains.annotations.NotNull()
    com.app.foodappdriver.Utilities.ChatListener listener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.socket.emitter.Emitter.Listener getOnReceive$food_debug() {
        return null;
    }
    
    public final void setOnReceive$food_debug(@org.jetbrains.annotations.NotNull()
    io.socket.emitter.Emitter.Listener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.socket.emitter.Emitter.Listener getProviderReceive$food_debug() {
        return null;
    }
    
    public final void setProviderReceive$food_debug(@org.jetbrains.annotations.NotNull()
    io.socket.emitter.Emitter.Listener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.socket.emitter.Emitter.Listener getReceiveMessageBroadcast$food_debug() {
        return null;
    }
    
    public final void setReceiveMessageBroadcast$food_debug(@org.jetbrains.annotations.NotNull()
    io.socket.emitter.Emitter.Listener p0) {
    }
    
    private ListenSocket() {
        super();
    }
}