package com.app.chat.socket;

import com.app.foodappdriver.Utilities.Constant;
import org.json.JSONObject;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0012\u001a\u00020\u0013J\u0006\u0010\u0014\u001a\u00020\u0013J\u0006\u0010\u0015\u001a\u00020\u0013R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001c\u0010\f\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001a\u0010\u000f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\b\u00a8\u0006\u0016"}, d2 = {"Lcom/app/chat/socket/SocketParams;", "", "()V", "toChatRoomId", "", "getToChatRoomId", "()Ljava/lang/String;", "setToChatRoomId", "(Ljava/lang/String;)V", "toUserId", "getToUserId", "setToUserId", "toUserName", "getToUserName", "setToUserName", "userId", "getUserId", "setUserId", "sendMessageJson", "Lorg/json/JSONObject;", "tokenJson", "updateUnreadCountJSON", "food_debug"})
public final class SocketParams {
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String userId;
    @org.jetbrains.annotations.Nullable()
    private static java.lang.String toUserName;
    @org.jetbrains.annotations.Nullable()
    private static java.lang.String toUserId;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String toChatRoomId;
    public static final com.app.chat.socket.SocketParams INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUserId() {
        return null;
    }
    
    public final void setUserId(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.json.JSONObject tokenJson() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getToUserName() {
        return null;
    }
    
    public final void setToUserName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getToUserId() {
        return null;
    }
    
    public final void setToUserId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.json.JSONObject sendMessageJson() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getToChatRoomId() {
        return null;
    }
    
    public final void setToChatRoomId(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.json.JSONObject updateUnreadCountJSON() {
        return null;
    }
    
    private SocketParams() {
        super();
    }
}