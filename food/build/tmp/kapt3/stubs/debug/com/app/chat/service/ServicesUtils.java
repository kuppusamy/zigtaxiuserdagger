package com.app.chat.service;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.util.Log;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/app/chat/service/ServicesUtils;", "", "()V", "Companion", "food_debug"})
public final class ServicesUtils {
    public static final com.app.chat.service.ServicesUtils.Companion Companion = null;
    
    public ServicesUtils() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0012\u0010\u0007\u001a\u00020\b2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007J\u001a\u0010\t\u001a\u00020\b2\n\u0010\n\u001a\u0006\u0012\u0002\b\u00030\u000b2\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\r"}, d2 = {"Lcom/app/chat/service/ServicesUtils$Companion;", "", "()V", "initChatService", "", "context", "Landroid/content/Context;", "isAppIsInBackground", "", "isMyServiceRunning", "serviceClass", "Ljava/lang/Class;", "stopChatService", "food_debug"})
    public static final class Companion {
        
        public final void initChatService(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        public final void stopChatService(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        public final boolean isMyServiceRunning(@org.jetbrains.annotations.NotNull()
        java.lang.Class<?> serviceClass, @org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return false;
        }
        
        @android.annotation.SuppressLint(value = {"NewApi"})
        public final boolean isAppIsInBackground(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return false;
        }
        
        private Companion() {
            super();
        }
    }
}