package com.app.foodappuser.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.app.foodappuser.NavigationInterFaceFood;
import com.app.foodappuser.R;
import dagger.android.support.DaggerAppCompatActivity;
import kotlinx.android.synthetic.main.activity_backtomain.*;
import kotlinx.android.synthetic.main.activity_sign_in.*;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0014R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u000e"}, d2 = {"Lcom/app/foodappuser/view/activities/EmptyFoodLayout;", "Ldagger/android/support/DaggerAppCompatActivity;", "()V", "navigationInterFaceFood", "Lcom/app/foodappuser/NavigationInterFaceFood;", "getNavigationInterFaceFood", "()Lcom/app/foodappuser/NavigationInterFaceFood;", "setNavigationInterFaceFood", "(Lcom/app/foodappuser/NavigationInterFaceFood;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "food_debug"})
public final class EmptyFoodLayout extends dagger.android.support.DaggerAppCompatActivity {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.app.foodappuser.NavigationInterFaceFood navigationInterFaceFood;
    public static final com.app.foodappuser.view.activities.EmptyFoodLayout.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.app.foodappuser.NavigationInterFaceFood getNavigationInterFaceFood() {
        return null;
    }
    
    public final void setNavigationInterFaceFood(@org.jetbrains.annotations.NotNull()
    com.app.foodappuser.NavigationInterFaceFood p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public EmptyFoodLayout() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/app/foodappuser/view/activities/EmptyFoodLayout$Companion;", "", "()V", "getIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "food_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Intent getIntent(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}