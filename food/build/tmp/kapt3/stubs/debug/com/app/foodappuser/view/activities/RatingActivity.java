package com.app.foodappuser.view.activities;

import android.app.Activity;
import android.os.Bundle;
import androidx.lifecycle.ViewModelProviders;
import com.app.foodappuser.Model.Response.ListRestaurantResponse.Ratings;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.ViewModel.RatingViewModel;
import kotlinx.android.synthetic.main.activity_rating.*;
import org.json.JSONObject;
import android.widget.RatingBar.OnRatingBarChangeListener;
import androidx.lifecycle.Observer;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.UrlHelper;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0014J\b\u0010\u000b\u001a\u00020\bH\u0002J\b\u0010\f\u001a\u00020\bH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/app/foodappuser/view/activities/RatingActivity;", "Lcom/app/foodappuser/view/activities/BaseActivity;", "()V", "rating", "Lcom/app/foodappuser/Model/Response/ListRestaurantResponse/Ratings;", "ratingViewModel", "Lcom/app/foodappuser/ViewModel/RatingViewModel;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "setListeners", "setRatingListeners", "food_debug"})
public final class RatingActivity extends com.app.foodappuser.view.activities.BaseActivity {
    private com.app.foodappuser.ViewModel.RatingViewModel ratingViewModel;
    private com.app.foodappuser.Model.Response.ListRestaurantResponse.Ratings rating;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setRatingListeners() {
    }
    
    private final void setListeners() {
    }
    
    public RatingActivity() {
        super();
    }
}