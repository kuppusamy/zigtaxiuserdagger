package com.app.chat.socket;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.app.foodappdriver.Utilities.Constant;
import com.example.chat.model.ChatObserver;
import io.socket.client.Ack;
import org.json.JSONObject;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000e\u001a\u00020\u000fJ\u0006\u0010\u0010\u001a\u00020\u000fJ\u0006\u0010\u0011\u001a\u00020\u000fR\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/app/chat/socket/EmitSocket;", "", "()V", "contextEmitSocket", "Landroid/content/Context;", "getContextEmitSocket", "()Landroid/content/Context;", "setContextEmitSocket", "(Landroid/content/Context;)V", "statusCheck", "Ljava/lang/Runnable;", "updatedOff", "", "updatedOn", "emitOnline", "", "sendMessage", "updateUnreadCount", "food_debug"})
public final class EmitSocket {
    private static boolean updatedOn;
    private static boolean updatedOff;
    @org.jetbrains.annotations.Nullable()
    private static android.content.Context contextEmitSocket;
    private static final java.lang.Runnable statusCheck = null;
    public static final com.app.chat.socket.EmitSocket INSTANCE = null;
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.Context getContextEmitSocket() {
        return null;
    }
    
    public final void setContextEmitSocket(@org.jetbrains.annotations.Nullable()
    android.content.Context p0) {
    }
    
    public final void emitOnline() {
    }
    
    public final void sendMessage() {
    }
    
    public final void updateUnreadCount() {
    }
    
    private EmitSocket() {
        super();
    }
}