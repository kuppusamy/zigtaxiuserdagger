package com.example.chat.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u0000 \u000e2\u00020\u0001:\u0002\u000e\u000fB\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0006J\u0006\u0010\n\u001a\u00020\u0006J\b\u0010\u000b\u001a\u00020\bH\u0002J\u000e\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/example/chat/model/ChatObserver;", "", "()V", "mListener", "Lcom/example/chat/model/ChatObserver$OnCustomStateListener;", "mState", "", "changeState", "", "state", "getState", "notifyStateChange", "setListener", "listener", "Companion", "OnCustomStateListener", "food_debug"})
public final class ChatObserver {
    private com.example.chat.model.ChatObserver.OnCustomStateListener mListener;
    private boolean mState;
    private static com.example.chat.model.ChatObserver mInstance;
    public static final com.example.chat.model.ChatObserver.Companion Companion = null;
    
    public final void setListener(@org.jetbrains.annotations.NotNull()
    com.example.chat.model.ChatObserver.OnCustomStateListener listener) {
    }
    
    public final void changeState(boolean state) {
    }
    
    public final boolean getState() {
        return false;
    }
    
    private final void notifyStateChange() {
    }
    
    public ChatObserver() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/example/chat/model/ChatObserver$OnCustomStateListener;", "", "stateChanged", "", "food_debug"})
    public static abstract interface OnCustomStateListener {
        
        public abstract void stateChanged();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/example/chat/model/ChatObserver$Companion;", "", "()V", "mInstance", "Lcom/example/chat/model/ChatObserver;", "getInstance", "food_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.example.chat.model.ChatObserver getInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}