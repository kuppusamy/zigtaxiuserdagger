package com.app.foodappuser.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.app.foodappuser.Entity.CartDetails;
import com.app.foodappuser.Entity.DishCustomizationItems;
import com.app.foodappuser.Entity.DishElementItems;
import com.app.foodappuser.Entity.DishItemTable;

@Dao
public interface CartDao {

    @Query("Select * FROM DishItemTable WHERE dish_id = :dishId")
    DishItemTable[] getDishItemId(String dishId);

    @Query("Select * FROM DishItemTable WHERE dish_id = :dishId")
    DishItemTable getDishItemQuantity(String dishId);

    @Query("Select * FROM DishItemTable")
    DishItemTable[] getAllDishItems();

    @Insert
    long[] insertDishItem(DishItemTable... dishItemTables);

    @Insert
    long[] insertDishCustomizationItems(DishCustomizationItems... dishCustomizationItems);

    @Insert
    long[] insertDishElementCustomizationItems(DishElementItems... dishElementItems);

    @Query("Update DishItemTable SET quantity = :quantity, total_price=:price, display_price=:displayPrice WHERE dish_id=:dishId")
    void updateDishItem(double quantity,String price,String displayPrice,String dishId);

    @Query("Update DishItemTable SET quantity = :quantity, total_price=:price, display_price=:displayPrice WHERE id=:id")
    void updateDishItems(double quantity,String price,String displayPrice,int id);

    @Query("Select * FROM CartDetails")
    CartDetails[] getOutlet();

    @Query("Select * FROM CartDetails")
    CartDetails getCartDetails();

    @Query("Select * FROM CartDetails WHERE outlet_id = :outletId")
    CartDetails checkOutlet(String outletId);

    @Insert
    long[] insertOutletId(CartDetails... cartDetails);

    @Query("Update CartDetails SET total_items=:totalItems, total_amount=:totalAmount WHERE outlet_id=:outletId")
    void updateCartDetails(String totalAmount,String totalItems,String outletId);

    @Query("Delete FROM DishItemTable WHERE dish_id=:dishId")
    void deleteDishItem(String dishId);

    @Query("Delete FROM DishItemTable")
    void deleteDishItemTable();

    @Query("Delete FROM CartDetails")
    void deleteCartDetailsTable();

    @Query("Delete FROM CategoryItems")
    void deleteCategoryItemsTable();

    @Query("Delete FROM DishCustomizationItems")
    void deleteDishCustomizationItemsTable();

    @Query("Delete FROM DishElementItems")
    void deleteDishElementItemsTable();

    @Query("Select * FROM DishCustomizationItems WHERE dish_items_id = :dishId")
    DishCustomizationItems[] getCustomizationItems(int dishId);

    @Query("Select * FROM DishElementItems WHERE dish_customization_items_id = :id")
    DishElementItems[] getDishElements(int id);

    @Query("Update DishItemTable SET quantity=:quantity WHERE id = :id")
    void updateDish(String id,int quantity);

    @Query("Update DishItemTable SET total_price=:total WHERE id = :id")
    void updateTotalAmount(double total,String id);

    @Query("Select * FROM DishItemTable WHERE id = :id")
        DishItemTable[] getDishItem(String id);

    @Query("Delete FROM DishItemTable WHERE id=:id")
    void deleteDish(String id);




}
