package com.app.foodappuser.Database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import com.app.foodappuser.DAO.CartDao;
import com.app.foodappuser.Entity.CartDetails;
import com.app.foodappuser.Entity.CategoryItems;
import com.app.foodappuser.Entity.DishCustomizationItems;
import com.app.foodappuser.Entity.DishElementItems;
import com.app.foodappuser.Entity.DishItemTable;

@Database(entities = {CategoryItems.class, DishCustomizationItems.class, DishElementItems.class, DishItemTable.class, CartDetails.class}, version = 1, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase{


    private static AppDataBase instance;

    public abstract CartDao cartDao();

    public static AppDataBase getAppDatabase(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDataBase.class,
                    "foodapp-cart-db")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }


}
