package com.app.foodappuser.Application;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import androidx.multidex.MultiDex;

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
//import com.crashlytics.android.Crashlytics;
import com.app.foodappuser.Utilities.RxBus;
import com.google.firebase.FirebaseApp;
import com.app.foodappuser.R;

//import io.fabric.sdk.android.Fabric;
//import io.fabric.sdk.android.Fabric;
import net.danlew.android.joda.JodaTimeAndroid;


//import com.crashlytics.android.Crashlytics;

/**
 * Created by yuvaraj on 25/04/18.
 */

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();
    private static Context mContext;
    private static AppController mInstance;
    private RequestQueue mRequestQueue;

    private RxBus rxBus;

    public RxBus getRxBus() {
        return rxBus;
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static Context getContext() {
        return mContext;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        mContext = this;
        mInstance = this;
        rxBus=new RxBus();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);

//        Fabric.with(this, new Crashlytics());

        FirebaseApp.initializeApp(getContext());

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        rxBus=new RxBus();
    }


}
