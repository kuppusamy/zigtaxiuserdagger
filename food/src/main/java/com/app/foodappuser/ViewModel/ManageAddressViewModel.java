package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.DeleteAddressResponseModel;
import com.app.foodappuser.Model.Response.GetSavedAddressResponse.GetSavedAddressResponseModel;
import com.app.foodappuser.Repository.ManageAddressRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class ManageAddressViewModel extends AndroidViewModel {

    ManageAddressRepository manageAddressRepository;

    public ManageAddressViewModel(@NonNull Application application) {
        super(application);
        manageAddressRepository=new ManageAddressRepository();
    }

    public LiveData<GetSavedAddressResponseModel> getSavedAddressResponseModelLiveData(InputForAPI input){
        return manageAddressRepository.getSavedAddressData(input);
    }

    public LiveData<DeleteAddressResponseModel> deleteAddressResponseModelLiveData(InputForAPI input){
        return manageAddressRepository.deleteAddressResponseModelLiveData(input);
    }

}
