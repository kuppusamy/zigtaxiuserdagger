package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Database.AppDataBase;
import com.app.foodappuser.Model.Response.EditProfileResponseModel;
import com.app.foodappuser.Model.Response.FaqResponseModel;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Model.Response.PastOrderResponse.PastOrderResponseModel;
import com.app.foodappuser.Repository.AccountRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class AccountViewModel extends AndroidViewModel {

    AccountRepository accountRepository;
    AppDataBase database;

    public AccountViewModel(@NonNull Application application) {
        super(application);
        accountRepository=new AccountRepository();
        database = AppDataBase.getAppDatabase(getApplication().getBaseContext());

    }

    public LiveData<EditProfileResponseModel> updateProfile(InputForAPI input){
        return accountRepository.updateProfile(input);
    }

    public LiveData<FaqResponseModel> getLinks(InputForAPI inputForAPI) {
        return accountRepository.getFaqResponseModel(inputForAPI);
    }

    public LiveData<PastOrderResponseModel> getPastOrders(InputForAPI inputForAPI) {
        return accountRepository.getPastOrders(inputForAPI);
    }
    public LiveData<GeneralResponseModel> generalResponseModelLiveData(InputForAPI inputForAPI) {
        return accountRepository.generalResponseModelLiveData(inputForAPI);
    }

    public void deleteAllTableValues() {
        database.cartDao().deleteCartDetailsTable();
        database.cartDao().deleteCategoryItemsTable();
        database.cartDao().deleteDishCustomizationItemsTable();
        database.cartDao().deleteDishElementItemsTable();
        database.cartDao().deleteDishItemTable();
    }

}
