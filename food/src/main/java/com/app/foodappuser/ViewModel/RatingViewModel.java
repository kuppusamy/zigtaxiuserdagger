package com.app.foodappuser.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Repository.RatingRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class RatingViewModel extends AndroidViewModel {

    RatingRepository ratingRepository;

    public RatingViewModel(@NonNull Application application) {
        super(application);
        ratingRepository =new RatingRepository();

    }

    public LiveData<GeneralResponseModel> GeneralResponseModel(InputForAPI input){
        return ratingRepository.generalResponseModelLiveData(input);
    }




}
