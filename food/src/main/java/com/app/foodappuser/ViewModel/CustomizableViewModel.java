package com.app.foodappuser.ViewModel;

import android.content.Context;

import com.app.foodappuser.Model.Custom.Custom;
import com.app.foodappuser.Model.Custom.CustomizationModel;
import com.app.foodappuser.Model.Response.ListDishesResponse.CustomizableDishItem;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CustomizableViewModel {

    Context context;
    List<Custom> customList=new ArrayList<>();
    public CustomizableViewModel(Context context) {
        this.context=context;
    }

    public void selectSingleCustomization(int position, List<CustomizableDishItem> customizableDishItemList) {
        Gson gson = new Gson();
        JSONObject jsonObject=new JSONObject();
        for(int i=0;i<customizableDishItemList.size();i++){
            if(position==i){
//                customizableDishItemList.get(i).setIsSelected(1);
                try {
                    jsonObject.put("customizableId",customizableDishItemList.get(i).getElementId());
                    jsonObject.put("customizableName",customizableDishItemList.get(i).getElementName());
//                    customList.add(new Custom(customizableDishItemList.get(i).getElementId(),customizableDishItemList.get(i).getElementName()));

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }else{
//                customizableDishItemList.get(i).setIsSelected(0);
            }
        }



        JSONObject jsonObject1=new JSONObject();
        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                JSONArray jsonArray=new JSONArray();
                jsonArray.put(jsonObject);
                jsonObject1.put("customizations",jsonArray);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utils.log("print", jsonObject1.toString());

        CustomizationModel customizationModel = gson.fromJson(jsonObject1.toString(),CustomizationModel.class);



        for(int j=0;j<customList.size();j++){
            Utils.log("print",customList.get(j).getName());
        }
    }
}
