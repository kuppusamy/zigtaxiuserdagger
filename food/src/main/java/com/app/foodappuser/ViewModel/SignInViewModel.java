package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.content.Context;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.CheckUserAvailabilityResponseModel;
import com.app.foodappuser.Model.Response.SignInResponseModelResponse.SignInResponseModel;
import com.app.foodappuser.Model.Response.SocialLoginResponse.FacebookLoginModel;
import com.app.foodappuser.Model.Response.SocialLoginResponse.SocialLoginModel;
import com.app.foodappuser.Repository.SignInRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.facebook.login.LoginResult;

public class SignInViewModel extends AndroidViewModel {

    SignInRepository signInRepository;
    Context context;



    public SignInViewModel(@NonNull Application application) {
        super(application);
        signInRepository = new SignInRepository();

    }

    public LiveData<CheckUserAvailabilityResponseModel> checkMobileNumber(InputForAPI inputs) {
        return  signInRepository.getCheckNumberResponse(inputs);
    }

    public LiveData<SignInResponseModel> login(InputForAPI inputs) {
        return  signInRepository.login(inputs);
    }
    public LiveData<SocialLoginModel> socialLoginModelLiveData(InputForAPI inputs) {
        return  signInRepository.socialLoginModelLiveData(inputs);
    }
    public LiveData<FacebookLoginModel> facebookData(LoginResult inputs) {
        return  signInRepository.facebookData(inputs);
    }



    public void storeValues() {

    }
}
