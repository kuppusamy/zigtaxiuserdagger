package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.content.Context;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.SignUpResponseModel;
import com.app.foodappuser.Repository.SignUpRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class SignUpViewModel extends AndroidViewModel{

    SignUpRepository signUpRepository;
    Context context;



    public SignUpViewModel(@NonNull Application application) {
        super(application);
        signUpRepository = new SignUpRepository();

    }

    public LiveData<SignUpResponseModel> signUp(InputForAPI inputs){
        return signUpRepository.getSignUpResponse(inputs);
    }



}
