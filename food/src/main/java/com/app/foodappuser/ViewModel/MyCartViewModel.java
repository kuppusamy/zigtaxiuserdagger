package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Database.AppDataBase;
import com.app.foodappuser.Entity.CartDetails;
import com.app.foodappuser.Entity.DishCustomizationItems;
import com.app.foodappuser.Entity.DishElementItems;
import com.app.foodappuser.Entity.DishItemTable;
import com.app.foodappuser.Model.Response.CouponResponseModel;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Model.Response.ListDeliveryResponse.ListDeliveryResponseModel;
import com.app.foodappuser.Model.Response.ViewCartResponse.ViewCartResponseModel;
import com.app.foodappuser.Repository.MyCartRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyCartViewModel extends AndroidViewModel {

    private static final String TAG = MyCartViewModel.class.getSimpleName();
    MyCartRepository myCartRepository;
    AppDataBase appDataBase;

    public MyCartViewModel(@NonNull Application application) {
        super(application);
        myCartRepository=new MyCartRepository();
        appDataBase = AppDataBase.getAppDatabase(getApplication().getBaseContext());

    }

    public LiveData<GeneralResponseModel> addtoCart(InputForAPI inputs) {
        return  myCartRepository.addToCart(inputs);
    }
    public LiveData<GeneralResponseModel> generalResponseModelLiveData(InputForAPI inputs) {
        return  myCartRepository.addToCart(inputs);
    }

    public LiveData<GeneralResponseModel> updateCart(InputForAPI inputs) {
        return  myCartRepository.updateCart(inputs);
    }
    public LiveData<CouponResponseModel> addCoupon(InputForAPI inputs) {
        return  myCartRepository.getAddCouponResponse(inputs);
    }

    public LiveData<ViewCartResponseModel> viewCart(InputForAPI inputs) {
        return  myCartRepository.viewCart(inputs);
    }
    public LiveData<ListDeliveryResponseModel> getDeliveryAddress(InputForAPI inputs) {
        return  myCartRepository.getDeliveryAddress(inputs);
    }

    public JSONArray getData() throws JSONException {
        JSONArray jsonArray=new JSONArray();

        DishItemTable[] dishItemTables=appDataBase.cartDao().getAllDishItems();

        for (int i = 0; i <dishItemTables.length ; i++) {

            JSONObject jsonObject=new JSONObject();

            if(dishItemTables[i].isCustomizable==0){
                jsonObject.put("dishId",dishItemTables[i].dishId);
                jsonObject.put("uuId",dishItemTables[i].id);
                jsonObject.put("quantity",dishItemTables[i].quantity);
                jsonObject.put("customisation",new JSONArray());
            }else{
                jsonObject.put("dishId", Integer.parseInt(dishItemTables[i].dishId));
                jsonObject.put("uuId",dishItemTables[i].id);
                jsonObject.put("quantity",dishItemTables[i].quantity);
                JSONArray jsonArray1=new JSONArray();
                int id=dishItemTables[i].id;
                Utils.log(TAG,"id - "+String.valueOf(id));
                DishCustomizationItems[] dishCustomizationItems=appDataBase.cartDao().getCustomizationItems(id);
                Utils.log(TAG, "Length = "+String.valueOf(dishCustomizationItems.length));
                for (int j = 0; j <dishCustomizationItems.length ; j++) {
                    DishElementItems[] dishElementItems=appDataBase.cartDao().getDishElements(dishCustomizationItems[j].id);
                    Utils.log(TAG,"dishCustomizationItems ID - "+String.valueOf(dishCustomizationItems[j].id));
                    Utils.log(TAG,"dishCustomizationItems LENGTH - "+String.valueOf(dishElementItems.length));
                    for (int k = 0; k <dishElementItems.length ; k++) {
                        JSONObject jsonObject1=new JSONObject();
                        jsonObject1.put("dishCustomisationId",dishElementItems[k].elementId);
                        jsonArray1.put(jsonObject1);
                    }
                }
                jsonObject.put("customisation",jsonArray1);
            }

            jsonArray.put(jsonObject);
        }

        return jsonArray;
    }

    public String getOutletId() {

        CartDetails[] cartDetails=appDataBase.cartDao().getOutlet();

        if(cartDetails!=null){
            if(cartDetails.length!=0){
                return cartDetails[0].outletId;
            }else{
                return "";
            }
        }else{
            return "";
        }

    }

    public Boolean checkIsAvailable() {

        CartDetails[] cartDetails=appDataBase.cartDao().getOutlet();

        if(cartDetails!=null){
            if(cartDetails.length!=0)
            return true;
            else
                return false;
        }else{
            return false;
        }

    }

    public void updateDb(int quantity, String id) {

        if(quantity<1){

            appDataBase.cartDao().deleteDish(id);

        }else{
            appDataBase.cartDao().updateDish(id,quantity);

            DishItemTable[] dishItemTable=appDataBase.cartDao().getDishItem(id);
            double price = 0;

            for (int i = 0; i <dishItemTable.length ; i++) {

                price= (dishItemTable[i].price);
            }

            appDataBase.cartDao().updateTotalAmount(price*quantity,id);
        }



    }

    public void updateCartItems(String outletId) {

        DishItemTable[] dishItemTables = appDataBase.cartDao().getAllDishItems();
        double totalPrice = 0;
        double totalCount = 0;
        for (int i = 0; i < dishItemTables.length; i++) {
            totalPrice = totalPrice + (dishItemTables[i].totalPrice);
            totalCount = totalCount + (dishItemTables[i].quantity);
        }

        appDataBase.cartDao().updateCartDetails(String.valueOf(totalPrice), String.valueOf(totalCount), outletId);

    }

    public void deleteAllTableValues() {
        appDataBase.cartDao().deleteCartDetailsTable();
        appDataBase.cartDao().deleteCategoryItemsTable();
        appDataBase.cartDao().deleteDishCustomizationItemsTable();
        appDataBase.cartDao().deleteDishElementItemsTable();
        appDataBase.cartDao().deleteDishItemTable();
    }

}
