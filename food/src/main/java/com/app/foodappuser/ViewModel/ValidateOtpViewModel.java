package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.ResendOtpResponseModel;
import com.app.foodappuser.Model.Response.ValidateOtpResponseModel;
import com.app.foodappuser.Repository.ValidateOtpRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class ValidateOtpViewModel extends AndroidViewModel {
    ValidateOtpRepository validateOtpRepository;

    public ValidateOtpViewModel(@NonNull Application application) {
        super(application);
        validateOtpRepository=new ValidateOtpRepository();
    }

    public LiveData<ValidateOtpResponseModel> validateOtp(InputForAPI input){
        return validateOtpRepository.validateOtpResponseModelLiveData(input);
    }

    public LiveData<ResendOtpResponseModel> resendOtp(InputForAPI input){
        return validateOtpRepository.resendOtpResponseModelLiveData(input);
    }
}
