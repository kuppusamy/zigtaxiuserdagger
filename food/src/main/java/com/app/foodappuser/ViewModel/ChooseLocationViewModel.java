package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.AutoCompleteResponse.AutoCompleteAddressResponseModel;
import com.app.foodappuser.Model.Response.ChooseLocationResponseModel;
import com.app.foodappuser.Model.Response.GetLatLongFromIdResponse.GetLatLongFromIdResponseModel;
import com.app.foodappuser.Model.Response.GetSavedAddressResponse.GetSavedAddressResponseModel;
import com.app.foodappuser.Model.Response.SetCurrentAddressResponseModel;
import com.app.foodappuser.Repository.ChooseLocationRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

import java.util.StringTokenizer;

public class ChooseLocationViewModel extends AndroidViewModel{
    ChooseLocationRepository chooseLocationRepository;


    public ChooseLocationViewModel(@NonNull Application application) {
        super(application);
        chooseLocationRepository = new ChooseLocationRepository();
    }

    public LiveData<AutoCompleteAddressResponseModel> getLocationDetails(InputForAPI input){
        return chooseLocationRepository.getChooseLocation(input);
    }

    public LiveData<ChooseLocationResponseModel> getCurrentAddress(InputForAPI input){
        return chooseLocationRepository.getCurrentAddress(input);
    }

    public LiveData<GetLatLongFromIdResponseModel> getLatitudeAndLongitudeFromId(InputForAPI input){
        return chooseLocationRepository.getlatitudeAndLogitudeFromId(input);
    }

    public LiveData<GetSavedAddressResponseModel> getSavedAddressResponseModelLiveData(InputForAPI input){
        return chooseLocationRepository.getSavedAddressData(input);
    }

    public LiveData<SetCurrentAddressResponseModel> setCurrentAddress(InputForAPI input){
        return chooseLocationRepository.setCurrentAddress(input);
    }









    public String getPlacesText(String placeDesc) {
        StringTokenizer st = new StringTokenizer(placeDesc, ",");
        String desc_detail = "";
        for (int i = 1; i < st.countTokens(); i++) {
            if (i == st.countTokens() - 1) {
                desc_detail = desc_detail + st.nextToken();
            } else {
                desc_detail = desc_detail + st.nextToken() + ",";
            }
        }
        return desc_detail;
    }




    public String getAreaName(String placeDesc) {
        StringTokenizer st = new StringTokenizer(placeDesc, ",");
        return st.nextToken();
    }
}
