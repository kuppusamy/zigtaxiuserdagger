package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Database.AppDataBase;
import com.app.foodappuser.Model.Response.OrderConfirmResponseModel;
import com.app.foodappuser.Model.Response.PaymentMethodResponse.PaymentMethodResponseModel;
import com.app.foodappuser.Repository.PaymentMethodRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class PaymentMethodViewModel extends AndroidViewModel {

    PaymentMethodRepository paymentMethodRepository;
    AppDataBase database;
    private Application application;


    public PaymentMethodViewModel(@NonNull Application application) {
        super(application);
        paymentMethodRepository=new PaymentMethodRepository();
        database = AppDataBase.getAppDatabase(getApplication().getBaseContext());
        this.application = application ;
    }

    public void initCustomerSession(){
        paymentMethodRepository.initCustomerSession(application);
    }

    public LiveData<PaymentMethodResponseModel> getPaymentMethod(InputForAPI inputs) {
        return paymentMethodRepository.getPaymentMethods(inputs);
    }

    public LiveData<OrderConfirmResponseModel> orderConfirmPayment(InputForAPI inputs) {
        return paymentMethodRepository.orderConfirm(inputs);
    }

    public void deleteAllTableValues() {
        database.cartDao().deleteCartDetailsTable();
        database.cartDao().deleteCategoryItemsTable();
        database.cartDao().deleteDishCustomizationItemsTable();
        database.cartDao().deleteDishElementItemsTable();
        database.cartDao().deleteDishItemTable();
    }
    public MutableLiveData<String> setCardSource(){
        return paymentMethodRepository.setCardSource();
    }

}
