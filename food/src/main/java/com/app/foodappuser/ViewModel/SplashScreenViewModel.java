package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.SplashScreenResponseModel;
import com.app.foodappuser.Repository.SplashScreenRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class SplashScreenViewModel extends AndroidViewModel {
    SplashScreenRepository splashScreenRepository;

    public SplashScreenViewModel(@NonNull Application application) {
        super(application);
        splashScreenRepository=new SplashScreenRepository();
    }

    public LiveData<SplashScreenResponseModel> SplashScreenResponse(InputForAPI input){
        return splashScreenRepository.getSplashScreenResponse(input);
    }
}
