package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.content.Context;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.DishItemsResponseModel;
import com.app.foodappuser.Repository.DishItemsRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class DishItemsViewModel extends AndroidViewModel {
    Context context;
    DishItemsRepository dishItemsRepository;


    public DishItemsViewModel(@NonNull Application application) {
        super(application);
        this.dishItemsRepository=new DishItemsRepository();
    }

    public LiveData<DishItemsResponseModel> addDishToCard(InputForAPI input){
        return dishItemsRepository.addDishToCard(input);
    }

    public String getConcatenatedString(String one, String two) {
        return one + two;
    }


}
