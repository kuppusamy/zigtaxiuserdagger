package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.ChooseLocationResponseModel;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Model.Response.GetCurrentAddressResponse.GetCurrentAddressResponseModel;
import com.app.foodappuser.Model.Response.ListRestaurantResponse.ListRestaurantResponseModel;
import com.app.foodappuser.Repository.NearMeRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class NearMeViewModel  extends AndroidViewModel {

    NearMeRepository nearMeRepository;

    public NearMeViewModel(@NonNull Application application) {
        super(application);
        nearMeRepository = new NearMeRepository();
    }

    public LiveData<ListRestaurantResponseModel> getRestaurantsNearMe(InputForAPI inputs) {
        return  nearMeRepository.getRestaurantsNearMe(inputs);
    }

    public LiveData<GetCurrentAddressResponseModel> getCurrentAddress(InputForAPI inputs) {
        return  nearMeRepository.getCurrentAddress(inputs);
    }

    public LiveData<ChooseLocationResponseModel> getCurrentAddressFromLatLong(InputForAPI input){
        return nearMeRepository.getCurrentAddressFromLatLong(input);
    }

    public LiveData<GeneralResponseModel> generalResponseModel(InputForAPI input){
        return nearMeRepository.updateDeviceToken(input);
    }



}
