package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.app.chat.socket.ListenSocket;
import com.app.foodappdriver.Utilities.ChatListener;
import com.app.foodappuser.Model.Response.LiveTrackingResponse.LiveTrackingResponseModel;
import com.app.foodappuser.Model.Response.LiveTrackingSocketResponseModel;
import com.app.foodappuser.Repository.LiveTrackingRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

import org.jetbrains.annotations.NotNull;

public class LiveTrackingViewModel extends AndroidViewModel implements ChatListener {
    LiveTrackingRepository liveTrackingRepository;
    public LiveTrackingViewModel(@NonNull Application application) {
        super(application);
        liveTrackingRepository=new LiveTrackingRepository();
        ListenSocket.INSTANCE.onReceiveMsgCallBack(this);
    }

    MutableLiveData<LiveTrackingSocketResponseModel> liveTrackingViewModelMutableLiveData=new MutableLiveData<>();
    MutableLiveData<LiveTrackingSocketResponseModel> liveTrackingProviderLocationViewModelMutableLiveData=new MutableLiveData<>();

    public LiveData<LiveTrackingResponseModel> getLiveTracking(InputForAPI input){
        return liveTrackingRepository.getTracking(input);
    }
    public MutableLiveData<LiveTrackingSocketResponseModel> getLiveTrackingSocket(){
        return liveTrackingViewModelMutableLiveData;
    }
    public MutableLiveData<LiveTrackingSocketResponseModel> getLiveTrackingProviderLocationSocket(){
        return liveTrackingProviderLocationViewModelMutableLiveData;
    }

    @Override
    public void onReceiveMessageListener(@NotNull LiveTrackingSocketResponseModel response) {
        liveTrackingViewModelMutableLiveData.postValue(response);
    }

    @Override
    public void onProviderLocation(@NotNull LiveTrackingSocketResponseModel socketResponse) {
        liveTrackingProviderLocationViewModelMutableLiveData.postValue(socketResponse);

    }
}
