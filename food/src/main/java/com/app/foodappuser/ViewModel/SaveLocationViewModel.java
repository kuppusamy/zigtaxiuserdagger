package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.LocationResponseModel;
import com.app.foodappuser.Model.Response.SaveLocationResponseModel;
import com.app.foodappuser.Repository.SaveLocationRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class SaveLocationViewModel extends AndroidViewModel {
    SaveLocationRepository saveLocationRepository;


    public SaveLocationViewModel(@NonNull Application application) {
        super(application);
        saveLocationRepository=new SaveLocationRepository();
    }

    public LiveData<LocationResponseModel> getCurrentAddress(InputForAPI inputs){
        return saveLocationRepository.getChooseLocation(inputs);
    }

    public LiveData<SaveLocationResponseModel> addAddress(InputForAPI input){
        return saveLocationRepository.saveLocation(input);
    }





}
