package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.content.Context;
import androidx.annotation.NonNull;

import com.app.foodappuser.Model.Response.ForgotPasswordResponseModel;
import com.app.foodappuser.Model.Response.ResendOtpResponseModel;
import com.app.foodappuser.Repository.ForgotPasswordRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public class ForgotPasswordViewModel extends AndroidViewModel {

    ForgotPasswordRepository forgotPasswordRepository;
    Context context;


    public ForgotPasswordViewModel(@NonNull Application application) {
        super(application);
        forgotPasswordRepository = new ForgotPasswordRepository();
    }

    public LiveData<ForgotPasswordResponseModel> getAPIResponse(InputForAPI input){
        return forgotPasswordRepository.getForgotPassword(input);
    }

    public LiveData<ResendOtpResponseModel> resendOtp(InputForAPI input){
        return forgotPasswordRepository.resendOtpResponseModelLiveData(input);
    }


}
