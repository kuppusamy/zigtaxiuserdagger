package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.util.Log;

import com.app.foodappuser.Database.AppDataBase;
import com.app.foodappuser.Entity.CartDetails;
import com.app.foodappuser.Entity.DishCustomizationItems;
import com.app.foodappuser.Entity.DishElementItems;
import com.app.foodappuser.Entity.DishItemTable;
import com.app.foodappuser.Model.CartCounts.CountDishandQuantityModel;
import com.app.foodappuser.Model.Custom.SelectedCustomizationsModel;
import com.app.foodappuser.Model.MenuItems.MenuItemsModel;
import com.app.foodappuser.Model.Response.ListDishesResponse.DishItems;
import com.app.foodappuser.Model.Response.ListDishesResponse.ListDishesResponseModel;
import com.app.foodappuser.Repository.RestaurantDetailsRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;

import java.util.ArrayList;

public class RestaurantDetailsViewModel extends AndroidViewModel {

    private static final String TAG = RestaurantDetailsViewModel.class.getSimpleName();
    RestaurantDetailsRepository restaurantDetailsRepository;
    AppDataBase database;

    public RestaurantDetailsViewModel(@NonNull Application application) {
        super(application);
        restaurantDetailsRepository = new RestaurantDetailsRepository();
        database = AppDataBase.getAppDatabase(getApplication().getBaseContext());
    }


    public LiveData<ListDishesResponseModel> getDishesInRestaurant(InputForAPI inputs) {
        return restaurantDetailsRepository.getDishesInRestaurant(inputs);
    }


    public String getConcatenatedString(String one, String two) {
        return one + two;
    }


    public Bundle increaseCartCount(DishItems dishItem, String outletId) {

        Bundle bundle = new Bundle();
        DishItemTable[] dishItemTables = database.cartDao().getDishItemId(dishItem.getDishId());

        if (dishItemTables.length == 0) {
            //Add dish in table
            DishItemTable dishItemTable = new DishItemTable();
            dishItemTable.dishId = dishItem.getDishId();
            dishItemTable.dishName = dishItem.getDishName();
            dishItemTable.displayPrice = dishItem.getDisplayPrice();
            dishItemTable.isCustomizable = dishItem.getIsCustomizable();


            dishItemTable.price = dishItem.getPrice();
            dishItemTable.totalPrice = dishItem.getPrice();
            dishItemTable.isVeg = dishItem.getIsVeg();
            dishItemTable.quantity = 1;
            long[] dishInsertid = database.cartDao().insertDishItem(dishItemTable);
            Utils.log(TAG, "DishInsert Id : " + dishInsertid[0]);
            bundle.putBoolean(ConstantKeys.STATUS, true);
            DishItemTable insertedQuantity = database.cartDao().getDishItemQuantity(dishItem.getDishId());
            int quan= (int) insertedQuantity.quantity;
            bundle.putString(ConstantKeys.QUANTITY, String.valueOf(quan));

        } else {

            double quantity = 0.0;

            DishItemTable dishItemTable = database.cartDao().getDishItemQuantity(dishItem.getDishId());

            quantity = dishItemTable.quantity + 1;

            double totalPrice = quantity * (dishItemTable.price);

            DishItemTable dishItemTable1 = new DishItemTable();
            dishItemTable1.quantity = quantity;
            dishItemTable1.totalPrice = (totalPrice);
            dishItemTable1.displayPrice = String.valueOf(totalPrice);

            database.cartDao().updateDishItem(quantity, String.valueOf(totalPrice), String.valueOf(totalPrice), dishItem.getDishId());
            bundle.putBoolean(ConstantKeys.STATUS, true);
            DishItemTable insertedQuantity = database.cartDao().getDishItemQuantity(dishItem.getDishId());
            int quan= (int) insertedQuantity.quantity;
            bundle.putString(ConstantKeys.QUANTITY, String.valueOf(quan));
            Utils.log(TAG, "Quantity : " + String.valueOf(insertedQuantity.quantity));

        }

        updateCartItems(outletId);

        return bundle;
    }

    private void updateCartItems(String outletId) {

        DishItemTable[] dishItemTables = database.cartDao().getAllDishItems();
        double totalPrice = 0;
        double totalCount = 0;
        for (int i = 0; i < dishItemTables.length; i++) {
            totalPrice = totalPrice + (dishItemTables[i].totalPrice);
            totalCount = totalCount + (dishItemTables[i].quantity);
        }

        database.cartDao().updateCartDetails(String.valueOf(totalPrice), String.valueOf(totalCount), outletId);

    }

    public Bundle addNewCustomization(DishItems dishItems, ArrayList<SelectedCustomizationsModel> selectedCustomizationsModels, String outletId) {

        Bundle bundle = new Bundle();
        DishItemTable[] dishItemTables = database.cartDao().getDishItemId(dishItems.getDishId());


        if (dishItemTables.length == 0) {

            //Add dish in table
            DishItemTable dishItemTable = new DishItemTable();
            dishItemTable.dishId = dishItems.getDishId();
            dishItemTable.dishName = dishItems.getDishName();
            dishItemTable.displayPrice = dishItems.getDisplayPrice();
            dishItemTable.isCustomizable = dishItems.getIsCustomizable();
            dishItemTable.isVeg = dishItems.getIsVeg();
            dishItemTable.totalPrice = (getTotalInCustomization(selectedCustomizationsModels, dishItems.getPrice()));
            dishItemTable.price = (getTotalInCustomization(selectedCustomizationsModels, dishItems.getPrice()));
            dishItemTable.quantity = 1;
            long[] dishInsertid = database.cartDao().insertDishItem(dishItemTable);
            Utils.log(TAG, "DishInsert Id : " + dishInsertid[0]);

            //addCustomizationDish
            DishCustomizationItems dishCustomizationItems = new DishCustomizationItems();
            dishCustomizationItems.dishItemsId = (int) dishInsertid[0];
            if(selectedCustomizationsModels.size() != 0){
                dishCustomizationItems.customizableId = selectedCustomizationsModels.get(0).getCategoryId();
            }
            long[] dishCustomizationItemInsertId = database.cartDao().insertDishCustomizationItems(dishCustomizationItems);
            Utils.log(TAG, "dishCustomizationItemInsertId : " + dishCustomizationItemInsertId[0]);

            for (SelectedCustomizationsModel selectedCustomizationsModel : selectedCustomizationsModels) {

                DishElementItems dishElementItems = new DishElementItems();
                dishElementItems.dishCustomizationItemsId = (int) dishCustomizationItemInsertId[0];
                dishElementItems.elementId = selectedCustomizationsModel.getElementId();
                dishElementItems.elementName = selectedCustomizationsModel.getCustomizableName();

                long[] dishElementInsertId = database.cartDao().insertDishElementCustomizationItems(dishElementItems);
            }

            bundle.putBoolean(ConstantKeys.STATUS, true);
            DishItemTable[] insertedQuantity = database.cartDao().getDishItemId(dishItems.getDishId());

            double quantity = 0.0;

            for (int i = 0; i < insertedQuantity.length; i++) {
                quantity = quantity + insertedQuantity[i].quantity;
            }
            int quan= (int) quantity;
            bundle.putString(ConstantKeys.QUANTITY, String.valueOf(quan));

        } else {

            int count=0;
            int toBeInsertId = 0;
            for (int i = 0; i < dishItemTables.length; i++) {
                int id=dishItemTables[i].id;
                DishCustomizationItems dishCustomizationItems[]=database.cartDao().getCustomizationItems(id);
                for (int j = 0; j < dishCustomizationItems.length; j++) {
                   int cusId=dishCustomizationItems[j].id;
                   DishElementItems dishElementItems[]=database.cartDao().getDishElements(cusId);
                    for (int l = 0; l < selectedCustomizationsModels.size(); l++) {
                        for (int k = 0; k < dishElementItems.length; k++) {
                            if(selectedCustomizationsModels.get(l).getElementId()==dishElementItems[k].elementId){
                                count=count+1;
                                if(count==selectedCustomizationsModels.size()){
                                    toBeInsertId=id;
                                }
                            }
                        }
                    }
                }
            }
            Utils.log(TAG,"Count : "+count);
            if(count==selectedCustomizationsModels.size()){

                double quantity = 0.0;
                int lastRow = 0;

                DishItemTable[] dishItemTable = database.cartDao().getDishItem(String.valueOf(toBeInsertId));

                lastRow = dishItemTable.length - 1;

                quantity = dishItemTable[lastRow].quantity + 1;

                double totalPrice = quantity * (dishItemTable[lastRow].price);

                DishItemTable dishItemTable1 = new DishItemTable();
                dishItemTable1.quantity = quantity;
                dishItemTable1.totalPrice = (totalPrice);
                dishItemTable1.displayPrice = String.valueOf(totalPrice);

                database.cartDao().updateDishItems(quantity, String.valueOf(totalPrice), String.valueOf(totalPrice), dishItemTable[lastRow].id);
                bundle.putBoolean(ConstantKeys.STATUS, true);
                DishItemTable[] insertedQuantity = database.cartDao().getDishItemId(String.valueOf(dishItemTable[lastRow].dishId));
                double quantitys = 0.0;
                for (int i = 0; i < insertedQuantity.length; i++) {
                    quantitys = quantitys + insertedQuantity[i].quantity;
                }
                int quan= (int) quantity;
                bundle.putString(ConstantKeys.QUANTITY, String.valueOf(quan));
                Utils.log(TAG, "Quantity : " + String.valueOf(quantitys));

                updateCartItems(outletId);

            }else{
                Utils.log(TAG,"Different : "+count);
                DishItemTable dishItemTable = new DishItemTable();
                dishItemTable.dishId = dishItems.getDishId();
                dishItemTable.dishName = dishItems.getDishName();
                dishItemTable.displayPrice = dishItems.getDisplayPrice();
                dishItemTable.isCustomizable = dishItems.getIsCustomizable();
                dishItemTable.isVeg = dishItems.getIsVeg();
                dishItemTable.totalPrice = (getTotalInCustomization(selectedCustomizationsModels, dishItems.getPrice()));
                dishItemTable.price = (getTotalInCustomization(selectedCustomizationsModels, dishItems.getPrice()));
                dishItemTable.quantity = 1;
                long[] dishInsertid = database.cartDao().insertDishItem(dishItemTable);
                Utils.log(TAG, "DishInsert Id : " + dishInsertid[0]);

                //addCustomizationDish
                DishCustomizationItems dishCustomizationItems = new DishCustomizationItems();
                dishCustomizationItems.dishItemsId = (int) dishInsertid[0];
                dishCustomizationItems.customizableId = selectedCustomizationsModels.get(0).getCategoryId();
                long[] dishCustomizationItemInsertId = database.cartDao().insertDishCustomizationItems(dishCustomizationItems);
                Utils.log(TAG, "dishCustomizationItemInsertId : " + dishCustomizationItemInsertId[0]);

                for (SelectedCustomizationsModel selectedCustomizationsModel : selectedCustomizationsModels) {

                    DishElementItems dishElementItems = new DishElementItems();
                    dishElementItems.dishCustomizationItemsId = (int) dishCustomizationItemInsertId[0];
                    dishElementItems.elementId = selectedCustomizationsModel.getElementId();
                    dishElementItems.elementName = selectedCustomizationsModel.getCustomizableName();

                    long[] dishElementInsertId = database.cartDao().insertDishElementCustomizationItems(dishElementItems);
                }

                bundle.putBoolean(ConstantKeys.STATUS, true);
                DishItemTable[] insertedQuantity = database.cartDao().getDishItemId(dishItems.getDishId());

                double quantity = 0;

                for (int i = 0; i < insertedQuantity.length; i++) {
                    quantity = quantity + insertedQuantity[i].quantity;
                }
                int quan= (int) quantity;
                bundle.putString(ConstantKeys.QUANTITY, String.valueOf(quan));
            }

            Utils.log(TAG, "CustomizationSize : " + selectedCustomizationsModels.size());

        }

        updateCartItems(outletId);

        return bundle;
    }

    public Bundle repeatCustomization(DishItems dishItem, String outletId, boolean isRepeat) {

        Bundle bundle = new Bundle();
        double quantity = 0;
        int lastRow = 0;

        DishItemTable[] dishItemTable = database.cartDao().getDishItemId(dishItem.getDishId());

        lastRow = dishItemTable.length - 1;

        quantity = dishItemTable[lastRow].quantity + 1;

        double totalPrice = quantity * (dishItemTable[lastRow].price);

        DishItemTable dishItemTable1 = new DishItemTable();
        dishItemTable1.quantity = quantity;
        dishItemTable1.totalPrice = (totalPrice);
        dishItemTable1.displayPrice = String.valueOf(totalPrice);

        database.cartDao().updateDishItems(quantity, String.valueOf(totalPrice), String.valueOf(totalPrice), dishItemTable[lastRow].id);
        bundle.putBoolean(ConstantKeys.STATUS, true);
        DishItemTable[] insertedQuantity = database.cartDao().getDishItemId(dishItem.getDishId());
        double quantitys = 0.0;
        for (int i = 0; i < insertedQuantity.length; i++) {
            quantitys = quantitys + insertedQuantity[i].quantity;
        }
        int quan= (int) quantitys;
        bundle.putString(ConstantKeys.QUANTITY, String.valueOf(quan));
        Utils.log(TAG, "Quantity : " + String.valueOf(quantitys));

        updateCartItems(outletId);

        return bundle;

    }

    public double getTotalInCustomization(ArrayList<SelectedCustomizationsModel> vals, double dishPrice) {

        double total = 0.0;

        for (int i = 0; i < vals.size(); i++) {
//            Log.e(TAG, "getTotalInCustomization: "+vals.get(i).getPrice());
//            total = total+ Double.parseDouble(vals.get(i).getPrice());
//            Log.e(TAG, "totalValue: "+total);
            total=total+Double.parseDouble(vals.get(i).getPrice());
        }
        return total + (dishPrice);
    }


    public boolean isCheckOutletAvailableInCart() {
        CartDetails[] cartDetails = database.cartDao().getOutlet();

        Utils.log(TAG, "isCheckOutletAvailableInCart : " + cartDetails.length);

        if (cartDetails.length == 0) {
            return false;
        } else {
            return true;
        }

    }


    public Bundle addOutletId(DishItems dishItem, String outletId) {

        CartDetails cartDetails = new CartDetails();
        cartDetails.outletId = outletId;
        cartDetails.totalAmount = 0.0;
        cartDetails.totalItems = 0.0;

        long[] id = database.cartDao().insertOutletId(cartDetails);

        Utils.log(TAG, String.valueOf(id[0]));

        return increaseCartCount(dishItem, outletId);
    }


    public boolean checkOutletIdRepeated(String outletId) {

        CartDetails cartDetail = database.cartDao().checkOutlet(outletId);

        Log.e(TAG, "checkOutletIdRepeated: " + outletId);

        if (cartDetail != null) {
            if (cartDetail.outletId.equalsIgnoreCase(outletId)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public CartDetails getCartDetails() {
        CartDetails cartDetail = database.cartDao().getCartDetails();
        if (cartDetail != null)
            return cartDetail;
        else
            return null;
    }


    public Bundle addOutletIdFromCustomization(DishItems dishItems, String outletId, ArrayList<SelectedCustomizationsModel> selectedCustomizationsModels) {
        CartDetails cartDetails = new CartDetails();
        cartDetails.outletId = outletId;
        cartDetails.totalAmount = 0.0;
        cartDetails.totalItems = 0.0;

        long[] id = database.cartDao().insertOutletId(cartDetails);

        Utils.log(TAG, String.valueOf(id[0]));

        return addNewCustomization(dishItems, selectedCustomizationsModels, outletId);
    }

    public ListDishesResponseModel changeQuantity(ListDishesResponseModel listDishesResponseModel) {

        try {

            DishItemTable[] dishItemTableItems = database.cartDao().getAllDishItems();

            ArrayList<CountDishandQuantityModel> countDishandQuantityModels=new ArrayList<>();

            ArrayList<Integer> arrayList=new ArrayList<>();

            for (DishItemTable dishItemTableItem : dishItemTableItems) {
                arrayList.add(Integer.valueOf(dishItemTableItem.dishId));
            }

            ArrayList<Integer> sorted=removeDuplicates(arrayList);

            int total=0;

            for (int i = 0; i < sorted.size(); i++) {

                for (int j = 0; j < dishItemTableItems.length; j++) {

                    if(sorted.get(i)==Integer.parseInt(dishItemTableItems[j].dishId)){
                        total= (int) (total+dishItemTableItems[j].quantity);
                    }
                }
                CountDishandQuantityModel countDishandQuantityModel=new CountDishandQuantityModel();
                countDishandQuantityModel.setId(String.valueOf(sorted.get(i)));
                countDishandQuantityModel.setQuantity(total);
                countDishandQuantityModels.add(countDishandQuantityModel);
                Utils.loge(TAG,""+total);
                total=0;
            }



            for (int i = 0; i <countDishandQuantityModels.size() ; i++) {
                Utils.log(TAG,"Id : "+countDishandQuantityModels.get(i).getId()+" Quantity : "+countDishandQuantityModels.get(i).getQuantity());
            }


            for (int i = 0; i < countDishandQuantityModels.size(); i++) {

                for (int j = 0; j < listDishesResponseModel.getDishes().size(); j++) {

                    if (listDishesResponseModel.getDishes().get(j).getIsHavingSubCategory() == 0) {
                        for (int k = 0; k < listDishesResponseModel.getDishes().get(j).getCategoryValues().size(); k++) {

                            if (countDishandQuantityModels.get(i).getId().equalsIgnoreCase(listDishesResponseModel.getDishes().get(j).getCategoryValues().get(k).getDishId())) {
                                listDishesResponseModel.getDishes().get(j).getCategoryValues().get(k).setQuantity(countDishandQuantityModels.get(i).getQuantity());

                            }
                        }

                    } else {
                        for (int k = 0; k < listDishesResponseModel.getDishes().get(j).getSubCategoryValues().size(); k++) {
                            for (int l = 0; l < listDishesResponseModel.getDishes().get(j).getSubCategoryValues().get(k).getDishItems().size(); l++) {
                                if (countDishandQuantityModels.get(i).getId().equalsIgnoreCase(listDishesResponseModel.getDishes().get(j).getSubCategoryValues().get(k).getDishItems().get(l).getDishId())) {
                                    listDishesResponseModel.getDishes().get(j).getSubCategoryValues().get(k).getDishItems().get(l).setQuantity(countDishandQuantityModels.get(i).getQuantity());
                                }
                            }
                        }
                    }


                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return listDishesResponseModel;
    }

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list)
    {

        // Create a new ArrayList
        ArrayList<T> newList = new ArrayList<T>();

        // Traverse through the first list
        for (T element : list) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }

        // return the new list
        return newList;
    }

    public boolean checkCustomizationdish(DishItems items) {
        DishItemTable[] dishItemTables = database.cartDao().getDishItemId(items.getDishId());

        if (dishItemTables.length == 0) {
            return true;
        } else {
            return false;
        }
    }

    public Bundle decreaseCartCount(DishItems dishItem, String outletId) {

        Bundle bundle = new Bundle();

        double quantity = 0;

        DishItemTable dishItemTable = database.cartDao().getDishItemQuantity(dishItem.getDishId());

        if(dishItemTable!=null){
            quantity = dishItemTable.quantity;

            if (quantity <= 1) {

                database.cartDao().deleteDishItem(dishItem.getDishId());
                bundle.putBoolean(ConstantKeys.STATUS, true);
                bundle.putString(ConstantKeys.QUANTITY, "0");

            } else {

                double totalPrice = 0;

                if (quantity <= 2) {
                    totalPrice = (dishItem.getPrice());
                } else {
                    totalPrice = (dishItemTable.totalPrice) - (dishItem.getPrice());
                }

                DishItemTable dishItemTable1 = new DishItemTable();
                dishItemTable1.quantity = quantity - 1;
                dishItemTable1.totalPrice = (totalPrice);
                dishItemTable1.displayPrice = String.valueOf(totalPrice);

                database.cartDao().updateDishItem(quantity - 1, String.valueOf(totalPrice), String.valueOf(totalPrice), dishItem.getDishId());
                bundle.putBoolean(ConstantKeys.STATUS, true);
                DishItemTable insertedQuantity = database.cartDao().getDishItemQuantity(dishItem.getDishId());
                int quan= (int) insertedQuantity.quantity;
                bundle.putString(ConstantKeys.QUANTITY, String.valueOf(quan));
                Utils.log(TAG, "Quantity : " + String.valueOf(insertedQuantity.quantity));
            }

            updateCartItems(outletId);

        }else{
            bundle.putBoolean(ConstantKeys.STATUS, false);

        }


        return bundle;
    }

    public void deleteAllTableValues() {
        database.cartDao().deleteCartDetailsTable();
        database.cartDao().deleteCategoryItemsTable();
        database.cartDao().deleteDishCustomizationItemsTable();
        database.cartDao().deleteDishElementItemsTable();
        database.cartDao().deleteDishItemTable();
    }

    public ArrayList<SelectedCustomizationsModel> addCustomizationElement(Boolean isSingleChoice, int categoryId, int elementId, String name, String price, ArrayList<SelectedCustomizationsModel> selectedCustomizationsModels, DishItems dishItems) {
        if (isSingleChoice) {
            for (int i = 0; i < selectedCustomizationsModels.size(); i++) {
                if (selectedCustomizationsModels.get(i).getCategoryId() == categoryId) {
                    selectedCustomizationsModels.remove(i);
                }
            }
            final SelectedCustomizationsModel selectedCustomizationsModel = new SelectedCustomizationsModel();
            selectedCustomizationsModel.setCustomizableName(name);
            selectedCustomizationsModel.setCategoryId(categoryId);
            selectedCustomizationsModel.setElementId(elementId);
            selectedCustomizationsModel.setPrice(price);
            selectedCustomizationsModels.add(selectedCustomizationsModel);

        } else {
            final SelectedCustomizationsModel selectedCustomizationsModel = new SelectedCustomizationsModel();
            selectedCustomizationsModel.setCustomizableName(name);
            selectedCustomizationsModel.setCategoryId(categoryId);
            selectedCustomizationsModel.setElementId(elementId);
            selectedCustomizationsModel.setPrice(price);
            selectedCustomizationsModels.add(selectedCustomizationsModel);

        }
        return selectedCustomizationsModels;
    }

    public ArrayList<MenuItemsModel> getOnlyMenuItems(ListDishesResponseModel listDishesResponseModel) {
        ArrayList<MenuItemsModel> menuItemsModels = new ArrayList<>();
        for (int i = 0; i < listDishesResponseModel.getDishes().size(); i++) {
            MenuItemsModel menuItemsModel=new MenuItemsModel();
            menuItemsModel.setCategoryId(listDishesResponseModel.getDishes().get(i).getCategoryId());
            menuItemsModel.setCategoryName(listDishesResponseModel.getDishes().get(i).getCategoryName());
            menuItemsModels.add(menuItemsModel);
        }
        return menuItemsModels;
    }
}
