package com.app.foodappuser.ViewModel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.app.foodappuser.Database.AppDataBase;
import com.app.foodappuser.Model.RecentSearches.RestaurantName;
import com.app.foodappuser.Model.Response.ExploreResponse.ExploreResponseModel;
import com.app.foodappuser.Model.Response.ListRestaurantResponse.ListRestaurantResponseModel;
import com.app.foodappuser.Repository.ExploreRepository;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;

import java.util.ArrayList;

public class ExploreViewModel extends AndroidViewModel {

    ExploreRepository exploreRepository;

    AppDataBase appDataBase;

    public ExploreViewModel(@NonNull Application application) {
        super(application);
        exploreRepository = new ExploreRepository();
        appDataBase = AppDataBase.getAppDatabase(getApplication().getBaseContext());

    }

    public LiveData<ExploreResponseModel> getRestaurantsNearMe(InputForAPI inputs) {
        return exploreRepository.getRestaurantsNearMe(inputs);
    }

    public void setRecentSearches(String restaurantName, String id, String cuisines, String displayTime, String averageReview, String displayCostForTwo, String couponEnabledForRestaurant, String longDescription) {



        AppSettings appSettings = new AppSettings(getApplication().getBaseContext());

        ArrayList<RestaurantName> searches = (ArrayList<RestaurantName>) appSettings.getSearches();

        if (searches != null) {


            RestaurantName restaurantObject = new RestaurantName();
            restaurantObject.setRestaurantName(restaurantName);
            restaurantObject.setId(id);
            restaurantObject.setCuisines(cuisines);
            restaurantObject.setDisplayTime(displayTime);
            restaurantObject.setAverageReview(averageReview);
            restaurantObject.setDisplayCostForTwo(displayCostForTwo);
            restaurantObject.setCouponEnabledForRestaurant(couponEnabledForRestaurant);
            restaurantObject.setLongDescription(longDescription);

            boolean containsResult=false;
            int containsIndex = 0;

            for (int i = 0; i < searches.size(); i++) {

                if(searches.get(i).getId().equalsIgnoreCase(id)){
                    containsResult=true;
                    containsIndex=i;
                }

            }

            if(containsResult){

                searches.remove(containsIndex);
                searches.add(0,restaurantObject);
                appSettings.setSearches(searches);

            }else{

                searches.add(restaurantObject);

                ArrayList<RestaurantName> oldlist = searches;
                ArrayList<RestaurantName> newList = new ArrayList<RestaurantName>();

                int size = oldlist.size()-1;

                for(int i=size;i>=0;i--){
                    newList.add(oldlist.get(i));
                }

                if (newList.size() > 5) {
                    for (int i = 5; i < newList.size(); i++) {
                        newList.remove(i);
                    }
                }

                for (RestaurantName name : newList) {
                    Utils.log("Explore ","outletId : "+name.getId()+" outletName : "+name.getRestaurantName());
                }

                appSettings.setSearches(newList);
            }



        } else {

            ArrayList<RestaurantName> newSearches = new ArrayList<>();
            RestaurantName restaurantObject = new RestaurantName();
            restaurantObject.setRestaurantName(restaurantName);
            restaurantObject.setId(id);
            restaurantObject.setCuisines(cuisines);
            restaurantObject.setDisplayTime(displayTime);
            restaurantObject.setAverageReview(averageReview);
            restaurantObject.setDisplayCostForTwo(displayCostForTwo);
            restaurantObject.setCouponEnabledForRestaurant(couponEnabledForRestaurant);
            restaurantObject.setLongDescription(longDescription);
            newSearches.add(restaurantObject);
            appSettings.setSearches(newSearches);

        }
    }
}
