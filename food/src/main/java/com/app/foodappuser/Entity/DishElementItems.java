package com.app.foodappuser.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DishElementItems {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "dish_customization_items_id")
    public int dishCustomizationItemsId;

    @ColumnInfo(name = "element_id")
    public int elementId ;

    @ColumnInfo(name = "element_name")
    public String elementName;
}
