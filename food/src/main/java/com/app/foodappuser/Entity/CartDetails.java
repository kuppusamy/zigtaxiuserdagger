package com.app.foodappuser.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class CartDetails {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "outlet_id")
    public String outletId;

    @ColumnInfo(name = "total_items")
    public double totalItems;

    @ColumnInfo(name = "total_amount")
    public double totalAmount;

}
