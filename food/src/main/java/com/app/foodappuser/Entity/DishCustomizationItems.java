package com.app.foodappuser.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DishCustomizationItems {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "dish_items_id")
    public int dishItemsId;

    @ColumnInfo(name = "customizable_id")
    public int customizableId;


}
