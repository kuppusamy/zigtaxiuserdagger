package com.app.foodappuser.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DishItemTable {

    @PrimaryKey(autoGenerate = true)
    public int id;

//    @ColumnInfo(name = "category_items_id")
//    public int categoryItemsId;

    @ColumnInfo(name = "dish_id")
    public String dishId;

    @ColumnInfo(name = "dish_name")
    public String dishName;

    @ColumnInfo(name = "is_veg")
    public int isVeg;

    @ColumnInfo(name = "is_customizable")
    public int isCustomizable;

    @ColumnInfo(name = "total_price")
    public double totalPrice;

    @ColumnInfo(name = "price")
    public double price;

    @ColumnInfo(name = "quantity")
    public double quantity;

    @ColumnInfo(name = "display_price")
    public String displayPrice;

}

