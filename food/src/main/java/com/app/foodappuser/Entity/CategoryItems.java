package com.app.foodappuser.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class CategoryItems {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "category_id")
    public String categoryId;

    @ColumnInfo(name = "category_name")
    public String categoryName;
}
