package com.app.foodappuser.Model.Custom;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomizationModel {

    @SerializedName("customizations")
    @Expose
    private List<SelectedCustomizationsModel> customizations = null;



    public List<SelectedCustomizationsModel> getCustomizations() {
        return customizations;
    }

    public void setCustomizations(List<SelectedCustomizationsModel> customizations) {
        this.customizations = customizations;
    }



}
