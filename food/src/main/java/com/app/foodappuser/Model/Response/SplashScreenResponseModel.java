package com.app.foodappuser.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SplashScreenResponseModel {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;

    @SerializedName("isPassword")
    @Expose
    private Boolean isPassword;

    @SerializedName("termsAndConditions")
    @Expose
    private String termsAndConditions;


    @SerializedName("androidMapKey")
    @Expose
    private String androidMapKey;

    public Boolean getIsTwilioOtp() {
        return isTwilioOtp;
    }

    public void setIsTwilioOtp(Boolean isTwilioOtp) {
        this.isTwilioOtp = isTwilioOtp;
    }

    @SerializedName("isTwilioOtp")
    @Expose
    private Boolean isTwilioOtp;


    @SerializedName("currency")
    @Expose
    private String currency;


    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }


    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getIsPasswordLogin() {
        return isPassword;
    }

    public void setIsPasswordLogin(Boolean isPassword) {
        this.isPassword = isPassword;
    }

    public String getAndroidMapKey() {
        return androidMapKey;
    }

    public void setAndroidMapKey(String androidMapKey) {
        this.androidMapKey = androidMapKey;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }


}
