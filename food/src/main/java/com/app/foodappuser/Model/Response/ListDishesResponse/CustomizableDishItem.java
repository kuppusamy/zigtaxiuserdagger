
package com.app.foodappuser.Model.Response.ListDishesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomizableDishItem {

    @SerializedName("elementId")
    @Expose
    private Integer elementId;
    @SerializedName("elementName")
    @Expose
    private String elementName;
    @SerializedName("isSelected")
    @Expose
    private Integer isSelected;
    @SerializedName("Price")
    @Expose
    private String price;
    @SerializedName("units")
    @Expose
    private String units;


    @SerializedName("displayPrice")
    @Expose
    private String displayPrice;
    @SerializedName("isVeg")
    @Expose
    private Integer isVeg;

    @SerializedName("customisationCategoryId")
    @Expose
    private Integer customisationCategoryId;

    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public Integer getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Integer isSelected) {
        this.isSelected = isSelected;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public String getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

    public Integer getCustomisationCategoryId() {
        return customisationCategoryId;
    }

    public void setCustomisationCategoryId(Integer customisationCategoryId) {
        this.customisationCategoryId = customisationCategoryId;
    }


}
