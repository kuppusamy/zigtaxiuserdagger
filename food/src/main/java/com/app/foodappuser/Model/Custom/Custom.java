package com.app.foodappuser.Model.Custom;

public class Custom {

    public Custom(String id, String name) {
        this.id = id;
        this.name = name;
    }

    String id;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    String name;
}
