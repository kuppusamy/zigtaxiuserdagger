
package com.app.foodappuser.Model.Response.ViewCartResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Total {

    @SerializedName("itemTotalName")
    @Expose
    private String itemTotalName;
    @SerializedName("displayItemTotal")
    @Expose
    private String displayItemTotal;
    @SerializedName("itemTotal")
    @Expose
    private double itemTotal;
    @SerializedName("topayName")
    @Expose
    private String topayName;
    @SerializedName("DisplayTopayAmount")
    @Expose
    private String displayTopayAmount;
    @SerializedName("topayAmount")
    @Expose
    private double topayAmount;

    public String getItemTotalName() {
        return itemTotalName;
    }

    public void setItemTotalName(String itemTotalName) {
        this.itemTotalName = itemTotalName;
    }

    public String getDisplayItemTotal() {
        return displayItemTotal;
    }

    public void setDisplayItemTotal(String displayItemTotal) {
        this.displayItemTotal = displayItemTotal;
    }

    public double getItemTotal() {
        return itemTotal;
    }

    public void setItemTotal(double itemTotal) {
        this.itemTotal = itemTotal;
    }

    public String getTopayName() {
        return topayName;
    }

    public void setTopayName(String topayName) {
        this.topayName = topayName;
    }

    public String getDisplayTopayAmount() {
        return displayTopayAmount;
    }

    public void setDisplayTopayAmount(String displayTopayAmount) {
        this.displayTopayAmount = displayTopayAmount;
    }

    public double getTopayAmount() {
        return topayAmount;
    }

    public void setTopayAmount(double topayAmount) {
        this.topayAmount = topayAmount;
    }

}
