
package com.app.foodappuser.Model.Response.ListDishesResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DishCustomization {

    @SerializedName("customizableId")
    @Expose
    private String customizableId;
    @SerializedName("customizableName")
    @Expose
    private String customizableName;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("customizableType")
    @Expose
    private String customizableType;
    @SerializedName("customizableDishItems")
    @Expose
    private List<CustomizableDishItem> customizableDishItems = null;

    public String getCustomizableId() {
        return customizableId;
    }

    public void setCustomizableId(String customizableId) {
        this.customizableId = customizableId;
    }

    public String getCustomizableName() {
        return customizableName;
    }

    public void setCustomizableName(String customizableName) {
        this.customizableName = customizableName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCustomizableType() {
        return customizableType;
    }

    public void setCustomizableType(String customizableType) {
        this.customizableType = customizableType;
    }

    public List<CustomizableDishItem> getCustomizableDishItems() {
        return customizableDishItems;
    }

    public void setCustomizableDishItems(List<CustomizableDishItem> customizableDishItems) {
        this.customizableDishItems = customizableDishItems;
    }

}
