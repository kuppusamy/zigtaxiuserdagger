
package com.app.foodappuser.Model.Response.PastOrderResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Charge implements Serializable {

    @SerializedName("displayKey")
    @Expose
    private String displayKey;
    @SerializedName("displayValue")
    @Expose
    private String displayValue;

    public String getDisplayKey() {
        return displayKey;
    }

    public void setDisplayKey(String displayKey) {
        this.displayKey = displayKey;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

}
