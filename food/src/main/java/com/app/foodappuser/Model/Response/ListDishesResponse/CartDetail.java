
package com.app.foodappuser.Model.Response.ListDishesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartDetail {

    @SerializedName("dishCount")
    @Expose
    private String dishCount;
    @SerializedName("totalAmount")
    @Expose
    private String totalAmount;
    @SerializedName("unit")
    @Expose
    private String unit;

    public String getDishCount() {
        return dishCount;
    }

    public void setDishCount(String dishCount) {
        this.dishCount = dishCount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

}
