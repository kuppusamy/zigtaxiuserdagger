package com.app.foodappuser.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckUserAvailabilityResponseModel {
    @SerializedName("error")
    @Expose
    private Boolean error;

    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;

    @SerializedName("isNewUser")
    @Expose
    private Boolean isNewUser;

    @SerializedName("otpNumber")
    @Expose
    private int otpNumber;



    @SerializedName("timeDelayForOtp")
    @Expose
    private int timeDelayForOtp;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getNewUser() {
        return isNewUser;
    }

    public void setNewUser(Boolean newUser) {
        isNewUser = newUser;
    }

    public int getOtpNumber() {
        return otpNumber;
    }

    public void setOtpNumber(int otpNumber) {
        this.otpNumber = otpNumber;
    }

    public int getTimeDelayForOtp() {
        return timeDelayForOtp;
    }

    public void setTimeDelayForOtp(int timeDelayForOtp) {
        this.timeDelayForOtp = timeDelayForOtp;
    }
}
