
package com.app.foodappuser.Model.Response.ExploreResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DishesList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("outletId")
    @Expose
    private Integer outletId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("slashedPrice")
    @Expose
    private Integer slashedPrice;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("isRecommended")
    @Expose
    private Integer isRecommended;
    @SerializedName("categoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("isVeg")
    @Expose
    private Integer isVeg;
    @SerializedName("showFromTime")
    @Expose
    private String showFromTime;
    @SerializedName("showToTime")
    @Expose
    private String showToTime;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOutletId() {
        return outletId;
    }

    public void setOutletId(Integer outletId) {
        this.outletId = outletId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getSlashedPrice() {
        return slashedPrice;
    }

    public void setSlashedPrice(Integer slashedPrice) {
        this.slashedPrice = slashedPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsRecommended() {
        return isRecommended;
    }

    public void setIsRecommended(Integer isRecommended) {
        this.isRecommended = isRecommended;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public String getShowFromTime() {
        return showFromTime;
    }

    public void setShowFromTime(String showFromTime) {
        this.showFromTime = showFromTime;
    }

    public String getShowToTime() {
        return showToTime;
    }

    public void setShowToTime(String showToTime) {
        this.showToTime = showToTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
