
package com.app.foodappuser.Model.Response.ExploreResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExploreResponseModel {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("listRestaurants")
    @Expose
    private List<ListRestaurant> listRestaurants = null;
    @SerializedName("listDishes")
    @Expose
    private List<ListDish> listDishes = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<ListRestaurant> getListRestaurants() {
        return listRestaurants;
    }

    public void setListRestaurants(List<ListRestaurant> listRestaurants) {
        this.listRestaurants = listRestaurants;
    }

    public List<ListDish> getListDishes() {
        return listDishes;
    }

    public void setListDishes(List<ListDish> listDishes) {
        this.listDishes = listDishes;
    }

}
