package com.app.foodappuser.Model.Custom;

public class SelectedCustomizationsModel {

    private String customizableName;
    private int categoryId;
    private int elementId;
    private String price;


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getElementId() {
        return elementId;
    }

    public void setElementId(int elementId) {
        this.elementId = elementId;
    }


    public String getCustomizableName() {
        return customizableName;
    }

    public void setCustomizableName(String customizableName) {
        this.customizableName = customizableName;
    }

}
