
package com.app.foodappuser.Model.Response.ListRestaurantResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RestaurantModel {

    @SerializedName("restaurantName")
    @Expose
    private String restaurantName;
    @SerializedName("restaurantId")
    @Expose
    private String restaurantId;
    @SerializedName("restaurantImage")
    @Expose
    private String restaurantImage;
    @SerializedName("cuisines")
    @Expose
    private String cuisines;
    @SerializedName("averageReview")
    @Expose
    private String averageReview;
    @SerializedName("costForTwo")
    @Expose
    private String costForTwo;
    @SerializedName("units")
    @Expose
    private String units;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("isExculsive")
    @Expose
    private String isExculsive;
    @SerializedName("isFavourite")
    @Expose
    private String isFavourite;
    @SerializedName("isPromoted")
    @Expose
    private String isPromoted;
    @SerializedName("outlet")
    @Expose
    private List<OutletModel> outlets = null;
    @SerializedName("isServicable")
    @Expose
    private Boolean isServicable;
    @SerializedName("displayTime")
    @Expose
    private String displayTime;
    @SerializedName("displayCostForTwo")
    @Expose
    private String displayCostForTwo;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;

    public String getCouponEnabledForRestaurant() {
        return couponEnabledForRestaurant;
    }

    public void setCouponEnabledForRestaurant(String couponEnabledForRestaurant) {
        this.couponEnabledForRestaurant = couponEnabledForRestaurant;
    }

    @SerializedName("couponEnabledForRestaurant")
    @Expose
    private String couponEnabledForRestaurant;
    @SerializedName("couponName")
    @Expose
    private String couponName;

    public String getDisplayCostForTwo() {
        return displayCostForTwo;
    }

    public void setDisplayCostForTwo(String displayCostForTwo) {
        this.displayCostForTwo = displayCostForTwo;
    }



    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }



    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantImage() {
        return restaurantImage;
    }

    public void setRestaurantImage(String restaurantImage) {
        this.restaurantImage = restaurantImage;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public String getAverageReview() {
        return averageReview;
    }

    public void setAverageReview(String averageReview) {
        this.averageReview = averageReview;
    }

    public String getCostForTwo() {
        return costForTwo;
    }

    public void setCostForTwo(String costForTwo) {
        this.costForTwo = costForTwo;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIsExculsive() {
        return isExculsive;
    }

    public void setIsExculsive(String isExculsive) {
        this.isExculsive = isExculsive;
    }

    public String getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(String isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getIsPromoted() {
        return isPromoted;
    }

    public void setIsPromoted(String isPromoted) {
        this.isPromoted = isPromoted;
    }

    public List<OutletModel> getOutlets() {
        return outlets;
    }

    public void setOutlets(List<OutletModel> outlets) {
        this.outlets = outlets;
    }

    public Boolean getIsServicable() {
        return isServicable;
    }

    public void setIsServicable(Boolean isServicable) {
        this.isServicable = isServicable;
    }

}
