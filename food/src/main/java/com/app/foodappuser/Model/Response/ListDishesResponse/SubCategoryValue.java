
package com.app.foodappuser.Model.Response.ListDishesResponse;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategoryValue {

    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("categoryValues")
    @Expose
    private List<DishItems> dishItems = null;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<DishItems> getDishItems() {
        return dishItems;
    }

    public List<DishItems> getVegOnlyValues() {
        List<DishItems> vegOnlyDish =new ArrayList<>();

        for (int i = 0; i <dishItems.size() ; i++) {
            if(dishItems.get(i).getIsVeg()==1){
                vegOnlyDish.add(dishItems.get(i));
            }
        }
        return vegOnlyDish;
    }


    public void setDishItems(List<DishItems> dishItems) {
        this.dishItems = dishItems;
    }

}
