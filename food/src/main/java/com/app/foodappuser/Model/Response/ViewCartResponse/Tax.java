
package com.app.foodappuser.Model.Response.ViewCartResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tax {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("taxName")
    @Expose
    private String taxName;
    @SerializedName("percentage")
    @Expose
    private double percentage;
    @SerializedName("dishplayTax")
    @Expose
    private String dishplayTax;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public String getDishplayTax() {
        return dishplayTax;
    }

    public void setDishplayTax(String dishplayTax) {
        this.dishplayTax = dishplayTax;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
