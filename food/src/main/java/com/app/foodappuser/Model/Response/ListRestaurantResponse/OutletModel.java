
package com.app.foodappuser.Model.Response.ListRestaurantResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutletModel {


    @SerializedName("outletName")
    @Expose
    private String outletName;
    @SerializedName("outletId")
    @Expose
    private String outletId;
    @SerializedName("restaurantId")
    @Expose
    private String restaurantId;
    @SerializedName("offers")
    @Expose
    private List<OfferModel> offers = null;
    @SerializedName("averageReview")
    @Expose
    private String averageReview;
    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("displayTime")
    @Expose
    private String displayTime;

    @SerializedName("cuisines")
    @Expose
    private String cuisines;

    @SerializedName("displayCostForTwo")
    @Expose
    private String displayCostForTwo;



    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    @SerializedName("longDescription")
    @Expose
    private String longDescription;
    @SerializedName("couponName")
    @Expose
    private String couponName;

    public String getCouponEnabledForRestaurant() {
        return couponEnabledForRestaurant;
    }

    public void setCouponEnabledForRestaurant(String couponEnabledForRestaurant) {
        this.couponEnabledForRestaurant = couponEnabledForRestaurant;
    }
    @SerializedName("couponEnabledForRestaurant")
    @Expose
    private String couponEnabledForRestaurant;


    public String getDisplayCostForTwo() {
        return displayCostForTwo;
    }

    public void setDisplayCostForTwo(String displayCostForTwo) {
        this.displayCostForTwo = displayCostForTwo;
    }


    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }


    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }



    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public List<OfferModel> getOffers() {
        return offers;
    }

    public void setOffers(List<OfferModel> offers) {
        this.offers = offers;
    }

    public String getAverageReview() {
        return averageReview;
    }

    public void setAverageReview(String averageReview) {
        this.averageReview = averageReview;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }
}
