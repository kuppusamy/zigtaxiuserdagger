package com.app.foodappuser.Model.Response.GetCurrentAddressResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentAddress {

    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("houseFlatNo")
    @Expose
    private String houseFlatNo;
    @SerializedName("landMark")
    @Expose
    private String landMark;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHouseFlatNo() {
        return houseFlatNo;
    }

    public void setHouseFlatNo(String houseFlatNo) {
        this.houseFlatNo = houseFlatNo;
    }

    public String getLandMark() {
        return landMark;
    }

    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

}
