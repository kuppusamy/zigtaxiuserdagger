
package com.app.foodappuser.Model.Response.ExploreResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Outlet {

    @SerializedName("outletId")
    @Expose
    private String outletId;
    @SerializedName("restaurantId")
    @Expose
    private Integer restaurantId;
    @SerializedName("outletName")
    @Expose
    private String outletName;
    @SerializedName("isServicable")
    @Expose
    private Integer isServicable;
    @SerializedName("offers")
    @Expose
    private List<Object> offers = null;
    @SerializedName("averageReview")
    @Expose
    private String averageReview;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("displayTime")
    @Expose
    private String displayTime;
    @SerializedName("cuisines")
    @Expose
    private String cuisines;
    @SerializedName("costForTwo")
    @Expose
    private Integer costForTwo;
    @SerializedName("displayCostForTwo")
    @Expose
    private String displayCostForTwo;
    @SerializedName("totalAmount")
    @Expose
    private String totalAmount;
    @SerializedName("balanceAmount")
    @Expose
    private String balanceAmount;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("couponEnabledForRestaurant")
    @Expose
    private String couponEnabledForRestaurant;

    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    @SerializedName("longDescription")
    @Expose
    private String longDescription;

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public Integer getIsServicable() {
        return isServicable;
    }

    public void setIsServicable(Integer isServicable) {
        this.isServicable = isServicable;
    }

    public List<Object> getOffers() {
        return offers;
    }

    public void setOffers(List<Object> offers) {
        this.offers = offers;
    }

    public String getAverageReview() {
        return averageReview;
    }

    public void setAverageReview(String averageReview) {
        this.averageReview = averageReview;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public Integer getCostForTwo() {
        return costForTwo;
    }

    public void setCostForTwo(Integer costForTwo) {
        this.costForTwo = costForTwo;
    }

    public String getDisplayCostForTwo() {
        return displayCostForTwo;
    }

    public void setDisplayCostForTwo(String displayCostForTwo) {
        this.displayCostForTwo = displayCostForTwo;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCouponEnabledForRestaurant() {
        return couponEnabledForRestaurant;
    }

    public void setCouponEnabledForRestaurant(String couponEnabledForRestaurant) {
        this.couponEnabledForRestaurant = couponEnabledForRestaurant;
    }

}
