
package com.app.foodappuser.Model.Response.ListDishesResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RestaurantDetail {

    @SerializedName("restaurantName")
    @Expose
    private String restaurantName;
    @SerializedName("restaurantId")
    @Expose
    private String restaurantId;
    @SerializedName("cuisines")
    @Expose
    private String cuisines;
    @SerializedName("averageReview")
    @Expose
    private String averageReview;
    @SerializedName("costForTwo")
    @Expose
    private String costForTwo;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("isFavourite")
    @Expose
    private String isFavourite;
    @SerializedName("units")
    @Expose
    private String units;
    @SerializedName("isVeg")
    @Expose
    private Integer isVeg;
    @SerializedName("Offers")
    @Expose
    private List<Offer> offers = null;

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public String getAverageReview() {
        return averageReview;
    }

    public void setAverageReview(String averageReview) {
        this.averageReview = averageReview;
    }

    public String getCostForTwo() {
        return costForTwo;
    }

    public void setCostForTwo(String costForTwo) {
        this.costForTwo = costForTwo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(String isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

}
