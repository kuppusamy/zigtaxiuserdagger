package com.app.foodappuser.Model.Response.LiveTrackingResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LiveTrackingResponseModel {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("ordersDetails")
    @Expose
    private OrdersDetails ordersDetails;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public OrdersDetails getOrdersDetails() {
        return ordersDetails;
    }

    public void setOrdersDetails(OrdersDetails ordersDetails) {
        this.ordersDetails = ordersDetails;
    }

}
