
package com.app.foodappuser.Model.Response.ViewCartResponse;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dishes {

    @SerializedName("outletDetails")
    @Expose
    private OutletDetails outletDetails;
    @SerializedName("dishes")
    @Expose
    private List<Dish> dishes = null;
    @SerializedName("billTotals")
    @Expose
    private List<BillTotal> billTotals = null;
    @SerializedName("isTwoMany")
    @Expose
    private Boolean isTwoMany;
    @SerializedName("toMany")
    @Expose
    private ToMany toMany;
    @SerializedName("address")
    @Expose
    private Address address;

    @SerializedName("isAddress")
    @Expose
    private Boolean isAddress;

    public void setAddress(Boolean address) {
        isAddress = address;
    }

    public Boolean getIsAddress(){
        return isAddress;
    }

    public OutletDetails getOutletDetails() {
        return outletDetails;
    }

    public void setOutletDetails(OutletDetails outletDetails) {
        this.outletDetails = outletDetails;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public List<BillTotal> getBillTotals() {
        return billTotals;
    }

    public void setBillTotals(List<BillTotal> billTotals) {
        this.billTotals = billTotals;
    }

    public Boolean getIsTwoMany() {
        return isTwoMany;
    }

    public void setIsTwoMany(Boolean isTwoMany) {
        this.isTwoMany = isTwoMany;
    }

    public ToMany getToMany() {
        return toMany;
    }

    public void setToMany(ToMany toMany) {
        this.toMany = toMany;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
