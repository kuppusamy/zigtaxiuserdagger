
package com.app.foodappuser.Model.Response.ViewCartResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomizableDishItem {

    @SerializedName("elementId")
    @Expose
    private Integer elementId;
    @SerializedName("elementName")
    @Expose
    private String elementName;
    @SerializedName("Price")
    @Expose
    private Integer price;
    @SerializedName("displayPrice")
    @Expose
    private String displayPrice;
    @SerializedName("customisationCategoryId")
    @Expose
    private Integer customisationCategoryId;
    @SerializedName("isSelected")
    @Expose
    private Integer isSelected;
    @SerializedName("isVeg")
    @Expose
    private Integer isVeg;

    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

    public Integer getCustomisationCategoryId() {
        return customisationCategoryId;
    }

    public void setCustomisationCategoryId(Integer customisationCategoryId) {
        this.customisationCategoryId = customisationCategoryId;
    }

    public Integer getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Integer isSelected) {
        this.isSelected = isSelected;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

}
