
package com.app.foodappuser.Model.Response.PastOrderResponse;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetails implements Serializable {

    @SerializedName("displayOrderId")
    @Expose
    private String displayOrderId;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("displayTotalItems")
    @Expose
    private String totalItems;
    @SerializedName("displayNetAmount")
    @Expose
    private String displayPrice;
    @SerializedName("userAddressType")
    @Expose
    private String userAddressType;
    @SerializedName("userAddress")
    @Expose
    private String userAddress;
    @SerializedName("isdeiveried")
    @Expose
    private Boolean isdeiveried;
    @SerializedName("deliveriedByAndTime")
    @Expose
    private String deliveriedByAndTime;
    @SerializedName("dishes")
    @Expose
    private List<Dish> dishes = null;
    @SerializedName("charges")
    @Expose
    private List<Charge> charges = null;
    @SerializedName("displayPaidVia")
    @Expose
    private String displayPaidVia;
    @SerializedName("displayValue")
    @Expose
    private String displayValue;
    @SerializedName("outletName")
    @Expose
    private String outletName;
    @SerializedName("outletAddress")
    @Expose
    private String outletAddress;

    public String getDisplayOrderId() {
        return displayOrderId;
    }

    public void setDisplayOrderId(String displayOrderId) {
        this.displayOrderId = displayOrderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public String getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

    public String getUserAddressType() {
        return userAddressType;
    }

    public void setUserAddressType(String userAddressType) {
        this.userAddressType = userAddressType;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public Boolean getIsdeiveried() {
        return isdeiveried;
    }

    public void setIsdeiveried(Boolean isdeiveried) {
        this.isdeiveried = isdeiveried;
    }

    public String getDeliveriedByAndTime() {
        return deliveriedByAndTime;
    }

    public void setDeliveriedByAndTime(String deliveriedByAndTime) {
        this.deliveriedByAndTime = deliveriedByAndTime;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public List<Charge> getCharges() {
        return charges;
    }

    public void setCharges(List<Charge> charges) {
        this.charges = charges;
    }

    public String getDisplayPaidVia() {
        return displayPaidVia;
    }

    public void setDisplayPaidVia(String displayPaidVia) {
        this.displayPaidVia = displayPaidVia;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getOutletAddress() {
        return outletAddress;
    }

    public void setOutletAddress(String outletAddress) {
        this.outletAddress = outletAddress;
    }

}
