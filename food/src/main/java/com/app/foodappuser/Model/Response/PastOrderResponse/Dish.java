
package com.app.foodappuser.Model.Response.PastOrderResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Dish implements Serializable {

    @SerializedName("dishplayDish")
    @Expose
    private String displayDish;
    @SerializedName("isVeg")
    @Expose
    private Integer isVeg;
    @SerializedName("displayPrice")
    @Expose
    private String displayPrice;

    public String getDisplayDish() {
        return displayDish;
    }

    public void setDisplayDish(String displayDish) {
        this.displayDish = displayDish;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public String getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

}
