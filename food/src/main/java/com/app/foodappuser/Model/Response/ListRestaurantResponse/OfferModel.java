
package com.app.foodappuser.Model.Response.ListRestaurantResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OfferModel {

    @SerializedName("offerType")
    @Expose
    private String offerType;
    @SerializedName("offerName")
    @Expose
    private String offerName;
    @SerializedName("isFavourite")
    @Expose
    private String isFavourite;

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(String isFavourite) {
        this.isFavourite = isFavourite;
    }

}
