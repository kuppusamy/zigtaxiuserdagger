
package com.app.foodappuser.Model.Response.ExploreResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListRestaurant {

    @SerializedName("restaurantId")
    @Expose
    private Integer restaurantId;
    @SerializedName("restaurantName")
    @Expose
    private String restaurantName;
    @SerializedName("restaurantImage")
    @Expose
    private String restaurantImage;
    @SerializedName("isPureVeg")
    @Expose
    private Integer isPureVeg;
    @SerializedName("displayCostForTwo")
    @Expose
    private String displayCostForTwo;
    @SerializedName("isExculsive")
    @Expose
    private Object isExculsive;
    @SerializedName("couponEnabledForRestaurant")
    @Expose
    private String couponEnabledForRestaurant;
    @SerializedName("cuisines")
    @Expose
    private String cuisines;
    @SerializedName("averageReview")
    @Expose
    private String averageReview;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("displayTime")
    @Expose
    private String displayTime;
    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    @SerializedName("longDescription")
    @Expose
    private String longDescription;
    @SerializedName("outlet")
    @Expose
    private List<Outlet> outlet = null;

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantImage() {
        return restaurantImage;
    }

    public void setRestaurantImage(String restaurantImage) {
        this.restaurantImage = restaurantImage;
    }

    public Integer getIsPureVeg() {
        return isPureVeg;
    }

    public void setIsPureVeg(Integer isPureVeg) {
        this.isPureVeg = isPureVeg;
    }

    public String getDisplayCostForTwo() {
        return displayCostForTwo;
    }

    public void setDisplayCostForTwo(String displayCostForTwo) {
        this.displayCostForTwo = displayCostForTwo;
    }

    public Object getIsExculsive() {
        return isExculsive;
    }

    public void setIsExculsive(Object isExculsive) {
        this.isExculsive = isExculsive;
    }

    public String getCouponEnabledForRestaurant() {
        return couponEnabledForRestaurant;
    }

    public void setCouponEnabledForRestaurant(String couponEnabledForRestaurant) {
        this.couponEnabledForRestaurant = couponEnabledForRestaurant;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public String getAverageReview() {
        return averageReview;
    }

    public void setAverageReview(String averageReview) {
        this.averageReview = averageReview;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }

    public List<Outlet> getOutlet() {
        return outlet;
    }

    public void setOutlet(List<Outlet> outlet) {
        this.outlet = outlet;
    }

}
