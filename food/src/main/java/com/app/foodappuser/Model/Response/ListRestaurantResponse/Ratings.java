package com.app.foodappuser.Model.Response.ListRestaurantResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Ratings implements Serializable {
    @SerializedName("orderId")
    @Expose
    private Integer orderId;
    @SerializedName("outletId")
    @Expose
    private Integer outletId;
    @SerializedName("deliveryStaffId")
    @Expose
    private Integer deliveryStaffId;
    @SerializedName("outletName")
    @Expose
    private String outletName;
    @SerializedName("deliverystaffName")
    @Expose
    private String deliverystaffName;
    @SerializedName("deliveredTime")
    @Expose
    private String deliveredTime;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getOutletId() {
        return outletId;
    }

    public void setOutletId(Integer outletId) {
        this.outletId = outletId;
    }

    public Integer getDeliveryStaffId() {
        return deliveryStaffId;
    }

    public void setDeliveryStaffId(Integer deliveryStaffId) {
        this.deliveryStaffId = deliveryStaffId;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getDeliverystaffName() {
        return deliverystaffName;
    }

    public void setDeliverystaffName(String deliverystaffName) {
        this.deliverystaffName = deliverystaffName;
    }

    public String getDeliveredTime() {
        return deliveredTime;
    }

    public void setDeliveredTime(String deliveredTime) {
        this.deliveredTime = deliveredTime;
    }
}
