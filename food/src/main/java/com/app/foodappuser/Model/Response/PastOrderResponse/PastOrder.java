
package com.app.foodappuser.Model.Response.PastOrderResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PastOrder implements Serializable {

    @SerializedName("displayNetAmount")
    @Expose
    private String displayPrice;
    @SerializedName("isdelivered")
    @Expose
    private Boolean isOrderDeliveried;
    @SerializedName("buttonName")
    @Expose
    private String buttonName;
    @SerializedName("displayDish")
    @Expose
    private String displayDishes;
    @SerializedName("displaydate")
    @Expose
    private String displayTime;
    @SerializedName("orderDetails")
    @Expose
    private OrderDetails orderDetails;
    @SerializedName("outletName")
    @Expose
    private String outletName;
    @SerializedName("outletLocation")
    @Expose
    private String outletLocation;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @SerializedName("orderId")
    @Expose
    private String orderId;

    public String getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

    public Boolean getIsOrderDeliveried() {
        return isOrderDeliveried;
    }

    public void setIsOrderDeliveried(Boolean isOrderDeliveried) {
        this.isOrderDeliveried = isOrderDeliveried;
    }

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getDisplayDishes() {
        return displayDishes;
    }

    public void setDisplayDishes(String displayDishes) {
        this.displayDishes = displayDishes;
    }

    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }

    public OrderDetails getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getOutletLocation() {
        return outletLocation;
    }

    public void setOutletLocation(String outletLocation) {
        this.outletLocation = outletLocation;
    }

}
