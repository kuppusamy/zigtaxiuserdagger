
package com.app.foodappuser.Model.Response.ListRestaurantResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BannerModel {

    @SerializedName("bannerId")
    @Expose
    private String bannerId;
    @SerializedName("bannerImage")
    @Expose
    private String bannerImage;
    @SerializedName("bannerStatus")
    @Expose
    private String bannerStatus;
    @SerializedName("outletId")
    @Expose
    private String outletsId;
    @SerializedName("outletName")
    @Expose
    private String outletName;


    public List<OutletModel> getOutlets() {
        return outlets;
    }

    public void setOutlets(List<OutletModel> outlets) {
        this.outlets = outlets;
    }

    @SerializedName("outlets")
    @Expose
    private List<OutletModel> outlets = null;

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getBannerStatus() {
        return bannerStatus;
    }

    public void setBannerStatus(String bannerStatus) {
        this.bannerStatus = bannerStatus;
    }

    public String getOutletsId() {
        return outletsId;
    }

    public void setOutletsId(String outletsId) {
        this.outletsId = outletsId;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

}
