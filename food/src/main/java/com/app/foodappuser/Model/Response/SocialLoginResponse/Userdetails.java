
package com.app.foodappuser.Model.Response.SocialLoginResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Userdetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("encrptedPassword")
    @Expose
    private Object encrptedPassword;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("otpNumber")
    @Expose
    private Object otpNumber;
    @SerializedName("deviceToken")
    @Expose
    private Object deviceToken;
    @SerializedName("os")
    @Expose
    private Object os;
    @SerializedName("udId")
    @Expose
    private Object udId;
    @SerializedName("loginType")
    @Expose
    private String loginType;
    @SerializedName("facebookToken")
    @Expose
    private Object facebookToken;
    @SerializedName("googleToken")
    @Expose
    private Object googleToken;
    @SerializedName("socialToken")
    @Expose
    private String socialToken;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("CurrentAddressId")
    @Expose
    private Integer currentAddressId;
    @SerializedName("latitude")
    @Expose
    private Object latitude;
    @SerializedName("longitude")
    @Expose
    private Object longitude;
    @SerializedName("refferalCode")
    @Expose
    private Object refferalCode;
    @SerializedName("stripeCustomerId")
    @Expose
    private String stripeCustomerId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Object getEncrptedPassword() {
        return encrptedPassword;
    }

    public void setEncrptedPassword(Object encrptedPassword) {
        this.encrptedPassword = encrptedPassword;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public Object getOtpNumber() {
        return otpNumber;
    }

    public void setOtpNumber(Object otpNumber) {
        this.otpNumber = otpNumber;
    }

    public Object getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(Object deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Object getOs() {
        return os;
    }

    public void setOs(Object os) {
        this.os = os;
    }

    public Object getUdId() {
        return udId;
    }

    public void setUdId(Object udId) {
        this.udId = udId;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public Object getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(Object facebookToken) {
        this.facebookToken = facebookToken;
    }

    public Object getGoogleToken() {
        return googleToken;
    }

    public void setGoogleToken(Object googleToken) {
        this.googleToken = googleToken;
    }

    public String getSocialToken() {
        return socialToken;
    }

    public void setSocialToken(String socialToken) {
        this.socialToken = socialToken;
    }

    public Object getOtp() {
        return otp;
    }

    public void setOtp(Object otp) {
        this.otp = otp;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Integer getCurrentAddressId() {
        return currentAddressId;
    }

    public void setCurrentAddressId(Integer currentAddressId) {
        this.currentAddressId = currentAddressId;
    }

    public Object getLatitude() {
        return latitude;
    }

    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    public Object getLongitude() {
        return longitude;
    }

    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

    public Object getRefferalCode() {
        return refferalCode;
    }

    public void setRefferalCode(Object refferalCode) {
        this.refferalCode = refferalCode;
    }

    public String getStripeCustomerId() {
        return stripeCustomerId;
    }

    public void setStripeCustomerId(String stripeCustomerId) {
        this.stripeCustomerId = stripeCustomerId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
