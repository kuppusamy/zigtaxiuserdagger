
package com.app.foodappuser.Model.Response.ViewCartResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Charge {

    @SerializedName("chargeName")
    @Expose
    private String chargeName;
    @SerializedName("displayCharge")
    @Expose
    private String displayCharge;

    public String getChargeName() {
        return chargeName;
    }

    public void setChargeName(String chargeName) {
        this.chargeName = chargeName;
    }

    public String getDisplayCharge() {
        return displayCharge;
    }

    public void setDisplayCharge(String displayCharge) {
        this.displayCharge = displayCharge;
    }

}
