package com.app.foodappuser.Model.Response.LiveTrackingResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrdersDetails {

    @SerializedName("orderId")
    @Expose
    private Integer orderId;
    @SerializedName("displayOrderId")
    @Expose
    private String displayOrderId;
    @SerializedName("orderItems")
    @Expose
    private Integer orderItems;
    @SerializedName("userLatitude")
    @Expose
    private String userLatitude;
    @SerializedName("userLongitude")
    @Expose
    private String userLongitude;
    @SerializedName("outletlatitude")
    @Expose
    private Double outletlatitude;
    @SerializedName("outletLongitude")
    @Expose
    private Double outletLongitude;

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    @SerializedName("eta")
    @Expose
    private String eta;

    public Double getDeliveryStaffLatitude() {
        return deliveryStaffLatitude;
    }

    public void setDeliveryStaffLatitude(Double deliveryStaffLatitude) {
        this.deliveryStaffLatitude = deliveryStaffLatitude;
    }

    public Double getDeliveryStaffLongitude() {
        return deliveryStaffLongitude;
    }

    public void setDeliveryStaffLongitude(Double deliveryStaffLongitude) {
        this.deliveryStaffLongitude = deliveryStaffLongitude;
    }

    @SerializedName("deliveryStaffLatitude")
    @Expose
    private Double deliveryStaffLatitude;
    public Boolean getOrderRejected() {
        return orderRejected;
    }

    public void setOrderRejected(Boolean orderRejected) {
        this.orderRejected = orderRejected;
    }

    @SerializedName("orderRejected")
    @Expose
    private Boolean orderRejected;
    @SerializedName("deliveryStaffLongitude")
    @Expose
    private Double deliveryStaffLongitude;
    @SerializedName("orderReceived")
    @Expose
    private Boolean orderReceived;
    @SerializedName("receivedTime")
    @Expose
    private String receivedTime;
    @SerializedName("orderConfirmed")
    @Expose
    private Boolean orderConfirmed;
    @SerializedName("orderConfirmedTime")
    @Expose
    private String orderConfirmedTime;
    @SerializedName("orderPickedUp")
    @Expose
    private Boolean orderPickedUp;
    @SerializedName("orderPickedupTime")
    @Expose
    private String orderPickedupTime;

    public Boolean getDeliveryboyAcceptedOrder() {
        return deliveryboyAcceptedOrder;
    }

    public void setDeliveryboyAcceptedOrder(Boolean deliveryboyAcceptedOrder) {
        this.deliveryboyAcceptedOrder = deliveryboyAcceptedOrder;
    }

    @SerializedName("deliveryboyAcceptedOrder")
    @Expose
    private Boolean deliveryboyAcceptedOrder;

    public Boolean getDeliveredStatus() {
        return deliveredStatus;
    }

    public void setDeliveredStatus(Boolean deliveredStatus) {
        this.deliveredStatus = deliveredStatus;
    }

    @SerializedName("deliveredStatus")
    @Expose
    private Boolean deliveredStatus;

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    @SerializedName("netAmount")
    @Expose
    private String netAmount;

    public String getRestaurantConfirmedDescription() {
        return restaurantConfirmedDescription;
    }

    public void setRestaurantConfirmedDescription(String restaurantConfirmedDescription) {
        this.restaurantConfirmedDescription = restaurantConfirmedDescription;
    }

    @SerializedName("restaurantConfirmedDescription")
    @Expose
    private String restaurantConfirmedDescription;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getDisplayOrderId() {
        return displayOrderId;
    }

    public void setDisplayOrderId(String displayOrderId) {
        this.displayOrderId = displayOrderId;
    }

    public Integer getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Integer orderItems) {
        this.orderItems = orderItems;
    }

    public String getUserLatitude() {
        return userLatitude;
    }

    public void setUserLatitude(String userLatitude) {
        this.userLatitude = userLatitude;
    }

    public String getUserLongitude() {
        return userLongitude;
    }

    public void setUserLongitude(String userLongitude) {
        this.userLongitude = userLongitude;
    }

    public Double getOutletlatitude() {
        return outletlatitude;
    }

    public void setOutletlatitude(Double outletlatitude) {
        this.outletlatitude = outletlatitude;
    }

    public Double getOutletLongitude() {
        return outletLongitude;
    }

    public void setOutletLongitude(Double outletLongitude) {
        this.outletLongitude = outletLongitude;
    }

    public Boolean getOrderReceived() {
        return orderReceived;
    }

    public void setOrderReceived(Boolean orderReceived) {
        this.orderReceived = orderReceived;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public Boolean getOrderConfirmed() {
        return orderConfirmed;
    }

    public void setOrderConfirmed(Boolean orderConfirmed) {
        this.orderConfirmed = orderConfirmed;
    }

    public String getOrderConfirmedTime() {
        return orderConfirmedTime;
    }

    public void setOrderConfirmedTime(String orderConfirmedTime) {
        this.orderConfirmedTime = orderConfirmedTime;
    }

    public Boolean getOrderPickedUp() {
        return orderPickedUp;
    }

    public void setOrderPickedUp(Boolean orderPickedUp) {
        this.orderPickedUp = orderPickedUp;
    }

    public String getOrderPickedupTime() {
        return orderPickedupTime;
    }

    public void setOrderPickedupTime(String orderPickedupTime) {
        this.orderPickedupTime = orderPickedupTime;
    }

}
