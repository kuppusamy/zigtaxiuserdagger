
package com.app.foodappuser.Model.Response.ViewCartResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DishCustomisation {

    @SerializedName("customizableId")
    @Expose
    private Integer customizableId;
    @SerializedName("customizableName")
    @Expose
    private String customizableName;
    @SerializedName("customizableType")
    @Expose
    private String customizableType;
    @SerializedName("customizableDishItems")
    @Expose
    private List<CustomizableDishItem> customizableDishItems = null;

    public Integer getCustomizableId() {
        return customizableId;
    }

    public void setCustomizableId(Integer customizableId) {
        this.customizableId = customizableId;
    }

    public String getCustomizableName() {
        return customizableName;
    }

    public void setCustomizableName(String customizableName) {
        this.customizableName = customizableName;
    }

    public String getCustomizableType() {
        return customizableType;
    }

    public void setCustomizableType(String customizableType) {
        this.customizableType = customizableType;
    }

    public List<CustomizableDishItem> getCustomizableDishItems() {
        return customizableDishItems;
    }

    public void setCustomizableDishItems(List<CustomizableDishItem> customizableDishItems) {
        this.customizableDishItems = customizableDishItems;
    }

}
