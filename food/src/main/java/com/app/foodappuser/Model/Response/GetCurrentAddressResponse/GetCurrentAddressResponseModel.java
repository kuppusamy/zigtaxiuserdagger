package com.app.foodappuser.Model.Response.GetCurrentAddressResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCurrentAddressResponseModel {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("currentAddress")
    @Expose
    private List<CurrentAddress> currentAddress = null;
    @SerializedName("isCurrentAddress")
    @Expose
    private Boolean isCurrentAddress;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<CurrentAddress> getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(List<CurrentAddress> currentAddress) {
        this.currentAddress = currentAddress;
    }

    public Boolean getIsCurrentAddress() {
        return isCurrentAddress;
    }

    public void setIsCurrentAddress(Boolean isCurrentAddress) {
        this.isCurrentAddress = isCurrentAddress;
    }

}