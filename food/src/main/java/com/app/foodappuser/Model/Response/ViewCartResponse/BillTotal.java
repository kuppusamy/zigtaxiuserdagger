
package com.app.foodappuser.Model.Response.ViewCartResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillTotal {

    @SerializedName("displayKey")
    @Expose
    private String displayKey;
    @SerializedName("displayValue")
    @Expose
    private String displayValue;
    @SerializedName("itemTotal")
    @Expose
    private double itemTotal;

    public Float getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Float netAmount) {
        this.netAmount = netAmount;
    }

    @SerializedName("netAmount")
    @Expose
    private Float netAmount;
    @SerializedName("topayAmount")
    @Expose
    private Integer topayAmount;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("percentage")
    @Expose
    private double percentage;
    @SerializedName("status")
    @Expose
    private Integer status;

    public String getDisplayKey() {
        return displayKey;
    }

    public void setDisplayKey(String displayKey) {
        this.displayKey = displayKey;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    public double getItemTotal() {
        return itemTotal;
    }

    public void setItemTotal(double itemTotal) {
        this.itemTotal = itemTotal;
    }

    public Integer getTopayAmount() {
        return topayAmount;
    }

    public void setTopayAmount(Integer topayAmount) {
        this.topayAmount = topayAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
