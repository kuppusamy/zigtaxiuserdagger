
package com.app.foodappuser.Model.Response.SocialLoginResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialLoginModel {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("isNewSocialUser")
    @Expose
    private Boolean isNewSocialUser;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("Userdetails")
    @Expose
    private Userdetails userdetails;
    @SerializedName("accessToken")
    @Expose
    private String accessToken;

    public Boolean getLogin() {
        return isLogin;
    }

    public void setLogin(Boolean login) {
        isLogin = login;
    }

    @SerializedName("isLogin")
    @Expose
    private Boolean isLogin;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Boolean getIsNewSocialUser() {
        return isNewSocialUser;
    }

    public void setIsNewSocialUser(Boolean isNewSocialUser) {
        this.isNewSocialUser = isNewSocialUser;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Userdetails getUserdetails() {
        return userdetails;
    }

    public void setUserdetails(Userdetails userdetails) {
        this.userdetails = userdetails;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}
