
package com.app.foodappuser.Model.Response.ListDishesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListDishesResponseModel {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
//    @SerializedName("cartDetails")
//    @Expose
//    private List<CartDetail> cartDetails = null;
    @SerializedName("dishes")
    @Expose
    private List<Dish> dishes = null;
//    @SerializedName("restaurantDetails")
//    @Expose
//    private List<RestaurantDetail> restaurantDetails = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

//    public List<CartDetail> getCartDetails() {
//        return cartDetails;
//    }

//    public void setCartDetails(List<CartDetail> cartDetails) {
//        this.cartDetails = cartDetails;
//    }

    public List<Dish> getDishes() {
        return dishes;
    }



    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

//    public List<RestaurantDetail> getRestaurantDetails() {
//        return restaurantDetails;
//    }

//    public void setRestaurantDetails(List<RestaurantDetail> restaurantDetails) {
//        this.restaurantDetails = restaurantDetails;
//    }

}
