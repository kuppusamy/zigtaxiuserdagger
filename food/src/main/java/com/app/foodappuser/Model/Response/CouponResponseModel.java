package com.app.foodappuser.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponResponseModel {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("couponStatus")
    @Expose
    private Boolean couponStatus;
    @SerializedName("couponName")
    @Expose
    private String couponName;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("message")
    @Expose
    private String message;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @SerializedName("amount")
    @Expose
    private String amount;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getCouponStatus() {
        return couponStatus;
    }

    public void setCouponStatus(Boolean couponStatus) {
        this.couponStatus = couponStatus;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}