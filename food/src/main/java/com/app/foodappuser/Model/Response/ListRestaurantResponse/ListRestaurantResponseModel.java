
package com.app.foodappuser.Model.Response.ListRestaurantResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListRestaurantResponseModel {

    @SerializedName("error")
    @Expose
    private Boolean error;

    public Boolean getRatingPending() {
        return ratingPending;
    }

    public void setRatingPending(Boolean ratingPending) {
        this.ratingPending = ratingPending;
    }

    @SerializedName("ratings")
    @Expose
    private List<Ratings> ratings;

    public List<Ratings> getRatings() {
        return ratings;
    }

    public void setRatings(List<Ratings> ratings) {
        this.ratings = ratings;
    }


    @SerializedName("ratingPending")
    @Expose
    private Boolean ratingPending;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("banners")
    @Expose
    private List<BannerModel> banners = null;
    @SerializedName("address")
    @Expose
    private List<Address> address = null;
    @SerializedName("restaurantCount")
    @Expose
    private String restaurantCount;
    @SerializedName("listRestaurants")
    @Expose
    private List<RestaurantModel> listRestaurents = null;
    @SerializedName("isServicable")
    @Expose
    private Boolean isServicable;
    @SerializedName("currentPage")
    @Expose
    private String currentPage;
    @SerializedName("pageCount")
    @Expose
    private Integer pageCount;

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<BannerModel> getBanners() {
        return banners;
    }

    public void setBanners(List<BannerModel> banners) {
        this.banners = banners;
    }

    public String getRestaurantCount() {
        return restaurantCount;
    }

    public void setRestaurantCount(String restaurantCount) {
        this.restaurantCount = restaurantCount;
    }

    public List<RestaurantModel> getListRestaurents() {
        return listRestaurents;
    }

    public void setListRestaurents(List<RestaurantModel> listRestaurents) {
        this.listRestaurents = listRestaurents;
    }

    public Boolean getIsServicable() {
        return isServicable;
    }

    public void setIsServicable(Boolean isServicable) {
        this.isServicable = isServicable;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

}
