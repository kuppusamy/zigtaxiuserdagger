package com.app.foodappuser.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgotPasswordResponseModel {
    @SerializedName("error")
    @Expose
    private Boolean error;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;

    public String getOtpNumber() {
        return otpNumber;
    }

    public void setOtpNumber(String otpNumber) {
        this.otpNumber = otpNumber;
    }
    @SerializedName("otpNumber")
    @Expose
    private String otpNumber;

    public String getTimeDelayForOtp() {
        return timeDelayForOtp;
    }

    public void setTimeDelayForOtp(String timeDelayForOtp) {
        this.timeDelayForOtp = timeDelayForOtp;
    }

    @SerializedName("timeDelayForOtp")
    @Expose
    private String timeDelayForOtp;

}
