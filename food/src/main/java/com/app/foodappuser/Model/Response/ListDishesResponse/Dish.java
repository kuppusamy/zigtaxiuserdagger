
package com.app.foodappuser.Model.Response.ListDishesResponse;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dish {

    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("isRecommended")
    @Expose
    private Integer isRecommended;
    @SerializedName("isHavingSubCategory")
    @Expose
    private Integer isHavingSubCategory;
    @SerializedName("categoryValues")
    @Expose
    private List<DishItems> categoryValues = null;
    @SerializedName("subCategoryValues")
    @Expose
    private List<SubCategoryValue> subCategoryValues = null;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getIsRecommended() {
        return isRecommended;
    }

    public void setIsRecommended(Integer isRecommended) {
        this.isRecommended = isRecommended;
    }

    public Integer getIsHavingSubCategory() {
        return isHavingSubCategory;
    }

    public void setIsHavingSubCategory(Integer isHavingSubCategory) {
        this.isHavingSubCategory = isHavingSubCategory;
    }

    public List<DishItems> getCategoryValues() {
        return categoryValues;
    }

    public List<DishItems> getVegOnlyValues() {
        List<DishItems> vegOnlyDish =new ArrayList<>();
        for (int i = 0; i <categoryValues.size() ; i++) {
            if(categoryValues.get(i).getIsVeg()==1){
                vegOnlyDish.add(categoryValues.get(i));
            }
        }
        return vegOnlyDish;
    }

    public void setCategoryValues(List<DishItems> categoryValues) {
        this.categoryValues = categoryValues;
    }

    public List<SubCategoryValue> getSubCategoryValues() {
        return subCategoryValues;
    }

    public void setSubCategoryValues(List<SubCategoryValue> subCategoryValues) {
        this.subCategoryValues = subCategoryValues;
    }

}