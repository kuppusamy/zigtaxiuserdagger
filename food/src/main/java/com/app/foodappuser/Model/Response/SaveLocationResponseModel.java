package com.app.foodappuser.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveLocationResponseModel {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    @SerializedName("addressId")
    @Expose
    private String addressId;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
