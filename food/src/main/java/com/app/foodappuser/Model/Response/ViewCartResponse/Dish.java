
package com.app.foodappuser.Model.Response.ViewCartResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dish {

    @SerializedName("dishId")
    @Expose
    private Integer dishId;
    @SerializedName("dishName")
    @Expose
    private String dishName;
    @SerializedName("dishPrice")
    @Expose
    private double dishPrice;
    @SerializedName("isVeg")
    @Expose
    private Integer isVeg;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("isAvailable")
    @Expose
    private Boolean isAvailable;
    @SerializedName("dishTotal")
    @Expose
    private double dishTotal;
    @SerializedName("displayDishTotal")
    @Expose
    private String displayDishTotal;
    @SerializedName("dishCustomisation")
    @Expose
    private List<DishCustomisation> dishCustomisation = null;
    @SerializedName("dishQuantity")
    @Expose
    private Integer dishQuantity;
    @SerializedName("cartId")
    @Expose
    private Integer cartId;
    @SerializedName("uuId")
    @Expose
    private String uuId;
    @SerializedName("isCustomizable")
    @Expose
    private Integer isCustomizable;

    public Integer getDishId() {
        return dishId;
    }

    public void setDishId(Integer dishId) {
        this.dishId = dishId;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public double getDishPrice() {
        return dishPrice;
    }

    public void setDishPrice(double dishPrice) {
        this.dishPrice = dishPrice;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public double getDishTotal() {
        return dishTotal;
    }

    public void setDishTotal(double dishTotal) {
        this.dishTotal = dishTotal;
    }

    public String getDisplayDishTotal() {
        return displayDishTotal;
    }

    public void setDisplayDishTotal(String displayDishTotal) {
        this.displayDishTotal = displayDishTotal;
    }

    public List<DishCustomisation> getDishCustomisation() {
        return dishCustomisation;
    }

    public void setDishCustomisation(List<DishCustomisation> dishCustomisation) {
        this.dishCustomisation = dishCustomisation;
    }

    public Integer getDishQuantity() {
        return dishQuantity;
    }

    public void setDishQuantity(Integer dishQuantity) {
        this.dishQuantity = dishQuantity;
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public String getUuId() {
        return uuId;
    }

    public void setUuId(String uuId) {
        this.uuId = uuId;
    }

    public Integer getIsCustomizable() {
        return isCustomizable;
    }

    public void setIsCustomizable(Integer isCustomizable) {
        this.isCustomizable = isCustomizable;
    }

}
