package com.app.foodappuser.Model.Response.ListDishesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DishItems {

    @SerializedName("dishId")
    @Expose
    private String dishId;



    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("dishName")
    @Expose
    private String dishName;

    @SerializedName("price")
    @Expose
    private double price;

    public String getSlashedPrice() {
        return slashedPrice;
    }

    public void setSlashedPrice(String slashedPrice) {
        this.slashedPrice = slashedPrice;
    }

    @SerializedName("slashedPrice")
    @Expose
    private String slashedPrice;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("quantity")
    @Expose
    private double quantity;
    @SerializedName("cuisineType")
    @Expose
    private String cuisineType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("availableFrom")
    @Expose
    private String availableFrom;

    @SerializedName("isAvailable")
    @Expose
    private Boolean isAvailable;

    @SerializedName("displayPrice")
    @Expose
    private String displayPrice;
    @SerializedName("availableTo")
    @Expose
    private String availableTo;
    @SerializedName("isHighlighted")
    @Expose
    private Integer isHighlighted;
    @SerializedName("highlightedValue")
    @Expose
    private String highlightedValue;
    @SerializedName("dishImage")
    @Expose
    private String dishImage;
    @SerializedName("isCustomizable")
    @Expose
    private Integer isCustomizable;
    @SerializedName("isVeg")
    @Expose
    private Integer isVeg;
    @SerializedName("dishCustomisation")
    @Expose
    private List<DishCustomization> dishCustomisation = null;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getCuisineType() {
        return cuisineType;
    }

    public void setCuisineType(String cuisineType) {
        this.cuisineType = cuisineType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvailableFrom() {
        return availableFrom;
    }

    public void setAvailableFrom(String availableFrom) {
        this.availableFrom = availableFrom;
    }

    public String getAvailableTo() {
        return availableTo;
    }

    public void setAvailableTo(String availableTo) {
        this.availableTo = availableTo;
    }

    public Integer getIsHighlighted() {
        return isHighlighted;
    }

    public void setIsHighlighted(Integer isHighlighted) {
        this.isHighlighted = isHighlighted;
    }

    public String getHighlightedValue() {
        return highlightedValue;
    }

    public void setHighlightedValue(String highlightedValue) {
        this.highlightedValue = highlightedValue;
    }

    public String getDishImage() {
        return dishImage;
    }

    public void setDishImage(String dishImage) {
        this.dishImage = dishImage;
    }

    public Integer getIsCustomizable() {
        return isCustomizable;
    }

    public void setIsCustomizable(Integer isCustomizable) {
        this.isCustomizable = isCustomizable;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public List<DishCustomization> getCustomizableElements() {
        return dishCustomisation;
    }

    public void setCustomizableElements(List<DishCustomization> dishCustomisation) {
        this.dishCustomisation = dishCustomisation;
    }

    public String getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

}
