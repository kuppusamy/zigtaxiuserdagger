package com.app.foodappuser.Model.Response.ListDeliveryResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListDeliveryResponseModel {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("listDeliveryAddress")
    @Expose
    private List<ListDeliveryAddress> listDeliveryAddress = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<ListDeliveryAddress> getListDeliveryAddress() {
        return listDeliveryAddress;
    }

    public void setListDeliveryAddress(List<ListDeliveryAddress> listDeliveryAddress) {
        this.listDeliveryAddress = listDeliveryAddress;
    }

}