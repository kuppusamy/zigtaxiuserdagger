package com.app.foodappuser.Firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.app.foodappuser.Application.AppController;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.view.activities.LiveTrackingActivity;
import com.app.foodappuser.view.activities.MainActivity;
import com.app.foodappuser.view.activities.SplashActivity;

import org.json.JSONObject;


public class FCMMsgService extends FirebaseMessagingService {
    private static final String TAG = FCMMsgService.class.getSimpleName();

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        new AppSettings(this).setFCMToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

//        Toast.makeText(this, "Notification Received", Toast.LENGTH_SHORT).show();
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Utils.log(TAG, "getData: " + remoteMessage.getData());
            JSONObject jsonObject = new JSONObject(remoteMessage.getData());
            sendNotification(jsonObject);
        }
        if (remoteMessage.getNotification() != null) {
            Utils.log(TAG, "getNotification: " + remoteMessage.getNotification());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void sendNotification(JSONObject jsonObject) {

        Intent intent = null;

        if (jsonObject.optString("notification_type").equalsIgnoreCase("assign")) {
            if (new AppSettings(FCMMsgService.this).getIsLoggedIn().equalsIgnoreCase("true")) {
                intent = new Intent(this, LiveTrackingActivity.class);
                intent.putExtra(ConstantKeys.INTENTKEYS.ORDER_ID, jsonObject.optString("orderId"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            } else {
                intent = new Intent(this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);

            }
        } else if (jsonObject.optString("notification_type").equalsIgnoreCase("accepted")) {
            if (new AppSettings(FCMMsgService.this).getIsLoggedIn().equalsIgnoreCase("true")) {
                intent = new Intent(this, LiveTrackingActivity.class);
                intent.putExtra(ConstantKeys.INTENTKEYS.ORDER_ID, jsonObject.optString("orderId"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ((AppController) getApplication())
                        .getRxBus()
                        .send("");
            } else {
                intent = new Intent(this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);

            }

        } else if (jsonObject.optString("notification_type").equalsIgnoreCase("pickedup")) {
            if (new AppSettings(FCMMsgService.this).getIsLoggedIn().equalsIgnoreCase("true")) {
                intent = new Intent(this, LiveTrackingActivity.class);
                intent.putExtra(ConstantKeys.INTENTKEYS.ORDER_ID, jsonObject.optString("orderId"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ((AppController) getApplication())
                        .getRxBus()
                        .send("");
            } else {
                intent = new Intent(this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);

            }

        } else if (jsonObject.optString("notification_type").equalsIgnoreCase("delivered")) {
            if (new AppSettings(FCMMsgService.this).getIsLoggedIn().equalsIgnoreCase("true")) {
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ((AppController) getApplication())
                        .getRxBus()
                        .send("delivered");
                Log.e(TAG, "sendNotification: "+"rxbus" );
//                intent.putExtra(ConstantKeys.INTENTKEYS.ORDER_ID, jsonObject.optString("orderId"));
            } else {
                intent = new Intent(this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);

            }

        } else if (jsonObject.optString("notification_type").equalsIgnoreCase("general")) {
            if (new AppSettings(FCMMsgService.this).getIsLoggedIn().equalsIgnoreCase("true")) {
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);

            } else {
                intent = new Intent(this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);

            }

        } else {
            intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        }

//        intent.putExtra(LOGIN_TYPE, HOME_TAB);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(jsonObject.optString("title"))
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        if (jsonObject.has("body")) {
            notificationBuilder.setContentText(jsonObject.optString("body"));
        } else {
            notificationBuilder.setContentText(jsonObject.optString("message"));

        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                String NOTIFICATION_CHANNEL_ID = "111";
                String NOTIFICATION_NAME = "foodapp";
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                        NOTIFICATION_NAME, importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.WHITE);
                notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }

}



