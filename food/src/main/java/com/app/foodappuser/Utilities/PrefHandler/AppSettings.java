package com.app.foodappuser.Utilities.PrefHandler;

import android.content.Context;
import android.util.Log;

import com.app.foodappuser.Model.RecentSearches.RestaurantName;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by yuvaraj on 25/04/18.
 */

public class AppSettings {
    Context context;
    String accessToken;
    String email;
    String username;
    String userid;
    String isLoggedIn;
    String lastKnownLatitude, lastKnownLongitude;
    String outletId, restaurentId;
    String mobileNumber;
    String termsAndConditions;
    String isWalkThroughPassed;
    String countryCode;
    String otpTimeOut;
    String isPasswordLoginEnabled;
    String isLatLongFetched;
    String isAppKilled;
    String apiMapKey;
    String currency;

    String loginType;



    Boolean isTwilioOtp;


    String token;
    String selectedCardName;
    boolean locationAvailable;

    String addressId;
    List<RestaurantName> searches;
    int selectedTab;
    String otp;

    public String getSelectedCardName() {
        selectedCardName = SharedPreference.getKey(context, "selectedCardName");
        return selectedCardName;
    }

    public void setSelectedCardName(String selectedCardName) {
        SharedPreference.putKey(context, "selectedCardName", selectedCardName);
        this.selectedCardName = selectedCardName;
    }

    public List<RestaurantName> getSearches() {
        Type type = new TypeToken<List<RestaurantName>>() {
        }.getType();
        searches = new Gson().fromJson(SharedPreference.getKey(context, "searches"), type);
        return searches;
    }

    public void setSearches(List<RestaurantName> searches) {
        SharedPreference.putKey(context, "searches", new Gson().toJson(searches));
        this.searches = searches;
    }

    public String getLastKnownLatitude() {
        lastKnownLatitude = SharedPreference.getKey(context, "lastKnownLatitude");

        return lastKnownLatitude;
    }

    public void setLastKnownLatitude(String lastKnownLatitude) {
        SharedPreference.putKey(context, "lastKnownLatitude", lastKnownLatitude);
        this.lastKnownLatitude = lastKnownLatitude;
    }

    public String getLastKnownLongitude() {
        lastKnownLongitude = SharedPreference.getKey(context, "lastKnownLongitude");

        return lastKnownLongitude;
    }

    public void setLastKnownLongitude(String lastKnownLongitude) {
        SharedPreference.putKey(context, "lastKnownLongitude", lastKnownLongitude);

        this.lastKnownLongitude = lastKnownLongitude;
    }

    public String getUdId() {
        udId = SharedPreference.getKey(context, "udId");

        return udId;
    }

    public void setUdId(String udId) {
        SharedPreference.putKey(context, "udId", udId);

        this.udId = udId;
    }

    String udId;

    public AppSettings(Context context) {
        this.context = context;
    }

    public String getIsLoggedIn() {
        isLoggedIn = SharedPreference.getKey(context, "isLoggedIn");
        return isLoggedIn;
    }

    public void setIsLoggedIn(String isLoggedIn) {
        SharedPreference.putKey(context, "isLoggedIn", isLoggedIn);
        this.isLoggedIn = isLoggedIn;
    }

    public String getAccessToken() {
        accessToken = SharedPreference.getKey(context, "accessToken");
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        SharedPreference.putKey(context, "accessToken", accessToken);
    }

    public String getEmail() {
        email = SharedPreference.getKey(context, "email");
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        SharedPreference.putKey(context, "email", email);
    }

    public String getUsername() {
        username = SharedPreference.getKey(context, "username");
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        SharedPreference.putKey(context, "username", username);

    }

    public String getUserid() {
        userid = SharedPreference.getKey(context, "userid");
        return userid;
    }

    public void setUserid(String userid) {
        SharedPreference.putKey(context, "userid", userid);
        this.userid = userid;
    }

    public String getOutletId() {
        outletId = SharedPreference.getKey(context, "outletId");
        return outletId;
    }

    public void setOutletId(String outletId) {
        SharedPreference.putKey(context, "outletId", outletId);
        this.outletId = outletId;
    }

    public String getRestaurentId() {
        outletId = SharedPreference.getKey(context, "restaurentId");
        return restaurentId;
    }

    public void setRestaurentId(String restaurentId) {
        SharedPreference.putKey(context, "restaurentId", restaurentId);
        this.restaurentId = restaurentId;
    }


    public String getIsPasswordLoginEnabled() {
        isPasswordLoginEnabled = SharedPreference.getKey(context, "isPasswordLoginEnabled");
        return isPasswordLoginEnabled;
    }

    public void setIsPasswordLoginEnabled(String isPasswordLoginEnabled) {
        SharedPreference.putKey(context, "isPasswordLoginEnabled", isPasswordLoginEnabled);

        this.isPasswordLoginEnabled = isPasswordLoginEnabled;
    }

    public String getMobileNumber() {
        mobileNumber = SharedPreference.getKey(context, "mobileNumber");

        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        SharedPreference.putKey(context, "mobileNumber", mobileNumber);
        this.mobileNumber = mobileNumber;
    }

    public String getCountryCode() {
        mobileNumber = SharedPreference.getKey(context, "countryCode");

        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        SharedPreference.putKey(context, "countryCode", countryCode);

        this.countryCode = countryCode;
    }

    public void setOtpTimeOut(String otpTimeOut) {
        SharedPreference.putKey(context, "otpTimeOut", otpTimeOut);

        this.otpTimeOut = otpTimeOut;
    }

    public String getOtpTimeOut() {
        otpTimeOut = SharedPreference.getKey(context, "otpTimeOut");

        return otpTimeOut;
    }

    public String getIsWalkThroughPassed() {
        isWalkThroughPassed = SharedPreference.getKey(context, "isWalkThroughPassed");
        return isWalkThroughPassed;
    }

    public void setIsWalkThroughPassed(String isWalkThroughPassed) {
        SharedPreference.putKey(context, "isWalkThroughPassed", isWalkThroughPassed);

        this.isWalkThroughPassed = isWalkThroughPassed;
    }

    public String getTermsAndConditions() {
        termsAndConditions = SharedPreference.getKey(context, "termsAndConditions");
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        SharedPreference.putKey(context, "termsAndConditions", termsAndConditions);
        this.termsAndConditions = termsAndConditions;
    }

    public String getIsLatLongFetched() {
        isLatLongFetched = SharedPreference.getKey(context, "isLatLongFetched");
        return isLatLongFetched;
    }

    public void setIsLatLongFetched(String isLatLongFetched) {
        SharedPreference.putKey(context, "isLatLongFetched", isLatLongFetched);
        this.isLatLongFetched = isLatLongFetched;
    }

    public String getIsAppKilled() {
        isAppKilled = SharedPreference.getKey(context, "isAppKilled");
        return isAppKilled;
    }

    public void setIsAppKilled(String isAppKilled) {
        SharedPreference.putKey(context, "isAppKilled", isAppKilled);
        this.isAppKilled = isAppKilled;
    }

    public String getOtp() {
        otp = SharedPreference.getKey(context, "otp");

        return otp;
    }

    public void setOtp(String otp) {
        SharedPreference.putKey(context, "otp", otp);

        this.otp = otp;
    }

    public String getApiMapKey() {
        apiMapKey = SharedPreference.getKey(context, "apiMapKey");
        return apiMapKey;
    }

    public void setApiMapKey(String apiMapKey) {
        SharedPreference.putKey(context, "apiMapKey", apiMapKey);
        this.apiMapKey = apiMapKey;
    }

    public String getCurrency() {
        currency = SharedPreference.getKey(context, "currency");
        return currency;
    }

    public void setCurrency(String currency) {
        SharedPreference.putKey(context, "currency", currency);
        this.currency = currency;
    }

    public String getAddressId() {
        addressId = SharedPreference.getKey(context, "addressId");
        return addressId;
    }

    public void setAddressId(String addressId) {
        SharedPreference.putKey(context, "addressId", addressId);

        this.addressId = addressId;
    }


    public int getSelectedTab() {
        selectedTab = Integer.parseInt(SharedPreference.getKey(context, "selectedTab"));
        return selectedTab;
    }

    public void setSelectedTab(int selectedTab) {
        Log.d("Appsettings", "onPageSelected: " + selectedTab);

        SharedPreference.putKey(context, "selectedTab", "" + selectedTab);
        this.selectedTab = selectedTab;
    }

    public String getFCMToken() {
        token = SharedPreference.getKey(context, "token");
        return token;
    }

    public void setFCMToken(String token) {
        SharedPreference.putKey(context, "token", token);
        this.token = token;
    }

    public boolean getLocationAvailable() {
        locationAvailable = SharedPreference.getBoolean(context, "locationAvailable");
        return locationAvailable;
    }

    public void setLocationAvailable(boolean locationAvailable) {
        SharedPreference.putBoolean(context, "locationAvailable", locationAvailable);
        this.locationAvailable = locationAvailable;
    }

    public String getLoginType() {
        loginType = SharedPreference.getKey(context, "loginType");
        return loginType;
    }

    public void setLoginType(String loginType) {
        SharedPreference.putKey(context, "loginType", loginType);
        this.loginType = loginType;
    }

    public Boolean getIsTwilioOtp() {
        isTwilioOtp = SharedPreference.getBoolean(context, "isTwilioOtp");
        return isTwilioOtp;
    }

    public void setIsTwilioOtp(Boolean isTwilioOtp) {
        SharedPreference.putBoolean(context, "isTwilioOtp", isTwilioOtp);
        this.isTwilioOtp = isTwilioOtp;
    }


}
