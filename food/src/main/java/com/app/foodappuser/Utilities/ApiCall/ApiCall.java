package com.app.foodappuser.Utilities.ApiCall;

import android.content.Context;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.foodappuser.Application.AppController;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.BaseUtils.ConnectionUtils;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.view.activities.SignInActivity;
import com.facebook.login.LoginManager;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ApiCall {

    private static String TAG = ApiCall.class.getSimpleName();
    private static int MY_SOCKET_TIMEOUT_MS = 5000;


    public static void PostMethod(InputForAPI input, final ResponseHandler volleyCallback) {
        final String url = input.getUrl();
        final Context context = input.getContext();
        JSONObject params = input.getJsonObject();
        final HashMap<String, String> headers = input.getHeaders();

        if (ConnectionUtils.isNetworkConnected(context)) {

            Utils.log(TAG, "url:" + url + "--input: " + params + "--headers: " + headers.toString());
            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);
                            if (response.optString("errorMessage").equalsIgnoreCase("Unauthenticated.")) {
                                logout(context);
                            } else {
                                volleyCallback.setDataResponse(response);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.log(TAG, "url:" + url + ", onErrorResponse: " + error);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.no_internet_connection));

                    } else if (error instanceof AuthFailureError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.authentication_error));

                    } else if (error instanceof ServerError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.server_error));

                    } else if (error instanceof NetworkError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.network_error));

                    } else if (error instanceof ParseError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.parse_error));

                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);

        } else {
            volleyCallback.setResponseError(context.getResources().getString(R.string.no_internet_connection));
        }
    }

    private static void logout(Context context) {
        AppSettings appSettings = new AppSettings(context);
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(context, gso);


//        if (appSettings.getLoginType().equalsIgnoreCase(context.getResources().getString(R.string.google))) {
//            mGoogleSignInClient.signOut()
//                    .addOnCompleteListener(context.getApplicationContext(), new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//                            appSettings.setIsLoggedIn(ConstantKeys.FALSE_STRING);
////                            accountViewModel.deleteAllTableValues();
//                            Intent intent = new Intent(context, SignInActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            context.startActivity(intent);
//                        }
//                    });
//        } else

        if (appSettings.getLoginType().equalsIgnoreCase(context.getResources().getString(R.string.facebook))) {
            LoginManager.getInstance().logOut();
            appSettings.setIsLoggedIn(ConstantKeys.FALSE_STRING);
//            accountViewModel.deleteAllTableValues();
            Intent intent = new Intent(context, SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
        } else {
            appSettings.setIsLoggedIn(ConstantKeys.FALSE_STRING);
//            accountViewModel.deleteAllTableValues();
            Intent intent = new Intent(context, SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
        }
    }

    public static void GetMethod(InputForAPI input, final ResponseHandler volleyCallback) {
        final String url = input.getUrl();
        final Context context = input.getContext();
        final HashMap<String, String> headers = input.getHeaders();
        if (ConnectionUtils.isNetworkConnected(context)) {
            Utils.log(TAG, "url:" + url + "--headers: " + headers.toString());

            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);

                            volleyCallback.setDataResponse(response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.log(TAG, "url:" + url + ", onErrorResponse: " + error);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.no_internet_connection));

                    } else if (error instanceof AuthFailureError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.authentication_error));

                    } else if (error instanceof ServerError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.server_error));

                    } else if (error instanceof NetworkError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.network_error));

                    } else if (error instanceof ParseError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.parse_error));

                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);

        } else {
            volleyCallback.setResponseError(context.getResources().getString(R.string.no_internet_connection));
        }

    }


    public interface ResponseHandler {

        public void setDataResponse(JSONObject response);

        public void setResponseError(String error);

    }

}
