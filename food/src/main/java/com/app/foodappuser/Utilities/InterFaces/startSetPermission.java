package com.app.foodappuser.Utilities.InterFaces;

import android.location.Location;



public interface startSetPermission {
    void setAfterPermission();
    void checkLocationSettings();
    Location getLocation();

}
