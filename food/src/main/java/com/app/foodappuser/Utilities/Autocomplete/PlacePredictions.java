package com.app.foodappuser.Utilities.Autocomplete;

import java.io.Serializable;
import java.util.ArrayList;


public class PlacePredictions implements Serializable {

    public String strLatitude = "";
    public String strLongitude = "";
    public String strLatLng = "";
    public String strAddress = "";

    public Boolean error;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String errorMessage;

    public ArrayList<PlaceAutoComplete> getPlaces() {
        return predictions;
    }

    public void setPlaces(ArrayList<PlaceAutoComplete> places) {
        this.predictions = places;
    }

    private ArrayList<PlaceAutoComplete> predictions;

}
