package com.app.foodappuser.Utilities.DialogUtils;

import android.app.Dialog;
import android.content.Context;

import com.app.foodappuser.Model.Response.CheckUserAvailabilityResponseModel;
import com.app.foodappuser.Model.Response.SignInResponseModelResponse.SignInResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.view.activities.ForgotPasswordActivity;
import com.app.foodappuser.view.activities.MainActivity;
import com.app.foodappuser.view.activities.SignUpActivity;
import com.app.foodappuser.view.activities.ValidateOtpActivity;
import com.app.foodappuser.ViewModel.SignInViewModel;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hbb20.CountryCodePicker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomBottomSheetDialog extends BottomSheetDialogFragment {
    private LayoutInflater layoutInflater;
    CustomTextInputEditText phoneNumber;
    CustomTextInputEditText password;
    TextView phoneNumberHint;
    LinearLayout passwordContainer;
    CardView loginButton;
    TextView forgotPassword;
    ProgressBar progressBar;
    CountryCodePicker countryCodePicker;
    private SignInViewModel signInActivityViewModel;
    private AppSettings appSettings;
    private boolean socialLogin;
    private CoordinatorLayout parnetLayout;
    private boolean fbLogin=false;
    private boolean googleLogin=false;
    private Bundle bundle=new Bundle();


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    Context context;
    int layout;
    private BottomSheetDialog dialog;
    private View view;
    private CoordinatorLayout parentLayout;
    GoogleSignInClient mGoogleSignInClient;

    public CustomBottomSheetDialog(Context context, int layout, CoordinatorLayout parentLayout, GoogleSignInClient mGoogleSignInClient) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.mGoogleSignInClient=mGoogleSignInClient;
//        this.parentLayout=parentLayout;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(layout, container, false);
        signInActivityViewModel = ViewModelProviders.of(this).get(SignInViewModel.class);
        appSettings = new AppSettings(getActivity());

//        initialize();
        phoneNumber = view.findViewById(R.id.phoneNumber);
        parnetLayout = view.findViewById(R.id.parnetLayout);
        progressBar = view.findViewById(R.id.progressBar);
        phoneNumberHint = view.findViewById(R.id.phoneNumberHint);
        passwordContainer = view.findViewById(R.id.passwordContainer);
        password = view.findViewById(R.id.password);
        countryCodePicker = view.findViewById(R.id.countryCodePicker);
        forgotPassword = view.findViewById(R.id.forgotPassword);
        loginButton = view.findViewById(R.id.loginButton);


        passwordContainer.setVisibility(View.GONE);
        phoneNumber.requestFocus();

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToForgotPasswordActivity();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (passwordContainer.getVisibility() != View.VISIBLE) {
                    checkForMobileNumber();
                } else {
                    Utils.hideKeyboardDialog(getActivity(), password);
                    login();
                }
            }
        });

        phoneNumber.setKeyImeChangeListener(new CustomTextInputEditText.KeyImeChange() {
            @Override
            public void onKeyIme(int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    phoneNumber.setText("");
                    phoneNumber.clearFocus();
                    dismiss();

                }
            }
        });
        phoneNumber.addTextChangedListener(new TextWatcher() {
            int count;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                count = i2;
                String totalValue = phoneNumber.getText().toString().replaceAll(" ", "");
                if (totalValue.length() > 6) {
                    loginButton.setClickable(true);
                    loginButton.setEnabled(true);
                    loginButton.setAlpha(1);
                } else {
                    loginButton.setClickable(false);
                    loginButton.setEnabled(false);
                    loginButton.setAlpha((float) 0.5);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    public void placeSecondLayout() {
        phoneNumber.setEnabled(false);
        countryCodePicker.setEnabled(false);
        passwordContainer.setVisibility(View.VISIBLE);
        phoneNumber.clearFocus();
        password.setEnabled(true);
        password.requestFocus();
        Utils.showKeyboard(getActivity(), password);
    }

    public void showBottomSheetSocialLoginSignup() {
        socialLogin = true;
    }

    public void refreshSheet() {
        phoneNumber.setEnabled(true);
        countryCodePicker.setEnabled(true);
        phoneNumber.setText("");
        passwordContainer.setVisibility(View.GONE);
        phoneNumber.requestFocus();
    }


    public void initialize() {
        view = layoutInflater.inflate(layout, null);
        dialog = new BottomSheetDialog(context);
        dialog.setContentView(view);
//        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    public View getView() {
        return view;
    }

    public BottomSheetDialog getDialog() {
        return dialog;
    }


    private void moveToRegisterActivity() {
        Intent intent = new Intent(getActivity(), SignUpActivity.class);
        intent.putExtra(ConstantKeys.MOBILE_NUMBER, phoneNumber.getText().toString());
        intent.putExtra(ConstantKeys.COUNTRY_CODE, countryCodePicker.getSelectedCountryCodeWithPlus());
        startActivity(intent);
        refreshSheet();

    }

    private void moveToForgotPasswordActivity() {

        Intent intent = new Intent(getActivity(), ForgotPasswordActivity.class);
        intent.putExtra(ConstantKeys.MOBILE_NUMBER, phoneNumber.getText().toString());
        intent.putExtra(ConstantKeys.COUNTRY_CODE, countryCodePicker.getSelectedCountryCodeWithPlus());
        startActivity(intent);
    }

    private void moveToValidateOtpActivity(int otpNumber) {
        Intent intent = new Intent(getActivity(), ValidateOtpActivity.class);
        intent.putExtra(ConstantKeys.MOBILE_NUMBER, phoneNumber.getText().toString());
        intent.putExtra(ConstantKeys.COUNTRY_CODE, countryCodePicker.getSelectedCountryCodeWithPlus());
        intent.putExtra(ConstantKeys.OTP_NUMBER, String.valueOf(otpNumber));
        intent.putExtras(bundle);
        startActivity(intent);
        refreshSheet();
    }

    private void login() {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ConstantKeys.MOBILE_NUMBER, phoneNumber.getText().toString());
            jsonObject.put(ConstantKeys.COUNTRY_CODE, countryCodePicker.getSelectedCountryCodeWithPlus());
            jsonObject.put(ConstantKeys.PASSWORD, password.getText().toString());
            jsonObject.put(ConstantKeys.UDID, password.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.USER_LOGIN);
        inputForAPI.setJsonObject(jsonObject);

        signInActivityViewModel.login(inputForAPI).observe(this, new Observer<SignInResponseModel>() {
            @Override
            public void onChanged(@Nullable SignInResponseModel signInResponseModel) {
                signInResponseModel = signInResponseModel;
                progressBar.setVisibility(View.GONE);
                if (signInResponseModel.getError()) {
                    Utils.showToast(signInResponseModel.getErrorMessage(),getActivity());

                } else {
                    appSettings.setLoginType("normal");
                    appSettings.setAccessToken(ConstantKeys.BEARER + signInResponseModel.getAccessToken());
                    appSettings.setEmail(signInResponseModel.getUserdetails().getEmail());
                    appSettings.setUserid(String.valueOf(signInResponseModel.getUserdetails().getId()));
                    appSettings.setUsername(signInResponseModel.getUserdetails().getUserName());
                    appSettings.setMobileNumber(signInResponseModel.getUserdetails().getMobileNumber());
                    appSettings.setIsLoggedIn(ConstantKeys.TRUE_STRING);
                    moveToMainActivity();
                }
            }
        });


    }

    private void checkForMobileNumber() {

        progressBar.setVisibility(View.VISIBLE);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ConstantKeys.MOBILE_NUMBER, phoneNumber.getText().toString());
            jsonObject.put(ConstantKeys.COUNTRY_CODE, countryCodePicker.getSelectedCountryCodeWithPlus());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.CHECK_MOBILE_NUMBER);
        inputForAPI.setJsonObject(jsonObject);

        signInActivityViewModel.checkMobileNumber(inputForAPI).observe(this, new Observer<CheckUserAvailabilityResponseModel>() {
            @Override
            public void onChanged(@Nullable CheckUserAvailabilityResponseModel checkUserAvailabilityResponseModel) {
                progressBar.setVisibility(View.GONE);

                if (checkUserAvailabilityResponseModel.getError()) {
                    Utils.hideKeyboardDialog(getActivity(), phoneNumber);

                    Utils.showToast(checkUserAvailabilityResponseModel.getErrorMessage(),getActivity());

                } else {

                    appSettings.setOtpTimeOut(String.valueOf(checkUserAvailabilityResponseModel.getTimeDelayForOtp() * 1000));
                    appSettings.setOtp(String.valueOf(checkUserAvailabilityResponseModel.getOtpNumber()));

                    if (checkUserAvailabilityResponseModel.getNewUser()) {
                        //Create a new User and validate otp

                        moveToValidateOtpActivity(checkUserAvailabilityResponseModel.getOtpNumber());
                    } else {
                        if (socialLogin) {
                            Toast.makeText(getActivity(), "Mobile number already exists", Toast.LENGTH_SHORT).show();
                        } else {
                            //Login with otp or password
                            if (appSettings.getIsPasswordLoginEnabled().equalsIgnoreCase(ConstantKeys.TRUE_STRING)) {
                                //Login with Password
                                placeSecondLayout();
                            } else {
                                //Login with Otp
                                moveToValidateOtpActivity(checkUserAvailabilityResponseModel.getOtpNumber());
                            }
                        }
                    }
                }
            }
        });
    }

    private void moveToMainActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        refreshSheet();
        dismiss();
    }


    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        if(socialLogin){
            socialLogin = false;
            if(googleLogin){
                mGoogleSignInClient.revokeAccess()
                        .addOnCompleteListener( getActivity(), new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                // ...
                                googleLogin=false;

                            }
                        });
            }else if(fbLogin){
                LoginManager.getInstance().logOut();
                fbLogin=false;
            }
        }
        refreshSheet();

    }

    public void setGoogleLogin() {
        googleLogin=true;
    }

    public void setFaceBookLogin() {
        fbLogin=true;
    }

    public void setBundle(Bundle bundle) {
        this.bundle=bundle;
    }
}
