package com.app.foodappuser.Utilities.InterFaces;

public interface PaymentAdapterInterface {
    void onPaymentClicked(int id,int position);
    void onCardSelection();
}
