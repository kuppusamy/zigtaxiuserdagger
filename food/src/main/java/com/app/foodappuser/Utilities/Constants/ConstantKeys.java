package com.app.foodappuser.Utilities.Constants;

public class ConstantKeys {
    public static final String FROM_ACTIVITY = "fromActivity";
    public static final String REASON_INTENT = "REASON_INTENT";
    public static final String ORDER_ID = "orderId";
    public static final String DEVICE_TOKEN = "deviceToken";
    public static final String OS_TYPE = "osType";
    public static String RATING = "rating";
    public static String STAFF_ID = "staffId";
    public static String SUGGESTIONS = "orderSuggestions";
    public static String CART_ID = "cartId";
    public static String TOKEN = "token";
    public static String CITY = "city";
    public static String RESTAURANT_NAME = "restaurantName";
    public static String OK = "ok";
    public static String STATUS = "status";
    public static String BEARER = "Bearer ";
    public static String USER_NAME = "userName";
    public static String PASSWORD = "password";
    public static String HEADING = "heading";
    public static String URL = "url";
    public static String MOBILE_NUMBER = "mobileNumber";
    public static String SOCIAL_TOKEN = "socialToken";
    public static String SOCIAL_DATAS = "socialDatas";
    public static String SOCIAL_LOGIN = "SOCIALLOGIN";
    public static String NORMAL = "NORMAL";
    public static String LOGIN_TYPE = "loginType";
    public static String REFERRAL_CODE = "refferalCode";
    public static String EMAIL = "email";
    public static String USER_ID = "userId";
    public static String COUNTRY_CODE = "countryCode";
    public static String OTP = "otp";
    public static String OTP_NUMBER = "otpNumber";
    public static String ACCESS_TOKEN = "access_token";
    public static String AUTHORIZATION = "Authorization";
    public static String BANNERS = "Banners";
    public static String BANNER_IMAGE = "bannerImage";
    public static String CURRENT_LATITUDE = "currentLatitude";
    public static String LATITUDE = "latitude";
    public static String LONGITUDE = "longitude";
    public static String LAT = "lat";
    public static String LONG = "lng";
    public static String CURRENT_LONGITUDE = "currentLongitude";
    public static String IS_SAVED_ADDRESS = "isSavedAddress";
    public static String ADDRESS_ID = "addressId";
    public static String RESTAURANT_LIST = "ListRestaurents";
    public static String UDID = "udId";
    public static String CITY_NAME = "cityName";
    public static String COUNTRY_NAME = "countryName";
    public static String POSTAL_CODE = "postalCode";
    public static String STATE_NAME = "stateName";
    public static String CITY_LOCATION = "cityLocation";
    public static String LOCATION = "location";
    public static String OUTLET_ID = "outletId";
    public static String ORDER_RATING = "orderRating";
    public static String DELIVERY_RATING = "deliveryRating";
    public static String FEEDBACK = "feedback";
    public static String RESTAURANT_ID = "restaurantId";
    public static String DISH_ID = "dishId";
    public static String QUANTITY = "quantity";
    public static String ITEM_PRICE = "itemPrice";
    public static String DISH_CUSTOMISATION_ID = "dishCustomisationId";
    public static String TRUE = "1";
    public static String FALSE = "0";
    public static String ERROR_STRING = "error";
    public static String ERROR_MESSAGE_STRING = "errorMessage";
    public static String SUCCESS_STRING = "success";
    public static String FAILED_STRING = "failed";
    public static String TRUE_STRING = "true";
    public static String FALSE_STRING = "false";
    public static String FORMATED_ADDRESS = "formatedAddress";
    public static String HOUSE_FLAT_NO = "houseFlatNo";
    public static String LANDMARK = "landMark";
    public static String TYPE = "type";
    public static String HOME = "Home";
    public static String WORK = "Work";
    public static String OTHER = "Other";
    public static String COUPON_CODE = "couponCode";
    public static String EDIT_ADDRESS = "edit_address";
    public static String DISHES = "dishes";
    public static String DELIVER_ADDRESS_ID = "deliveryAddressId";
    public static String NET_AMOUNT = "netAmount";
    public static String PAYMENT_TYPE = "paymentType";
    public static String COUPON_NAME = "couponName";
    public static String DISCOUNT = "discount";


    public static class INTENTKEYS {
        public static String ADDRESS_LOCATION = "address_location";
        public static String ADDRESS_TYPE = "address_type";
        public static String LANDMARK = "landmark";
        public static String HOUSE_FLAT_NO = "house_no";
        public static String ADDRESS_LIST = "address_list";
        public static String LATITUDE = "latitude";
        public static String LONGITUDE = "longitude";
        public static String NAME = "name";
        public static String LOCATION = "location";
        public static String FROM_ACTIVITY = "fromActivity";
        public static String REASON_INTENT = "reason";
        public static String OUTLET_ID = "outletId";
        public static String OUTLET_NAME = "outletName";
        public static String CUISINE_NAME = "cuisineName";
        public static String OUTLET_MIN = "outletMins";
        public static String OUTLET_REVIEW = "outletReview";
        public static String OUTLET_COST_FOR_TWO = "costForTwo";
        public static String COUPON_ENABLED = "couponEnabled";
        public static String COUPON_LONG_DESCRIPTION = "couponLongDescription";
        public static String ADDRESS_ID = "address_id";
        public static String DETAILS = "details";
        public static String ORDER_ID = "orderId";
    }

    public static class RESTAURANTSOUTLET{
        public static String COUPON_FOR_ENABLE_TRUE = "1";
        public static String COUPON_FOR_ENABLE_FALSE = "0";
    }
}
