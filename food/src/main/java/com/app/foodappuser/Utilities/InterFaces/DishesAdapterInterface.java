package com.app.foodappuser.Utilities.InterFaces;

import com.app.foodappuser.Model.Response.ListDishesResponse.DishItems;

public interface DishesAdapterInterface {
    void onIncreased(DishItems dishItem,onDatabaseAdded onDatabaseAdded);
    void onDecreased(DishItems dishItem,boolean isCustomizable,onDatabaseAdded onDatabaseAdded);
    void onCustomization(DishItems items,onDatabaseAdded onDatabaseAdded);
}
