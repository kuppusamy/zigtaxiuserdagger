package com.app.foodappuser.Utilities;

import android.location.Location;

public interface NearMeFragmentLogicInterface {
    void onLocationFetched(Location location);
    void onLocationFailed();
    void callRestaurantApi();
}
