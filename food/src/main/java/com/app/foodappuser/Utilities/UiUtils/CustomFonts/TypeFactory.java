package com.app.foodappuser.Utilities.UiUtils.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by rujul on 2/5/2016.
 */
public class TypeFactory {

    final String ONE_NORMAL="fonts/Aktivgrotesk.ttf"; //AktivGrotesk.ttf
    final String ONE_MEDIUM="fonts/Aktivgrotesk-medium.ttf"; //Aktivgrotesk-medium.ttf
    final String ONE_BOLD="fonts/AktivgroteskBold.ttf";//AktivgroteskBold.ttf
    final String TWO_BOLD="fonts/Multi-Bold.ttf";//Multi-Bold.ttf
    final String TWO_REGULAR="fonts/Multi-Regular.ttf";//Multi-Regular.ttf
    final String TWO_SEMIBOLD="fonts/Multi-SemiBold.ttf";//Multi-SemiBold.ttf
    final String THREE_REGULAR="fonts/Archivo-Regular.ttf";//Archivo-Regular.ttf
    final String FOUR_SEMIBOLD="fonts/Raleway-SemiBold.ttf";//Raleway-SemiBold.ttf



    Typeface oneNormal;
    Typeface oneMedium;
    Typeface oneBold;
    Typeface twoBold;
    Typeface twoRegular;
    Typeface twoSemiBold;
    Typeface threeRegular;
    Typeface fourSemiBold;

    public TypeFactory(Context context){
        oneNormal = Typeface.createFromAsset(context.getAssets(),ONE_NORMAL);
        oneMedium = Typeface.createFromAsset(context.getAssets(),ONE_MEDIUM);
        oneBold = Typeface.createFromAsset(context.getAssets(),ONE_BOLD);
        twoBold = Typeface.createFromAsset(context.getAssets(),TWO_BOLD);
        twoRegular = Typeface.createFromAsset(context.getAssets(),TWO_REGULAR);
        twoSemiBold = Typeface.createFromAsset(context.getAssets(),TWO_SEMIBOLD);
        threeRegular = Typeface.createFromAsset(context.getAssets(),THREE_REGULAR);
        fourSemiBold = Typeface.createFromAsset(context.getAssets(),FOUR_SEMIBOLD);
    }

    public Typeface getOneNormal() {
        return oneNormal;
    }

    public Typeface getOneMedium() {
        return oneMedium;
    }

    public Typeface getOneBold() {
        return oneBold;
    }

    public Typeface getTwoBold() {
        return twoBold;
    }

    public Typeface getTwoRegular() {
        return twoRegular;
    }

    public Typeface getTwoSemiBold() {
        return twoSemiBold;
    }

    public Typeface getThreeRegular() {
        return threeRegular;
    }

    public Typeface getFourSemiBold() {
        return fourSemiBold;
    }


}
