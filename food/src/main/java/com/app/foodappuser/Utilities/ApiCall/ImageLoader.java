package com.app.foodappuser.Utilities.ApiCall;

import android.content.Context;
import android.widget.ImageView;

import com.app.foodappuser.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class ImageLoader {
    public Context context;

    public ImageLoader(Context contextvalue) {
        context = contextvalue;
    }

    public void load(String url, ImageView imageView) {
        Glide.with(context).load(url).apply(getRequestOptions()).into(imageView);
    }

    public static RequestOptions getRequestOptions() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.feed_placeholder);
        requestOptions.error(R.drawable.feed_placeholder);
        return requestOptions;
    }
}
