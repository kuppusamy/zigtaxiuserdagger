package com.app.foodappuser.Utilities.InterFaces;

public interface OutletAdapterInterface {
    void onClick();
    void setRecentSearches(String outletName, String outletId, String cuisines, String displayTime, String averageReview, String displayCostForTwo, String couponEnabledForRestaurant, String longDescription);
}
