package com.app.foodappuser.Utilities.InterFaces;

public interface ViewCartDishesAdapterInterface {
    void onIncreased(int cartId,int quantity,String id);
    void onDecreased(int cartId,int quantity,String id);
    void onDeleted(int cartId,int quantity,String id);
}
