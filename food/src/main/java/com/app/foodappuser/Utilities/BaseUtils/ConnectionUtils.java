package com.app.foodappuser.Utilities.BaseUtils;

import android.content.Context;
import android.net.ConnectivityManager;

public class ConnectionUtils {

    public static String NO_CONNECTION="4";

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            return cm.getActiveNetworkInfo() != null;
        }else{
            return false;
        }
    }
}
