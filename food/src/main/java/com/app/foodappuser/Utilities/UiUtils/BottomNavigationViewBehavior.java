package com.app.foodappuser.Utilities.UiUtils;

import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;

public class BottomNavigationViewBehavior extends CoordinatorLayout.Behavior<BottomNavigationView> {

    private int height;
    float oldX = 0;
    float oldY = 0;
    float newX = 0;
    float newY = 0;

    @Override
    public boolean onLayoutChild(CoordinatorLayout parent, BottomNavigationView child, int layoutDirection) {
        height = child.getHeight();
        return super.onLayoutChild(parent, child, layoutDirection);
    }

    @Override
    public boolean onTouchEvent(@NonNull CoordinatorLayout parent, @NonNull BottomNavigationView child, @NonNull MotionEvent event) {


        if(event.getAction() == MotionEvent.ACTION_DOWN) {

            oldX = event.getX();
            oldY = event.getY();
            slideDown(child);

            //start timer

        } else if (event.getAction() == MotionEvent.ACTION_UP) {

            //long timerTime = getTime between two event down to Up
            newX = event.getX();
            newY = event.getY();
            slideUp(child);


            float distance = (float) Math.sqrt((newX - oldX) * (newX - oldX) + (newY - oldY) * (newY - oldY));
            float speed = distance / 200;
//            Log.e("Touch", "onTouchEvent: "+speed );

        }



        return  super.onTouchEvent(parent, child, event);

    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout,
                                       BottomNavigationView child, @NonNull
                                               View directTargetChild, @NonNull View target,
                                       int axes, int type) {
//        Log.e("Touch", "onTouchEvent: "+axes+" ==> "+type );

        return axes == ViewCompat.SCROLL_AXIS_VERTICAL;
    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull BottomNavigationView child,
                               @NonNull View target, int dxConsumed, int dyConsumed,
                               int dxUnconsumed, int dyUnconsumed,
                               @ViewCompat.NestedScrollType int type) {
       /* if (dyConsumed > 0) {
            slideDown(child);
        } else if (dyConsumed < 0) {
            slideUp(child);
        }*/
    }

    private void slideUp(BottomNavigationView child) {
        child.clearAnimation();
        child.animate().translationY(0).setDuration(100);
    }

    private void slideDown(BottomNavigationView child) {
        child.clearAnimation();
        child.animate().translationY(height).setDuration(100);
    }
}
