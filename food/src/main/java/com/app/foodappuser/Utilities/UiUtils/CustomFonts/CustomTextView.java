package com.app.foodappuser.Utilities.UiUtils.CustomFonts;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.app.foodappuser.R;

/**
 * Created by rujul on 2/5/2016.
 */
public class CustomTextView extends androidx.appcompat.widget.AppCompatTextView {

    private int typefaceType;

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);
        try {
            typefaceType = array.getInteger(R.styleable.CustomTextView_font_name, 0);
        } finally {
            array.recycle();
        }
        if (!isInEditMode()) {

            setTypeface(CustomApp.getApp().getTypeFace(typefaceType));
        }
    }
}
