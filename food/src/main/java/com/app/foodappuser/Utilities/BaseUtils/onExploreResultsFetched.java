package com.app.foodappuser.Utilities.BaseUtils;

import com.app.foodappuser.Model.Response.ExploreResponse.ExploreResponseModel;
import com.app.foodappuser.Model.Response.ListRestaurantResponse.ListRestaurantResponseModel;
import com.app.foodappuser.ViewModel.ExploreViewModel;

public class onExploreResultsFetched {

    Boolean mvalue;
    ExploreResponseModel mlistRestaurantResponseModel;

    public Boolean getMvalue() {
        return mvalue;
    }

    public ExploreResponseModel getMlistRestaurantResponseModel() {
        return mlistRestaurantResponseModel;
    }

    public String getMtypedTet() {
        return mtypedTet;
    }

    String mtypedTet;

    public ExploreViewModel getExploreViewModel() {
        return exploreViewModel;
    }

    ExploreViewModel exploreViewModel;

    public onExploreResultsFetched(Boolean value, ExploreResponseModel listRestaurantResponseModel, String typedTet, ExploreViewModel exploreViewModel) {
        this.mvalue = value;
        this.mlistRestaurantResponseModel = listRestaurantResponseModel;
        this.mtypedTet = typedTet;
        this.exploreViewModel=exploreViewModel;
//        setValue(value);
//        setListRestaurantResponseModel(listRestaurantResponseModel);
//        setTypedTet(typedTet);
    }

}
