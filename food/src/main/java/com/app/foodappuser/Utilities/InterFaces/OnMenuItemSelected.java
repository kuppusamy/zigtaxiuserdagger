package com.app.foodappuser.Utilities.InterFaces;

public interface OnMenuItemSelected {
    void onItemSelected(String categoryId,int position);
}
