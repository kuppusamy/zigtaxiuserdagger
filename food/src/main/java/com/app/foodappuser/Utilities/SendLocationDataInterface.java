package com.app.foodappuser.Utilities;

import android.location.Location;

public interface SendLocationDataInterface {
    void onLocationFetched(Location location);
    void onLocationFailed();
}
