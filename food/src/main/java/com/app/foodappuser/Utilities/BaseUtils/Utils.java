package com.app.foodappuser.Utilities.BaseUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Build;
import androidx.annotation.RequiresApi;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.app.foodappuser.Application.AppController.getContext;

public class Utils {


    private static final String TAG = Utils.class.getSimpleName();

    public static String getString(Context context, int id) {
        return context.getResources().getString(id);
    }

    public static String getFormattedString(Context context, int id, String placeHolderText) {
        return context.getResources().getString(id, placeHolderText);
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideKeyboardDialog(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);


        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    public static void setStatusBarColorWhite(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    public static void setStatusBarColorBlue(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.blue_color));
        }
    }


    public static boolean isGpsEnabled(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;


        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();

        }

        return gps_enabled;

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static final void setSystemBarTheme(final Activity pActivity, final boolean pIsDark) {
        // Fetch the current flags.
        final int lFlags = pActivity.getWindow().getDecorView().getSystemUiVisibility();
        // Update the SystemUiVisibility dependening on whether we want a Light or Dark theme.
        pActivity.getWindow().getDecorView().setSystemUiVisibility(pIsDark ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR) : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));
    }

    public static String getCombinedStrings(String... strings) {
        StringBuilder finalString = new StringBuilder();
        for (String string : strings) {
            finalString.append(string);
        }
        return finalString.toString();
    }


    public static void log(String tag, String s) {
//        if (BuildConfig.DEBUG) {
            Log.d(tag, "log: " + s);
//        }
    }
    public static void loge(String tag, String s) {
//        if (BuildConfig.DEBUG) {
            Log.e(tag, "log: " + s);
//        }
    }

    public static void showSnackBar(View parentLayout, String message) {

        Snackbar mySnackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);

        View snackbarView = mySnackbar.getView();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            snackbarView.setElevation(30);
//        }

//        TextView tv = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
//        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Muli-Regular.ttf");
//        tv.setTypeface(font);

        mySnackbar.show();
    }
    public static void showToast(String message,Context context) {

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public static boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static JSONObject getCityAndNameAddressLine(JSONObject googleMapJson) throws JSONException {


        if (googleMapJson.optString("status").equalsIgnoreCase("ok")) {


            String address_line_1 = "";
            String address_line_2 = null;
            String city = null;
            String state = null;
            String country = null;
            String postal_code = null;
            String formated_address = "";
            JSONArray addressComponents = null;
            addressComponents = googleMapJson.optJSONArray("results").optJSONObject(0).getJSONArray("address_components");
            formated_address = googleMapJson.optJSONArray("results").optJSONObject(0).optString("formatted_address");
            for (int i = 0; i < addressComponents.length(); i++) {
                JSONArray typesArray = addressComponents.getJSONObject(i).getJSONArray("types");
                for (int j = 0; j < typesArray.length(); j++) {
                    if (typesArray.get(j).toString().equalsIgnoreCase("postal_code")) {
                        postal_code = addressComponents.optJSONObject(i).getString("long_name");
                    }
                    if (typesArray.get(j).toString().equalsIgnoreCase("locality")) {
                        city = addressComponents.getJSONObject(i).getString("long_name");
                    }
                    if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_1")) {
                        state = addressComponents.getJSONObject(i).getString("long_name");
                    }
                    if (typesArray.get(j).toString().equalsIgnoreCase("sublocality_level_2")) {
                        address_line_2 = addressComponents.getJSONObject(i).getString("long_name");
                    }
                    if (typesArray.get(j).toString().equalsIgnoreCase("premise") || typesArray.get(j).toString().equalsIgnoreCase("street_number")) {
                        address_line_1 = addressComponents.getJSONObject(i).getString("long_name");
                    }
                    if (typesArray.get(j).toString().equalsIgnoreCase("route")) {
                        address_line_1 = address_line_1 + " " + addressComponents.getJSONObject(i).getString("long_name");
                    }
                    if (typesArray.get(j).toString().equalsIgnoreCase("country")) {
                        country = addressComponents.getJSONObject(i).getString("long_name");
                    }

                }
            }
            Utils.log("Addresses:", "address_line_1: " + address_line_1 + " address_line_2: " + address_line_2 +
                    " city: " + city + " state: " + state + " country: " + country + " postal_code: " + postal_code);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantKeys.ERROR_STRING, ConstantKeys.FALSE_STRING);
            jsonObject.put(ConstantKeys.ERROR_MESSAGE_STRING, ConstantKeys.SUCCESS_STRING);
            jsonObject.put(ConstantKeys.CITY_NAME, city);
            jsonObject.put(ConstantKeys.CITY_LOCATION, address_line_1 + " " + address_line_2);
            jsonObject.put(ConstantKeys.STATE_NAME, state);
            jsonObject.put(ConstantKeys.COUNTRY_NAME, country);
            jsonObject.put(ConstantKeys.POSTAL_CODE, postal_code);
            jsonObject.put(ConstantKeys.FORMATED_ADDRESS, formated_address);
            Utils.log("FormatedAddressjson", jsonObject.toString());

            return jsonObject;

        } else {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ConstantKeys.ERROR_STRING, ConstantKeys.TRUE_STRING);
            jsonObject.put(ConstantKeys.ERROR_MESSAGE_STRING, ConstantKeys.FAILED_STRING);
            return jsonObject;
        }


    }

    public static String getDurationString(int seconds) {

        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        return twoDigitString(minutes);
    }

    public static String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

    public static HashMap<String, String> getAuthorizationHeader(Context context) {

        AppSettings appSettings = new AppSettings(context);

        HashMap<String, String> headers = new HashMap<>();
        headers.put(ConstantKeys.AUTHORIZATION, appSettings.getAccessToken());

        return headers;

    }

    public static void showKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static String convertTime(String dateStr) {

        SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss a", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formatedDate= df.format(date);
        DateFormat originalFormat = new SimpleDateFormat("hh:mm:ss a", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("hh:mm a");
        Date date1 = null;

        try {
             date1 = originalFormat.parse(formatedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date1);

    }

    public static String convertTimeFromTimeStamp(String dateStr) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());

        String formatedDate= df.format(date);
        DateFormat originalFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("hh:mm a");
        Date date1 = null;
        Date sysDate=new Date();
        try {
            date1 = originalFormat.parse(formatedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diffMs = sysDate.getTime() - date1.getTime();
        long diffSec = diffMs / 1000;
        long min = diffSec / 60;
        return ""+min+" mins";

    }

    public static String calculateTimeDifference(String serverTime) {
        Log.d(TAG, "calculateTimeDifference: "+serverTime);
        String date = "";
        String input="yyyy-mm-dd hh:mm:ss";
        SimpleDateFormat df = new SimpleDateFormat(input);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dates1 = null;
        try {
            dates1 = df.parse(serverTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return "offline";
        }
        DateTime postDateTime = new DateTime(dates1);
        DateTime currentDateTime = new DateTime();
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(input);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String dates = postDateTime.toString(input);
            String currentDates = currentDateTime.toString(input);
            Date date1 = simpleDateFormat.parse(dates);
            Date date2 = simpleDateFormat.parse(currentDates);
            if (date1.getTime() - date2.getTime() == 0) {
                date = " a moments ago";
            } else {
                Period period = null;
                try {
                    Interval interval = new Interval(date1.getTime(), date2.getTime());
                    period = interval.toPeriod(PeriodType.dayTime());
                    if (period.getYears() != 0) {
                        date = period.getYears() + " yrs ";
                    } else if (period.getYears() == 1) {
                        date = period.getYears() + " yr ";
                    } else if (period.getMonths() != 0) {
                        date = period.getMonths() + " mnths ";
                    } else if (period.getMonths() == 1) {
                        date = period.getMonths() + " mnth ";
                    } else if (period.getDays() != 0) {
                        date = period.getDays() + " days ";
                    } else if (period.getHours() != 0) {
                        date = period.getHours() + " hrs ";
                    } else if (period.getMinutes() != 0) {
                        date = period.getMinutes() + " mins ";
                    } else if (period.getMinutes() == 1) {
                        date = period.getMinutes() + " min ";
                    } else if (period.getSeconds() != 0) {
                        date = period.getSeconds() + " secs ";
                    } else if (period.getSeconds() == 1) {
                        date = period.getSeconds() + " sec ";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    date = " moments ago";
                }
//                System.out.printf(
//                        "%d years, %d months, %d days, %d hours, %d minutes, %d seconds%n",
//                        period.getYears(), period.getMonths(), period.getDays(),
//                        period.getHours(), period.getMinutes(), period.getSeconds());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "calculateTimeDifferenceNew: "+date);
        return date;
    }

    public static String convertTimeWithoutSeconds(String dateStr) {

        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formatedDate= df.format(date);
        DateFormat originalFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("hh:mm a");
        Date date1 = null;

        try {
            date1 = originalFormat.parse(formatedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date1);

    }

    public static Snackbar showSnackBarIndefinite(View parentLayout, String message) {

        Snackbar mySnackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_INDEFINITE);

        View snackbarView = mySnackbar.getView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            snackbarView.setElevation(30);
        }
        TextView tv = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
//        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/muli_regular.ttf");
//        tv.setTypeface(font);

        mySnackbar.show();
        return mySnackbar;
    }

}
