package com.app.foodappuser.Utilities.Constants;

import com.app.foodappuser.BuildConfig;

public class UrlHelper {
    //    public static String BASE_URL = Constants.BASE_URL;
    public static String BASE_URL = BuildConfig.BASE_URL+"/";
    public static String SOCKET_URL = "http://139.59.50.198:8002/";
    public static String UPDATE_ADDRESS = BASE_URL + "updateAddress";
    public static String CHECK_MOBILE_NUMBER = BASE_URL + "CheckAvailability";
    public static String USER_LOGIN = BASE_URL + "userPasswordLogin";
    public static String SIGN_UP_WITH_PASSWORD = BASE_URL + "userWithPasswordSignup";
    public static String SIGN_UP_WITHOUT_PASSWORD = BASE_URL + "userWithoutPasswordSignup";
    public static String FORGOT_PASSWORD = BASE_URL + "forgotPassword";
    public static String VERIFY_OTP = BASE_URL + "otpVerification";
    public static String CHANGE_PASSWORD = BASE_URL + "changePassword";
    public static String RATING = BASE_URL + "rating";
    public static String RESTAURANTS_NEAR_ME = BASE_URL + "listRestaurant";
    public static String SKIP_RATING = BASE_URL + "skipRating";
    public static String DISHES = BASE_URL + "listDishes";
    public static String LOGIN_SETTING = BASE_URL + "getloginSetting";
    public static String OTP_VERIFY = BASE_URL + "userOtpLogin";
    public static String RESEND_OTP = BASE_URL + "resendOtp";
    public static String GET_CURRENT_ADDRESS = BASE_URL + "getCurrentAddress";
    public static String GET_ADDRESS = BASE_URL + "getAddress";
    public static String ADD_ADDRESS = BASE_URL + "addAddress";
    public static String SET_CURRENT_ADDRESS = BASE_URL + "setCurrentAddress";
    public static String SEARCH = BASE_URL + "searchRestaurants";
    public static String SEARCH_RESTAURANT_AND_DISHES = BASE_URL + "searchDishesAndRestaurants";
    public static String UPDATE_PROFILE = BASE_URL + "updateProfile";
    public static String DESTROY_ADDRESS = BASE_URL + "destroyAddress";
    public static String STATIC_PAGES = BASE_URL + "getStaticpages";
    public static String ADD_TO_CART = BASE_URL + "addToCart";
    public static String VIEW_CART = BASE_URL + "viewCart";
    public static String UPDATE_CART = BASE_URL + "updateCart";
    public static String LIST_DELIVERY_ADDRESS = BASE_URL + "listDeliveryAddress";
    public static String LIST_PAYMENT_METHOD = BASE_URL + "listPaymentMethod";
    public static String ORDER_CONFIRM = BASE_URL + "orderConfirm";
    public static String PAST_ORDERS = BASE_URL + "listPastOrders";
    public static String TRACK_ORDER = BASE_URL + "trackOrders";
    public static String ADD_COUPON = BASE_URL + "addCoupon";
    public static String REMOVE_COUPON = BASE_URL + "removeCoupon";
    public static String SOCIAL_LOGIN = BASE_URL + "socialLogin";
    public static String USER_SIGNUP = BASE_URL + "userSignup";
    public static String USER_LOGOUT = BASE_URL + "userLogout";
    public static String UPDATE_DEVICE_TOKEN = BASE_URL + "updateDeviceToken";
}
