package com.app.foodappuser.Utilities.InterFaces;

import com.app.foodappuser.Model.Response.ListDishesResponse.DishItems;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;

public interface onQuantityChanged {
    void onIncreased(InputForAPI input, int position, DishItems dishItem);
    void onDecreased(InputForAPI  input, int position);
}
