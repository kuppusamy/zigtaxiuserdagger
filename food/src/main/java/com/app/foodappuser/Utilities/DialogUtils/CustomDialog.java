package com.app.foodappuser.Utilities.DialogUtils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class CustomDialog {


    Context context;
    int layout;
    private LayoutInflater layoutInflater;
    private Dialog dialog;
    private View view;
    private int gravity;

    public CustomDialog(Context context, int layout) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        initialize();

    }

    public void initialize() {
        view = layoutInflater.inflate(layout, null);
    }

    public void setGravity(int gravity)
    {
     this.gravity=gravity;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public void show() {
        dialog=new Dialog(context);
        dialog.setContentView(view);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(gravity);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();

    }

}
