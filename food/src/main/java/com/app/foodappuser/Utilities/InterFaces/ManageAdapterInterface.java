package com.app.foodappuser.Utilities.InterFaces;

import com.app.foodappuser.Model.Response.GetSavedAddressResponse.Address;

public interface ManageAdapterInterface {
    void removeAddress(Address list);
    void editAddress(Address list);
}
