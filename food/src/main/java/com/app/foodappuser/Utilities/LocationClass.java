package com.app.foodappuser.Utilities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.app.foodappuser.view.activities.BaseActivity;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

public class LocationClass extends BaseActivity implements
        LocationListener {
    private static final int LOCATION_CODE = 101;
    private static final int REQUEST_PERMISSION_SETTING = 102;
    private LocationCallback locationCallback;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest mLocationRequest;
    private boolean isNetworkLocation, isGPSLocation;
    private boolean isLocation;
    private SendLocationDataInterface sendData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    }

    public void checkLocationStatus() {
        LocationManager mListener = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (mListener != null) {
            isGPSLocation = mListener.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkLocation = mListener.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            Log.e("gps, network", String.valueOf(isGPSLocation + "," + isNetworkLocation));
        }

        if (isGPSLocation) {
            checkPermissions();

        }
//        else if (isNetworkLocation) {
//            checkPermissions();
//
//        }
        else {
            showLocationDialog();
        }
    }

    private void showLocationDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setMessage("You need to set your device's location.");
        dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.setFlags(0);
                startActivityForResult(intent, LOCATION_CODE);
                dialogInterface.dismiss();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                sendData.onLocationFailed();
            }
        });
        dialog.show();
    }

    private void showLocationPermissionDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setMessage("You need to grant permission to location.");
        dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                List<String> listPermissionsNeeded = new ArrayList<>();
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
                ActivityCompat.requestPermissions(LocationClass.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                dialogInterface.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void showLocationRationaleDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setMessage("You need to grant permission to location.");
        dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                checkPermissions();
                dialogInterface.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void showEnableLocationInSettingDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setMessage("You need to grant permission location permission in settings.");
        dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                dialogInterface.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void getLastKnownLocation() {
        fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    // Logic to handle location object
                    sendData.onLocationFetched(location);
//                    new AppSettings(LocationClass.this).setLastKnownLatitude(String.valueOf(location.getLatitude()));
//                    new AppSettings(LocationClass.this).setLastKnownLongitude(String.valueOf(location.getLongitude()));
                } else {
                    getMyLocation();
                }
            }
        });
    }

//    private synchronized void setUpGClient() {
//        googleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, 0, this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .build();
//        googleApiClient.connect();
//    }

    private void checkPermissions() {
        int permissionLocation = ContextCompat.checkSelfPermission(LocationClass.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            showLocationPermissionDialog();
        } else {
            getLastKnownLocation();
        }
    }

    private void getMyLocation() {
//        if (googleApiClient != null) {
//            if (googleApiClient.isConnected()) {
        int permissionLocation = ContextCompat.checkSelfPermission(LocationClass.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {

            SettingsClient mSettingsClient = LocationServices.getSettingsClient(this);
            /*
             * Callback returning location result
             */
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult result) {
                    super.onLocationResult(result);
                    //mCurrentLocation = locationResult.getLastLocation();
                    Location mCurrentLocation = result.getLocations().get(0);


                    if (mCurrentLocation != null) {
                        Log.e("Location(Lat)==", "" + mCurrentLocation.getLatitude());
                        Log.e("Location(Long)==", "" + mCurrentLocation.getLongitude());
//                        new AppSettings(LocationClass.this).setLastKnownLatitude(String.valueOf(mCurrentLocation.getLatitude()));
//                        new AppSettings(LocationClass.this).setLastKnownLongitude(String.valueOf(mCurrentLocation.getLongitude()));
                        sendData.onLocationFetched(mCurrentLocation);
                    }


                    /**
                     * To get location information consistently
                     * mLocationRequest.setNumUpdates(1) Commented out
                     * Uncomment the code below
                     */
                    fusedLocationClient.removeLocationUpdates(locationCallback);
                }

                //Locatio nMeaning that all relevant information is available
                @Override
                public void onLocationAvailability(LocationAvailability availability) {
                    isLocation = availability.isLocationAvailable();
                }
            };
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(5000);
            mLocationRequest.setFastestInterval(5000);
            //To get location information only once here
            mLocationRequest.setNumUpdates(3);

            if (isGPSLocation) {
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            } else if (isNetworkLocation) {
                mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            } else {
                if (!isLocation) {
//                            LocationSettingDialog.newInstance().show(getSupportFragmentManager(), "Setting");
                }
            }


            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            /**
             * Stores the type of location service the client wants to use. Also used for positioning.
             */
            LocationSettingsRequest mLocationSettingsRequest = builder.build();

            Task<LocationSettingsResponse> locationResponse = mSettingsClient.checkLocationSettings(mLocationSettingsRequest);
            locationResponse.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                    Log.e("Response", "Successful acquisition of location information!!");
                    //
                    if (ActivityCompat.checkSelfPermission(LocationClass.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    fusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper());
                }
            });
            //When the location information is not set and acquired, callback
            locationResponse.addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.e("onFailure", "Location environment check");
                            sendData.onLocationFailed();
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            String errorMessage = "Check location setting";
                            sendData.onLocationFailed();

                            Log.e("onFailure", errorMessage);
                    }
                }
            });
        }
//            }
//        }
    }

    public void initLocationInterface(SendLocationDataInterface sendData) {
        this.sendData = sendData;
    }


    @Override
    public void onLocationChanged(Location location) {

    }


    @Override
    protected void onStop() {
        super.onStop();
        if (fusedLocationClient != null && locationCallback != null) {
            fusedLocationClient.removeLocationUpdates(locationCallback);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_CODE) {
            checkLocationStatus();
        }

        if (requestCode == REQUEST_PERMISSION_SETTING) {
            checkPermissions();
        }
        Log.e("onActivityResult", "requestCode: " + requestCode + " resultCode: " + resultCode + "");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(LocationClass.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        } else {
            boolean showRationale = false;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                showRationale = shouldShowRequestPermissionRationale(permissions[0]);
                if (!showRationale) {
                    showEnableLocationInSettingDialog();
                    sendData.onLocationFailed();
                } else {
                    showLocationRationaleDialog();
                    // user did NOT check "never ask again"
                    // this is a good place to explain the user
                    // why you need the permission and ask if he wants
                    // to accept it (the rationale)
                }
            }

        }
    }


}
