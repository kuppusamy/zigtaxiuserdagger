package com.app.foodappuser.Utilities.BaseUtils;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.util.Log;

import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.InterFaces.startSetPermission;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.view.activities.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.greenrobot.eventbus.EventBus;


public class LocationPermissionChecker extends BaseActivity implements startSetPermission, com.google.android.gms.location.LocationListener, LocationListener,
        ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {

    protected static final int REQUEST_PERMISSION = 0x2;
    protected static final String TAG = LocationPermissionChecker.class.getSimpleName();
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mLocation;
    //    private SettingsClient mSettingsClient;
    protected FusedLocationProviderClient mFusedLocationClient;
    protected LocationRequest locationRequest;
    protected LocationCallback mLocationCallback;
    //    private LatLng mLatLng;
    protected LocationSettingsRequest.Builder builder;
    protected boolean isGPSOn = false;
    private AppSettings appSettings;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appSettings=new AppSettings(this);
        connectGoogleClient();
//        setAfterPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();

    }

    @Override
    public void setAfterPermission() {
        if (!isPermissionGranted()) {
            askPermission();
        } else {
            //showTurnOnLocationDialog();
            checkLocationSettings();
        }
    }



    public boolean isPermissionGranted() {
        if (ActivityCompat.checkSelfPermission(LocationPermissionChecker.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(LocationPermissionChecker.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else
            return true;
    }

    public void askPermission() {
        ActivityCompat.requestPermissions((LocationPermissionChecker.this),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);
    }

    public LocationRequest setLocationRequest() {
        LocationRequest lr = LocationRequest.create();
        lr.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        lr.setInterval(10000);
        lr.setFastestInterval(10000 / 2);
        return lr;
    }

    @Override
    public void checkLocationSettings() {

        if (checkGPSStatus())
            startLocationUpdates();
        else {
            setGPSOn();
//            showTurnOnLocationDialog();
        }
    }

    public boolean checkGPSStatus() {
        connectGoogleClient();
        locationRequest = setLocationRequest();

        builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert manager != null;
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Log.e(TAG, "checkLocationSettings: " + statusOfGPS);
        if (mGoogleApiClient != null) {
            return statusOfGPS;
        } else
            return false;
    }

    public GoogleApiClient getGoogleCLient()
    {
        return mGoogleApiClient;
    }

    private void connectGoogleClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

    }

    private void setGPSOn() {
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
//                    Log.e(TAG, "onSuccess: " + locationSettingsResponse.getLocationSettingsStates().isGpsPresent());
            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    Log.e(TAG, "onFailure: ");
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(LocationPermissionChecker.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                        Log.e(TAG, "onFailure: ", sendEx);
                    }
                }
            }
        });

    }

    @Override
    public Location getLocation() {
//        updateLocations();
        return mLocation;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "thissCalled:" + "called");
        switch (requestCode) {
            case REQUEST_PERMISSION:
                Log.e(TAG, "onRequestPermissionsResult: " + grantResults.length);
                if (grantResults.length > 1) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        Log.e(TAG, "onRequestPermissionsResult: granted");
                        checkLocationSettings();
                    } else if (ActivityCompat.shouldShowRequestPermissionRationale(LocationPermissionChecker.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                        // Permission Denied
                        askPermission();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showTurnOnLocationDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LocationPermissionChecker.this);
        // builder.setTitle("Need location  Permission");
        builder.setMessage("For the best experience, kindly switch on the location");

        builder.setPositiveButton(Html.fromHtml("<font color='#448EF6'>Turn On</font>"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
                setGPSOn();
            }
        });

        builder.setNegativeButton(Html.fromHtml("<font color='#448EF6'>Cancel</font>"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();

                showWarningDialog();

            }
        });


        builder.show();


    }

    private void showWarningDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LocationPermissionChecker.this);
        // builder.setTitle("Need location  Permission");
        builder.setMessage("Are you sure you want to deny this permission?");

        builder.setPositiveButton(Html.fromHtml("<font color='#448EF6'>Yes</font>"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
                finish();
            }
        });

        builder.setNegativeButton(Html.fromHtml("<font color='#448EF6'>No</font>"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();

                checkLocationSettings();

            }
        });
        builder.show();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "onConnected: ");
        setAfterPermission();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult);
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                isGPSOn = true;
                Log.e(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                isGPSOn = false;
                Log.e(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(LocationPermissionChecker.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                isGPSOn = false;
                Log.e(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;

        }
    }


    @SuppressLint("MissingPermission")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        Log.e(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();

                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e(TAG, "User chose not to make required location settings changes.");
                        checkLocationSettings();
//                        showWarningDialog();
                        break;
                }
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG, "onLocationChanged: " + location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Log.e(TAG, "onStatusChanged: " + i + "\n" + bundle);
    }

    @Override
    public void onProviderEnabled(String s) {
        Log.e(TAG, "onProviderEnabled: " + s);
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.e(TAG, "onProviderDisabled: " + s);
    }

    private void stopLocationUpdates() {
        if (mFusedLocationClient != null && mLocationCallback != null)
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    public void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mLocation = locationResult.getLastLocation();

                Utils.log(TAG, "onLocationResult: " + mLocation.getLatitude());
                Utils.log(TAG, "onLocationResult: " + mLocation.getLongitude());
                appSettings.setIsLatLongFetched(ConstantKeys.TRUE_STRING);
//                appSettings.setLastKnownLatitude(""+mLocation.getLatitude());
//                appSettings.setLastKnownLongitude(""+mLocation.getLongitude());
                EventBus.getDefault().post(new onLocationFetched(mLocation));
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);

            }
        };
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, null);
    }
}