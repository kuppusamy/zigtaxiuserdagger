package com.app.foodappuser.Utilities.InterFaces;

public interface CustomizationAdapterInterface {
    void addCustomizationDishName(String name,int categoryId,int elementId,String price,Boolean isSingleChoice);
    void removeCustomizationDishName(String name,int categoryId,int elementId,String price,Boolean isSingleChoice);
}
