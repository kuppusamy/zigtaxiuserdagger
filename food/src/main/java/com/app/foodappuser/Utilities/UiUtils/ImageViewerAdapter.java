package com.app.foodappuser.Utilities.UiUtils;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.foodappuser.R;

import java.util.ArrayList;

public class ImageViewerAdapter extends PagerAdapter {
    private ArrayList<Integer> imageList;
    private Context mContext;

    public ImageViewerAdapter(Context imageViewer_activity, ArrayList<Integer> imageArray) {
        this.imageList = imageArray;
        this.mContext = imageViewer_activity;
    }


    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.fragment_walk_through, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.walkThroughBgImage);
        imageView.setImageResource(imageList.get(position));

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
