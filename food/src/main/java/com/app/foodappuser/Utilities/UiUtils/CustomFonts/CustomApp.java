package com.app.foodappuser.Utilities.UiUtils.CustomFonts;

import android.app.Application;
import android.graphics.Typeface;

import com.app.foodappuser.Utilities.BaseUtils.Utils;

/**
 * Created by rujul on 2/5/2016.
 */
public class CustomApp extends Application {

    private static final String TAG = CustomApp.class.getSimpleName();
    private static CustomApp mInstance;
    private TypeFactory mFontFactory;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized CustomApp getApp(){
        return mInstance;
    }

    public Typeface getTypeFace(int type){
        Utils.log("TYPE", String.valueOf(type));

        if(mFontFactory==null)
            mFontFactory = new TypeFactory(this);


        switch (type){
            case Constants.ONE_NORMAL : return mFontFactory.getOneNormal();

            case Constants.ONE_MEDIUM: return mFontFactory.getOneMedium();

            case Constants.ONE_BOLD: return mFontFactory.getOneBold();

            case Constants.TWO_BOLD: return mFontFactory.getTwoBold();

            case Constants.TWO_REGULAR: return mFontFactory.getTwoRegular();

            case Constants.TWO_SEMIBOLD: return mFontFactory.getTwoSemiBold();

            case Constants.THREE_REGULAR: return mFontFactory.getThreeRegular();

            case Constants.FOUR_SEMIBOLD: return mFontFactory.getFourSemiBold();

            default: return mFontFactory.getOneNormal();
        }
    }

    public interface Constants {
        int ONE_NORMAL = 1,
                ONE_MEDIUM = 2,
                ONE_BOLD = 3,
                TWO_BOLD=4,
                TWO_REGULAR=5,
                TWO_SEMIBOLD=6,
                THREE_REGULAR=7,
                FOUR_SEMIBOLD=8;
    }
}
