package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ListDishesResponse.CustomizableDishItem;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.InterFaces.CustomizationAdapterInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class CustomizableDishItemMultipleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<CustomizableDishItem> customizableDishItemList;
    Context context;
    CustomizationAdapterInterface customizationAdapterInterface;


    public CustomizableDishItemMultipleAdapter(Context context, List<CustomizableDishItem> customizableDishItems, CustomizationAdapterInterface customizationAdapterInterface) {
        this.customizableDishItemList = customizableDishItems;
        this.context = context;
        this.customizationAdapterInterface = customizationAdapterInterface;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView;
        itemView = LayoutInflater.from(context).inflate(R.layout.customizable_dish_multiple_item, viewGroup, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new CustomizableDishItemMultipleHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final CustomizableDishItemMultipleHolder customizableElementHolder = (CustomizableDishItemMultipleHolder) viewHolder;
        final CustomizableDishItem customizableElement = customizableDishItemList.get(position);

        if (customizableElement.getIsVeg() == 1) {
            customizableElementHolder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.veg_icon));
        } else {
            customizableElementHolder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.non_veg_icon));
        }

        customizableElementHolder.displayPrice.setText(customizableElement.getDisplayPrice());
        customizableElementHolder.dishCheckBox.setText(customizableElement.getElementName()+"  "+customizableElement.getDisplayPrice());

        customizableElementHolder.dishCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    customizationAdapterInterface.addCustomizationDishName(customizableElement.getElementName(), customizableElement.getCustomisationCategoryId(), customizableElement.getElementId(), customizableElement.getPrice(), false);
                } else {
                    customizationAdapterInterface.removeCustomizationDishName(customizableElement.getElementName(), customizableElement.getCustomisationCategoryId(), customizableElement.getElementId(), customizableElement.getPrice(), false);

                }
            }
        });

//        customizableElementHolder.dishCheckBox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(customizableElementHolder.dishCheckBox.isChecked()){
//                    customizationAdapterInterface.removeCustomizationDishName(customizableElement.getElementName(),customizableElement.getCustomisationCategoryId(),customizableElement.getElementId(),false);
//                }else{
//                    customizationAdapterInterface.addCustomizationDishName(customizableElement.getElementName(),customizableElement.getCustomisationCategoryId(),customizableElement.getElementId(),false);
//                }
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return (customizableDishItemList != null) ? customizableDishItemList.size() : 0;
    }

    class CustomizableDishItemMultipleHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.dishCheckBox)
        CheckBox dishCheckBox;
        @BindView(R2.id.displayPrice)
        TextView displayPrice;
        @BindView(R2.id.isVegView)
        ImageView isVegView;
        public CustomizableDishItemMultipleHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
