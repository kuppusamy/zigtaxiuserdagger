package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ViewCartResponse.Dish;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.InterFaces.ViewCartDishesAdapterInterface;
import com.app.foodappuser.Utilities.Ticker.TickerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewCartDishesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;

    private List<Dish> list;

    private ViewCartDishesAdapterInterface viewCartDishesAdapterInterface;

    public ViewCartDishesAdapter(Context context, List<Dish> dishes) {
        this.context = context;
        this.list = dishes;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.view_cart_dishes_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;
        final Dish dish=list.get(position);


        if(dish.getIsAvailable()){

            if (dish.getIsVeg() == 1) {
                holder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.veg_icon));
            } else {
                holder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.non_veg_icon));
            }

            holder.actionLayout.setVisibility(View.VISIBLE);
            holder.displayPrice.setVisibility(View.VISIBLE);
            holder.unAvailableText.setVisibility(View.GONE);
            holder.deleteItem.setVisibility(View.GONE);
            holder.dishName.setText(dish.getDishName().trim());
            holder.quantity.setText(String.valueOf(dish.getDishQuantity()));
            holder.displayPrice.setText(dish.getDisplayDishTotal().trim());

        }else{

            holder.actionLayout.setVisibility(View.GONE);
            holder.displayPrice.setVisibility(View.GONE);
            holder.deleteItem.setVisibility(View.VISIBLE);
            holder.unAvailableText.setVisibility(View.VISIBLE);
        }

        if(dish.getIsCustomizable()==1){
            holder.customizableLayout.setVisibility(View.VISIBLE);
        }else{
            holder.customizableLayout.setVisibility(View.GONE);

        }

        holder.increaseCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewCartDishesAdapterInterface.onIncreased(dish.getCartId(),dish.getDishQuantity()+1,dish.getUuId());
            }
        });

        holder.decreaseCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewCartDishesAdapterInterface.onDecreased(dish.getCartId(),dish.getDishQuantity()-1,dish.getUuId());
            }
        });

        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewCartDishesAdapterInterface.onDeleted(dish.getCartId(),0,dish.getUuId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return  (list != null)?list.size():0;
    }

    public void setOnClickListener(ViewCartDishesAdapterInterface viewCartDishesAdapterInterface) {
        this.viewCartDishesAdapterInterface=viewCartDishesAdapterInterface;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.isVegView)
        ImageView isVegView;
        @BindView(R2.id.dishName)
        TextView dishName;
        @BindView(R2.id.unAvailableText)
        TextView unAvailableText;
        @BindView(R2.id.decreaseCart)
        RelativeLayout decreaseCart;
        @BindView(R2.id.quantity)
        TickerView quantity;
        @BindView(R2.id.increaseCart)
        RelativeLayout increaseCart;
        @BindView(R2.id.actionLayout)
        LinearLayout actionLayout;
        @BindView(R2.id.deleteItem)
        ImageView deleteItem;
        @BindView(R2.id.displayPrice)
        TextView displayPrice;
        @BindView(R2.id.customizableLayout)
        LinearLayout customizableLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }




}