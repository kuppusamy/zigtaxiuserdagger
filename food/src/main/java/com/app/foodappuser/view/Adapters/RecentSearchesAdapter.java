package com.app.foodappuser.view.Adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.RecentSearches.RestaurantName;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.view.activities.RestaurantDetailsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecentSearchesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private List<RestaurantName> recentList;

    public RecentSearchesAdapter(Context context, List<RestaurantName> searches) {
        this.context = context;
        this.recentList = searches;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.recent_search_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new Recent(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        final Recent holder = (Recent) viewHolder;

        holder.restaurantName.setText(recentList.get(i).getRestaurantName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RestaurantDetailsActivity.class);
                intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_ID, recentList.get(i).getId());
                intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_NAME, recentList.get(i).getRestaurantName());
                intent.putExtra(ConstantKeys.INTENTKEYS.CUISINE_NAME, recentList.get(i).getCuisines());
                intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_MIN, recentList.get(i).getDisplayTime());
                intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_REVIEW, recentList.get(i).getAverageReview());
                intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_COST_FOR_TWO, recentList.get(i).getDisplayCostForTwo());
                intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_ENABLED, recentList.get(i).getCouponEnabledForRestaurant());
                intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_LONG_DESCRIPTION, recentList.get(i).getLongDescription());

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (recentList != null) ? recentList.size() : 0;
    }

    class Recent extends RecyclerView.ViewHolder {
        @BindView(R2.id.restaurantName)
        TextView restaurantName;

        public Recent(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
