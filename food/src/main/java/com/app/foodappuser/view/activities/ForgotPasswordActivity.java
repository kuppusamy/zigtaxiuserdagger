package com.app.foodappuser.view.activities;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ForgotPasswordResponseModel;
import com.app.foodappuser.Model.Response.ResendOtpResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.ViewAnimationUtils;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.BaseUtils.onMessageFetched;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.OTPListener.OnOtpCompletionListener;
import com.app.foodappuser.Utilities.OTPListener.OtpView;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.ViewModel.ForgotPasswordViewModel;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener,OnOtpCompletionListener {


    private static final String TAG = ForgotPasswordActivity.class.getSimpleName();

    @BindView(R2.id.otp_view)
    OtpView otp_view;

    @BindView(R2.id.proceedButton)
    CardView proceedButton;

    @BindView(R2.id.backButton)
    ImageView backButton;

    @BindView(R2.id.blockImage)
    ImageView blockImage;

    @BindView(R2.id.progressBarOTP)
    ProgressBar progressBarOTP;

    @BindView(R2.id.changePassword)
    CardView changePassword;

    @BindView(R2.id.newPassword)
    EditText newPassword;

    @BindView(R2.id.confirmPassword)
    EditText confirmPassword;


    @BindView(R2.id.changePasswordLayout)
    RelativeLayout changePasswordLayout;

    @BindView(R2.id.slideDownLayout)
    RelativeLayout slideDownLayout;

    @BindView(R2.id.parentLayout)
    ConstraintLayout parentLayout;

    @BindView(R2.id.resendOtp)
    TextView resendOtp;

    String mobileNumber, countryCode;
    ForgotPasswordViewModel forgotPasswordViewModel;
    boolean isFirstTime = true;
    private InputForAPI inputForAPI;

    Boolean isResendOtpEnabled = false;

    AppSettings appSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        forgotPasswordViewModel = ViewModelProviders.of(this).get(ForgotPasswordViewModel.class);
        appSettings = new AppSettings(ForgotPasswordActivity.this);
        initisteners();
        getValues();
        validatePassword();

    }



    @Override
    public void onStart() {
        super.onStart();
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void setTimer() {

        new CountDownTimer(Long.parseLong(appSettings.getOtpTimeOut()), 1000) {

            public void onTick(long millisUntilFinished) {
                isResendOtpEnabled = false;
                resendOtp.setText(getResources().getString(R.string.resendOtpDescription) + " " + millisUntilFinished / 1000 + " " + getResources().getString(R.string.seconds));
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                isResendOtpEnabled = true;
                resendOtp.setText(getResources().getString(R.string.resendOtp));
            }
        }.start();

    }


    private void validatePassword() {

        confirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (newPassword.getText().toString().trim().length() != 0) {
                    Utils.log(TAG, String.valueOf(s));
                    if (newPassword.getText().toString().trim().equalsIgnoreCase(confirmPassword.getText().toString().trim())) {
                        changePassword.setAlpha(1);
                        changePassword.setEnabled(true);
                    } else {
                        changePassword.setAlpha((float) 0.5);
                        changePassword.setEnabled(false);

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void getValues() {
        mobileNumber = getIntent().getStringExtra(ConstantKeys.MOBILE_NUMBER);
        countryCode = getIntent().getStringExtra(ConstantKeys.COUNTRY_CODE);
        if (isFirstTime) {
            isFirstTime = false;
            try {
                forgotPassword();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void initisteners() {
        otp_view.setOtpCompletionListener(this);
        backButton.setOnClickListener(this);
        proceedButton.setOnClickListener(this);
        changePassword.setOnClickListener(this);
        blockImage.setOnClickListener(this);
        slideDownLayout.setOnClickListener(this);

        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isResendOtpEnabled) {
                    otpResendResponse();
                }
            }
        });

    }

    private void otpResendResponse() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ConstantKeys.MOBILE_NUMBER, mobileNumber);
            jsonObject.put(ConstantKeys.COUNTRY_CODE, countryCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(ForgotPasswordActivity.this);
        inputForAPI.setUrl(UrlHelper.RESEND_OTP);
        inputForAPI.setJsonObject(jsonObject);

        forgotPasswordViewModel.resendOtp(inputForAPI).observe(this, new Observer<ResendOtpResponseModel>() {
            @Override
            public void onChanged(@Nullable ResendOtpResponseModel resendOtpResponseModel) {
                if (resendOtpResponseModel.getError()) {
                    Utils.showSnackBar(parentLayout, resendOtpResponseModel.getErrorMessage());
                } else {
                    Utils.showSnackBar(parentLayout, resendOtpResponseModel.getErrorMessage());
                    setTimer();
                }
            }
        });

    }


    @Override
    public void onClick(View view) {
        if (view == backButton) {
            finish();
        } else if (view == slideDownLayout) {

        } else if (view == blockImage) {

            hideChangePasswordLayout();

        } else if (view == proceedButton) {
            try {
                verifyOtp();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (view == changePassword) {
            if (isValidInputs())
                try {
                    changePasswordWithAPI();
                    Utils.hideKeyboard(ForgotPasswordActivity.this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }
    }

    @Override
    public void onBackPressed() {

        if (changePasswordLayout.getVisibility() == View.VISIBLE) {
            hideChangePasswordLayout();
        } else {
            super.onBackPressed();

        }
    }

    private void hideChangePasswordLayout() {
        progressBarOTP.setVisibility(View.INVISIBLE);
        changePasswordLayout.setVisibility(View.GONE);
        Utils.hideKeyboard(ForgotPasswordActivity.this);

    }

    public void showToast(String message) {
        Utils.showSnackBar(findViewById(android.R.id.content), message);
    }

    private boolean isValidInputs() {
        if (newPassword.getText().toString().trim().length() == 0 || confirmPassword.getText().toString().trim().length() == 0) {
            showToast(Utils.getString(ForgotPasswordActivity.this, R.string.passowrd_cant_be_empty));
            return false;
        } else if (!newPassword.getText().toString().equalsIgnoreCase(confirmPassword.getText().toString())) {
            showToast(Utils.getString(ForgotPasswordActivity.this, R.string.passowrd_mismatch));

            return false;
        } else {
            return true;
        }
    }

    private void changePasswordWithAPI() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.MOBILE_NUMBER, mobileNumber);
        jsonObject.put(ConstantKeys.COUNTRY_CODE, countryCode);
        jsonObject.put(ConstantKeys.PASSWORD, newPassword.getText().toString());
        jsonObject.put(ConstantKeys.OTP_NUMBER, otp_view.getText().toString());
        inputForAPI = new InputForAPI(ForgotPasswordActivity.this);
        inputForAPI.setUrl(UrlHelper.CHANGE_PASSWORD);
        inputForAPI.setJsonObject(jsonObject);
        forgotPasswordViewModel.getAPIResponse(inputForAPI).observe(this, new Observer<ForgotPasswordResponseModel>() {
            @Override
            public void onChanged(@Nullable ForgotPasswordResponseModel forgotPasswordResponseModel) {
                if (forgotPasswordResponseModel.getError()) {
                    Utils.showSnackBar(parentLayout, forgotPasswordResponseModel.getErrorMessage());
                } else {
                    progressBarOTP.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(ForgotPasswordActivity.this, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    EventBus.getDefault().post(new onMessageFetched(forgotPasswordResponseModel.getErrorMessage()));
                    startActivity(intent);
                }
            }
        });

    }


    private void verifyOtp() throws JSONException {
        if(otp_view.getText().toString().length()==6){
            checkOtpValidResponse(otp_view.getText().toString());

        }else{
            Utils.showSnackBar(parentLayout, this.getResources().getString(R.string.enter_valid_otp));

        }

    }

    private void checkOtpValidResponse(String otp) throws JSONException {
        progressBarOTP.setVisibility(View.VISIBLE);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.MOBILE_NUMBER, mobileNumber);
        jsonObject.put(ConstantKeys.COUNTRY_CODE, countryCode);
        jsonObject.put(ConstantKeys.OTP_NUMBER, otp);
        inputForAPI = new InputForAPI(ForgotPasswordActivity.this);
        inputForAPI.setUrl(UrlHelper.VERIFY_OTP);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setFile(null);
        inputForAPI.setHeaders(new HashMap<String, String>());

        forgotPasswordViewModel.getAPIResponse(inputForAPI).observe(this, new Observer<ForgotPasswordResponseModel>() {
            @Override
            public void onChanged(@Nullable ForgotPasswordResponseModel forgotPasswordResponseModel) {
                if (forgotPasswordResponseModel.getError()) {
                    progressBarOTP.setVisibility(View.INVISIBLE);
                    Utils.showSnackBar(findViewById(android.R.id.content), forgotPasswordResponseModel.getErrorMessage());
                } else {
                    showChangePasswordDialog();
                }
            }
        });
    }

    private void showChangePasswordDialog() {
        changePasswordLayout.setVisibility(View.VISIBLE);
        ViewAnimationUtils.expand(slideDownLayout, ForgotPasswordActivity.this);
    }

    private void forgotPassword() throws JSONException {

        progressBarOTP.setVisibility(View.VISIBLE);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.MOBILE_NUMBER, mobileNumber);
        jsonObject.put(ConstantKeys.COUNTRY_CODE, countryCode);
        inputForAPI = new InputForAPI(ForgotPasswordActivity.this);
        inputForAPI.setUrl(UrlHelper.FORGOT_PASSWORD);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setFile(null);
        inputForAPI.setHeaders(new HashMap<String, String>());
        isFirstTime = false;

        forgotPasswordViewModel.getAPIResponse(inputForAPI).observe(this, new Observer<ForgotPasswordResponseModel>() {
            @Override
            public void onChanged(@Nullable ForgotPasswordResponseModel forgotPasswordResponseModel) {
                if (forgotPasswordResponseModel.getError()) {
                    progressBarOTP.setVisibility(View.INVISIBLE);

                    Utils.showSnackBar(findViewById(android.R.id.content), forgotPasswordResponseModel.getErrorMessage());

                } else {
                    progressBarOTP.setVisibility(View.INVISIBLE);
                    Utils.showSnackBar(findViewById(android.R.id.content), forgotPasswordResponseModel.getErrorMessage());
                    otp_view.setText((String.valueOf(forgotPasswordResponseModel.getOtpNumber())));
                    proceedButton.setAlpha(1);
                    proceedButton.setEnabled(true);
                    appSettings.setOtpTimeOut((String.valueOf(Integer.parseInt(forgotPasswordResponseModel.getTimeDelayForOtp())*1000)));
                    setTimer();

                }

            }
        });


    }

    @Override
    public void onOtpCompleted(String otp) {
        if(otp.length()==6){

            proceedButton.setAlpha(1);
            proceedButton.setEnabled(true);
        }else{
            proceedButton.setAlpha((float) 0.5);
            proceedButton.setEnabled(false);
        }
    }


}
