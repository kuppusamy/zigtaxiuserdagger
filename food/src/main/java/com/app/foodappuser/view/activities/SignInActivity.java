package com.app.foodappuser.view.activities;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;

import com.app.foodappuser.Model.Response.SocialLoginResponse.FacebookLoginModel;
import com.app.foodappuser.Model.Response.SocialLoginResponse.SocialLoginModel;
import com.app.foodappuser.R2;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.BaseUtils.onMessageFetched;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.DialogUtils.CustomBottomSheetDialog;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.ViewModel.SignInViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;

public class SignInActivity extends BaseActivity {

    private static final int RC_SIGN_IN = 001;
    private static final String EMAIL = "email";

    private static final String TAG = SignInActivity.class.getSimpleName();
    @BindView(R2.id.loginCardView)
    CardView loginCardView;
    boolean isBottomSheetDialogShown = false;
    CustomBottomSheetDialog customBottomSheetDialog;
    BottomSheetDialog bottomSheetDialog;
    View bottomSheetDialogView;

    SignInViewModel signInActivityViewModel;
    CoordinatorLayout parentBottomsheetLayout;
    AppSettings appSettings;
    SignInButton google_sign_in_button;
    CoordinatorLayout parentLayout;
    Bundle bundle = new Bundle();
    Intent intent;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager callbackManager;
    private LoginButton fb_sign_in_button;
    private boolean socialLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        signInActivityViewModel = ViewModelProviders.of(this).get(SignInViewModel.class);
        appSettings = new AppSettings(SignInActivity.this);
        setupGoogleSignIn();
        initViewsAndVariables();
        setupFaceBookSignIn();
    }

    private void setupFaceBookSignIn() {

        callbackManager = CallbackManager.Factory.create();

        fb_sign_in_button.setReadPermissions(Arrays.asList(EMAIL));
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        fb_sign_in_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                setFacebookData(loginResult);
//                Toast.makeText(SignInActivity.this, "success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(SignInActivity.this, "cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e(TAG, "onError: " + exception.getMessage());
                Toast.makeText(SignInActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupGoogleSignIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageFetchedOrNot(final onMessageFetched messageFetched) {
        Utils.log("Event", messageFetched.getMessage());
        Toast.makeText(this, messageFetched.getMessage(), Toast.LENGTH_SHORT).show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.showSnackBar(findViewById(android.R.id.content), messageFetched.getMessage());

            }
        }, 200);
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initListners();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            EventBus.getDefault().unregister(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initViewsAndVariables() {
        parentLayout = findViewById(R.id.parentLayout);
        customBottomSheetDialog = new CustomBottomSheetDialog(SignInActivity.this, R.layout.login_bottom_sheet, parentLayout, mGoogleSignInClient);

        google_sign_in_button = findViewById(R.id.sign_in_button);
        fb_sign_in_button = findViewById(R.id.login_button);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            checkSocialLoginApi(account.getId(), getResources().getString(R.string.google).toUpperCase(), account.getEmail());
            // Signed in successfully, show authenticated UI.
            bundle.putString(ConstantKeys.SOCIAL_TOKEN, account.getId());
            bundle.putString(ConstantKeys.USER_NAME, account.getDisplayName());
            bundle.putString(ConstantKeys.EMAIL, account.getEmail());
            bundle.putString(ConstantKeys.SOCIAL_LOGIN, getResources().getString(R.string.google).toUpperCase());
            bundle.putString(ConstantKeys.LOGIN_TYPE, "social");

            customBottomSheetDialog.setBundle(bundle);
            customBottomSheetDialog.setGoogleLogin();
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            Toast.makeText(this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
//            updateUI(null);
        }
    }

    private void checkSocialLoginApi(String id, String type, String email) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ConstantKeys.SOCIAL_TOKEN, id);
            jsonObject.put(ConstantKeys.LOGIN_TYPE, type);
            jsonObject.put(ConstantKeys.EMAIL, email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(SignInActivity.this);
        inputForAPI.setUrl(UrlHelper.SOCIAL_LOGIN);
        inputForAPI.setJsonObject(jsonObject);

        signInActivityViewModel.socialLoginModelLiveData(inputForAPI).observe(this, new Observer<SocialLoginModel>() {
            @Override
            public void onChanged(SocialLoginModel socialLoginModel) {
                if (socialLoginModel.getError()) {
                    Utils.showToast(socialLoginModel.getErrorMessage(), SignInActivity.this);

                } else {
                    if (socialLoginModel.getIsNewSocialUser()) {
                        if (socialLoginModel.getLogin()) {
                            //Home Dashboard
//                            if (socialLogin) {
//                                appSettings.setLoginType(bundle.getString(ConstantKeys.SOCIAL_LOGIN, getResources().getString(R.string.social)));
                            appSettings.setLoginType(type);
//                            } else {
//                                appSettings.setLoginType(getResources().getString(R.string.normal));
//                            }
                            appSettings.setAccessToken(ConstantKeys.BEARER + socialLoginModel.getAccessToken());
                            appSettings.setEmail(socialLoginModel.getUserdetails().getEmail());
                            appSettings.setUserid(String.valueOf(socialLoginModel.getUserdetails().getId()));

                            appSettings.setUsername(socialLoginModel.getUserdetails().getUserName());
                            appSettings.setMobileNumber(socialLoginModel.getUserdetails().getMobileNumber());
                            appSettings.setIsLoggedIn(ConstantKeys.TRUE_STRING);
                            moveToMainActivity();
                        } else {
                            //signup
                            socialLogin = true;
                            customBottomSheetDialog.show(getSupportFragmentManager(), customBottomSheetDialog.getTag());
                            customBottomSheetDialog.showBottomSheetSocialLoginSignup();
                        }
                    } else {
                        if (type.equalsIgnoreCase(getResources().getString(R.string.facebook).toUpperCase())) {
                            LoginManager.getInstance().logOut();
                        } else {
                            mGoogleSignInClient.signOut();
                        }
                        Utils.showSnackBar(findViewById(android.R.id.content), socialLoginModel.getErrorMessage());
                    }
                }
            }
        });
    }

    private void moveToMainActivity() {
        Intent intent = new Intent(SignInActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void setFacebookData(final LoginResult loginResult) {

        signInActivityViewModel.facebookData(loginResult).observe(this, new Observer<FacebookLoginModel>() {
            @Override
            public void onChanged(FacebookLoginModel facebookLoginModel) {
                bundle.putString(ConstantKeys.USER_NAME, facebookLoginModel.getFirstName() + " " + facebookLoginModel.getLastName());
                bundle.putString(ConstantKeys.EMAIL, facebookLoginModel.getEmail());
                bundle.putString(ConstantKeys.SOCIAL_TOKEN, facebookLoginModel.getProfileId());
                bundle.putString(ConstantKeys.SOCIAL_LOGIN, getResources().getString(R.string.facebook).toUpperCase());
                bundle.putString(ConstantKeys.LOGIN_TYPE, "social");

                customBottomSheetDialog.setBundle(bundle);
                customBottomSheetDialog.setFaceBookLogin();

                checkSocialLoginApi(facebookLoginModel.getProfileId(), getResources().getString(R.string.facebook).toUpperCase(), facebookLoginModel.getEmail());

            }
        });
    }

    private void initListners() {

        google_sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        loginCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                SignInBottomSheetDialogFragment signInBottomSheetDialogFragment=SignInBottomSheetDialogFragment.newInstance();
//                signInBottomSheetDialogFragment.show(getSupportFragmentManager(),"signin");
                socialLogin = false;
                bundle.putString(ConstantKeys.LOGIN_TYPE, "normal");
                customBottomSheetDialog.setBundle(bundle);
                if (!customBottomSheetDialog.isAdded())
                    customBottomSheetDialog.show(getSupportFragmentManager(), customBottomSheetDialog.getTag());

            }
        });


//        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface dialogInterface) {
//                new Handler().postDelayed(new Runnable() {
//
//                    public void run() {
//                        isBottomSheetDialogShown = true;
//                        passwordContainer.setVisibility(View.GONE);
//                        phoneNumber.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
//                        phoneNumber.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
//                        phoneNumber.setText("");
//                    }
//                }, 200);
//            }
//        });

//        bottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                refreshSheet();
//            }
//        });


    }

}
