package com.app.foodappuser.view.activities;

import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.app.foodappuser.Model.Response.AutoCompleteResponse.AutoCompleteAddressResponseModel;
import com.app.foodappuser.Model.Response.AutoCompleteResponse.Prediction;
import com.app.foodappuser.Model.Response.GetLatLongFromIdResponse.GetLatLongFromIdResponseModel;
import com.app.foodappuser.Model.Response.GetSavedAddressResponse.GetSavedAddressResponseModel;
import com.app.foodappuser.Model.Response.SetCurrentAddressResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.Autocomplete.PlacePredictions;
import com.app.foodappuser.Utilities.InterFaces.onClickListener;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.LocationClass;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.Utilities.SendLocationDataInterface;
import com.app.foodappuser.view.Adapters.AddressListAdapter;
import com.app.foodappuser.view.Adapters.AutoCompleteAdapter;
import com.app.foodappuser.ViewModel.ChooseLocationViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

public class ChooseLocationActivity extends LocationClass implements SendLocationDataInterface {

    @BindView(R2.id.autoCompleteList)
    RecyclerView autoCompleteList;

    @BindView(R2.id.addressList)
    RecyclerView addressList;

    @BindView(R2.id.searchBox)
    EditText searchBox;

    @BindView(R2.id.currentLocationLayout)
    LinearLayout currentLocationLayout;

    @BindView(R2.id.currentLocationWithAddressLayout)
    LinearLayout currentLocationWithAddressLayout;

    @BindView(R2.id.savedAddressLayout)
    LinearLayout savedAddressLayout;

    @BindView(R2.id.progressBarOTP)
    ProgressBar progressBarOTP;

    @BindView(R2.id.backButton)
    RelativeLayout backButton;

    ChooseLocationViewModel chooseLocationViewModel;

    AppSettings appSettings = new AppSettings(ChooseLocationActivity.this);
    AutoCompleteAdapter autoCompleteAdapter;
    String selectedLat, selectedLong;
    String selectedAddressName, selectedAddressText;
    private PlacePredictions predictions = new PlacePredictions();
    private String TAG = ChooseLocationActivity.class.getSimpleName();

    AddressListAdapter addressListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_location);
        chooseLocationViewModel = ViewModelProviders.of(this).get(ChooseLocationViewModel.class);
        initLocationInterface(this);

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onStart() {
        super.onStart();
        initListeners();
        getSavedAddressData();
    }

    private void getSavedAddressData() {

        progressBarOTP.setVisibility(View.VISIBLE);

        InputForAPI inputForAPI = new InputForAPI(this);
        inputForAPI.setUrl(UrlHelper.GET_ADDRESS);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));

        chooseLocationViewModel.getSavedAddressResponseModelLiveData(inputForAPI).observe(this, new Observer<GetSavedAddressResponseModel>() {
            @Override
            public void onChanged(@Nullable GetSavedAddressResponseModel getSavedAddressResponseModel) {
                progressBarOTP.setVisibility(View.INVISIBLE);

                if (getSavedAddressResponseModel.getError()) {
//                    Utils.showSnackBar(findViewById(android.R.id.content),getSavedAddressResponseModel.getErrorMessage());
                } else {
                    if (getSavedAddressResponseModel.getAddress().size() == 0) {
                        savedAddressLayout.setVisibility(View.GONE);
                        progressBarOTP.setVisibility(View.INVISIBLE);
                    } else {
                        savedAddressLayout.setVisibility(View.VISIBLE);
                        progressBarOTP.setVisibility(View.INVISIBLE);
                        setAddressAdapter(getSavedAddressResponseModel);
                    }
                }
            }
        });
    }

    private void setAddressAdapter(final GetSavedAddressResponseModel getSavedAddressResponseModel) {
        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        addressList.setLayoutManager(layout);
        addressListAdapter = new AddressListAdapter(this, getSavedAddressResponseModel.getAddress());
        addressList.setAdapter(addressListAdapter);
        addressListAdapter.setOnClickListner(new onClickListener() {
            @Override
            public void onClicked(int position) {
                try {
                    setCurrentAddress(getSavedAddressResponseModel.getAddress().get(position).getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setCurrentAddress(Integer id) throws JSONException {
        progressBarOTP.setVisibility(View.VISIBLE);
        InputForAPI inputForAPI = new InputForAPI(ChooseLocationActivity.this);
        inputForAPI.setUrl(UrlHelper.SET_CURRENT_ADDRESS);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.ADDRESS_ID, id);
        inputForAPI.setJsonObject(jsonObject);

        chooseLocationViewModel.setCurrentAddress(inputForAPI).observe(this, new Observer<SetCurrentAddressResponseModel>() {
            @Override
            public void onChanged(@Nullable SetCurrentAddressResponseModel setCurrentAddressResponseModel) {
                progressBarOTP.setVisibility(View.INVISIBLE);
                if (setCurrentAddressResponseModel.getError()) {

                } else {
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void searchPlaces() {
        progressBarOTP.setVisibility(View.VISIBLE);
        InputForAPI inputForAPI = new InputForAPI(ChooseLocationActivity.this);
        inputForAPI.setUrl(getPlaceAutoCompleteUrl(searchBox.getText().toString(), selectedLat, selectedLong));
        inputForAPI.setFile(null);
        inputForAPI.setHeaders(new HashMap<String, String>());
        inputForAPI.setJsonObject(new JSONObject());

        chooseLocationViewModel.getLocationDetails(inputForAPI).observe(this, new Observer<AutoCompleteAddressResponseModel>() {
            @Override
            public void onChanged(@Nullable AutoCompleteAddressResponseModel autoCompleteAddressResponseModel) {
                progressBarOTP.setVisibility(View.INVISIBLE);
                if (autoCompleteAddressResponseModel.getError()) {

                } else {
                    setAdapters(autoCompleteAddressResponseModel.getPredictions());
                }
            }
        });


    }

    public String getPlaceAutoCompleteUrl(String input, String selectedLat, String selectedLong) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/place/autocomplete/json");
        urlString.append("?input=");
        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        urlString.append("&location=");
        urlString.append(selectedLat + "," + selectedLong); // append lat long of current location to show nearby results.
        urlString.append("&radius=500&language=en");
        urlString.append("&key=" + appSettings.getApiMapKey());

        Log.d("FINAL URL:::   ", urlString.toString());
        return urlString.toString();
    }

    public String getLocationSearchUrl(String latitude, String longitude) {

        return "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true&key=" + appSettings.getApiMapKey();
    }

    public String getLatLngByPlaceID(String placeID) {

        return "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeID + "&key=" + appSettings.getApiMapKey();

    }


    private void setAdapters(final List<Prediction> predictions) {
        autoCompleteList.setVisibility(View.VISIBLE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChooseLocationActivity.this, LinearLayoutManager.VERTICAL, false);
        autoCompleteList.setLayoutManager(linearLayoutManager);
        autoCompleteAdapter = new AutoCompleteAdapter(ChooseLocationActivity.this, predictions);
        autoCompleteList.setAdapter(autoCompleteAdapter);
        autoCompleteAdapter.notifyDataSetChanged();
        autoCompleteAdapter.setOnClickListner(new onClickListener() {
            @Override
            public void onClicked(final int position) {

                InputForAPI inputForAPI = new InputForAPI(ChooseLocationActivity.this);
                inputForAPI.setUrl(getLatLngByPlaceID(predictions.get(position).getPlaceId()));
                inputForAPI.setFile(null);
                inputForAPI.setHeaders(new HashMap<String, String>());
                inputForAPI.setJsonObject(new JSONObject());
                chooseLocationViewModel.getLatitudeAndLongitudeFromId(inputForAPI).observe(ChooseLocationActivity.this, new Observer<GetLatLongFromIdResponseModel>() {
                    @Override
                    public void onChanged(@Nullable GetLatLongFromIdResponseModel getLatLongFromIdResponseModel) {
                        if (getLatLongFromIdResponseModel.getError()) {

                        } else {
                            searchBox.setText("");
                            String areaname = predictions.get(position).getDescription();
                            String desc_detail = predictions.get(position).getTerms().get(0).getValue();
                            selectedLat = String.valueOf(getLatLongFromIdResponseModel.getResult().getGeometry().getLocation().getLat());
                            selectedLong = String.valueOf(getLatLongFromIdResponseModel.getResult().getGeometry().getLocation().getLng());
                            selectedAddressName = areaname;
                            selectedAddressText = desc_detail;
                            finishIntent(selectedLat, selectedLong);
                        }
                    }
                });

            }
        });

    }

    private void finishIntent(String selectedLat, String selectedLong) {
        Intent intent = new Intent(ChooseLocationActivity.this, SaveLocationActivity.class);
        intent.putExtra(ConstantKeys.INTENTKEYS.FROM_ACTIVITY, ChooseLocationActivity.class.getSimpleName());
        intent.putExtra(ConstantKeys.INTENTKEYS.REASON_INTENT, ChooseLocationActivity.class.getSimpleName());
        intent.putExtra(ConstantKeys.INTENTKEYS.LATITUDE, selectedLat);
        intent.putExtra(ConstantKeys.INTENTKEYS.LONGITUDE, selectedLong);
        startActivityForResult(intent, 1);
//        finish();
    }


    private void initListeners() {
        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (searchBox.getText().toString().length() > 3) {
                    currentLocationWithAddressLayout.setVisibility(View.GONE);
                    searchPlaces();
                } else {
                    currentLocationWithAddressLayout.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        currentLocationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                processMyLocationCLick();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseLocationActivity.super.onBackPressed();
            }
        });

    }

    private void processMyLocationCLick() {

        if(appSettings.getLocationAvailable()){
            finishIntent(appSettings.getLastKnownLatitude(),appSettings.getLastKnownLongitude());
        }else{
            checkLocationStatus();
        }

//        if (!Utils.isGpsEnabled(ChooseLocationActivity.this)) {
//            askPermission();
//        } else {
//
//            finishIntent(appSettings.getLastKnownLatitude(), appSettings.getLastKnownLongitude());
//
//            /*InputForAPI inputForAPI = new InputForAPI(ChooseLocationActivity.this);
//            inputForAPI.setUrl(getLocationSearchUrl(appSettings.getLastKnownLatitude(), appSettings.getLastKnownLongitude()));
//            inputForAPI.setFile(null);
//            inputForAPI.setHeaders(new HashMap<String, String>());
//            inputForAPI.setJsonObject(new JSONObject());
//            chooseLocationViewModel.getCurrentAddress(inputForAPI).observe(this, new Observer<ChooseLocationResponseModel>() {
//                @Override
//                public void onChanged(@Nullable ChooseLocationResponseModel chooseLocationResponseModel) {
//                    if (chooseLocationResponseModel.getError()) {
//
//                    } else {
//                        selectedLat = "" + appSettings.getLastKnownLatitude();
//                        selectedLong = "" + appSettings.getLastKnownLongitude();
//                        selectedAddressName = chooseLocationResponseModel.getCityName();
//                        selectedAddressText = chooseLocationResponseModel.getCityLocation();
//                        finishIntent();
//                    }
//                }
//            });*/
//        }
    }

    @Override
    public void onLocationFetched(Location location) {
        finishIntent(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
    }

    @Override
    public void onLocationFailed() {
        finishIntent(null,null);
    }
}
