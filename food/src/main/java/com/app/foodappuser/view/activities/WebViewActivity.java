package com.app.foodappuser.view.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;

import butterknife.BindView;

public class WebViewActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R2.id.headingText)
    TextView headingText;

    @BindView(R2.id.webView)
    WebView webView;

    @BindView(R2.id.progressBar)
    ProgressBar progressBar;

    @BindView(R2.id.backButton)
    ImageView backButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

    }

    @Override
    public void onStart() {
        super.onStart();
        setValues();
        initListeners();
    }

    private void initListeners() {
        backButton.setOnClickListener(this);
    }


    private void setValues() {
        headingText.setText(getIntent().getStringExtra(ConstantKeys.HEADING));
        webView.setWebViewClient(new MyWebViewClient());
        progressBar.setVisibility(View.VISIBLE);
        webView.loadUrl(getIntent().getStringExtra(ConstantKeys.URL));
    }

    @Override
    public void onClick(View view) {
        if (view==backButton)
        {
            finish();
        }
    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            if (progressBar.getVisibility() != View.VISIBLE) {
                progressBar.setVisibility(View.VISIBLE);
            }


            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            System.out.println("on finish");
            if (progressBar.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.INVISIBLE);
            }

        }
    }

}
