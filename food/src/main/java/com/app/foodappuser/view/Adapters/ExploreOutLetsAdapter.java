package com.app.foodappuser.view.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.foodappuser.Model.Response.ExploreResponse.Outlet;
import com.app.foodappuser.Model.Response.SelectedTab;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.ImageLoader;
import com.app.foodappuser.Utilities.BaseUtils.ViewAnimationUtils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.InterFaces.OutletAdapterInterface;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.view.activities.RestaurantDetailsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ExploreOutLetsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Outlet> outlets;
    ImageLoader imageLoader;

    private Context context;
    private AppSettings appSettings;
    SelectedTab selectedTab;
    OutletAdapterInterface outletAdapterInterface;

    public ExploreOutLetsAdapter(Context context, List<Outlet> outlets) {
        this.context = context;
        this.outlets = outlets;
        imageLoader = new ImageLoader(context);
        selectedTab = new SelectedTab();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.outlet_items, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new Restaurant(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Restaurant holder = (Restaurant) viewHolder;
        appSettings = new AppSettings(context);
        final Outlet outletModel = outlets.get(position);
        holder.restaurantOutletsName.setText(outletModel.getOutletName());
        holder.restaurantOutletsRating.setText(outletModel.getAverageReview());
        holder.restaurantOutletsTime.setText(outletModel.getDisplayTime());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appSettings.setRestaurentId(""+outletModel.getRestaurantId());
                appSettings.setOutletId(""+outletModel.getOutletId());

//                if (appSettings.getSelectedTab() == 1) {
                outletAdapterInterface.setRecentSearches(outletModel.getOutletName(),
                        outletModel.getOutletId(),
                        outletModel.getCuisines(),
                        outletModel.getDisplayTime(),
                        outletModel.getAverageReview(),
                        outletModel.getDisplayCostForTwo(),
                        outletModel.getCouponEnabledForRestaurant(),
                        outletModel.getLongDescription());
//                }

                moveRestaurantDetailsPage(outletModel.getOutletId(), outletModel.getCuisines(), outletModel.getDisplayCostForTwo(), outletModel.getDisplayTime(), outletModel.getAverageReview(), outletModel.getOutletName(),outletModel.getCouponEnabledForRestaurant(),outletModel.getLongDescription());
            }
        });
    }

    private void moveRestaurantDetailsPage(String outletId, String cuisines, String displayCostForTwo, String displayTime, String averageReview, String outletName,String isCouponCodeEnabled,String couponDescription) {
        Intent intent = new Intent(context, RestaurantDetailsActivity.class);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_ID, outletId);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_NAME, outletName);
        intent.putExtra(ConstantKeys.INTENTKEYS.CUISINE_NAME, cuisines);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_MIN, displayTime);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_REVIEW, averageReview);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_COST_FOR_TWO, displayCostForTwo);
        intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_ENABLED, isCouponCodeEnabled);
        intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_LONG_DESCRIPTION, couponDescription);

        context.startActivity(intent);
        ViewAnimationUtils.startActivityTransaction((Activity) context);
    }

    public void setOnClickListner(OutletAdapterInterface outletAdapterInterface) {
        this.outletAdapterInterface = outletAdapterInterface;
    }


    @Override
    public int getItemCount() {
        return (outlets != null) ? outlets.size() : 0;
    }

    class Restaurant extends RecyclerView.ViewHolder {

        @BindView(R2.id.restaurantOutletsName)
        TextView restaurantOutletsName;
        @BindView(R2.id.restaurantOutletsRating)
        TextView restaurantOutletsRating;
        @BindView(R2.id.restaurantOutletsTime)
        TextView restaurantOutletsTime;

        Restaurant(View view) {
            super(view);
            ButterKnife.bind(this,itemView);
        }
    }
}