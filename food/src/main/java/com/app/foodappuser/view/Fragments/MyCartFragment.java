package com.app.foodappuser.view.Fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.CouponResponseModel;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Model.Response.ListDeliveryResponse.ListDeliveryResponseModel;
import com.app.foodappuser.Model.Response.ViewCartResponse.Address;
import com.app.foodappuser.Model.Response.ViewCartResponse.BillTotal;
import com.app.foodappuser.Model.Response.ViewCartResponse.Dish;
import com.app.foodappuser.Model.Response.ViewCartResponse.OutletDetails;
import com.app.foodappuser.Model.Response.ViewCartResponse.ViewCartResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.ImageLoader;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.InterFaces.DeliverAddressAdapterInterface;
import com.app.foodappuser.Utilities.InterFaces.ViewCartDishesAdapterInterface;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.Utilities.UiUtils.DialogViews;
import com.app.foodappuser.view.activities.ApplyCouponActivity;
import com.app.foodappuser.view.activities.PaymentMethodActivity;
import com.app.foodappuser.view.activities.SaveLocationActivity;
import com.app.foodappuser.view.Adapters.DeliveryAddressAdapter;
import com.app.foodappuser.view.Adapters.ExtraChargesAdapter;
import com.app.foodappuser.view.Adapters.TaxChargesAdapter;
import com.app.foodappuser.view.Adapters.ViewCartDishesAdapter;
import com.app.foodappuser.ViewModel.MyCartViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class MyCartFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = MyCartFragment.class.getSimpleName();
    private static final int COUPON_CODE_KEY = 101;
    View view;
    MyCartViewModel myCartViewModel;
    AppSettings appSettings;
    String outletId;
    JSONArray dishes;

    @BindView(R2.id.restaurantName)
    TextView restaurantName;
    @BindView(R2.id.restaurantImage)
    ImageView restaurantImage;
    @BindView(R2.id.restaurantLocation)
    TextView restaurantLocation;
    @BindView(R2.id.dishesRecycler)
    RecyclerView dishesRecycler;
    @BindView(R2.id.chargesRecycler)
    RecyclerView chargesRecycler;
    @BindView(R2.id.tooManyItemsLayout)
    View tooManyItemsLayout;
    @BindView(R2.id.progressBar)
    ProgressBar progressBar;
    @BindView(R2.id.contentLayout)
    View contentLayout;
    @BindView(R2.id.deliverAddressLayout)
    View deliverAddressLayout;

    ViewCartDishesAdapter viewCartDishesAdapter;

    TaxChargesAdapter taxChargesAdapter;

    ExtraChargesAdapter extraChargesAdapter;

    Dialog setDeliveryLocationDialog;
    @BindView(R2.id.deliverImage)
    ImageView deliverImage;
    @BindView(R2.id.deliverToText)
    TextView deliverToText;
    @BindView(R2.id.deliverAddressText)
    TextView deliverAddressText;
    @BindView(R2.id.deliverMinsText)
    TextView deliverMinsText;
    @BindView(R2.id.changeAddressButton)
    RelativeLayout changeAddressButton;
    @BindView(R2.id.proceedPayButton)
    RelativeLayout proceedPayButton;
    @BindView(R2.id.noCartItems)
    View noCartItems;
    @BindView(R2.id.noAddressLayout)
    View noAddressLayout;
    @BindView(R2.id.connectionLostLayout)
    View connectionLostLayout;
    @BindView(R2.id.retryButton)
    RelativeLayout retryButton;
    @BindView(R2.id.applyCouponLayout)
    RelativeLayout applyCouponLayout;
    @BindView(R2.id.couponAppliedLayout)
    RelativeLayout couponAppliedLayout;
    @BindView(R2.id.couponCode)
    TextView couponCode;
    @BindView(R2.id.suggestions)
    TextView suggestions;
    @BindView(R2.id.removeCoupon)
    ImageView removeCoupon;
    @BindView(R2.id.spacer)
    Space spacer;
    ImageView statusImage;

    Dialog showCouponSuccessFailureDialog;
    TextView couponTitle,couponErrortitle,couponErrorMessage, couponMessage, couponAmount,yayText,okCouponText;
    LinearLayout successLayout, failureLayout;
    LinearLayout addNewAddressButton;

    @BindView(R2.id.addNewAddres)
    RelativeLayout addNewAddres;

    RecyclerView deliveryAddressRecycler;

    DeliveryAddressAdapter deliveryAddressAdapter;

    ViewCartResponseModel mviewCartResponseModel;

    boolean fromViewPager = false;
    private boolean onActivityResultCalled = false;
    private String couponDiscountAmount="";

    @SuppressLint("ValidFragment")
    public MyCartFragment(boolean fromViewPager) {
        // Required empty public constructor
        this.fromViewPager = fromViewPager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mycart, container, false);
        ButterKnife.bind(this, view);
        myCartViewModel = ViewModelProviders.of(this).get(MyCartViewModel.class);
        appSettings = new AppSettings(getActivity());
        setDeliveryLocationDialog = DialogViews.showSetDeliveryLocation(getActivity());
        showCouponSuccessFailureDialog = DialogViews.showCouponSuccessFailureDialog(getActivity());
        addNewAddressButton = setDeliveryLocationDialog.findViewById(R.id.addNewAddressButton);
        successLayout = showCouponSuccessFailureDialog.findViewById(R.id.successLayout);
        failureLayout = showCouponSuccessFailureDialog.findViewById(R.id.failureLayout);
        couponTitle = showCouponSuccessFailureDialog.findViewById(R.id.title);
        couponMessage = showCouponSuccessFailureDialog.findViewById(R.id.message);
        couponAmount = showCouponSuccessFailureDialog.findViewById(R.id.amount);
        statusImage = showCouponSuccessFailureDialog.findViewById(R.id.statusImage);
        couponErrortitle = showCouponSuccessFailureDialog.findViewById(R.id.couponErrortitle);
        couponErrorMessage = showCouponSuccessFailureDialog.findViewById(R.id.couponErrorMessage);
        yayText = showCouponSuccessFailureDialog.findViewById(R.id.yayText);
        okCouponText = showCouponSuccessFailureDialog.findViewById(R.id.okCouponText);

        if(fromViewPager){
            spacer.setVisibility(View.VISIBLE);

        }else{
            spacer.setVisibility(View.GONE);
        }

        setListeners();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!onActivityResultCalled) {
            startDataLoading();
            onActivityResultCalled = false;
        }
    }

    private void startDataLoading() {
        try {
            if (myCartViewModel.checkIsAvailable()) {
                dishes = myCartViewModel.getData();
                outletId = myCartViewModel.getOutletId();
                addToCart();
            } else {
                viewNoCartItems();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setListeners() {
        proceedPayButton.setOnClickListener(this);
        changeAddressButton.setOnClickListener(this);
        addNewAddres.setOnClickListener(this);
        addNewAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDeliveryLocationDialog.dismiss();
                finishIntent(appSettings.getLastKnownLatitude(), appSettings.getLastKnownLongitude());
            }
        });

        yayText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCouponSuccessFailureDialog.dismiss();
            }
        });
        okCouponText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCouponSuccessFailureDialog.dismiss();
            }
        });
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDataLoading();
            }
        });

        applyCouponLayout.setOnClickListener(this);

        removeCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    removeCoupon();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void viewNoCartItems() {
        noCartItems.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        tooManyItemsLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.GONE);
        deliverAddressLayout.setVisibility(View.GONE);
    }

    private void finishIntent(String selectedLat, String selectedLong) {

        Intent intent = new Intent(getActivity(), SaveLocationActivity.class);
        intent.putExtra(ConstantKeys.INTENTKEYS.FROM_ACTIVITY, MyCartFragment.class.getSimpleName());
        intent.putExtra(ConstantKeys.INTENTKEYS.REASON_INTENT, MyCartFragment.class.getSimpleName());
        intent.putExtra(ConstantKeys.INTENTKEYS.LATITUDE, selectedLat);
        intent.putExtra(ConstantKeys.INTENTKEYS.LONGITUDE, selectedLong);
        startActivity(intent);
    }


    private void getDeliveryAddressData() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.OUTLET_ID, outletId);

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.LIST_DELIVERY_ADDRESS);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));

        myCartViewModel.getDeliveryAddress(inputForAPI).observe(this, new Observer<ListDeliveryResponseModel>() {
            @Override
            public void onChanged(@Nullable ListDeliveryResponseModel listDeliveryResponseModel) {
                if (listDeliveryResponseModel.getError()) {
                    Utils.showSnackBar(getActivity().findViewById(android.R.id.content), listDeliveryResponseModel.getErrorMessage());
                } else {
                    setAddresses(listDeliveryResponseModel);
                }
            }
        });
    }

    private void setAddresses(ListDeliveryResponseModel listDeliveryResponseModel) {

        deliveryAddressRecycler = setDeliveryLocationDialog.findViewById(R.id.deliveryAddress);
        LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        deliveryAddressRecycler.setLayoutManager(layout);
        deliveryAddressAdapter = new DeliveryAddressAdapter(getActivity(), listDeliveryResponseModel.getListDeliveryAddress());
        deliveryAddressRecycler.setAdapter(deliveryAddressAdapter);
        deliveryAddressAdapter.setOnClickListener(new DeliverAddressAdapterInterface() {
            @Override
            public void setAddressId(String addressId) {
                appSettings.setAddressId(addressId);
                setDeliveryLocationDialog.dismiss();
                try {
                    viewCart(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void addToCart() throws JSONException {

        startLoading();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.OUTLET_ID, outletId);
        jsonObject.put(ConstantKeys.UDID, appSettings.getUdId());
        jsonObject.put(ConstantKeys.DISHES, dishes.toString());

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.ADD_TO_CART);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));

        myCartViewModel.addtoCart(inputForAPI).observe(this, new Observer<GeneralResponseModel>() {
            @Override
            public void onChanged(@Nullable GeneralResponseModel generalResponseModel) {
                if (generalResponseModel.getError()) {
                    if (generalResponseModel.getErrorMessage().equalsIgnoreCase(getResources().getString(R.string.no_internet_connection))) {

                        showNoInternetConnection();
                    } else {

                        Utils.showSnackBar(getActivity().findViewById(android.R.id.content), generalResponseModel.getErrorMessage());
                    }
                } else {
                    try {
                        viewCart(false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void startLoading() {
        progressBar.setVisibility(View.VISIBLE);
        tooManyItemsLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.GONE);
        deliverAddressLayout.setVisibility(View.GONE);
        noCartItems.setVisibility(View.VISIBLE);
        connectionLostLayout.setVisibility(View.GONE);
    }

    private void updateCartStartLoading() {
        progressBar.setVisibility(View.VISIBLE);
        tooManyItemsLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
        deliverAddressLayout.setVisibility(View.GONE);
        noCartItems.setVisibility(View.GONE);
        connectionLostLayout.setVisibility(View.GONE);
    }

    private void showNoInternetConnection() {
        progressBar.setVisibility(View.INVISIBLE);
        tooManyItemsLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.GONE);
        deliverAddressLayout.setVisibility(View.GONE);
        noCartItems.setVisibility(View.GONE);
        connectionLostLayout.setVisibility(View.VISIBLE);
    }

    private void viewCart(boolean showContent) throws JSONException {

        if (showContent) {
            updateCartStartLoading();
        } else {
            startLoading();
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.UDID, appSettings.getUdId());
        jsonObject.put(ConstantKeys.ADDRESS_ID, appSettings.getAddressId());

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.VIEW_CART);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));

        myCartViewModel.viewCart(inputForAPI).observe(this, new Observer<ViewCartResponseModel>() {
            @Override
            public void onChanged(@Nullable ViewCartResponseModel viewCartResponseModel) {

                stopLoading();

                if (viewCartResponseModel.getError()) {

                    if (viewCartResponseModel.getErrorMessage().equalsIgnoreCase("Your Cart is Empty,Add Something from the Menu")) {
                        myCartViewModel.deleteAllTableValues();
                        viewNoCartItems();
                    } else {
                        Utils.showSnackBar(getActivity().findViewById(android.R.id.content), viewCartResponseModel.getErrorMessage());
                    }

                } else {

                    mviewCartResponseModel = viewCartResponseModel;

                    if (viewCartResponseModel.getCouponStatus()) {
                        applyCouponLayout.setVisibility(View.GONE);
                        couponAppliedLayout.setVisibility(View.VISIBLE);
                        couponCode.setText(viewCartResponseModel.getCouponName());
                    } else {
                        applyCouponLayout.setVisibility(View.VISIBLE);
                        couponAppliedLayout.setVisibility(View.GONE);
                    }

                    if (viewCartResponseModel.getDishes().getIsTwoMany()) {
                        tooManyItemsLayout.setVisibility(View.VISIBLE);
                        deliverAddressLayout.setVisibility(View.GONE);

                    } else {

                        if (viewCartResponseModel.getDishes().getIsAddress()) {
                            noAddressLayout.setVisibility(View.GONE);
                            deliverAddressLayout.setVisibility(View.VISIBLE);

                        } else {
                            noAddressLayout.setVisibility(View.VISIBLE);
                            deliverAddressLayout.setVisibility(View.GONE);
                        }
                        tooManyItemsLayout.setVisibility(View.GONE);
                    }

                    setOutletDetails(viewCartResponseModel.getDishes().getOutletDetails());
                    setDishes(viewCartResponseModel.getDishes().getDishes());
                    setCharges(viewCartResponseModel.getDishes().getBillTotals());
                    setDeliverDetails(viewCartResponseModel.getDishes().getAddress());
                }
            }
        });
    }

    private void setDeliverDetails(Address address) {
        deliverToText.setText(address.getName());
        deliverAddressText.setText(address.getAddress());
        deliverMinsText.setText(address.getDisplayTime());
    }


    private void setCharges(List<BillTotal> charges) {
        LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        chargesRecycler.setLayoutManager(layout);
        extraChargesAdapter = new ExtraChargesAdapter(getActivity(), charges);
        chargesRecycler.setAdapter(extraChargesAdapter);
    }

    private void stopLoading() {
        progressBar.setVisibility(View.INVISIBLE);
        tooManyItemsLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
        deliverAddressLayout.setVisibility(View.GONE);
        noCartItems.setVisibility(View.GONE);
        connectionLostLayout.setVisibility(View.GONE);

    }


    private void setDishes(List<Dish> dishes) {
        LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        dishesRecycler.setLayoutManager(layout);
        viewCartDishesAdapter = new ViewCartDishesAdapter(getActivity(), dishes);
        dishesRecycler.setAdapter(viewCartDishesAdapter);
        viewCartDishesAdapter.setOnClickListener(new ViewCartDishesAdapterInterface() {

            @Override
            public void onIncreased(int cartId, int quantity, String id) {
                try {
                    updateCart(cartId, quantity, id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDecreased(int cartId, int quantity, String id) {
                try {
                    updateCart(cartId, quantity, id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDeleted(int cartId, int quantity, String id) {
                try {
                    updateCart(cartId, quantity, id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateCart(int cartId, final int quantity, final String id) throws JSONException {
        updateCartStartLoading();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.UDID, appSettings.getUdId());
        jsonObject.put(ConstantKeys.OUTLET_ID, outletId);
        jsonObject.put(ConstantKeys.QUANTITY, quantity);
        jsonObject.put(ConstantKeys.CART_ID, cartId);

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));
        inputForAPI.setUrl(UrlHelper.UPDATE_CART);
        inputForAPI.setJsonObject(jsonObject);

        myCartViewModel.updateCart(inputForAPI).observe(this, new Observer<GeneralResponseModel>() {
            @Override
            public void onChanged(@Nullable GeneralResponseModel generalResponseModel) {
                stopLoading();
                if (generalResponseModel.getError()) {
                    Utils.showSnackBar(getActivity().findViewById(android.R.id.content), generalResponseModel.getErrorMessage());
                } else {
                    try {
                        viewCart(true);
                        myCartViewModel.updateDb(quantity, id);
                        myCartViewModel.updateCartItems(outletId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void setOutletDetails(OutletDetails outletDetails) {
        restaurantName.setText(outletDetails.getOutletName());
        restaurantLocation.setText(outletDetails.getOutletArea());
        ImageLoader imageLoader = new ImageLoader(getActivity());
        imageLoader.load(outletDetails.getOutletImage(), restaurantImage);
    }


    @Override
    public void onClick(View v) {
        if (v == proceedPayButton) {
            moveToPaymentPage();
        }
        if (v == changeAddressButton) {
            setDeliveryLocationDialog.show();
            try {
                getDeliveryAddressData();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (addNewAddres == v) {
            finishIntent(appSettings.getLastKnownLatitude(), appSettings.getLastKnownLongitude());
        }
        if (applyCouponLayout == v) {
            Intent intent = new Intent(getActivity(), ApplyCouponActivity.class);
            if (fromViewPager) {
                getActivity().startActivityForResult(intent, COUPON_CODE_KEY);
            } else {
                startActivityForResult(intent, COUPON_CODE_KEY);
            }
        }
    }

    private void moveToPaymentPage() {

        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < mviewCartResponseModel.getDishes().getDishes().size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("cartId", mviewCartResponseModel.getDishes().getDishes().get(i).getCartId());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Bundle bundle = new Bundle();
        bundle.putString("outletId", outletId);
        bundle.putString("addressId", mviewCartResponseModel.getDishes().getAddress().getAddressId());
        int size = mviewCartResponseModel.getDishes().getBillTotals().size() - 1;
        float toPay = mviewCartResponseModel.getDishes().getBillTotals().get(size).getNetAmount();
        bundle.putString("netAmount", String.valueOf(toPay));
        bundle.putString("cartId", String.valueOf(jsonArray));
        bundle.putString(ConstantKeys.COUPON_NAME, mviewCartResponseModel.getCouponName());
        bundle.putString(ConstantKeys.DISCOUNT, couponDiscountAmount);
        bundle.putString(ConstantKeys.SUGGESTIONS, suggestions.getText().toString());

        Intent intent = new Intent(getActivity(), PaymentMethodActivity.class);
        intent.putExtra("bundle", bundle);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult: " + requestCode);
        if (requestCode == COUPON_CODE_KEY) {
            if (resultCode == Activity.RESULT_OK) {
                onActivityResultCalled = true;
                String couponName = data.getStringExtra(ConstantKeys.COUPON_NAME);
                Log.e(TAG, "onActivityResult: " + couponName);
                try {
                    addCoupon(couponName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void addCoupon(String couponName) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.UDID, appSettings.getUdId());
        jsonObject.put(ConstantKeys.OUTLET_ID, outletId);
        jsonObject.put(ConstantKeys.COUPON_CODE, couponName);

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));
        inputForAPI.setUrl(UrlHelper.ADD_COUPON);
        inputForAPI.setJsonObject(jsonObject);

        myCartViewModel.addCoupon(inputForAPI).observe(this, new Observer<CouponResponseModel>() {
            @Override
            public void onChanged(@Nullable CouponResponseModel couponResponseModel) {
//                if (couponResponseModel.getError()) {
//                    Utils.showSnackBar(getActivity().findViewById(android.R.id.content), couponResponseModel.getErrorMessage());
//                } else {
                    if (!couponResponseModel.getError()) {
                        showCouponSuccessDialog(couponResponseModel);
                        try {
                            viewCart(true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        showCouponFailureDialog(couponResponseModel);
                    }
//                }
            }
        });
    }

    private void showCouponFailureDialog(CouponResponseModel couponResponseModel) {
        showCouponSuccessFailureDialog.show();
        successLayout.setVisibility(View.GONE);
        couponAmount.setVisibility(View.GONE);
        failureLayout.setVisibility(View.VISIBLE);
        couponErrortitle.setText(getActivity().getResources().getString(R.string.oops));
        couponErrorMessage.setText(couponResponseModel.getErrorMessage());
        applyCouponLayout.setVisibility(View.VISIBLE);
        statusImage.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.warning_icon_with_border));
        couponAppliedLayout.setVisibility(View.GONE);
    }

    private void showCouponSuccessDialog(CouponResponseModel couponResponseModel) {
        showCouponSuccessFailureDialog.show();
        successLayout.setVisibility(View.VISIBLE);
        failureLayout.setVisibility(View.GONE);
        couponAmount.setVisibility(View.VISIBLE);
        couponTitle.setText(couponResponseModel.getTitle());
        couponMessage.setText(couponResponseModel.getMessage());
        couponDiscountAmount=couponResponseModel.getAmount();
        couponAmount.setText(new AppSettings(getActivity()).getCurrency()+couponResponseModel.getAmount());
        statusImage.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.offer_icon_with_border));
        applyCouponLayout.setVisibility(View.GONE);
        couponAppliedLayout.setVisibility(View.VISIBLE);
        couponCode.setText(couponResponseModel.getCouponName());

    }

    private void removeCoupon() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.UDID, appSettings.getUdId());

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));
        inputForAPI.setUrl(UrlHelper.REMOVE_COUPON);
        inputForAPI.setJsonObject(jsonObject);

        myCartViewModel.generalResponseModelLiveData(inputForAPI).observe(this, new Observer<GeneralResponseModel>() {
            @Override
            public void onChanged(@Nullable GeneralResponseModel generalResponseModel) {
                if (generalResponseModel.getError()) {
                    Utils.showSnackBar(getActivity().findViewById(android.R.id.content), generalResponseModel.getErrorMessage());
                } else {
                    applyCouponLayout.setVisibility(View.VISIBLE);
                    couponAppliedLayout.setVisibility(View.GONE);
                    try {
                        viewCart(true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }

    public void refresh() {
        startDataLoading();
    }
}
