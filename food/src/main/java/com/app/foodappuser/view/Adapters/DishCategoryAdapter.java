package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ListDishesResponse.Dish;
import com.app.foodappuser.Model.Response.ListDishesResponse.DishItems;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.InterFaces.DishItemChanged;
import com.app.foodappuser.Utilities.InterFaces.DishesAdapterInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DishCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final String TAG = DishCategoryAdapter.class.getSimpleName();
    DishesAdapterInterface dishesAdapterInterface;
    List<Dish> dishes = new ArrayList<>();
    DishItemChanged dishItemChanged;

    DishItemsAdapter dishItemGridAdapter;
    DishItemsAdapter dishItemNormalAdapter;
    DishSubCategoryAdapter dishSubCategoryAdapter;


    private Context context;
    private boolean isVegSwitch;

    public DishCategoryAdapter(Context context, List<Dish> dishes, boolean isVegSwitch) {
        this.context = context;
        this.dishes = dishes;
        this.isVegSwitch = isVegSwitch;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.dish_category_item, parent, false);

        RecyclerView.ViewHolder viewHolder;
        viewHolder = new DishCategoryHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

        DishCategoryHolder holder = (DishCategoryHolder) viewHolder;
        Dish dish = dishes.get(position);
        holder.categoryName.setText(dish.getCategoryName());

        Utils.log(TAG,"Items changed "+position);

        if (dish.getIsRecommended() == 1) {
            setGridAdapter(holder, dish);
        } else {

            if (dish.getIsHavingSubCategory() == 0) {
                Utils.log(TAG, "Adapter position " + position);

                setDishesItemAdapter(holder, dish);
            } else {
                setSubCategoryAdapter(holder, dish);
            }
        }
    }

    private void setSubCategoryAdapter(DishCategoryHolder holder, Dish dish) {

        holder.categoryRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        dishSubCategoryAdapter = new DishSubCategoryAdapter(context, dish.getSubCategoryValues(), dishesAdapterInterface, isVegSwitch);
        holder.categoryRecyclerView.setAdapter(dishSubCategoryAdapter);
        holder.bottomView.setVisibility(View.INVISIBLE);
    }

    private void setDishesItemAdapter(DishCategoryHolder holder, Dish dish) {
        holder.categoryRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        dishItemNormalAdapter = new DishItemsAdapter(context, dish.getCategoryValues(), dish.getVegOnlyValues(), false, dishesAdapterInterface, isVegSwitch);
        holder.categoryRecyclerView.setAdapter(dishItemNormalAdapter);
    }

    private void setGridAdapter(DishCategoryHolder holder, Dish dish) {
        holder.categoryRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        dishItemGridAdapter = new DishItemsAdapter(context, dish.getCategoryValues(), dish.getVegOnlyValues(), true, dishesAdapterInterface, isVegSwitch);
        holder.categoryRecyclerView.setAdapter(dishItemGridAdapter);
    }

    @Override
    public int getItemCount() {
        return (dishes != null) ? dishes.size() : 0;
    }

    public void setOnClickListner(DishesAdapterInterface dishesAdapterInterface) {
        this.dishesAdapterInterface = dishesAdapterInterface;
    }

    public void setIsVeg(boolean isChecked) {
        this.isVegSwitch = isChecked;
    }

    public void onItemChanged(DishItems dish, String quantity) {

        Utils.log(TAG,"CategoryId= "+dish.getCategoryId());

        for (int i = 0; i < dishes.size(); i++) {

            if (!dishes.get(i).getCategoryId().equalsIgnoreCase(dish.getCategoryId())) {

                if (dishes.get(i).getIsRecommended() == 1) {

                    for (int j = 0; j < dishes.get(i).getCategoryValues().size(); j++) {

                        if (dishes.get(i).getCategoryValues().get(j).getDishId().equalsIgnoreCase(dish.getDishId())) {

                            dishes.get(i).getCategoryValues().get(j).setQuantity(Integer.valueOf(quantity));

                            dishItemGridAdapter.onItemChanged(j,Integer.valueOf(quantity));

                            Utils.log(TAG,"TYPE - RECOMENDED");

//                            notifyItemChanged(i);
                        }
                    }

                } else {

                    if (dishes.get(i).getIsHavingSubCategory() == 0) {

                        for (int j = 0; j < dishes.get(i).getCategoryValues().size(); j++) {

                            if (dishes.get(i).getCategoryValues().get(j).getDishId().equalsIgnoreCase(dish.getDishId())) {

                                dishes.get(i).getCategoryValues().get(j).setQuantity(Integer.valueOf(quantity));

                                Utils.log(TAG,"TYPE - NOSUBCATEGORY");

//                                dishItemNormalAdapter.onItemChanged(j,Integer.valueOf(quantity));

                                notifyItemChanged(i);

                            }
                        }

                    } else {

                        for (int j = 0; j < dishes.get(i).getSubCategoryValues().size(); j++) {

                            for (int k = 0; k < dishes.get(i).getSubCategoryValues().get(j).getDishItems().size(); k++) {

                                if (dishes.get(i).getSubCategoryValues().get(j).getDishItems().get(k).getDishId().equalsIgnoreCase(dish.getDishId())) {

                                    dishes.get(i).getSubCategoryValues().get(j).getDishItems().get(k).setQuantity(Integer.valueOf(quantity));

                                    Utils.log(TAG,"TYPE - HAVINGSUBCATEGORY");

                                    dishSubCategoryAdapter.onDishQuantityChanged(dishes.get(i).getSubCategoryValues().get(j).getDishItems(), dish, quantity);

//                                    notifyItemChanged(i);
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    class DishCategoryHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.categoryName)
        TextView categoryName;
        @BindView(R2.id.categoryRecyclerView)
        RecyclerView categoryRecyclerView;
        @BindView(R2.id.bottomView)
        View bottomView;

        DishCategoryHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);

        }
    }
}