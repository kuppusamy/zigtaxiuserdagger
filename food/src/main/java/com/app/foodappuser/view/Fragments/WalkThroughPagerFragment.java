package com.app.foodappuser.view.Fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class WalkThroughPagerFragment extends Fragment {

    public static WalkThroughPagerFragment newInstance(int image) {

        Bundle args = new Bundle();

        WalkThroughPagerFragment fragment = new WalkThroughPagerFragment();
        args.putInt(ConstantKeys.BANNER_IMAGE,image);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_walk_through, container, false);
        ImageView walkThroughBgImage=view.findViewById(R.id.walkThroughBgImage);
//        walkThroughBgImage.setImageResource(getArguments().getInt(ConstantKeys.BANNER_IMAGE));

        Glide.with(getActivity()).load(getArguments().getInt(ConstantKeys.BANNER_IMAGE)).apply(new RequestOptions().centerCrop()).into(walkThroughBgImage);
//        walkThroughBgImage.setImageResource(getArguments().getInt(ConstantKeys.BANNER_IMAGE));
        return view;
    }
}
