package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.PastOrderResponse.Charge;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PastOrderChargesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    List<Charge> charges;


    public PastOrderChargesAdapter(Context context, List<Charge> charges) {
        this.context = context;
        this.charges = charges;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.past_order_charges_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new ChargesViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        final ChargesViewHolder holder = (ChargesViewHolder) viewHolder;
        Charge charge = charges.get(i);

        holder.chargesText.setText(charge.getDisplayKey());
        holder.chargesCost.setText(charge.getDisplayValue());

    }

    @Override
    public int getItemCount() {
        return (charges != null) ? charges.size() : 0;
    }


    class ChargesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.chargesText)
        TextView chargesText;
        @BindView(R2.id.chargesCost)
        TextView chargesCost;

        public ChargesViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}