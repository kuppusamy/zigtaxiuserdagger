package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ListDishesResponse.DishCustomization;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.InterFaces.CustomizationAdapterInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizableElementAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<DishCustomization> dishCustomizations = new ArrayList<>();
    Context context;
    CustomizationAdapterInterface customizationAdapterInterface;
    double dishPrice;

    public CustomizableElementAdapter(Context context, List<DishCustomization> dishCustomizations, double dishPrice) {
        this.context = context;
        this.dishCustomizations = dishCustomizations;
        this.dishPrice = dishPrice;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView;
        itemView = LayoutInflater.from(context).inflate(R.layout.customizable_dish_element, viewGroup, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new CustomizableElementHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        CustomizableElementHolder customizableElementHolder = (CustomizableElementHolder) viewHolder;
        DishCustomization dishCustomization = this.dishCustomizations.get(position);
        setViewsAndAdapter(customizableElementHolder, dishCustomization, position);
    }

    private void setViewsAndAdapter(CustomizableElementHolder customizableElementHolder, DishCustomization dishCustomization, int position) {
        customizableElementHolder.dishName.setText(dishCustomization.getCustomizableName());
        if (dishCustomization.getCustomizableType().equalsIgnoreCase("single")) {
            customizableElementHolder.customizableDishItemsRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            CustomizableDishItemSingleAdapter adapter = new CustomizableDishItemSingleAdapter(context, dishCustomization.getCustomizableDishItems(), customizationAdapterInterface);
            customizableElementHolder.customizableDishItemsRecycler.setAdapter(adapter);
        } else if (dishCustomization.getCustomizableType().equalsIgnoreCase("multiple")) {
            customizableElementHolder.customizableDishItemsRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            CustomizableDishItemMultipleAdapter adapter = new CustomizableDishItemMultipleAdapter(context, dishCustomization.getCustomizableDishItems(), customizationAdapterInterface);
            customizableElementHolder.customizableDishItemsRecycler.setAdapter(adapter);
        }
    }

    @Override
    public int getItemCount() {
        return (dishCustomizations != null) ? dishCustomizations.size() : 0;
    }

    public void setOnClickListener(CustomizationAdapterInterface customizationAdapterInterface) {
        this.customizationAdapterInterface = customizationAdapterInterface;
    }

    class CustomizableElementHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.dishName)
        TextView dishName;
        @BindView(R2.id.customizableDishItemsRecycler)
        RecyclerView customizableDishItemsRecycler;

        public CustomizableElementHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
