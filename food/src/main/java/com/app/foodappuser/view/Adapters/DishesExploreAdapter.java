package com.app.foodappuser.view.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.foodappuser.Model.Response.ExploreResponse.ListDish;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.InterFaces.onClickListener;
import com.app.foodappuser.view.activities.RestaurantDetailsActivity;

import java.util.List;

public class DishesExploreAdapter extends RecyclerView.Adapter<DishesExploreAdapter.DishesViewHolder> {
    List<ListDish> listDishes;
    Context context;

    public DishesExploreAdapter(Context context, List<ListDish> listDishes) {
        this.context=context;
        this.listDishes=listDishes;
    }

    @NonNull
    @Override
    public DishesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dishes_explore_item, parent, false);
        DishesViewHolder viewHolder = new DishesViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DishesViewHolder holder, int position) {
        holder.restaurantName.setText(listDishes.get(position).getRestaurantName());
        holder.restaurantRating.setText(listDishes.get(position).getAverageReview());
        holder.clock.setText(listDishes.get(position).getDisplayTime());
        holder.cusine_text.setText(listDishes.get(position).getCuisines());
        holder.dishes_explore_recycler.setAdapter(new DishesDetailExploreAdapter(context,listDishes.get(position).getDishesList(), new onClickListener() {
            @Override
            public void onClicked(int pos) {
                moveRestaurantDetailsPage(String.valueOf(listDishes.get(position).getRestaurantId()),
                        listDishes.get(position).getOutletName(),
                        listDishes.get(position).getCuisines(),
                        listDishes.get(position).getDisplayTime(),
                        listDishes.get(position).getAverageReview(),
                        listDishes.get(position).getDisplayCostForTwo(),
                        listDishes.get(position).getCouponEnabledForRestaurant(),
                        listDishes.get(position).getLongDescription());

            }
        }));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveRestaurantDetailsPage(String.valueOf(listDishes.get(position).getRestaurantId()),
                        listDishes.get(position).getOutletName(),
                        listDishes.get(position).getCuisines(),
                        listDishes.get(position).getDisplayTime(),
                        listDishes.get(position).getAverageReview(),
                        listDishes.get(position).getDisplayCostForTwo(),
                        listDishes.get(position).getCouponEnabledForRestaurant(),
                        listDishes.get(position).getLongDescription());
            }
        });
    }

    private void moveRestaurantDetailsPage(String outletId, String outletName,
                                           String cuisines, String displayTime,
                                           String averageReview,
                                           String displayCostForTwo,
                                           String isCouponCodeEnabled,
                                           String couponDescription) {
        Intent intent = new Intent(context, RestaurantDetailsActivity.class);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_ID, outletId);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_NAME, outletName);
        intent.putExtra(ConstantKeys.INTENTKEYS.CUISINE_NAME, cuisines);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_MIN, displayTime);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_REVIEW, averageReview);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_COST_FOR_TWO, displayCostForTwo);
        intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_ENABLED, isCouponCodeEnabled);
        intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_LONG_DESCRIPTION, couponDescription);


        context.startActivity(intent);
    }


    @Override
    public int getItemCount() {
        return listDishes!=null?listDishes.size():0;
    }

    class DishesViewHolder extends RecyclerView.ViewHolder {
        RecyclerView dishes_explore_recycler;
        TextView restaurantName,restaurantRating,clock,cusine_text;
        public DishesViewHolder(@NonNull View itemView) {
            super(itemView);
            dishes_explore_recycler=itemView.findViewById(R.id.dishes_explore_recycler);
            restaurantName=itemView.findViewById(R.id.restaurantName);
            restaurantRating=itemView.findViewById(R.id.restaurantRating);
            cusine_text=itemView.findViewById(R.id.cusine_text);
            clock=itemView.findViewById(R.id.clock);
        }
    }
}
