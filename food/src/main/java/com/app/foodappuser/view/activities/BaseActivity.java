package com.app.foodappuser.view.activities;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class  BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }



    public void onStart() {
        super.onStart();
        ButterKnife.bind(this);
    }

    protected void getDmeo(){}




}
