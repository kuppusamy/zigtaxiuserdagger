package com.app.foodappuser.view.Fragments;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ExploreResponse.ExploreResponseModel;
import com.app.foodappuser.Model.Response.SelectedTab;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.BaseUtils.onExploreResultsFetched;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.view.Adapters.RecentSearchesAdapter;
import com.app.foodappuser.ViewModel.ExploreViewModel;
import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExploreFragment extends Fragment {

    @BindView(R2.id.tabLayout)
    SlidingTabLayout tabLayout;

    @BindView(R2.id.pager)
    ViewPager pager;

    @BindView(R2.id.exploreEditText)
    EditText exploreEditText;

    @BindView(R2.id.restaurantApiContentLayout)
    View restaurantApiContentLayout;

    @BindView(R2.id.restaurantLocaleContentLayout)
    View restaurantLocaleContentLayout;

    @BindView(R2.id.recentSearchRecycler)
    RecyclerView recentSearchRecycler;

    @BindView(R2.id.clearText)
    ImageView clearText;

    private List<Fragment> FragmentList = new ArrayList();

    private View view;

    private ExploreViewModel exploreViewModel;

    AppSettings appSettings;

    SelectedTab selectedTab;

    RecentSearchesAdapter recentSearchesAdapter;

    public ExploreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_explore, container, false);
        ButterKnife.bind(this, view);
        exploreViewModel = ViewModelProviders.of(this).get(ExploreViewModel.class);
        selectedTab=new SelectedTab();
        appSettings=new AppSettings(getActivity());
        clearText.setVisibility(View.GONE);

        tabLayoutSetups();
        editTextListeners();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setRecentSearchAdapter();

    }

    private void setRecentSearchAdapter() {

        LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recentSearchRecycler.setLayoutManager(layout);
        recentSearchesAdapter = new RecentSearchesAdapter(getActivity(), appSettings.getSearches());
        recentSearchRecycler.setAdapter(recentSearchesAdapter) ;

    }

    private void editTextListeners() {
        exploreEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedTab.setSelectedTab(1);
                if(count>=1){
                    clearText.setVisibility(View.VISIBLE);
                }else{
                    clearText.setVisibility(View.GONE);
                }
                if(count>2){
                    try {
                        startSearching();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    restaurantApiContentLayout.setVisibility(View.VISIBLE);
                    restaurantLocaleContentLayout.setVisibility(View.GONE);

                }else{
                    restaurantApiContentLayout.setVisibility(View.GONE);
                    restaurantLocaleContentLayout.setVisibility(View.VISIBLE);

                    ExploreResponseModel listRestaurantResponseModel=new ExploreResponseModel();
                    EventBus.getDefault().post(new onExploreResultsFetched(false,listRestaurantResponseModel,exploreEditText.getText().toString(), exploreViewModel));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        clearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exploreEditText.setText("");
            }
        });
    }

    private void startSearching() throws JSONException {

        AppSettings appSettings=new AppSettings(getActivity());

        JSONObject jsonObject=new JSONObject();
        jsonObject.put(ConstantKeys.LATITUDE,appSettings.getLastKnownLatitude());
        jsonObject.put(ConstantKeys.LONGITUDE,appSettings.getLastKnownLongitude());
        jsonObject.put(ConstantKeys.RESTAURANT_NAME,exploreEditText.getText().toString());

        InputForAPI inputForAPI=new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.SEARCH_RESTAURANT_AND_DISHES);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));
        exploreViewModel.getRestaurantsNearMe(inputForAPI).observe(this, new Observer<ExploreResponseModel>() {
            @Override
            public void onChanged(@Nullable ExploreResponseModel listRestaurantResponseModel) {
                if(listRestaurantResponseModel.getError()){
                    EventBus.getDefault().post(new onExploreResultsFetched(false,listRestaurantResponseModel,exploreEditText.getText().toString(),exploreViewModel));
                }else{
                    EventBus.getDefault().post(new onExploreResultsFetched(true,listRestaurantResponseModel,exploreEditText.getText().toString(),exploreViewModel));
                }
            }
        });
    }

    private void tabLayoutSetups() {

        final String[] mTitles = {
                getActivity().getResources().getString(R.string.restaurant),
                getActivity().getResources().getString(R.string.dishes)};

        Viewpager viewpager=new Viewpager(getFragmentManager());

        viewpager.addFragment(new RestaurantExploreFragment());
        viewpager.addFragment(new DishesExploreFragment());

        tabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                pager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        pager.setAdapter(viewpager);

        tabLayout.setViewPager(pager,mTitles);

        setFontsinTab();
    }

    private void setFontsinTab() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {

            if(j==0){
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        Typeface font = Typeface.createFromAsset( getActivity().getAssets(),"fonts/Muli-SemiBold.ttf");

                        ((TextView) tabViewChild).setTypeface(font);
                    }
                }
            }else{
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        Typeface font = Typeface.createFromAsset( getActivity().getAssets(),"fonts/Muli-SemiBold.ttf");

                        ((TextView) tabViewChild).setTypeface(font);
                    }
                }
            }
        }
    }

    public class Viewpager extends FragmentPagerAdapter {

        public Viewpager(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment) {
            FragmentList.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0)
            {
                fragment = new RestaurantExploreFragment();
            }

            if(position==1){
                fragment = new DishesExploreFragment();

            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
