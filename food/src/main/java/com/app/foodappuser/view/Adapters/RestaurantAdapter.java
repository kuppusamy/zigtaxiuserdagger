package com.app.foodappuser.view.Adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ListRestaurantResponse.OutletModel;
import com.app.foodappuser.Model.Response.ListRestaurantResponse.RestaurantModel;
import com.app.foodappuser.Model.Response.SelectedTab;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.ImageLoader;
import com.app.foodappuser.Utilities.BaseUtils.ViewAnimationUtils;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.InterFaces.OutletAdapterInterface;
import com.app.foodappuser.Utilities.InterFaces.RestaurantAdapterInterface;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.Utilities.UiUtils.ExpandableLayout;
import com.app.foodappuser.Utilities.UiUtils.ShadowView.ShadowView;
import com.app.foodappuser.view.activities.RestaurantDetailsActivity;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RestaurantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ImageLoader imageLoader;

    private Activity context;
    private List<RestaurantModel> restaurantArray;
    private AppSettings appSettings;
    private String TAG = RestaurantAdapter.class.getSimpleName();
    SelectedTab selectedTab;
    RestaurantAdapterInterface restaurantAdapterInterface;

    public RestaurantAdapter(Activity context, List<RestaurantModel> restaurantArray) {
        this.context = context;
        this.restaurantArray = restaurantArray;
        imageLoader = new ImageLoader(context);
        selectedTab = new SelectedTab();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.restaurant_item_2, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new Restaurant(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final Restaurant holder = (Restaurant) viewHolder;

        appSettings = new AppSettings(context);
        final RestaurantModel restaurantModel = restaurantArray.get(position);
        holder.restaurantName.setText(restaurantModel.getRestaurantName());
        if (restaurantModel.getCuisines().length() != 0) {
            holder.restaurantCuisineType.setVisibility(View.VISIBLE);
        } else {
            holder.restaurantCuisineType.setVisibility(View.GONE);
        }

        holder.restaurantCuisineType.setText(restaurantModel.getCuisines());
        holder.restaurantDeliveryTime.setText(restaurantModel.getDisplayTime());
        holder.restaurantPrice.setText(restaurantModel.getDisplayCostForTwo());
        holder.restaurantRating.setText(restaurantModel.getAverageReview());
        holder.coupon_description.setText(restaurantModel.getShortDescription());
        imageLoader.load(restaurantModel.getRestaurantImage(), holder.restaurantImage);

        setOutletsAdapter(holder, restaurantModel.getOutlets());
        if (restaurantModel.getOutlets().size() > 1) {
            holder.restaurantOutletsCountLayout.setVisibility(View.VISIBLE);
            holder.restaurantOutletsCount.setText(Utils.getFormattedString(context, R.string.outlet_count_template, "" + restaurantModel.getOutlets().size()));
        } else {
            holder.restaurantOutletsCountLayout.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (restaurantModel.getOutlets().size() > 1) {
                    holder.expandableLayout.toggleExpansion();
                } else {
                    appSettings.setRestaurentId(restaurantModel.getRestaurantId());
                    appSettings.setOutletId(restaurantModel.getOutlets().get(0).getOutletId());
                    moveRestaurantDetailsPage(restaurantModel.getOutlets().get(0).getOutletId(),
                            restaurantModel.getOutlets().get(0).getOutletName(),
                            restaurantModel.getOutlets().get(0).getCuisines(),
                            restaurantModel.getOutlets().get(0).getDisplayTime(),
                            restaurantModel.getOutlets().get(0).getAverageReview(),
                            restaurantModel.getOutlets().get(0).getDisplayCostForTwo(),
                            restaurantModel.getOutlets().get(0).getCouponEnabledForRestaurant(),
                            restaurantModel.getOutlets().get(0).getLongDescription());

                    restaurantAdapterInterface.setRecentSearches(restaurantModel.getOutlets().get(0).getOutletName(),
                            restaurantModel.getOutlets().get(0).getOutletId(),
                            restaurantModel.getOutlets().get(0).getCuisines(),
                            restaurantModel.getOutlets().get(0).getDisplayTime(),
                            restaurantModel.getOutlets().get(0).getAverageReview(),
                            restaurantModel.getOutlets().get(0).getDisplayCostForTwo(),
                            restaurantModel.getOutlets().get(0).getCouponEnabledForRestaurant(),
                            restaurantModel.getOutlets().get(0).getLongDescription());

                }
            }
        });

    }

    public void setOnClickListner(RestaurantAdapterInterface restaurantAdapterInterface) {
        this.restaurantAdapterInterface = restaurantAdapterInterface;
    }

    private void setOutletsAdapter(Restaurant holder, List<OutletModel> outlets) {
        LinearLayoutManager layout = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.outLetRecyclerView.setLayoutManager(layout);
        OutLetsAdapter outLetsAdapter = new OutLetsAdapter(context, outlets);
        holder.outLetRecyclerView.setAdapter(outLetsAdapter);
        outLetsAdapter.setOnClickListner(new OutletAdapterInterface() {
            @Override
            public void onClick() {

            }

            @Override
            public void setRecentSearches(String outletName, String outletId, String cuisines, String displayTime, String averageReview, String displayCostForTwo, String couponEnabledForRestaurant, String longDescription) {
                restaurantAdapterInterface.setRecentSearches(outletName, outletId, cuisines, displayTime, averageReview, displayCostForTwo, couponEnabledForRestaurant,longDescription);
            }
        });
    }

    private void moveRestaurantDetailsPage(String outletId, String outletName,
                                           String cuisines, String displayTime,
                                           String averageReview,
                                           String displayCostForTwo,
                                           String isCouponCodeEnabled,
                                           String couponDescription) {
        Intent intent = new Intent(context, RestaurantDetailsActivity.class);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_ID, outletId);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_NAME, outletName);
        intent.putExtra(ConstantKeys.INTENTKEYS.CUISINE_NAME, cuisines);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_MIN, displayTime);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_REVIEW, averageReview);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_COST_FOR_TWO, displayCostForTwo);
        intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_ENABLED, isCouponCodeEnabled);
        intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_LONG_DESCRIPTION, couponDescription);


        context.startActivity(intent);
        ViewAnimationUtils.startActivityTransaction(context);
    }

    @Override
    public int getItemCount() {
        return (restaurantArray != null) ? restaurantArray.size() : 0;
    }

    class Restaurant extends RecyclerView.ViewHolder {

        @BindView(R2.id.restaurantImage)
        RoundedImageView restaurantImage;
        @BindView(R2.id.restaurantName)
        TextView restaurantName;
        @BindView(R2.id.restaurantCuisineType)
        TextView restaurantCuisineType;
        @BindView(R2.id.restaurantRating)
        TextView restaurantRating;
        @BindView(R2.id.restaurantDeliveryTime)
        TextView restaurantDeliveryTime;
        @BindView(R2.id.restaurantPrice)
        TextView restaurantPrice;
        @BindView(R2.id.restaurantOutletsCount)
        TextView restaurantOutletsCount;
        @BindView(R2.id.restaurantOutletsCountLayout)
        LinearLayout restaurantOutletsCountLayout;
        @BindView(R2.id.shadow_view)
        ShadowView shadowView;
        @BindView(R2.id.outLetRecyclerView)
        RecyclerView outLetRecyclerView;
        @BindView(R2.id.outLetLayout)
        ShadowView outLetLayout;
        @BindView(R2.id.expandableLayout)
        ExpandableLayout expandableLayout;
        @BindView(R2.id.layout)
        LinearLayout layout;
        @BindView(R2.id.coupon_description)
        TextView coupon_description;


        Restaurant(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}