package com.app.foodappuser.view.Fragments;


import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ChooseLocationResponseModel;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Model.Response.GetCurrentAddressResponse.GetCurrentAddressResponseModel;
import com.app.foodappuser.Model.Response.ListRestaurantResponse.ListRestaurantResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.InterFaces.RestaurantAdapterInterface;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.BaseUtils.onLocationFetched;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.NearMeFragmentLogicInterface;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.Utilities.UiUtils.MyLinearSnapHelper;
import com.app.foodappuser.view.activities.ChooseLocationActivity;
import com.app.foodappuser.view.activities.MainActivity;
import com.app.foodappuser.view.activities.RatingActivity;
import com.app.foodappuser.view.Adapters.RestaurantAdapter;
import com.app.foodappuser.view.Adapters.RestaurantBannerImageAdapter;
import com.app.foodappuser.ViewModel.NearMeViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class NearMeFragment extends Fragment implements NearMeFragmentLogicInterface {

    private static final String TAG = NearMeFragment.class.getSimpleName();
    View view;

    @BindView(R2.id.bannerViewPager)
    RecyclerView bannerViewPager;

    @BindView(R2.id.swipeView)
    SwipeRefreshLayout swipeView;

    @BindView(R2.id.contentLayout)
    View contentLayout;

    @BindView(R2.id.loadingLayout)
    View loadingLayout;

    @BindView(R2.id.setDeliveryLocationLayout)
    View setDeliveryLocationLayout;

    @BindView(R2.id.locationlayout)
    LinearLayout locationlayout;


    @BindView(R2.id.restaurantRecyclerView)
    RecyclerView restaurantRecyclerView;

    @BindView(R2.id.restaurantCount)
    TextView restaurantCount;

    @BindView(R2.id.addressName)
    TextView addressName;

    @BindView(R2.id.addressText)
    TextView addressText;

    @BindView(R2.id.locationTypeLayout)
    LinearLayout locationTypeLayout;

    @BindView(R2.id.setDeliveryLocationButton)
    RelativeLayout setDeliveryLocationButton;

    @BindView(R2.id.restaurantLayout)
    NestedScrollView restaurantLayout;

    @BindView(R2.id.noRestaurantAvailable)
    LinearLayout noRestaurantAvailable;

    @BindView(R2.id.ratingLayout)
    View ratingLayout;

    @BindView(R2.id.rating_close)
    ImageView rating_close;

    //Connection Lost Layout
    @BindView(R2.id.retryButton)
    RelativeLayout retryButton;
    @BindView(R2.id.connectionLostLayout)
    View connectionLostLayout;

    NearMeViewModel nearMeViewModel;
    RestaurantAdapter restaurantAdapter;
    String addressId, isSavedAddress, selectedLat = "", selectedLong = "", selectedAddressName = "", selectedAddressText = "";
    AppSettings appSettings;

    boolean iscalled = true;


    private ListRestaurantResponseModel responseModel;
    private String selectedCityName;


    public NearMeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_near_me, container, false);
        ButterKnife.bind(this, view);
        appSettings = new AppSettings(getActivity());
        nearMeViewModel = ViewModelProviders.of(this).get(NearMeViewModel.class);
        initListners();
        try {
            updateDeviceToken();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ((MainActivity) getActivity()).logicOfRestaurant(this);


        return view;
    }

    private void updateDeviceToken() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.DEVICE_TOKEN, appSettings.getFCMToken());
        jsonObject.put(ConstantKeys.OS_TYPE, "android");
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.UPDATE_DEVICE_TOKEN);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));

        nearMeViewModel.generalResponseModel(inputForAPI).observe(this, new Observer<GeneralResponseModel>() {
            @Override
            public void onChanged(@Nullable GeneralResponseModel generalResponseModel) {
                Utils.log(TAG, generalResponseModel.getErrorMessage());
            }
        });
    }


    private void getDataFromLocation() {
        Utils.log("isLocationFetched", appSettings.getIsLatLongFetched());
        Utils.log("isAppKilled", appSettings.getIsAppKilled());

        selectedLat = String.valueOf(appSettings.getLastKnownLatitude());
        selectedLong = String.valueOf(appSettings.getLastKnownLongitude());

        Utils.log("latitudeNear", appSettings.getLastKnownLatitude());
        Utils.log("longitudeNear", appSettings.getLastKnownLongitude());


        if (appSettings.getIsAppKilled().equalsIgnoreCase("") || appSettings.getIsAppKilled().equalsIgnoreCase(ConstantKeys.TRUE_STRING)) {
            if (!(appSettings.getIsLatLongFetched().equalsIgnoreCase("") || appSettings.getIsLatLongFetched().equalsIgnoreCase(ConstantKeys.FALSE_STRING))) {
                getAddressFromLatLong();
            }
        } else {
            try {
                getRestaurantData();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        iscalled = false;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLocationFetchedOrNot(onLocationFetched locationFetched) {

        if (iscalled) {
//            getDataFromLocation();
            iscalled = false;
        }
    }


    private void getAddressData() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.LATITUDE, appSettings.getLastKnownLatitude());
        jsonObject.put(ConstantKeys.LONGITUDE, appSettings.getLastKnownLongitude());
        jsonObject.put(ConstantKeys.LOCATION, selectedAddressText);
        jsonObject.put(ConstantKeys.CITY, selectedCityName);

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.GET_CURRENT_ADDRESS);
        HashMap<String, String> header = new HashMap<>();
        header.put(ConstantKeys.AUTHORIZATION, appSettings.getAccessToken());
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setFile(null);

        nearMeViewModel.getCurrentAddress(inputForAPI).observe(this, new Observer<GetCurrentAddressResponseModel>() {
            @Override
            public void onChanged(@Nullable GetCurrentAddressResponseModel getCurrentAddressResponseModel) {

                if (getCurrentAddressResponseModel.getError()) {

                    Utils.showSnackBar(swipeView, getCurrentAddressResponseModel.getErrorMessage());

                } else {

                    try {
                        getRestaurantData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }

    private void getAddressFromLatLong() {

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(getLocationSearchUrl(appSettings.getLastKnownLatitude(), appSettings.getLastKnownLongitude()));
        inputForAPI.setFile(null);
        inputForAPI.setHeaders(new HashMap<String, String>());
        inputForAPI.setJsonObject(new JSONObject());

        nearMeViewModel.getCurrentAddressFromLatLong(inputForAPI).observe(this, new Observer<ChooseLocationResponseModel>() {
            @Override
            public void onChanged(@Nullable ChooseLocationResponseModel chooseLocationResponseModel) {
                if (chooseLocationResponseModel.getError()) {

                    Utils.showSnackBar(swipeView, chooseLocationResponseModel.getErrorMessage());

                } else {
                    selectedAddressText = chooseLocationResponseModel.getFormatedAddress();
                    selectedCityName = chooseLocationResponseModel.getCityName();
                    try {
                        getAddressData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    public String getLocationSearchUrl(String latitude, String longitude) {

        return "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true&key=" + appSettings.getApiMapKey();
    }

    private void initListners() {

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getRestaurantData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        locationlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveSelectLocationActivity();

            }
        });

        setDeliveryLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveSelectLocationActivity();
            }
        });

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getDataFromLocation();
                try {
                    getRestaurantData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        rating_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingLayout.setVisibility(View.GONE);
                try {
                    closeRating();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        ratingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RatingActivity.class);
                intent.putExtra(ConstantKeys.RATING, responseModel.getRatings().get(0));
                startActivityForResult(intent, 1);
            }
        });

    }

    private void closeRating() throws JSONException {
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.SKIP_RATING);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.ORDER_ID, responseModel.getRatings().get(0).getOrderId());
        inputForAPI.setJsonObject(jsonObject);
        nearMeViewModel.generalResponseModel(inputForAPI).observe(this, new Observer<GeneralResponseModel>() {
            @Override
            public void onChanged(GeneralResponseModel generalResponseModel) {
                if (generalResponseModel.getError()) {
                    Utils.showToast(generalResponseModel.getErrorMessage(), getActivity());
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    getRestaurantData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }


    private void setAddressValues(String selectedAddressName, String selectedAddressText) {

        Utils.log("print", selectedAddressName + selectedAddressText);
        addressName.setText(selectedAddressName);
        addressText.setText(selectedAddressText);

    }

    private void moveSelectLocationActivity() {

        Intent intent = new Intent(getActivity(), ChooseLocationActivity.class);
        getActivity().startActivityForResult(intent, 1);
    }

    private void startLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);
        connectionLostLayout.setVisibility(View.GONE);
        setDeliveryLocationLayout.setVisibility(View.GONE);
        ratingLayout.setVisibility(View.GONE);
    }

    private void stopLoading() {
        loadingLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
        connectionLostLayout.setVisibility(View.GONE);
        setDeliveryLocationLayout.setVisibility(View.GONE);
        ratingLayout.setVisibility(View.GONE);

        noRestaurantAvailable.setVisibility(View.GONE);
        restaurantLayout.setVisibility(View.VISIBLE);

    }

    public void setNoConnection() {
        loadingLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.GONE);
        connectionLostLayout.setVisibility(View.VISIBLE);
        setDeliveryLocationLayout.setVisibility(View.GONE);
        ratingLayout.setVisibility(View.GONE);

    }

    public void setDeliveryLocation() {
        loadingLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.GONE);
        connectionLostLayout.setVisibility(View.GONE);
        setDeliveryLocationLayout.setVisibility(View.VISIBLE);
        ratingLayout.setVisibility(View.GONE);

    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            EventBus.getDefault().unregister(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getRestaurantData() throws JSONException {

        startLoading();

        try {
            swipeView.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.RESTAURANTS_NEAR_ME);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));

        nearMeViewModel.getRestaurantsNearMe(inputForAPI).observe(this, new Observer<ListRestaurantResponseModel>() {
            @Override
            public void onChanged(@Nullable ListRestaurantResponseModel listRestaurantResponseModel) {
                responseModel = listRestaurantResponseModel;
                stopLoading();

                if (responseModel.getError()) {
                    if (responseModel.getErrorMessage().equalsIgnoreCase(getResources().getString(R.string.no_internet_connection))) {

                        setNoConnection();
                    }
                    if (responseModel.getErrorMessage().equalsIgnoreCase(getResources().getString(R.string.no_restaurant_available_location))) {
                        if (responseModel.getAddress() != null) {
                            appSettings.setAddressId(listRestaurantResponseModel.getAddress().get(0).getAddressId());
                            setAddressValues(listRestaurantResponseModel.getAddress().get(0).getType(), listRestaurantResponseModel.getAddress().get(0).getFullAddress());
                        }
                        noRestaurantAvailable.setVisibility(View.VISIBLE);
                        restaurantLayout.setVisibility(View.GONE);
                        loadingLayout.setVisibility(View.GONE);
                        contentLayout.setVisibility(View.VISIBLE);
                        locationlayout.setVisibility(View.VISIBLE);
                        connectionLostLayout.setVisibility(View.GONE);
                        setDeliveryLocationLayout.setVisibility(View.GONE);
                    } else {

                        if (responseModel.getAddress() != null) {
                            appSettings.setAddressId(listRestaurantResponseModel.getAddress().get(0).getAddressId());
                            setAddressValues(listRestaurantResponseModel.getAddress().get(0).getType(), listRestaurantResponseModel.getAddress().get(0).getFullAddress());

                            noRestaurantAvailable.setVisibility(View.VISIBLE);
                            restaurantLayout.setVisibility(View.GONE);
                            loadingLayout.setVisibility(View.GONE);
                            contentLayout.setVisibility(View.VISIBLE);
                            locationlayout.setVisibility(View.VISIBLE);
                            connectionLostLayout.setVisibility(View.GONE);
                            setDeliveryLocationLayout.setVisibility(View.GONE);
                        } else {
                            ((MainActivity) getActivity()).checkLocationStatus();
                        }
                    }
                } else {
                    appSettings.setLocationAvailable(true);
                    appSettings.setIsAppKilled(ConstantKeys.FALSE_STRING);
                    appSettings.setAddressId(listRestaurantResponseModel.getAddress().get(0).getAddressId());
                    setAddressValues(listRestaurantResponseModel.getAddress().get(0).getType(), listRestaurantResponseModel.getAddress().get(0).getFullAddress());
                    setBannerAdapter();
                    setRestaurantAdapter();
                    if (listRestaurantResponseModel.getRatingPending()) {
                        ratingLayout.setVisibility(View.VISIBLE);
                    } else {
                        ratingLayout.setVisibility(View.GONE);
                    }
                }
            }
        });

    }


    private void setRestaurantAdapter() {
        restaurantCount.setText(getFormattedString(R.string.restaurant_count_text, responseModel.getRestaurantCount()));
        LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        restaurantRecyclerView.setLayoutManager(layout);
        restaurantAdapter = new RestaurantAdapter(getActivity(), responseModel.getListRestaurents());
        restaurantRecyclerView.setAdapter(restaurantAdapter);
        restaurantAdapter.setOnClickListner(new RestaurantAdapterInterface() {
            @Override
            public void onClick() {

            }

            @Override
            public void setRecentSearches(String outletName, String outletId, String cuisines, String displayTime, String averageReview, String displayCostForTwo, String couponEnabledForRestaurant, String longDescription) {

            }
        });
    }


    public String getFormattedString(int resourceId, String value) {
        return Utils.getFormattedString(this.getContext(), resourceId, value);

    }

    private void setBannerAdapter() {
        LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        bannerViewPager.setLayoutManager(layout);
        bannerViewPager.setAdapter(new RestaurantBannerImageAdapter(getActivity(), responseModel.getBanners()));

        try {
            SnapHelper snapHelper = new MyLinearSnapHelper();
            snapHelper.attachToRecyclerView(bannerViewPager);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onLocationFetched(Location location) {
        //call add address api
        appSettings.setLastKnownLatitude(String.valueOf(location.getLatitude()));
        appSettings.setLastKnownLongitude(String.valueOf(location.getLongitude()));
        appSettings.setLocationAvailable(true);
        getAddressFromLatLong();

    }

    @Override
    public void onLocationFailed() {
        //show add location screen
        appSettings.setLocationAvailable(false);
        setDeliveryLocation();
    }

    @Override
    public void callRestaurantApi() {
        //call restaurant api
        try {
            getRestaurantData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}