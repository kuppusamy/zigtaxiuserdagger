package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ViewCartResponse.Tax;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaxChargesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    List<Tax> list;

    public TaxChargesAdapter(Context context, List<Tax> tax) {
        this.context = context;
        this.list=tax;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.extra_charges_adapter, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;
        Tax tax=list.get(position);

        holder.chargesCost.setText(tax.getDishplayTax());
        holder.chargesText.setText(tax.getTaxName());

    }

    @Override
    public int getItemCount() {
        return (list!=null)?list.size():0;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.chargesText)
        TextView chargesText;
        @BindView(R2.id.chargesCost)
        TextView chargesCost;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}