package com.app.foodappuser.view.activities;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.CountDownTimer;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;

import androidx.cardview.widget.CardView;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ResendOtpResponseModel;
import com.app.foodappuser.Model.Response.ValidateOtpResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.OTPListener.OnOtpCompletionListener;
import com.app.foodappuser.Utilities.OTPListener.OtpView;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.ViewModel.ValidateOtpViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ValidateOtpActivity extends BaseActivity implements OnOtpCompletionListener {


    @BindView(R2.id.proceedButton)
    CardView proceedButton;

    @BindView(R2.id.constraintLayout)
    ConstraintLayout constraintLayout;

    @BindView(R2.id.resendOtp)
    TextView resendOtp;

    @BindView(R2.id.backButton)
    ImageView backButton;

    @BindView(R2.id.otp_view)
    OtpView otp_view;

    @BindView(R2.id.progressBarOTP)
    ProgressBar progressBarOTP;

    AppSettings appSettings;

    ValidateOtpViewModel validateOtpViewModel;

    Intent intent;

    String mobileNumber = "", countryCode = "", otp = "";

    String otpNumber;

    Boolean isResendOtpEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_otp);
        ButterKnife.bind(this);
        validateOtpViewModel = ViewModelProviders.of(this).get(ValidateOtpViewModel.class);
        appSettings = new AppSettings(ValidateOtpActivity.this);
        getDatasFromIntent();
        setListeners();
        setTimer();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setTimer() {

        new CountDownTimer(Long.parseLong(appSettings.getOtpTimeOut()), 1000) {
            public void onTick(long millisUntilFinished) {
                isResendOtpEnabled = false;
                resendOtp.setText(getResources().getString(R.string.resendOtpDescription) + " " + millisUntilFinished / 1000 + " " + getResources().getString(R.string.seconds));
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                isResendOtpEnabled = true;
                resendOtp.setText(getResources().getString(R.string.resendOtp));
            }
        }.start();
    }


    private void getDatasFromIntent() {
        intent = getIntent();
        mobileNumber = intent.getStringExtra(ConstantKeys.MOBILE_NUMBER);
        countryCode = intent.getStringExtra(ConstantKeys.COUNTRY_CODE);
        otpNumber = intent.getStringExtra(ConstantKeys.OTP_NUMBER);
        if (!appSettings.getIsTwilioOtp()) {
            otp_view.setText(String.valueOf(otpNumber));
        }

        proceedButton.setAlpha(1);
        proceedButton.setEnabled(true);
    }

    private void setListeners() {
        otp_view.setOtpCompletionListener(this);

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateOtp();
            }
        });

        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isResendOtpEnabled) {
                    otpResendResponse();
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValidateOtpActivity.super.onBackPressed();
            }
        });

    }

    private void otpResendResponse() {

        progressBarOTP.setVisibility(View.VISIBLE);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ConstantKeys.MOBILE_NUMBER, mobileNumber);
            jsonObject.put(ConstantKeys.COUNTRY_CODE, countryCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(ValidateOtpActivity.this);
        inputForAPI.setUrl(UrlHelper.RESEND_OTP);
        inputForAPI.setJsonObject(jsonObject);

        validateOtpViewModel.resendOtp(inputForAPI).observe(this, new Observer<ResendOtpResponseModel>() {
            @Override
            public void onChanged(@Nullable ResendOtpResponseModel resendOtpResponseModel) {
                progressBarOTP.setVisibility(View.INVISIBLE);

                if (resendOtpResponseModel.getError()) {
                    Utils.showSnackBar(constraintLayout, resendOtpResponseModel.getErrorMessage());
                } else {
                    Utils.showSnackBar(constraintLayout, resendOtpResponseModel.getErrorMessage());
                    setTimer();
                }
            }
        });

    }

    private void validateOtp() {

        if (otp_view.getText().toString().length() == 6) {
            checkOtpValidResponse(otp_view.getText().toString());

        } else {
            Utils.showSnackBar(constraintLayout, this.getResources().getString(R.string.enter_valid_otp));

        }

    }

    private void checkOtpValidResponse(String fullOtp) {

        progressBarOTP.setVisibility(View.VISIBLE);


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ConstantKeys.MOBILE_NUMBER, mobileNumber);
            jsonObject.put(ConstantKeys.COUNTRY_CODE, countryCode);
            jsonObject.put(ConstantKeys.OTP_NUMBER, fullOtp);
            jsonObject.put(ConstantKeys.UDID, appSettings.getUdId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(ValidateOtpActivity.this);
        inputForAPI.setUrl(UrlHelper.OTP_VERIFY);
        inputForAPI.setJsonObject(jsonObject);

        validateOtpViewModel.validateOtp(inputForAPI).observe(this, new Observer<ValidateOtpResponseModel>() {
            @Override
            public void onChanged(@Nullable ValidateOtpResponseModel validateOtpResponseModel) {
                progressBarOTP.setVisibility(View.INVISIBLE);

                if (validateOtpResponseModel.getError()) {
                    Utils.showSnackBar(constraintLayout, validateOtpResponseModel.getErrorMessage());
                } else {
                    if (validateOtpResponseModel.getOtpVerified()) {
                        //check new user or not
                        if (validateOtpResponseModel.getIsNewUser()) {
                            //Go To Sign up Screen
                            moveToRegisterActivity();
                        } else {
                            //Go to Home Screen
                            appSettings.setAccessToken(ConstantKeys.BEARER + validateOtpResponseModel.getAccessToken());
                            appSettings.setUsername(validateOtpResponseModel.getUserDetails().getUserName());
                            appSettings.setUserid(validateOtpResponseModel.getUserDetails().getUserId());
                            appSettings.setEmail(validateOtpResponseModel.getUserDetails().getEmail());
                            appSettings.setMobileNumber(validateOtpResponseModel.getUserDetails().getMobileNumber());
                            appSettings.setCountryCode(validateOtpResponseModel.getUserDetails().getCountryCode());
                            appSettings.setIsLoggedIn(ConstantKeys.TRUE_STRING);
                            moveToMainActivity();
                        }

                    } else {
                        Utils.showSnackBar(constraintLayout, validateOtpResponseModel.getErrorMessage());
                    }
                }
            }
        });

    }

    private void moveToMainActivity() {
        Intent intent = new Intent(ValidateOtpActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void moveToRegisterActivity() {
        Intent intent = new Intent(ValidateOtpActivity.this, SignUpActivity.class);
        intent.putExtra(ConstantKeys.MOBILE_NUMBER, mobileNumber);
        intent.putExtra(ConstantKeys.COUNTRY_CODE, countryCode);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
        finish();
    }


    @Override
    public void onOtpCompleted(String otp) {
        if (otp.length() == 6) {

            proceedButton.setAlpha(1);
            proceedButton.setEnabled(true);
        } else {
            proceedButton.setAlpha((float) 0.5);
            proceedButton.setEnabled(false);
        }
    }


}
