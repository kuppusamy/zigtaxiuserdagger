package com.app.foodappuser.view.activities;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import androidx.annotation.Nullable;

import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.chat.service.ServiceClass;
import com.app.chat.service.ServicesUtils;
import com.app.chat.socket.EmitSocket;
import com.app.chat.socket.SocketParams;
import com.app.foodappuser.Application.AppController;
import com.app.foodappuser.Model.Response.LiveTrackingSocketResponseModel;
import com.app.foodappuser.R2;
import com.google.android.gms.maps.model.Marker;
import com.app.foodappuser.MapUtils.DataParser;
import com.app.foodappuser.Model.Response.LiveTrackingResponse.LiveTrackingResponseModel;
import com.app.foodappuser.Model.Response.LiveTrackingResponse.OrdersDetails;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.ViewModel.LiveTrackingViewModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

public class LiveTrackingActivity extends BaseActivity implements OnMapReadyCallback {

    private static final String TAG = LiveTrackingActivity.class.getSimpleName();
    @BindView(R2.id.backButton)
    ImageView backButton;
    @BindView(R2.id.orderNumber)
    TextView orderNumber;
    @BindView(R2.id.totalItem)
    TextView totalItem;
    @BindView(R2.id.toPayAmount)
    TextView toPayAmount;
    @BindView(R2.id.orderReceivedTime)
    TextView orderReceivedTime;
    @BindView(R2.id.orderReceivedTick)
    ImageView orderReceivedTick;
    @BindView(R2.id.orderReceivedNotTick)
    ImageView orderReceivedNotTick;
    @BindView(R2.id.orderReceivedText)
    TextView orderReceivedText;
    @BindView(R2.id.orderConfirmedTime)
    TextView orderConfirmedTime;
    @BindView(R2.id.orderConfirmedTick)
    ImageView orderConfirmedTick;
    @BindView(R2.id.orderConfirmedNotTick)
    ImageView orderConfirmedNotTick;
    @BindView(R2.id.orderConfirmedTextDescri)
    TextView orderConfirmedTextDescri;
    @BindView(R2.id.orderPickedUpTime)
    TextView orderPickedUpTime;
    @BindView(R2.id.orderPickedUpTick)
    ImageView orderPickedUpTick;
    @BindView(R2.id.orderPickedUpNotTick)
    ImageView orderPickedUpNotTick;
    @BindView(R2.id.orderPickedUpText)
    TextView orderPickedUpText;
    @BindView(R2.id.orderReceivedLayout)
    LinearLayout orderReceivedLayout;
    @BindView(R2.id.orderConfirmedLayout)
    LinearLayout orderConfirmedLayout;
    @BindView(R2.id.orderPickedupLayout)
    LinearLayout orderPickedupLayout;
    @BindView(R2.id.orderRejectedLayout)
    LinearLayout orderRejectedLayout;
    @BindView(R2.id.orderDeliveredLayout)
    LinearLayout orderDeliveredLayout;
    @BindView(R2.id.livetrackingLoadingLayout)
    View livetrackingLoadingLayout;
    @BindView(R2.id.livetrackingContentLayout)
    View livetrackingContentLayout;
    @BindView(R2.id.eta_time)
    TextView eta_time;
    private GoogleMap mMap;
    private LatLng originLatLng;
    private LatLng destiLatLng;
    private LatLng prevDriverLatLng;
    private LatLng driverLatLng;
    ArrayList<LatLng> points;
    private Polyline polyline;
    @BindView(R2.id.orderConfirmedText)
    TextView orderConfirmedText;
    LiveTrackingViewModel liveTrackingViewModel;
    double resLat, resLon, cusLat, cusLon, driverlat, driverlon;
    boolean pickedup = false;

    Intent intent;

    Handler handler = new Handler();
    private Runnable runnable;
    private boolean isMarkerRotating = false;
    private SupportMapFragment mapFragment;

    int status = 0;
    private Boolean deliveryBoyAcceptedOrder = false;
    private Boolean deliveredStatus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_tracking);
        ButterKnife.bind(this);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        liveTrackingViewModel = ViewModelProviders.of(this).get(LiveTrackingViewModel.class);

        intent = getIntent();

//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    getTracking();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                handler.postDelayed(this, 5000);
//            }
//        });

        try {
            getTracking();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LiveTrackingActivity.super.onBackPressed();
            }
        });
        listenNotification();
        listenSocket();
    }

    private void listenNotification() {
        ((AppController) getApplication()).getRxBus().toObservable().subscribe(new Consumer<String>() {
            @Override
            public void accept(final String value) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            getTracking();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

            }
        });
    }

    private void listenSocket() {
        liveTrackingViewModel.getLiveTrackingSocket().observe(this, new Observer<LiveTrackingSocketResponseModel>() {
            @Override
            public void onChanged(LiveTrackingSocketResponseModel liveTrackingSocketResponseModel) {
                Log.e(TAG, "getLiveTrackingSocket: " + liveTrackingSocketResponseModel.getOrderStatus());
                try {
                    getTracking();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        liveTrackingViewModel.getLiveTrackingProviderLocationSocket().observe(this, new Observer<LiveTrackingSocketResponseModel>() {
            @Override
            public void onChanged(LiveTrackingSocketResponseModel liveTrackingSocketResponseModel) {
                Log.e(TAG, "getLiveTrackingProviderLocationSocket: " + liveTrackingSocketResponseModel.getLatitude());
                setMapView();

            }
        });
    }

    @Override
    protected void getDmeo() {
        super.getDmeo();
    }

    private void getTracking() throws JSONException {
        startLoading();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.ORDER_ID, intent.getStringExtra(ConstantKeys.INTENTKEYS.ORDER_ID));
        InputForAPI inputForAPI = new InputForAPI(this);
        inputForAPI.setUrl(UrlHelper.TRACK_ORDER);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));
        liveTrackingViewModel.getLiveTracking(inputForAPI).observe(this, new Observer<LiveTrackingResponseModel>() {
            @Override
            public void onChanged(@Nullable LiveTrackingResponseModel liveTrackingResponseModel) {
                if (liveTrackingResponseModel.getError()) {
                    Utils.showToast(liveTrackingResponseModel.getErrorMessage(),LiveTrackingActivity.this);
                } else {
                    stopLoading();
//                    if (status == 0) {
                    mapFragment.getMapAsync(LiveTrackingActivity.this);

//                    }
                    orderNumber.setText(liveTrackingResponseModel.getOrdersDetails().getDisplayOrderId());
                    totalItem.setText(String.valueOf(liveTrackingResponseModel.getOrdersDetails().getOrderItems()));
                    toPayAmount.setText(liveTrackingResponseModel.getOrdersDetails().getNetAmount());
                    orderConfirmedTextDescri.setText(liveTrackingResponseModel.getOrdersDetails().getRestaurantConfirmedDescription());
                    setOrderReceived(liveTrackingResponseModel.getOrdersDetails());
                    setOrderConfirmed(liveTrackingResponseModel.getOrdersDetails());
                    resLat = liveTrackingResponseModel.getOrdersDetails().getOutletlatitude();
                    resLon = liveTrackingResponseModel.getOrdersDetails().getOutletLongitude();
                    cusLat = Double.parseDouble(liveTrackingResponseModel.getOrdersDetails().getUserLatitude());
                    cusLon = Double.parseDouble(liveTrackingResponseModel.getOrdersDetails().getUserLongitude());

                    if (liveTrackingResponseModel.getOrdersDetails().getDeliveryStaffLatitude() != null)
                        driverlat = liveTrackingResponseModel.getOrdersDetails().getDeliveryStaffLatitude();
                    if (liveTrackingResponseModel.getOrdersDetails().getDeliveryStaffLongitude() != null)
                        driverlon = liveTrackingResponseModel.getOrdersDetails().getDeliveryStaffLongitude();

                    pickedup = liveTrackingResponseModel.getOrdersDetails().getOrderPickedUp();
                    deliveryBoyAcceptedOrder = liveTrackingResponseModel.getOrdersDetails().getDeliveryboyAcceptedOrder();
                    deliveredStatus = liveTrackingResponseModel.getOrdersDetails().getDeliveredStatus();
                    setOrderReject(liveTrackingResponseModel.getOrdersDetails());
                    eta_time.setText(Utils.calculateTimeDifference(liveTrackingResponseModel.getOrdersDetails().getEta()));
                    if (deliveredStatus) {
                        setDelivered();
                    } else if (pickedup) {
                        setOrderPickedUp(liveTrackingResponseModel.getOrdersDetails(), resLat, resLon, cusLat, cusLon);
                    } else if (deliveryBoyAcceptedOrder) {
                        setDeliveryBoyAcceptedOrder();
                    }

                    EmitSocket.INSTANCE.emitOnline();

//                    if (status != 0) {
//                        setMapView();
//                    }

                }
            }
        });
    }

    private void startLoading() {
        livetrackingLoadingLayout.setVisibility(View.VISIBLE);
        livetrackingContentLayout.setVisibility(View.GONE);
    }
    private void stopLoading() {
        livetrackingLoadingLayout.setVisibility(View.GONE);
        livetrackingContentLayout.setVisibility(View.VISIBLE);
    }

    private void setDelivered() {
        if (deliveredStatus) {
            orderReceivedLayout.setVisibility(View.GONE);
            orderConfirmedLayout.setVisibility(View.GONE);
            orderPickedupLayout.setVisibility(View.GONE);
            orderRejectedLayout.setVisibility(View.GONE);
            orderDeliveredLayout.setVisibility(View.VISIBLE);

        } else {
            orderReceivedLayout.setVisibility(View.VISIBLE);
            orderConfirmedLayout.setVisibility(View.VISIBLE);
            orderPickedupLayout.setVisibility(View.VISIBLE);
            orderRejectedLayout.setVisibility(View.GONE);
            orderDeliveredLayout.setVisibility(View.GONE);
        }
    }

    private void setDeliveryBoyAcceptedOrder() {
        orderRejectedLayout.setVisibility(View.GONE);
        orderDeliveredLayout.setVisibility(View.GONE);
        orderPickedUpTime.setVisibility(View.INVISIBLE);


        LatLng origin;
        origin = new LatLng(driverlat, driverlon);
        LatLng dest = new LatLng(resLat, resLon);

        String url = getUrl(origin, dest);
        Log.d("onMapClick", url.toString());
        FetchUrl FetchUrl = new FetchUrl();
        FetchUrl.execute(url);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: " + new AppSettings(this).getUserid());
        SocketParams.INSTANCE.setUserId(new AppSettings(this).getUserid());
        if (ServiceClass.Companion.getSocketInstance() == null) {
            if (ServicesUtils.Companion.isMyServiceRunning(ServiceClass.class, this)) {
                stopService(new Intent(this, ServiceClass.class));
            }
            startService(new Intent(this, ServiceClass.class));
        } else {
            if (!ServicesUtils.Companion.isMyServiceRunning(ServiceClass.class, this)) {
                startService(new Intent(this, ServiceClass.class));
            }
        }

        EmitSocket.INSTANCE.emitOnline();


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onStop() {
        super.onStop();
//        handler.removeCallbacksAndMessages(null);
        if (ServicesUtils.Companion.isMyServiceRunning(ServiceClass.class, this)) {
            stopService(new Intent(this, ServiceClass.class));
        }
    }

    private void setOrderReject(OrdersDetails ordersDetails) {
        if (ordersDetails.getOrderRejected()) {
            orderReceivedLayout.setVisibility(View.GONE);
            orderConfirmedLayout.setVisibility(View.GONE);
            orderPickedupLayout.setVisibility(View.GONE);
            orderRejectedLayout.setVisibility(View.VISIBLE);
            orderDeliveredLayout.setVisibility(View.GONE);

        } else {
            orderReceivedLayout.setVisibility(View.VISIBLE);
            orderConfirmedLayout.setVisibility(View.VISIBLE);
            orderPickedupLayout.setVisibility(View.VISIBLE);
            orderRejectedLayout.setVisibility(View.GONE);
            orderDeliveredLayout.setVisibility(View.GONE);

        }
    }


    private void setOrderPickedUp(OrdersDetails ordersDetails, double resLat, double resLon, double cusLat, double cusLon) {
        orderRejectedLayout.setVisibility(View.GONE);
        orderDeliveredLayout.setVisibility(View.GONE);

        if (ordersDetails.getOrderPickedUp()) {

            orderConfirmedTime.setVisibility(View.VISIBLE);
            orderConfirmedTime.setText(Utils.convertTime(ordersDetails.getOrderConfirmedTime()));
            orderConfirmedNotTick.setVisibility(View.GONE);
            orderConfirmedTick.setVisibility(View.VISIBLE);

            orderPickedUpTime.setVisibility(View.VISIBLE);
            orderPickedUpNotTick.setVisibility(View.GONE);
            orderPickedUpTick.setVisibility(View.VISIBLE);
            orderPickedUpTime.setText(Utils.convertTime(ordersDetails.getOrderPickedupTime()));

            LatLng origin = new LatLng(driverlat, driverlon);

            LatLng dest = new LatLng(cusLat, cusLon);

            String url = getUrl(origin, dest);
            Log.d("onMapClick", url.toString());
            FetchUrl FetchUrl = new FetchUrl();
            FetchUrl.execute(url);

        } else {
            orderPickedUpTime.setVisibility(View.INVISIBLE);
            orderPickedUpNotTick.setVisibility(View.VISIBLE);
            orderPickedUpTick.setVisibility(View.GONE);
        }

    }

    private void setOrderConfirmed(OrdersDetails ordersDetails) {
        orderRejectedLayout.setVisibility(View.GONE);
        orderDeliveredLayout.setVisibility(View.GONE);

        if (ordersDetails.getOrderConfirmed()) {
            orderConfirmedTime.setVisibility(View.VISIBLE);
            orderConfirmedTime.setText(Utils.convertTime(ordersDetails.getOrderConfirmedTime()));
            orderConfirmedNotTick.setVisibility(View.GONE);
            orderConfirmedTick.setVisibility(View.VISIBLE);
            orderPickedUpTime.setVisibility(View.INVISIBLE);

        } else {
            orderPickedUpTime.setVisibility(View.INVISIBLE);
            orderConfirmedTime.setVisibility(View.INVISIBLE);
            orderConfirmedNotTick.setVisibility(View.VISIBLE);
            orderConfirmedTick.setVisibility(View.GONE);

        }
    }

    private void setOrderReceived(OrdersDetails ordersDetails) {
        orderRejectedLayout.setVisibility(View.GONE);
        orderDeliveredLayout.setVisibility(View.GONE);

        if (ordersDetails.getOrderReceived()) {
            orderReceivedTime.setVisibility(View.VISIBLE);
            orderReceivedTime.setText(Utils.convertTime(ordersDetails.getReceivedTime()));

            orderReceivedNotTick.setVisibility(View.GONE);
            orderReceivedTick.setVisibility(View.VISIBLE);
            orderPickedUpTime.setVisibility(View.INVISIBLE);
        } else {
            orderPickedUpTime.setVisibility(View.INVISIBLE);
            orderReceivedTime.setVisibility(View.INVISIBLE);
            orderReceivedNotTick.setVisibility(View.VISIBLE);
            orderReceivedTick.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        if (mMap != null) {
            mMap.clear();
        }
        mMap = googleMap;
        setMapView();
    }

    private void setMapView() {

        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);

//        Log.e("LATLONG", "Customer: " + cusLat + " long : " + cusLon);
//        Log.e("LATLONG", "Restaurant: " + resLat + " long : " + resLon);

        originLatLng = new LatLng(cusLat, cusLon);
        destiLatLng = new LatLng(resLat, resLon);
        driverLatLng = new LatLng(driverlat, driverlon);


        if (deliveredStatus) {
            setRestaurantToCustomerDottedLine();

        } else if (pickedup) {
            drawDeliverBoyToCustomerRoute();

        } else if (deliveryBoyAcceptedOrder) {
            drawDeliveryBoyToRestaurantRoute();

        } else {
            setRestaurantToCustomerDottedLine();
        }

//        Animation anim = new AlphaAnimation(0.3f, 1.0f);
//        anim.setDuration(1000); //You can manage the time of the blink with this parameter
//        anim.setStartOffset(20);
//        anim.setRepeatMode(Animation.REVERSE);
//        anim.setRepeatCount(Animation.INFINITE);
//        orderConfirmedText.startAnimation(anim);

    }

    private void drawDeliveryBoyToRestaurantRoute() {
        Bitmap restaurantIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.restaurant_marker);
        Bitmap resizedRestaurantIcon = Bitmap.createScaledBitmap(restaurantIcon, 50, 65, false);
        BitmapDescriptor restaurantMarker = BitmapDescriptorFactory.fromBitmap(resizedRestaurantIcon);
        MarkerOptions markerOptions2 = new MarkerOptions().position(destiLatLng).draggable(false).visible(true).icon(restaurantMarker);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        mMap.addMarker(markerOptions2);
        Bitmap driverIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.driver_marker);
        Bitmap resizedDriverIcon = Bitmap.createScaledBitmap(driverIcon, 50, 90, false);
        BitmapDescriptor driverMarker = BitmapDescriptorFactory.fromBitmap(resizedDriverIcon);
        android.location.Location resLocation = new android.location.Location("service");
        resLocation.setLatitude(resLat);
        resLocation.setLongitude(resLon);

        android.location.Location driverLocation = new android.location.Location("service");
        driverLocation.setLatitude(driverlat);
        driverLocation.setLongitude(driverlon);

        float bearingTo = driverLocation.bearingTo(resLocation);
        MarkerOptions markerOptions3 = new MarkerOptions().position(driverLatLng)
                .draggable(false)
                .visible(true)
                .rotation(bearingTo)
                .flat(true)
                .icon(driverMarker);
        mMap.addMarker(markerOptions3);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(markerOptions3.getPosition());
        builder.include(markerOptions2.getPosition());
        LatLngBounds bounds = builder.build();

        int padding = 140; // offset from edges of the map in pixels
        final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(cu);
            }
        });
    }

    private void drawDeliverBoyToCustomerRoute() {
        Bitmap customerIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.customer_marker);
        Bitmap resizedCustomerIcon = Bitmap.createScaledBitmap(customerIcon, 50, 65, false);
        BitmapDescriptor customerMarker = BitmapDescriptorFactory.fromBitmap(resizedCustomerIcon);
        MarkerOptions markerOptions1 = new MarkerOptions().position(originLatLng).draggable(false).visible(true).icon(customerMarker);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        mMap.addMarker(markerOptions1);
        Bitmap driverIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.driver_marker);
        Bitmap resizedDriverIcon = Bitmap.createScaledBitmap(driverIcon, 50, 90, false);
        BitmapDescriptor driverMarker = BitmapDescriptorFactory.fromBitmap(resizedDriverIcon);
        android.location.Location resLocation = new android.location.Location("service");
        resLocation.setLatitude(cusLat);
        resLocation.setLongitude(cusLon);

        android.location.Location driverLocation = new android.location.Location("service");
        driverLocation.setLatitude(driverlat);
        driverLocation.setLongitude(driverlon);

        float bearingTo = driverLocation.bearingTo(resLocation);
        MarkerOptions markerOptions3 = new MarkerOptions().position(driverLatLng)
                .draggable(false)
                .visible(true)
                .rotation(bearingTo)
                .flat(true)
                .icon(driverMarker);
        mMap.addMarker(markerOptions3);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(markerOptions3.getPosition());
        builder.include(markerOptions1.getPosition());
        LatLngBounds bounds = builder.build();

        int padding = 140; // offset from edges of the map in pixels
        final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(cu);
            }
        });
    }

    private void setRestaurantToCustomerDottedLine() {
        Bitmap restaurantIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.restaurant_marker);
        Bitmap resizedRestaurantIcon = Bitmap.createScaledBitmap(restaurantIcon, 50, 65, false);
        BitmapDescriptor restaurantMarker = BitmapDescriptorFactory.fromBitmap(resizedRestaurantIcon);

        Bitmap customerIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.customer_marker);
        Bitmap resizedCustomerIcon = Bitmap.createScaledBitmap(customerIcon, 50, 65, false);
        BitmapDescriptor customerMarker = BitmapDescriptorFactory.fromBitmap(resizedCustomerIcon);

        IconGenerator iconFactory_right = new IconGenerator(this);
        iconFactory_right.setBackground(null);

        View view_right = View.inflate(this, R.layout.map_info_window, null);
        iconFactory_right.setContentView(view_right);

//        MarkerOptions markerOptions1 = new MarkerOptions().position(originLatLng).draggable(false).visible(true).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromBitmap(iconFactory_right.makeIcon("current")));
        MarkerOptions markerOptions2 = new MarkerOptions().position(destiLatLng).draggable(false).visible(true).icon(restaurantMarker);
        MarkerOptions markerOptions1 = new MarkerOptions().position(originLatLng).draggable(false).visible(true).icon(customerMarker);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        mMap.addMarker(markerOptions2);
        mMap.addMarker(markerOptions1);
        this.showCurvedPolylineCurved(originLatLng, destiLatLng, 0.2, true);
        this.showCurvedPolylinesShadow(originLatLng, destiLatLng, 0.1, true);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(markerOptions1.getPosition());
        builder.include(markerOptions2.getPosition());
        LatLngBounds bounds = builder.build();

        int padding = 140; // offset from edges of the map in pixels
        final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(cu);
            }
        });
    }

    private void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 2000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    float bearing = -rot > 180 ? rot / 2 : rot;

                    marker.setRotation(bearing);

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }

    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    private String getUrl(LatLng origin, LatLng dest) {

        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin.latitude + "," + origin.longitude + "&destination=" + dest.latitude + "," + dest.longitude + "&key=" + new AppSettings(this).getApiMapKey() + "";
        return url;
    }

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    private void showCurvedPolylineCurved(LatLng p1, LatLng p2, double k, boolean show) {
        //Calculate distance and heading between two points
        double d = SphericalUtil.computeDistanceBetween(p1, p2);
        double h = SphericalUtil.computeHeading(p1, p2);

        //Midpoint position
        LatLng p = SphericalUtil.computeOffset(p1, d * 0.5, h);

        //Apply some mathematics to calculate position of the circle center
        double x = (1 - k * k) * d * 0.5 / (2 * k);
        double r = (1 + k * k) * d * 0.5 / (2 * k);

        LatLng c = SphericalUtil.computeOffset(p, x, h + 90.0);

        //Polyline options
        PolylineOptions options = new PolylineOptions();
        List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dash(20), new Gap(10));

        //Calculate heading between circle center and two points
        double h1 = SphericalUtil.computeHeading(c, p1);
        double h2 = SphericalUtil.computeHeading(c, p2);

        //Calculate positions of points on circle border and add them to polyline options
        int numpoints = 100;
        double step = (h2 - h1) / numpoints;

        for (int i = 0; i < numpoints; i++) {
            LatLng pi = SphericalUtil.computeOffset(c, r, h1 + i * step);
            options.add(pi);
        }

        //Draw polyline
        Polyline polyline = mMap.addPolyline(options.width(6).color(this.getResources().getColor(R.color.text_color_dark_blue)).geodesic(false).pattern(pattern));
        if (!show) {

            polyline.remove();
        }

    }

    private void showCurvedPolylinesShadow(LatLng p1, LatLng p2, double k, boolean show) {
        //Calculate distance and heading between two points
        double d = SphericalUtil.computeDistanceBetween(p1, p2);
        double h = SphericalUtil.computeHeading(p1, p2);

        //Midpoint position
        LatLng p = SphericalUtil.computeOffset(p1, d * 0.5, h);

        //Apply some mathematics to calculate position of the circle center
        double x = (1 - k * k) * d * 0.5 / (2 * k);
        double r = (1 + k * k) * d * 0.5 / (2 * k);

        LatLng c = SphericalUtil.computeOffset(p, x, h + 90.0);

        //Polyline options
        PolylineOptions options = new PolylineOptions();
        List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dash(20), new Gap(10));

        //Calculate heading between circle center and two points
        double h1 = SphericalUtil.computeHeading(c, p1);
        double h2 = SphericalUtil.computeHeading(c, p2);

        //Calculate positions of points on circle border and add them to polyline options
        int numpoints = 100;
        double step = (h2 - h1) / numpoints;

        for (int i = 0; i < numpoints; i++) {
            LatLng pi = SphericalUtil.computeOffset(c, r, h1 + i * step);
            options.add(pi);
        }

        //Draw polyline
        Polyline polyline = mMap.addPolyline(options.width(6).color(this.getResources().getColor(R.color.demo_dark_transparent)).geodesic(false).pattern(pattern));
        if (!show) {

            polyline.remove();
        }

    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            List<List<HashMap<String, String>>> routes = null;

            JSONObject jObject;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }

            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(7);
                lineOptions.color(LiveTrackingActivity.this.getResources().getColor(R.color.text_color_dark_blue));

                setMapView();

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);


        }
    }


}
