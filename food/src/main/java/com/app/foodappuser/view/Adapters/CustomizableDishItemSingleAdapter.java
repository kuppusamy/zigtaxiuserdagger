package com.app.foodappuser.view.Adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ListDishesResponse.CustomizableDishItem;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.InterFaces.CustomizationAdapterInterface;
import com.app.foodappuser.ViewModel.CustomizableViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class CustomizableDishItemSingleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<CustomizableDishItem> customizableDishItemList;
    Context context;
    CustomizableViewModel customizableViewModel;
    private RadioButton selectedRadioButton;
    CustomizationAdapterInterface customizationAdapterInterface;

    public CustomizableDishItemSingleAdapter(Context context, List<CustomizableDishItem> customizableDishItems, CustomizationAdapterInterface customizationAdapterInterface) {
        this.customizableDishItemList = customizableDishItems;
        this.context = context;
        this.customizableViewModel = new CustomizableViewModel(context);
        this.customizationAdapterInterface = customizationAdapterInterface;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView;
        itemView = LayoutInflater.from(context).inflate(R.layout.customizable_dish_single_item, viewGroup, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new CustomizableDishItemSingleHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final CustomizableDishItemSingleHolder customizableElementHolder = (CustomizableDishItemSingleHolder) viewHolder;
        final CustomizableDishItem customizableElement = customizableDishItemList.get(position);


        if (customizableElement.getIsVeg() == 1) {
            customizableElementHolder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.veg_icon));
        } else {
            customizableElementHolder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.non_veg_icon));
        }

        customizableElementHolder.displayPrice.setText(customizableElement.getDisplayPrice());
        customizableElementHolder.singleSelect.setText(customizableElement.getElementName() + "  " + customizableElement.getDisplayPrice());

//        if(customizableElement.getIsSelected()==0){
//            customizableElementHolder.singleSelect.setChecked(false);
//        }else if(customizableElement.getIsSelected()==1){
//            customizableElementHolder.singleSelect.setChecked(true);
//        }


        customizableElementHolder.singleSelect.setTag(position);

        customizableElementHolder.singleSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (CustomizableDishItem model : customizableDishItemList)
                    model.setIsSelected(0);

                // Set "checked" the model associated to the clicked radio button
                customizableElement.setIsSelected(1);

                // If current view (RadioButton) differs from previous selected radio button, then uncheck selectedRadioButton
                if (null != selectedRadioButton && !v.equals(selectedRadioButton))
                    selectedRadioButton.setChecked(false);

                // Replace the previous selected radio button with the current (clicked) one, and "check" it
                selectedRadioButton = (RadioButton) v;
                selectedRadioButton.setChecked(true);
                Log.e("working", "onBindViewHolder: "+"inside" );

                customizationAdapterInterface.addCustomizationDishName(customizableElement.getElementName(), customizableElement.getCustomisationCategoryId(), customizableElement.getElementId(), customizableElement.getPrice(), true);

            }
        });

        if (position == 0) {
            Log.e("working", "onBindViewHolder: "+"clicked" );
            customizableElementHolder.singleSelect.performClick();
//            customizationAdapterInterface.addCustomizationDishName(customizableElement.getElementName(), customizableElement.getCustomisationCategoryId(), customizableElement.getElementId(), customizableElement.getPrice(), true);

        }



    }

    @Override
    public int getItemCount() {
        return (customizableDishItemList != null) ? customizableDishItemList.size() : 0;
    }

    class CustomizableDishItemSingleHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.singleSelect)
        RadioButton singleSelect;

        @BindView(R2.id.displayPrice)
        TextView displayPrice;

        @BindView(R2.id.isVegView)
        ImageView isVegView;

        public CustomizableDishItemSingleHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

