package com.app.foodappuser.view.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.app.foodappuser.R
import com.app.foodappuser.Utilities.Constants.ConstantKeys
import com.app.foodappuser.Utilities.LocationClass
import kotlinx.android.synthetic.main.activity_apply_coupon.*

class ApplyCouponActivity : LocationClass() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apply_coupon)

        setListeners();


    }

    private fun setListeners() {
        back_button.setOnClickListener { onBackPressed() }

        apply_coupon.setOnClickListener {
            val intent=Intent()
            intent.putExtra(ConstantKeys.COUPON_NAME,coupon_code.text.toString())
            setResult(Activity.RESULT_OK,intent)
            finish()
        }
    }
}
