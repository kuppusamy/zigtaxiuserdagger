package com.app.foodappuser.view.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.foodappuser.Model.Response.ExploreResponse.DishesList;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.ApiCall.ImageLoader;
import com.app.foodappuser.Utilities.InterFaces.onClickListener;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;

import java.util.List;

public class DishesDetailExploreAdapter extends RecyclerView.Adapter<DishesDetailExploreAdapter.DishesViewHolder> {
    List<DishesList> listDishes;
    Context context;
    onClickListener onClickListener;

    public DishesDetailExploreAdapter(Context context, List<DishesList> listDishes, onClickListener onClickListener) {
        this.context=context;
        this.listDishes=listDishes;
        this.onClickListener=onClickListener;
    }

    @NonNull
    @Override
    public DishesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dishes_detail_explore_item, parent, false);
        DishesViewHolder viewHolder = new DishesViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DishesViewHolder holder, int position) {
        holder.dish_name.setText(listDishes.get(position).getName());
        holder.dish_price.setText(new AppSettings(context).getCurrency()+" "+listDishes.get(position).getPrice());
        new ImageLoader(context).load(listDishes.get(position).getImage(),holder.restaurant_image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listDishes!=null?listDishes.size():0;
    }

    class DishesViewHolder extends RecyclerView.ViewHolder {
        ImageView restaurant_image;
        TextView dish_name,dish_price;
        public DishesViewHolder(@NonNull View itemView) {
            super(itemView);
            restaurant_image=itemView.findViewById(R.id.restaurant_image);
            dish_name=itemView.findViewById(R.id.dish_name);
            dish_price=itemView.findViewById(R.id.dish_price);
        }
    }
}
