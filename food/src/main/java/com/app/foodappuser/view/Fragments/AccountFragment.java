package com.app.foodappuser.view.Fragments;


import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.app.foodappuser.Application.AppController;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.R2;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;

import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.foodappuser.Model.Response.EditProfileResponseModel;
import com.app.foodappuser.Model.Response.FaqResponseModel;
import com.app.foodappuser.Model.Response.PastOrderResponse.PastOrderResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.ViewAnimationUtils;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.DialogUtils.CustomTextInputEditText;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.Utilities.UiUtils.ExpandableLayout;
import com.app.foodappuser.view.activities.ManageAddressActivity;
import com.app.foodappuser.view.activities.SignInActivity;
import com.app.foodappuser.view.activities.WebViewActivity;
import com.app.foodappuser.view.Adapters.PastOrdersAdapter;
import com.app.foodappuser.ViewModel.AccountViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

    @BindView(R2.id.nameText)
    TextView nameText;
    @BindView(R2.id.mobileNumberText)
    TextView mobileNumberText;
    @BindView(R2.id.emailText)
    TextView emailText;
    @BindView(R2.id.editButton)
    RelativeLayout editButton;
    @BindView(R2.id.myAccountButton)
    LinearLayout myAccountButton;
    @BindView(R2.id.myAccountLayout)
    ExpandableLayout myAccountLayout;
    @BindView(R2.id.manageAddressButton)
    LinearLayout manageAddressButton;
    @BindView(R2.id.blockImage)
    ImageView blockImage;
    @BindView(R2.id.focus_thief)
    View focusThief;
    @BindView(R2.id.progressBarChangePassword)
    ProgressBar progressBarChangePassword;
    @BindView(R2.id.progressBarAccount)
    ProgressBar progressBarAccount;
    @BindView(R2.id.editCloseButton)
    ImageView editCloseButton;
    @BindView(R2.id.saveButton)
    RelativeLayout saveButton;
    @BindView(R2.id.nameEditText)
    CustomTextInputEditText nameEditText;
    @BindView(R2.id.newPasswordWrapper)
    TextInputLayout newPasswordWrapper;
    @BindView(R2.id.newPasswordContainer)
    LinearLayout newPasswordContainer;
    @BindView(R2.id.emailAddress)
    CustomTextInputEditText emailAddress;
    @BindView(R2.id.confirmPasswordWrapper)
    TextInputLayout confirmPasswordWrapper;
    @BindView(R2.id.confirmPasswordContainer)
    LinearLayout confirmPasswordContainer;
    @BindView(R2.id.slideDownLayout)
    RelativeLayout slideDownLayout;
    @BindView(R2.id.changePasswordLayout)
    RelativeLayout changePasswordLayout;
    @BindView(R2.id.logoutButton)
    CardView logoutButton;
    @BindView(R2.id.faqLinksButton)
    LinearLayout faqLinksButton;
    @BindView(R2.id.pastOrdersRecycler)
    RecyclerView pastOrdersRecycler;
    @BindView(R2.id.pastOrdersLayout)
    LinearLayout pastOrdersLayout;
    private View view;
    AppSettings appSettings;
    AccountViewModel accountViewModel;

    PastOrdersAdapter pastOrdersAdapter;
    private GoogleSignInClient mGoogleSignInClient;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);
        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
        appSettings = new AppSettings(getActivity());
        initListeners();
        setDatas();
        pastOrders();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);

        ((AppController) getActivity().getApplication()).getRxBus().toObservable().subscribe(new Consumer<String>() {
            @Override
            public void accept(final String value) {

                if (value.equalsIgnoreCase("delivered"))
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pastOrders();
                        }
                    });

            }
        });

        return view;
    }

    private void pastOrders() {

        progressBarAccount.setVisibility(View.VISIBLE);

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.PAST_ORDERS);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));

        accountViewModel.getPastOrders(inputForAPI).observe(this, new Observer<PastOrderResponseModel>() {
            @Override
            public void onChanged(@Nullable PastOrderResponseModel pastOrderResponseModel) {
                progressBarAccount.setVisibility(View.INVISIBLE);

                if (pastOrderResponseModel != null) {

                    if (pastOrderResponseModel.getError()) {

//                        Utils.showSnackBar(getActivity().findViewById(android.R.id.content), pastOrderResponseModel.getErrorMessage());

                    } else {
                        pastOrdersLayout.setVisibility(View.VISIBLE);
                        LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        pastOrdersRecycler.setLayoutManager(layout);
                        pastOrdersAdapter = new PastOrdersAdapter(getActivity(), pastOrderResponseModel.getPastOrders());
                        pastOrdersRecycler.setAdapter(pastOrdersAdapter);

                    }
                }
            }
        });
    }

    private void setDatas() {

        mobileNumberText.setText(appSettings.getMobileNumber());
        emailText.setText(appSettings.getEmail());
        nameText.setText(appSettings.getUsername());
        nameEditText.setText(appSettings.getUsername());
        emailAddress.setText(appSettings.getEmail());

    }

    private void initListeners() {

        myAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAccountLayout.toggleExpansion();
            }
        });

        manageAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ManageAddressActivity.class);
                startActivity(intent);
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangePasswordDialog();
            }
        });

        editCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboard(getActivity());
                changePasswordLayout.setVisibility(View.GONE);

            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboard(getActivity());
                try {
                    updateProfile();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        blockImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmationLogoutPopup();


            }
        });

        faqLinksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    getFaqLinks();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void confirmationLogoutPopup() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setMessage("Are you sure you want to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        logoutApiCall();

                    }
                }).setNegativeButton("Cancel", null);

        AlertDialog alert1 = alert.create();
        alert1.show();
    }

    private void logoutApiCall() {
        progressBarAccount.setVisibility(View.VISIBLE);

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.USER_LOGOUT);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));

        accountViewModel.generalResponseModelLiveData(inputForAPI).observe(this, new Observer<GeneralResponseModel>() {
            @Override
            public void onChanged(@Nullable GeneralResponseModel generalResponseModel) {
                progressBarAccount.setVisibility(View.INVISIBLE);
                if (generalResponseModel != null) {

                    if (generalResponseModel.getError()) {

                        Utils.showSnackBar(getActivity().findViewById(android.R.id.content), generalResponseModel.getErrorMessage());

                    } else {
                        logout();
                    }
                }
            }
        });
    }

    private void logout() {
        Utils.loge("LOGOUT: ",appSettings.getLoginType());
        if (appSettings.getLoginType().equalsIgnoreCase(getContext().getResources().getString(R.string.google))) {

            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Utils.loge("GOOGLE SIGNOUT: ","signout successfully");
                            appSettings.setIsLoggedIn(ConstantKeys.FALSE_STRING);
                            accountViewModel.deleteAllTableValues();
                            Intent intent = new Intent(getActivity(), SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else if (appSettings.getLoginType().equalsIgnoreCase(getContext().getResources().getString(R.string.facebook))) {
            LoginManager.getInstance().logOut();
            appSettings.setIsLoggedIn(ConstantKeys.FALSE_STRING);
            accountViewModel.deleteAllTableValues();
            Intent intent = new Intent(getActivity(), SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            appSettings.setIsLoggedIn(ConstantKeys.FALSE_STRING);
            accountViewModel.deleteAllTableValues();
            Intent intent = new Intent(getActivity(), SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private void getFaqLinks() throws JSONException {
        progressBarAccount.setVisibility(View.VISIBLE);
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.STATIC_PAGES);

        accountViewModel.getLinks(inputForAPI).observe(this, new Observer<FaqResponseModel>() {
            @Override
            public void onChanged(@Nullable FaqResponseModel faqResponseModel) {
                progressBarAccount.setVisibility(View.INVISIBLE);

                if (faqResponseModel.getError()) {
                    Utils.showSnackBar(getActivity().findViewById(android.R.id.content), faqResponseModel.getErrorMessage());

                } else {
                    Intent intent = new Intent(getActivity(), WebViewActivity.class);
                    intent.putExtra(ConstantKeys.HEADING, Utils.getString(getActivity(), R.string.faq_and_address));
                    intent.putExtra(ConstantKeys.URL, faqResponseModel.getFaqPages());
                    startActivity(intent);
                }
            }
        });
    }


    private void updateProfile() throws JSONException {

        saveButton.setEnabled(false);
        progressBarChangePassword.setVisibility(View.VISIBLE);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.EMAIL, emailAddress.getText().toString().trim());
        jsonObject.put(ConstantKeys.USER_NAME, nameEditText.getText().toString().trim());
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelper.UPDATE_PROFILE);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(getActivity()));

        accountViewModel.updateProfile(inputForAPI).observe(this, new Observer<EditProfileResponseModel>() {
            @Override
            public void onChanged(@Nullable EditProfileResponseModel editProfileResponseModel) {

                saveButton.setEnabled(true);
                progressBarChangePassword.setVisibility(View.INVISIBLE);

                if (editProfileResponseModel.getError()) {

                    Utils.showSnackBar(getActivity().findViewById(android.R.id.content), editProfileResponseModel.getErrorMessage());

                } else {
                    appSettings.setEmail(emailAddress.getText().toString().trim());
                    appSettings.setUsername(nameEditText.getText().toString().trim());
                    setDatas();
                    Utils.showSnackBar(getActivity().findViewById(android.R.id.content), editProfileResponseModel.getErrorMessage());
                    emailAddress.clearFocus();
                    changePasswordLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showChangePasswordDialog() {
        changePasswordLayout.setVisibility(View.VISIBLE);
        ViewAnimationUtils.expand(slideDownLayout, getActivity());
    }


}
