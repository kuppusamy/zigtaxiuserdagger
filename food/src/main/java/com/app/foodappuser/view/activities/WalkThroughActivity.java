package com.app.foodappuser.view.activities;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.app.foodappuser.R2;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.Utilities.UiUtils.ImageViewerAdapter;
import com.app.foodappuser.Utilities.UiUtils.PagerAdapter;
import com.app.foodappuser.view.Fragments.WalkThroughPagerFragment;

import butterknife.BindView;
import me.relex.circleindicator.CircleIndicator;

public class WalkThroughActivity extends BaseActivity {

    @BindView(R2.id.viewPagerContainer)
    ViewPager viewPagerContainer;

    @BindView(R2.id.goNextButton)
    ImageView goNextButton;


    PagerAdapter pagerAdapter;

    ImageViewerAdapter imageViewerAdapter;

    @BindView(R2.id.indicator)
    CircleIndicator indicator;

    AppSettings appSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk_through);

        appSettings=new AppSettings(WalkThroughActivity.this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        appSettings.setFCMToken(token);
                        // Log and toast
                        Utils.log("USER_ID",token);
                    }
                });


    }

    @Override
    public void onStart() {
        super.onStart();
        initViews();
    }

    private void initViews() {

        pagerAdapter = new PagerAdapter(getSupportFragmentManager());

        pagerAdapter.addFragment(WalkThroughPagerFragment.newInstance(R.drawable.walkthrough_image_one));
        pagerAdapter.addFragment(WalkThroughPagerFragment.newInstance(R.drawable.walkthrough_image_two));
        pagerAdapter.addFragment(WalkThroughPagerFragment.newInstance(R.drawable.walkthrough_image_three));

//        ArrayList<Integer> arrayList = new ArrayList<>();
//        arrayList.add(R.drawable.walkthrough_image_one);
//        arrayList.add(R.drawable.walkthrough_image_two);
//        arrayList.add(R.drawable.walkthrough_image_three);
//
//        viewPagerContainer.setAdapter(new ImageViewerAdapter(WalkThroughActivity.this, arrayList));

        viewPagerContainer.setAdapter(pagerAdapter);
        viewPagerContainer.setOffscreenPageLimit(2);

        indicator.setViewPager(viewPagerContainer);

        viewPagerContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
//                Utils.log(WalkThroughActivity.class.getSimpleName(), String.valueOf(i));
                if (i == 2) {
                    goNextButton.setVisibility(View.VISIBLE);
                    indicator.setVisibility(View.GONE);
                } else {
                    goNextButton.setVisibility(View.GONE);
                    indicator.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        goNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appSettings.setIsWalkThroughPassed(ConstantKeys.FALSE_STRING);
                Intent intent = new Intent(WalkThroughActivity.this, SignInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });



    }
}
