package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ViewCartResponse.BillTotal;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExtraChargesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;

    List<BillTotal> list;

    public ExtraChargesAdapter(Context context, List<BillTotal> charges) {
        this.context = context;
        this.list = charges;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.extra_charges_adapter, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;
        BillTotal charge = list.get(position);
        holder.chargesText.setText(charge.getDisplayKey());
        holder.chargesCost.setText(charge.getDisplayValue());
    }

    @Override
    public int getItemCount() {
        return (list!=null)?list.size():0;

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.chargesText)
        TextView chargesText;
        @BindView(R2.id.chargesCost)
        TextView chargesCost;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}