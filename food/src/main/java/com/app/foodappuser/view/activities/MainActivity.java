package com.app.foodappuser.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.LocationClass;
import com.app.foodappuser.Utilities.NearMeFragmentLogicInterface;
import com.app.foodappuser.Utilities.SendLocationDataInterface;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.SelectedTab;
import com.app.foodappuser.Utilities.BaseUtils.BottomNavigationViewHelper;
import com.app.foodappuser.Utilities.BaseUtils.NonSwipeableViewPager;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.view.Adapters.HomeViewPagerAdapter;
import com.app.foodappuser.view.Fragments.AccountFragment;
import com.app.foodappuser.view.Fragments.ExploreFragment;
import com.app.foodappuser.view.Fragments.NearMeFragment;
import com.app.foodappuser.view.Fragments.MyCartFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends LocationClass implements SendLocationDataInterface {


    @BindView(R2.id.coordinator_layout)
    CoordinatorLayout coordinator_layout;
    private static final int COUPON_CODE_KEY = 101;
    @BindView(R2.id.bottomNavigation)
    BottomNavigationView bottomNavigation;
    @BindView(R2.id.viewPager)
    NonSwipeableViewPager viewPager;
    private String TAG = MainActivity.class.getSimpleName();
    AppSettings appSettings;

    SelectedTab selectedTab;

    NearMeFragment nearMeFragment = new NearMeFragment();
    ExploreFragment exploreFragment = new ExploreFragment();
    MyCartFragment myCartFragment = new MyCartFragment(true);
    AccountFragment accountFragment = new AccountFragment();
    private NearMeFragmentLogicInterface nearMeFragmentLogicInterface;

//    CoordinatorLayout.LayoutParams params;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setStatusBarColorBlue(MainActivity.this);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        appSettings = new AppSettings(this);
        selectedTab = new SelectedTab();
//        params = (CoordinatorLayout.LayoutParams) coordinator_layout.getLayoutParams();
        initListeners();
        initLocationInterface(this);
    }


    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            bottomNavigation.setSelectedItemId(R.id.near_you_item);
            viewPager.setCurrentItem(0);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void initListeners() {

        BottomNavigationViewHelper.removeShiftMode(bottomNavigation);

        viewPager.setPagingEnabled(false);
        HomeViewPagerAdapter adapter = new HomeViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(nearMeFragment, "");
        adapter.addFragment(new ExploreFragment(), "");
        adapter.addFragment(myCartFragment, "");
        adapter.addFragment(new AccountFragment(), "");
//        viewPager.setPageTransformer(true,new FadeOutTransformation());
        viewPager.setAdapter(adapter);
//        viewPager.setOffscreenPageLimit(4);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                Log.d(TAG, "onPageSelected: " + i);
                appSettings.setSelectedTab(i);

                if (i == 0) {
                    Utils.setStatusBarColorBlue(MainActivity.this);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Utils.setSystemBarTheme(MainActivity.this, true);
                    }
                } else if (i == 1) {
                    Utils.setStatusBarColorWhite(MainActivity.this);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Utils.setSystemBarTheme(MainActivity.this, false);
                    }
                } else if (i == 2) {
                    Utils.setStatusBarColorWhite(MainActivity.this);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Utils.setSystemBarTheme(MainActivity.this, false);
                    }
                } else if (i == 3) {
                    Utils.setStatusBarColorWhite(MainActivity.this);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Utils.setSystemBarTheme(MainActivity.this, false);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R2.id.near_you_item:
                        viewPager.setCurrentItem(0);
//                        params.setBehavior(new AppBarLayout.ScrollingViewBehavior(MainActivity.this, null));
                        return true;
                    case R2.id.explore_item:
                        viewPager.setCurrentItem(1);
                        return true;

                    case R2.id.orders_item:
                        viewPager.setCurrentItem(2);
//                        myCartFragment.refresh();
                        return true;

                    case R2.id.profile_item:
                        viewPager.setCurrentItem(3);
                        return true;

                }
                return false;
            }
        });

        changeTextSize();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult: " + requestCode);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + viewPager.getCurrentItem());
                if (0 == viewPager.getCurrentItem() && null != fragment) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }

        if (requestCode == COUPON_CODE_KEY) {
            if (resultCode == Activity.RESULT_OK) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + viewPager.getCurrentItem());
                if (2 == viewPager.getCurrentItem() && null != fragment) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        appSettings.setIsAppKilled(ConstantKeys.TRUE_STRING);
    }

    private void changeTextSize() {

        TextView near_you_item = bottomNavigation.findViewById(R.id.near_you_item).findViewById(R.id.largeLabel);
        TextView explore_item = bottomNavigation.findViewById(R.id.explore_item).findViewById(R.id.largeLabel);
        TextView orders_item = bottomNavigation.findViewById(R.id.orders_item).findViewById(R.id.largeLabel);
        TextView profile_item = bottomNavigation.findViewById(R.id.profile_item).findViewById(R.id.largeLabel);
        near_you_item.setTextSize(12);
        explore_item.setTextSize(12);
        orders_item.setTextSize(12);
        profile_item.setTextSize(12);

        ImageView near_you_item_image = bottomNavigation.findViewById(R.id.near_you_item).findViewById(R.id.icon);
        ImageView explore_item_image = bottomNavigation.findViewById(R.id.explore_item).findViewById(R.id.icon);
        ImageView orders_item_image = bottomNavigation.findViewById(R.id.orders_item).findViewById(R.id.icon);
        ImageView profile_item_image = bottomNavigation.findViewById(R.id.profile_item).findViewById(R.id.icon);
        near_you_item_image.setPadding(7, 7, 7, 7);
        explore_item_image.setPadding(7, 7, 7, 7);
        orders_item_image.setPadding(7, 7, 7, 7);
        profile_item_image.setPadding(7, 7, 7, 7);
    }

    @Override
    public void onLocationFetched(Location location) {
        nearMeFragmentLogicInterface.onLocationFetched(location);
    }

    @Override
    public void onLocationFailed() {
        nearMeFragmentLogicInterface.onLocationFailed();
    }

    public void logicOfRestaurant(NearMeFragmentLogicInterface nearMeFragmentLogicInterface) {
        this.nearMeFragmentLogicInterface = nearMeFragmentLogicInterface;
        if (!appSettings.getLocationAvailable()) {
            checkLocationStatus();
        } else {
            this.nearMeFragmentLogicInterface.callRestaurantApi();
        }
    }
}
