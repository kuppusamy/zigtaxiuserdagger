package com.app.foodappuser.view.activities;

import android.Manifest;
import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.foodappuser.R2;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.LocationResponseModel;
import com.app.foodappuser.Model.Response.SaveLocationResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.LocationPermissionChecker;
import com.app.foodappuser.Utilities.BaseUtils.ResizeAnimation;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.ViewModel.SaveLocationViewModel;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SaveLocationActivity extends LocationPermissionChecker implements ResultCallback<LocationSettingsResult>, OnMapReadyCallback, View.OnClickListener {


    private static final String TAG = SaveLocationActivity.class.getSimpleName();
    Location mLocation;
    SupportMapFragment mapFragment;

    @BindView(R2.id.addresslayout)
    LinearLayout addresslayout;

    @BindView(R2.id.backimg)
    ImageView backimg;

    @BindView(R2.id.topPanel)
    RelativeLayout topPanel;

    @BindView(R2.id.bottomSheet)
    FrameLayout bottomSheet;


    @BindView(R2.id.homeIcon)
    ImageView homeIcon;

    @BindView(R2.id.homeLayout)
    RelativeLayout homeLayout;

    @BindView(R2.id.homeText)
    TextView homeText;

    @BindView(R2.id.homeBottomView)
    View homeBottomView;

    @BindView(R2.id.workIcon)
    ImageView workIcon;

    @BindView(R2.id.workLayout)
    RelativeLayout workLayout;

    @BindView(R2.id.workText)
    TextView workText;

    @BindView(R2.id.workBottomView)
    View workBottomView;

    @BindView(R2.id.otherIcon)
    ImageView otherIcon;

    @BindView(R2.id.otherLayout)
    RelativeLayout otherLayout;

    @BindView(R2.id.otherText)
    TextView otherText;

    @BindView(R2.id.otherBottomView)
    View otherBottomView;


    @BindView(R2.id.otherAddress)
    EditText otherAddress;
    @BindView(R2.id.locationName)
    EditText locationName;
    @BindView(R2.id.house_number)
    EditText house_number;
    @BindView(R2.id.landmark)
    EditText landmark;

    @BindView(R2.id.saveButton)
    Button saveButton;

    @BindView(R2.id.progressBarOTP)
    ProgressBar progressBarOTP;

    @BindView(R2.id.othersAddresLayout)
    LinearLayout othersAddresLayout;

    public boolean mMapViewExpanded = false;


    View mapView;
    SaveLocationViewModel saveLocationViewModel;
    private GoogleMap mGoogleMap;
    private BottomSheetBehavior mBottomSheetBehavior;
    private CoordinatorLayout.LayoutParams layoutParams;
    String selectedLat = "", selectedLong = "", type = "";
    private Intent intent;
    private String adddressId;

    AppSettings appSettings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_location);
        ButterKnife.bind(this);
        saveLocationViewModel = ViewModelProviders.of(this).get(SaveLocationViewModel.class);
        appSettings = new AppSettings(this);
        intent = getIntent();
        initBottomSheet();
        initMap();
        initListners();

    }


    private void initListners() {
        homeLayout.setOnClickListener(this);
        workLayout.setOnClickListener(this);
        otherLayout.setOnClickListener(this);
        backimg.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        setHalfAlpha(homeLayout);
        setHalfAlpha(workLayout);
        setHalfAlpha(otherLayout);
    }

    private void initMap() {
        mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment));
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();
        setAfterPermission();
        getLocation();
        startLocationUpdates();
    }

    private void initBottomSheet() {
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutParams = (CoordinatorLayout.LayoutParams) topPanel.getLayoutParams();
                layoutParams.height = bottomSheet.getTop();
                topPanel.setLayoutParams(layoutParams);
                topPanel.requestLayout();
            }
        }, 500);


        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {

            }

            @Override
            public void onSlide(@NonNull View view, float v) {
                animateMapView(view.getTop(), topPanel);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mapFragment != null)
            mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.getUiSettings().isMyLocationButtonEnabled();
        mGoogleMap.getUiSettings().isZoomControlsEnabled();
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.setMyLocationEnabled(true);


        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (mGoogleMap != null) {
                    mGoogleMap.clear();
                }

                LatLng mPosition = mGoogleMap.getCameraPosition().target;
                selectedLat = String.valueOf(mPosition.latitude);
                selectedLong = String.valueOf(mPosition.longitude);
                if (intent.getStringExtra(ConstantKeys.INTENTKEYS.REASON_INTENT).equalsIgnoreCase(ConstantKeys.EDIT_ADDRESS)) {
                    house_number.setText(intent.getStringExtra(ConstantKeys.INTENTKEYS.HOUSE_FLAT_NO));
                    landmark.setText(intent.getStringExtra(ConstantKeys.INTENTKEYS.LANDMARK));
                    locationName.setText(intent.getStringExtra(ConstantKeys.INTENTKEYS.ADDRESS_LOCATION));
                    type = intent.getStringExtra(ConstantKeys.INTENTKEYS.ADDRESS_TYPE);
                    Log.e(TAG, "onCameraIdle: " + type);
                    if (type.equalsIgnoreCase(ConstantKeys.HOME)) {
                        setHomeSelected();
                    } else if (type.equalsIgnoreCase(ConstantKeys.WORK)) {
                        setWorkSelected();
                    } else if (type.equalsIgnoreCase(ConstantKeys.OTHER)) {
                        setOtherSelected();
                    } else {
                        setHalfAlpha(homeLayout);
                        setHalfAlpha(workLayout);
                        setFullAlpha(otherLayout);
                        othersAddresLayout.setVisibility(View.VISIBLE);
                        otherAddress.setText(type);
                    }

                    if (type.equalsIgnoreCase(ConstantKeys.HOME)||type.equalsIgnoreCase(ConstantKeys.WORK)) {
                        type = intent.getStringExtra(ConstantKeys.INTENTKEYS.ADDRESS_TYPE);
                    }else{
                        type="Other";
                    }
                }

                if (selectedLat != null && selectedLong != null) {
                    getAddressForLocation();
                }

            }
        });

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, SaveLocationActivity.this);

        mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (intent.getStringExtra(ConstantKeys.FROM_ACTIVITY).equalsIgnoreCase(ChooseLocationActivity.class.getSimpleName())
                || intent.getStringExtra(ConstantKeys.FROM_ACTIVITY).equalsIgnoreCase(ManageAddressActivity.class.getSimpleName())
        ) {
            //            selectedLat = String.valueOf(intent.getStringExtra(ConstantKeys.INTENTKEYS.LATITUDE));
//            selectedLong = String.valueOf(intent.getStringExtra(ConstantKeys.INTENTKEYS.LATITUDE));
            String lat = null;
            String lng = null;
            selectedLat = lat;
            selectedLong = lng;
            lat = intent.getStringExtra(ConstantKeys.INTENTKEYS.LATITUDE);
            lng = intent.getStringExtra(ConstantKeys.INTENTKEYS.LONGITUDE);
            adddressId = intent.getStringExtra(ConstantKeys.INTENTKEYS.ADDRESS_ID);

            if(intent.getStringExtra(ConstantKeys.FROM_ACTIVITY).equalsIgnoreCase(ManageAddressActivity.class.getSimpleName())){
                if (type.equalsIgnoreCase(ConstantKeys.HOME)||type.equalsIgnoreCase(ConstantKeys.WORK)) {
                    type = intent.getStringExtra(ConstantKeys.INTENTKEYS.ADDRESS_TYPE);
                }else{
                    type="Other";
                }
            }

            if (lat != null && lng != null) {
                Utils.log(TAG, "LATITUDE: " + lat + " Longitude: " + lng);
                double currentLatitude = Double.parseDouble(lat);
                double currentLongitude = Double.parseDouble(lng);
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 17));
            }
        } else {
            if (mLocation != null) {
                double currentLatitude = mLocation.getLatitude();
                double currentLongitude = mLocation.getLongitude();
                selectedLat = String.valueOf(mLocation.getLatitude());
                selectedLong = String.valueOf(mLocation.getLongitude());
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 17));
            }
        }
    }

    public void animateMapView(int end, RelativeLayout topPanel) {
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) topPanel.getLayoutParams();

        ResizeAnimation a = new ResizeAnimation(topPanel);
        a.setDuration(250);

        if (!getMapViewStatus()) {
            mMapViewExpanded = true;
            a.setParams(lp.height, end);
        } else {
            mMapViewExpanded = false;
            a.setParams(lp.height, end);
        }
        topPanel.startAnimation(a);
    }

    private boolean getMapViewStatus() {
        return mMapViewExpanded;
    }


    private void getAddressForLocation() {
        progressBarOTP.setVisibility(View.VISIBLE);
        InputForAPI inputForAPI = new InputForAPI(SaveLocationActivity.this);
        inputForAPI.setUrl(getLocationSearchUrl(selectedLat, selectedLong));
        inputForAPI.setFile(null);
        inputForAPI.setHeaders(new HashMap<String, String>());
        inputForAPI.setJsonObject(new JSONObject());

        saveLocationViewModel.getCurrentAddress(inputForAPI).observe(this, new Observer<LocationResponseModel>() {
            @Override
            public void onChanged(@Nullable LocationResponseModel locationResponseModel) {
                progressBarOTP.setVisibility(View.INVISIBLE);

                if (locationResponseModel.getError()) {
                    Utils.showSnackBar(findViewById(android.R.id.content), locationResponseModel.getErrorMessage());
                } else {
                    locationName.setText(locationResponseModel.getFormatedAddress());
                }
            }
        });

    }

    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);

    }


    @Override
    public void onClick(View view) {
        if (view == homeLayout) {
            setHomeSelected();
        } else if (view == workLayout) {
            setWorkSelected();
        } else if (view == otherLayout) {
            setOtherSelected();

        } else if (view == backimg) {
            finish();
        } else if (view == saveButton) {
            try {
                validate();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void validate() throws JSONException {
        if (locationName.getText().toString().length() == 0) {
            Utils.showSnackBar(findViewById(android.R.id.content), this.getResources().getString(R.string.enter_location));
        } else if (house_number.getText().toString().length() == 0) {
            Utils.showSnackBar(findViewById(android.R.id.content), this.getResources().getString(R.string.enter_house_flat_no));

        } else if (type.length() == 0) {
            Utils.showSnackBar(findViewById(android.R.id.content), this.getResources().getString(R.string.choose_type));
        } else if (type.equalsIgnoreCase(ConstantKeys.OTHER)) {
            if (otherAddress.getText().toString().trim().length() == 0) {
                Utils.showSnackBar(findViewById(android.R.id.content), getResources().getString(R.string.please_enter_other));
            } else {
                if (intent.getStringExtra(ConstantKeys.INTENTKEYS.REASON_INTENT).equalsIgnoreCase(ConstantKeys.EDIT_ADDRESS)) {

                    updateAddress();

                } else {

                    saveAddress();
                }
            }
        } else {
            if (intent.getStringExtra(ConstantKeys.INTENTKEYS.REASON_INTENT).equalsIgnoreCase(ConstantKeys.EDIT_ADDRESS)) {

                updateAddress();

            } else {

                saveAddress();
            }
        }
    }

    private void updateAddress() throws JSONException {
        progressBarOTP.setVisibility(View.VISIBLE);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.LOCATION, locationName.getText().toString().trim());
        jsonObject.put(ConstantKeys.ADDRESS_ID, adddressId);
        jsonObject.put(ConstantKeys.HOUSE_FLAT_NO, house_number.getText().toString().trim());
        jsonObject.put(ConstantKeys.LANDMARK, landmark.getText().toString().trim());
        if (type.equalsIgnoreCase(ConstantKeys.OTHER)) {
            if (otherAddress.getText().toString().trim().length() != 0) {
                jsonObject.put(ConstantKeys.TYPE, otherAddress.getText().toString().trim());
            } else {
                jsonObject.put(ConstantKeys.TYPE, type.trim());
            }
        } else {
            jsonObject.put(ConstantKeys.TYPE, type);
        }
        jsonObject.put(ConstantKeys.LATITUDE, selectedLat);
        jsonObject.put(ConstantKeys.LONGITUDE, selectedLong);

        InputForAPI inputForAPI = new InputForAPI(SaveLocationActivity.this);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setUrl(UrlHelper.UPDATE_ADDRESS);


        saveLocationViewModel.addAddress(inputForAPI).observe(this, new Observer<SaveLocationResponseModel>() {
            @Override
            public void onChanged(@Nullable SaveLocationResponseModel saveLocationResponseModel) {

                progressBarOTP.setVisibility(View.INVISIBLE);

                if (saveLocationResponseModel.getError()) {
                    Utils.showSnackBar(findViewById(android.R.id.content), saveLocationResponseModel.getErrorMessage());
                } else {
                    appSettings.setAddressId(saveLocationResponseModel.getAddressId());
                    type = "";
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }
        });
    }

    private void saveAddress() throws JSONException {
        progressBarOTP.setVisibility(View.VISIBLE);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.LOCATION, locationName.getText().toString().trim());
        jsonObject.put(ConstantKeys.HOUSE_FLAT_NO, house_number.getText().toString().trim());
        jsonObject.put(ConstantKeys.LANDMARK, landmark.getText().toString().trim());
        if (type.equalsIgnoreCase(ConstantKeys.OTHER)) {
            if (otherAddress.getText().toString().trim().length() != 0) {
                jsonObject.put(ConstantKeys.TYPE, otherAddress.getText().toString().trim());
            } else {
                jsonObject.put(ConstantKeys.TYPE, type.trim());
            }
        } else {
            jsonObject.put(ConstantKeys.TYPE, type.trim());
        }
        jsonObject.put(ConstantKeys.LATITUDE, selectedLat);
        jsonObject.put(ConstantKeys.LONGITUDE, selectedLong);
        InputForAPI inputForAPI = new InputForAPI(SaveLocationActivity.this);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));
        inputForAPI.setJsonObject(jsonObject);
        if (intent.getStringExtra(ConstantKeys.INTENTKEYS.REASON_INTENT).equalsIgnoreCase(ConstantKeys.EDIT_ADDRESS)) {
            inputForAPI.setUrl(UrlHelper.ADD_ADDRESS);

        } else {

            inputForAPI.setUrl(UrlHelper.ADD_ADDRESS);
        }

        saveLocationViewModel.addAddress(inputForAPI).observe(this, new Observer<SaveLocationResponseModel>() {
            @Override
            public void onChanged(@Nullable SaveLocationResponseModel saveLocationResponseModel) {

                progressBarOTP.setVisibility(View.INVISIBLE);

                if (saveLocationResponseModel.getError()) {
                    Utils.showSnackBar(findViewById(android.R.id.content), saveLocationResponseModel.getErrorMessage());
                } else {
                    appSettings.setAddressId(saveLocationResponseModel.getAddressId());
                    type = "";
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
//                    Intent intent = new Intent(SaveLocationActivity.this, MainActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
                }
            }
        });
    }

    private void setOtherSelected() {

        setHalfAlpha(homeLayout);
        setHalfAlpha(workLayout);
        setFullAlpha(otherLayout);
        type = ConstantKeys.OTHER;
        othersAddresLayout.setVisibility(View.VISIBLE);

    }

    private void setWorkSelected() {

        setHalfAlpha(homeLayout);
        setFullAlpha(workLayout);
        setHalfAlpha(otherLayout);
        type = ConstantKeys.WORK;
        othersAddresLayout.setVisibility(View.GONE);
    }

    private void setHomeSelected() {
        setFullAlpha(homeLayout);
        setHalfAlpha(workLayout);
        setHalfAlpha(otherLayout);
        type = ConstantKeys.HOME;

        othersAddresLayout.setVisibility(View.GONE);

    }

    private void setFullAlpha(RelativeLayout layout) {
        layout.setAlpha(1);
    }

    private void setHalfAlpha(RelativeLayout layout) {
        layout.setAlpha((float) 0.4);

    }

    public String getLocationSearchUrl(String latitude, String longitude) {

        return "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true&key=" + new AppSettings(this).getApiMapKey();
    }


}
