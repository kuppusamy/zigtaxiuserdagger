package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.GetSavedAddressResponse.Address;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.InterFaces.ManageAdapterInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ManageAddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<Address> list;
    ManageAdapterInterface manageAdapterInterface;


    public ManageAddressAdapter(Context context, List<Address> addresses) {
        this.context = context;
        this.list = addresses;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.manage_address_item, parent, false);

        RecyclerView.ViewHolder viewHolder;
        viewHolder = new Addresses(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        final Addresses holder = (Addresses) viewHolder;
        holder.typeText.setText(list.get(i).getType());
        holder.addressText.setText(list.get(i).getLocation());

        if (list.get(i).getType().equalsIgnoreCase(ConstantKeys.HOME)) {
            holder.typeImage.setImageDrawable(context.getResources().getDrawable(R.drawable.home_icon));
        } else if (list.get(i).getType().equalsIgnoreCase(ConstantKeys.WORK)) {
            holder.typeImage.setImageDrawable(context.getResources().getDrawable(R.drawable.work_icon));
        } else {
            holder.typeImage.setImageDrawable(context.getResources().getDrawable(R.drawable.location_icon));
        }

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageAdapterInterface.editAddress(list.get(i));
            }
        });

        holder.deleteAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                manageAdapterInterface.removeAddress(list.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return (list != null) ? list.size() : 0;
    }

    public void setOnClickListner(ManageAdapterInterface manageAdapterInterface) {
        this.manageAdapterInterface = manageAdapterInterface;
    }

    class Addresses extends RecyclerView.ViewHolder {

        @BindView(R2.id.typeImage)
        ImageView typeImage;
        @BindView(R2.id.typeText)
        TextView typeText;
        @BindView(R2.id.addressText)
        TextView addressText;
        @BindView(R2.id.deleteAddress)
        ImageView deleteAddress;
        @BindView(R2.id.editButton)
        TextView editButton;

        public Addresses(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
