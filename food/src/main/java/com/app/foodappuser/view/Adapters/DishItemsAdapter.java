package com.app.foodappuser.view.Adapters;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ListDishesResponse.DishItems;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.ApiCall.ImageLoader;
import com.app.foodappuser.Utilities.InterFaces.DishesAdapterInterface;
import com.app.foodappuser.Utilities.InterFaces.onClickListener;
import com.app.foodappuser.Utilities.InterFaces.onDatabaseAdded;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.Utilities.Ticker.TickerView;
import com.app.foodappuser.Utilities.UiUtils.ShadowView.ShadowView;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class DishItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final String TAG = DishItemsAdapter.class.getSimpleName();
    onClickListener onClickListener;
    List<DishItems> dishItems;
    ImageLoader imageLoader;
    private Context context;
    private boolean isRecommended;
    AppSettings appSettings;
    DishesAdapterInterface dishesAdapterInterface;
    private boolean isVegSwitch;
    List<DishItems> vegOnlyDish;
    List<DishItems> vegOnlyDishs = new ArrayList<>();

    public DishItemsAdapter(Context context, List<DishItems> dishItems, List<DishItems> vegOnlyDish, boolean isRecommended, DishesAdapterInterface dishesAdapterInterface, boolean isVegSwitch) {

        this.context = context;
        this.dishItems = dishItems;
        this.isRecommended = isRecommended;
        imageLoader = new ImageLoader(context);
        appSettings = new AppSettings(context);
        this.dishesAdapterInterface = dishesAdapterInterface;
        this.isVegSwitch = isVegSwitch;
        if (isVegSwitch) {
            for (int i = 0; i < dishItems.size(); i++) {
                if (dishItems.get(i).getIsVeg() == 1) {
                    vegOnlyDishs.add(dishItems.get(i));
                }
            }
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        if (viewType == 0) {
            itemView = LayoutInflater.from(context).inflate(R.layout.recommended_dish_item, parent, false);
        } else {
            itemView = LayoutInflater.from(context).inflate(R.layout.dish_item, parent, false);
        }

        RecyclerView.ViewHolder viewHolder;
        viewHolder = new DishItemHolder(itemView);
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        if (isRecommended)
            return 0;
        else {
            return 1;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

        DishItemHolder dishItemHolder = (DishItemHolder) viewHolder;
        if (isVegSwitch) {
            DishItems dishItem = vegOnlyDishs.get(position);
            setAdapterValues(dishItemHolder, dishItem, position);
        } else {
            DishItems dishItem = dishItems.get(position);
            setAdapterValues(dishItemHolder, dishItem, position);
        }
    }


    private void setAdapterValues(final DishItemHolder dishItemHolder, final DishItems dishItem, final int position) {

        dishItemHolder.dishName.setText(dishItem.getDishName());
        if (isRecommended) {
            imageLoader.load(dishItem.getDishImage(), dishItemHolder.dishImage);
        }
        dishItemHolder.description.setText(dishItem.getCuisineType());
        dishItemHolder.slashPrice.setText(appSettings.getCurrency() + " " + dishItem.getSlashedPrice());
        dishItemHolder.price.setText(dishItem.getDisplayPrice());
        dishItemHolder.slashPrice.setPaintFlags(dishItemHolder.slashPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (dishItem.getQuantity() == 0.0) {
            dishItemHolder.quantity.setText("0");

        } else {
            dishItemHolder.quantity.setText(String.valueOf((int) dishItem.getQuantity()));

        }

        if (dishItem.getIsVeg() == 1) {
            dishItemHolder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.veg_icon));
        } else {
            dishItemHolder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.non_veg_icon));
        }

        boolean isDishAvailable = getIsDishAvailable(dishItem.getAvailableFrom().trim(), dishItem.getAvailableTo().trim());
        String notAvailableText = getAvailableText(dishItem.getAvailableFrom());


        if (dishItem.getAvailable() != null) {
            if (!dishItem.getAvailable()) {
                dishItemHolder.actionLayout.setVisibility(View.INVISIBLE);
                dishItemHolder.customizationText.setVisibility(View.INVISIBLE);
                dishItemHolder.addText.setVisibility(View.INVISIBLE);
                dishItemHolder.unAvailableText.setVisibility(View.VISIBLE);
                dishItemHolder.unAvailableText.setText(notAvailableText);
            } else {
                dishItemHolder.addText.setVisibility(View.INVISIBLE);
                dishItemHolder.unAvailableText.setVisibility(View.INVISIBLE);

                if (dishItem.getQuantity() == 0.0) {
                    dishItemHolder.actionLayout.setVisibility(View.INVISIBLE);
                    dishItemHolder.addText.setVisibility(View.VISIBLE);
                } else {
                    if (dishItem.getQuantity() == 0.0) {
                        dishItemHolder.actionLayout.setVisibility(View.INVISIBLE);
                        dishItemHolder.addText.setVisibility(View.VISIBLE);
                    } else {
                        dishItemHolder.actionLayout.setVisibility(View.VISIBLE);
                        dishItemHolder.addText.setVisibility(View.INVISIBLE);
                    }
                }

                if (dishItem.getSlashedPrice().equalsIgnoreCase("")) {
                    dishItemHolder.slashPrice.setVisibility(View.GONE);
                } else {
                    dishItemHolder.slashPrice.setVisibility(View.VISIBLE);
                }

                if (dishItem.getIsCustomizable() == 1) {
                    dishItemHolder.customizationText.setVisibility(View.VISIBLE);
                    dishItemHolder.customizationText.setText(context.getResources().getString(R.string.customization_available));
                } else {
                    dishItemHolder.customizationText.setVisibility(View.INVISIBLE);
                }
            }
        }


        dishItemHolder.addText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dishItem.getIsCustomizable() == 0) {
                    dishesAdapterInterface.onIncreased(dishItem, new onDatabaseAdded() {
                        @Override
                        public void onAdded(final Bundle bundle) {
                            if (bundle != null) {
                                if (bundle.getBoolean(ConstantKeys.STATUS)) {

                                    showCountWithAnimation(dishItemHolder.quantity, bundle.getString(ConstantKeys.QUANTITY));

                                    AlphaAnimation animation1 = new AlphaAnimation(1f, 0f);
                                    animation1.setDuration(400);
                                    dishItemHolder.addText.setAlpha(1f);
                                    animation1.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                            dishItemHolder.actionLayout.setVisibility(View.VISIBLE);
                                            dishItemHolder.addText.setEnabled(false);

                                            slideInRight(dishItemHolder.increaseCart);
                                            slideInLeft(dishItemHolder.decreaseCart);

//                                            dishItemHolder.quantity.setText(bundle.getString(ConstantKeys.QUANTITY));
                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            dishItemHolder.addText.setEnabled(true);

                                            dishItemHolder.addText.setVisibility(View.INVISIBLE);
                                            dishItemHolder.addText.setAlpha(1f);
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {

                                        }
                                    });
                                    dishItemHolder.addText.startAnimation(animation1);

                                }
                            }
                        }
                    });


                } else {
                    dishesAdapterInterface.onCustomization(dishItem, new onDatabaseAdded() {
                        @Override
                        public void onAdded(Bundle bundle) {
                            if (bundle != null) {
                                if (bundle.getBoolean(ConstantKeys.STATUS)) {
                                    dishItemHolder.actionLayout.setVisibility(View.VISIBLE);
                                    dishItemHolder.addText.setVisibility(View.INVISIBLE);
                                    dishItemHolder.quantity.setText(bundle.getString(ConstantKeys.QUANTITY));

                                }
                            } else {
                                Utils.log(TAG, "Null Pointer");
                            }
                        }
                    });

                }

            }
        });


        dishItemHolder.increaseCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (dishItem.getIsCustomizable() == 0) {
                    dishesAdapterInterface.onIncreased(dishItem, new onDatabaseAdded() {
                        @Override
                        public void onAdded(Bundle bundle) {
                            if (bundle != null) {
                                if (bundle.getBoolean(ConstantKeys.STATUS)) {
                                    dishItemHolder.quantity.setText(bundle.getString(ConstantKeys.QUANTITY));
                                }
                            }
                        }
                    });

                } else {
//                    dishesAdapterInterface.onCustomization(dishItem);
                    dishesAdapterInterface.onCustomization(dishItem, new onDatabaseAdded() {
                        @Override
                        public void onAdded(Bundle bundle) {
                            if (bundle != null) {
                                if (bundle.getBoolean(ConstantKeys.STATUS)) {
                                    dishItemHolder.quantity.setText(bundle.getString(ConstantKeys.QUANTITY));
                                }
                            } else {
                                Utils.log(TAG, "Null POinter");
                            }
                        }
                    });


                }
            }
        });

        dishItemHolder.decreaseCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dishItem.getIsCustomizable() == 0) {
                    dishesAdapterInterface.onDecreased(dishItem, false, new onDatabaseAdded() {
                        @Override
                        public void onAdded(final Bundle bundle) {
                            if (bundle != null) {
                                if (bundle.getBoolean(ConstantKeys.STATUS)) {

                                    if (bundle.getString(ConstantKeys.QUANTITY).equalsIgnoreCase("0")) {

                                        hideCountWithAnimation(dishItemHolder.quantity, bundle.getString(ConstantKeys.QUANTITY));

                                        slideOutLeft(dishItemHolder.decreaseCart, dishItemHolder.actionLayout);
                                        slideOutRight(dishItemHolder.increaseCart);

                                        AlphaAnimation animation1 = new AlphaAnimation(0f, 1f);
                                        animation1.setDuration(800);
                                        dishItemHolder.addText.setAlpha(1f);
                                        animation1.setAnimationListener(new Animation.AnimationListener() {
                                            @Override
                                            public void onAnimationStart(Animation animation) {
                                                dishItemHolder.addText.setEnabled(false);
                                            }

                                            @Override
                                            public void onAnimationEnd(Animation animation) {
//                                                dishItemHolder.actionLayout.setVisibility(View.INVISIBLE);
                                                dishItemHolder.addText.setEnabled(true);

                                                dishItemHolder.addText.setVisibility(View.VISIBLE);
//                                                dishItemHolder.quantity.setText(bundle.getString(ConstantKeys.QUANTITY));
                                                dishItemHolder.addText.setAlpha(1f);
                                            }

                                            @Override
                                            public void onAnimationRepeat(Animation animation) {

                                            }
                                        });
                                        dishItemHolder.addText.startAnimation(animation1);


//                                        dishItemHolder.actionLayout.setVisibility(View.INVISIBLE);
//                                        dishItemHolder.addText.setVisibility(View.VISIBLE);
//                                        dishItemHolder.quantity.setText(bundle.getString(ConstantKeys.QUANTITY));

                                    } else {
                                        dishItemHolder.quantity.setText(bundle.getString(ConstantKeys.QUANTITY));
                                    }
                                }
                            } else {
                                Utils.log(TAG, "Null POinter");
                            }
                        }
                    });

                } else {
                    dishesAdapterInterface.onDecreased(dishItem, true, new onDatabaseAdded() {
                        @Override
                        public void onAdded(Bundle bundle) {

                        }
                    });
                }
            }
        });
    }

    private void hideCountWithAnimation(final TickerView quantity, final String string) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1f, 0f);
        alphaAnimation.setDuration(500);
        quantity.setAlpha(1f);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                quantity.setVisibility(View.INVISIBLE);
                quantity.setText(string);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        quantity.setAnimation(alphaAnimation);
    }

    private void showCountWithAnimation(final TickerView quantity, final String string) {
        quantity.setVisibility(View.VISIBLE);
        quantity.setText(string);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0f, 1f);
        alphaAnimation.setDuration(500);
        quantity.setAlpha(1f);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        quantity.setAnimation(alphaAnimation);
    }

    private void slideInLeft(final RelativeLayout decreaseCart) {
        decreaseCart.setVisibility(View.VISIBLE);
        AnimationSet animationSet = new AnimationSet(false);

        AlphaAnimation alphaAnimation = new AlphaAnimation(0f, 1f);
        alphaAnimation.setDuration(500);
        decreaseCart.setAlpha(1f);

        Animation animFadeIn = AnimationUtils.loadAnimation(context, R.anim.slide_in_left);
        animFadeIn.setDuration(400);

        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(animFadeIn);

        decreaseCart.startAnimation(animationSet);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                decreaseCart.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                decreaseCart.setEnabled(true);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    private void slideInRight(final RelativeLayout increaseCart) {
        increaseCart.setVisibility(View.VISIBLE);
        AnimationSet animationSet = new AnimationSet(false);

        AlphaAnimation alphaAnimation = new AlphaAnimation(0f, 1f);
        alphaAnimation.setDuration(500);
        increaseCart.setAlpha(1f);

        Animation animFadeIn = AnimationUtils.loadAnimation(context, R.anim.slide_in_right);
        animFadeIn.setDuration(400);

        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(animFadeIn);

        increaseCart.startAnimation(animationSet);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                increaseCart.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                increaseCart.setEnabled(true);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void slideOutLeft(final RelativeLayout decreaseCart, final LinearLayout actionLayout) {

        AnimationSet animationSet = new AnimationSet(false);

        AlphaAnimation alphaAnimation = new AlphaAnimation(1f, 0f);
        alphaAnimation.setDuration(400);
        decreaseCart.setAlpha(1f);

        Animation animFadeIn = AnimationUtils.loadAnimation(context, R.anim.slide_out_left);
        animFadeIn.setDuration(500);

        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(animFadeIn);

        decreaseCart.startAnimation(animationSet);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                decreaseCart.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                actionLayout.setVisibility(View.INVISIBLE);
                decreaseCart.setEnabled(true);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void slideOutRight(final RelativeLayout increaseCart) {
        increaseCart.setVisibility(View.VISIBLE);
        AnimationSet animationSet = new AnimationSet(false);

        AlphaAnimation alphaAnimation = new AlphaAnimation(1f, 0f);
        alphaAnimation.setDuration(400);
        increaseCart.setAlpha(1f);

        Animation animFadeIn = AnimationUtils.loadAnimation(context, R.anim.slide_out_right);
        animFadeIn.setDuration(500);

        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(animFadeIn);

        increaseCart.startAnimation(animationSet);

        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                increaseCart.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                increaseCart.setEnabled(true);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public boolean getIsDishAvailable(String availableFrom, String availableTo) {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.MILLISECOND, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.HOUR_OF_DAY, 0);
        long millisecondsDate = today.getTimeInMillis();
        long from = millisecondsDate + Integer.parseInt(availableFrom);
        long to = millisecondsDate + Integer.parseInt(availableTo);
        if (System.currentTimeMillis() < from || System.currentTimeMillis() > to) {
            return false;
        } else {
            return true;
        }
    }


    public String getAvailableText(String availableFrom) {
        String hourText = Utils.getDate(Long.parseLong(availableFrom), "h a");
        return Utils.getFormattedString(context, R.string.unavailable_text_template, hourText);

    }

    @Override
    public int getItemCount() {
        if (isVegSwitch) {
            return (vegOnlyDishs != null) ? vegOnlyDishs.size() : 0;
        } else {
            return (dishItems != null) ? dishItems.size() : 0;
        }
    }


    public void setOnClickListner(onClickListener onClickListner) {
        this.onClickListener = onClickListner;
    }

    public void onDishChanged(String id, String quantity) {

        Utils.log(TAG, "Dish Item Size : " + dishItems.size());

        for (int i = 0; i < dishItems.size(); i++) {
            if (dishItems.get(i).getDishId().equalsIgnoreCase(id)) {
                dishItems.get(i).setQuantity(Integer.valueOf(quantity));
                notifyItemChanged(i);

                Utils.log(TAG, "Notified Item : " + i);
            }
        }

    }

    public void onItemChanged(int j, Integer valueOf) {
        dishItems.get(j).setQuantity(valueOf);
        notifyItemChanged(j);
    }


    class DishItemHolder extends RecyclerView.ViewHolder {
        private RoundedImageView dishImage;
        private TextView dishName;
        private TextView unAvailableText;
        private TextView description;
        private TextView price;
        private TextView slashPrice;
        private TextView addText;
        private RelativeLayout increaseCart;
        private TickerView quantity;
        private RelativeLayout decreaseCart;
        private LinearLayout actionLayout;
        private TextView customizationText;
        private ShadowView shadowview;
        private LinearLayout subCategoryLayout;
        private ImageView isVegView;

        DishItemHolder(View view) {
            super(view);
            subCategoryLayout = itemView.findViewById(R.id.subCategoryLayout);
            unAvailableText = itemView.findViewById(R.id.unAvailableText);
            shadowview = itemView.findViewById(R.id.shadow_view);
            customizationText = itemView.findViewById(R.id.customizationText);
            actionLayout = itemView.findViewById(R.id.actionLayout);
            decreaseCart = itemView.findViewById(R.id.decreaseCart);
            quantity = itemView.findViewById(R.id.quantity);
            increaseCart = itemView.findViewById(R.id.increaseCart);
            addText = itemView.findViewById(R.id.addText);
            slashPrice = itemView.findViewById(R.id.slashPrice);
            price = itemView.findViewById(R.id.price);
            description = itemView.findViewById(R.id.description);
            dishName = itemView.findViewById(R.id.dishName);
            isVegView = itemView.findViewById(R.id.isVegView);
            if (isRecommended) {
                dishImage = itemView.findViewById(R.id.dishImage);
            }
        }
    }
}