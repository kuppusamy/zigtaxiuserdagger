package com.app.foodappuser.view.activities;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.app.foodappuser.Model.Response.SplashScreenResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.ApiCall.NetworkStateReceiver;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.BaseUtils.ViewAnimationUtils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.ViewModel.SplashScreenViewModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;

import butterknife.BindView;

public class SplashActivity extends BaseActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private static final String TAG = SplashActivity.class.getSimpleName();
    AppSettings appSettings;
    SplashScreenViewModel splashScreenViewModel;
    Snackbar snackbar = null;

    @BindView(R2.id.splashLayout)
    ConstraintLayout splashLayout;

    NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashScreenViewModel = ViewModelProviders.of(this).get(SplashScreenViewModel.class);
        Log.e(TAG, "onCreate: " + checkPlayServices());
        registerNetworkListener();

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                Dialog dialog = apiAvailability.getErrorDialog(this, resultCode, 2);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            } else {
                Log.i("Demo App", "This device is not supported.");

                AlertDialog.Builder dialog_app = new AlertDialog.Builder(SplashActivity.this);
                dialog_app.setTitle("Error");
                dialog_app.setMessage("This device is not supported..");
                dialog_app.setCancelable(false);
                dialog_app.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                dialog_app.show();
            }
            return false;
        }
        return true;
    }

    private void registerNetworkListener() {
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void getLoginSettingResponse() {

        InputForAPI inputForAPI = new InputForAPI(SplashActivity.this);
        inputForAPI.setUrl(UrlHelper.LOGIN_SETTING);
        inputForAPI.setFile(null);
        inputForAPI.setHeaders(new HashMap<String, String>());

        splashScreenViewModel.SplashScreenResponse(inputForAPI).observe(this, new Observer<SplashScreenResponseModel>() {
            @Override
            public void onChanged(@Nullable SplashScreenResponseModel splashScreenResponseModel) {
                if (splashScreenResponseModel.getError()) {
                    snackbar = Utils.showSnackBarIndefinite(findViewById(android.R.id.content), splashScreenResponseModel.getErrorMessage());
                } else {
                    if (snackbar != null)
                        snackbar.dismiss();
                    appSettings.setCurrency(splashScreenResponseModel.getCurrency());
                    appSettings.setTermsAndConditions(splashScreenResponseModel.getTermsAndConditions());
                    appSettings.setApiMapKey(splashScreenResponseModel.getAndroidMapKey());
                    appSettings.setIsTwilioOtp(splashScreenResponseModel.getIsTwilioOtp());

                    if (splashScreenResponseModel.getIsPasswordLogin()) {
                        appSettings.setIsPasswordLoginEnabled(ConstantKeys.TRUE_STRING);
                    } else {
                        appSettings.setIsPasswordLoginEnabled(ConstantKeys.FALSE_STRING);
                    }

                    if (appSettings.getIsLoggedIn().equalsIgnoreCase(ConstantKeys.FALSE_STRING) || appSettings.getIsLoggedIn().equalsIgnoreCase("")) {
                        moveSignInActivity();
                    } else {
                        moveMainActivity();
                    }
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    private void processSplashLogic() {
        String deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        appSettings = new AppSettings(SplashActivity.this);
        Handler handler = new Handler();
        appSettings.setUdId(deviceId);
        getLoginSettingResponse();
    }

    private void moveSignInActivity() {

        if (appSettings.getIsWalkThroughPassed().equalsIgnoreCase(ConstantKeys.TRUE_STRING) || appSettings.getIsWalkThroughPassed().equalsIgnoreCase("")) {
            moveWalkThroughActivity();
        } else {
            Intent intent = new Intent(SplashActivity.this, SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            ViewAnimationUtils.startActivityTransaction(this);
            startActivity(intent);
        }
    }

    private void moveMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ViewAnimationUtils.startActivityTransaction(this);
        startActivity(intent);
    }

    private void moveWalkThroughActivity() {
        Intent intent = new Intent(SplashActivity.this, WalkThroughActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ViewAnimationUtils.startActivityTransaction(this);
        startActivity(intent);
    }


    @Override
    public void networkAvailable() {
        if (snackbar != null)
            snackbar.dismiss();
        processSplashLogic();
    }

    @Override
    public void networkUnavailable() {
        snackbar = Utils.showSnackBarIndefinite(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection));
    }
}
