package com.app.foodappuser.view.Fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.SelectedTab;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.InterFaces.RestaurantAdapterInterface;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.BaseUtils.onExploreResultsFetched;
import com.app.foodappuser.view.Adapters.ExploreRestaurantAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RestaurantExploreFragment extends Fragment {

    View view;

    @BindView(R2.id.restaurantRecyclerView)
    RecyclerView restaurantRecyclerView;
    @BindView(R2.id.relatedTo)
    TextView relatedTo;

    ExploreRestaurantAdapter restaurantAdapter;


    public RestaurantExploreFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dishes_explore, container, false);
        ButterKnife.bind(this, view);

        Utils.log("selected", String.valueOf(new SelectedTab().getSelectedTab()));

        return view;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onExploreResultsFetched(final onExploreResultsFetched mOnExploreResultsFetched) {

        LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        restaurantRecyclerView.setLayoutManager(layout);
        restaurantAdapter = new ExploreRestaurantAdapter(getActivity(), mOnExploreResultsFetched.getMlistRestaurantResponseModel().getListRestaurants());
        restaurantRecyclerView.setAdapter(restaurantAdapter);
        restaurantAdapter.setOnClickListner(new RestaurantAdapterInterface() {
            @Override
            public void onClick() {

            }

            @Override
            public void setRecentSearches(String outletName, String outletId, String cuisines, String displayTime, String averageReview, String displayCostForTwo, String couponEnabledForRestaurant, String longDescription) {
                mOnExploreResultsFetched.getExploreViewModel().setRecentSearches(outletName,outletId,cuisines,displayTime,averageReview,displayCostForTwo,couponEnabledForRestaurant,longDescription);
            }

        });

        if (mOnExploreResultsFetched.getMvalue()) {
            relatedTo.setText(getActivity().getResources().getString(R.string.related_to) + " \"" + mOnExploreResultsFetched.getMtypedTet() + "\"");
        } else {
            relatedTo.setText(getActivity().getResources().getString(R.string.no_restaurant_match) + " \"" + mOnExploreResultsFetched.getMtypedTet() + "\"");
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            EventBus.getDefault().unregister(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
