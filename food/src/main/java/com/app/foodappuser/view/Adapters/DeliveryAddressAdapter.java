package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ListDeliveryResponse.ListDeliveryAddress;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.InterFaces.DeliverAddressAdapterInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeliveryAddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private List<ListDeliveryAddress> listDeliveryAddresses;
    DeliverAddressAdapterInterface deliverAddressAdapterInterface;


    public DeliveryAddressAdapter(Context context, List<ListDeliveryAddress> listDeliveryAddress) {
        this.context = context;
        this.listDeliveryAddresses = listDeliveryAddress;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.delivery_address_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        final ViewHolder holder = (ViewHolder) viewHolder;
        final ListDeliveryAddress listDeliveryAddress = listDeliveryAddresses.get(i);

        holder.deliverAddressText.setText(listDeliveryAddress.getAddress());
        holder.deliverMinsText.setText(listDeliveryAddress.getDisplayTime());
        holder.typeText.setText(listDeliveryAddress.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deliverAddressAdapterInterface.setAddressId(String.valueOf(listDeliveryAddress.getAddressId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return (listDeliveryAddresses != null) ? listDeliveryAddresses.size() : 0;

    }

    public void setOnClickListener(DeliverAddressAdapterInterface deliverAddressAdapterInterface) {
        this.deliverAddressAdapterInterface=deliverAddressAdapterInterface;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.deliverTypeImage)
        ImageView deliverTypeImage;
        @BindView(R2.id.typeText)
        TextView typeText;
        @BindView(R2.id.deliverAddressText)
        TextView deliverAddressText;
        @BindView(R2.id.deliverMinsText)
        TextView deliverMinsText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}