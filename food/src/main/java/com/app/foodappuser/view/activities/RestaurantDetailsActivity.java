package com.app.foodappuser.view.activities;

import android.animation.ObjectAnimator;
import android.app.Dialog;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.foodappuser.Entity.CartDetails;
import com.app.foodappuser.Model.CartCounts.CartItemCounts;
import com.app.foodappuser.Model.Custom.SelectedCustomizationsModel;
import com.app.foodappuser.Model.MenuItems.MenuItemsModel;
import com.app.foodappuser.Model.Response.ListDishesResponse.DishItems;
import com.app.foodappuser.Model.Response.ListDishesResponse.ListDishesResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.ViewAnimationUtils;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.InterFaces.CustomizationAdapterInterface;
import com.app.foodappuser.Utilities.InterFaces.DishesAdapterInterface;
import com.app.foodappuser.Utilities.InterFaces.OnMenuItemSelected;
import com.app.foodappuser.Utilities.InterFaces.onDatabaseAdded;
import com.app.foodappuser.Utilities.InterFaces.status;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.Utilities.UiUtils.DialogViews;
import com.app.foodappuser.Utilities.UiUtils.SwitchButton.SwitchButton;
import com.app.foodappuser.view.Adapters.CustomizableElementAdapter;
import com.app.foodappuser.view.Adapters.DishCategoryAdapter;
import com.app.foodappuser.view.Adapters.MenuListAdapter;
import com.app.foodappuser.view.Adapters.SelectedCustomizationAdapter;
import com.app.foodappuser.ViewModel.DishItemsViewModel;
import com.app.foodappuser.ViewModel.RestaurantDetailsViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RestaurantDetailsActivity extends BaseActivity {

    private static final String TAG = RestaurantDetailsActivity.class.getSimpleName();
    @BindView(R2.id.categoryRecyclerView)
    RecyclerView categoryRecyclerView;

    @BindView(R2.id.backButton)
    ImageView backButton;

    @BindView(R2.id.nestedScrollView)
    NestedScrollView nestedScrollView;

    @BindView(R2.id.swipeView)
    SwipeRefreshLayout swipeView;

    @BindView(R2.id.contentLayout)
    View contentLayout;

    @BindView(R2.id.loadingLayout)
    View loadingLayout;

    @BindView(R2.id.noInternetLayout)
    View noInternetLayout;

    @BindView(R2.id.restaurantName)
    TextView restaurantName;

    @BindView(R2.id.restaurantCuisineType)
    TextView restaurantCuisineType;

    @BindView(R2.id.restaurantDeliveryTime)
    TextView restaurantDeliveryTime;

    @BindView(R2.id.restaurantPrice)
    TextView restaurantPrice;

    @BindView(R2.id.restaurantRating)
    TextView restaurantRating;

    @BindView(R2.id.menuButton)
    RelativeLayout menuButton;

    @BindView(R2.id.transparentLayout)
    RelativeLayout transparentLayout;

    @BindView(R2.id.menu_layout)
    LinearLayout menu_layout;

    @BindView(R2.id.cartLayout)
    RelativeLayout cartLayout;

    @BindView(R2.id.totalItems)
    TextView totalItems;

    @BindView(R2.id.totalAmount)
    TextView totalAmount;

    @BindView(R2.id.viewCart)
    TextView viewCart;

    @BindView(R2.id.restaurantNameToolbar)
    TextView restaurantNameToolbar;

    @BindView(R2.id.menuRecycler)
    RecyclerView menuRecycler;

    String outletId = "";
    Intent intent;

    DishCategoryAdapter dishCategoryAdapter;
    ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;

    RestaurantDetailsViewModel restaurantDetailsViewModel;
    DishItemsViewModel dishItemsViewModel;

    Dialog customizableDialog;
    RecyclerView customizableElementsRecycler;
    RecyclerView selectedCustomizationsRecycler;
    TextView dishName, selectedItems, showMore, itemTotalText, dishCost;
    CardView addCustomizationButton;
    ImageView meatContaining;

    Dialog repeatCustomizableDialog;
    RelativeLayout chooseCustomization, repeatCustomization;

    Dialog clearCartDialog;
    TextView noButton, yesButton;

    Dialog decreaseCustomizableDialog;
    TextView okButton;

    Boolean expandable = true;
    boolean expand = false;

    status status;

    AppSettings appSettings;

    MenuListAdapter menuListAdapter;

    ArrayList<SelectedCustomizationsModel> selectedCustomizationsModels = new ArrayList<>();
    final ArrayList<CartItemCounts> cartItemCounts = new ArrayList<>();

    @BindView(R2.id.vegOnlySwitch)
    SwitchButton vegOnlySwitch;
    @BindView(R2.id.estimatedTimeLayout)
    LinearLayout estimatedTimeLayout;
    @BindView(R2.id.retryButton)
    RelativeLayout retryButton;
    @BindView(R2.id.coupon_description)
    TextView coupon_description;
    @BindView(R2.id.couponLayout)
    LinearLayout couponLayout;

    private DishItems dishItems;

    LinearLayoutManager categoryRecyclerViewLayoutManger;

    Bundle customizationBundle;
    private Bundle repeatCustomizationBundle;
    private String outletName, cuisineType, outletTime, outletReview, outletCostForTwo;

    ListDishesResponseModel mListDishesResponseModel;

    boolean showCartAnimation = true;
    private String couponEnabled;
    private String couponLongDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_details);
        ButterKnife.bind(this);
        restaurantDetailsViewModel = ViewModelProviders.of(this).get(RestaurantDetailsViewModel.class);
        dishItemsViewModel = ViewModelProviders.of(this).get(DishItemsViewModel.class);
        appSettings = new AppSettings(this);
        intent = getIntent();
        customizableDialog = DialogViews.showCustomizableDialog(this);
        repeatCustomizableDialog = DialogViews.showRepeatCustomizableDialog(this);
        decreaseCustomizableDialog = DialogViews.showDecreaseCustomizableDialog(this);
        clearCartDialog = DialogViews.showClearCartDialog(this);
        vegOnlySwitch.setChecked(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ViewAnimationUtils.backToPreviousActivityTransaction(this);
    }


    private void setCartLayoutValues() {
        CartDetails cartDetails = restaurantDetailsViewModel.getCartDetails();
        if (cartDetails != null) {
            Utils.log(TAG, "Total Items : " + cartDetails.totalItems);
            if (cartDetails.totalItems == 0.0) {
//                cartLayout.setVisibility(View.GONE);
                ViewAnimationUtils.slideDown(cartLayout, this);
//                totalItems.setText(cartDetails.totalItems);
//                totalAmount.setText(cartDetails.totalAmount);
                showCartAnimation = true;
                restaurantDetailsViewModel.deleteAllTableValues();
            } else {
                if (showCartAnimation) {
                    ViewAnimationUtils.slideUp(cartLayout, this);
                } else {
                    cartLayout.setVisibility(View.VISIBLE);
                }
                int totalIm = (int) (cartDetails.totalItems);
                totalItems.setText(String.valueOf(totalIm));
                float amount = (float) cartDetails.totalAmount;
                totalAmount.setText(appSettings.getCurrency() + " " + amount);
                showCartAnimation = false;
            }
        } else {
            cartLayout.setVisibility(View.GONE);
//            ViewAnimationUtils.slideDownImmediate(cartLayout);
        }


    }

    private void initListeners() {

        outletId = intent.getStringExtra(ConstantKeys.INTENTKEYS.OUTLET_ID);
        outletName = intent.getStringExtra(ConstantKeys.INTENTKEYS.OUTLET_NAME);
        cuisineType = intent.getStringExtra(ConstantKeys.INTENTKEYS.CUISINE_NAME);
        outletTime = intent.getStringExtra(ConstantKeys.INTENTKEYS.OUTLET_MIN);
        outletReview = intent.getStringExtra(ConstantKeys.INTENTKEYS.OUTLET_REVIEW);
        outletCostForTwo = intent.getStringExtra(ConstantKeys.INTENTKEYS.OUTLET_COST_FOR_TWO);
        couponEnabled = intent.getStringExtra(ConstantKeys.INTENTKEYS.COUPON_ENABLED);
        couponLongDescription = intent.getStringExtra(ConstantKeys.INTENTKEYS.COUPON_LONG_DESCRIPTION);

        restaurantName.setText(outletName);
        if (cuisineType.trim().length() != 0) {
            restaurantCuisineType.setVisibility(View.VISIBLE);
            restaurantCuisineType.setText(cuisineType);
        } else {
            restaurantCuisineType.setVisibility(View.GONE);
        }
        restaurantRating.setText(outletReview);
        restaurantDeliveryTime.setText(outletTime);
        restaurantPrice.setText(outletCostForTwo);
        restaurantNameToolbar.setText(outletName);
        coupon_description.setText(couponLongDescription);

        if (couponEnabled.equalsIgnoreCase(ConstantKeys.RESTAURANTSOUTLET.COUPON_FOR_ENABLE_TRUE)) {
            couponLayout.setVisibility(View.VISIBLE);
        } else {
            couponLayout.setVisibility(View.GONE);
        }

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeOrOpenMenu();
            }
        });

        transparentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeOrOpenMenu();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestaurantDetailsActivity.super.onBackPressed();
            }
        });

        cartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RestaurantDetailsActivity.this, ViewCartActivity.class);
                startActivity(intent);
            }
        });

        vegOnlySwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (mListDishesResponseModel != null) {
                    dishCategoryAdapter.setIsVeg(isChecked);
                    dishCategoryAdapter.notifyDataSetChanged();
                }
            }
        });

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                try {
                    getDishesData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (restaurantDetailsViewModel.isCheckOutletAvailableInCart()) {
                    setCartLayoutValues();
                } else {
                    cartLayout.setVisibility(View.GONE);
                }
            }
        });

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getDishesData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (restaurantDetailsViewModel.isCheckOutletAvailableInCart()) {
                    cartLayout.setVisibility(View.VISIBLE);
                    setCartLayoutValues();
                } else {
                    cartLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    private void closeOrOpenMenu() {
        if (menu_layout.getVisibility() == View.VISIBLE) {
            ViewAnimationUtils.closeMenu(getBaseContext(), transparentLayout, menu_layout, menuButton);
        } else {
            ViewAnimationUtils.openMenu(getBaseContext(), transparentLayout, menu_layout, menuButton);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        swipeView.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener =
                new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (nestedScrollView.getScrollY() == 0)
                            swipeView.setEnabled(true);
                        else
                            swipeView.setEnabled(false);
                    }
                });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            nestedScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    Utils.log(TAG, "Scroll = " + scrollY);
                    if (scrollY > oldScrollY) {

                        if (scrollY > 55) {
                            //start alpha
                            restaurantNameToolbar.setVisibility(View.VISIBLE);

                            restaurantNameToolbar.setAlpha(1f);
                            return;
                        } else if (scrollY > 40) {
                            restaurantNameToolbar.setAlpha(0.6f);
                            return;

                        } else if (scrollY > 35) {
                            //show
                            restaurantNameToolbar.setAlpha(0.3f);
                            restaurantNameToolbar.setVisibility(View.VISIBLE);

                            return;

                        }
                    }

                    if (scrollY < oldScrollY) {

                        if (scrollY < 35) {
                            restaurantNameToolbar.setAlpha(0.3f);
                            restaurantNameToolbar.setVisibility(View.GONE);

                        } else if (scrollY < 40) {
                            restaurantNameToolbar.setAlpha(0.6f);

                        } else if (scrollY < 55) {
                            restaurantNameToolbar.setAlpha(1f);
                        }

                    }
                }
            });
        }
    }

    @Override
    public void onStop() {
        swipeView.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListener);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showCartAnimation = true;
        initListeners();

        try {
            getDishesData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (restaurantDetailsViewModel.isCheckOutletAvailableInCart()) {

            setCartLayoutValues();
        } else {
            cartLayout.setVisibility(View.GONE);
//            ViewAnimationUtils.slideDownImmediate(cartLayout);
        }
    }

    private void startLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);
        noInternetLayout.setVisibility(View.GONE);
    }

    private void stopLoading() {
        loadingLayout.setVisibility(View.GONE);
        noInternetLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }

    private void setNoInternetConnection() {
        loadingLayout.setVisibility(View.GONE);
        noInternetLayout.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);
    }

    private void getDishesData() throws JSONException {

        startLoading();

        try {
            swipeView.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.OUTLET_ID, outletId);
        InputForAPI inputForAPI = new InputForAPI(RestaurantDetailsActivity.this);
        inputForAPI.setUrl(UrlHelper.DISHES);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));
        inputForAPI.setFile(null);
        inputForAPI.setJsonObject(jsonObject);

        restaurantDetailsViewModel.getDishesInRestaurant(inputForAPI).observe(this, new Observer<ListDishesResponseModel>() {
            @Override
            public void onChanged(@Nullable ListDishesResponseModel listDishesResponseModel) {
                stopLoading();
                if (listDishesResponseModel.getError())
                    if (listDishesResponseModel.getErrorMessage().equalsIgnoreCase(getResources().getString(R.string.no_internet_connection))) {
                        setNoInternetConnection();
                    } else {
                        Utils.showSnackBar(swipeView, listDishesResponseModel.getErrorMessage());
                    }
                else {
                    mListDishesResponseModel = listDishesResponseModel;
                    if (restaurantDetailsViewModel.checkOutletIdRepeated(outletId)) {
                        setValuesInadapter(restaurantDetailsViewModel.changeQuantity(listDishesResponseModel), false);
                    } else {
                        setValuesInadapter(listDishesResponseModel, false);
                    }
                    setMenuAdapter(listDishesResponseModel);
                }
            }
        });
    }

    private void setMenuAdapter(ListDishesResponseModel listDishesResponseModel) {
        LinearLayoutManager layout = new LinearLayoutManager(RestaurantDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        menuRecycler.setLayoutManager(layout);
        final ArrayList<MenuItemsModel> menuItems = restaurantDetailsViewModel.getOnlyMenuItems(listDishesResponseModel);
        menuListAdapter = new MenuListAdapter(RestaurantDetailsActivity.this, menuItems);
        menuRecycler.setAdapter(menuListAdapter);
        menuListAdapter.setOnClickListener(new OnMenuItemSelected() {
            @Override
            public void onItemSelected(String categoryId, int position) {
                Utils.log(TAG, categoryId);
                closeOrOpenMenu();
//                float y = categoryRecyclerView.getY() + categoryRecyclerView.getChildAt(position).getY();
//                nestedScrollView.smoothScrollTo(0, (int) y);

            }
        });
    }


    private void setValuesInadapter(final ListDishesResponseModel listDishesResponseModel, boolean isVegSwitch) {
        categoryRecyclerViewLayoutManger = new LinearLayoutManager(RestaurantDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        categoryRecyclerView.setLayoutManager(categoryRecyclerViewLayoutManger);
        dishCategoryAdapter = new DishCategoryAdapter(RestaurantDetailsActivity.this, listDishesResponseModel.getDishes(), isVegSwitch);
        categoryRecyclerView.setAdapter(dishCategoryAdapter);

        dishCategoryAdapter.setOnClickListner(new DishesAdapterInterface() {
            @Override
            public void onIncreased(DishItems dishItem, onDatabaseAdded onDatabaseAdded) {

                if (restaurantDetailsViewModel.isCheckOutletAvailableInCart()) {

                    if (restaurantDetailsViewModel.checkOutletIdRepeated(outletId)) {

                        Bundle bundle = restaurantDetailsViewModel.increaseCartCount(dishItem, outletId);
                        setCartLayoutValues();
                        onDatabaseAdded.onAdded(bundle);
                        dishCategoryAdapter.onItemChanged(dishItem, bundle.getString(ConstantKeys.QUANTITY));

                    } else {
                        //showClearCart Dialog
                        showClearCartDialog(dishItem, onDatabaseAdded, false);

                    }

                } else {
                    Bundle bundle = restaurantDetailsViewModel.addOutletId(dishItem, outletId);
                    setCartLayoutValues();
                    onDatabaseAdded.onAdded(bundle);
                    dishCategoryAdapter.onItemChanged(dishItem, bundle.getString(ConstantKeys.QUANTITY));

                }
            }

            @Override
            public void onDecreased(DishItems items, boolean isCustomizable, onDatabaseAdded onDatabaseAdded) {

                if (isCustomizable) {
                    showDecreaseCustomizationDialog();
                } else {
                    Bundle bundle = restaurantDetailsViewModel.decreaseCartCount(items, outletId);
                    setCartLayoutValues();
                    onDatabaseAdded.onAdded(bundle);
                    dishCategoryAdapter.onItemChanged(items, bundle.getString(ConstantKeys.QUANTITY));

                }

            }

            @Override
            public void onCustomization(DishItems items, onDatabaseAdded onDatabaseAdded) {

                dishItems = items;

                if (restaurantDetailsViewModel.isCheckOutletAvailableInCart()) {

                    if (restaurantDetailsViewModel.checkOutletIdRepeated(outletId)) {

                        if (restaurantDetailsViewModel.checkCustomizationdish(items)) {
                            showCustomizationDialog(onDatabaseAdded);

                        } else {
                            showRepeatCustomizationDialog(onDatabaseAdded);
                        }

                    } else {
                        //showClearCart Dialog
                        showClearCartDialog(items, onDatabaseAdded, true);
                    }

                } else {
                    if (restaurantDetailsViewModel.checkCustomizationdish(items)) {
                        showCustomizationDialog(onDatabaseAdded);

                    } else {
                        showRepeatCustomizationDialog(onDatabaseAdded);
                    }
                }
            }
        });

    }

    private void showDecreaseCustomizationDialog() {
        decreaseCustomizableDialog.show();
        okButton = decreaseCustomizableDialog.findViewById(R.id.okButton);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decreaseCustomizableDialog.dismiss();
                Intent intent = new Intent(RestaurantDetailsActivity.this, ViewCartActivity.class);
                startActivity(intent);

            }
        });
    }

    private void showClearCartDialog(final DishItems dishItem, final onDatabaseAdded onDatabaseAdded, final boolean isCustomizable) {
        yesButton = clearCartDialog.findViewById(R.id.yesButton);
        noButton = clearCartDialog.findViewById(R.id.noButton);
        clearCartDialog.show();
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                restaurantDetailsViewModel.deleteAllTableValues();
                clearCartDialog.dismiss();

                if (isCustomizable) {

                    dishItems = dishItem;

                    if (restaurantDetailsViewModel.isCheckOutletAvailableInCart()) {

                        if (restaurantDetailsViewModel.checkOutletIdRepeated(outletId)) {

                            if (restaurantDetailsViewModel.checkCustomizationdish(dishItem)) {
                                showCustomizationDialog(onDatabaseAdded);

                            } else {
                                showRepeatCustomizationDialog(onDatabaseAdded);
                            }

                        } else {
                            //showClearCart Dialog
                            showClearCartDialog(dishItem, onDatabaseAdded, true);
                        }

                    } else {
                        if (restaurantDetailsViewModel.checkCustomizationdish(dishItem)) {
                            showCustomizationDialog(onDatabaseAdded);

                        } else {
                            showRepeatCustomizationDialog(onDatabaseAdded);
                        }
                    }
                } else {
                    if (restaurantDetailsViewModel.isCheckOutletAvailableInCart()) {

                        if (restaurantDetailsViewModel.checkOutletIdRepeated(outletId)) {

                            Bundle bundle = restaurantDetailsViewModel.increaseCartCount(dishItem, outletId);
                            setCartLayoutValues();
                            onDatabaseAdded.onAdded(bundle);

                        } else {
                            //showClearCart Dialog
                            showClearCartDialog(dishItem, onDatabaseAdded, false);

                        }

                    } else {
                        Bundle bundle = restaurantDetailsViewModel.addOutletId(dishItem, outletId);
                        setCartLayoutValues();
                        onDatabaseAdded.onAdded(bundle);
                    }
                }
                setCartLayoutValues();
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearCartDialog.dismiss();
            }
        });

    }


    private void showRepeatCustomizationDialog(final onDatabaseAdded onDatabaseAdded) {

        chooseCustomization = repeatCustomizableDialog.findViewById(R.id.chooseCustomization);
        repeatCustomization = repeatCustomizableDialog.findViewById(R.id.repeatCustomization);

        repeatCustomizableDialog.show();

        chooseCustomization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeatCustomizableDialog.dismiss();
                showCustomizationDialog(onDatabaseAdded);
            }
        });

        repeatCustomization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                repeatCustomizableDialog.dismiss();
                repeatCustomizationBundle = restaurantDetailsViewModel.repeatCustomization(dishItems, outletId, true);
                onDatabaseAdded.onAdded(repeatCustomizationBundle);
                dishCategoryAdapter.onItemChanged(dishItems, repeatCustomizationBundle.getString(ConstantKeys.QUANTITY));
                setCartLayoutValues();

            }
        });

    }


    private void showCustomizationDialog(final onDatabaseAdded onDatabaseAdded) {

        dishName = customizableDialog.findViewById(R.id.dishName);
        dishCost = customizableDialog.findViewById(R.id.dishCost);
        itemTotalText = customizableDialog.findViewById(R.id.itemTotalText);
        itemTotalText.setText(dishItems.getDisplayPrice());
        addCustomizationButton = customizableDialog.findViewById(R.id.addCustomizationButton);
        customizableElementsRecycler = customizableDialog.findViewById(R.id.customizableElementsRecycler);
        selectedCustomizationsRecycler = customizableDialog.findViewById(R.id.selectedCustomizationsRecycler);
        meatContaining = customizableDialog.findViewById(R.id.meatContaining);
        selectedItems = customizableDialog.findViewById(R.id.selectedItems);
        showMore = customizableDialog.findViewById(R.id.showMore);

        dishName.setText(dishItems.getDishName());
        dishCost.setText(dishItems.getDisplayPrice());

        customizableElementsRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        final CustomizableElementAdapter customizableElementAdapter = new CustomizableElementAdapter(this, dishItems.getCustomizableElements(), dishItems.getPrice());
        customizableElementsRecycler.setAdapter(customizableElementAdapter);
        customizableDialog.show();

        customizableElementAdapter.setOnClickListener(new CustomizationAdapterInterface() {
            @Override
            public void addCustomizationDishName(String name, int categoryId, int elementId, String price, Boolean isSingleChoice) {
                Log.e(TAG, "addCustomizationDishNames: " + price);
                selectedCustomizationsModels = restaurantDetailsViewModel.addCustomizationElement(isSingleChoice, categoryId, elementId, name, price, selectedCustomizationsModels, dishItems);
                setTextViewValues(selectedCustomizationsModels, dishItems.getPrice());

            }

            @Override
            public void removeCustomizationDishName(String name, int categoryId, int elementId, String price, Boolean isSingleChoice) {
                Log.e(TAG, "addCustomizationDishName: " + price);
                for (int i = 0; i < selectedCustomizationsModels.size(); i++) {
                    if (selectedCustomizationsModels.get(i).getCategoryId() == categoryId && selectedCustomizationsModels.get(i).getElementId() == elementId) {
                        selectedCustomizationsModels.remove(i);
                    }
                }
                setTextViewValues(selectedCustomizationsModels, dishItems.getPrice());
            }
        });

        addCustomizationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (restaurantDetailsViewModel.isCheckOutletAvailableInCart()) {

                    if (restaurantDetailsViewModel.checkOutletIdRepeated(outletId)) {
                        Bundle bundle = restaurantDetailsViewModel.addNewCustomization(dishItems, selectedCustomizationsModels, outletId);
                        onDatabaseAdded.onAdded(bundle);
                        dishCategoryAdapter.onItemChanged(dishItems, bundle.getString(ConstantKeys.QUANTITY));

                    } else {
                        //showClearCart Dialog

                    }

                } else {
                    Bundle bundle = restaurantDetailsViewModel.addOutletIdFromCustomization(dishItems, outletId, selectedCustomizationsModels);
                    onDatabaseAdded.onAdded(bundle);
                    dishCategoryAdapter.onItemChanged(dishItems, bundle.getString(ConstantKeys.QUANTITY));

                }

                customizableDialog.dismiss();
                selectedCustomizationsModels.clear();
                setCartLayoutValues();
            }
        });

        customizableDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                customizableDialog.dismiss();
                selectedCustomizationsModels.clear();
            }
        });

        customizableDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                customizableDialog.dismiss();
                selectedCustomizationsModels.clear();
            }
        });

        customizableDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    customizableDialog.dismiss();
                    selectedCustomizationsModels.clear();
                }
                return true;
            }
        });

    }


    private void checkTextviewsize() {

        selectedItems.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (expandable) {
                    expandable = false;
                    if (selectedItems.getLineCount() > 2) {
                        showMore.setVisibility(View.VISIBLE);
                        ObjectAnimator animation = ObjectAnimator.ofInt(selectedItems, "maxLines", 2);
                        animation.setDuration(0).start();
                    }
                }
            }
        });

        showMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!expand) {
                    expand = true;
                    ObjectAnimator animation = ObjectAnimator.ofInt(selectedItems, "maxLines", 40);
                    animation.setDuration(100).start();
                } else {
                    expand = false;
                    ObjectAnimator animation = ObjectAnimator.ofInt(selectedItems, "maxLines", 2);
                    animation.setDuration(100).start();
                }

            }
        });

    }

    public void setTextViewValues(ArrayList<SelectedCustomizationsModel> vals, double dishPrice) {

        String output = "";

        int total = 0;

        for (int i = 0; i < vals.size(); i++) {
            total = total + Integer.parseInt(vals.get(i).getPrice());
            if (i == vals.size() - 1) {
                output += vals.get(i).getCustomizableName();
            } else {
                output += vals.get(i).getCustomizableName() + ", ";
            }
        }

        selectedItems.setText(output);
        double customizationTotalPrice = total + (dishPrice);
        itemTotalText.setText(appSettings.getCurrency() + " " + String.valueOf(customizationTotalPrice));

    }

    private void setCustomizabledishAdapter(ArrayList<SelectedCustomizationsModel> selectedCustomizationsModels) {
        selectedCustomizationsRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        SelectedCustomizationAdapter adapters = new SelectedCustomizationAdapter(this, selectedCustomizationsModels);
        selectedCustomizationsRecycler.setAdapter(adapters);
    }

    public void setStatus(status status) {
        this.status = status;
    }

}
