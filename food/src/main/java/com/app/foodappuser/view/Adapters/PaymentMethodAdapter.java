package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.PaymentMethodResponse.PaymentGateway;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.ImageLoader;
import com.app.foodappuser.Utilities.InterFaces.PaymentAdapterInterface;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentMethodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    List<PaymentGateway> paymentGateways;
    ImageLoader imageLoader;
    PaymentAdapterInterface paymentAdapterInterface;
    private String cardname = "";
    AppSettings appSettings;

    public PaymentMethodAdapter(Context context, List<PaymentGateway> paymentGateway) {
        this.context = context;
        this.paymentGateways = paymentGateway;
        imageLoader = new ImageLoader(context);
        appSettings = new AppSettings(context);
        cardname = appSettings.getSelectedCardName();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.payment_method_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        final ViewHolder holder = (ViewHolder) viewHolder;
        final PaymentGateway paymentGateway = paymentGateways.get(i);

        if (i == 0) {
            holder.cardName.setVisibility(View.GONE);
        } else {
            if (appSettings.getSelectedCardName().length() == 0) {
                holder.cardName.setVisibility(View.GONE);
            } else {
                holder.cardName.setVisibility(View.VISIBLE);
                holder.cardName.setText(appSettings.getSelectedCardName());
            }
        }

        holder.paymentName.setText(paymentGateway.getName());
        imageLoader.load(paymentGateway.getImage(), holder.paymentImage);

        if (paymentGateway.getIsSelected() != null) {
            if (paymentGateway.getIsSelected() == 1) {
                holder.checkPayment.setChecked(true);
            } else {
                holder.checkPayment.setChecked(false);
            }
        } else {
            holder.checkPayment.setChecked(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(i == 0){
                    for(int j = 0 ; j < paymentGateways.size() ; j++){
                        if( i == j){
                            paymentGateways.get(j).setIsSelected(1);
                        }else {
                            paymentGateways.get(j).setIsSelected(0);
                        }
                    }
                    notifyDataSetChanged();
                    paymentAdapterInterface.onPaymentClicked(paymentGateway.getId(), i);
                }else {
                    paymentAdapterInterface.onCardSelection();
                }

            }
        });

        holder.checkPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int j = 0 ; j < paymentGateways.size() ; j++){
                    if( i == j){
                        paymentGateways.get(j).setIsSelected(1);
                    }else {
                        paymentGateways.get(j).setIsSelected(0);
                    }
                }

               if(i != 0){
                   if(appSettings.getSelectedCardName().length() == 0){
                       paymentAdapterInterface.onCardSelection();
                   }
               }
                notifyDataSetChanged();
                paymentAdapterInterface.onPaymentClicked(paymentGateway.getId(), i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (paymentGateways != null) ? paymentGateways.size() : 0;

    }

    public void setcardname() {
        notifyDataSetChanged();
    }

    public void setOnClickListener(PaymentAdapterInterface paymentAdapterInterface) {
        this.paymentAdapterInterface = paymentAdapterInterface;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.paymentName)
        TextView paymentName;
        @BindView(R2.id.cardName)
        TextView cardName;
        @BindView(R2.id.checkPayment)
        RadioButton checkPayment;
        @BindView(R2.id.paymentImage)
        ImageView paymentImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}