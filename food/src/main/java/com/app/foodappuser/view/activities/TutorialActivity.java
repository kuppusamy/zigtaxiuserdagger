package com.app.foodappuser.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.app.foodappuser.R;

public class TutorialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
    }
}
