package com.app.foodappuser.view.activities;

import android.app.Dialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.OrderConfirmResponseModel;
import com.app.foodappuser.Model.Response.PaymentMethodResponse.PaymentGateway;
import com.app.foodappuser.Model.Response.PaymentMethodResponse.PaymentMethodResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.InterFaces.PaymentAdapterInterface;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.Utilities.UiUtils.DialogViews;
import com.app.foodappuser.view.Adapters.PaymentMethodAdapter;
import com.app.foodappuser.ViewModel.PaymentMethodViewModel;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.model.Source;
import com.stripe.android.model.SourceCardData;
import com.stripe.android.view.PaymentMethodsActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class PaymentMethodActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = PaymentMethodActivity.class.getSimpleName();
    @BindView(R2.id.backButton)
    ImageView backButton;
    @BindView(R2.id.totalItem)
    TextView totalItem;
    @BindView(R2.id.toPayAmount)
    TextView toPayAmount;
    @BindView(R2.id.paymentMethodsRecycler)
    RecyclerView paymentMethodsRecycler;
    @BindView(R2.id.proceedPayButton)
    RelativeLayout proceedPayButton;

    @BindView(R2.id.loadingLayout)
    View loadingLayout;

    @BindView(R2.id.contentLayout)
    View contentLayout;

    PaymentMethodViewModel paymentMethodViewModel;

    PaymentMethodAdapter paymentMethodAdapter;

    Intent intent;

    AppSettings appSettings;

    Dialog loader;

    int clickedposition = 0;
    int paymentId = 0;
    String cardID = "";
    String cardname = "";
    private final int REQUEST_CODE_SELECT_SOURCE = 55;
    List<PaymentGateway> paymentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);
        paymentMethodViewModel = ViewModelProviders.of(this).get(PaymentMethodViewModel.class);
        intent = getIntent();
        appSettings = new AppSettings(this);
        loader = DialogViews.showOrderLoading(this);
        PaymentConfiguration.init(getResources().getString(R.string.STRIPE_KEY));
        initObserver();
        paymentMethodViewModel.initCustomerSession();
    }

    private void initObserver() {
        paymentMethodViewModel.setCardSource().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
//                Toast.makeText(PaymentMethodActivity.this, ""+s, Toast.LENGTH_SHORT).show();
                setStripeCard(s);
            }
        });
    }

    private String buildCardString(@NonNull SourceCardData data) {
        return Utils.getCombinedStrings(data.getBrand(), " ", getString(R.string.ending_in), " ", data.getLast4());
    }

    private void setStripeCard(String selectedSource) {
        Source source = Source.fromString(selectedSource);
        if (source != null && Source.CARD.equals(source.getType())) {
            SourceCardData sourceCardData = (SourceCardData) source.getSourceTypeModel();
            if (sourceCardData != null) {
                cardID = source.getId();
                cardname = buildCardString(sourceCardData);
                appSettings.setSelectedCardName(cardname);
                paymentMethodAdapter.setcardname();
            } else {
                cardID = "";
                cardname = "";
            }
        }
    }

    private void setListeners() {
        proceedPayButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    private void getPayments() {

        startLoading();

        InputForAPI inputForAPI = new InputForAPI(this);
        inputForAPI.setUrl(UrlHelper.LIST_PAYMENT_METHOD);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));

        paymentMethodViewModel.getPaymentMethod(inputForAPI).observe(this, new Observer<PaymentMethodResponseModel>() {
            @Override
            public void onChanged(@Nullable PaymentMethodResponseModel paymentMethodResponseModel) {
                stopLoading();
                if (paymentMethodResponseModel.getError()) {
                    Utils.showSnackBar(findViewById(android.R.id.content), paymentMethodResponseModel.getErrorMessage());
                } else {
                    paymentList = paymentMethodResponseModel.getPaymentGateway();
                    setPaymentAdapter(paymentMethodResponseModel.getPaymentGateway());
                }
            }
        });
    }

    private void stopLoading() {
        loadingLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }

    private void startLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);
    }

    private void setPaymentAdapter(List<PaymentGateway> paymentGateway) {

        LinearLayoutManager layout = new LinearLayoutManager(PaymentMethodActivity.this, LinearLayoutManager.VERTICAL, false);
        paymentMethodsRecycler.setLayoutManager(layout);
        paymentMethodAdapter = new PaymentMethodAdapter(PaymentMethodActivity.this, paymentGateway);
        paymentMethodsRecycler.setAdapter(paymentMethodAdapter);
        paymentMethodAdapter.setOnClickListener(new PaymentAdapterInterface() {
            @Override
            public void onPaymentClicked(int id, int position) {
                clickedposition = position;
                paymentId = id;
                proceedPayButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCardSelection() {
                launchWithCustomer();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getPayments();
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        proceedPayButton.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SELECT_SOURCE && resultCode == RESULT_OK) {
            String selectedSource = data.getStringExtra(PaymentMethodsActivity.EXTRA_SELECTED_PAYMENT);
            setStripeCard(selectedSource);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == proceedPayButton) {
            if (clickedposition == 0) {
                try {
                    proceedToPay();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (cardID.equals(""))
                    launchWithCustomer();
                else {
                    try {
                        proceedToPay();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        if (v == backButton) {
            PaymentMethodActivity.super.onBackPressed();
        }
    }

    private void launchWithCustomer() {
        Intent payIntent = PaymentMethodsActivity.newIntent(this);
        startActivityForResult(payIntent, REQUEST_CODE_SELECT_SOURCE);
    }

    private void proceedToPay() throws JSONException {

        showLoaderDialog();

        JSONObject jsonObject = new JSONObject();
        Bundle bundle = intent.getBundleExtra("bundle");
        jsonObject.put(ConstantKeys.OUTLET_ID, bundle.getString("outletId"));
        jsonObject.put(ConstantKeys.DELIVER_ADDRESS_ID, bundle.getString("addressId"));
        jsonObject.put(ConstantKeys.NET_AMOUNT, bundle.getString("netAmount"));
        jsonObject.put(ConstantKeys.UDID, bundle.getString("outletId"));
        jsonObject.put(ConstantKeys.CART_ID, bundle.getString("cartId"));
        jsonObject.put(ConstantKeys.COUPON_NAME, bundle.getString(ConstantKeys.COUPON_NAME));
        jsonObject.put(ConstantKeys.DISCOUNT, bundle.getString(ConstantKeys.DISCOUNT));
        jsonObject.put(ConstantKeys.SUGGESTIONS, bundle.getString(ConstantKeys.SUGGESTIONS));
        jsonObject.put(ConstantKeys.TOKEN, cardID);
        jsonObject.put(ConstantKeys.PAYMENT_TYPE, paymentId);

        InputForAPI inputForAPI = new InputForAPI(this);
        inputForAPI.setUrl(UrlHelper.ORDER_CONFIRM);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));
        inputForAPI.setJsonObject(jsonObject);

        paymentMethodViewModel.orderConfirmPayment(inputForAPI).observe(this, new Observer<OrderConfirmResponseModel>() {
            @Override
            public void onChanged(@Nullable OrderConfirmResponseModel generalResponseModel) {
                if (generalResponseModel.getError()) {
                    loader.dismiss();
                    Utils.showSnackBar(findViewById(android.R.id.content), generalResponseModel.getErrorMessage());

                } else {
                    paymentMethodViewModel.deleteAllTableValues();
                    Intent intent = new Intent(PaymentMethodActivity.this, LiveTrackingActivity.class);
                    intent.putExtra(ConstantKeys.INTENTKEYS.ORDER_ID, String.valueOf(generalResponseModel.getOrderId()));
                    startActivity(intent);
                    loader.dismiss();
                    finish();

                }
            }
        });

    }

    private void showLoaderDialog() {
        loader.show();
        LinearLayout loderLayout = loader.findViewById(R.id.loadingLayout);
        LinearLayout successLayout = loader.findViewById(R.id.successLayout);
        loderLayout.setVisibility(View.VISIBLE);
        successLayout.setVisibility(View.GONE);
    }
}
