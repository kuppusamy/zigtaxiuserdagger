package com.app.foodappuser.view.activities;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;

import com.app.foodappuser.Model.Response.DeleteAddressResponseModel;
import com.app.foodappuser.Model.Response.GetSavedAddressResponse.Address;
import com.app.foodappuser.Model.Response.GetSavedAddressResponse.GetSavedAddressResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.InterFaces.ManageAdapterInterface;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.view.Adapters.ManageAddressAdapter;
import com.app.foodappuser.ViewModel.ManageAddressViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;

public class ManageAddressActivity extends BaseActivity {

    @BindView(R2.id.backButton)
    ImageView backButton;
    @BindView(R2.id.savedAddressRecycler)
    RecyclerView savedAddressRecycler;
    @BindView(R2.id.addNewAddressButton)
    CardView addNewAddressButton;
    @BindView(R2.id.connectionLostLayout)
    View connectionLostLayout;
    @BindView(R2.id.contentLayout)
    View contentLayout;
    @BindView(R2.id.loadingLayout)
    View loadingLayout;

    ManageAddressViewModel manageAddressViewModel;
    ManageAddressAdapter manageAddressAdapter;
    AppSettings appSettings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_address);
        manageAddressViewModel = ViewModelProviders.of(this).get(ManageAddressViewModel.class);
        appSettings = new AppSettings(this);
    }

    private void getAddress() {
        startLoading();
        InputForAPI inputForAPI = new InputForAPI(this);
        inputForAPI.setUrl(UrlHelper.GET_ADDRESS);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));

        manageAddressViewModel.getSavedAddressResponseModelLiveData(inputForAPI).observe(this, new Observer<GetSavedAddressResponseModel>() {
            @Override
            public void onChanged(@Nullable GetSavedAddressResponseModel getSavedAddressResponseModel) {
                stopLoading();
                if (getSavedAddressResponseModel.getError()) {
                    setAdapters(getSavedAddressResponseModel);
//                    Utils.showSnackBar(findViewById(android.R.id.content), getSavedAddressResponseModel.getErrorMessage());

                } else {

                    setAdapters(getSavedAddressResponseModel);

                }
            }
        });

    }

    private void stopLoading() {
        loadingLayout.setVisibility(View.GONE);
        connectionLostLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }

    private void startLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
        connectionLostLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.GONE);
    }

    private void setAdapters(GetSavedAddressResponseModel getSavedAddressResponseModel) {

        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        savedAddressRecycler.setLayoutManager(layout);
        manageAddressAdapter = new ManageAddressAdapter(this, getSavedAddressResponseModel.getAddress());
        savedAddressRecycler.setAdapter(manageAddressAdapter);
        manageAddressAdapter.setOnClickListner(new ManageAdapterInterface() {
            @Override
            public void removeAddress(Address list) {
                try {
                    if (list.getCurrentAddress() == 1) {
                        appSettings.setIsAppKilled(ConstantKeys.TRUE_STRING);
                    }
                    deleteAddress(list.getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void editAddress(Address list) {
                moveToEditAddress(list);
            }
        });
    }

    private void moveToEditAddress(Address list) {
        Intent intent = new Intent(this, SaveLocationActivity.class);
        intent.putExtra(ConstantKeys.INTENTKEYS.FROM_ACTIVITY, ManageAddressActivity.class.getSimpleName());
        intent.putExtra(ConstantKeys.INTENTKEYS.REASON_INTENT, ConstantKeys.EDIT_ADDRESS);
        intent.putExtra(ConstantKeys.INTENTKEYS.LATITUDE, list.getLatitude());
        intent.putExtra(ConstantKeys.INTENTKEYS.LONGITUDE, list.getLongitude());
        intent.putExtra(ConstantKeys.INTENTKEYS.HOUSE_FLAT_NO, list.getHouseFlatNo());
        intent.putExtra(ConstantKeys.INTENTKEYS.LANDMARK, list.getLandMark());
        intent.putExtra(ConstantKeys.INTENTKEYS.ADDRESS_TYPE, list.getType());
        intent.putExtra(ConstantKeys.INTENTKEYS.ADDRESS_LOCATION, list.getLocation());
        intent.putExtra(ConstantKeys.INTENTKEYS.ADDRESS_ID, String.valueOf(list.getId()));
        startActivityForResult(intent, 1);
    }

    private void deleteAddress(int id) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.ADDRESS_ID, id);
        InputForAPI inputForAPI = new InputForAPI(this);
        inputForAPI.setUrl(UrlHelper.DESTROY_ADDRESS);
        inputForAPI.setHeaders(Utils.getAuthorizationHeader(this));
        inputForAPI.setJsonObject(jsonObject);
        manageAddressViewModel.deleteAddressResponseModelLiveData(inputForAPI).observe(this, new Observer<DeleteAddressResponseModel>() {
            @Override
            public void onChanged(@Nullable DeleteAddressResponseModel deleteAddressResponseModel) {
                if (deleteAddressResponseModel.getError()) {
                    appSettings.setIsAppKilled(ConstantKeys.TRUE_STRING);
//                    Utils.showSnackBar(findViewById(android.R.id.content), deleteAddressResponseModel.getErrorMessage());
                } else {
                    getAddress();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        initListeners();
        getAddress();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            getAddress();
        }
    }

    private void initListeners() {

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageAddressActivity.super.onBackPressed();
            }
        });

        addNewAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishIntent(appSettings.getLastKnownLatitude(), appSettings.getLastKnownLongitude());
            }
        });

    }

    private void finishIntent(String selectedLat, String selectedLong) {

        Intent intent = new Intent(this, SaveLocationActivity.class);
        intent.putExtra(ConstantKeys.FROM_ACTIVITY, ManageAddressActivity.class.getSimpleName());
        intent.putExtra(ConstantKeys.INTENTKEYS.REASON_INTENT, ManageAddressActivity.class.getSimpleName());
        intent.putExtra(ConstantKeys.INTENTKEYS.LATITUDE, selectedLat);
        intent.putExtra(ConstantKeys.INTENTKEYS.LONGITUDE, selectedLong);
        startActivityForResult(intent, 1);
    }

}
