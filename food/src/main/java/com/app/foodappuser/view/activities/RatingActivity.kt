package com.app.foodappuser.view.activities

import android.app.Activity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.app.foodappuser.Model.Response.ListRestaurantResponse.Ratings
import com.app.foodappuser.R
import com.app.foodappuser.Utilities.ApiCall.InputForAPI
import com.app.foodappuser.Utilities.Constants.ConstantKeys
import com.app.foodappuser.ViewModel.RatingViewModel
import kotlinx.android.synthetic.main.activity_rating.*
import org.json.JSONObject
import android.widget.RatingBar.OnRatingBarChangeListener
import androidx.lifecycle.Observer
import com.app.foodappuser.Utilities.BaseUtils.Utils
import com.app.foodappuser.Utilities.Constants.UrlHelper


class RatingActivity : BaseActivity() {

    private lateinit var ratingViewModel: RatingViewModel
    private var rating: Ratings? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rating)
        ratingViewModel = ViewModelProviders.of(this).get(RatingViewModel::class.java)

        rating= intent.getSerializableExtra(ConstantKeys.RATING) as Ratings?
        from_outlet.setText(resources.getString(R.string.from)+" "+rating?.outletName)
        from_delivery.setText(resources.getString(R.string.by)+" "+rating?.deliverystaffName)
        setListeners()
        setRatingListeners()

    }

    private fun setRatingListeners() {
        restaurant_rating.setOnRatingBarChangeListener(OnRatingBarChangeListener { ratingBar, rating, fromUser ->
            if (rating < 1.0f)
                ratingBar.rating = 1.0f
        })
        delivery_boy_rating.setOnRatingBarChangeListener(OnRatingBarChangeListener { ratingBar, rating, fromUser ->
            if (rating < 1.0f)
                ratingBar.rating = 1.0f
        })
    }

    private fun setListeners() {
        backButton.setOnClickListener {
            onBackPressed()
        }

        submitButton.setOnClickListener {
            val inputForAPI=InputForAPI(this)
            val jsonObject=JSONObject()
            jsonObject.put(ConstantKeys.ORDER_ID,rating?.orderId)
            jsonObject.put(ConstantKeys.STAFF_ID,rating?.deliveryStaffId)
            jsonObject.put(ConstantKeys.OUTLET_ID,rating?.outletId)
            jsonObject.put(ConstantKeys.ORDER_RATING,restaurant_rating.rating)
            jsonObject.put(ConstantKeys.DELIVERY_RATING,delivery_boy_rating.rating)
            jsonObject.put(ConstantKeys.FEEDBACK,suggestions_feedback.text.toString().trim())
            inputForAPI.jsonObject=jsonObject
            inputForAPI.url=UrlHelper.RATING
            inputForAPI.headers=Utils.getAuthorizationHeader(this)

            ratingViewModel.GeneralResponseModel(inputForAPI).observe(this, Observer { response->
                if(response.error){
                    Utils.showToast(response.errorMessage,this)
                }else{
                    setResult(Activity.RESULT_OK)
                    finish()
                    Utils.showToast(resources.getString(R.string.thanks_for_rating),this)
                }
            })
        }
    }
}
