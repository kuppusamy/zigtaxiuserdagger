package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.PastOrderResponse.Dish;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PastOrderDishesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    List<Dish> dishes;

    public PastOrderDishesAdapter(Context context, List<Dish> dishes) {
        this.context = context;
        this.dishes = dishes;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.past_order_dishes_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new DishesViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        final DishesViewHolder holder = (DishesViewHolder) viewHolder;
        Dish dish = dishes.get(i);

        if (dish.getIsVeg() == 1) {
            holder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.veg_icon));
        } else {
            holder.isVegView.setImageDrawable(context.getResources().getDrawable(R.drawable.non_veg_icon));
        }

        holder.dishName.setText(dish.getDisplayDish());
        holder.displayPrice.setText(dish.getDisplayPrice());

    }

    @Override
    public int getItemCount() {
        return (dishes != null) ? dishes.size() : 0;
    }


    class DishesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.isVegView)
        ImageView isVegView;
        @BindView(R2.id.dishName)
        TextView dishName;
        @BindView(R2.id.deleteItem)
        ImageView deleteItem;
        @BindView(R2.id.displayPrice)
        TextView displayPrice;
        @BindView(R2.id.customizableLayout)
        LinearLayout customizableLayout;

        public DishesViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}