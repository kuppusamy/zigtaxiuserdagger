package com.app.foodappuser.view.Adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.foodappuser.Model.Response.ListRestaurantResponse.BannerModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.ImageLoader;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.view.activities.RestaurantDetailsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RestaurantBannerImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<BannerModel> banners;
    ImageLoader imageLoader;

    private Context context;
    private AppSettings appSettings;

    public RestaurantBannerImageAdapter(Context context, List<BannerModel> banners) {
        this.context = context;
        this.banners = banners;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.home_banner_items, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new Restaurant(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Restaurant holder = (Restaurant) viewHolder;
        appSettings = new AppSettings(context);
        final BannerModel banner = banners.get(position);
        imageLoader.load(banner.getBannerImage(), holder.bannerImageItem);

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveRestaurantDetailsPage(banner.getOutlets().get(0).getOutletId(), banner.getOutlets().get(0).getOutletName(),
                        banner.getOutlets().get(0).getCuisines(), banner.getOutlets().get(0).getDisplayTime(),
                        banner.getOutlets().get(0).getAverageReview(), banner.getOutlets().get(0).getDisplayCostForTwo(), banner.getOutlets().get(0).getCouponEnabledForRestaurant(), banner.getOutlets().get(0).getLongDescription());

            }
        });
    }

    private void moveRestaurantDetailsPage(String outletId, String outletName, String cuisines, String displayTime, String averageReview, String displayCostForTwo, String isCouponCodeEnabled, String couponDescription) {

        Intent intent = new Intent(context, RestaurantDetailsActivity.class);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_ID, outletId);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_NAME, outletName);
        intent.putExtra(ConstantKeys.INTENTKEYS.CUISINE_NAME, cuisines);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_MIN, displayTime);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_REVIEW, averageReview);
        intent.putExtra(ConstantKeys.INTENTKEYS.OUTLET_COST_FOR_TWO, displayCostForTwo);
        intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_ENABLED, isCouponCodeEnabled);
        intent.putExtra(ConstantKeys.INTENTKEYS.COUPON_LONG_DESCRIPTION, couponDescription);
        context.startActivity(intent);
    }


    @Override
    public int getItemCount() {
        return (banners != null) ? banners.size() : 0;
    }

    class Restaurant extends RecyclerView.ViewHolder {
        @BindView(R2.id.bannerImageItem)
        ImageView bannerImageItem;
        @BindView(R2.id.button)
        CardView button;

        Restaurant(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
        }
    }
}