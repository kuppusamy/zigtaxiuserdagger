package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.ListDishesResponse.DishItems;
import com.app.foodappuser.Model.Response.ListDishesResponse.SubCategoryValue;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.InterFaces.DishesAdapterInterface;
import com.app.foodappuser.Utilities.InterFaces.onClickListener;
import com.app.foodappuser.Utilities.UiUtils.ExpandableLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DishSubCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final String TAG = DishSubCategoryAdapter.class.getSimpleName();
    onClickListener onClickListener;
    List<SubCategoryValue> dishes;

    private Context context;
    DishesAdapterInterface dishesAdapterInterface;
    private boolean isVegSwitch;
    DishItemsAdapter dishItemsAdapter;

    public DishSubCategoryAdapter(Context context, List<SubCategoryValue> dishes, DishesAdapterInterface dishesAdapterInterface, boolean isVegSwitch) {
        this.context = context;
        this.dishes = dishes;
        this.isVegSwitch = isVegSwitch;
        this.dishesAdapterInterface = dishesAdapterInterface;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.dish_sub_category_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new DishSubCategoryHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

        final DishSubCategoryHolder holder = (DishSubCategoryHolder) viewHolder;
        final SubCategoryValue subCategoryValue = dishes.get(position);
        holder.subCategoryName.setText(subCategoryValue.getCategoryName());
        setSubCategoryAdapter(holder, subCategoryValue);
        holder.topLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.expandableLayout.toggleExpansion();
            }
        });
    }


    private void setSubCategoryAdapter(DishSubCategoryHolder holder, SubCategoryValue subCategoryValue) {
        holder.subCategoryRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        dishItemsAdapter = new DishItemsAdapter(context, subCategoryValue.getDishItems(),subCategoryValue.getVegOnlyValues(), false, dishesAdapterInterface, isVegSwitch);
        holder.subCategoryRecyclerView.setAdapter(dishItemsAdapter);
    }


    @Override
    public int getItemCount() {
        return (dishes != null) ? dishes.size() : 0;
    }


    public void setOnClickListner(onClickListener onClickListner) {
        this.onClickListener = onClickListner;
    }


    public void onDishQuantityChanged(List<DishItems> dishItems, DishItems dish, String quantity) {

        for (int k = 0; k < dishItems.size(); k++) {

            if (dishItems.get(k).getDishId().equalsIgnoreCase(dish.getDishId())) {
//                dishItemsAdapter.onItemChanged(k,Integer.valueOf(quantity));
                Utils.log(TAG, "IF SUBcATEGORY Dish Id : " + dishItems.get(k).getDishId() + " SelectedCategoryId : " + dish.getDishId());
                notifyItemChanged(k);
            }

        }
    }


    class DishSubCategoryHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.subCategoryName)
        TextView subCategoryName;
        @BindView(R2.id.subCategroyNameItems)
        TextView subCategroyNameItems;
        @BindView(R2.id.topLayout)
        LinearLayout topLayout;
        @BindView(R2.id.subCategoryRecyclerView)
        RecyclerView subCategoryRecyclerView;
        @BindView(R2.id.expandableLayout)
        ExpandableLayout expandableLayout;


        DishSubCategoryHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);

        }
    }
}