package com.app.foodappuser.view.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.PastOrderResponse.PastOrder;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.view.activities.LiveTrackingActivity;
import com.app.foodappuser.view.activities.OrderDetailActivity;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PastOrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    List<PastOrder> pastOrders;

    public PastOrdersAdapter(Context context, List<PastOrder> pastOrders) {

        this.context = context;
        this.pastOrders = pastOrders;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.past_order_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new ViewHolderItem(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        final ViewHolderItem holder = (ViewHolderItem) viewHolder;
        final PastOrder pastOrder=pastOrders.get(i);
        List<String> list = Arrays.asList(pastOrder.getDisplayTime().split(","));
        holder.dateAndTime.setText(list.get(0)+ ", "+Utils.convertTimeWithoutSeconds(list.get(1).trim()));
        holder.outletName.setText(pastOrder.getOutletName());
        holder.outletLocation.setText(pastOrder.getOutletLocation());
        holder.totalPrice.setText(pastOrder.getDisplayPrice());
        holder.dishItemsOrdered.setText(pastOrder.getDisplayDishes());
        holder.reOrderTrackButton.setText(pastOrder.getButtonName());

        if(pastOrder.getIsOrderDeliveried()){
            holder.reOrderTrackButton.setVisibility(View.GONE);
        }else{
            holder.reOrderTrackButton.setVisibility(View.VISIBLE);
            if(pastOrder.getButtonName().equalsIgnoreCase(context.getResources().getString(R.string.cancelled))){
                holder.reOrderTrackButton.setEnabled(false);
            }else{
                holder.reOrderTrackButton.setEnabled(true);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, OrderDetailActivity.class);
                intent.putExtra(ConstantKeys.INTENTKEYS.DETAILS,pastOrder.getOrderDetails());
                context.startActivity(intent);
            }
        });

        holder.reOrderTrackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, LiveTrackingActivity.class);
                intent.putExtra(ConstantKeys.INTENTKEYS.ORDER_ID,pastOrder.getOrderId());

                context.startActivity(intent);
            }
        });

        holder.showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return (pastOrders != null) ? pastOrders.size() : 0;
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {

        @BindView(R2.id.outletName)
        TextView outletName;
        @BindView(R2.id.outletLocation)
        TextView outletLocation;
        @BindView(R2.id.totalPrice)
        TextView totalPrice;
        @BindView(R2.id.dishItemsOrdered)
        TextView dishItemsOrdered;
        @BindView(R2.id.dateAndTime)
        TextView dateAndTime;
        @BindView(R2.id.reOrderTrackButton)
        TextView reOrderTrackButton;
        @BindView(R2.id.showMore)
        TextView showMore;

        public ViewHolderItem(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}