package com.app.foodappuser.view.activities;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.SignUpResponseModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.Utilities.Constants.UrlHelper;
import com.app.foodappuser.Utilities.PrefHandler.AppSettings;
import com.app.foodappuser.ViewModel.SignUpViewModel;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {
    String mobileNumber = "";
    String countryCode = "";

    @BindView(R2.id.progressBarOTP)
    ProgressBar progressBarOTP;

    @BindView(R2.id.phoneNumber)
    TextView phoneNumber;

    @BindView(R2.id.signUpText)
    TextView signUpText;

    @BindView(R2.id.terms_and_text)
    TextView terms_and_text;
    @BindView(R2.id.email)
    EditText email;
    @BindView(R2.id.name)
    EditText name;
    @BindView(R2.id.password)
    EditText password;
    @BindView(R2.id.referralCodeCheckbox)
    CheckBox referralCodeCheckbox;

    @BindView(R2.id.referralCode)
    EditText referralCode;

    @BindView(R2.id.backButton)
    ImageView backButton;
    @BindView(R2.id.referralCodeParent)
    LinearLayout referralCodeParent;
    @BindView(R2.id.passwordWrapper)
    TextInputLayout passwordWrapper;

    @BindView(R2.id.signUpButton)
    CardView signUpButton;

    @BindView(R2.id.passwordLayout)
    LinearLayout passwordLayout;


    SignUpViewModel signUpViewModel;
    AppSettings appSettings;

    Bundle bundle;
    private boolean socialLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel.class);

    }

    @Override
    public void onStart() {
        super.onStart();
        getIntentValues();
        setValues();
        initListeners();

    }

    private void initListeners() {

        backButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        referralCodeCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    referralCodeParent.setVisibility(View.VISIBLE);
                } else {
                    referralCodeParent.setVisibility(View.GONE);
                }
            }
        });

        setAlphaToButton();
        signUpText.setText(getResources().getString(R.string.enter_valid_email));

        email.addTextChangedListener(new GenericTextWatcher(email));
        name.addTextChangedListener(new GenericTextWatcher(name));
        if (appSettings.getIsPasswordLoginEnabled().equalsIgnoreCase(ConstantKeys.TRUE_STRING)) {
            password.addTextChangedListener(new GenericTextWatcher(password));
        }

        bundle = getIntent().getExtras();

        if (bundle != null) {
            if(bundle.getString(ConstantKeys.LOGIN_TYPE,"").equalsIgnoreCase("normal")){
                socialLogin = false;
                email.setEnabled(true);
                name.setEnabled(true);
                if (appSettings.getIsPasswordLoginEnabled().equalsIgnoreCase(ConstantKeys.TRUE_STRING)) {
                    passwordLayout.setVisibility(View.VISIBLE);
                } else {
                    passwordLayout.setVisibility(View.GONE);
                }
            }else{
                socialLogin = true;
                passwordLayout.setVisibility(View.GONE);
                email.setText(bundle.getString(ConstantKeys.EMAIL, "email"));
                name.setText(bundle.getString(ConstantKeys.USER_NAME, "hello"));
                email.setEnabled(false);
                name.setEnabled(false);
            }

        }

    }

    private void getIntentValues() {

        appSettings = new AppSettings(SignUpActivity.this);
        mobileNumber = getIntent().getStringExtra(ConstantKeys.MOBILE_NUMBER);
        countryCode = getIntent().getStringExtra(ConstantKeys.COUNTRY_CODE);
        setValues();
    }


    public void postSignUp() throws JSONException {

        progressBarOTP.setVisibility(View.VISIBLE);


        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ConstantKeys.MOBILE_NUMBER, mobileNumber);
        jsonObject.put(ConstantKeys.COUNTRY_CODE, countryCode);
        if (appSettings.getIsPasswordLoginEnabled().equalsIgnoreCase(ConstantKeys.TRUE_STRING)) {
            jsonObject.put(ConstantKeys.PASSWORD, password.getText().toString());
        }
        jsonObject.put(ConstantKeys.USER_NAME, name.getText().toString());
        jsonObject.put(ConstantKeys.REFERRAL_CODE, referralCode.getText().toString());
        jsonObject.put(ConstantKeys.EMAIL, email.getText().toString());
        jsonObject.put(ConstantKeys.UDID, appSettings.getUdId());
        jsonObject.put(ConstantKeys.OTP_NUMBER, appSettings.getOtp());
        if (socialLogin) {
            jsonObject.put(ConstantKeys.LOGIN_TYPE,bundle.getString(ConstantKeys.SOCIAL_LOGIN, "SOCIAL").toUpperCase());
            jsonObject.put(ConstantKeys.SOCIAL_TOKEN,bundle.getString(ConstantKeys.SOCIAL_TOKEN, "socialToken"));
        } else {
            jsonObject.put(ConstantKeys.LOGIN_TYPE,ConstantKeys.NORMAL);
        }

        final InputForAPI inputForAPI = new InputForAPI(SignUpActivity.this);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setFile(null);
        inputForAPI.setHeaders(new HashMap<String, String>());
        inputForAPI.setUrl(UrlHelper.USER_SIGNUP);

//        if (appSettings.getIsPasswordLoginEnabled().equalsIgnoreCase(ConstantKeys.TRUE_STRING)) {
//            inputForAPI.setUrl(UrlHelper.SIGN_UP_WITH_PASSWORD);
//        } else {
//            inputForAPI.setUrl(UrlHelper.SIGN_UP_WITHOUT_PASSWORD);
//        }

        signUpViewModel.signUp(inputForAPI).observe(this, new Observer<SignUpResponseModel>() {
            @Override
            public void onChanged(@Nullable SignUpResponseModel signUpResponseModel) {
                progressBarOTP.setVisibility(View.INVISIBLE);
                if (signUpResponseModel.getError()) {

                    Utils.showSnackBar(findViewById(android.R.id.content), signUpResponseModel.getErrorMessage());
                } else {
                    AppSettings appSettings = new AppSettings(SignUpActivity.this);
                    if(socialLogin){
                        appSettings.setLoginType(bundle.getString(ConstantKeys.SOCIAL_LOGIN, "social"));
                    }else{
                        appSettings.setLoginType("normal");
                    }
                    appSettings.setAccessToken(ConstantKeys.BEARER + signUpResponseModel.getAccessToken());
                    appSettings.setEmail(signUpResponseModel.getUserdetails().getEmail());
                    appSettings.setUsername(signUpResponseModel.getUserdetails().getUserName());
                    appSettings.setMobileNumber(signUpResponseModel.getUserdetails().getMobileNumber());
                    appSettings.setIsLoggedIn(ConstantKeys.TRUE_STRING);

                    moveToMainActivity();

                }
            }
        });
    }

    private void moveToMainActivity() {
        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void setValues() {
        phoneNumber.setText(countryCode + mobileNumber);

        SpannableString ss = new SpannableString(Utils.getString(SignUpActivity.this, R.string.by_creating_account));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                moveToWebActivity();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 37, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue_color)), 37, ss.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        terms_and_text.setText(ss);
        terms_and_text.setMovementMethod(LinkMovementMethod.getInstance());
        terms_and_text.setHighlightColor(Color.TRANSPARENT);
    }

    private void moveToWebActivity() {
        Intent intent = new Intent(SignUpActivity.this, WebViewActivity.class);
        intent.putExtra(ConstantKeys.HEADING, Utils.getString(SignUpActivity.this, R.string.terms_and_conditions));
        intent.putExtra(ConstantKeys.URL, appSettings.getTermsAndConditions());
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        if (view == backButton) {
            finish();
        } else if (view == signUpButton) {
            try {
                postSignUp();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAlphaToButton() {

        signUpButton.setAlpha((float) 0.5);
    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub


            if (Utils.isValidEmail(email.getText().toString())) {
                setAlphaToButton();
                if (name.getText().toString().length() > 1) {
                    if (appSettings.getIsPasswordLoginEnabled().equalsIgnoreCase(ConstantKeys.TRUE_STRING)) {
                        if (socialLogin) {
                            signUpText.setText(getResources().getString(R.string.sign_up));
                            signUpButton.setAlpha(1);
                        } else {
                            if (password.getText().toString().length() < 5) {
                                signUpText.setText(getResources().getString(R.string.enter_valid_password));
                            } else {
                                signUpText.setText(getResources().getString(R.string.sign_up));
                                signUpButton.setAlpha(1);
                            }
                        }
                    } else {
                        signUpText.setText(getResources().getString(R.string.sign_up));
                        signUpButton.setAlpha(1);
                    }


                } else {
                    signUpText.setText(getResources().getString(R.string.enter_name));
                }
            } else {
                setAlphaToButton();
                signUpText.setText(getResources().getString(R.string.enter_valid_email));

            }


        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

}
