package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.MenuItems.MenuItemsModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.InterFaces.OnMenuItemSelected;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    ArrayList<MenuItemsModel> menuItems;
    OnMenuItemSelected onMenuItemSelected;
    public MenuListAdapter(Context context, ArrayList<MenuItemsModel> menuItems) {
        this.context = context;
        this.menuItems=menuItems;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.menu_adapter_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new MenuViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        final MenuViewHolder holder = (MenuViewHolder) viewHolder;
        holder.menuItem.setText(menuItems.get(i).getCategoryName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMenuItemSelected.onItemSelected(menuItems.get(i).getCategoryId(),i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (menuItems!=null)?menuItems.size():0;
    }

    public void setOnClickListener(OnMenuItemSelected onMenuItemSelected) {
        this.onMenuItemSelected=onMenuItemSelected;
    }


    class MenuViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.menuItem)
        TextView menuItem;
        public MenuViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


}