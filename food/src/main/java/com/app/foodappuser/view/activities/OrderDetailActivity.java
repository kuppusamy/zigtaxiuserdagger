package com.app.foodappuser.view.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.PastOrderResponse.Charge;
import com.app.foodappuser.Model.Response.PastOrderResponse.Dish;
import com.app.foodappuser.Model.Response.PastOrderResponse.OrderDetails;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.app.foodappuser.view.Adapters.PastOrderChargesAdapter;
import com.app.foodappuser.view.Adapters.PastOrderDishesAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDetailActivity extends BaseActivity {

    OrderDetails orderDetails;
    Intent i;

    @BindView(R2.id.backButton)
    ImageView backButton;
    @BindView(R2.id.orderNumber)
    TextView orderNumber;
    @BindView(R2.id.totalItem)
    TextView totalItem;
    @BindView(R2.id.toPayAmount)
    TextView toPayAmount;
    @BindView(R2.id.outletName)
    TextView outletName;
    @BindView(R2.id.outletLocation)
    TextView outletLocation;
    @BindView(R2.id.addressTypeName)
    TextView addressTypeName;
    @BindView(R2.id.addressLocation)
    TextView addressLocation;
    @BindView(R2.id.dishesRecycler)
    RecyclerView dishesRecycler;
    @BindView(R2.id.billDetailsRecycler)
    RecyclerView billDetailsRecycler;

    PastOrderDishesAdapter pastOrderDishesAdapter;
    PastOrderChargesAdapter pastOrderChargesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        i = getIntent();
        orderDetails = (OrderDetails) i.getSerializableExtra(ConstantKeys.INTENTKEYS.DETAILS);
    }

    @Override
    public void onStart() {
        super.onStart();
        setViews();
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetailActivity.super.onBackPressed();
            }
        });
    }

    private void setViews() {
        orderNumber.setText(orderDetails.getDisplayOrderId());
        totalItem.setText(orderDetails.getTotalItems());
        toPayAmount.setText(orderDetails.getDisplayPrice());
        outletName.setText(orderDetails.getOutletName());
        outletLocation.setText(orderDetails.getOutletAddress());
        addressTypeName.setText(orderDetails.getUserAddressType());
        addressLocation.setText(orderDetails.getUserAddress());
        setDishesAdapter(orderDetails.getDishes());
        setChargesAdapter(orderDetails.getCharges());
    }

    private void setDishesAdapter(List<Dish> dishes) {
        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        dishesRecycler.setLayoutManager(layout);
        pastOrderDishesAdapter = new PastOrderDishesAdapter(this, dishes);
        dishesRecycler.setAdapter(pastOrderDishesAdapter);
    }

    private void setChargesAdapter(List<Charge> charges) {
        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        billDetailsRecycler.setLayoutManager(layout);
        pastOrderChargesAdapter = new PastOrderChargesAdapter(this, charges);
        billDetailsRecycler.setAdapter(pastOrderChargesAdapter);
    }
}
