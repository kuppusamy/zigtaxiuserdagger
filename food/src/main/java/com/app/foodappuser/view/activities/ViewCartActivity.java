package com.app.foodappuser.view.activities;

import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.app.foodappuser.R;
import com.app.foodappuser.R2;
import com.app.foodappuser.view.Fragments.MyCartFragment;

import butterknife.BindView;

public class ViewCartActivity extends BaseActivity {

    @BindView(R2.id.container)
    FrameLayout container;
    @BindView(R2.id.backButton)
    ImageView backButton;

    FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_view_cart);


        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new MyCartFragment(false));
        transaction.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        setListeners();

    }

    private void setListeners() {
        backButton.setOnClickListener(view -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
