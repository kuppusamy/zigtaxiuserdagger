package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.foodappuser.Model.Custom.SelectedCustomizationsModel;
import com.app.foodappuser.R;
import com.app.foodappuser.R2;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectedCustomizationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<SelectedCustomizationsModel> list;

    public SelectedCustomizationAdapter(Context context, ArrayList<SelectedCustomizationsModel> selectedCustomizableJsonArray) {
        this.context = context;
        this.list = selectedCustomizableJsonArray;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView;
        itemView = LayoutInflater.from(context).inflate(R.layout.selected_customization_dish_item, viewGroup, false);

        RecyclerView.ViewHolder viewHolder;
        viewHolder = new SelectedCustomizationHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        SelectedCustomizationHolder selectedCustomizationHolder = (SelectedCustomizationHolder) viewHolder;
        selectedCustomizationHolder.dishName.setText(list.get(i).getCustomizableName());
    }

    @Override
    public int getItemCount() {
        return (list != null) ? list.size() : 0;
    }

    class SelectedCustomizationHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.dishName)
        TextView dishName;


        public SelectedCustomizationHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
