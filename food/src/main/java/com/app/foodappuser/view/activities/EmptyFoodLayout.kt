package com.app.foodappuser.view.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.app.foodappuser.NavigationInterFaceFood
import com.app.foodappuser.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_backtomain.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import javax.inject.Inject

class EmptyFoodLayout: DaggerAppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, EmptyFoodLayout::class.java)
    }

    @Inject
    lateinit var navigationInterFaceFood : NavigationInterFaceFood
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_backtomain)

        backToMain.setOnClickListener {navigationInterFaceFood.goToMain(this) }




    }
}