package com.app.foodappuser.view.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.foodappuser.Model.Response.GetSavedAddressResponse.Address;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.InterFaces.onClickListener;

import java.util.List;

public class AddressListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private List<Address> addressList;
    onClickListener onClickListener;


    public AddressListAdapter(Context context, List<Address> addressList) {
        this.context = context;
        this.addressList = addressList;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.location_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new AddressHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final AddressHolder holder = (AddressHolder) viewHolder;

        holder.place_name.setText(addressList.get(position).getType());
        holder.place_detail.setText(addressList.get(position).getFullAddress());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClicked(position);
            }
        });


    }

    public void setOnClickListner(onClickListener onClickListner) {
        this.onClickListener=onClickListner;
    }


    @Override
    public int getItemCount() {
        if (addressList != null) {
            return addressList.size();
        } else {
            return 0;
        }
    }

    class AddressHolder extends RecyclerView.ViewHolder {
        ImageView typeImage;
        TextView place_name, place_detail;

        public AddressHolder(@NonNull View itemView) {
            super(itemView);
            typeImage = itemView.findViewById(R.id.typeImage);
            place_name = itemView.findViewById(R.id.place_name);
            place_detail = itemView.findViewById(R.id.place_detail);
        }
    }
}
