package com.app.chat.socket


import android.content.Context
import android.os.Handler
import android.util.Log
import com.app.chat.service.ServiceClass.Companion.getSocketInstance
import com.app.chat.service.ServicesUtils.Companion.isAppIsInBackground
import com.app.chat.socket.SocketParams.sendMessageJson
import com.app.chat.socket.SocketParams.tokenJson
import com.app.chat.socket.SocketParams.updateUnreadCountJSON
import com.app.foodappdriver.Utilities.Constant
import com.example.chat.model.ChatObserver
import io.socket.client.Ack
import org.json.JSONObject


object EmitSocket {

//    private val handler: Handler = Handler()
//    private val handlerOffline = Handler()

    private var updatedOn = false
    private var updatedOff = false
    /*todo Emit from user online event*/
    var contextEmitSocket: Context? = null

    fun emitOnline() {
        println("SocketLog OnlineJson ==> ${tokenJson()}")
        Log.e("SocketLog--", "OnlineJson ==> ${tokenJson()}")

        getSocketInstance()?.emit(Constant.ONLINE, tokenJson(), Ack { args ->

            val data = args[0] as JSONObject
            println("SocketLog ONLINE ack ==> $data")
            Log.e("SocketLog--", "ONLINE ack ==> $data")

        })
//        handler.post(statusCheck)
    }

    /*todo emit send message*/
    fun sendMessage() {
        println("SocketLog sendMessageJson ==> ${sendMessageJson()}")

        getSocketInstance()?.emit(Constant.SEND_MESSAGE, sendMessageJson(), Ack { args ->

            val data = args[0] as JSONObject
            println("SocketLog sendMessage ack ==> $data")
            ChatObserver.getInstance()?.changeState(true)


        })
    }

    fun updateUnreadCount() {
        println("SocketLog updateUnreadCount ==> ${updateUnreadCountJSON()}")
        getSocketInstance()?.emit(Constant.UPDATE_UNREAD_COUNT, updateUnreadCountJSON(), Ack { args ->
            val data = args[0] as JSONObject
            println("SocketLog updateUnreadCount ack ==> $data")
            ChatObserver.getInstance()?.changeState(true)

        })
    }

    private val statusCheck: Runnable = object : Runnable {
        override fun run() {

            val appIsInBackground: Boolean = isAppIsInBackground(contextEmitSocket!!)
            if (appIsInBackground) {
                if (!updatedOff) {
                    Log.e("sendoffline", "sendoffline ")
                    //                    CustomModel.getInstance().changeState(true);
                    // sendoffline(activity)
                    updatedOff = true
                    updatedOn = false
                }
            } else {
                if (!updatedOn) {
                    Log.e("sendOnline", "sendOnline ")
                    emitOnline()
                    updatedOff = false
                    updatedOn = true
                }
            }
//            handlerOffline.postDelayed(this, 1000)
        }
    }
}