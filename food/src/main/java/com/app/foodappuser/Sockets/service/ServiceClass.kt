package com.app.chat.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import com.app.chat.socket.EmitSocket
import com.app.chat.socket.ListenSocket.onReceive
import com.app.chat.socket.ListenSocket.providerReceive
import com.app.chat.socket.ListenSocket.receiveMessageBroadcast
import com.app.foodappdriver.Utilities.Constant
import com.app.foodappuser.Utilities.Constants.UrlHelper
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.client.Url
import io.socket.emitter.Emitter


class ServiceClass : Service() {

//    private val chatSocketUrl = "https://www.bazziferads.com/"

    companion object {
        private var socket: Socket? = null
        fun getSocketInstance() = socket

    }


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Toast.makeText(this,"started",Toast.LENGTH_SHORT)
        socketInitialization()
        return START_NOT_STICKY

    }

    private fun socketInitialization() {

        if (socket != null)
            socketDisConnection()

        Log.e("socketInitialization--", "socketInitialization ")

        val options = IO.Options()
        options.forceNew = true
        options.reconnection = true
        socket = IO.socket(UrlHelper.SOCKET_URL, options)
        socket?.connect()
        socketConnection()
    }

    private fun socketConnection() {

        socket?.on(Socket.EVENT_CONNECT, onConnect)
        socket?.on(Socket.EVENT_DISCONNECT, onDisconnect)
        socket?.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        socket?.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        socket?.on(Constant.RECEIVE_MESSAGE, onReceive)
        socket?.on(Constant.RECEIVE_PROVIDER_LOCATION, providerReceive)
        socket?.on(Constant.RECEIVE_BROADCAST_MESSAGE, receiveMessageBroadcast)
    }

    private fun socketDisConnection() {
        socket?.off(Socket.EVENT_CONNECT, onConnect)
        socket?.off(Socket.EVENT_DISCONNECT, onDisconnect)
        socket?.off(Socket.EVENT_CONNECT_ERROR, onConnectError)
        socket?.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        socket?.off(Constant.RECEIVE_MESSAGE, onReceive)
        socket?.on(Constant.RECEIVE_PROVIDER_LOCATION, providerReceive)
        socket?.off(Constant.RECEIVE_BROADCAST_MESSAGE, receiveMessageBroadcast)

    }

    var isConnect: Boolean = false
    val onConnectError: Emitter.Listener = Emitter.Listener {
        println("SocketLog onConnectError ")
        isConnect = false

    }

    var onConnect: Emitter.Listener = Emitter.Listener {
        println("SocketLog connection ")
        Log.e("SocketLog", "connection")
        if (!isConnect) {
            EmitSocket.emitOnline()
            isConnect = true

        }
    }
    var onDisconnect: Emitter.Listener = Emitter.Listener {
        println("SocketLog disconnection ")
        Log.e("SocketLog", "disconnection")
        isConnect = false
    }


    override fun onDestroy() {
        super.onDestroy()
        stopSelf()
    }


}