package com.app.chat.socket

import com.app.foodappdriver.Utilities.Constant
import org.json.JSONObject

object SocketParams {


    //santosh
    // private var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkYWIzZTUzNWQzODk2MDRkMmEyYzE0MyIsImlhdCI6MTU3MjUyMzM2N30.TF24O8Bh-eRAdejQfGWqHZTb7C80jGUH1UfxYHUoQFU"


    //mocail
    //private var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkNWQxOTY2ZTI2OTI4NjUxZmU2MWRlNSIsImlhdCI6MTU3MzA0MjY3NX0.wvWr6PNO1MUHggcvF1IvLn3edcO50xLUPVWjRvAIBKc"

    var userId: String = ""
    /*online*/
    fun tokenJson(): JSONObject {

        val jsonObject = JSONObject()
        jsonObject.put(Constant.ID, userId)
        return jsonObject
    }


    /*send message*/
//    var chatMessage: ChatMessage? = null
    var toUserName: String? = null
    var toUserId: String? = null

    fun sendMessageJson(): JSONObject {
        val jsonObject = JSONObject()

//        jsonObject.put(Constant.USER_ID, token)
//        jsonObject.put(Constant.TO_ID, chatMessage!!.userId)
//        jsonObject.put(Constant.CONTENT, chatMessage!!.message)
//        jsonObject.put(Constant.CONTENT_TYPE, chatMessage!!.messageType)
//        jsonObject.put(Constant.MSG_ID, chatMessage!!.messageId)
//        jsonObject.put(Constant.MSG_TIMING, chatMessage!!.messagetime)

        return jsonObject

    }
    var toChatRoomId: String=""
    fun updateUnreadCountJSON(): JSONObject {

        val jsonObject = tokenJson()
        jsonObject.put(Constant.CHATROOM_ID, toChatRoomId)
        return jsonObject
    }

}