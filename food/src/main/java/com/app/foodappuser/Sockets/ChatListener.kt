package com.app.foodappdriver.Utilities

import com.app.foodappuser.Model.Response.LiveTrackingSocketResponseModel


interface ChatListener {
    fun onReceiveMessageListener(socketResponse: LiveTrackingSocketResponseModel)
    fun onProviderLocation(socketResponse: LiveTrackingSocketResponseModel)
}