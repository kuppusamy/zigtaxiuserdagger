package com.app.foodappdriver.Utilities


class Constant {
    companion object {



        /*todo Socket Emitter*/
        const val ONLINE: String = "userOnline"
        const val SEND_MESSAGE: String = "sendMessage"
        const val UPDATE_UNREAD_COUNT: String = "updateUnreadCount"

        /*todo Socket Listener*/
        const val RECEIVE_MESSAGE: String = "receiveTracking"
        const val RECEIVE_PROVIDER_LOCATION: String = "providerLocation"
        const val RECEIVE_BROADCAST_MESSAGE: String = "receiveMessageBroadcast"

        /*todo Send message*/
        const val ID: String = "id"
        const val USER_ID: String = "id"
        const val CHATROOM_ID: String = "chatroomId"

        /*todo msg Type*/
        const val TEXT: String = "text"
        const val IMAGE: String = "image"
        const val VIDEO: String = "video"
        const val DOCUMENT: String = "document"


    }


}