package com.example.chat.model

class ChatObserver {
    interface OnCustomStateListener {
        fun stateChanged()
    }

    private var mListener: OnCustomStateListener? = null
    private var mState = false
    fun setListener(listener: OnCustomStateListener) {
        mListener = listener
    }

    companion object {
        private var mInstance: ChatObserver? = null

        fun getInstance(): ChatObserver? {
            if (mInstance == null) {
                mInstance = ChatObserver()
            }
            return mInstance
        }
    }

    fun changeState(state: Boolean) {
        if (mListener != null) {
            mState = state
            notifyStateChange()
        }
    }

    fun getState(): Boolean {
        return mState
    }
    private fun notifyStateChange() {
        mListener!!.stateChanged()
    }


}
