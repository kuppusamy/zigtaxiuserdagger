package com.app.chat.service

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.util.Log



class ServicesUtils {


    companion object {

        fun initChatService(context: Context) {
            Log.e("socketInitialization--", "initChatService ")

            if (!isMyServiceRunning(ServiceClass::class.java, context)) {
                Log.e("socketInitialization--", "isMyServiceRunning ")

                context.startService(Intent(context, ServiceClass::class.java))
            }
        }
        fun stopChatService(context: Context) {
            if (isMyServiceRunning(ServiceClass::class.java, context)) {
                context.stopService(Intent(context, ServiceClass::class.java))
            }

        }

        fun isMyServiceRunning(serviceClass: Class<*>, context: Context): Boolean {
            var finalA = false
            val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.name == service.service.className) {
                    finalA = true
                    break
                }
            }
            return finalA
        }

        @SuppressLint("NewApi")
        fun isAppIsInBackground(context: Context?): Boolean {
            var isInBackground = true
            try {
                val am = context?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                    val runningProcesses = am.runningAppProcesses
                    for (processInfo in runningProcesses) {
                        if (processInfo.importance === ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                            for (activeProcess in processInfo.pkgList) {
                                if (activeProcess == context.packageName) {
                                    isInBackground = false
                                }
                            }
                        }
                    }
                } else {
                    val taskInfo = am.getRunningTasks(1)
                    val componentInfo = taskInfo[0].topActivity
                    if (componentInfo?.packageName.equals(context.packageName)) {
                        isInBackground = false
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return isInBackground
        }


    }
}