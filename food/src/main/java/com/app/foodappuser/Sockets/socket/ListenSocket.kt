package com.app.chat.socket

import com.app.chat.socket.SocketParams.toChatRoomId
import com.app.foodappdriver.Utilities.ChatListener
import com.app.foodappuser.Model.Response.LiveTrackingSocketResponseModel
import com.example.chat.model.ChatObserver
import com.google.gson.Gson
import io.socket.emitter.Emitter
import org.json.JSONObject

object ListenSocket {


    val gSon = Gson()
    var chatListener: ChatListener? = null


    fun onReceiveMsgCallBack(listener: ChatListener) {
        chatListener = listener
    }

    internal var onReceive = Emitter.Listener { args ->
        val data = args[0] as JSONObject
        println("SocketLog onReceive response ==> ${data}")

        val socketResponse: LiveTrackingSocketResponseModel =
            gSon.fromJson(data.toString(), LiveTrackingSocketResponseModel::class.java)
        chatListener?.onReceiveMessageListener(socketResponse)
    }

    internal var providerReceive = Emitter.Listener { args ->
        val data = args[0] as JSONObject
        println("SocketLog onReceive response ==> ${data}")

        val socketResponse: LiveTrackingSocketResponseModel =
            gSon.fromJson(data.toString(), LiveTrackingSocketResponseModel::class.java)
        chatListener?.onProviderLocation(socketResponse)
    }

    internal var receiveMessageBroadcast = Emitter.Listener { args ->
        val data = args[0] as JSONObject
        println("SocketLog receiveMessageBroadcast response ==> $data")

        val socketResponse: LiveTrackingSocketResponseModel =
            gSon.fromJson(data.toString(), LiveTrackingSocketResponseModel::class.java)
        chatListener?.onReceiveMessageListener(socketResponse)
    }


}