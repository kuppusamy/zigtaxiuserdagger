package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.DishItemsResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class DishItemsRepository {

    public DishItemsRepository() {

    }


    public LiveData<DishItemsResponseModel> addDishToCard(InputForAPI input){

        final MutableLiveData<DishItemsResponseModel> dishItemsResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                DishItemsResponseModel dishItemsResponseModel= gson.fromJson(response.toString(), DishItemsResponseModel.class);
                dishItemsResponseModelMutableLiveData.setValue(dishItemsResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                DishItemsResponseModel dishItemsResponseModel=new DishItemsResponseModel();
                dishItemsResponseModel.setError(true);
                dishItemsResponseModel.setErrorMessage(volleyError);
                dishItemsResponseModelMutableLiveData.setValue(dishItemsResponseModel);
            }
        });

        return dishItemsResponseModelMutableLiveData;

    }
}
