package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.EditProfileResponseModel;
import com.app.foodappuser.Model.Response.FaqResponseModel;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Model.Response.PastOrderResponse.PastOrderResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class RatingRepository {

    public RatingRepository() {

    }




    public LiveData<GeneralResponseModel> generalResponseModelLiveData(InputForAPI input) {

        final MutableLiveData<GeneralResponseModel> generalResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson=new Gson();
                GeneralResponseModel generalResponseModel=gson.fromJson(response.toString(),GeneralResponseModel.class);
                generalResponseModelMutableLiveData.setValue(generalResponseModel);
            }

            @Override
            public void setResponseError(String error) {
                GeneralResponseModel generalResponseModel=new GeneralResponseModel();
                generalResponseModel.setError(true);
                generalResponseModel.setErrorMessage(error);
                generalResponseModelMutableLiveData.setValue(generalResponseModel);
            }
        });
        return generalResponseModelMutableLiveData;
    }
}
