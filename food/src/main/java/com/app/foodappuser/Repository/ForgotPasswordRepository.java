package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.ForgotPasswordResponseModel;
import com.app.foodappuser.Model.Response.ResendOtpResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ForgotPasswordRepository {


    public ForgotPasswordRepository() {

    }

    public LiveData<ForgotPasswordResponseModel> getForgotPassword(final InputForAPI input){
        final MutableLiveData<ForgotPasswordResponseModel> forgotPasswordResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ForgotPasswordResponseModel forgotPasswordResponseModel= gson.fromJson(response.toString(), ForgotPasswordResponseModel.class);
                forgotPasswordResponseModelMutableLiveData.setValue(forgotPasswordResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                ForgotPasswordResponseModel forgotPasswordResponseModel=new ForgotPasswordResponseModel();
                forgotPasswordResponseModel.setError(true);
                forgotPasswordResponseModel.setErrorMessage(volleyError);
                forgotPasswordResponseModelMutableLiveData.setValue(forgotPasswordResponseModel);
            }
        });


        return forgotPasswordResponseModelMutableLiveData;
    }

    public LiveData<ResendOtpResponseModel> resendOtpResponseModelLiveData(final InputForAPI input){
        final MutableLiveData<ResendOtpResponseModel> resendOtpResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson=new Gson();
                ResendOtpResponseModel validateOtpResponseModel=gson.fromJson(response.toString(),ResendOtpResponseModel.class);
                resendOtpResponseModelMutableLiveData.setValue(validateOtpResponseModel);
            }

            @Override
            public void setResponseError(String error) {
                ResendOtpResponseModel resendOtpResponseModel=new ResendOtpResponseModel();
                resendOtpResponseModel.setError(true);
                resendOtpResponseModel.setErrorMessage(error);
                resendOtpResponseModelMutableLiveData.setValue(resendOtpResponseModel);
            }
        });

        return resendOtpResponseModelMutableLiveData    ;
    }



}
