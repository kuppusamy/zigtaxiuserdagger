package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.ResendOtpResponseModel;
import com.app.foodappuser.Model.Response.ValidateOtpResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ValidateOtpRepository {
    public ValidateOtpRepository() {
    }

    public LiveData<ValidateOtpResponseModel> validateOtpResponseModelLiveData(final InputForAPI input){
        final MutableLiveData<ValidateOtpResponseModel> validateOtpResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson=new Gson();
                ValidateOtpResponseModel validateOtpResponseModel=gson.fromJson(response.toString(),ValidateOtpResponseModel.class);
                validateOtpResponseModelMutableLiveData.setValue(validateOtpResponseModel);
            }

            @Override
            public void setResponseError(String error) {
                ValidateOtpResponseModel validateOtpResponseModel=new ValidateOtpResponseModel();
                validateOtpResponseModel.setError(true);
                validateOtpResponseModel.setErrorMessage(error);
                validateOtpResponseModelMutableLiveData.setValue(validateOtpResponseModel);
            }
        });

        return validateOtpResponseModelMutableLiveData;
    }

    public LiveData<ResendOtpResponseModel> resendOtpResponseModelLiveData(final InputForAPI input){
        final MutableLiveData<ResendOtpResponseModel> resendOtpResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson=new Gson();
                ResendOtpResponseModel validateOtpResponseModel=gson.fromJson(response.toString(),ResendOtpResponseModel.class);
                resendOtpResponseModelMutableLiveData.setValue(validateOtpResponseModel);
            }

            @Override
            public void setResponseError(String error) {
                ResendOtpResponseModel resendOtpResponseModel=new ResendOtpResponseModel();
                resendOtpResponseModel.setError(true);
                resendOtpResponseModel.setErrorMessage(error);
                resendOtpResponseModelMutableLiveData.setValue(resendOtpResponseModel);
            }
        });

        return resendOtpResponseModelMutableLiveData    ;
    }
}
