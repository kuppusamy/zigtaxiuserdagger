package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.AutoCompleteResponse.AutoCompleteAddressResponseModel;
import com.app.foodappuser.Model.Response.ChooseLocationResponseModel;
import com.app.foodappuser.Model.Response.GetLatLongFromIdResponse.GetLatLongFromIdResponseModel;
import com.app.foodappuser.Model.Response.GetSavedAddressResponse.GetSavedAddressResponseModel;
import com.app.foodappuser.Model.Response.SetCurrentAddressResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class ChooseLocationRepository {



    public ChooseLocationRepository() {

    }

    public LiveData<AutoCompleteAddressResponseModel> getChooseLocation(InputForAPI input){
        final MutableLiveData<AutoCompleteAddressResponseModel> autoCompleteAddressResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                if (response.optString(ConstantKeys.STATUS).equalsIgnoreCase(ConstantKeys.OK)) {
                    try {
                        response.put(ConstantKeys.ERROR_STRING,ConstantKeys.FALSE_STRING);
                        response.put(ConstantKeys.ERROR_MESSAGE_STRING,ConstantKeys.SUCCESS_STRING);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Gson gson = new Gson();
                    AutoCompleteAddressResponseModel predictions = gson.fromJson(response.toString(), AutoCompleteAddressResponseModel.class);
                    autoCompleteAddressResponseModelMutableLiveData.setValue(predictions);
                }else{
                    AutoCompleteAddressResponseModel placePredictions=new AutoCompleteAddressResponseModel();
                    placePredictions.setError(true);
                    placePredictions.setErrorMessage(ConstantKeys.FAILED_STRING);
                    autoCompleteAddressResponseModelMutableLiveData.setValue(placePredictions);
                }
            }

            @Override
            public void setResponseError(String volleyError) {
                AutoCompleteAddressResponseModel placePredictions=new AutoCompleteAddressResponseModel();
                placePredictions.setError(true);
                placePredictions.setErrorMessage(volleyError);
                autoCompleteAddressResponseModelMutableLiveData.setValue(placePredictions);
            }
        });

        return autoCompleteAddressResponseModelMutableLiveData;
    }

    public LiveData<ChooseLocationResponseModel> getCurrentAddress(InputForAPI input){
        final MutableLiveData<ChooseLocationResponseModel> chooseLocationResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                JSONObject jsonObject=new JSONObject();
                try {
                    jsonObject = Utils.getCityAndNameAddressLine(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Gson gson = new Gson();
                ChooseLocationResponseModel chooseLocationResponseModel=gson.fromJson(jsonObject.toString(),ChooseLocationResponseModel.class);
                chooseLocationResponseModelMutableLiveData.setValue(chooseLocationResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                ChooseLocationResponseModel chooseLocationResponseModel=new ChooseLocationResponseModel();
                chooseLocationResponseModel.setError(true);
                chooseLocationResponseModel.setErrorMessage(volleyError);
                chooseLocationResponseModelMutableLiveData.setValue(chooseLocationResponseModel);
            }
        });

        return chooseLocationResponseModelMutableLiveData;
    }


    public LiveData<GetSavedAddressResponseModel> getSavedAddressData(InputForAPI input) {
        final MutableLiveData<GetSavedAddressResponseModel> getSavedAddressResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                GetSavedAddressResponseModel getSavedAddressResponseModel=gson.fromJson(response.toString(),GetSavedAddressResponseModel.class);
                getSavedAddressResponseModelMutableLiveData.setValue(getSavedAddressResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                GetSavedAddressResponseModel getSavedAddressResponseModel=new GetSavedAddressResponseModel();
                getSavedAddressResponseModel.setError(true);
                getSavedAddressResponseModel.setErrorMessage(volleyError);
                getSavedAddressResponseModelMutableLiveData.setValue(getSavedAddressResponseModel);
            }
        });

        return getSavedAddressResponseModelMutableLiveData;
    }


    public LiveData<GetLatLongFromIdResponseModel> getlatitudeAndLogitudeFromId(InputForAPI input){
        final MutableLiveData<GetLatLongFromIdResponseModel> getLatLongFromIdResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                if (response.optString(ConstantKeys.STATUS).equalsIgnoreCase(ConstantKeys.OK)) {
                    try {
                        response.put(ConstantKeys.ERROR_STRING,ConstantKeys.FALSE_STRING);
                        response.put(ConstantKeys.ERROR_MESSAGE_STRING,ConstantKeys.SUCCESS_STRING);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Gson gson = new Gson();
                    GetLatLongFromIdResponseModel getLatLongFromIdResponseModel=gson.fromJson(response.toString(),GetLatLongFromIdResponseModel.class);
                    getLatLongFromIdResponseModelMutableLiveData.setValue(getLatLongFromIdResponseModel);
                }else{
                    GetLatLongFromIdResponseModel getLatLongFromIdResponseModel=new GetLatLongFromIdResponseModel();
                    getLatLongFromIdResponseModel.setError(true);
                    getLatLongFromIdResponseModel.setErrorMessage(ConstantKeys.FAILED_STRING);
                    getLatLongFromIdResponseModelMutableLiveData.setValue(getLatLongFromIdResponseModel);
                }

            }

            @Override
            public void setResponseError(String volleyError) {
                GetLatLongFromIdResponseModel getLatLongFromIdResponseModel=new GetLatLongFromIdResponseModel();
                getLatLongFromIdResponseModel.setError(true);
                getLatLongFromIdResponseModel.setErrorMessage(volleyError);
                getLatLongFromIdResponseModelMutableLiveData.setValue(getLatLongFromIdResponseModel);
            }
        });

        return getLatLongFromIdResponseModelMutableLiveData;
    }

    public LiveData<SetCurrentAddressResponseModel> setCurrentAddress(InputForAPI input) {
        final MutableLiveData<SetCurrentAddressResponseModel> mutableLiveData=new MutableLiveData<>();
        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                SetCurrentAddressResponseModel setCurrentAddressResponseModel=gson.fromJson(response.toString(),SetCurrentAddressResponseModel.class);
                mutableLiveData.setValue(setCurrentAddressResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                SetCurrentAddressResponseModel setCurrentAddressResponseModel=new SetCurrentAddressResponseModel();
                setCurrentAddressResponseModel.setError(true);
                setCurrentAddressResponseModel.setErrorMessage(volleyError);
                mutableLiveData.setValue(setCurrentAddressResponseModel);
            }
        });
        return mutableLiveData;
    }
}
