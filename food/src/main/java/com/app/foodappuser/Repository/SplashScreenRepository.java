package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.SplashScreenResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class SplashScreenRepository {

    public SplashScreenRepository() {
    }

    public LiveData<SplashScreenResponseModel> getSplashScreenResponse(final InputForAPI input){
        final MutableLiveData<SplashScreenResponseModel> splashScreenResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                SplashScreenResponseModel splashScreenResponseModel= gson.fromJson(response.toString(), SplashScreenResponseModel.class);
                splashScreenResponseModelMutableLiveData.setValue(splashScreenResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                SplashScreenResponseModel splashScreenResponseModel=  new SplashScreenResponseModel();
                splashScreenResponseModel.setError(true);
                splashScreenResponseModel.setErrorMessage(volleyError);
                splashScreenResponseModelMutableLiveData.setValue(splashScreenResponseModel);

            }
        });
        return splashScreenResponseModelMutableLiveData;
    }
}
