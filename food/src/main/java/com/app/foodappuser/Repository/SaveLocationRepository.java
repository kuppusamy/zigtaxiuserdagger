package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.LocationResponseModel;
import com.app.foodappuser.Model.Response.SaveLocationResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class SaveLocationRepository {
    public SaveLocationRepository() {

    }

    public LiveData<LocationResponseModel> getChooseLocation(final InputForAPI input) {
        final MutableLiveData<LocationResponseModel> locationResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject = Utils.getCityAndNameAddressLine(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Gson gson = new Gson();
                LocationResponseModel locationResponseModel = gson.fromJson(jsonObject.toString(), LocationResponseModel.class);
                locationResponseModelMutableLiveData.setValue(locationResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                LocationResponseModel locationResponseModel = new LocationResponseModel();
                locationResponseModel.setError(true);
                locationResponseModel.setErrorMessage(volleyError);
                locationResponseModelMutableLiveData.setValue(locationResponseModel);

            }
        });
        return locationResponseModelMutableLiveData;

    }


    public LiveData<SaveLocationResponseModel> saveLocation(InputForAPI input) {
        final MutableLiveData<SaveLocationResponseModel> saveLocationResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                SaveLocationResponseModel saveLocationResponseModel= gson.fromJson(response.toString(), SaveLocationResponseModel.class);
                saveLocationResponseModelMutableLiveData.setValue(saveLocationResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                SaveLocationResponseModel saveLocationResponseModel = new SaveLocationResponseModel();
                saveLocationResponseModel.setError(true);
                saveLocationResponseModel.setErrorMessage(volleyError);
                saveLocationResponseModelMutableLiveData.setValue(saveLocationResponseModel);

            }
        });

        return saveLocationResponseModelMutableLiveData;
    }
}
