package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.LiveTrackingResponse.LiveTrackingResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class LiveTrackingRepository {

    public LiveTrackingRepository() {

    }

    public LiveData<LiveTrackingResponseModel> getTracking(final InputForAPI input){
        final MutableLiveData<LiveTrackingResponseModel> liveTrackingResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                LiveTrackingResponseModel liveTrackingResponseModel= gson.fromJson(response.toString(), LiveTrackingResponseModel.class);
                liveTrackingResponseModelMutableLiveData.setValue(liveTrackingResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                LiveTrackingResponseModel liveTrackingResponseModel=new LiveTrackingResponseModel();
                liveTrackingResponseModel.setError(true);
                liveTrackingResponseModel.setErrorMessage(volleyError);
                liveTrackingResponseModelMutableLiveData.setValue(liveTrackingResponseModel);
            }
        });


        return liveTrackingResponseModelMutableLiveData;
    }

}
