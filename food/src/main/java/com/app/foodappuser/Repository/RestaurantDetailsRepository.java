package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.ListDishesResponse.ListDishesResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class RestaurantDetailsRepository {



    public RestaurantDetailsRepository() {

    }


    public LiveData<ListDishesResponseModel> getDishesInRestaurant(final InputForAPI input) {

        final MutableLiveData<ListDishesResponseModel> dishesLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ListDishesResponseModel listDishesResponse = gson.fromJson(response.toString(), ListDishesResponseModel.class);
                dishesLiveData.setValue(listDishesResponse);
            }

            @Override
            public void setResponseError(String volleyError) {
                ListDishesResponseModel listDishesResponse = new ListDishesResponseModel();
                listDishesResponse.setError(true);
                listDishesResponse.setErrorMessage(volleyError);
                dishesLiveData.setValue(listDishesResponse);

            }
        });

        return dishesLiveData;
    }


}
