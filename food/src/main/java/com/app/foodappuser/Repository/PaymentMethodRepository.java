package com.app.foodappuser.Repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import android.content.Context;

import com.app.foodappuser.Model.Response.OrderConfirmResponseModel;
import com.app.foodappuser.Model.Response.PaymentMethodResponse.PaymentMethodResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.app.foodappuser.Utilities.Stripe.ExampleEphemeralKeyProvider;
import com.google.gson.Gson;
import com.stripe.android.CustomerSession;
import com.stripe.android.model.Customer;
import com.stripe.android.model.CustomerSource;

import org.json.JSONObject;

public class PaymentMethodRepository {

    MutableLiveData<String> cardSource = new MediatorLiveData<>();

    public PaymentMethodRepository() {

    }

    public LiveData<PaymentMethodResponseModel> getPaymentMethods(final InputForAPI input) {

        final MutableLiveData<PaymentMethodResponseModel> paymentMethodResponseModelMutableLiveData = new MutableLiveData<>();

        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                PaymentMethodResponseModel paymentMethodResponseModel = gson.fromJson(response.toString(), PaymentMethodResponseModel.class);
                paymentMethodResponseModelMutableLiveData.setValue(paymentMethodResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                PaymentMethodResponseModel paymentMethodResponseModel = new PaymentMethodResponseModel();
                paymentMethodResponseModel.setError(true);
                paymentMethodResponseModel.setErrorMessage(volleyError);
                paymentMethodResponseModelMutableLiveData.setValue(paymentMethodResponseModel);
            }
        });
        return paymentMethodResponseModelMutableLiveData;
    }

    public void initCustomerSession(Context context) {

        CustomerSession.initCustomerSession(
                new ExampleEphemeralKeyProvider(
                        new ExampleEphemeralKeyProvider.ProgressListener() {
                            @Override
                            public void onStringResponse(String string) {
                                if (string.startsWith("Error: ")) {
                                }
                            }
                        }, context));

        CustomerSession.getInstance().retrieveCurrentCustomer(
                new CustomerSession.CustomerRetrievalListener() {
                    @Override
                    public void onCustomerRetrieved(@NonNull Customer customer) {
                        CustomerSource sourcee = customer.getSourceById(customer.getDefaultSource());
                        String selectedSource;
                        if (sourcee != null) {
                            selectedSource = sourcee.toString();
                            cardSource.setValue(selectedSource);
//                            setStripeCard(selectedSource);
                        }
                    }

                    @Override
                    public void onError(int errorCode, @Nullable String errorMessage) {
                        Utils.log("chek", "error: ");
                    }
                });
    }

    public LiveData<OrderConfirmResponseModel> orderConfirm(final InputForAPI input) {

        final MutableLiveData<OrderConfirmResponseModel> paymentMethodResponseModelMutableLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                OrderConfirmResponseModel paymentMethodResponseModel = gson.fromJson(response.toString(), OrderConfirmResponseModel.class);
                paymentMethodResponseModelMutableLiveData.setValue(paymentMethodResponseModel);
            }


            @Override
            public void setResponseError(String volleyError) {
                OrderConfirmResponseModel paymentMethodResponseModel = new OrderConfirmResponseModel();
                paymentMethodResponseModel.setError(true);
                paymentMethodResponseModel.setErrorMessage(volleyError);
                paymentMethodResponseModelMutableLiveData.setValue(paymentMethodResponseModel);
            }
        });

        return paymentMethodResponseModelMutableLiveData;
    }

    public MutableLiveData<String> setCardSource() {
        return cardSource;
    }

}
