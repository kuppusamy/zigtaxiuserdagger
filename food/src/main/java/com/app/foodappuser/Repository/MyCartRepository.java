package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.CouponResponseModel;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Model.Response.ListDeliveryResponse.ListDeliveryResponseModel;
import com.app.foodappuser.Model.Response.ViewCartResponse.ViewCartResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class MyCartRepository {

    public MyCartRepository() {
    }

    public LiveData<GeneralResponseModel> addToCart(InputForAPI input){

        final MutableLiveData<GeneralResponseModel> generalResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GeneralResponseModel generalResponseModel= gson.fromJson(response.toString(), GeneralResponseModel.class);
                generalResponseModelMutableLiveData.setValue(generalResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                GeneralResponseModel generalResponseModel=new GeneralResponseModel();
                generalResponseModel.setError(true);
                generalResponseModel.setErrorMessage(volleyError);
                generalResponseModelMutableLiveData.setValue(generalResponseModel);
            }
        });


        return generalResponseModelMutableLiveData;

    }

    public LiveData<GeneralResponseModel> updateCart(InputForAPI input){

        final MutableLiveData<GeneralResponseModel> generalResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GeneralResponseModel generalResponseModel= gson.fromJson(response.toString(), GeneralResponseModel.class);
                generalResponseModelMutableLiveData.setValue(generalResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                GeneralResponseModel generalResponseModel=new GeneralResponseModel();
                generalResponseModel.setError(true);
                generalResponseModel.setErrorMessage(volleyError);
                generalResponseModelMutableLiveData.setValue(generalResponseModel);
            }
        });


        return generalResponseModelMutableLiveData;

    }

    public LiveData<ViewCartResponseModel> viewCart(InputForAPI input){

        final MutableLiveData<ViewCartResponseModel> viewCartResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ViewCartResponseModel viewCartResponseModel= gson.fromJson(response.toString(), ViewCartResponseModel.class);
                viewCartResponseModelMutableLiveData.setValue(viewCartResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                ViewCartResponseModel viewCartResponseModel=new ViewCartResponseModel();
                viewCartResponseModel.setError(true);
                viewCartResponseModel.setErrorMessage(volleyError);
                viewCartResponseModelMutableLiveData.setValue(viewCartResponseModel);
            }
        });

        return viewCartResponseModelMutableLiveData;

    }

    public LiveData<ListDeliveryResponseModel> getDeliveryAddress(InputForAPI input){

        final MutableLiveData<ListDeliveryResponseModel> listDeliveryResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ListDeliveryResponseModel listDeliveryResponseModel= gson.fromJson(response.toString(), ListDeliveryResponseModel.class);
                listDeliveryResponseModelMutableLiveData.setValue(listDeliveryResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                ListDeliveryResponseModel listDeliveryResponseModel=new ListDeliveryResponseModel();
                listDeliveryResponseModel.setError(true);
                listDeliveryResponseModel.setErrorMessage(volleyError);
                listDeliveryResponseModelMutableLiveData.setValue(listDeliveryResponseModel);
            }
        });

        return listDeliveryResponseModelMutableLiveData;

    }

    public LiveData<CouponResponseModel> getAddCouponResponse(InputForAPI input){

        final MutableLiveData<CouponResponseModel> couponResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CouponResponseModel couponResponseModel= gson.fromJson(response.toString(), CouponResponseModel.class);
                couponResponseModelMutableLiveData.setValue(couponResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                CouponResponseModel couponResponseModel=new CouponResponseModel();
                couponResponseModel.setError(true);
                couponResponseModel.setErrorMessage(volleyError);
                couponResponseModelMutableLiveData.setValue(couponResponseModel);
            }
        });

        return couponResponseModelMutableLiveData;

    }

}
