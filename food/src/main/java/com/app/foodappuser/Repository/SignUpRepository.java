package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.SignUpResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class SignUpRepository {


    public SignUpRepository() {

    }

    public LiveData<SignUpResponseModel> getSignUpResponse(final InputForAPI input) {

        final MutableLiveData<SignUpResponseModel> signUpResponseModelMutableLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                SignUpResponseModel signUpResponseModel = gson.fromJson(response.toString(), SignUpResponseModel.class);
                signUpResponseModelMutableLiveData.setValue(signUpResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {

                SignUpResponseModel signUpResponseModel = new SignUpResponseModel();
                signUpResponseModel.setError(true);
                signUpResponseModel.setErrorMessage(volleyError);
                signUpResponseModelMutableLiveData.setValue(signUpResponseModel);
            }
        });

        return signUpResponseModelMutableLiveData;
    }


}
