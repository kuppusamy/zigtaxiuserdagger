package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.ChooseLocationResponseModel;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Model.Response.GetCurrentAddressResponse.GetCurrentAddressResponseModel;
import com.app.foodappuser.Model.Response.ListRestaurantResponse.ListRestaurantResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.BaseUtils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class NearMeRepository {



    public NearMeRepository() {

    }


    public LiveData<ListRestaurantResponseModel> getRestaurantsNearMe(final InputForAPI input) {

        final MutableLiveData<ListRestaurantResponseModel> nearMeLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ListRestaurantResponseModel restaurantResponseModel = gson.fromJson(response.toString(), ListRestaurantResponseModel.class);
                nearMeLiveData.setValue(restaurantResponseModel);
            }


            @Override
            public void setResponseError(String volleyError) {
                ListRestaurantResponseModel restaurantResponseModel =  new ListRestaurantResponseModel();
                restaurantResponseModel.setError(true);
                restaurantResponseModel.setErrorMessage(volleyError);
                nearMeLiveData.setValue(restaurantResponseModel);
            }
        });

        return nearMeLiveData;
    }

    public LiveData<GetCurrentAddressResponseModel> getCurrentAddress(final InputForAPI input) {

        final MutableLiveData<GetCurrentAddressResponseModel> getCurrentAddressResponseModelMutableLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GetCurrentAddressResponseModel getCurrentAddressResponseModel = gson.fromJson(response.toString(), GetCurrentAddressResponseModel.class);
                getCurrentAddressResponseModelMutableLiveData.setValue(getCurrentAddressResponseModel);
            }


            @Override
            public void setResponseError(String volleyError) {
                GetCurrentAddressResponseModel getCurrentAddressResponseModel =  new GetCurrentAddressResponseModel();
                getCurrentAddressResponseModel.setError(true);
                getCurrentAddressResponseModel.setErrorMessage(volleyError);
                getCurrentAddressResponseModelMutableLiveData.setValue(getCurrentAddressResponseModel);
            }
        });

        return getCurrentAddressResponseModelMutableLiveData;
    }

    public LiveData<GeneralResponseModel> updateDeviceToken(final InputForAPI input) {

        final MutableLiveData<GeneralResponseModel> getCurrentAddressResponseModelMutableLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GeneralResponseModel getCurrentAddressResponseModel = gson.fromJson(response.toString(), GeneralResponseModel.class);
                getCurrentAddressResponseModelMutableLiveData.setValue(getCurrentAddressResponseModel);
            }


            @Override
            public void setResponseError(String volleyError) {
                GeneralResponseModel getCurrentAddressResponseModel =  new GeneralResponseModel();
                getCurrentAddressResponseModel.setError(true);
                getCurrentAddressResponseModel.setErrorMessage(volleyError);
                getCurrentAddressResponseModelMutableLiveData.setValue(getCurrentAddressResponseModel);
            }
        });

        return getCurrentAddressResponseModelMutableLiveData;
    }



    public LiveData<ChooseLocationResponseModel> getCurrentAddressFromLatLong(InputForAPI input){
        final MutableLiveData<ChooseLocationResponseModel> chooseLocationResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                JSONObject jsonObject=new JSONObject();
                try {
                    jsonObject = Utils.getCityAndNameAddressLine(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Gson gson = new Gson();
                ChooseLocationResponseModel chooseLocationResponseModel=gson.fromJson(jsonObject.toString(),ChooseLocationResponseModel.class);
                chooseLocationResponseModelMutableLiveData.setValue(chooseLocationResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                ChooseLocationResponseModel chooseLocationResponseModel=new ChooseLocationResponseModel();
                chooseLocationResponseModel.setError(true);
                chooseLocationResponseModel.setErrorMessage(volleyError);
                chooseLocationResponseModelMutableLiveData.setValue(chooseLocationResponseModel);
            }
        });

        return chooseLocationResponseModelMutableLiveData;
    }



}
