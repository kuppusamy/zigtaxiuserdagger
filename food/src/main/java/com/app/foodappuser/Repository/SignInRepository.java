package com.app.foodappuser.Repository;

import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.CheckUserAvailabilityResponseModel;
import com.app.foodappuser.Model.Response.SignInResponseModelResponse.SignInResponseModel;
import com.app.foodappuser.Model.Response.SocialLoginResponse.FacebookLoginModel;
import com.app.foodappuser.Model.Response.SocialLoginResponse.SocialLoginModel;
import com.app.foodappuser.R;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.app.foodappuser.Utilities.Constants.ConstantKeys;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class SignInRepository {



    public SignInRepository() {

    }

    public LiveData<CheckUserAvailabilityResponseModel> getCheckNumberResponse(final InputForAPI input) {

        final MutableLiveData<CheckUserAvailabilityResponseModel> checkUserAvailabilityResponseModelMutableLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CheckUserAvailabilityResponseModel checkUserAvailabilityResponseModel = gson.fromJson(response.toString(), CheckUserAvailabilityResponseModel.class);
                checkUserAvailabilityResponseModelMutableLiveData.setValue(checkUserAvailabilityResponseModel);
            }


            @Override
            public void setResponseError(String volleyError) {
                CheckUserAvailabilityResponseModel checkUserAvailabilityResponseModel =  new CheckUserAvailabilityResponseModel();
                checkUserAvailabilityResponseModel.setError(true);
                checkUserAvailabilityResponseModel.setErrorMessage(volleyError);
                checkUserAvailabilityResponseModelMutableLiveData.setValue(checkUserAvailabilityResponseModel);
            }
        });

        return checkUserAvailabilityResponseModelMutableLiveData;
    }

    public LiveData<SignInResponseModel> login(final InputForAPI input) {

        final MutableLiveData<SignInResponseModel> nearMeLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                SignInResponseModel signInResponseModel = gson.fromJson(response.toString(), SignInResponseModel.class);
                nearMeLiveData.setValue(signInResponseModel);
            }


            @Override
            public void setResponseError(String volleyError) {
                SignInResponseModel signInResponseModel =  new SignInResponseModel();
                signInResponseModel.setError(true);
                signInResponseModel.setErrorMessage(volleyError);
                nearMeLiveData.setValue(signInResponseModel);
            }
        });

        return nearMeLiveData;
    }

    public LiveData<SocialLoginModel> socialLoginModelLiveData(final InputForAPI input) {

        final MutableLiveData<SocialLoginModel> socialLoginModelMutableLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                SocialLoginModel socialLoginModel = gson.fromJson(response.toString(), SocialLoginModel.class);
                socialLoginModelMutableLiveData.setValue(socialLoginModel);
            }


            @Override
            public void setResponseError(String volleyError) {
                SocialLoginModel socialLoginModel =  new SocialLoginModel();
                socialLoginModel.setError(true);
                socialLoginModel.setErrorMessage(volleyError);
                socialLoginModelMutableLiveData.setValue(socialLoginModel);
            }
        });

        return socialLoginModelMutableLiveData;
    }

    public LiveData<FacebookLoginModel> facebookData(final LoginResult loginResult) {

        final MutableLiveData<FacebookLoginModel> socialLoginModelMutableLiveData = new MutableLiveData<>();

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        Profile profile = Profile.getCurrentProfile();
                        String id = profile.getId();
                        try {
                            FacebookLoginModel facebookLoginModel=new FacebookLoginModel();
                            facebookLoginModel.setError(false);
                            facebookLoginModel.setMessage("Success");
                            facebookLoginModel.setEmail(response.getJSONObject().getString("email"));
                            facebookLoginModel.setFirstName(response.getJSONObject().getString("first_name"));
                            facebookLoginModel.setLastName(response.getJSONObject().getString("last_name"));
                            facebookLoginModel.setProfileId(id);
                            socialLoginModelMutableLiveData.setValue(facebookLoginModel);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender");
        request.setParameters(parameters);
        request.executeAsync();

        return socialLoginModelMutableLiveData;
    }





}
