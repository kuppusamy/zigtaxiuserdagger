package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.ExploreResponse.ExploreResponseModel;
import com.app.foodappuser.Model.Response.ListRestaurantResponse.ListRestaurantResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ExploreRepository {

    public ExploreRepository() {

    }
    public LiveData<ExploreResponseModel> getRestaurantsNearMe(final InputForAPI input) {

        final MutableLiveData<ExploreResponseModel> nearMeLiveData = new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ExploreResponseModel restaurantResponseModel = gson.fromJson(response.toString(), ExploreResponseModel.class);
                nearMeLiveData.setValue(restaurantResponseModel);
            }


            @Override
            public void setResponseError(String volleyError) {
                ExploreResponseModel restaurantResponseModel =  new ExploreResponseModel();
                restaurantResponseModel.setError(true);
                restaurantResponseModel.setErrorMessage(volleyError);
                nearMeLiveData.setValue(restaurantResponseModel);
            }
        });

        return nearMeLiveData;
    }

}
