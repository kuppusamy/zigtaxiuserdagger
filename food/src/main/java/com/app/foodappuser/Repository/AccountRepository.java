package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.EditProfileResponseModel;
import com.app.foodappuser.Model.Response.FaqResponseModel;
import com.app.foodappuser.Model.Response.GeneralResponseModel;
import com.app.foodappuser.Model.Response.PastOrderResponse.PastOrderResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class AccountRepository {

    public AccountRepository() {

    }

    public LiveData<EditProfileResponseModel> updateProfile(InputForAPI input){

        final MutableLiveData<EditProfileResponseModel> editProfileResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                EditProfileResponseModel editProfileResponseModel= gson.fromJson(response.toString(), EditProfileResponseModel.class);
                editProfileResponseModelMutableLiveData.setValue(editProfileResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                EditProfileResponseModel editProfileResponseModel=new EditProfileResponseModel();
                editProfileResponseModel.setError(true);
                editProfileResponseModel.setErrorMessage(volleyError);
                editProfileResponseModelMutableLiveData.setValue(editProfileResponseModel);
            }
        });


        return editProfileResponseModelMutableLiveData;

    }

    public LiveData<FaqResponseModel> getFaqResponseModel(InputForAPI input){
        final MutableLiveData<FaqResponseModel> faqResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson=new Gson();
                FaqResponseModel faqResponseModel=gson.fromJson(response.toString(),FaqResponseModel.class);
                faqResponseModelMutableLiveData.setValue(faqResponseModel);
            }

            @Override
            public void setResponseError(String error) {
                FaqResponseModel faqResponseModel=new FaqResponseModel();
                faqResponseModel.setError(true);
                faqResponseModel.setErrorMessage(error);
                faqResponseModelMutableLiveData.setValue(faqResponseModel);
            }
        });
        return faqResponseModelMutableLiveData;
    }


    public LiveData<PastOrderResponseModel> getPastOrders(InputForAPI input) {

        final MutableLiveData<PastOrderResponseModel> pastOrderResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson=new Gson();
                PastOrderResponseModel pastOrderResponseModel=gson.fromJson(response.toString(),PastOrderResponseModel.class);
                pastOrderResponseModelMutableLiveData.setValue(pastOrderResponseModel);
            }

            @Override
            public void setResponseError(String error) {
                PastOrderResponseModel pastOrderResponseModel=new PastOrderResponseModel();
                pastOrderResponseModel.setError(true);
                pastOrderResponseModel.setErrorMessage(error);
                pastOrderResponseModelMutableLiveData.setValue(pastOrderResponseModel);
            }
        });
        return pastOrderResponseModelMutableLiveData;
    }
    public LiveData<GeneralResponseModel> generalResponseModelLiveData(InputForAPI input) {

        final MutableLiveData<GeneralResponseModel> generalResponseModelMutableLiveData=new MutableLiveData<>();

        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson=new Gson();
                GeneralResponseModel generalResponseModel=gson.fromJson(response.toString(),GeneralResponseModel.class);
                generalResponseModelMutableLiveData.setValue(generalResponseModel);
            }

            @Override
            public void setResponseError(String error) {
                GeneralResponseModel generalResponseModel=new GeneralResponseModel();
                generalResponseModel.setError(true);
                generalResponseModel.setErrorMessage(error);
                generalResponseModelMutableLiveData.setValue(generalResponseModel);
            }
        });
        return generalResponseModelMutableLiveData;
    }
}
