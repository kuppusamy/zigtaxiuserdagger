package com.app.foodappuser.Repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.foodappuser.Model.Response.DeleteAddressResponseModel;
import com.app.foodappuser.Model.Response.GetSavedAddressResponse.GetSavedAddressResponseModel;
import com.app.foodappuser.Utilities.ApiCall.ApiCall;
import com.app.foodappuser.Utilities.ApiCall.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ManageAddressRepository {

    public ManageAddressRepository() {

    }

    public LiveData<GetSavedAddressResponseModel> getSavedAddressData(InputForAPI input) {
        final MutableLiveData<GetSavedAddressResponseModel> getSavedAddressResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.GetMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                GetSavedAddressResponseModel getSavedAddressResponseModel=gson.fromJson(response.toString(),GetSavedAddressResponseModel.class);
                getSavedAddressResponseModelMutableLiveData.setValue(getSavedAddressResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                GetSavedAddressResponseModel getSavedAddressResponseModel=new GetSavedAddressResponseModel();
                getSavedAddressResponseModel.setError(true);
                getSavedAddressResponseModel.setErrorMessage(volleyError);
                getSavedAddressResponseModelMutableLiveData.setValue(getSavedAddressResponseModel);
            }
        });

        return getSavedAddressResponseModelMutableLiveData;
    }

    public LiveData<DeleteAddressResponseModel> deleteAddressResponseModelLiveData(InputForAPI input) {
        final MutableLiveData<DeleteAddressResponseModel> deleteAddressResponseModelMutableLiveData=new MutableLiveData<>();
        ApiCall.PostMethod(input, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                DeleteAddressResponseModel deleteAddressResponseModel=gson.fromJson(response.toString(),DeleteAddressResponseModel.class);
                deleteAddressResponseModelMutableLiveData.setValue(deleteAddressResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                DeleteAddressResponseModel deleteAddressResponseModel=new DeleteAddressResponseModel();
                deleteAddressResponseModel.setError(true);
                deleteAddressResponseModel.setErrorMessage(volleyError);
                deleteAddressResponseModelMutableLiveData.setValue(deleteAddressResponseModel);
            }
        });

        return deleteAddressResponseModelMutableLiveData;
    }


}
